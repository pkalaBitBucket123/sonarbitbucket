package com.connecture.shopping.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.Scriptable;

public class DustCompiler
{

  /**
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException
  {
    System.out.println("Compiling Dust Templates");

    String scriptBase = args[0];
    String htmlBase = args[1];

    Context cx = Context.enter();
    cx.setOptimizationLevel(9);

    Scriptable globalScope = cx.initStandardObjects();

    String dust = DustCompiler.readFile(scriptBase + File.separator + "scripts"
      + File.separator + "thirdparty" + File.separator + "dust-full-1.1.1.js");
    cx.evaluateString(globalScope, dust, "dust", 0, null);

    String dustHelpers = DustCompiler.readFile(scriptBase + File.separator + "scripts"
      + File.separator + "thirdparty" + File.separator + "dust-helpers-1.1.0.js");
    cx.evaluateString(globalScope, dustHelpers, "dustHelpers", 0, null);

    File directory = new File(htmlBase);

    Collection<File> dustTemplates = FileUtils.listFiles(directory,
      new WildcardFileFilter("*.dust"), TrueFileFilter.INSTANCE);

    ArrayList<File> templates = new ArrayList<File>(dustTemplates);

    for (File file : templates)
    {
      String compiled = DustCompiler.compile(globalScope, cx, DustCompiler.readFile(file),
        FilenameUtils.getBaseName(file.getName()));
      System.out.println("Compiling:" + file.getPath());

      FileWriter fooWriter = new FileWriter(file, false); // true to append
      // false to overwrite.
      fooWriter.write(compiled);
      fooWriter.flush();
      fooWriter.close();
    }

  }

  private static String readFile(String path) throws IOException
  {
    return DustCompiler.readFile(new File(path));
  }

  private static String readFile(File file) throws IOException
  {
    return FileUtils.readFileToString(file);
  }

  private static String compile(
    Scriptable globalScope,
    Context context,
    String template,
    String name)
  {
    try
    {
      Scriptable compileScope = context.newObject(globalScope);
      compileScope.setParentScope(globalScope);
      compileScope.put("rawSource", compileScope, template);
      compileScope.put("name", compileScope, name);

      String compile = (String) context.evaluateString(compileScope,
        "(dust.compile(rawSource, name))", "JDustCompiler", 0, null);
      return compile;
    }
    catch (JavaScriptException e)
    {
      // Fail hard on any compile time error for dust templates
      throw new RuntimeException(e);
    }
  }

}
