/**
 * 
 */
package com.connecture.shopping.ui;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ServerErrorJSONAction extends ActionSupport
{
  private JSONObject jsonObject;
  private Exception exception;

  public String execute() throws Exception
  {
    // Client side is using server errors to better manage known issues vs
    // unknown issues
    final HttpServletResponse response = (HttpServletResponse) ActionContext.getContext().get(
      ServletActionContext.HTTP_RESPONSE);

    // Ok we cannot return a response of 400 or 500 from here because we are not
    // sure how consuming web containers will modify that response. Instead we
    // are going to set an error header for the client side to check for cases
    // of exceptions.

    // Prefixing custom headers with X- has been deprecated so using, SH for now
    // http://tools.ietf.org/html/rfc6648
    response.setHeader("SH-Standard-Error", "true");

    jsonObject = new JSONObject();
    if (exception instanceof org.hibernate.StaleObjectStateException)
    {
      jsonObject.put("resultCode", "concurrency");
      jsonObject.put("message", "The information you are trying to modify has been updated by "
        + "another user. Please review the updated information and modify where necessary.");
    }
    else
    {
      jsonObject.put("resultCode", "error");
      jsonObject.put("message", exception.getMessage());
      jsonObject.put("exceptionName", exception.getClass().getSimpleName());
      jsonObject.put("exceptionClass", exception.getClass().getName());
      StringWriter writer = new StringWriter();
      exception.printStackTrace(new PrintWriter(writer));
      jsonObject.put("exceptionStack", writer.toString());
    }

    TokenHelperJSON.appendToken(jsonObject);

    return SUCCESS;
  }

  /**
   * @return the jsonObject
   */
  public JSONObject getJsonObject()
  {
    return jsonObject;
  }

  /**
   * @param jsonObject the new value of jsonObject
   */
  public void setJsonObject(JSONObject jsonObject)
  {
    this.jsonObject = jsonObject;
  }

  /**
   * @return the exception
   */
  public Exception getException()
  {
    return exception;
  }

  /**
   * @param exception the new value of exception
   */
  public void setException(Exception exception)
  {
    this.exception = exception;
  }
}