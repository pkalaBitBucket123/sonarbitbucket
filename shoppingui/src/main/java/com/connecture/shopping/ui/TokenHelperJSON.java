package com.connecture.shopping.ui;

import java.util.Map;

import org.apache.struts2.util.TokenHelper;
import org.json.JSONException;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;

/**
 * Helper class for working with webwork tokens and webwork json support.
 */
public class TokenHelperJSON
{
  /**
   * @param responseJSON
   * @throws JSONException
   */
  public static void appendToken(JSONObject responseJSON) throws JSONException
  {
    String tokenName = getTokenName();

    String token = TokenHelper.setToken(tokenName);

    TokenHelperJSON.appendToken(responseJSON, tokenName, token);
  }

  /**
   * @param responseJSON
   * @param appendExisting
   * @throws JSONException
   */
  public static void appendToken(JSONObject responseJSON, boolean appendExisting)
    throws JSONException
  {
    String tokenName = TokenHelperJSON.getTokenName();
    Map<String, Object> session = ActionContext.getContext().getSession();
    String token = (String) session.get(tokenName);

    if (token == null || !appendExisting)
    {
      TokenHelperJSON.appendToken(responseJSON);
    }
    else
    {
      TokenHelperJSON.appendToken(responseJSON, tokenName, token);
    }
  }

  /**
   * @param responseJSON
   * @param tokenName
   * @param token
   * @throws JSONException
   */
  public static void appendToken(JSONObject responseJSON, String tokenName, String token)
    throws JSONException
  {
    responseJSON.put(TokenHelper.TOKEN_NAME_FIELD, tokenName);
    responseJSON.put(tokenName, token);
  }

  /**
   * @return
   */
  public static String getTokenName()
  {
    String tokenName = TokenHelper.getTokenName();
    if (tokenName == null)
    {
      tokenName = TokenHelper.DEFAULT_TOKEN_NAME;
    }
    return tokenName;
  }
}
