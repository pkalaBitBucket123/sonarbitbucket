package com.connecture.shopping.ui;

import org.apache.commons.lang.StringUtils;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.ActorService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Ideally this class or something like it would determine the role or the
 * context of the incoming user and redirect them to the appropriate html page
 * that will launch that app for them
 * 
 * @author Tcvetan
 */
public class EntryPointAction extends ActionSupport
{
	
  private String transactionId;
  private String producerToken;
  // This would most likely be temporary. Right now it is a way
  // for us to test switching
  private String context;
  private String route;
  
  private ActorService ifpActorService;
  private Account sessionAccountImpl;
  private Account persistedAccountImpl;
  private String coverageType;
  private long effectiveMs;
  
 

  /**
   * 
   */
  private static final long serialVersionUID = -4650018284256922444L;

  public String handleRequest() throws Exception
  {
    ShoppingContext ctx = ShoppingContext.fromContextName(context);
    // N.B. we are lower-casing the contexts for the URL / client-side use
     
    if (ctx == null)
    {
      ctx = ShoppingContext.ANONYMOUS;
      context = ctx.getContextName();
    }

    // we shouldn't throw empty transactionIds and empty producerTokens into the URL,
    // so things get messy. values here should match shopping-struts with the appropriate params.
    // TODO | find a better way around this when time allows (CK 5/30/2013)
    if (ctx == ShoppingContext.ANONYMOUS)
    {
      Actor actor = ifpActorService.getActorData();
      if (actor.isInternalUser()){
        // clear out the shopping session data.
        // need to do this to make sure that we don't 
        // load previous quote data when we try to start up a new quote.
        sessionAccountImpl.clearAccount();
      }
           
      if (!StringUtils.isBlank(transactionId))
      {
        ctx = ShoppingContext.ANON_LEAD;
        if (!StringUtils.isBlank(producerToken))
        {
          ctx = ShoppingContext.ANON_LEAD_PRODUCER;
        }
      }
    }
    if( transactionId !=null && persistedAccountImpl.getOrCreateAccount(transactionId)!=null ){
    	persistedAccountImpl.getAccount(transactionId).put("coverageType", coverageType);
    	if(effectiveMs != 0 && ("DEN".equalsIgnoreCase(coverageType) || "STH".equalsIgnoreCase(coverageType))){
    	  persistedAccountImpl.getAccount(transactionId).put("effectiveMs", effectiveMs);
    	}
    	  
    }
    // this could do something more robust, for now the context is fine
    return ctx.getContextName();
  }

  /**
   * @return the transactionId
   */
  public String getTransactionId()
  {
    return transactionId;
  }

  /**
   * @param transactionId the new value of transactionId
   */
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public String getProducerToken()
  {
    return producerToken;
  }

  public void setProducerToken(String producerToken)
  {
    this.producerToken = producerToken;
  }

  public String getContext()
  {
    return context;
  }

  public void setContext(String context)
  {
    if (context != null)
    {
      this.context = context.toLowerCase();
    }
  }

  public String getRoute()
  {
    return route;
  }

  public void setRoute(String route)
  {
    this.route = route;
  }

  public void setIfpActorService(ActorService ifpActorService)
  {
    this.ifpActorService = ifpActorService;
  }

  public void setSessionAccountImpl(Account sessionAccountImpl)
  {
    this.sessionAccountImpl = sessionAccountImpl;
  }
  public void setPersistedAccountImpl(Account persistedAccountImpl)
  {
    this.persistedAccountImpl = persistedAccountImpl;
  }
  
  public String getCoverageType()
  {
	return coverageType;
  }

  public void setCoverageType(String coverageType) 
  {
	this.coverageType = coverageType;
  }
    
  public long getEffectiveMs()
  {
    return effectiveMs;
  }

  public void setEffectiveMs(long effectiveMs)
  {
    this.effectiveMs = effectiveMs;
  }
}