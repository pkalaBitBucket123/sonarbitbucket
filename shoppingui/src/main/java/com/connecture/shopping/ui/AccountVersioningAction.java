package com.connecture.shopping.ui;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.AccountVersioningProcess;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class AccountVersioningAction extends ActionSupport
{
  private static Logger LOG = Logger.getLogger(AccountVersioningAction.class);

  private AccountVersioningProcess accountVersioningProcess;
  private JSONObject jsonRequestObject; 
  private JSONObject jsonObject;
  
  public String versionAccounts() throws Exception {
    try
    {
      if (null != jsonRequestObject && jsonRequestObject.has("convertToVersion"))
      {
        accountVersioningProcess.setConvertToVersion(jsonRequestObject.getString("convertToVersion"));
      }
      accountVersioningProcess.versionAccounts();
      jsonObject = new JSONObject();
      jsonObject.put("convertToVersion", accountVersioningProcess.getConvertToVersion());
      jsonObject.put("errors", accountVersioningProcess.getErrors());
      jsonObject.put("converted", accountVersioningProcess.getConvertedAccounts().size());
      jsonObject.put("errored", accountVersioningProcess.getErroredAccounts().size());
    }
    catch (Exception e)
    {
      LOG.error("failed to convert accounts", e);
    }

    return SUCCESS;
  }
  
  public JSONObject getJsonObject() 
  {
    return jsonObject;
  }
  
  public void setAccountVersioningProcess(AccountVersioningProcess accountVersioningProcess)
  {
    this.accountVersioningProcess = accountVersioningProcess;
  }

  /**
   * @param jsonRequestObject the new value of jsonRequestObject
   * @throws JSONException 
   */
  public void setJsonRequestObject(String jsonRequestObject) throws JSONException
  {
    this.jsonRequestObject = new JSONObject(jsonRequestObject);
  }
}
