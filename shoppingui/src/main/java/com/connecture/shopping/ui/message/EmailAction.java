/**
 * 
 */
package com.connecture.shopping.ui.message;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingLead;
import com.connecture.shopping.process.integration.ShoppingIndividualIntegrationProcess;
import com.connecture.shopping.process.message.MessageBean;
import com.connecture.shopping.process.message.MessageProcess;
import com.connecture.shopping.process.message.MessageRecipient;
import com.connecture.shopping.process.message.MessageUtils;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 */
@SuppressWarnings("serial")
public class EmailAction extends ActionSupport implements ServletRequestAware
{
  private ShoppingIndividualIntegrationProcess shoppingIFPIntegrationProcess;
  private MessageProcess shoppingMessageProcess;
  private JSONObject jsonRequestObject;
  private JSONObject jsonObject;

  private HttpServletRequest servletRequest;

  private String transactionId;
  private String producerToken;
  private String context;

  /**
   * Method for e-mailing plan information to a user-specified e-mail address
   * Currently (5/15/13) only used for Employee shopping. Email for IFP /
   * Anonymous shopping are generated through InsureAdvantage as part of the
   * lead generation process.
   * 
   * @return String
   * @throws Exception
   */
  public String emailPlan() throws Exception
  {
    MessageBean messageBean = new MessageBean();

    String serverName = servletRequest.getServerName();

    JSONObject senderObject = jsonRequestObject.getJSONObject("sender");
    String senderFirstName = senderObject.getString("firstName");
    String senderLastName = senderObject.optString("lastName");
    String costViewCode = jsonRequestObject.optString("costViewCode");

    // build subject line
    String subject = this.buildSubject(serverName, senderFirstName, senderLastName);

    messageBean.setSubject(subject);
    messageBean.setRecipients(this.getRecipients(jsonRequestObject));

    JSONArray planDataJSONArray = jsonRequestObject.getJSONArray("productData");

    String emailContent = shoppingMessageProcess.buildEmailPlanContent(planDataJSONArray,
      costViewCode, buildNoteText(jsonRequestObject));

    messageBean.setContent(emailContent);

    boolean success = shoppingMessageProcess.sendEmail(messageBean);

    jsonObject = new JSONObject();
    jsonObject.put("result", success);

    return SUCCESS;
  }

  protected String buildSubject(String serverName, String senderFirstName, String senderLastName)
  {
    // build subject line
    return this.getText("shopping.sg.emailPlan.subject", new String[]{
        senderFirstName, senderLastName, serverName});
  }

  /**
   * Used for both Anonymous and IFP "email me". Triggers storing of email form
   * data into InsureAdvantage as a new lead, which in turn triggers
   * InsureAdvantage to send an email out to the specified recipient.
   * 
   * @return String
   * @throws Exception
   */
  public String generateLead() throws Exception
  {
    // build email content
    JSONArray planDataJSONArray = jsonRequestObject.getJSONArray("productData");

    Map<String, Double> planRates = extractPlanRatesFromJSON(planDataJSONArray);

    String email = jsonRequestObject.optString("email");
    String phone = jsonRequestObject.optString("phone");

    // TODO | phone field masking
    if (!StringUtils.isBlank(phone))
    {
      phone = phone.replaceAll("[\\W]", "");
    }
    String firstName = jsonRequestObject.optString("firstName");
    String lastName = jsonRequestObject.optString("lastName");
    Boolean callMe = jsonRequestObject.optBoolean("callMe");
    Boolean futureEmails = jsonRequestObject.optBoolean("futureEmails");

    // construct a lead
    ShoppingLead lead = MessageUtils.constructShoppingLead(email, callMe, futureEmails, firstName,
      lastName, phone);

    lead.setProducerToken(producerToken);
    lead.setLeadOriginationServerURL(deriveServerURL());
    lead.setPlanRates(planRates);
    boolean success = shoppingIFPIntegrationProcess.generateLead(lead, context, transactionId);
    jsonObject = new JSONObject();
    jsonObject.put("result", success);

    return SUCCESS;
  }

  private Map<String, Double> extractPlanRatesFromJSON(JSONArray productArrayJSON)
    throws JSONException
  {
    Map<String, Double> planRates = new HashMap<String, Double>();
    for (int i = 0; i < productArrayJSON.length(); i++)
    {
      JSONObject productJSON = productArrayJSON.getJSONObject(i);
      //String productId = productJSON.getString("id");
      String productId = productJSON.getString("planId");
      JSONObject productCostJSON = productJSON.getJSONObject("cost");
      JSONObject monthlyCostJSON = productCostJSON.getJSONObject("monthly");
      JSONObject monthlyRateJSON = monthlyCostJSON.getJSONObject("rate");
      Double monthlyRateAmount = monthlyRateJSON.getDouble("amount");
      planRates.put(productId, monthlyRateAmount);
    }
    return planRates;
  }

  public String deriveServerURL()
  {
    String scheme = servletRequest.getScheme();
    String serverName = servletRequest.getServerName();
    int port = servletRequest.getServerPort();
    String contextPath = servletRequest.getContextPath();
    return scheme + "://" + serverName + ":" + port + contextPath;
  }

  private static String buildNoteText(JSONObject jsonRequestObject)
  {
    // get user note
    String note = "";
    String rawNote = jsonRequestObject.optString("note");
    // convert rawNote to html version, with line breaks
    if (!StringUtils.isBlank(rawNote))
    {
      note = rawNote.replaceAll("(\r\n|\n\r|\r|\n)", "<br />");
    }

    return note;
  }

  private List<MessageRecipient> getRecipients(JSONObject requestObject) throws JSONException
  {
    List<MessageRecipient> recipients = new LinkedList<MessageRecipient>();

    JSONObject senderObject = requestObject.getJSONObject("sender");

    String senderEmail = senderObject.getString("email");

    if (requestObject.optBoolean("copyMe"))
    {
      MessageRecipient selfRecipient = new MessageRecipient();
      selfRecipient.setEmail(senderEmail);
      recipients.add(selfRecipient);
    }

    // handle multiple recipients
    String toEmail = requestObject.getString("toEmail");
    String[] recipientEmails = StringUtils.split(toEmail, ';');
    for (String recipientEmail : recipientEmails)
    {
      MessageRecipient recipient = new MessageRecipient();
      recipient.setEmail(recipientEmail);
      recipients.add(recipient);
    }

    return recipients;
  }

  /**
   * @param messageProcess the new value of messageProcess
   */
  public void setShoppingMessageProcess(MessageProcess shoppingMessageProcess)
  {
    this.shoppingMessageProcess = shoppingMessageProcess;
  }

  // /**
  // * @return the jsonRequestObject
  // */
  // public JSONObject getJsonRequestObject()
  // {
  // return jsonRequestObject;
  // }

  /**
   * @param jsonRequestObject the new value of jsonRequestObject
   */
  public void setJsonRequestObject(JSONObject jsonRequestObject)
  {
    this.jsonRequestObject = jsonRequestObject;
  }

  /**
   * @return the jsonObject
   */
  public JSONObject getJsonObject()
  {
    return jsonObject;
  }

  // /**
  // * @param jsonObject the new value of jsonObject
  // */
  // public void setJsonObject(JSONObject jsonObject)
  // {
  // this.jsonObject = jsonObject;
  // }

  /**
   * @param servletRequest the new value of servletRequest
   */
  public void setServletRequest(HttpServletRequest servletRequest)
  {
    this.servletRequest = servletRequest;
  }

  public void setShoppingIFPIntegrationProcess(
    ShoppingIndividualIntegrationProcess shoppingIFPIntegrationProcess)
  {
    this.shoppingIFPIntegrationProcess = shoppingIFPIntegrationProcess;
  }

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public String getProducerToken()
  {
    return producerToken;
  }

  public void setProducerToken(String producerToken)
  {
    this.producerToken = producerToken;
  }

  public String getContext()
  {
    return context;
  }

  public void setContext(String context)
  {
    this.context = context;
  }
}