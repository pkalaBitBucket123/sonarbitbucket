/**
 * 
 */
package com.connecture.shopping.ui;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;
import org.json.JSONException;
import org.json.JSONObject;

import com.opensymphony.xwork2.conversion.TypeConversionException;

/**
 * 
 */
public class JSONObjectTypeConverter extends StrutsTypeConverter
{

  /**
   * @see org.apache.struts2.util.StrutsTypeConverter#convertFromString(java.util.Map, java.lang.String[], java.lang.Class)
   */
  @SuppressWarnings("rawtypes")
  @Override
  public Object convertFromString(Map context, String[] values, Class toClass)
  {
    String pageData = values[0];   
    JSONObject jsonObject = this.parseJSONObject(pageData);   
    return jsonObject;
  }
  
  private JSONObject parseJSONObject(String jsonData) throws TypeConversionException
  {
    JSONObject jsonObject = null;
    try
    {
      jsonData = jsonData.replaceAll("&quot;", "\\\"");   
      jsonObject = new JSONObject(jsonData);
    }
    catch(JSONException exception)
    {
      throw new TypeConversionException("JSON Exception while converting from String to JSONObject: " + exception.getMessage());
    }
    return jsonObject;
  }

  /**
   * @see org.apache.struts2.util.StrutsTypeConverter#convertToString(java.util.Map, java.lang.Object)
   */
  @Override
  @SuppressWarnings("rawtypes") // StrutsTypeConverter
  public String convertToString(Map context, Object o)
  {
    // TODO Auto-generated method stub
    return null;
  }

}
