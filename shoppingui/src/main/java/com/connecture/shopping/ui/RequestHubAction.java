package com.connecture.shopping.ui;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.work.GetWorker;
import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.Work;
import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.context.ContextFactory;
import com.connecture.shopping.process.work.context.ContextWorkFactory;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class RequestHubAction extends ActionSupport
{
  private static Logger LOG = Logger.getLogger(RequestHubAction.class);

  // JSON response data (ties to "JSON" result)
  private JSONObject jsonObject;

  // JSON request data
  private JSONObject envelope;

  private String transactionId;

  private ContextFactory contextFactory;

  private Work accountWork;
  private Work applicationConfigWork;
  private Map<String, String> configParams;

  /**
   * TODO: key in the request might be useless now
   * 
   * @return
   * @throws Exception
   */
  public String handleRequests() throws Exception
  {
    // result object
    jsonObject = new JSONObject();
    jsonObject.put("requests", new JSONArray());

    // we only want to return the account if it was actually request.
    boolean accountRequested = false;
    boolean applicationConfigRequested = false;
    configParams = new HashMap<String, String>();

    // collection of requests from client; each request has a type and method
    // which will dictate what "type" of data to operate on, and the "method"
    // of
    // operation
    JSONArray requests = envelope.getJSONArray("requests");

    String context = envelope.getString("context");
    configParams.put("context", context);

    // a list of request handlers; one for each request. These will be
    // responsible for operating on a certain type of data (plans, members,
    // etc)
    // based on the "type" of the request.
    List<Work> workObjects = new LinkedList<Work>();

    // a separate list of request handlers that can run asynchronously in
    // threads; right now we are only doing this for "read method" requests.
    List<Work> asyncWorkObjects = new LinkedList<Work>();

    DataCache dataCache = new DataCache();
    
    ShoppingContext shoppingContext = ShoppingContext.fromContextName(context);

    ContextWorkFactory workFactory = contextFactory.getFactory(shoppingContext);

    // for each component request, create a "work" object.
    // determine if "work" object can be operated on asynchronously
    for (int i = 0; i < requests.length(); i++)
    {
      JSONObject componentRequestJSON = requests.getJSONObject(i);

      // key/unique identifier for the component
      String key = componentRequestJSON.getString("key");

      // type of data will we operate on
      String requestType = componentRequestJSON.getString("type");
      RequestType type = RequestType.ignoreCaseValueOf(requestType);

      // method of work to do on the type
      String methodStr = componentRequestJSON.getString("method");
      WorkConstants.Method method = WorkConstants.Method.ignoreCaseValueOf(methodStr);

      // input parameters
      JSONArray params = (JSONArray) componentRequestJSON.opt("params");

      // input data
      JSONObject data = (JSONObject) componentRequestJSON.opt("data");
      
      // validate up front?
      // give type, work, input, should be able to set on JSON object right
      // now
      Work work = null;
  
      // get "Work" object for type
      work = workFactory.getWork(transactionId, type, key, method, params, data, dataCache);

      if (RequestType.APPLICATION_CONFIG.equals(type))
      {
        applicationConfigWork = work;
        applicationConfigRequested = true;
      }
      else if (RequestType.ACCOUNT.equals(type))
      {
        accountWork = work;
        accountRequested = true;
      }
      else 
      {
        workObjects.add(work);
      }
    }

    // if nothing is requesting account work, well
    // we need to build it out anyway
    if (null == accountWork)
    {
      // Right now, we still need to create this work so that
      // we build out the account if it has not been created before
      // regardless if there is a request for it
      accountWork = workFactory.getAccountWork(transactionId);
    }

    // If we got a valid accountWork
    if (null != accountWork && accountWork.getErrors().isEmpty())
    {
      // Get or Create the initial AccountInfo
      accountWork.run();
    }

    for (Work work : workObjects)
    {
      if (work.getErrors().isEmpty())
      {
        if (work.isAsynchronous())
        {
          asyncWorkObjects.add(work);
        }
        else
        {
          work.run();
        }
      }
      else
      {
        LOG.error("Errors encountered while executing " + work.getKey() + ":\n" + work.getErrors());
      }
    }

    // run all asynchronous operations
    final CountDownLatch sync = new CountDownLatch(asyncWorkObjects.size());
    for (Work readWork : asyncWorkObjects)
    {
      new Thread(new GetWorker(sync, readWork), readWork.getKey()).start();
    }

    // Wait for all threads to complete
    if (!sync.await(60, TimeUnit.SECONDS))
    {
      LOG.error("Timed-out waiting for onLoad");
    }

    // If there was an accountWork specified
    if (null != accountWork)
    {
      // Run it again so we get the latest stored value
      // should this run again if it is an update?
      accountWork.run();

      // Add it into the workObjects so it gets returned in the response
      if (accountRequested)
      {
        workObjects.add(accountWork);
      }

    }

    // build the response objects
    for (Work workObject : workObjects)
    {
      if (workObject.getErrors().isEmpty())
      {
        this.addWorkResponse(workObject);
        if (applicationConfigRequested)
        {
          configParams.putAll(workObject.getConfigCriteria());
        }       
      }
    }

    // If there was an applicationConfigWork specified
    if (applicationConfigRequested && (null != applicationConfigWork))
    {
      applicationConfigWork.setConfigCriteria(configParams);
      
      applicationConfigWork.run();
      // Add it into the response
      this.addWorkResponse(applicationConfigWork);
    }

    return SUCCESS;
  }

  private void addWorkResponse(Work workObject) throws Exception
  {
    try
    {
      JSONArray responses = (JSONArray) jsonObject.get("requests");
      responses.put(buildResponse(workObject));
    }
    catch (Exception e)
    {
      LOG.error("Failed building response for work " + workObject.getKey(), e);
      throw e;
    }
  }

  private static JSONObject buildResponse(Work work) throws Exception
  {
    JSONObject response = new JSONObject();

    JSONObject dataResult = work.processData();
    JSONObject errorResult = work.processErrors();
    String key = work.getKey();

    response.put("key", key);
    response.put("data", dataResult);
    response.put("errors", errorResult);

    return response;
  }

  /**
   * @return the envelope
   */
  public JSONObject getEnvelope()
  {
    return envelope;
  }

  /**
   * @param envelope the new value of envelope
   */
  public void setEnvelope(JSONObject envelope)
  {
    this.envelope = envelope;
  }

  /**
   * @return the jsonObject
   */
  public JSONObject getJsonObject()
  {
    return jsonObject;
  }

  /**
   * @param jsonObject the new value of jsonObject
   */
  public void setJsonObject(JSONObject jsonObject)
  {
    this.jsonObject = jsonObject;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  /**
   * @param contextFactory
   */
  public void setContextFactory(ContextFactory contextFactory)
  {
    this.contextFactory = contextFactory;
  }

}