package com.connecture.shopping.ui.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.json.JSONObject;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Shopping result handler to handle requests which were trying to access secure
 * java types for a "not logged in" user.
 */
@SuppressWarnings("serial")
public class ShoppingResultHandlerAction extends ActionSupport
{
  static final String ORIGINAL_REQUEST_URI = "javax.servlet.forward.request_uri";
  static final String ORIGINAL_SERVLET_PATH = "javax.servlet.forward.servlet_path";
  static final String ORIGINAL_CONTEXT_PATH = "javax.servlet.forward.context_path";
  static final String ORIGINAL_PATH_INFO = "javax.servlet.forward.path_info";
  static final String ORIGINAL_QUERY_STRING = "javax.servlet.forward.query_string";

  private JSONObject jsonObject;

  private String processResponse() throws IOException, org.json.JSONException
  {
    final HttpServletRequest request = getHttpServletRequest();

    final HttpServletResponse response = getHttpServletResponse();

    String originalUri = (String) request.getAttribute(ORIGINAL_REQUEST_URI);
    String originalServletPath = (String) request.getAttribute(ORIGINAL_SERVLET_PATH);
    String originalContextPath = (String) request.getAttribute(ORIGINAL_CONTEXT_PATH);
    String originalPathInfo = (String) request.getAttribute(ORIGINAL_PATH_INFO);
    String originalQueryString = (String) request.getAttribute(ORIGINAL_QUERY_STRING);

    String originalPath = buildRequestUrl(originalServletPath, originalUri, originalContextPath,
      originalPathInfo, originalQueryString);

    if (StringUtils.isNotBlank(originalPath) && originalPath.toLowerCase().indexOf(".action") > -1)
    {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      jsonObject = new JSONObject();
      jsonObject.put("message", "User should be logged in to access the action");
      jsonObject.put("type", Action.LOGIN);
      jsonObject.put("resultCode", "login");
      return "ajaxLogin";
    }
    else
    {
      return Action.LOGIN;
    }
  }

  private String processUnauthorized() throws IOException, org.json.JSONException
  {
    final HttpServletRequest request = getHttpServletRequest();

    final HttpServletResponse response = getHttpServletResponse();

    String originalUri = (String) request.getAttribute(ORIGINAL_REQUEST_URI);
    String originalServletPath = (String) request.getAttribute(ORIGINAL_SERVLET_PATH);
    String originalContextPath = (String) request.getAttribute(ORIGINAL_CONTEXT_PATH);
    String originalPathInfo = (String) request.getAttribute(ORIGINAL_PATH_INFO);
    String originalQueryString = (String) request.getAttribute(ORIGINAL_QUERY_STRING);

    String originalPath = buildRequestUrl(originalServletPath, originalUri, originalContextPath,
      originalPathInfo, originalQueryString);

    if (StringUtils.isNotBlank(originalPath) && originalPath.toLowerCase().indexOf(".action") > -1)
    {
      response.setHeader("X-ResultCode", "UNAUTHORIZED");
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      jsonObject = new JSONObject();
      jsonObject.put("message", "User is not authorized for the action");
      jsonObject.put("type", "unauthorized");
      jsonObject.put("resultCode", "unauthorized");
      return "ajaxUnauthorized";
    }
    else
    {
      return "unauthorized";
    }
  }

  HttpServletResponse getHttpServletResponse()
  {
    return (HttpServletResponse) ActionContext.getContext().get(
      ServletActionContext.HTTP_RESPONSE);
  }

  HttpServletRequest getHttpServletRequest()
  {
    return (HttpServletRequest) ActionContext.getContext().get(
      ServletActionContext.HTTP_REQUEST);
  }

  public String login() throws Exception
  {
    String resultType = processResponse();
    return resultType;
  }

  public String unauthorized() throws Exception
  {
    String resultType = processUnauthorized();
    return resultType;
  }

  /**
   * copied form UrlUtils Obtains the web application-specific fragment of the
   * URL.
   */
  String buildRequestUrl(
    String servletPath,
    String requestURI,
    String contextPath,
    String pathInfo,
    String queryString)
  {

    StringBuilder url = new StringBuilder();

    if (servletPath != null)
    {
      url.append(servletPath);
      if (pathInfo != null)
      {
        url.append(pathInfo);
      }
    }
    else
    {
      if (contextPath != null)
      {
        url.append(requestURI.substring(contextPath.length()));
      }
    }
    
    if (url.length() > 0)
    {
      if (queryString != null)
      {
        url.append("?").append(queryString);
      }
    }

    return url.toString();
  }

  public JSONObject getJsonObject()
  {
    return jsonObject;
  }

  public void setJsonObject(JSONObject jsonObject)
  {
    this.jsonObject = jsonObject;
  }
}
