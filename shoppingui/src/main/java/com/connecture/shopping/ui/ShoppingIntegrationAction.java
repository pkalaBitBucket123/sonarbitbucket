/**
 * 
 */
package com.connecture.shopping.ui;

import com.connecture.shopping.process.ShoppingIntegrationProcess;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class ShoppingIntegrationAction extends ActionSupport
{
  private String accountId;
  private String groupId;
  private String transactionId;
  private String producerToken;
  
  protected static final String RESULT_UNAUTHORIZED = "unauthorized";

  private ShoppingIntegrationProcess integrationProcess;

  public String checkoutAnonymous() throws Exception
  {
    // nothing to do here; forward to an IA action
    return SUCCESS;
  }
  
  public String checkoutIFP() throws Exception
  {
    // nothing to do here; forward to an IA action
    return SUCCESS;
  }

  public String enroll() throws Exception
  {

    Long resultAccountId = integrationProcess.getAccountId(transactionId);
    if (resultAccountId != null)
    {
      accountId = Long.toString(resultAccountId);
    }
    else
    {
      LOG.error("Account data could not be found for the transactionId supplied to the enroll action : "
        + transactionId);
      return RESULT_UNAUTHORIZED;
    }

    return SUCCESS;
  }

  /**
   * @return the accountId
   */
  public String getAccountId()
  {
    return accountId;
  }

//  /**
//   * @param accountId the new value of accountId
//   */
//  public void setAccountId(String accountId)
//  {
//    this.accountId = accountId;
//  }

  /**
   * @return the transactionId
   */
  public String getTransactionId()
  {
    return transactionId;
  }

  /**
   * @param transactionId the new value of transactionId
   */
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public String getProducerToken()
  {
    return producerToken;
  }

  public void setProducerToken(String producerToken)
  {
    this.producerToken = producerToken;
  }

  /**
   * @param integrationProcess the new value of integrationProcess
   */
  public void setIntegrationProcess(ShoppingIntegrationProcess integrationProcess)
  {
    this.integrationProcess = integrationProcess;
  }

  /**
   * @return the groupId
   */
  public String getGroupId()
  {
    return groupId;
  }

  /**
   * @param groupId the groupId to set
   */
  public void setGroupId(String groupId)
  {
    this.groupId = groupId;
  }
}