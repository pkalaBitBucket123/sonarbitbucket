package com.connecture.shopping.ui;

import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsException;
import org.json.JSONException;
import org.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;

@SuppressWarnings("serial")
public class JSONResult implements Result
{
  private static final Logger LOG = Logger.getLogger(JSONResult.class);

  private String jsonObjectProperty = "jsonObject";
  private String contentType = "application/json";

  /**
   * Returns the property which will be used to lookup {@link JSONObject} in WebWork's ValueStack. Default to
   * 'jsonObject'.
   *
   * @return String
   */
  public String getJSONObjectProperty() {
      return jsonObjectProperty;
  }

  /**
   * Set the property which will be used to lookup {@link JSONObject} in WebWork's ValueStack. Default to
   * 'jsonObject'.
   * 
   * @param jsonObject
   */
  public void setJSONObjectProperty(String jsonObject) {
      this.jsonObjectProperty = jsonObject;
  }

  /**
   * Returns the content-type header to be used. Default to 'application/json'.
   * 
   * @return String
   */
  public String getContentType() {
      return contentType;
  }

  /**
   * Set the content-type header to be used. Default to 'application/json'.
   * 
   * @param contentType
   */
  public void setContentType(String contentType) {
      this.contentType = contentType;
  }


  /**
   * Writes the string representation of {@link JSONObject} defined through {@link #getJSONObjectProperty()}
   * to {@link javax.servlet.http.HttpServletResponse}'s outputstream. 
   *
   * @param invocation
   * @throws Exception
   */
  public void execute(ActionInvocation invocation) throws Exception {

      if (LOG.isDebugEnabled()) {
          LOG.debug("executing JSONResult");
      }

      JSONObject jsonObject = getJSONObject(invocation);
      if (jsonObject != null) {
          String json = jsonObject.toString();
          HttpServletResponse response = getServletResponse(invocation);
          response.setContentType(getContentType());
          response.setContentLength(json.getBytes().length);

          OutputStream os = response.getOutputStream();
          os.write(json.getBytes());
          os.flush();

          if (LOG.isDebugEnabled()) {
              LOG.debug("written ["+json+"] to HttpServletResponse outputstream");
          }
      }
  }

  /**
   * Attempt to look up a {@link org.json.JSONObject} instance through the property
   * ({@link #getJSONObjectProperty()}) by looking up the property in WebWork's ValueStack. It shall be found if there's
   * accessor method for the property in WebWork's action itself.
   * <p/>
   * Returns null if one cannot be found.
   * <p/>
   * We could override this method to return the desired JSONObject when writing testcases.
   *
   * @param invocation
   * @return {@link JSONObject} or null if one cannot be found
   */
  protected JSONObject getJSONObject(ActionInvocation invocation) throws JSONException {
      ActionContext actionContext = invocation.getInvocationContext();
      Object obj = actionContext.getValueStack().findValue(jsonObjectProperty);


      if (obj == null) {
          LOG.error("property ["+ jsonObjectProperty +"] returns null, expecting JSONObject", new StrutsException());
          return null;
      }
      if (! JSONObject.class.isInstance(obj)) {
          LOG.error("property ["+ jsonObjectProperty +"] is ["+obj+"] especting an instance of JSONObject", new StrutsException());
          return null;
      }
      return (JSONObject) obj;
  }


  /**
   * Returns a {@link javax.servlet.http.HttpServletResponse} by looking it up through WebWork's ActionContext Map.
   * </p>
   * We could override this method to return the desired Mock HttpServletResponse when writing testcases.
   * 
   * @param invocation
   * @return {@link javax.servlet.http.HttpServletResponse}
   */
  protected HttpServletResponse getServletResponse(ActionInvocation invocation) {
      return (HttpServletResponse) invocation.getInvocationContext().getContextMap().get(ServletActionContext.HTTP_RESPONSE);
  }
}
