package com.connecture.shopping.ui.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.opensymphony.xwork2.Action;

public class ShoppingResultHandlerActionTest
{
  @Test
  public void testBuildRequestUrl()
  {
    ShoppingResultHandlerAction action = new ShoppingResultHandlerAction();
    String result = action.buildRequestUrl(null, null, null, null, null);
    Assert.assertEquals("", result);
    
    result = action.buildRequestUrl(null, null, null, null, "age=10" );
    Assert.assertEquals("", result);
    
    result = action.buildRequestUrl("http://me/servlet/test", null, null, null, null );
    Assert.assertEquals("http://me/servlet/test", result);
    
    result = action.buildRequestUrl("http://me/servlet/test", null, null, "/buggle", "age=10" );
    Assert.assertEquals("http://me/servlet/test/buggle?age=10", result);
    
    result = action.buildRequestUrl(null, "http://me/servlet/test/buggle", "http://me/servlet", null, "age=10" );
    Assert.assertEquals("/test/buggle?age=10", result);
    
    // Weird combination of everything, servletPath wins
    result = action.buildRequestUrl("http://me/servlet/test", "http://me/servlet/test/buggle", "http://me/servlet", "/buggle", "age=10" );
    Assert.assertEquals("http://me/servlet/test/buggle?age=10", result);
  }
  
  @Test
  public void testLogin() throws Exception
  {
    ShoppingResultHandlerAction action = Mockito.spy(new ShoppingResultHandlerAction());
    
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_REQUEST_URI)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_SERVLET_PATH)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_CONTEXT_PATH)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_PATH_INFO)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_QUERY_STRING)).thenReturn(null);
    Mockito.doReturn(request).when(action).getHttpServletRequest();
    
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    Mockito.doReturn(response).when(action).getHttpServletResponse();
    
    String result = action.login();
    Assert.assertEquals(Action.LOGIN, result);
    
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_REQUEST_URI)).thenReturn("http://me/servlet/test/buggle");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_SERVLET_PATH)).thenReturn("http://me/servlet/test");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_CONTEXT_PATH)).thenReturn("http://me/servlet");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_PATH_INFO)).thenReturn("/buggle");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_QUERY_STRING)).thenReturn("age=10");
    
    result = action.login();
    Assert.assertEquals(Action.LOGIN, result);

    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_SERVLET_PATH)).thenReturn("http://me/servlet/test.action");
    result = action.login();
    Assert.assertEquals("ajaxLogin", result);
    
    JSONObject jsonObject = action.getJsonObject();
    Assert.assertEquals(Action.LOGIN, jsonObject.getString("type"));
  }
  
  @Test
  public void testUnauthorized() throws Exception
  {
    ShoppingResultHandlerAction action = Mockito.spy(new ShoppingResultHandlerAction());
    
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_REQUEST_URI)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_SERVLET_PATH)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_CONTEXT_PATH)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_PATH_INFO)).thenReturn(null);
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_QUERY_STRING)).thenReturn(null);
    Mockito.doReturn(request).when(action).getHttpServletRequest();
    
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    Mockito.doReturn(response).when(action).getHttpServletResponse();
    
    String result = action.unauthorized();
    Assert.assertEquals("unauthorized", result);
    
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_REQUEST_URI)).thenReturn("http://me/servlet/test/buggle");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_SERVLET_PATH)).thenReturn("http://me/servlet/test");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_CONTEXT_PATH)).thenReturn("http://me/servlet");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_PATH_INFO)).thenReturn("/buggle");
    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_QUERY_STRING)).thenReturn("age=10");
    
    result = action.unauthorized();
    Assert.assertEquals("unauthorized", result);

    Mockito.when(request.getAttribute(ShoppingResultHandlerAction.ORIGINAL_SERVLET_PATH)).thenReturn("http://me/servlet/test.action");
    result = action.unauthorized();
    Assert.assertEquals("ajaxUnauthorized", result);
    
    JSONObject jsonObject = action.getJsonObject();
    Assert.assertEquals("unauthorized", jsonObject.getString("type"));
  }
}
