package com.connecture.shopping.ui;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.ShoppingIntegrationProcess;
import com.opensymphony.xwork2.ActionSupport;

public class ShoppingIntegrationActionTest
{
  @Test
  public void testAnonymousCheckout() throws Exception
  {
    ShoppingIntegrationAction integrationAction = new ShoppingIntegrationAction();
    String testProducerToken = "producer123";
    integrationAction.setProducerToken(testProducerToken);
    String result = integrationAction.checkoutAnonymous();
    Assert.assertEquals(testProducerToken, integrationAction.getProducerToken());
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }
  
  @Test
  public void testIFPCheckout() throws Exception
  {
    ShoppingIntegrationAction integrationAction = new ShoppingIntegrationAction();
    String result = integrationAction.checkoutIFP();
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }
  
  @Test
  public void testEmployeeCheckoutSuccess() throws Exception
  {
    Long testAccountId = 100L;
    String testTransactionId = "1234";
    String testGroupId = "3333";
    
    ShoppingIntegrationAction integrationAction = new ShoppingIntegrationAction();
    ShoppingIntegrationProcess integrationProcess = Mockito.mock(ShoppingIntegrationProcess.class);
    Mockito.when(integrationProcess.getAccountId(Mockito.anyString())).thenReturn(testAccountId);
    integrationAction.setGroupId(testGroupId);
    integrationAction.setTransactionId(testTransactionId);
    integrationAction.setIntegrationProcess(integrationProcess);
    
    String result = integrationAction.enroll();
    
    Assert.assertEquals(Long.toString(testAccountId), integrationAction.getAccountId());
    Assert.assertEquals(testTransactionId, integrationAction.getTransactionId());
    Assert.assertEquals(testGroupId, integrationAction.getGroupId());
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }
  
  @Test
  public void testEmployeeCheckoutUnauthorized() throws Exception
  {
    ShoppingIntegrationAction integrationAction = new ShoppingIntegrationAction();
    ShoppingIntegrationProcess integrationProcess = Mockito.mock(ShoppingIntegrationProcess.class);
    Mockito.when(integrationProcess.getAccountId(Mockito.anyString())).thenReturn(null);
    integrationAction.setIntegrationProcess(integrationProcess);
    
    String result = integrationAction.enroll();
    
    Assert.assertEquals(ShoppingIntegrationAction.RESULT_UNAUTHORIZED, result);
  }
}
