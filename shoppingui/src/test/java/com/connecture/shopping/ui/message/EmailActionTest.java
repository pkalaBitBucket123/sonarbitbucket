package com.connecture.shopping.ui.message;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.data.ShoppingLead;
import com.connecture.shopping.process.integration.ShoppingIndividualIntegrationProcess;
import com.connecture.shopping.process.message.MessageProcess;
import com.opensymphony.xwork2.ActionSupport;

public class EmailActionTest
{
  @Test
  public void testEmailPlanNoOptional() throws Exception
  {
    this.testEmailPlanBase(false);
  }

  @Test
  public void testEmailPlanWithOptional() throws Exception
  {
    this.testEmailPlanBase(true);
  }

  private void testEmailPlanBase(boolean includeOptionalFields) throws Exception
  {
    HttpServletRequest servletRequest = this.getMockServletRequest();

    // mock the messageProcess
    MessageProcess messageProcess = Mockito.mock(MessageProcess.class);
    Mockito.when(
      messageProcess.buildEmailPlanContent(Mockito.any(JSONArray.class), Mockito.anyString(),
        Mockito.anyString())).thenReturn("This is the email content!");

    // mock the client JSON input
    JSONObject jsonRequestObject = this.getMockEmployeeEmailFormData(includeOptionalFields);

    // add in some optional fields
    if (includeOptionalFields)
    {
      jsonRequestObject.put("note", "this is a note");
    }

    // get productData
    String productDataJSON = FileUtils.readFileToString(FileUtils.toFile(this.getClass()
      .getResource("eeEmailProducts.json")));

    jsonRequestObject.put("productData", new JSONArray(productDataJSON));

    // need to get a little tricky
    EmailAction emailAction = Mockito.spy(new EmailAction());
    // EmailAction emailAction = Mockito.mock(EmailAction.class);

    Mockito.doReturn("mockSubjectTest!").when(emailAction)
      .buildSubject(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
    // Mockito.when(emailAction.buildSubject(Mockito.anyString(),
    // Mockito.anyString(), Mockito.anyString())).thenReturn("wubwubwub");

    emailAction.setShoppingMessageProcess(messageProcess);
    emailAction.setServletRequest(servletRequest);
    emailAction.setJsonRequestObject(jsonRequestObject);

    String result = emailAction.emailPlan();
    Assert.assertEquals(ActionSupport.SUCCESS, result);
    // TODO | more assertions
  }

  @Test
  public void testGenerateLeadNoOptional() throws Exception
  {
    this.testGenerateLeadBase(false);
  }

  @Test
  public void testGenerateLeadWithOptional() throws Exception
  {
    this.testGenerateLeadBase(true);
  }

  private void testGenerateLeadBase(boolean includeOptionalFields) throws Exception
  {
    // get servlet request
    HttpServletRequest servletRequest = this.getMockServletRequest();

    // mock the lead generation process
    ShoppingIndividualIntegrationProcess shoppingIFPIntegrationProcess = Mockito
      .mock(ShoppingIndividualIntegrationProcess.class);
    Mockito.when(
      shoppingIFPIntegrationProcess.generateLead(Mockito.any(ShoppingLead.class),
        Mockito.anyString(), Mockito.anyString())).thenReturn(true);

    // product data (rename from EE)
    String productDataJSON = FileUtils.readFileToString(FileUtils.toFile(this.getClass()
      .getResource("eeEmailProducts.json")));

    // get anonymous email form data
    JSONObject jsonRequestObject = new JSONObject();
    jsonRequestObject.put("productData", new JSONArray(productDataJSON));

    if (includeOptionalFields)
    {
      jsonRequestObject.put("phone", "(222) 333-4444");
    }

    String testTransactionId = "trans123";
    String testProducerToken = "producer123";
    String testContext = "individual";

    EmailAction emailAction = Mockito.spy(new EmailAction());
    emailAction.setTransactionId(testTransactionId);
    emailAction.setProducerToken(testProducerToken);
    emailAction.setServletRequest(servletRequest);
    emailAction.setJsonRequestObject(jsonRequestObject);
    emailAction.setShoppingIFPIntegrationProcess(shoppingIFPIntegrationProcess);
    emailAction.setContext(testContext);
    String result = emailAction.generateLead();

    Assert.assertEquals(testTransactionId, emailAction.getTransactionId());

    Assert.assertEquals(testProducerToken, emailAction.getProducerToken());

    Assert.assertEquals(testContext, emailAction.getContext());

    Assert.assertEquals(true, emailAction.getJsonObject().getBoolean("result"));

    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }

  private HttpServletRequest getMockServletRequest()
  {
    // mock the servletRequest
    HttpServletRequest servletRequest = Mockito.mock(HttpServletRequest.class);
    Mockito.when(servletRequest.getServerName()).thenReturn("www.testserver.com");
    Mockito.when(servletRequest.getScheme()).thenReturn("http://");
    Mockito.when(servletRequest.getServerPort()).thenReturn(8081);
    Mockito.when(servletRequest.getContextPath()).thenReturn("/someApplication");
    // TODO | maybe shouldn't require server port in there?!
    return servletRequest;
  }

  private JSONObject getMockEmployeeEmailFormData(boolean includeOptional) throws JSONException
  {
    // mock the client JSON input
    JSONObject jsonRequestObject = new JSONObject();
    JSONObject senderObject = new JSONObject();
    senderObject.put("firstName", "John");
    senderObject.put("lastName", "Doe");
    senderObject.put("email", "mctest@test.com");
    jsonRequestObject.put("sender", senderObject);
    jsonRequestObject.put("costViewCode", "monthly");
    jsonRequestObject.put("copyMe", includeOptional);
    jsonRequestObject.put("toEmail", "youremail@test.com");
    return jsonRequestObject;
  }

}
