package com.connecture.shopping.ui;

import org.junit.Assert;
import org.junit.Before;
import org.mockito.Matchers;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.common.account.PersistedAccountImpl;
import com.connecture.shopping.process.common.account.SessionAccountImpl;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.work.individual.IFPActorWork;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class EntryPointActionTest
{
  
  Account persistedAccountImpl;
  JSONObject persistedAccountJSON;
  private long effectiveMs;
  
  @Before
  public void setUp()
  {
    persistedAccountJSON = new JSONObject();
    persistedAccountImpl = Mockito.spy(new SessionAccountImpl());
    effectiveMs = (new Date()).getTime();
  }
  
  @Test
  public void testHandleRequestNoContext() throws Exception
  { 
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Account sessionAccountImpl = Mockito.mock(SessionAccountImpl.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(actor.isInternalUser()).thenReturn(false);   
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
       
    EntryPointAction entryPointAction = new EntryPointAction();
    entryPointAction.setIfpActorService(ifpActorService);
    entryPointAction.setSessionAccountImpl(sessionAccountImpl);
    entryPointAction.setContext(null);
    
    String result = entryPointAction.handleRequest();
    
    Assert.assertEquals(ShoppingContext.ANONYMOUS.getContextName(), entryPointAction.getContext());
    Assert.assertEquals(ShoppingContext.ANONYMOUS.getContextName(), result);
  }
  
  @Test
  public void testHandleRequestIFPRoute() throws Exception
  { 
    // NOTE : route should go away in favor of workFlow criteria selection
    
    String route = "/goToTest";
    
    EntryPointAction entryPointAction = new EntryPointAction();
    entryPointAction.setContext(ShoppingContext.INDIVIDUAL.getContextName());
    entryPointAction.setRoute(route);
    
    String result = entryPointAction.handleRequest();
    
    Assert.assertEquals(route, entryPointAction.getRoute());
    Assert.assertEquals(ShoppingContext.INDIVIDUAL.getContextName(), entryPointAction.getContext());
    Assert.assertEquals(ShoppingContext.INDIVIDUAL.getContextName(), result);
  }
  
  @Test
  public void testHandleRequestAnonymousLead() throws Exception
  { 
    String transactionId = "leadTransIdTest";
    
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Account sessionAccountImpl = Mockito.mock(SessionAccountImpl.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(actor.isInternalUser()).thenReturn(false);
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
    
    EntryPointAction entryPointAction = new EntryPointAction();
    entryPointAction.setIfpActorService(ifpActorService);
    entryPointAction.setSessionAccountImpl(sessionAccountImpl);
    entryPointAction.setContext(ShoppingContext.ANON_LEAD.getContextName());
    entryPointAction.setTransactionId(transactionId);
    Account persistedAccountImpl = Mockito.mock(PersistedAccountImpl.class);
    entryPointAction.setPersistedAccountImpl(persistedAccountImpl);
    
    String result = entryPointAction.handleRequest();   
    
    Assert.assertEquals(transactionId, entryPointAction.getTransactionId());
    Assert.assertEquals(ShoppingContext.ANON_LEAD.getContextName(), result);
  }
  
  @Test
  public void testHandleRequestAnonymousLeadProducer() throws Exception
  { 
    String transactionId = "leadTransIdTest";
    String producerToken = "producerTokenTest";
    
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Account sessionAccountImpl = Mockito.mock(SessionAccountImpl.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(actor.isInternalUser()).thenReturn(false);
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
    
    EntryPointAction entryPointAction = new EntryPointAction();
    entryPointAction.setIfpActorService(ifpActorService);
    entryPointAction.setSessionAccountImpl(sessionAccountImpl);
    entryPointAction.setContext(ShoppingContext.ANONYMOUS.getContextName());
    entryPointAction.setTransactionId(transactionId);
    entryPointAction.setProducerToken(producerToken);
    Account persistedAccountImpl = Mockito.mock(PersistedAccountImpl.class);
    entryPointAction.setPersistedAccountImpl(persistedAccountImpl);
    String result = entryPointAction.handleRequest(); 
    
    Assert.assertEquals(transactionId, entryPointAction.getTransactionId());
    Assert.assertEquals(producerToken, entryPointAction.getProducerToken());
       
    Assert.assertEquals(ShoppingContext.ANON_LEAD_PRODUCER.getContextName(), result);
  }
  
  /**
     * @throws Exception
     * Method under test: handleRequest()
     * Scenario: EntryPointAction has CoverageType & EffectiveMs set in   
     * Expectation: persistedAccountImpl has coverageType & EffectiveMs set in
     * with values
     */
  @Test
  public void testHandleRequest_CoverageTypeAndEffectiveMsPresent() throws Exception
  { 
    String transactionId = "IF_Lead_269";
    String coverageType = "DEN";
    setUpJSONObjects();
    
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Account sessionAccountImpl = Mockito.mock(SessionAccountImpl.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(actor.isInternalUser()).thenReturn(false);
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getAccount(Matchers.anyString());
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getOrCreateAccount(Matchers.anyString());
    
    EntryPointAction entryPointAction = new EntryPointAction();
    entryPointAction.setIfpActorService(ifpActorService);
    entryPointAction.setSessionAccountImpl(sessionAccountImpl);
    entryPointAction.setContext(ShoppingContext.ANON_LEAD.getContextName());
    entryPointAction.setTransactionId(transactionId);
    entryPointAction.setCoverageType(coverageType);
    entryPointAction.setPersistedAccountImpl(persistedAccountImpl);
    entryPointAction.setEffectiveMs(effectiveMs);
    
    String result = entryPointAction.handleRequest();   
    Assert.assertTrue( persistedAccountImpl.getAccount(transactionId).has("coverageType") );
    Assert.assertEquals("DEN", persistedAccountImpl.getAccount(transactionId).get("coverageType"));
    Assert.assertEquals(effectiveMs, persistedAccountImpl.getAccount(transactionId).get("effectiveMs"));
  }
  
  
  /**
     * @throws Exception
     * Method under test: handleRequest()
     * Scenario: EntryPointAction has CoverageType present but EffectiveMs missing(0)   
     * Expectation: persistedAccountImpl has coverageType set in with values but EffectiveMs missing
     */
  @Test
  public void testHandleRequest_CoverageTypeMissingAndEffectiveMsPresent() throws Exception
  { 
    String transactionId = "IF_Lead_269";
    String coverageType = "DEN";
    setUpJSONObjects();
    
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Account sessionAccountImpl = Mockito.mock(SessionAccountImpl.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(actor.isInternalUser()).thenReturn(false);
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getAccount(Matchers.anyString());
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getOrCreateAccount(Matchers.anyString());
    
    EntryPointAction entryPointAction = new EntryPointAction();
    entryPointAction.setIfpActorService(ifpActorService);
    entryPointAction.setSessionAccountImpl(sessionAccountImpl);
    entryPointAction.setContext(ShoppingContext.ANON_LEAD.getContextName());
    entryPointAction.setTransactionId(transactionId);
    entryPointAction.setCoverageType(coverageType);
    entryPointAction.setPersistedAccountImpl(persistedAccountImpl);
    
    String result = entryPointAction.handleRequest();   
    Assert.assertTrue( persistedAccountImpl.getAccount(transactionId).has("coverageType") );
    Assert.assertEquals("DEN", persistedAccountImpl.getAccount(transactionId).get("coverageType"));
    Assert.assertTrue(! persistedAccountImpl.getAccount(transactionId).has("effectiveMs"));

  }
  
  public void setUpJSONObjects() throws Exception{
    persistedAccountJSON.put("transactionId", "IF_Lead_269");
    
  }
}
