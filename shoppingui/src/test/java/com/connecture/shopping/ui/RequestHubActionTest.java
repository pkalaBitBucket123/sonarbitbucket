package com.connecture.shopping.ui;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.context.ContextFactory;
import com.connecture.shopping.process.work.context.EmployeeWorkFactory;
import com.connecture.shopping.process.work.core.AccountWork;
import com.connecture.shopping.process.work.core.ActorWork;
import com.opensymphony.xwork2.ActionSupport;

public class RequestHubActionTest
{
  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void testHandleRequests() throws Exception
  {
    String eeEnvelopeJSON = getEnvelopeEmployeeAccountAppConfig();

    EmployeeWorkFactory employeeWorkFactory = Mockito.mock(EmployeeWorkFactory.class);
    AccountWork accountWork = Mockito.mock(AccountWork.class);
    ActorWork actorWork = Mockito.mock(ActorWork.class);
    Mockito.when(employeeWorkFactory.getAccountWork(Mockito.anyString())).thenReturn(accountWork);
    Mockito.when(
      employeeWorkFactory.getWork(Mockito.anyString(), Mockito.any(RequestType.class),
        Mockito.anyString(), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONArray.class),
        Mockito.any(JSONObject.class), Mockito.any(DataCache.class))).thenReturn(actorWork);

    ContextFactory contextFactory = Mockito.mock(ContextFactory.class);
    Mockito.when(contextFactory.getFactory(Mockito.any(ShoppingContext.class))).thenReturn(
      employeeWorkFactory);

    String result = handleRequests(eeEnvelopeJSON, contextFactory);
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }

  @Test
  public void testHandleRequestsNotAccount() throws Exception
  {
    String eeEnvelopeJSON = getEnvelopeEmployeeNoAccountNoAppConfig();

    EmployeeWorkFactory employeeWorkFactory = Mockito.mock(EmployeeWorkFactory.class);
    AccountWork accountWork = Mockito.mock(AccountWork.class);
    ActorWork actorWork = Mockito.mock(ActorWork.class);
    Mockito.when(employeeWorkFactory.getAccountWork(Mockito.anyString())).thenReturn(accountWork);
    Mockito.when(
      employeeWorkFactory.getWork(Mockito.anyString(), Mockito.any(RequestType.class),
        Mockito.anyString(), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONArray.class),
        Mockito.any(JSONObject.class), Mockito.any(DataCache.class))).thenReturn(actorWork);

    ContextFactory contextFactory = Mockito.mock(ContextFactory.class);
    Mockito.when(contextFactory.getFactory(Mockito.any(ShoppingContext.class))).thenReturn(
      employeeWorkFactory);

    String result = handleRequests(eeEnvelopeJSON, contextFactory);
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }
  
//  @Test
//  public void testHandleRequestsAsync() throws Exception
//  {
//    String eeEnvelopeJSON = this.getEnvelopeEmployeeAsyncData();
//
//    EmployeeWorkFactory employeeWorkFactory = Mockito.mock(EmployeeWorkFactory.class);
//    AccountWork accountWork = Mockito.mock(AccountWork.class);
//    ActorWork actorWork1 = Mockito.mock(ActorWork.class);
//    ActorWork actorWork2 = Mockito.mock(ActorWork.class);
//   
//    Mockito.when(employeeWorkFactory.getAccountWork(Mockito.anyString())).thenReturn(accountWork);
//    
//    Mockito.when(
//      employeeWorkFactory.getWork(Mockito.anyString(), Mockito.any(RequestType.class),
//        Mockito.eq("get:master:[]"), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONArray.class),
//        Mockito.any(JSONObject.class), Mockito.any(DataCache.class))).thenReturn(actorWork1);
//    
//    Mockito.when(actorWork1.isAsynchronous()).thenReturn(true);
//    Mockito.when(actorWork1.getKey()).thenReturn("master");
//    
//    Mockito.when(actorWork2.isAsynchronous()).thenReturn(true);
//    Mockito.when(actorWork1.getKey()).thenReturn("blaster");
//    
//    Mockito.when(
//      employeeWorkFactory.getWork(Mockito.anyString(), Mockito.any(RequestType.class),
//        Mockito.eq("get:blaster:[]"), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONArray.class),
//        Mockito.any(JSONObject.class), Mockito.any(DataCache.class))).thenReturn(actorWork2);
//    
//    
//    
//    ContextFactory contextFactory = Mockito.mock(ContextFactory.class);
//    
//    Mockito.when(contextFactory.getFactory(Mockito.any(ShoppingContext.class))).thenReturn(
//      employeeWorkFactory);
//
//    String result = handleRequests(eeEnvelopeJSON, contextFactory);
//    Assert.assertEquals(ActionSupport.SUCCESS, result);
//  }

  @Test
  public void testHandleRequestsWorkErrors() throws Exception
  {
    String eeEnvelopeJSON = getEnvelopeEmployeeNoAccountNoAppConfig();

    EmployeeWorkFactory employeeWorkFactory = Mockito.mock(EmployeeWorkFactory.class);
    AccountWork accountWork = Mockito.mock(AccountWork.class);
    ActorWork actorWork = Mockito.mock(ActorWork.class);

    Map<String, String> errorMap = new HashMap<String, String>();
    errorMap.put("TestError", "This is only a test. You're so lucky!");

    Mockito.when(actorWork.getErrors()).thenReturn(errorMap);

    Mockito.when(employeeWorkFactory.getAccountWork(Mockito.anyString())).thenReturn(accountWork);
    Mockito.when(
      employeeWorkFactory.getWork(Mockito.anyString(), Mockito.any(RequestType.class),
        Mockito.anyString(), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONArray.class),
        Mockito.any(JSONObject.class), Mockito.any(DataCache.class))).thenReturn(actorWork);

    ContextFactory contextFactory = Mockito.mock(ContextFactory.class);
    Mockito.when(contextFactory.getFactory(Mockito.any(ShoppingContext.class))).thenReturn(
      employeeWorkFactory);

    String result = handleRequests(eeEnvelopeJSON, contextFactory);
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }
  
  @Test
  public void testHandleRequestsAccountWorkErrors() throws Exception
  {
    String eeEnvelopeJSON = getEnvelopeEmployeeAccountAppConfig();

    EmployeeWorkFactory employeeWorkFactory = Mockito.mock(EmployeeWorkFactory.class);
    AccountWork accountWork = Mockito.mock(AccountWork.class);
    ActorWork actorWork = Mockito.mock(ActorWork.class);

    Map<String, String> errorMap = new HashMap<String, String>();
    errorMap.put("TestError", "This is only a test. You're so lucky!");

    Mockito.when(accountWork.getErrors()).thenReturn(errorMap);

    Mockito.when(employeeWorkFactory.getAccountWork(Mockito.anyString())).thenReturn(accountWork);
    Mockito.when(
      employeeWorkFactory.getWork(Mockito.anyString(), Mockito.any(RequestType.class),
        Mockito.anyString(), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONArray.class),
        Mockito.any(JSONObject.class), Mockito.any(DataCache.class))).thenReturn(actorWork);

    ContextFactory contextFactory = Mockito.mock(ContextFactory.class);
    Mockito.when(contextFactory.getFactory(Mockito.any(ShoppingContext.class))).thenReturn(
      employeeWorkFactory);

    String result = handleRequests(eeEnvelopeJSON, contextFactory);
    Assert.assertEquals(ActionSupport.SUCCESS, result);
  }

  private String handleRequests(String envelopeJSONStr, ContextFactory contextFactory)
    throws Exception
  {

    RequestHubAction requestHubAction = new RequestHubAction();
    requestHubAction.setEnvelope(new JSONObject(envelopeJSONStr));
    requestHubAction.setContextFactory(contextFactory);

    return requestHubAction.handleRequests();
  }

  private String createSimpleMockRequest(String context, String method, String type)
  {
    StringBuilder requestBuilder = new StringBuilder();
    requestBuilder.append("{");
    requestBuilder.append("\"method\":\"" + method + "\",");
    requestBuilder.append("\"type\":\"" + type + "\",");
    requestBuilder.append("\"key\":\"" + method + ":" + type + ":[]\",");
    requestBuilder.append("\"params\":[],");
    requestBuilder.append("\"context\":\"" + context + "\"");
    requestBuilder.append("}");
    return requestBuilder.toString();
  }

  private String getEnvelopeEmployeeNoAccountNoAppConfig()
  {
    StringBuilder requestBuilder = new StringBuilder();
    requestBuilder.append("{");
    requestBuilder.append("\"requests\":[");

    requestBuilder.append(createSimpleMockRequest("employee","get","actor"));
    
    requestBuilder.append("],");
    requestBuilder.append("\"context\":\"employee\"");
    requestBuilder.append("}");

    return requestBuilder.toString();
  }
  
  private String getEnvelopeEmployeeAccountAppConfig()
  {
    StringBuilder requestBuilder = new StringBuilder();
    requestBuilder.append("{");
    requestBuilder.append("\"requests\":[");

    requestBuilder.append(createSimpleMockRequest("employee","get","account"));
    requestBuilder.append(",");
    requestBuilder.append(createSimpleMockRequest("employee","get","applicationConfig"));
    
    requestBuilder.append("],");
    requestBuilder.append("\"context\":\"employee\"");
    requestBuilder.append("}");

    return requestBuilder.toString();
  }
  
//  private String getEnvelopeEmployeeAsyncData()
//  {
//    StringBuilder requestBuilder = new StringBuilder();
//    requestBuilder.append("{");
//    requestBuilder.append("\"requests\":[");
//
//    requestBuilder.append(createSimpleMockRequest("employee","get","master"));
//    requestBuilder.append(",");
//    requestBuilder.append(createSimpleMockRequest("employee","get","blaster"));
//    
//    requestBuilder.append("],");
//    requestBuilder.append("\"context\":\"employee\"");
//    requestBuilder.append("}");
//
//    return requestBuilder.toString();
//  }
}
