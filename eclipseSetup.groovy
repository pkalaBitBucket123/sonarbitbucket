import groovy.xml.MarkupBuilder;

def ant = new AntBuilder()

ant.sequential {
    echo("Deleting lib directory")
    delete(dir:'lib')
    echo("Downloading all depenencies to lib directory")
	exec(executable:'mvn.bat') {
		arg(value:'-N')
		arg(value:'dependency:copy-dependencies')
		arg(value:'-DoutputDirectory=lib')
	}
}

def writer = new StringWriter()
def xml = new MarkupBuilder(writer)
xml.setDoubleQuotes(true);
xml.classpath() {
  classpathentry(kind:'src', path:'shoppingprocess/src/main/java')
  classpathentry(kind:'src', path:'shoppingprocess/src/main/resources')
  classpathentry(kind:'src', path:'shoppingprocess/src/test/java')
  classpathentry(kind:'src', path:'shoppingprocess/src/test/resources')
  classpathentry(kind:'src', path:'shoppingui/src/main/java')
  classpathentry(kind:'src', path:'shoppingui/src/main/resources')
  classpathentry(kind:'src', path:'shoppingui/src/test/java')
  classpathentry(kind:'src', path:'shoppingui/src/test/resources')
  classpathentry(kind:'src', path:'shoppingwar/src/main/resources')
  classpathentry(kind:'src', path:'shoppingwar/src/main/webapp')
  classpathentry(kind:'src', path:'shoppingwar/src/test/js')
  classpathentry(kind:'con', path:'org.eclipse.jdt.launching.JRE_CONTAINER')
  new File('lib').eachFile({ f ->
    classpathentry(kind:'lib', path:'lib/' + f.name)
  });
  classpathentry(kind:'output', path:'bin')
}

ant.echo('Generating Eclipse .classpath file');
def configFile = new File('.classpath');
def configFileWriter = configFile.newWriter();
configFileWriter << '<?xml version="1.0" encoding="UTF-8"?>\n' + writer;
configFileWriter.flush();
configFileWriter.close();
ant.echo('Setup was Successful');
