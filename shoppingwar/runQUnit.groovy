import groovy.xml.MarkupBuilder;

def ant = new AntBuilder();
def jsTestDir = 'src/test/js/';

def debug = false;
try {
	if (args.length > 0 && args[0].equals('debug')) {
	  debug = true;
	}
} catch (MissingPropertyException e) {
  // ignore missing args variable
}

def directoryPrefix = '';
if (!new File('.').canonicalPath.contains('shoppingwar')) {
  directoryPrefix = 'shoppingwar/';
  if (!new File(directoryPrefix + jsTestDir).exists()) {
    println 'Javascript unit test directory does not exist';
    System.exit(1);
  }
}

// make sure directories exist
ant.mkdir(dir:directoryPrefix + 'target/surefire-reports/')

ant.echo("Running QUnit Tests")
File indexFile = new File(directoryPrefix + 'src/test/index.html');
def outputFilePath = directoryPrefix + 'target/surefire-reports/TEST-qunit.xml';
def outputFile = new File(outputFilePath);
println "Using index file: ${indexFile.toURL()}";
println "Using outputfile file: ${outputFile.canonicalPath}";
ant.exec(executable:'phantomjs', dir:directoryPrefix, output:outputFilePath, errorproperty:'quniterror', failonerror:false, timeout:60000) {
	arg(value:'src/test/qunit/qunit-runner.js')
	arg(value:indexFile.toURL())
}

def outputText = outputFile.text;
if (debug) {
	ant.echo outputText
}

if (outputText.contains('<failure')) {
  ant.echo("[QUnit] -> FAILED")
} else if (!outputText.contains('<testsuite')) {
  ant.echo("[QUnit] -> CRITICAL FAILURE")
} else {
  ant.echo("[QUnit] -> SUCCESS")
}
