/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'sinon', 'mockjax', 'components/account/component.account', 'components/account/models/model.eligibility', 'common/backbone/backbone.sync' ], function(require, Backbone, Sinon, Mockjax, AccountComponent, EligibilityModel, BackboneSync) {
	var AccountComponentModel = Backbone.Model.extend({
	});
	// var AccountModel = Backbone.Model.extend({});
	var SessionModel = Backbone.Model.extend({
	});
	
	module('component.account', {
		setup : function() {
			self = this;
			
			this.isCostEstimatorIncluded = false;
			this.isCostEstimatorCoverageSelected = false;
			this.updateMemberEligibilityAndRender = false;
			
			// TODO Try this with a sandbox
			Sinon.stub(AccountComponent.AccountComponentContentView.prototype, "setWorkflowStep");
			Sinon.stub(AccountComponent.AccountComponentContentView.prototype, "isCostEstimatorIncluded", function() {
				return self.isCostEstimatorIncluded;
			});
			Sinon.stub(AccountComponent.AccountComponentContentView.prototype, "continueToMethods");
			Sinon.stub(AccountComponent.AccountComponentContentView.prototype, "notifyMembersUpdated");
			Sinon.stub(AccountComponent.AccountComponentContentView.prototype, "updateCostEstimationData");
			Sinon.stub(AccountComponent.AccountComponentContentView.prototype, "isCostEstimatorCoverageSelected", function() {
				return self.isCostEstimatorCoverageSelected;
			});
			
			this.view = new AccountComponent.AccountComponentContentView({
				model : new AccountComponentModel({
					"eligibilityModel" : new EligibilityModel({})
				}),
				session : new SessionModel({
					
				}),
				eventManager : {
					on : function() {
						
					}
				}
			});
			
			this.view.currentView = {
				updateMemberEligibilityAndRender : function(){
					return self.updateMemberEligibilityAndRender;
				}
			};

			$.mockjax({
				  url: 'http://localhost:8080/shopping/shopping/handleRequests.action',
  			      responseText : '{"requests":[{"key":"get:eligibility:[]","data":{"eligibility":{"memberProductLines":[{"memberKey":"0","productLineEligibility":{"Vision":true,"Medical":true,"Dental":true}},{"memberKey":"1","productLineEligibility":{"Vision":true,"Medical":true,"Dental":true}},{"memberKey":"2","productLineEligibility":{"Vision":false,"Medical":true,"Dental":true}}]}},"errors":{}}]}'
				});
		},
		teardown : function() {
			$.mockjaxClear();
			
			AccountComponent.AccountComponentContentView.prototype.setWorkflowStep.restore();
			AccountComponent.AccountComponentContentView.prototype.isCostEstimatorIncluded.restore();
			AccountComponent.AccountComponentContentView.prototype.continueToMethods.restore();
			AccountComponent.AccountComponentContentView.prototype.notifyMembersUpdated.restore();
			AccountComponent.AccountComponentContentView.prototype.updateCostEstimationData.restore();
			AccountComponent.AccountComponentContentView.prototype.isCostEstimatorCoverageSelected.restore();
		}
	});
	
	test('validateEligibilityAndContinue - invalid', function() {
		QUnit.stop();
		
		this.view.validateEligibilityAndContinue(function() {
			equal(AccountComponent.AccountComponentContentView.prototype.notifyMembersUpdated.callCount, 0);
			equal(AccountComponent.AccountComponentContentView.prototype.continueToMethods.callCount, 0);
			equal(AccountComponent.AccountComponentContentView.prototype.updateCostEstimationData.callCount, 0);
			
			QUnit.start();
		});
	});
});