/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'components/account/widgets/views/view.widget.demographics' ], function(require, Backbone, WhosCoveredView) {
	var WhosCoveredModel = Backbone.Model.extend({});
	var AccountModel = Backbone.Model.extend({});
	
	module('view.product.summary', {
		setup : function() {
			this.view = new WhosCoveredView({
				model : new WhosCoveredModel({
					account : new AccountModel()
				})
			});
		},
		teardown : function() {
		}
	});

	test('validateDate', function() {
		var memberValidationErrors = {};
		ok(this.view.FieldValidator.validateDate('name', '10/10/2003', { notSpecified : "not specified", invalid : "invalid" }, memberValidationErrors ));
		deepEqual(memberValidationErrors, {});
	});

	test('validateDate : Invalid Date', function() {
		var memberValidationErrors = {};
		ok(!this.view.FieldValidator.validateDate('name', 'ABC', { notSpecified : "not specified", invalid : "invalid" }, memberValidationErrors ));
		deepEqual(memberValidationErrors, { 'name' : { "text" : "invalid" }});
	});

	test('validateDate : Not Specified', function() {
		var memberValidationErrors = {};
		ok(!this.view.FieldValidator.validateDate('name', '', { notSpecified : "not specified", invalid : "invalid" }, memberValidationErrors ));
		deepEqual(memberValidationErrors, { 'name' : { "text" :  "not specified" } });
	});
});