/** License: MIT License Url: http://www.opensource.org/licenses/mit-license.php
*
* Dual licensed under the MIT and GPL licenses: http://www.opensource.org/licenses/mit-license.php http://www.gnu.org/licenses/gpl.html
* Print Element Plugin 1.2
*
* Copyright (c) 2010 Erik Zaadi
*
* Inspired by PrintArea (http://plugins.jquery.com/project/PrintArea) and
* http://stackoverflow.com/questions/472951/how-do-i-print-an-iframe-from-javascript-in-safari-chrome
*
*  Home Page : http://projects.erikzaadi/jQueryPlugins/jQuery.printElement
*  Issues (bug reporting) : http://github.com/erikzaadi/jQueryPlugins/issues/labels/printElement
*  jQuery plugin page : http://plugins.jquery.com/project/printElement
*
*  Thanks to David B (http://github.com/ungenio) and icgJohn (http://www.blogger.com/profile/11881116857076484100)
*  For their great contributions!
*
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*
*   Note, Iframe Printing is not supported in Opera and Chrome 3.0, a popup window will be shown instead
*/

(function(e,t){function i(t,i){var o=a(t,i),u=null,f=null;if(i["printMode"].toLowerCase()=="popup")u=e.open("about:blank","printElementWindow","width=650,height=440,scrollbars=yes"),f=u.document;else{var l="printElement_"+Math.round(Math.random()*99999).toString(),c=n.createElement("IFRAME");r(c).attr({style:i.iframeElementOptions.styleToAdd,id:l,className:i.iframeElementOptions.classNameToAdd,frameBorder:0,scrolling:"no",src:"about:blank"}),n.body.appendChild(c),f=c.contentWindow||c.contentDocument,f.document&&(f=f.document),c=n.frames?n.frames[l]:n.getElementById(l),u=c.contentWindow||c}focus(),f.open(),f.write(o),f.close(),s(u)}function s(e){e&&e.printPage?e.printPage():setTimeout(function(){s(e)},50)}function o(e){var t=r(e);r(":checked",t).each(function(){this.setAttribute("checked","checked")}),r("input[type='text']",t).each(function(){this.setAttribute("value",r(this).val())}),r("select",t).each(function(){var e=r(this);r("option",e).each(function(){e.val()==r(this).val()&&this.setAttribute("selected","selected")})}),r("textarea",t).each(function(){var e=r(this).attr("value");r.browser.mozilla&&this.firstChild?this.firstChild.textContent=e:this.innerHTML=e});var n=r("<div></div>").append(t.clone()).html();return n}function u(){var t=e.location.port?":"+e.location.port:"";return e.location.protocol+"//"+e.location.hostname+t+e.location.pathname}function a(e,t){var i=r(e),s=o(e),a=new Array;a.push('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'),a.push("<html><head><title>"+t.pageTitle+"</title>");if(t.overrideElementCSS){if(t.overrideElementCSS.length>0)for(var f=0;f<t.overrideElementCSS.length;f++){var l=t.overrideElementCSS[f];typeof l=="string"?a.push('<link type="text/css" rel="stylesheet" href="'+l+'" >'):a.push('<link type="text/css" rel="stylesheet" href="'+l.href+'" media="'+l.media+'" >')}}else r("link",n).filter(function(){return r(this).attr("rel").toLowerCase()=="stylesheet"}).each(function(){a.push('<link type="text/css" rel="stylesheet" href="'+r(this).attr("href")+'" media="'+r(this).attr("media")+'" >')});return t.overrideElementLess&&t.overrideElementLess.length>0&&a.push('<link rel="stylesheet/less" type="text/css" media="all" href="'+t.overrideElementLess+'">'),t.overrideElementScript&&t.overrideElementScript.length>0&&a.push('<script type="text/javascript" src="'+t.overrideElementScript+'"></script>'),a.push('<base href="'+u()+'" />'),a.push('</head><body style="'+t.printBodyOptions.styleToAdd+'" class="'+t.printBodyOptions.classNameToAdd+'">'),a.push('<div class="'+i.attr("class")+'">'+s+"</div>"),a.push('<script type="text/javascript">function printPage(){focus();print();'+(!t.leaveOpen&&t["printMode"].toLowerCase()=="popup"?"close();":"")+"}</script>"),a.push("</body></html>"),a.join("")}var n=e.document,r=e.jQuery;r.fn.printElement=function(e){var t=r.extend({},r.fn.printElement.defaults,e);return t["printMode"]=="iframe"&&/chrome/.test(navigator.userAgent.toLowerCase())&&(t.printMode="popup"),r("[id^='printElement_']").remove(),this.each(function(){var e=r.meta?r.extend({},t,r(this).data()):t;i(r(this),e)})},r.fn.printElement.defaults={printMode:"iframe",pageTitle:"",overrideElementCSS:null,printBodyOptions:{styleToAdd:"padding:10px;margin:10px;",classNameToAdd:""},leaveOpen:!1,iframeElementOptions:{styleToAdd:"border:none;position:absolute;width:0px;height:0px;bottom:0px;left:0px;",classNameToAdd:""}},r.fn.printElement.cssElement={href:"",media:""}})(window)