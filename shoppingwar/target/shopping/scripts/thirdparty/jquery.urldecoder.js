/** License: MIT  License Url: http://www.opensource.org/licenses/mit-license.php
 * --------------------------------------------------------------------------
 *	jQuery URL Decoder
 *	Version 1.0
 *	Parses URL and return its components. Can also build URL from components
 *	
 * ---------------------------------------------------------------------------
 *	HOW TO USE:
 *
 *	$.url.decode('http://username:password@hostname/path?arg1=value%40+1&arg2=touch%C3%A9#anchor')
 *	// returns
 *	// http://username:password@hostname/path?arg1=value@ 1&arg2=touché#anchor
 *	// Note: "%40" is replaced with "@", "+" is replaced with " " and "%C3%A9" is replaced with "é"
 *	
 *	$.url.encode('file.htm?arg1=value1 @#456&amp;arg2=value2 touché')
 *	// returns
 *	// file.htm%3Farg1%3Dvalue1%20%40%23456%26arg2%3Dvalue2%20touch%C3%A9
 *	// Note: "@" is replaced with "%40" and "é" is replaced with "%C3%A9"
 *	
 *	$.url.parse('http://username:password@hostname/path?arg1=value%40+1&arg2=touch%C3%A9#anchor')
 *	// returns
 *	{
 *		source: 'http://username:password@hostname/path?arg1=value%40+1&arg2=touch%C3%A9#anchor',
 *		protocol: 'http',
 *		authority: 'username:password@hostname',
 *		userInfo: 'username:password',
 *		user: 'username',
 *		password: 'password',
 *		host: 'hostname',
 *		port: '',
 *		path: '/path',
 *		directory: '/path',
 *		file: '',
 *		relative: '/path?arg1=value%40+1&arg2=touch%C3%A9#anchor',
 *		query: 'arg1=value%40+1&arg2=touch%C3%A9',
 *		anchor: 'anchor',
 *		params: {
 *			'arg1': 'value@ 1',
 *			'arg2': 'touché'
 *		}
 *	}
 *	
 *	$.url.build({
 *		protocol: 'http',
 *		username: 'username',
 *		password: 'password',
 *		host: 'hostname',
 *		path: '/path',
 *		query: 'arg1=value%40+1&arg2=touch%C3%A9',
 *		// or 
 *		//params: {
 *		//	'arg1': 'value@ 1',
 *		//	'arg2': 'touché'
 *		//}
 *		anchor: 'anchor',
 *	})
 *	// returns
 *	// http://username:password@hostname/path?arg1=value%40+1&arg2=touch%C3%A9#anchor	
 *	
 * ---------------------------------------------------------------------------
 * OTHER PARTIES' CODE:
 *
 * Parser based on the Regex-based URI parser by Steven Levithan.
 * For more information visit http://blog.stevenlevithan.com/archives/parseuri
 *
 * Deparam taken from jQuery BBQ by Ben Alman. Dual licensed under the MIT and GPL licenses (http://benalman.com/about/license/)
 * http://benalman.com/projects/jquery-bbq-plugin/
 *  
 * ---------------------------------------------------------------------------
	
*/

jQuery.url=function(){function e(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);r<128?t+=String.fromCharCode(r):r>127&&r<2048?(t+=String.fromCharCode(r>>6|192),t+=String.fromCharCode(r&63|128)):(t+=String.fromCharCode(r>>12|224),t+=String.fromCharCode(r>>6&63|128),t+=String.fromCharCode(r&63|128))}return t}function t(e){var t="",n=0,r=0,i=0;while(n<e.length)r=e.charCodeAt(n),r<128?(t+=String.fromCharCode(r),n++):r>191&&r<224?(i=e.charCodeAt(n+1),t+=String.fromCharCode((r&31)<<6|i&63),n+=2):(i=e.charCodeAt(n+1),c3=e.charCodeAt(n+2),t+=String.fromCharCode((r&15)<<12|(i&63)<<6|c3&63),n+=3);return t}function n(e,t){var n={},r={"true":!0,"false":!1,"null":null};return $.each(e.replace(/\+/g," ").split("&"),function(e,i){var s=i.split("="),u=o(s[0]),a,f=n,l=0,c=u.split("]["),h=c.length-1;/\[/.test(c[0])&&/\]$/.test(c[h])?(c[h]=c[h].replace(/\]$/,""),c=c.shift().split("[").concat(c),h=c.length-1):h=0;if(s.length===2){a=o(s[1]),t&&(a=a&&!isNaN(a)?+a:a==="undefined"?undefined:r[a]!==undefined?r[a]:a);if(h)for(;l<=h;l++)u=c[l]===""?f.length:c[l],f=f[u]=l<h?f[u]||(c[l+1]&&isNaN(c[l+1])?{}:[]):a;else $.isArray(n[u])?n[u].push(a):n[u]!==undefined?n[u]=[n[u],a]:n[u]=a}else u&&(n[u]=t?undefined:"")}),n}function r(e){e=e||window.location;var t=/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/,r=["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],i=t.exec(e),s={},o=r.length;while(o--)s[r[o]]=i[o]||"";if(s.directory){var u=s.directory;u.charAt(0)=="/"&&(u=u.substring(1)),u.charAt(u.length-1)=="/"&&(u=u.substring(0,u.length-1)),s.segments=u.split("/")}return s.query&&(s.params=n(s.query,!0)),s}function i(e){if(e.source)return encodeURI(e.source);var t=[];return e.protocol&&(e.protocol=="file"?t.push("file:///"):e.protocol=="mailto"?t.push("mailto:"):t.push(e.protocol+"://")),e.authority?t.push(e.authority):(e.userInfo?t.push(e.userInfo+"@"):e.user&&(t.push(e.user),e.password&&t.push(":"+e.password),t.push("@")),e.host&&(t.push(e.host),e.port&&t.push(":"+e.port))),e.path?t.push(e.path):(e.directory?t.push(e.directory):e.segments&&(t.push("/"),t.push(e.segments.join("/")),t.push("/")),e.file&&t.push(e.file)),e.query?t.push("?"+e.query):e.params&&t.push("?"+$.param(e.params)),e.anchor&&t.push("#"+e.anchor),t.join("")}function s(e){return encodeURIComponent(e)}function o(e){return e=e||window.location.toString(),t(unescape(e.replace(/\+/g," ")))}return{encode:s,decode:o,parse:r,build:i}}()