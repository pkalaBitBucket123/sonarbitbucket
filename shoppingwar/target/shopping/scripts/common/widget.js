/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to become the base Widget file. We will slowly move stuff into here from the existinw widget.question.js
 * 
 */

define(["require","backbone"],function(e,t){var n=function(e){this.model=e.widgetModel,this.attributes=e.attributes,this.eventManager=e.eventManager,this.isPrimary=!1,this.initialize()};return n.prototype={initialize:function(){},start:function(){},render:function(){},getSummaryView:function(){return null},canMovePrev:function(){return!0},canMoveNext:function(){return!0},canSkip:function(){return!0},destroy:function(){},canRender:function(){return!0},configModel:function(){return this.model},setPrimary:function(e){this.isPrimary=e},historyKey:function(){return this.model.get("widgetId")},isComplete:function(){return!0},state:function(e){return!0}},n})