/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["module","log4javascript"],function(e,t){var n=e.config().enabled;n=n||!1,t.setEnabled(n);var r=t.getLogger(),i=new t.PopUpAppender,s=new t.PatternLayout("%d{HH:mm:ss} %-5p - %m%n");return i.setLayout(s),r.addAppender(i),r})