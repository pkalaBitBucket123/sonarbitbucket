/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone"],function(e,t){var n=t.Model.extend({storageEngine:"Proxy",initialize:function(e){e=_.extend({},e),e.storeItem?(this.storeItem=e.storeItem,this.item=_.isObject(this.storeItem)?this.storeItem.item:this.storeItem):e.requestKey&&(this.storeItem=null,this.item=null,this.requestKey=e.requestKey)}});return n})