/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/backbone/backbone.amplify"],function(e,t,n){var r=t.Model.extend({storageEngine:"Session",initialize:function(e){this.bind("change",function(){this.save()})},clear:function(e){var n=this.id;typeof e=="undefined"&&(e={}),e=_.extend(e,{silent:!0});var r=t.Model.prototype.clear.call(this,e);return this.id=n,this.save(),r}});return r})