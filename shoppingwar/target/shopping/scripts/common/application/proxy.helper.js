/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","jquery.url"],function(e){return{getUrlParams:function(e,t){var n={},r=$.url.parse();return r.params&&_.each(e,function(e){if(r.params.hasOwnProperty(e)||e==="context")e!=="context"?n[e]=r.params[e]:n.context=t}),n},getRequestParams:function(e){var t=[],n=$.url.parse();return _.each(e,function(e){var r=null;typeof e=="string"?n.params&&n.params.hasOwnProperty(e)&&(r={},r.key=e,r.value=n.params[e]):(r={},r=e),r&&t.push(r)}),t}}})