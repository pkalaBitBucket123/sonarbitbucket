/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone"],function(e,t){var n=t.Collection.extend({storageEngine:"Proxy",initialize:function(e,t){t=_.extend({},t),this.storeItem=t.storeItem?t.storeItem:null,this.item=_.isObject(this.storeItem)?this.storeItem.item:this.storeItem},setData:function(e){this.storeItem&&(this.storeItem.data=e)}});return n})