/**
 * Copyright (C) 2012 Connecture
 * 
 * Going to return an object literal for now. I do not see us needing multiple messenger objects created within the life-time of the application. If anything we should have a single object but
 * multiple channels if necessary.
 * 
 */

define(["require","underscore","log","common/application/eventmanager"],function(e,t,n,r){var i=new r;return{subscribe:function(e,r){t.each(e,function(e,t){e&&$.isFunction(e)?i.on(t,e,r):typeof e=="string"?e.lastIndexOf("update",0)===0&&i.on(t,self._proxyUpdate):n.error("You are using an unsupported event type.")})},publish:function(e,n,r){t.each(e,function(e,t){n.eventManager.on(t,function(){var e=Array.prototype.slice.call(arguments);typeof this.data!="undefined"?e.unshift(this.key,this.data):e.unshift(this.key),i.trigger.apply(i,e)},{key:e,data:r})})}}})