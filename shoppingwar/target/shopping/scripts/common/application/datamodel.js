/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 * 
 */

define(["module","log","backbone","common/application/proxy.cache"],function(e,t,n,r){var i=function(e){this.mappings=e,this.cache=r};return i.prototype={model:function(e){var r=this,i={};return _.each(this.mappings,function(t,s){if(e[s]){if(_.isArray(e[s])){var o=n.Collection;t.Collection&&(o=t.Collection);if(!r.cache[s]){var u=new o(e[s]);r.cache[s]=u}else r.cache[s].reset(e[s],{silent:!0})}else{var a=n.Model;t.Model&&(a=t.Model);if(!r.cache[s]){var f=new a(e[s]);r.cache[s]=f}else r.cache[s].clear({silent:!0}),r.cache[s].set(e[s],{silent:!0})}i[s]=r.cache[s]}}),t.debug(JSON.stringify(this.cache)),i}},i})