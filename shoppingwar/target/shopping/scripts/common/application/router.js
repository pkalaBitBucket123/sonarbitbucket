/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","log","backbone"],function(e,t,n){var r=n.Router.extend({routes:{"":"root",index:"index",welcome:"welcome"},initialize:function(e){var t=this;this.application=e.application;var n=this.application.getRoutes();_.each(n,function(e){t.route(e.route,e.name,function(){var n=e.callback.apply(this,arguments);t.application.start({initial:!1,route:n})})})},root:function(){this.application.start({initial:!0})},index:function(){this.application.start({initial:!1,index:!0})},welcome:function(){this.application.start({initial:!0})},current:function(){var e=this,t=n.history.fragment,r=_.pairs(e.routes),i=null,s=null,o;return o=_.find(r,function(n){return i=_.isRegExp(n[0])?n[0]:e._routeToRegExp(n[0]),i.test(t)}),o&&(s=e._extractParameters(i,t),i=o[1]),{route:i,fragment:t,params:s}}});return r})