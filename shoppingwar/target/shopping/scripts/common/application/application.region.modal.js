/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","underscore","common/application/application.region"],function(e,t,n){var r=function(e){e.el="#shopping-modal",n.call(this,e),this.on("view:show",this.showModal,this),this.on("view:closed",this.hideModal,this)};return r.prototype=Object.create(n.prototype),r.prototype.showModal=function(e){var t=this;this.$el.dialog({modal:!0,width:650,minHeight:200,minWidth:300,close:function(){t.close()}})},r.prototype.hideModal=function(e){this.$el.dialog("close")},r})