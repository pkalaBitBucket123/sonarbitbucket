/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to be automatically added to the dust helpers conext, I am not make this it's own define because of this. This will have to be shimmed to be added to your project
 */

(function(){typeof exports!="undefined"&&(dust=require("dust")),typeof dust.helpers=="undefined"&&(dust.helpers={}),dust.helpers.iter=function(e,t,n){var r=t.current(),i=1;for(var s in r)r.hasOwnProperty(s)&&(e=e.render(n.block,t.push({count:i,key:s,value:r[s]})),i++);return e},dust.helpers.contains=function(e,t,n,r){if(typeof r.setKey=="string"&&(typeof r.value=="string"||typeof r.value=="function")){var i=r.setKey,s=r.value;typeof r["value"]=="function"&&(s=dust.helpers.tap(r.value,e,t));var o=t.get(i),u=!1;for(var a in o)if(o[a]==s){u=!0;break}u?e=e.render(n.block,t):e=e.render(n["else"],t)}return e},typeof exports!="undefined"&&(module.exports=dust)})()