/**
 * Copyright (C) 2012 Connecture
 * 
 * This is being used for both component and application plugins, should I define separate objects for them?
 */

define(["require","exports","module"],function(e,t,n){var r=function(e){this.type=null};return r.prototype={initialize:function(e){if(!e.plugged){var t=new Error("The plugged object that we are plugging into must be specificed.");throw t.name="NoPluggedError",t}if(!e.eventManager){var t=new Error("An event manager must be specified");throw t.name="NoEventManagerError",t}_.extend(this,e)},addEvents:function(){},addMethods:function(){},plugify:function(e){this.initialize(e),this.load(),this.addEvents(),this.addMethods()},load:function(e){}},r})