/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 */

(function(e){var t=e(window).width(),n=e(window).height(),r=function(t){var t=0;this.each(function(){e(this).css("min-height","").css("height","auto"),e(this).height()>t&&(t=e(this).height()+3)}).each(function(){e(this).css("min-height",t+"px")})},i={start:function(){var i=e(this);return r.call(i),this.each(function(){e(window).bind("resize.equal",function(){var s=e(window).width(),o=e(window).height();(t!=s||n!=o)&&r.call(i),t=s,n=o})})},stop:function(){return this.each(function(){e(window).unbind("resize.equal")})}};e.fn.equal_height=function(t){if(i[t])return i[t].apply(this,Array.prototype.slice.call(arguments,1));if(typeof t=="object"||!t)return i.init.apply(this,arguments);e.error("Method "+t+" does not exist on jQuery.cart_resize")}})(jQuery)