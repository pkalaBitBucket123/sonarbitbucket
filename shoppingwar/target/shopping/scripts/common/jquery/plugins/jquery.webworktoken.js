/** Copyright (C) 2012 Connecture
 *
 * Webwork Token JQuery Plugin
 *
 * Maintains the struts.token.name and value.  Provides convenience methods to 
 * append the token to request json and extract values from a response json.
 */

(function(e){e.webworktoken={TOKEN_NAME_FIELD:"struts.token.name",_tokenName:null,_tokenValue:null,generateTokenName:function(e){var t=[],n="0123456789ABCDEF";e&&t.push(e);for(var r=0;r<6;r++)t.push(n.substr(Math.floor(Math.random()*16),1));this._tokenName=t.join("")},updateToken:function(e){e[this.TOKEN_NAME_FIELD]&&(this._tokenName=e[this.TOKEN_NAME_FIELD],e[this._tokenName]&&(this._tokenValue=e[this._tokenName]))},appendToken:function(e){this._tokenName&&(e[this.TOKEN_NAME_FIELD]=this._tokenName,this._tokenValue&&(e[this._tokenName]=this._tokenValue))},appendTokenAsArray:function(e){this._tokenName&&(e.push({name:this.TOKEN_NAME_FIELD,value:this._tokenName}),this._tokenValue&&e.push({name:this._tokenName,value:this._tokenValue}))},setTokenNameValue:function(e,t){this._tokenName=e,this._tokenValue=t}}})(jQuery)