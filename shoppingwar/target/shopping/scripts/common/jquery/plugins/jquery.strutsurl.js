/**
 * Copyright (C) 2012 Connecture
 * 
 * This is really an override script for jquery.webworkurl because of some defects in that file.
 * 
 * Instead of the override, we should be fixing the original file but we need to update it in all projects first before we can do that. This will override the methods that are having issues for GPA
 * only.
 */

(function(e){e.strutsurl={url:function(e,t,n){return this.replaceUrl(null,null,e,t,n)},htmlUrl:function(e,t,n){return this.replaceUrl(null,".html",e,t,n)},htmUrl:function(e,t,n){return this.replaceUrl(null,".htm",e,t,n)},replaceUrl:function(t,n,r,i,s,o){var u=e.url.parse(t);r!==null&&(u.segments[1]=r),n!=null?i+=n:i.match(/.action$/)||(i+=".action");var a=null;s&&(a={},e.each(s,function(e,t){t?a[e]=t:typeof u.params!="undefined"&&u.params.hasOwnProperty(e)&&(a[e]=u.params[e])}));var f={protocol:u.protocol,host:u.host,port:u.port,segments:u.segments,file:i,params:a,anchor:o};return e.url.build(f)},getParameters:function(t){var n=e.url.parse(t);return n.params},addParameters:function(t,n){var r=e.url.parse(t);return r.source=null,r.params=n,e.url.build(r)}}})(jQuery)