/*
 * Copyright (C) 2012 Connecture
 * Version: 0.1
 */

connecture.ui=function(){var e={};return e.dialogalert=function(e){var t='<div id="alertDialog"></div>';$("body").append(t),$("#alertDialog").dialog({autoOpen:!1,draggable:!0,title:"Alert!",width:350,height:150,modal:!0});var n="<h3>"+e+"</h3>";n+='<div class="buttonGroup"><input type="button" id="" class="buttonPrimary" value="Ok" onclick="$("#alertDialog").dialog("open")"/></div>',$("#alertDialog").append(n),$("#alertDialog").dialog("open")},e}()