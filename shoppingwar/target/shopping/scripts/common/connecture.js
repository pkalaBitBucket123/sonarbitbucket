/**
 * Copyright (C) 2012 Connecture
 */

(function(e){var t={};t.core={},t.ui={},t.getinternetexplorerversion=function(){var e=-1;if(navigator.appName=="Microsoft Internet Explorer"){var t=navigator.userAgent,n=new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");n.exec(t)!=null&&(e=parseFloat(RegExp.$1))}return e},typeof define=="function"&&define.amd&&define("connecture",[],function(){return t}),e.connecture=t})(window)