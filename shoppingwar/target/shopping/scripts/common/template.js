/**
 * Copyright (C) 2012 Connecture
 * 
 * How to handle roles and anon. We will need a global spot where the role of the logged in user is accessible. Then based on role, here we will check to see if there is another template for that
 * role...if there is load, that one instead of the default. We might have to have some code special in here, during the build, to load all possible templates given roles so that they are in the file
 * 
 */

define(["dust","log"],function(e,t){return{load:function(t,n){typeof t=="string"&&(t.indexOf("(function(){dust.register")===0?e.loadSource(t):e.loadSource(e.compile(t,n)))},render:function(n,r){var i="";return typeof n!="undefined"&&n!==null&&n!==""&&e.render(n,r,function(e,n){i=n,e&&t.error(e)}),i}}})