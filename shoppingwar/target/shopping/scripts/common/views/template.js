/**
 * Copyright (C) 2012 Connecture
 * 
 * AMD Backbone View Template to be used within Shopping.
 */

define(["require","backbone","templateEngine","common/component/component","REPLACE/WITH/TEMPLATE/LOCATION"],function(e,t,n,r,i){n.load(i,"replace.with.template.name");var s=r.view.Base.extend({template:"replace.with.template.name",initialize:function(e){this.eventManager=e.eventManager},events:{},render:function(){var e={};return $(this.el).html(this.renderTemplate({data:e})),this},close:function(){}});return s})