/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 */

define(["require","backbone","templateEngine","jquery.equalHeight"],function(e,t,n){var r=function(e){this.views=[],t.View.apply(this,[e])};return _.extend(r.prototype,t.View.prototype,{dispose:function(e){e=_.extend({empty:!1},e),this.disposeSubViews(),this.unbind(),this.stopListening(),e.empty?$(this.el).empty():this.remove()},disposeSubViews:function(){var e=this;this.views&&_.each(this.views,function(t){e.stopListening(t),t.dispose()})},renderTemplate:function(e){var t="",r;e&&e.template?r=this[e.template]:r=this.template;var i;e&&e.data?i=e.data:i=this.data;var s=i;return e&&e.functions&&(s=dust.makeBase(e.functions),s=s.push(i)),t=n.render(r,s),t},addEvents:function(e){this.events=_.extend(_.clone(this.events),e),this.delegateEvents()}}),r.extend=t.View.extend,r})