/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main application view. It will define the template and high level stuff for the application as a whole.
 * 
 * TODO: This might be better labeled as a page instead of app. Unless we use this for bootstrapping stuff ,
 */

define(["require","backbone","templateEngine","common/views/base","text!html/shopping/views/application.dust"],function(e,t,n,r,i){n.load(i,"application");var s=r.extend({el:$("body"),appTemplate:"application",events:{},initialize:function(e){e.eventManager&&(this.eventManager=e.eventManager)},render:function(){var e=this;return $(e.el).html(n.render(this.appTemplate,{})),$("a[title]",e.el).tooltip({style:{classes:"ui-tooltip-standard"},position:{my:"bottom center",at:"top center"}}),this}});return s})