/**
 * Copyright (C) 2012 Connecture
 * 
 * Component Template
 * 
 * 
 */

define(["require","backbone","templateEngine","common/component/component"],function(e,t,n,r){var i=function(e){r.component.core.Base.call(this,e),this.items=[],this.events=_.extend({},this.events,{}),this.triggers={}};return i.prototype=Object.create(r.component.core.Base.prototype),i.prototype.load=function(e){},i.prototype.update=function(e){},i.prototype.registerEvents=function(){this.eventManager.on("start",function(){},this)},i})