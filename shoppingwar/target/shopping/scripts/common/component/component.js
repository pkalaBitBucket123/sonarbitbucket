/**
 * Copyright (C) 2012 Connecture
 * 
 * Add extensions for: - making this persistable - how we handle returning data, JSON or parameters-strutsy
 * 
 * This needs to be updated so it gets based a component base context and then this expands upon it
 * 
 * 
 * 
 * 
 */

define(["require","underscore","common/views/base","common/component/component.core","common/component/component.base","common/component/component.view","common/application/application.region","common/application/application.manager.region"],function(e,t,n,r,i,s,o,u){return r.component.core.View=s,r.component.core.Base=i,r.component.layout={},r.component.layout.Region=o,r.component.layout.RegionManager=u,r})