/**
 * Copyright (C) 2012 Connecture
 * 
 * This defines the component structure off of the connecture namespace
 * 
 */

define(["require","connecture","common/views/base"],function(e,t,n){return t.component={},t.component.core={},t.view={},t.view.Base=n,t})