/**
 * Copyright (C) 2012 Connecture
 * 
 * This file allows us to configure and override Backbone.sync for different purposes.
 * 
 * Each model or collection that needs to use a specific type, should specify with a given string.
 * 
 * This file will then include all possible sync overrides and determine which one to use based on how the model or collection is configured.
 * 
 * Originally we had the model or collection directly referencing the override files but that was causing circular references because one includes Proxy, which is used elsewhere.
 * 
 * This should be more robust and not cause circular dependencies
 * 
 */

define(["require","backbone","common/backbone/backbone.proxy","common/backbone/backbone.amplify"],function(e,t,n,r){var i=[n,r];return t.ajaxSync=t.sync,t.getSyncMethod=function(e){var n=t.ajaxSync;return _.each(i,function(t){t.type===e.storageEngine&&(n=t.sync)}),n},t.sync=function(e,n,r){return t.getSyncMethod(n).apply(this,[e,n,r])},t})