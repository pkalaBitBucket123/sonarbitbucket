/**
 * Copyright (C) 2012 Connecture
 * 
 * Backbone sync override for working with the Amplify.
 * 
 */

define(["require","backbone","amplify"],function(e,t,n){var r={create:function(e,t){return n.store(e.id,JSON.stringify(e.toJSON())),e.toJSON()},read:function(e,t){var r=n.store.sessionStorage(e.id);return r?JSON.parse(r):null},update:function(e){return n.store.sessionStorage(e.id,null),n.store.sessionStorage(e.id,JSON.stringify(e.toJSON())),e.toJSON()}};return{type:"Session",sync:function(e,t,n,i){typeof n=="function"&&(n={success:n,error:i});var s={};switch(e){case"read":s=t.id!=undefined?r.read(t):findAll();break;case"create":s=r.create(t);break;case"update":s=r.update(t);break;case"delete":s=r.destroy(t)}s?n&&n.success&&n.success(s):n&&n.error&&n.error("Record not found")}}})