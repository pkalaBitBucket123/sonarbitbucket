/**
 * Copyright (C) 2012 Connecture
 * 
 * Backbone sync override for working with the Proxy API.
 * 
 */

define(["require","backbone","common/application/proxy"],function(e,t,n){var r=new n,i={read:function(e,t){var n="component:get",i={defer:!0,raw:!0};e.requestKey?n=e.requestKey:i.requests=[e.storeItem],e.trigger("request"),r.queue(r.process(n,i,function(n){t.success(n[e.item])}),"get")},update:function(e,t){var n="component:persist",i={raw:!0,defer:!0};if(e.requestKey)n=e.requestKey,i.data=e.toJSON();else{var s={};s[e.item]=e.toJSON(),i.requests=[{store:e.storeItem,data:s}]}e.trigger("request"),r.queue(r.process(n,i,function(n){e.item?t.success(n[e.item]):t.success(n)}),"post")}};return{type:"Proxy",sync:function(e,t,n,r){typeof n=="function"&&(n={success:n,error:r});switch(e){case"read":i.read(t,n);break;case"create":i.update(t,n);break;case"update":i.update(t,n);break;case"delete":}}}})