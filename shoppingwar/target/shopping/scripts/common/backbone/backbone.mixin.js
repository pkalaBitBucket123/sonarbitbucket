/**
 * Copyright (C) 2012 Connecture
 * 
 * This is based off the following gist
 * 
 * https://gist.github.com/hxgdzyuyi/3652964
 * 
 * Handles model/collection/view mixins.  This should be expanded
 * though to handle any number of methods, not just inititalize. 
 * 
 */

define(["require","backbone"],function(e,t){return t.mixin=function(e,t,n){n&&(n.events&&t.events&&(t=_.clone(t),_.defaults(n.events,t.events)),_.extend(t,n));var r=e.prototype||e;_.defaults(r,t),t.events&&(r.events?_.defaults(r.events,t.events):r.events=t.events);if(t.initialize!==undefined){var i=r.initialize;r.initialize=function(){t.initialize.apply(this,arguments),i.apply(this,arguments)}}},t})