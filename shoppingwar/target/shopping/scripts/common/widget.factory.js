/**
 * Copyright (C) 2012 Connecture
 * 
 * Not sure the best place for this now but going to use this as a widget factory.
 * 
 * The implementing application will pass in the definition.
 * 
 * For now, the constructor of this factory will take in the configuration map for those widgets.
 * 
 * Ideally we can figure out a better way to get that configuration in there
 * 
 */

define(["require","backbone","widget/definition"],function(e,t,n){var r=function(e){this.configuration=e};return r.prototype={getWidget:function(e,t){return n.createWidget(e,t)}},r})