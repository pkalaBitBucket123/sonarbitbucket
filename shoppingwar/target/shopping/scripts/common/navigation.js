/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["module"],function(e){var t=function(){this.views=[]};return t.prototype={addView:function(e,t){this.views.push({View:e,options:t})}},t})