/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/views/base","text!html/shopping/common/workflows/view.workflow.item.widget.progress.dust","dust-helpers"],function(e,t,n,r,i){n.load(i,"view.workflow.item.widget.progress");var s=r.extend({template:"view.workflow.item.widget.progress",events:{"click .progBarList > ._question":"questionSelected"},render:function(){var e=this.collection.toJSON(),t={currentWidgetId:this.model.get("widgetId"),wigetList:e,renderProgressList:e.length>1};return $(this.el).html(this.renderTemplate({data:t})),this},questionSelected:function(e){var t=$(e.currentTarget),n=t.attr("data-id");this.options.eventManager.trigger("workflow:item:sub:render:widget",n)},workflowSkipToPlans:function(){this.options.eventManager.trigger("widget:done","exit")}});return s})