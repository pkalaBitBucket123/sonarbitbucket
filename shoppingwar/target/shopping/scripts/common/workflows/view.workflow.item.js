/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to end up being the frame from the workflow item
 * 
 */

define(["require","backbone","templateEngine","common/views/base","text!html/shopping/common/workflows/view.workflow.template.dust","dust-helpers"],function(e,t,n,r,i){n.load(i,"view.workflow.template");var s=r.extend({className:"questionnaire _componentView",template:"view.workflow.template",events:{},render:function(){var e=this,t=this.renderTemplate();return $(e.el).append(t),this}});return s})