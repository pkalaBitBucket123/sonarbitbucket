/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the top level component header. This is the main header that will be customized depending on where shopping is included.
 * 
 */

define(["module","log","backbone","templateEngine","common/component/component","shopping/individual/components/header/component.header"],function(e,t,n,r,i,s){var o=function(e){s.call(this,e)};return o.prototype=Object.create(s.prototype),o.prototype.changeQuotePlans=function(e){e.remove||this.showQuote(!0)},o.prototype.removeQuotePlan=function(e){var t=this.models.quote.get("plans");for(var n=0;n<t.length;n++)t[n].planId==e.planId&&t.splice(n,1);var r=this.models.quote.get("selectedRiders");r&&r.DENTAL&&delete r.DENTAL[e.planId],this.models.quote.unset("selectedRiders",{silent:!0}),this.models.quote.set({selectedRiders:r},{remove:!0}),this.models.quote.unset("plans",{silent:!0}),this.models.quote.set({plans:t},{remove:!0}),this.showQuote()},o.prototype.saveQuote=function(){this.saveAndExit()},o})