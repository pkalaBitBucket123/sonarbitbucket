/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","components/header/views/view.header.welcome"],function(e,t,n){return HeaderWelcomeView=n.extend({events:{"click ._login":"login"},login:function(e){this.eventManager.trigger("view:login")}}),HeaderWelcomeView})