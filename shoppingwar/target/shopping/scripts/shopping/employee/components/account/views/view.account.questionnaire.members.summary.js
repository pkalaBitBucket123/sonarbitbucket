/**
 * Copyright (C) 2012 Connecture
 */

define(["require","backbone","components/account/views/view.account.questionnaire.members.summary"],function(e,t,n){var r=n.extend({initialize:function(e){n.prototype.initialize.call(this,e)}});return r.prototype=Object.create(n.prototype),r.prototype.isMemberCovered=function(e){return e.get("coverageSelected")},r})