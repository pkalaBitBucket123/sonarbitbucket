/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/component/component","components/account/widgets/widget.costestimator"],function(e,t,n,r,i,s){var o=function(e){s.call(this,e)};return o.prototype=Object.create(s.prototype),o.prototype.getMembersForEstimation=function(e){var t=[],n=this.model.get("config").productLineForEstimation;for(var r=0;r<e.length;r++){var i=!1;for(var s=0;e[r].products&&s<e[r].products.length&&!i;s++)i=e[r].products[s].productLineCode==n,i&&t.push(e[r])}return t},o.prototype.isCoverageSelected=function(e){return e.coverageSelected},o})