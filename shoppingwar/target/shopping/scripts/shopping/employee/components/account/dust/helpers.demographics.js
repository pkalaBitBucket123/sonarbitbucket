/**
 * Copyright (C) 2012 Connecture
 */

define(["require","dust","dust-extensions"],function(e,t,n){t.helpers.hasValue=function(e,t,n,r){var i=!1,s=r.fieldName,o=t.current(),u=t.get("missingValues"),a=o.memberRefName;return i=u&&u[a]&&u[a][s],i&&i=="missing"?e=e.render(n["else"],t):e=e.render(n.block,t),e}})