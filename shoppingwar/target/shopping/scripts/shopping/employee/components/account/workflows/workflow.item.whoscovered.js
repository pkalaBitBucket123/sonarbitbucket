/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","components/account/workflows/workflow.item.whoscovered"],function(e,t,n){var r=function(e,t,r){n.call(this,e,t,r)};return r.prototype=Object.create(n.prototype),r.prototype.isCostEstimatorCoverageSelected=function(){var e=!1;if(this.costEstimatorIncluded){var t=this.costEstimatorWidgetModel.get("config").productLineForEstimation;e=this.options.models.account.isCoverageSelected(t)}return e},r})