/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/component/component","text!html/employee/components/header/view.header.links.internal.dust"],function(e,t,n,r,i,s){return n.load(s,"view.header.links.internal"),HeaderLinksView=i.view.Base.extend({template:"view.header.links.internal",events:{"click ._backButton":"back","click ._logout":"logout"},initialize:function(e){this.eventManager=e.eventManager},render:function(){var e=this,t=this.renderTemplate({data:e.options.data});return $(e.el).html(t),this},back:function(){this.eventManager.trigger("exit",{key:"back",location:this.options.data.rosterURL})},logout:function(){this.eventManager.trigger("view:logout")}}),HeaderLinksView})