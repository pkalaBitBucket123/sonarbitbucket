/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","components/header/views/view.app.header.logo"],function(e,t,n){return AppHeaderLogoView=n.extend({initialize:function(e){var t=this.options.models.employer;this.logoData={},this.logoData.name=t.get("name"),this.logoData.logoPath=t.get("logoPath")}}),AppHeaderLogoView})