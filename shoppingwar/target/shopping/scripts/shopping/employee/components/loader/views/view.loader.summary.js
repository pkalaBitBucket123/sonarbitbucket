/**
 * Copyright (C) 2012 Connecture
 *
 */

define(["require","backbone","components/loader/views/view.loader.summary"],function(e,t,n){var r=864e5,i=n.extend({className:"callout",initialize:function(e){n.prototype.initialize.call(this,e),this.data={productLines:e.models.productLines.toJSON(),employer:e.models.employer.toJSON()};var t=e.models.account.get("expirationTimeMs");if(t){var i=new Date(t);t=(new Date(i.getFullYear(),i.getMonth(),i.getDate())).getTime(),t+=r-1;var s=(new Date).getTime();this.data.daysLeft=0;if(t>s){var o=t-s,u=Math.floor(o/r);this.data.daysLeft=u}}}});return i})