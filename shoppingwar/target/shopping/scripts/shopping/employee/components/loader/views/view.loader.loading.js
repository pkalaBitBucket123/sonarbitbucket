/**
 * Copyright (C) 2012 Connecture
 *
 */

define(["require","backbone","common/views/base"],function(e,t,n){var r=n.extend({className:"callout",initialize:function(e){},render:function(){var e=this;return $(e.el).html('<div class="section days-left"><div class="action-indicator"><div class="indicator"></div><div class="message">Loading your benefits</div></div></div>'),this}});return r})