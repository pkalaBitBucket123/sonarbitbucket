/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: This needs to get moved out of common
 * 
 * this will control how data is packaged and requested for your application
 * 
 * This should have the store stuff
 * 
 * This should also have the evenlope stuff
 * 
 * I think this should get referenced in the proxy
 * 
 * So then the proxy will call it's datamodel class to process each request
 * 
 * Based on the request the data model can decide what to do
 * 
 * 
 */

define(["module","backbone","common/application/datamodel","shopping/common/models/model.account","components/account/models/model.costestimatorconfig","components/account/models/model.eligibility","components/list/models/model.planconfig","shopping/common/collections/collection.product","components/list/models/model.costestimatorcosts","shopping/common/models/model.employer","shopping/common/models/model.member","shopping/common/collections/collection.widget","shopping/common/collections/collection.app.workflow","shopping/common/models/model.workflow"],function(e,t,n,r,i,s,o,u,a,f,l,c,h,p){var d={account:{Model:r},widgets:{Collection:c},costEstimatorConfig:{Model:i},members:{Collection:t.Collection},productLines:{Collection:t.Collection},eligibility:{Model:s},relationships:{Collection:t.Collection},genders:{Collection:t.Collection},plans:{Collection:u},planConfig:{Model:o},filters:{Model:t.Model},costEstimatorCosts:{Model:a},applicationConfig:{Model:t.Model},actor:{Model:t.Model},employer:{Model:f},accountHolder:{Model:l},questionEvaluations:{Collection:t.Collection},workflowItems:{Collection:h},workflow:{Model:p},plugins:{Collection:t.Collection}};return new n(d)})