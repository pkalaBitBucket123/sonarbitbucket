/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */

define(["module","backbone","templateEngine","common/component/component","components/cart/component.cart"],function(e,t,n,r,i){var s=function(e){i.call(this,e)};return s.prototype=Object.create(i.prototype),s.prototype.buildCoveredMembers=function(e){var t=new Array;return _.each(e,function(e){t[t.length]=e}),t},s.prototype.enroll=function(){this.eventManager.trigger("exit",{key:"checkout"})},s})