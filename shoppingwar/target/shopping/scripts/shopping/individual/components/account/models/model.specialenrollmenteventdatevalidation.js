/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){var t=e.eventDate?e.eventDate:"",r=e.eventReason?e.eventReason:"";n.prototype.initialize.call(this,{storeItem:{item:"specialEnrollmentEventDateValidation",params:[{key:"eventDate",value:t},{key:"eventReason",value:r}]}})}});return r})