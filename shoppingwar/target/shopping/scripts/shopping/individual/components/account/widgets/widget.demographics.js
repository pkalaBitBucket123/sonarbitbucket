/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/widget","components/account/widgets/views/view.widget.demographics","shopping/individual/components/account/widgets/views/view.widget.demographics.onexchange","components/account/widgets/widget.demographics"],function(e,t,n,r,i,s){var o=function(e){s.call(this,e)};return o.prototype=Object.create(s.prototype),o.prototype.getView=function(e){var t=this.attributes.session,n=this.model.get("onExchange");if(this.isPrimary){var s=this.attributes;s.demographicsConfig=this.model,s.eventManager=this.eventManager,s.demographicsInitialized=this.demographicsInitialized,s.missingValues=t.get("missingValues"),s.applicationConfig=this.attributes.applicationConfig,e&&(s.demographicsData=e),n?this.view=new i(s):this.view=new r(s)}else this.view=null;return this.view},o.prototype.destroy=function(){this.view&&this.view.remove()},o})