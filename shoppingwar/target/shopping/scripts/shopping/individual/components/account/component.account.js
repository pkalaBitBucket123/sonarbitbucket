/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */

define(["module","backbone","templateEngine","dust-helpers","common/component/component","components/account/component.account"],function(e,t,n,r,i,s){var o=function(e){s.call(this,e)};return o.prototype=Object.create(s.prototype),o.prototype.registerEvents=function(){s.prototype.registerEvents.call(this)},o.prototype.updateMemberProviders=function(e){var t=this,n=[];_.each(e.members,function(t){var r={};r.memberId=t.memberRefName;var i=new Array;_.each(t.value,function(t){_.each(e.data.providerSearchResults,function(e){e.xrefid==t&&(i[i.length]=e)})}),r.providers=i,n[n.length]=r}),_.each(n,function(e){var n=t.models.account.get("members");_.each(n,function(t){t.memberRefName==e.memberId&&(t.providers=e.providers)})}),this.models.account.save()},o})