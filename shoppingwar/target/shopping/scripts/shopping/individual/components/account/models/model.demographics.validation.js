/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){var t=e.effectiveDate||"",r=e.zipCode||"",i=e.eventType||"",s=e.eventDate||"";n.prototype.initialize.call(this,{storeItem:{item:"demographicsValidation",params:[{key:"effectiveDate",value:t},{key:"zipCode",value:r},{key:"eventType",value:i},{key:"eventDate",value:s}]}})},validateDemographics:function(e){this.save({},e)}});return r})