/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/component/component","components/account/widgets/widget.costestimator"],function(e,t,n,r,i,s){var o=function(e){s.call(this,e)};return o.prototype=Object.create(s.prototype),o.prototype.getMembersForEstimation=function(e){return e},o.prototype.isCoverageSelected=function(e){return!0},o})