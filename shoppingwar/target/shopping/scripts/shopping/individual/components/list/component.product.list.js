/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */

define(["module","backbone","templateEngine","common/component/component","components/list/component.product.list"],function(e,t,n,r,i){var s=function(e){i.call(this,e)};return s.prototype=Object.create(i.prototype),s.prototype.registerEvents=function(){i.prototype.registerEvents.call(this),this.eventManager.on("view:selectForQuote",function(e){this.models.quote.togglePlanInclusion(e),this._refreshCurrentView()},this),this.models.quote.on("change:plans",function(){this._refreshCurrentView()},this)},s})