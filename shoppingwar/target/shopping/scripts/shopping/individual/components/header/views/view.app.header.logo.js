/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","components/header/views/view.app.header.logo"],function(e,t,n){return AppHeaderLogoView=n.extend({initialize:function(e){this.logoData={},this.logoData.name="",this.logoData.logoPath="../images/carrierlogoNE.png"}}),AppHeaderLogoView})