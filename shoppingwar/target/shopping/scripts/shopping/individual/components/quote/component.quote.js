/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */

define(["module","backbone","templateEngine","common/component/component","components/quote/component.quote"],function(e,t,n,r,i){var s=function(e){i.call(this,e)};return s.prototype=Object.create(i.prototype),s.prototype.registerEvents=function(){i.prototype.registerEvents.call(this)},s})