/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/component/component","text!html/shopping/components/header/view.header.producer.dust"],function(e,t,n,r,i){return n.load(i,"view.header.producer"),ProducerView=r.view.Base.extend({template:"view.header.producer",events:{},initialize:function(){},render:function(){var e=this,t=this.model.toJSON();t.contact.phonenumber!=null?t.contact.phonenumber=t.contact.phonenumber.replace(/(\d{3})(\d{4})/,"$1-$2"):t.contact.phonenumber="";var n=this.renderTemplate({data:this.model.toJSON()});return $(e.el).html(n),this}}),ProducerView})