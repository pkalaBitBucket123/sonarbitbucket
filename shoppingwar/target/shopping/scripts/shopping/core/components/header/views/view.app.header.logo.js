/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/views/base","text!html/shopping/components/header/view.app.header.logo.dust"],function(e,t,n,r,i,s){return n.load(s,"view.app.header.logo"),AppHeaderLogoView=i.extend({template:"view.app.header.logo",events:{},initialize:function(){this.logoData={}},render:function(){var e=this,t=this.renderTemplate({data:this.logoData});return $(e.el).html(t),this}}),AppHeaderLogoView})