/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/component/component","text!html/shopping/components/header/view.app.header.dust"],function(e,t,n,r,i,s){n.load(s,"view.app.header");var o=i.component.core.View.extend({template:"view.app.header",className:"_component third-section",events:{},initialize:function(){i.component.core.View.prototype.initialize.call(self)},render:function(){var e=this,t=this.renderTemplate({});return $(e.el).html(t),this}});return o})