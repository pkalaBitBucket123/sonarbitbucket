/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/component/component","text!html/shopping/components/header/view.header.dust"],function(e,t,n,r,i,s){return n.load(s,"view.header"),HeaderView=i.component.core.View.extend({template:"view.header",frame_localization:{title:""},events:{},initialize:function(){i.component.core.View.prototype.initialize.call(self)},render:function(){var e=this,t=this.renderTemplate({});return $(e.el).html(t),this}}),HeaderView})