/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the shopping application header component.
 * 
 */

define(["module","backbone","templateEngine","common/component/component","shopping/common/models/model.account","components/header/views/view.app.header","components/header/views/view.app.header.logo"],function(e,t,n,r,i,s,o){var u=function(t){r.component.core.Base.call(this,t),this.items=e.config().items,this.configuration.layout=_.extend({},this.configuration.layout,{View:s,regions:{logo:".logo"}})};return u.prototype=Object.create(r.component.core.Base.prototype),u.prototype.registerEvents=function(){this.eventManager.on("start",function(){this.regionManager.logo.show(new o({models:this.models}))},this)},u.prototype.load=function(e){r.component.core.Base.prototype.load.call(this,e)},u.prototype.hide=function(){},u.prototype.show=function(){},u.prototype.minimize=function(){},u})