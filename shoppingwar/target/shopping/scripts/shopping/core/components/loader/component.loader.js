/**
 * Copyright (C) 2012 Connecture
 * 
 * Component Template
 * 
 * 
 */

define(["module","backbone","templateEngine","common/component/component","components/loader/views/view.loader.loading","components/loader/views/view.loader.summary","text!html/shopping/components/loader/view.loader.dust"],function(e,t,n,r,i,s,o){n.load(o,"view.loader");var u=r.component.core.View.extend({template:"view.loader",render:function(){var e=this;return $(e.el).html(this.renderTemplate()),$(e.el).removeClass("_component"),this}}),a=function(t){r.component.core.Base.call(this,t),this.items=e.config().items,this.publish={},this.id="WelcomeComponent",this.configuration.layout=_.extend({},this.configuration.layout,{el:"._loaderComponent",View:u,regions:{content:"._content"}})};return a.prototype=Object.create(r.component.core.Base.prototype),a.prototype.load=function(e){},a.prototype.update=function(e){},a.prototype.registerEvents=function(){this.eventManager.on("start",function(){this.regionManager.content.show(new i)},this),this.eventManager.on("test",function(){this.regionManager.content.show(new s({eventManager:this.eventManager,models:this.models}))},this)},a})