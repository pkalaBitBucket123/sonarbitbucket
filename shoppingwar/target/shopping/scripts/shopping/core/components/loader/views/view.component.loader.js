/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/views/base","text!html/shopping/components/view.component.loader.dust"],function(e,t,n,r,i){n.load(i,"view.component.loader");var s=r.extend({template:"view.component.loader",render:function(){return $(this.el).html(this.renderTemplate()),this},initialize:function(){var e=this,t=new Date,n=new Date;t.setFullYear(2014,0,1),t.setHours(0,0,0,0),n.setFullYear(2014,10,15),n.setHours(0,0,0,0);var r=new Date;if(r>=t)var i="true";else var i="false";t.setFullYear(2015,1,16);if(r>=n&&r<t)var s="false";else var s="true";this.data={hide:i,hideSpecial:s}}});return s})