/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","dust-helpers","common/component/component","text!html/shopping/components/menu/view.menu.mega.dust"],function(e,t,n,r,i,s){n.load(s,"view.menu.mega");var o=i.view.Base.extend({template:"view.menu.mega",events:{},initialize:function(e){this.eventManager=e.eventManager,this.navigation=e.navigation},render:function(){var e=this;$(e.el).html(this.renderTemplate({data:{}})),_.each(this.navigation.views,function(t){var n=new t.View(t.options);$("ul._navigation",e.el).append(n.render().el),e.views.push(n)});var t=$("ul._navigation li a._showProductList",e.el);return secondLi=t.parent(),secondLi.prependTo($("ul._navigation li:first",e.el).parent()),this}});return o})