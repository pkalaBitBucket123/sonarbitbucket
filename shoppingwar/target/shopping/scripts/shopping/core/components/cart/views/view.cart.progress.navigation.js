/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/component/component","text!html/shopping/components/cart/view.cart.progress.navigation.dust"],function(e,t,n,r,i){n.load(i,"view.cart.progress.navigation");var s=r.view.Base.extend({template:"view.cart.progress.navigation",tagName:"li",events:{"click a":"show"},initialize:function(e){this.eventManager=e.eventManager,this.componentId=e.componentId,this.account=e.account},render:function(){var e=this,t={};return this.account.get("progress")&&this.account.get("progress").cart&&(t.visited=!0),$(e.el).html(this.renderTemplate({data:t})),this},show:function(){t.history.navigate("view/"+this.componentId+"/open",{trigger:!0})}});return s})