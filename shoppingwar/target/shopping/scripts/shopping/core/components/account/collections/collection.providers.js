/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.collection","components/account/models/model.provider"],function(e,t,n,r){var i=n.extend({model:r,initialize:function(e,t){var r={};t&&t.search&&(r.search=t.search),n.prototype.initialize.call(this,e,{storeItem:{item:"providers",data:r}})},setData:function(e){return n.prototype.setData({search:e}),this}});return i})