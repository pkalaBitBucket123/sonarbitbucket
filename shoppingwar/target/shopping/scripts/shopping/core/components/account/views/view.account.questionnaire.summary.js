/**
 * Copyright (C) 2012 Connecture
 */

define(["require","backbone","templateEngine","common/component/component","components/account/views/view.account.question.summary","text!html/shopping/components/account/view.questionnaire.summary.dust"],function(e,t,n,r,i,s){n.load(s,"view.questionnaire.summary");var o=r.view.Base.extend({template:"view.questionnaire.summary",initialize:function(e){this.title=e.title,this.questions=e.questions,this.account=e.account},render:function(){var e=this;return $(e.el).html(e.renderTemplate({title:e.title})),this.questions.each(function(t){var n=new i({model:t,account:e.account});$(".dataGroup.multiGroup",e.el).append(n.render().el),e.views.push(n)}),this}});return o})