/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/widget","components/account/widgets/views/view.widget.demographics"],function(e,t,n,r){var i=function(e){n.call(this,e)};return i.prototype=Object.create(n.prototype),i.prototype.initialize=function(){var e=this;e.demographicsInitialized=!1,this.eventManager.on("view:specialenrollment:show",function(t){e.demographicsInitialized=!0,this.eventManager.trigger("widget:showSpecialEnrollment",t)},this)},i.prototype.canRender=function(){return this.isPrimary},i.prototype.getView=function(e){if(this.isPrimary){var t=this.attributes;t.demographicsConfig=this.model,t.eventManager=this.eventManager,t.demographicsInitialized=this.demographicsInitialized,e&&(t.demographicsData=e),this.view=new r(t)}else this.view=null;return this.view},i.prototype.destroy=function(){this.view&&this.view.remove()},i})