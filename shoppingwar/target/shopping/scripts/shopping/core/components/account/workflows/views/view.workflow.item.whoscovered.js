/**
 * Copyright (C) 2012 Connecture
 * 
 * UGH, right now I need this file to change the template...make this better later
 * 
 */

define(["require","backbone","templateEngine","common/component/component","text!html/shopping/components/account/workflows/view.workflow.whoscovered.template.dust"],function(e,t,n,r,i){n.load(i,"view.workflow.whoscovered.template");var s=r.view.Base.extend({className:"questionnaire _componentView",template:"view.workflow.whoscovered.template",events:{},initialize:function(e){r.component.core.View.prototype.initialize.call(this)},render:function(){var e=this,t=this.renderTemplate();return $(e.el).append(t),this}});return s})