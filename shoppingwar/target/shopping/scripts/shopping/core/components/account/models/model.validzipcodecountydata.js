/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){var t=e.zipCode?e.zipCode:"";n.prototype.initialize.call(this,{storeItem:{item:"validZipCodeCountyData",params:[{key:"zipCode",value:t}]}})}});return r})