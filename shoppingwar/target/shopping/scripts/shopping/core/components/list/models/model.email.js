/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the old version of model.email. Please use the common/models/model.email going forward
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){function i(e){if(e==null||e=="")return!0;var t=/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;return t.test(e)}var r=n.extend({initialize:function(e){n.prototype.initialize.call(this,{requestKey:"email"})},validate:function(e){this.errors=[];if(!e.toEmail)this.errors[this.errors.length]={field:"_toEmail",value:"no recipients"};else{var t=[],n=e.toEmail.split(";");for(var r=0;r<n.length;r++)i(n[r].trim())||(t[t.length]=n[r].trim());if(t.length>0){var s="The email address format is invalid. Please make sure the format is x@x.xxx  or x@x.xx  or x@x.x.xxx  or x@x.x.xx";for(var o=0;o<t.length;o++)s+=t[o],t.length-o>1&&(s+=", ");this.errors[this.errors.length]={field:"_toEmail",value:s}}}if(this.errors.length>0)return this.errors}});return r})