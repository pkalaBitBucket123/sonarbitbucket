/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){n.prototype.initialize.call(this,{storeItem:{item:"subsidyUsage",params:[]}})},applySubsidy:function(e){return this.save({},e)}});return r})