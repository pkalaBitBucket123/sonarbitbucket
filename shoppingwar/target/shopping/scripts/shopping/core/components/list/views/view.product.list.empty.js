/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/component/component","text!html/shopping/components/list/view.product.list.empty.dust"],function(e,t,n,r,i){n.load(i,"view.product.list.empty");var s=r.view.Base.extend({template:"view.product.list.empty",initialize:function(){},render:function(){var e=this;return $(e.el).html(this.renderTemplate()),$("a[title]",e.el).tooltip(),this}});return s})