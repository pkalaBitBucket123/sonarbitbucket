/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the new version of workflows, and should replace collection.workflow.js
 * 
 */

define(["require","backbone","shopping/common/models/model.workflow.item"],function(e,t,n){var r=t.Collection.extend({model:n,initialize:function(e,t){}});return r})