/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone"],function(e,t){var n=t.Collection.extend({initialize:function(e,t){},getWidget:function(e,t){var n=null;return this.each(function(r){t==="widget"?r.get("config").widgetId===e&&(n=r):r.get("widgetId")===e&&(n=r)}),n},isCostEstimatorIncluded:function(){return this.getWidget("CostEstimator","widget")!==null},getCostEstimatorId:function(){var e=null,t=this.toJSON();for(var n=0;n<t.length&&e==null;n++)t[n].widgetId=="CostEstimator"&&(e=t[n].questionRefId);return e},getFilteredWidgets:function(e,t){var r=this;if(_.isEmpty(e))return new n(this.models);var i=new n;return _.each(e,function(e){var n=_.isObject(e)?e.id:e;r.each(function(e){(t&&n===e.get("config")[t]||n===e.get("widgetId"))&&i.add(e)})}),i}});return n})