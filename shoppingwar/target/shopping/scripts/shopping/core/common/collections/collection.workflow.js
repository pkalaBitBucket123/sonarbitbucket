/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: This is going away with the config redesign
 * 
 */

define(["require","backbone"],function(e,t){var n=t.Collection.extend({initialize:function(e,t){},getDefaultWorkflow:function(){var e=null;return this.each(function(t){t.get("defaultWorkflow")===!0&&(e=t)}),e},getWorkflow:function(e){var t=null;return this.each(function(n){n.get("workflowRefId")==e&&(t=n)}),t}});return n})