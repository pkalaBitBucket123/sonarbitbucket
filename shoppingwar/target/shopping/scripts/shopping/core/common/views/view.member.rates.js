/**
 * Copyright (C) 2012 Connecture
 * 
 * This is in the common section because it is referenced across components
 * 
 */

define(["require","backbone","jquery-formatCurrency","templateEngine","common/component/component","text!html/shopping/views/view.member.rates.dust"],function(e,t,n,r,i,s){r.load(s,"view.member.rates");var o=i.view.Base.extend({template:"view.member.rates",events:{},initialize:function(e){this.eventManager=e.eventManager,this.account=this.options.account},render:function(){var e=this,t={};t.rate=this.model.get("cost.monthly.rate.amount"),t.name=this.model.get("name"),t.members=[];var n=this.model.get("cost.monthly.memberLevelRating.memberRates"),r=this.account.get("members");return _.each(r,function(e){var r="included",i=null;_.each(n,function(t){if(e.memberExtRefId==t.memberId||e.memberRefName==t.memberId)t.amount!=="included"&&!isNaN(parseFloat(t.amount))&&parseFloat(t.amount)!==0&&(r=t.amount),t.tobaccoSurcharge&&t.tobaccoSurcharge!=="included"&&parseFloat(t.tobaccoSurcharge)!==0&&(i=t.tobaccoSurcharge)}),t.members.push({name:e.name,relationship:e.memberRelationship,rate:r,tobaccoSurcharge:i})}),$(e.el).html(this.renderTemplate({data:t})),$(".rate",e.el).formatCurrency({roundToDecimalPlace:2}),$(".member-rate",e.el).formatCurrency({roundToDecimalPlace:2}),this}});return o})