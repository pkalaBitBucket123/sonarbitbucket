/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/views/base","text!html/shopping/views/view.notification.dust"],function(e,t,n,r,i){return n.load(i,"view.notification"),ConfrimationModalView=r.extend({template:"view.notification",events:{"click ._btnClose":"closeModal"},initialize:function(){},render:function(){var e=this,t=this.renderTemplate({data:e.options.data});return $(e.el).html(t),this},closeModal:function(e){e.preventDefault();var t=this;t.options.eventManager.trigger("view:closeNotificationModal")}}),ConfrimationModalView})