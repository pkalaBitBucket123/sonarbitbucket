/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/views/base","text!html/shopping/views/view.confirmation.dust"],function(e,t,n,r,i){return n.load(i,"view.confirmation"),ConfrimationModalView=r.extend({template:"view.confirmation",events:{"click ._btnClose":"closeModal"},initialize:function(){},render:function(){var e=this,t=this.renderTemplate({data:e.options.data});return $(e.el).html(t),this},closeModal:function(e){e.preventDefault();var t=this;t.options.eventManager.trigger("view:closeConfirmationModal")}}),ConfrimationModalView})