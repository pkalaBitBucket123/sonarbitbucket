/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","templateEngine","common/component/component","text!html/shopping/views/view.product.loading.dust"],function(e,t,n,r,i){return n.load(i,"view.product.loading"),ProductLoadingView=r.view.Base.extend({template:"view.product.loading",events:{},initialize:function(){},render:function(){var e=this,t=this.renderTemplate({data:{}});return $(e.el).html(t),this}}),ProductLoadingView})