/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){n.prototype.initialize.call(this,{storeItem:"quote"})}}),i=t.Model.extend(_.extend({hasQuotePlans:function(){return!_.isEmpty(this.get("plans"))},inQuote:function(e){var t=!1,n=this.toJSON();return n.hasOwnProperty("plans")&&(t=_.any(n.plans,function(t){if(t.planId==e)return!0})),t},togglePlanInclusion:function(e){var t=this.get("plans"),n=!1;for(var r=0;r<t.length&&!n;r++)t[r].planId==e&&(n=!0);n||t.push({planId:e});if(t.length>1)for(var r=0;r<t.length;r++)for(var i=r+1;i<t.length;)t[i].planId==t[r].planId?t.splice(t,i):i++;this.unset("plans",{silent:!0}),this.set("plans",t)}},new r));return i})