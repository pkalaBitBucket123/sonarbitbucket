/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){n.prototype.initialize.call(this,{storeItem:"account"})}}),i=t.Model.extend(_.extend({hasMembers:function(){return!_.isEmpty(this.get("members"))},inCart:function(e){var t=!1,n=this.toJSON();return n.hasOwnProperty("cart")&&n.cart.hasOwnProperty("productLines")&&_.each(n.cart.productLines,function(n){_.each(n.products,function(n){n.id===e&&(t=!0)})}),t},productLineInCart:function(e){var t=!1,n=this.toJSON();return n.hasOwnProperty("cart")&&n.cart.hasOwnProperty("productLines")&&_.each(n.cart.productLines,function(n,r){r===e&&!_.isEmpty(n)&&(t=!0)}),t},isCoverageSelected:function(e){var t=!1,n=this.get("members");return _.each(n,function(n){_.each(n.products,function(n){n.productLineCode==e&&(t=!0)})}),t}},new r));return i})