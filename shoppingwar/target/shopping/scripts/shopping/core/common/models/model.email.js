/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/application/proxy.model"],function(e,t,n){var r=n.extend({initialize:function(e){n.prototype.initialize.call(this,{requestKey:"email"})},validation:{toEmail:{required:!0,pattern:"email",maxLength:50},firstName:{required:!1,namePattern:!0,maxLength:25},lastName:{required:!1,namePattern:!0,maxLength:25},phone:{required:!1}},labels:{toEmail:"Email",email:"Email must be in the format xx@xx.xxx"}});return r})