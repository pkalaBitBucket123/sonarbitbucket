/**
 * Copyright (C) 2012 Connecture
 * 
 * This model represents the configuration around all of the workflows for a given application.
 * 
 */

define(["require","backbone"],function(e,t){var n=Backbone.Model.extend({initialize:function(){},indexOfWorkflowItem:function(e){var t=-1;return _.every(this.get("items"),function(n,r){if(_.isObject(n)){if(n.item===e){t=r;return}}else if(n===e){t=r;return}return!0}),t},getWorkflowItem:function(e){var t=null;return _.every(this.get("items"),function(n,r){if(_.isObject(n)){if(n.item===e){t=n;return}}else if(n===e){t=n;return}return!0}),_.isObject(t)||(retval={item:t,next:null,previous:null}),t},getWorkflowItemByAlias:function(e,t){var n=null,r=this;_.each(t,function(t){if(e==t.get("alias")){var i=t.get("id");_.every(r.get("items"),function(e){if(i===e.item){n=e;return}if(i===e){n=e;return}return!0})}});if(!_.isObject(n)){var i={item:n,next:null,previous:null};n=i}return n},getDefaultItem:function(){if(this.get("default"))return this.get("default");var e=_.first(this.get("items"));return _.isObject(e)&&(e=e.item),e}});return n})