/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","common/workflows/workflow.item","components/account/workflows/workflow.item.questions","components/account/workflows/workflow.item.whoscovered","components/account/workflows/workflow.item.summary"],function(e,t,n,r,i,s){return{createWidget:function(e,t,o,u){var a=null;return e==="QuestionsItem"?a=new r(t,o,u):e==="WhosCoveredItem"?a=new i(t,o,u):e==="Summary"?a=new s(t,o,u):a=new n(t,o,u),a}}})