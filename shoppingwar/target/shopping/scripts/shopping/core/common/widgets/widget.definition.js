/**
 * Copyright (C) 2012 Connecture
 * 
 */

define(["require","backbone","components/account/widgets/widget.configuredquestion","components/account/widgets/widget.providersearch","components/account/widgets/widget.costestimator","components/account/widgets/widget.method","components/account/widgets/widget.subsidy","components/account/widgets/widget.demographics","components/account/widgets/widget.specialenrollment","components/account/widgets/widget.enrollmentdate"],function(e,t,n,r,i,s,o,u,a,f){return{createWidget:function(e,t){var l=null;return e==="ProviderLookup"?l=new r(t):e==="CostEstimator"?l=new i(t):e==="Method"?l=new s(t):e==="SubsidyQuickCheck"?l=new o(t):e==="Demographics"?l=new u(t):e==="SpecialEnrollment"?l=new a(t):e==="EnrollmentDate"?l=new f(t):l=new n(t),l}}})