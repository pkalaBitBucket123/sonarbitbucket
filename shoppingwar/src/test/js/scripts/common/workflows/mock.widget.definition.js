/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'test/common/workflows/mock.widget', 'test/common/workflows/mock.widget.two' ], function(require, Backbone, MockWidget, MockWidgetTwo) {

	return {
		createWidget : function(widgetType, configuration) {
			var retVal = null;
			if (widgetType === "MockWidget1") {
				retVal = new MockWidget(configuration);
			} else if (widgetType === 'MockWidget2') {
				retVal = new MockWidgetTwo(configuration);
			}

			return retVal;
		}
	};
});
