/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'dust-helpers' ], function(require, Backbone, templateEngine, BaseView) {

	templateEngine.load("{@eq key=\"{renderProgressList}\" value=\"true\" type=\"boolean\" }" + "<ul>" + "{#wigetList currentWidgetId=currentWidgetId}" + "{@select key=\"{config.questionRefId}\"}"
			+ "{@eq value=\"{currentWidgetId}\"}" + "<li class=\"progBarList active\">" + "{:else}" + "<li class=\"progBarList\">" + "{/eq}" + "{/select}"
			+ "<a class=\"_question\" data-id=\"{config.questionRefId}\" href=\"javascript:void(0)\">{config.title}</a>" + "</li>" + "{/wigetList}" + "</ul>"
			+ "<a class=\"carousel-additional-info _skipToPlansLink\" href=\"javascript:void(0)\">Skip To Plans</a>" + "{/eq}", "view.workflow.item.widget.progress");

	var WorkflowItemView = BaseView.extend({
		template : "view.workflow.item.widget.progress",
		events : {
			"click .progBarList > ._question" : "questionSelected",
			"click ._skipToPlansLink" : "workflowSkipToPlans"
		},
		initialize : function(options) {
			BaseView.prototype.initialize.call(this);
		},
		render : function() {
			var wigetList = this.collection.toJSON();
			var progressData = {
				currentWidgetId : this.model.get('widgetId'),
				"wigetList" : wigetList,
				// FU dust
				'renderProgressList' : wigetList.length > 1
			};

			$(this.el).html(this.renderTemplate({
				"data" : progressData
			}));

			return this;
		},
		questionSelected : function(event) {
			var $selectedQuestion = $(event.currentTarget);

			var questionRefId = $selectedQuestion.attr("data-id");
			this.options.eventManager.trigger('workflow:item:sub:render:widget', questionRefId);
		},
		workflowSkipToPlans : function() {
			this.options.eventManager.trigger('widget:done', 'exit');
		}
	});

	return WorkflowItemView;
});