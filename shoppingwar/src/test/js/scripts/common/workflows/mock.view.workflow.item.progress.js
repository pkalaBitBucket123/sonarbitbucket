/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'dust-helpers' ], function(require, Backbone, templateEngine, BaseView) {

	templateEngine.load("<div class='loadable-nav'>" + "<a class='_questionnaireNavs' href='javascript:void(0)'>" + "{#progress enabledWorkflowSteps=enabledWorkflowSteps}" +

	"<a class='nav-action" + "{@select key='{currentWorkflowStep}'}" + "{@eq value='{id}'} active{/eq}" + "{/select}"
			+ "{@contains setKey='enabledWorkflowSteps' value='{id}'} enabled{:else} muted{/contains}" + "' data-id='{id}' href='{route}'>" +

			"<div class='action-title'>{label}</div>" + "</a>" +

			"{/progress}" + "</a>" + "</div>", 'view.workflow.item.progress');

	var WorkflowProgressView = BaseView.extend({
		template : "view.workflow.item.progress",
		events : {
			"click .loadable-nav .nav-action" : "navSelected"
		},
		initialize : function(options) {
			this.workflows = options.workflows;
			this.account = options.account;
			this.session = options.session;
			this.eventManager = options.eventManager;
			this.componentId = this.options.componentId;
		},
		render : function() {
			var self = this;

			// lol this is hardcoded for just questions, whoops fix when necessary I am lazy right now.
			var workflowMenu = [];
			_.each(this.workflows.get('Questions').toJSON().workflows, function(subworkflow) {
				if (!_.isEmpty(subworkflow.widgets)) {
					workflowMenu.push({
						id : subworkflow.id,
						label : subworkflow.config.title,
						route : '#view/' + self.componentId + '/Questions/' + subworkflow.widgets[0]
					});
				}
			});

			var data = {
				"progress" : workflowMenu,
				"currentWorkflowStep" : this.session.get("currentWorkflowStep"),
				"enabledWorkflowSteps" : this.session.get("enabledWorkflowSteps"),
			};

			$(self.el).html(this.renderTemplate({
				data : data
			}));

			return this;
		},
		navSelected : function(event) {

		}
	});

	return WorkflowProgressView;

});