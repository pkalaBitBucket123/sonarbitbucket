/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: WIP
 */
requirejs.config({
	map : {
		'*' : {

			'widget/definition' : 'test/common/workflows/mock.widget.definition',
			'common/workflows/view.workflow.item' : 'test/common/workflows/mock.view.workflow.item',
			'common/workflows/view.workflow.item.progress' : 'test/common/workflows/mock.view.workflow.item.actions',
			'common/workflows/view.workflow.item.actions' : 'test/common/workflows/mock.view.workflow.item.actions',
			'common/workflows/view.workflow.item.widget.progress' : 'test/common/workflows/mock.view.workflow.item.widget.progress'
		}
	}
});

define([ 'require', 'common/workflows/workflow.item', 'shopping/common/collections/collection.widget', 'backbone', 'underscore', 'sinon' ], function(require, WorkflowItem, WidgetList, Backbone, _,
		sinon) {
	module('workflow.item', {
		setup : function() {

			this.eventManager = _.extend({}, Backbone.Events);

			this.models = {};

			this.models.applicationConfig = new Backbone.Model({

				"applicationConfig" : {
					"messages-url" : "http://www.google.com",
					"my-account-url" : "http://www.connecture.com",
					"roster-url" : "http://www.bing.com",
					"sbc-base-url" : "http://www.yahoo.com",
					"brochure-base-url" : "http://www.cnn.com",
					"emailFormPhoneVisible" : true,
					"emailFormPhoneRequired" : false,
					"emailFormFirstNameVisible" : true,
					"emailFormFirstNameRequired" : true,
					"emailFormLastNameVisible" : true,
					"emailFormLastNameRequired" : true,
					"ffmEnabled" : true,
					"ffmPriorToApplyURL" : "http://www.healthcare.gov/law/resources/regulations/guidance-to-states-on-exchanges.html"
				},
				"widgets" : [ {
					"widgetId" : "MockWidget1",
					"widgetType" : "MockWidget1",
					"config" : {
						"workflows" : [ {
							"workflowRefId" : "WF1",
							"title" : "Guide Me",
							"infoBlock" : "I'd like to answer a few lifestyle and budget questions so that you can help guide me to the right plan for 2013.",
							"classOverride" : "half-section",
							"defaultWorkflow" : true,
							"questionRefIds" : [ "Q1", "Q2", "Q4", "Q5", "Q6" ]
						}, {
							"workflowRefId" : "WF2",
							"title" : "Browse All Plans",
							"infoBlock" : "I'd just like to view all your available plans and determine the right plan myself for 2013.",
							"classOverride" : "half-section shop-for-plans",
							"defaultWorkflow" : false,
							"questionRefIds" : []
						} ]
					}
				} ],
				"workflowItems" : [],
				"workflow" : {

				}
			});

			this.widgets = new WidgetList([ {
				"widgetId" : "MockWidget1",
				"widgetType" : "MockWidget1",
				"config" : {
					"workflows" : [ {
						"workflowRefId" : "WF1",
						"title" : "Guide Me",
						"infoBlock" : "I'd like to answer a few lifestyle and budget questions so that you can help guide me to the right plan for 2013.",
						"classOverride" : "half-section",
						"defaultWorkflow" : true,
						"questionRefIds" : [ "Q1", "Q2", "Q4", "Q5", "Q6" ]
					}, {
						"workflowRefId" : "WF2",
						"title" : "Browse All Plans",
						"infoBlock" : "I'd just like to view all your available plans and determine the right plan myself for 2013.",
						"classOverride" : "half-section shop-for-plans",
						"defaultWorkflow" : false,
						"questionRefIds" : []
					} ]
				}
			}, {
				"widgetId" : "MockWidget2",
				"widgetType" : "MockWidget2",
				"config" : {
					"workflows" : [ {
						"workflowRefId" : "WF1",
						"title" : "Guide Me",
						"infoBlock" : "I'd like to answer a few lifestyle and budget questions so that you can help guide me to the right plan for 2013.",
						"classOverride" : "half-section",
						"defaultWorkflow" : true,
						"questionRefIds" : [ "Q1", "Q2", "Q4", "Q5", "Q6" ]
					}, {
						"workflowRefId" : "WF2",
						"title" : "Browse All Plans",
						"infoBlock" : "I'd just like to view all your available plans and determine the right plan myself for 2013.",
						"classOverride" : "half-section shop-for-plans",
						"defaultWorkflow" : false,
						"questionRefIds" : []
					} ]
				}
			} ]);

		},
		teardown : function() {

		}
	});

	// Test create
	test('create', function() {
		var workflow = new WorkflowItem(new Backbone.Model({
			id : "Method",
			label : "Method",
			widgets : [ 'MockWidget1' ],
			workflows : [],
			history : {
				trigger : true,
				root : ''
			},
			megaMenu : {
				enabled : true
			},
			allowRenderMultipleWidgets : false,
			showNavigation : false
		}), this.widgets, {
			models : this.models,
			eventManager : this.eventManager,
			session : this.session,
			history : {
				id : this.id
			},
			view : {
				el : 'body'
			}
		});

		equal(typeof workflow, 'object', 'Object created');

		ok(workflow.regionManager.content, 'Has content region');
		ok(workflow.regionManager.actions, 'Has action region');
		ok(workflow.regionManager.widgetProgress, 'Has wdiget progress region');
		ok(workflow.regionManager.workflowProgress, 'Has workflow progress region');
	});

	// Test one widget workflow
	test('workflowOneWdiget', function() {

		var workflow = new WorkflowItem(new Backbone.Model({
			id : "Method",
			label : "Method",
			widgets : [ 'MockWidget1' ],
			workflows : [],
			history : {
				trigger : true,
				root : ''
			},
			megaMenu : {
				enabled : true
			},
			allowRenderMultipleWidgets : false,
			showNavigation : false
		}), this.widgets, {
			models : this.models,
			eventManager : this.eventManager,
			session : this.session,
			history : {
				id : this.id
			},
			view : {
				el : ''
			}
		});

		var workflowItemStartedSpy = sinon.spy();
		this.eventManager.bind('workflow:item:started', workflowItemStartedSpy);
		var workflowItemFinishedSpy = sinon.spy();
		this.eventManager.bind('workflow:item:finished', workflowItemFinishedSpy);

		workflow.start();

		equal(typeof workflow, 'object', 'Object created');
		equal(workflow.activeWidgets.length, 1, 'Has correct number of active widgets');
		// grab the regionManager content and make sure it outputs it correctly
		equal($(workflow.regionManager.content.currentView.$el).html(), '<div id="MockWidget1View"><span>Fun Widget</span></div>', 'Main content region has correct HTML');
		notEqual(workflow.primaryWidget, null, 'A primary widget was set.');
		equal(workflow.primaryWidget.isPrimary, true, 'The widget\'s primary flag was set to true.');

		ok(workflowItemStartedSpy.calledOnce, "A workflow item start event callback was correctly triggered");

		ok(workflowItemFinishedSpy.calledOnce, "A workflow item finish event callback was correctly triggered");

		console.log();
	});

	// Test one widget with different regions
	test('workflowOneWdigetDifferentRegions', function() {

		var workflow = new WorkflowItem(new Backbone.Model({
			id : "Method",
			label : "Method",
			widgets : [ 'MockWidget1' ],
			workflows : [],
			history : {
				trigger : true,
				root : ''
			},
			megaMenu : {
				enabled : true
			},
			allowRenderMultipleWidgets : false,
			showNavigation : false,
			regions : {
				'fancySauce' : {
					el : '._workflowContent',
					primary : true,
					widgetRegion : true
				}
			}
		}), this.widgets, {
			models : this.models,
			eventManager : this.eventManager,
			session : this.session,
			history : {
				id : this.id
			},
			view : {
				el : ''
			}
		});

		workflow.start();

		ok(workflow.regionManager.fancySauce, 'Has content region');
		notStrictEqual(workflow.regionManager.actions, 'undefined', 'Doesn\'t have default actions region.');
		notStrictEqual(workflow.regionManager.widgetProgress, 'undefined', 'Doesn\'t have default widgetProgress region.');
		notStrictEqual(workflow.regionManager.workflowProgress, 'undefined', 'Doesn\'t have default workflowProgress region.');

		// grab the regionManager content and make sure it outputs it correctly
		equal($(workflow.regionManager.fancySauce.currentView.$el).html(), '<div id="MockWidget1View"><span>Fun Widget</span></div>', 'Main fancySauce region has correct HTML');

		console.log();
	});

	// Test multiple widget workflow, only allow one to be active at a time

	// Test miltiple widget workflow, with multiple active
});