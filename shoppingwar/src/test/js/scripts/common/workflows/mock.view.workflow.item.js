/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to end up being the frame from the workflow item
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'dust-helpers' ], function(require, Backbone, templateEngine, BaseView) {

	templateEngine.load("<div class='_workflowProgress'></div>" + "<div class='content-section carousel-container'>" + "<div class='carousel-progress _workflowWidgetProgress'></div>"
			+ "<div class='full-section _workflowContent'>" + "</div>" + "<div class='_workflowActions'>" + "</div>" + +"</div>", 'view.workflow.template');

	var WorkflowItemView = BaseView.extend({
		className : 'questionnaire _componentView',
		template : "view.workflow.template",
		events : {

		},
		initialize : function(options) {
			connecture.component.core.View.prototype.initialize.call(this);
		},
		render : function() {
			var self = this;
			// render frame
			var viewHTML = this.renderTemplate();
			$(self.el).append(viewHTML);

			return this;
		}
	});

	return WorkflowItemView;
});