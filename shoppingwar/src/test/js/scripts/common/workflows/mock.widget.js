/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'common/views/base' ], function(require, Backbone, Widget, BaseView) {

	var MockWidgetView = BaseView.extend({
		initialize : function(options) {
			this.eventManager = options.eventManager;
		},
		events : {

		},
		render : function() {
			$(this.el).html('<div id="MockWidget1View"><span>Fun Widget</span></div>');

			return this;
		},
		close : function() {

		}
	});

	var MethodWidget = function(options) {
		Widget.call(this, options);
	};

	MethodWidget.prototype = Object.create(Widget.prototype);

	MethodWidget.prototype.start = function() {
		this.view = new MockWidgetView({
			eventManager : this.eventManager
		});
	};

	MethodWidget.prototype.getView = function() {
		return this.view;
	};

	MethodWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return MethodWidget;
});