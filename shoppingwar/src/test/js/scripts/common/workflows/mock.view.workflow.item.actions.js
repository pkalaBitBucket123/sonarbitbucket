/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'dust-helpers' ], function(require, Backbone, templateEngine, BaseView) {

	templateEngine.load("<div class='paging-section'>" + "<div class='paging-buttons'>" +

	"<div class='_nextQuestion'>" + "{#isLastWidget}" + "<a class='btn_primary btn-next' href='javascript:void(0)'>Finish & See Results</a>" + "{:else}"
			+ "<a class='btn_primary btn-next' href='javascript:void(0)'>Save & Continue</a>" + "{/isLastWidget}" + "</div>" +

			"<div class='_prevQuestion'>" + "{#hasPrevQuestion}" + "<a class='paging-link btn-prev' href='javascript:void(0)'>Previous</a>" + "{:else}"
			+ "<a class='paging-link btn-prev' href='javascript:void(0)'>Previous</a>" + "{/hasPrevQuestion}" + "</div>" +

			"<div class='_clearQuestion'>" + "<a class='paging-link btn-clear' href='javascript:void(0)'>Clear</a>" + "</div>" + "{#isLastWidget}" + "{:else}" + "<div class='_skipQuestion'>"
			+ "<a class='btn-utility btn-skip' href='javascript:void(0)'>Skip Question</a>" + "</div>	" + "{/isLastWidget}" + "</div>" + "</div>", "view.workflow.item.actions");

	var WorkflowItemView = BaseView.extend({
		template : "view.workflow.item.actions",
		events : {
			"click .paging-section .paging-buttons ._prevQuestion a" : "workflowPrevQuestion",
			"click .paging-section .paging-buttons ._clearQuestion a" : "workflowClearQuestion",
			"click .paging-section .paging-buttons ._skipQuestion a" : "workflowSkipQuestion",
			"click .paging-section .paging-buttons ._nextQuestion a" : "workflowNextQuestion",
		},
		initialize : function(options) {
			connecture.component.core.View.prototype.initialize.call(this);
			this.isLastWidget = options.isLastWidget;
		},
		render : function() {
			$(this.el).html(this.renderTemplate({
				"data" : {
					hasPrevQuestion : this.hasPrevWorkflowWidget(),
					isLastWidget : this.isLastWidget
				}
			}));

			return this;
		},
		hasPrevWorkflowWidget : function() {
			return this.isPrevWidget() || this.collection.first().get('widgetId') !== this.model.get('widgetId');
		},
		isPrevWidget : function() {
			var indexOfCurrentWidget = this.collection.indexOf(this.model);

			if (indexOfCurrentWidget <= 0) {
				return false;
			} else {
				return true;
			}
		},
		workflowPrevQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:previous:widget', event);
		},
		workflowNextQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:next:widget', event, true);
		},
		workflowSkipQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:next:widget', event);
		},
		workflowClearQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:widget:clear');
		},
	});

	return WorkflowItemView;
});