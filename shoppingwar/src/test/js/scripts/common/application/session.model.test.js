define([ 'require', 'common/application/session.model', 'common/backbone/backbone.sync' ], function(require, SessionModel, sync) {
	module('session.model', {
		setup : function() {
			this.session = new SessionModel({
				id : '1'
			});
		},
		teardown : function() {
			this.session.clear();
		}
	});

	test('set', function() {
		this.session.set({
			hello : 'goodbye'
		});

		equal(this.session.get('hello'), 'goodbye', 'set');
	});

	test('sync', function() {
		this.session.set({
			hello : 'goodbye'
		});

		this.session.save();

		equal(this.session.get('hello'), 'goodbye', 'sync');
	});

	test('clear', function() {
		this.session.set({
			space : 'fun'
		});

		this.session.clear();

		equal(this.session.get('space'), undefined, 'clear');

	});

});