define([ 'require', 'backbone', 'backbone.validation',
              'shopping/individual/components/account/models/model.demographics.form',
              'sinon'], 
              function(require, Backbone, BackboneValidation, DemographicsFormModel, Sinon) {
	
   Backbone.Validation.configure({
          labelFormatter : 'label'
   });
   var formatFunctions = {
        formatLabel: function(attrName, model) {
        return defaultLabelFormatters[defaultOptions.labelFormatter](attrName, model);
   },
  
 
    format: function() {
      var args = Array.prototype.slice.call(arguments),
      text = args.shift();
      model.set('error',text);
      return text.replace(/\{(\d+)\}/g, function(match, number) {
    	  return typeof args[number] !== 'undefined' ? args[number] : match;
       });
     }
   };

    var defaultLabelFormatters = Backbone.Validation.labelFormatters = {
  
    
      none: function(attrName) {
        return attrName;
      },
  
      
      sentenceCase: function(attrName) {
        return attrName.replace(/(?:^\w|[A-Z]|\b\w)/g, function(match, index) {
          return index === 0 ? match.toUpperCase() : ' ' + match.toLowerCase();
        }).replace('_', ' ');
      },
     
      label: function(attrName, model) {
        return (model.labels && model.labels[attrName]) || defaultLabelFormatters.sentenceCase(attrName, model);
      }
    };

    var temp = _.extend(BackboneValidation.validators, formatFunctions, {validations : function(value, attr, customValue, model) {}});
    var demographicsFormModel;
	var value = '01/01/2014';
	var attr = new Backbone.Model();
	var customValue = new Backbone.Model();
	var model = new Backbone.Model();
	var demographiFormModel;
    module('model.demographics.form', {
       setup : function() {
    	   this.demographicsFormModel = new DemographicsFormModel();
	       var temp1 = temp;
	       Sinon.stub(temp, "formatLabel");
       },
       teardown : function() {
    	   temp.formatLabel.restore();
    	   model.unset("isInternalUser");
   		
       }
    });
   
    /* Test cases for SHSBCBSNE-1300: Begin */
    /**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Internal user selects Tempcare insurance start date of more than 60 days in future.
     * Expectation: User should get validation error that date must be between today and 60 days in the future.
     */
	test('dateNotMore90Days_STH', function() {
		 model.set('isInternalUser',true);
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 61);
		 $('input[name="coverageType"]').val('STH');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		notEqual(retValue, undefined);
		deepEqual(retValue, "Insurance Start Date must be between today and 60 days in the future." );
	});
	
	/**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Internal user selects Dental insurance start date of more than 60 but less than 90 days in future.
     * Expectation: User should not get any validation error.
     */
	test('dateNotMore90Days_DEN', function() {
		 model.set('isInternalUser',true);
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 61);
		 $('input[name="coverageType"]').val('DEN');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		deepEqual(retValue, undefined);
	});
	
	 /**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Internal user selects Dental insurance start date of more than 90 days in future.
     * Expectation: User should get validation error that date must be between today and 90 days in the future.
     */
	test('dateNotMore90Days_DEN90', function() {
		 model.set('isInternalUser',true);
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 91);
		 $('input[name="coverageType"]').val('DEN');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		notEqual(retValue, undefined);
		deepEqual(retValue, "Insurance Start Date must be between today and 90 days in the future." );
	});
	
	/**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Anonymous user or consumer selects Tempcare insurance start date of more than 60 but less than 90 days in future.
     * Expectation: User should not get any validation error.
     */
	test('dateNotMore90Days_STH60_consumer', function() {
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 61);
		 $('input[name="coverageType"]').val('STH');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		notEqual(retValue, undefined);
		deepEqual(retValue, "Insurance Start Date must be between today and 60 days in the future." );
	});
	
	/**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Anonymous user or consumer selects Tempcare insurance start date of more than 90 days in future.
     * Expectation: User should get validation error that date must be between today and 90 days in the future.
     */
	test('dateNotMore90Days_STH90_consumer', function() {
		
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 91);
		 $('input[name="coverageType"]').val('STH');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		deepEqual(retValue, "Insurance Start Date must be between today and 60 days in the future." );
	});
	
	/**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Anonymous user or consumer selects Dental insurance start date of more than 60 but less than 90 days in future.
     * Expectation: User should not get any validation error.
     */
	test('dateNotMore90Days_DEN_consumer', function() {
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 61);
		 $('input[name="coverageType"]').val('DEN');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		deepEqual(retValue, undefined);
	});
	
	/**Test Method: dateNotMore90Days(value, attr, customValue, model)
     * Scenario : Anonymous user or consumer selects Dental insurance start date of more than 90 days in future.
     * Expectation: User should get validation error that date must be between today and 90 days in the future.
     */
	test('dateNotMore90Days_DEN90_consumer', function() {
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + 91);
		 $('input[name="coverageType"]').val('DEN');
		 $("#effectiveDate").val(todayDate);
		var retValue = temp.dateNotMore90Days(value, attr, customValue, model);
		notEqual(retValue, undefined);
		deepEqual(retValue, "Insurance Start Date must be between today and 90 days in the future." );
	});
	
	/*NE:Change SHSBCBSNE-1300: End*/
});
