
/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine','shopping/individual/components/account/widgets/views/view.widget.demographics',
'shopping/core/common/models/model.account', 'shopping/core/common/models/model.application.config',
'text!html/individual/components/account/view.whoscovered.dust',
		'text!html/individual/components/account/view.whoscovered.counties.dust','jquery.maskedInput','jquery-ui' ], function(require, Backbone,templateEngine, WhosCoveredView, 
		AccountModel, DemographicsConfig, template, countiesTemplate, maskedInput ,jqueryinput) {
    templateEngine.load(template, "view.whoscovered");
	templateEngine.load(countiesTemplate, "view.whoscovered.counties");

	var AccountModel = Backbone.Model.extend({});
	var collection = Backbone.Collection.extend({});
	module('view.widget.demographics', {
		setup : function() {
         var accountModel = new AccountModel({
			"eventDate": '10/10/2015',
			"eventType": "birth",
			"effectiveMs" :'01/01/2014',
			"coverageDate" : '01/01/2014',
			effectiveDate1 : '05/01/2014',
			"members" :{
			"relationshipKey" : "PRIMARY",
			}
			});
			
			var coll = new collection({
			"member0" : 'member0',
			});
			applicationConfig = new DemographicsConfig({
			"quotingEnabled" : 'true'
			});
			
			 var demographicsData = {effectiveMs:'NHK',currentMembers : coll };
		    var demographicsConfig = new DemographicsConfig({
			"effectiveMs" :'01/01/2014',
				});
		    var actor = new AccountModel({});
			 var options = {eventManager: _.extend({}, Backbone.Events), account: accountModel, actor : actor, demographicsConfig: demographicsConfig, applicationConfig: applicationConfig,demographicsData:demographicsData};
		     this.view =  new WhosCoveredView(options);
			
		},
		teardown : function() {
	   		
        }
	});
	
	test('render', function() {
		this.view.demographicsConfig  = new DemographicsConfig({
				});
		this.view.demographicsConfig.demographicFields = '';
		this.view.render();
		equal(this.view.model.get("effectiveDate1"),'05/01/2014',"dates are equal");
		notDeepEqual( $('#effectiveDateWL', this.el), null, "effectiveDateWL is not null" );
		
		
	});
	
	
	/**
	 * Method under test: updateEffectiveDate()   
	 * Scenario: SE reason LossOfCovg chosen with event date as current date 
	 * Expectation: return value of updateEffectiveDate will be same as output of getExpectedEffectiveDate taking currentDate as input 
	 *   
	 */
	test('updateEffectiveDate_LossOfCovg_currentDate', function() {
		var now = new Date();
		now.setHours(0,0,0,0);
		selectedDate = new Date();
		selectedDate.setHours(0,0,0,0);
		selectedDateString = getDateString(selectedDate);
		expectedEffectiveDate = getExpectedEffectiveDate(now,selectedDate);
		this.view.demographicsData = {eventType:'LossOfCovg', eventDate:selectedDate};
		var effectiveDate = this.view.updateEffectiveDate();
		equal(effectiveDate,expectedEffectiveDate,"effective date on demographics equals expected effective date when event date is current date");
	});
	
	/**
	 * Method under test: updateEffectiveDate()   
	 * Scenario: SE reason LossOfCovg chosen with event date as next month date 
	 * Expectation: return value of updateEffectiveDate will be same as output of getExpectedEffectiveDate taking nextMonthDate as input
	 *   
	 */
	test('updateEffectiveDate_LossOfCovg_nextMonthDate', function() {
		var now = new Date();
		now.setHours(0,0,0,0);
		selectedDate = new Date();
		selectedDate.setHours(0,0,0,0);
		selectedDate.setMonth(now.getMonth()+1);
		selectedDateString = getDateString(selectedDate);
		expectedEffectiveDate = getExpectedEffectiveDate(now,selectedDate);
		this.view.demographicsData = {eventType:'LossOfCovg', eventDate:selectedDateString};
		var effectiveDate = this.view.updateEffectiveDate();
		equal(effectiveDate,expectedEffectiveDate,"effective date on demographics equals expected effective date when event date is of next month");
	});
	
	/**
	 * Method under test: updateEffectiveDate()   
	 * Scenario: SE reason LossOfCovg chosen with event date as next date 
	 * Expectation: return value of updateEffectiveDate will be same as output of getExpectedEffectiveDate taking 59Day in Future as input
	 *   
	 */
	test('updateEffectiveDate_LossOfCovg_FutureDate', function() {
		var now = new Date();		
		now.setHours(0,0,0,0);
		selectedDate = new Date();
		selectedDate.setDate(selectedDate.getDate()+1);
		selectedDate.setHours(0,0,0,0);
		selectedDateString = getDateString(selectedDate);
		expectedEffectiveDate = getExpectedEffectiveDate(now,selectedDate);
		this.view.demographicsData = {eventType:'LossOfCovg', eventDate:selectedDateString};
		var effectiveDate = this.view.updateEffectiveDate();
		equal(effectiveDate,expectedEffectiveDate,"effective date on demographics equals expected effective date when event date is one day in future");
	});
	
	/**
	 * Method under test: updateEffectiveDate()   
	 * Scenario: SE reason LossOfCovg chosen with event date as previous date 
	 * Expectation: return value of updateEffectiveDate will be same as output of getExpectedEffectiveDate taking 59Day in Future as input
	 *   
	 */
	test('updateEffectiveDate_LossOfCovg_PastDate', function() {
		var now = new Date();		
		now.setHours(0,0,0,0);
		selectedDate = new Date();
		selectedDate.setDate(selectedDate.getDate()-1);
		selectedDate.setHours(0,0,0,0);
		selectedDateString = getDateString(selectedDate);
		expectedEffectiveDate = getExpectedEffectiveDate(now,selectedDate);
		this.view.demographicsData = {eventType:'LossOfCovg', eventDate:selectedDateString};
		var effectiveDate = this.view.updateEffectiveDate();
		equal(effectiveDate,expectedEffectiveDate,"effective date on demographics equals expected effective date when event date is one day in past");
	});
	
	/**
	 * Method under test: updateEffectiveDate()   
	 * Scenario: SE reason LossOfCovg chosen with event date as 59th day in future 
	 * Expectation: return value of updateEffectiveDate will be same as output of getExpectedEffectiveDate taking 59Day in Future as input
	 *   
	 */
	test('updateEffectiveDate_LossOfCovg_59thFutureDate', function() {
		var now = new Date();		
		now.setHours(0,0,0,0);
		selectedDate = new Date();
		selectedDate.setDate(selectedDate.getDate()+59);
		selectedDate.setHours(0,0,0,0);
		selectedDateString = getDateString(selectedDate);
		expectedEffectiveDate = getExpectedEffectiveDate(now,selectedDate);
		this.view.demographicsData = {eventType:'LossOfCovg', eventDate:selectedDateString};
		var effectiveDate = this.view.updateEffectiveDate();
		equal(effectiveDate,expectedEffectiveDate,"effective date on demographics equals expected effective date when event date is 59th date in future");
	});
	
	/**
	 * Method under test: updateEffectiveDate()   
	 * Scenario: SE reason LossOfCovg chosen with event date as 59th day in future 
	 * Expectation: return value of updateEffectiveDate will be same as output of getExpectedEffectiveDate taking 59Day in Future as input
	 *   
	 */
	test('updateEffectiveDate_LossOfCovg_59thPastDate', function() {
		var now = new Date();		
		now.setHours(0,0,0,0);
		selectedDate = new Date();
		selectedDate.setDate(selectedDate.getDate()-59);
		selectedDate.setHours(0,0,0,0);
		selectedDateString = getDateString(selectedDate);
		expectedEffectiveDate = getExpectedEffectiveDate(now,selectedDate);
		this.view.demographicsData = {eventType:'LossOfCovg', eventDate:selectedDateString};
		var effectiveDate = this.view.updateEffectiveDate();
		equal(effectiveDate,expectedEffectiveDate,"effective date on demographics equals expected effective date when event date is 59th date in past");
	});
	/*
	 * utility method of this test file returning string representation of date as passed as input
	 */
	function getDateString(date) {
		var yyyy = date.getFullYear().toString();
		var mm = (date.getMonth()+1).toString(); 
		var dd  = date.getDate().toString();
		var dateStr = (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]) + "/" + yyyy ;
		return dateStr;
	};
	
	/*
	 * utility method of this test file returning the expected effective date when passed current date and event date as input
	 */
	function getExpectedEffectiveDate(date, selectedDate){
		
		if( selectedDate > date) {
			var effDateBasedOnEnrollDate = new Date();
			var date = effDateBasedOnEnrollDate.getDate();
			effDateBasedOnEnrollDate.setDate(1);
				var month = selectedDate.getMonth();
				var year = selectedDate.getFullYear();
			    if (month == 11)
			    {
			    	month = -1
			    	year = year + 1
			    }
			    effDateBasedOnEnrollDate.setMonth(month + 1);
			    effDateBasedOnEnrollDate.setYear(year);
			    date = effDateBasedOnEnrollDate;
		}else{
			var resultDate = date;
			resultDate.setDate(1);
			resultDate.setMonth(resultDate.getMonth() + 1);
			date = resultDate;
		}
		return getDateString(date);
	};
});

