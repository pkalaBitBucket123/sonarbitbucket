define([ 'require', 'backbone', 'templateEngine', 'shopping/individual/components/loader/views/view.loader.summary','shopping/common/collections/collection.widget' ], function(require, Backbone,
		templateEngine, BaseLoaderSummaryView, WidgetList) {
	
	
	
	var ProgressNavigationModel = Backbone.Model.extend({});
	var AccountModel = Backbone.Model.extend({});
	var plans = Backbone.Model.extend({});
	
	
	
	
	module('view.loader.summary', {
		setup : function() {
			this.view = new ProgressNavigationModel({
				id : '123',
				name : 'Plan 1',
				benefitCategories : [],
				
			});
			this.view.widgets = new WidgetList([ {
				"widgetId" : "Demographics",
				"widgetType" : "MockWidget1",
				"config" : {
				"effectiveDate" : "01/01/2014",
				"daysLeftToEnroll" : "22",
					"workflows" : [ {
						"workflowRefId" : "WF1",
						"title" : "Guide Me",
						"infoBlock" : "I'd like to answer a few lifestyle and budget questions so that you can help guide me to the right plan for 2013.",
						"classOverride" : "half-section",
						"defaultWorkflow" : true,
						"questionRefIds" : [ "Q1", "Q2", "Q4", "Q5", "Q6" ]
					}, {
						"workflowRefId" : "WF2",
						"title" : "Browse All Plans",
						"infoBlock" : "I'd just like to view all your available plans and determine the right plan myself for 2013.",
						"classOverride" : "half-section shop-for-plans",
						"defaultWorkflow" : false,
						"questionRefIds" : []
					} ]
				}
			}
		 ]);
		
		this.view.account = new AccountModel({
			coverageType : "ID3",
			endDateOE : '02/15/2015',
			startDateOE : '11/15/2014',
			
		});
			this.view = new BaseLoaderSummaryView({
				models : this.view
			});
			
			
	},
	
		teardown : function() {

		}
	});
	
	test('initialize', function() {
			var a = this.view.initialize(this.view.options);
			equal(this.view.data.formattedEffectiveDate == "Jan 1, 2014",true);
	});
		
	test('initialize_checkNoOfDaysLeftToEnroll', function() {
		    this.view.data.daysLeft = 22;
			this.view.initialize(this.view.options);
			var startDateOE = new Date(this.view.options.models.account.get("startDateOE"));
			var endDateOE = new Date(this.view.options.models.account.get("endDateOE"));
			var today = new Date();
			today.setHours(0,0,0,0);
			var oneDay = 24*60*60*1000;
			if(today>=startDateOE && today<= endDateOE){
				equal(Math.round(Math.abs((endDateOE.getTime()-today.getTime())/(oneDay))), this.view.data.daysLeft);
			}else{
				equal(22, this.view.data.daysLeft);	
			}
	});
});