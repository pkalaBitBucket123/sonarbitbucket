
define([ 'require', 'backbone', 'components/list/views/view.product.summary', 'common/application/application' ], function(require, Backbone, SummaryView) {
	var SummaryModel = Backbone.Model.extend({});

	module('view.product.summary', {
		setup : function() {
			var summaryModel = new SummaryModel({
				id : '123',
				name : 'Plan 1',
				benefitCategories : []
			});

			this.view = new SummaryView({
				model : summaryModel
			});
		},
		teardown : function() {

		}
	});

	test('render', function() {
		var $el = this.view.render().$el;
		var name = $el.find('._planName').text();
		equal($el.length, 1);
	});
});