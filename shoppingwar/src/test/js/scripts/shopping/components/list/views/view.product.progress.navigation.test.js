define([ 'require', 'backbone', 'components/list/views/view.product.progress.navigation' ], function(require, Backbone, productNavigation) {
	
	
	var SummaryModel = Backbone.Model.extend({});
	var AccountModel = Backbone.Model.extend({});
	
	module('view.product.progress.navigation', {
		setup : function() {
			this.view = new SummaryModel({
				
			});

			this.view = new productNavigation({
				model : this.view,
				eventManager : _.extend({}, Backbone.Events),
				account :  new AccountModel({
					"tagUrl" : "hgjh",
				}),
				
			});
			
		},
		teardown : function() {
		}
	});
	test('initialize', function() {
		var a  = this.view.initialize(this.view);
		ok(true, a);
	});
	
});