define(
		[ 'require', 'backbone', 'templateEngine',
				'common/component/component',
				'shopping/core/components/loader/views/view.loader.summary',
				'shopping/common/collections/collection.widget' ],
		function(require, Backbone, templateEngine, connecture,
				BaseLoaderSummaryView, WidgetList) {

			var ProgressNavigationModel = Backbone.Model.extend({});
			var AccountModel = Backbone.Model.extend({});

			module('view.loader.summary', {
				setup : function() {

					this.view = new ProgressNavigationModel({});
					this.view.account = new AccountModel({
						effectiveDate : "02/01/2015",
						startDateOE : '11/15/2014',
						endDateOE : '02/15/2015',
						userRole : 'fbbroker',
					});
					this.view = new BaseLoaderSummaryView({
						models : this.view,
						eventManager : _.extend({}, Backbone.Events)
					});
				},

				teardown : function() {

				}
			});

			test(
					'initialize_specialEnroll_btn_click_fbbrokerLogin',
					function() {
						$("#qunit-fixture")
								.append(
										'<a class="_specialEnrollBtn _denCoverage" name="specialEnroll" id="specialEnroll" href="javascript:void(0);">special enrollment</a>');
						$("a[name=specialEnroll]").trigger('click',
								this.view.initialize(this.view.options));
						equal(this.view.models.account.has("specialEnroll"),
								false,
								"specialEnroll not set in model's account. farm bureau broker stopped");
					});

			test(
					'initialize_specialEnroll_btn_click_bobbrokerLogin',
					function() {
						this.view.options.models.account.set("userRole",
								"bobbroker");
						$("#qunit-fixture")
								.append(
										'<a class="_specialEnrollBtn _denCoverage" name="specialEnroll" id="specialEnroll" href="javascript:void(0);">special enrollment</a>');
						$("a[name=specialEnroll]").trigger('click',
								this.view.initialize(this.view.options));
						equal(this.view.models.account.has("specialEnroll"),
								true,
								"specialEnroll set in model's account. broker allowed");
					});

			test(
					'initialize_specialEnroll_btn_click_anonymousLogin',
					function() {
						this.view.options.models.account.set("userRole",
								"anonymous");
						$("#qunit-fixture")
								.append(
										'<a class="_specialEnrollBtn _denCoverage" name="specialEnroll" id="specialEnroll" href="javascript:void(0);">special enrollment</a>');
						$("a[name=specialEnroll]").trigger('click',
								this.view.initialize(this.view.options));
						equal(this.view.models.account.has("specialEnroll"),
								true,
								"specialEnroll set in model's account. anonymous user allowed");
					});
		});