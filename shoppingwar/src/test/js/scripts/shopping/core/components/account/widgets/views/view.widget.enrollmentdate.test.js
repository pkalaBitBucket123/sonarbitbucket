/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/component/component',
		'components/account/widgets/views/view.widget.enrollmentdate',
		'shopping/core/common/models/model.application.config' ], function(
		require, Backbone, connecture, ProgressNavigation, DemographicsConfig) {

	var AccountModel = Backbone.Model.extend({});

	module('view.widget.enrollmentdate', {
		setup : function() {

			var demographicsConfig = new DemographicsConfig({
				"effectiveMs" : '01/01/2014',
				"config" : {
					"effectiveDate" : "01/01/2014",
					"daysLeftToEnroll" : "22",
				},
			});

			var demographicsData = {
				effectiveDate : '10/01/2014'
			};

			this.view = new ProgressNavigation({
				model : this.view,
				eventManager : _.extend({}, Backbone.Events),
				demographicsConfig : demographicsConfig,
				demographicsData : demographicsData,

			});

			this.view.account = new AccountModel({
				coverageType : "ID3",

			});

		},

		teardown : function() {

		}
	});

	test('render', function() {
		this.view.render();
		equal(22, this.view.demographicsConfig.daysLeftToEnroll)
	});
	
	test('render_SE', function() {
		this.view.demographicsData =   {"eventDate" : '11/15/2014', "eventType" : 'Birth'} ;
		this.view.render();
		equal(22, this.view.demographicsConfig.daysLeftToEnroll)
	});

});
