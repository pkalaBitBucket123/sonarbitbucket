/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/views/base', 'components/list/views/view.product.base'], function(
		require, Backbone, BaseView, BaseProductView) {

//	var BaseProductView = Backbone.Model.extend({});
	var AccountModel = Backbone.Model.extend({});
	var OptionsModel = Backbone.Model.extend({});
	var modelJSON = Backbone.Model.extend({});
	
	module('view.account.progress.navigation', {
		setup : function() {

			this.view = new BaseProductView({
				options : new OptionsModel({
					account : new AccountModel({
						"coverageType" : "STH"
					})
				})
			});
			
			this.data = {coverageType : ''};
		},
		teardown : function() {
			
		}
	});
	
	test('setDataCoverageType', function() {
		this.view.setDataCoverageType(this.view.options.options.get("account"),this.data);
		ok(this.data.coverageType =="STH", "this.data has STH coverageType set in");
	});
});
