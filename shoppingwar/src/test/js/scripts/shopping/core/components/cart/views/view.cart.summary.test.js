define([ 'require', 'backbone', 'templateEngine','shopping/core/components/cart/views/view.cart.summary' ,'common/views/base', 'text!html/shopping/components/cart/view.cart.summary.dust', 'components/cart/dust/helpers.cart', 'text!html/shopping/components/list/plansDisclaimer.dust','text!html/shopping/components/list/plansExtraInfoDisclaimer.dust' ], function(require, Backbone,
templateEngine, BaseView, template, cartView, cartHelpers, plansDisclaimerTemplate ,plansExtraInfoDisclaimerTemplate) {
		templateEngine.load(template, "view.cart.summary");
		templateEngine.load(plansDisclaimerTemplate, "plansDisclaimer");
	    templateEngine.load(plansExtraInfoDisclaimerTemplate, "plansExtraInfoDisclaimer");
			
		module('view.cart.summary', {
			
			setup : function() {			
				ProductElectionSummaryView = BaseView.extend({
				"model" :{
					"coverageType" : "ID3",
					"plansId" : 111,
					"displayPlansDisclaimer" : true
				},
				"options" : { "account" :{
				}}
			});	
			},
			teardown : function() {
			}
		});
		
		test('render', function() {
			ProductElectionSummaryView.prototype.options.account.has = function(){};
		    ProductElectionSummaryView.prototype.render();
			equal(ProductElectionSummaryView.prototype.model['displayPlansDisclaimer'], true);
		});
});