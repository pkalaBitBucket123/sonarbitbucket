define([ 'require', 'backbone', 'common/views/base', 'shopping/core/components/list/views/view.product.compare'], function(require, Backbone, BaseView,  BaseProductView) {

	 module('view.product.compare', {
	   setup : function() {
		 ProductCompareView = BaseProductView.extend({
		 collection: {},
		 options :{"displayPlansDisclaimer" : true},
		 displayPlansDisclaimer : true
		 });
	  },
	  teardown : function() {			
	  }
    });
	test('render', function() {		
	   ProductCompareView.prototype.collection.each = function(){};
	   ProductCompareView.prototype.render();
	   ok(ProductCompareView.prototype.displayPlansDisclaimer == true, "Displaying Plan Disclaimer");
	});
});
