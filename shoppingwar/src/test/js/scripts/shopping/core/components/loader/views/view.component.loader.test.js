define([ 'require', 'backbone', 'templateEngine','shopping/core/components/loader/views/view.component.loader', 'common/views/base', 'text!html/shopping/components/view.component.loader.dust' ], function(require, backbone, templateEngine,ComponentLoader, BaseView, template) {

	templateEngine.load(template, 'view.component.loader');

	module('view.component.loader', {
		setup : function() {			
		ComponentLoaderView = ComponentLoader.extend({});	
		},
		teardown : function() {
		}
	});
	test('initialize', function() {		
	   ComponentLoaderView.prototype.initialize();
	   notEqual(ComponentLoaderView.prototype.data.hide,null);
	   notEqual(ComponentLoaderView.prototype.data.hide,undefined);
	   notEqual(ComponentLoaderView.prototype.data.hideSpecial,null);
	   notEqual(ComponentLoaderView.prototype.data.hideSpecial,undefined);
	});
});