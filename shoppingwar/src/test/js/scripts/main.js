/** Copyright (C) 2012 Connecture */
requirejs.config({
	waitSeconds : 60,
	// because the accessing HTML pages all now must go
	// under root/shopping/ we need to move the base url as well so
	// everything falls into place
	baseUrl : '../main/webapp/scripts',
	paths : {
		'test' : '../../../test/js/scripts',
		'html' : '../html/',
		'html/shopping' : '../html/shopping/core',
		'shopping/individual/components' :'shopping/individual/components',
		'common' : 'common',
		'app' : 'shopping/core/app',
		'components' : 'shopping/core/components',
		'shopping/common' : 'shopping/core/common',
		'shopping/dust' : 'shopping/core/dust',
		'jquery' : 'thirdparty/jquery-1.10.1',
		'jquery-ui' : 'thirdparty/jquery-ui-1.10.1.custom',
		'jquery-formatCurrency' : 'thirdparty/jquery.formatCurrency-1.4.0.min',
		'jquery-printElement' : 'thirdparty/jquery.printElement',
		'dust' : 'thirdparty/dust-full-1.1.1',
		'dust-helpers' : 'thirdparty/dust-helpers-1.1.0',
		'json2' : 'thirdparty/json2',
		'jquery.strutsurl' : 'common/jquery/plugins/jquery.strutsurl',
		'backbone' : 'thirdparty/backbone',
		'underscore' : 'thirdparty/underscore',
		'connecture' : 'common/connecture',
		'es5-shim' : 'thirdparty/es5-shim',
		'dust-extensions' : 'common/dust/dust-extensions',
		'jquery.charts' : 'common/jquery/plugins/jquery.charts',
		'templateEngine' : 'common/template',
		'jquery.url' : 'thirdparty/jquery.urldecoder',
		'jquery.equalHeight' : 'common/jquery/plugins/jquery.equalHeight',
		'amplify' : 'thirdparty/amplify',
		'backbone.deep-model' : 'thirdparty/backbone.deep-model',
		'backbone.validation' : 'thirdparty/backbone-validation-amd',
		'mockjax' : 'thirdparty/jquery.mockjax',
		'sinon' : 'thirdparty/sinon-1.6.0',
		'underscore.deep-extend' : 'thirdparty/underscore.mixin.deepExtend',
		'log4javascript' : 'thirdparty/log4javascript',
		'log' : 'common/logger',
		'jquery.selectToUISlider' : 'thirdparty/selectToUISlider.jQuery.custom',
		'jquery.maskedInput' : 'thirdparty/jquery.maskedinput',
		'jquery.blockUI' : 'thirdparty/jquery.blockUI-2.59.0',
		'date' : 'thirdparty/date',
		// define the base widget definition here, might make sense to move to context apps instead
		'widget/definition' : 'shopping/core/common/widgets/widget.definition',
		'workflow/definition' : 'shopping/core/common/workflows/workflow.definition',
		'text' : '../../../test/js/scripts/mock.text'
	},
	shim : {
		'jquery' : {
			exports : '$'
		},
		'jquery-ui' : {
			deps : [ 'jquery' ]
		},
		'jquery-formatCurrency' : {
			deps : [ 'jquery' ]
		},
		'jquery-printElement' : {
			deps : [ 'jquery' ]
		},
		'jquery.charts' : {
			deps : [ 'jquery', 'connecture' ]
		},
		'dust' : {
			exports : 'dust'
		},
		'dust-helpers' : {
			deps : [ 'dust' ]
		},
		// This requires dust-helpers because we are adding
		// to that
		'dust-extensions' : {
			deps : [ 'dust', 'dust-helpers' ]
		},
		'json2' : {
			exports : 'JSON'
		},
		'jquery.strutsurl' : {
			deps : [ 'jquery' ]
		},
		'jquery.url' : {
			deps : [ 'jquery' ]
		},
		'jquery.equalHeight' : {
			deps : [ 'jquery' ]
		},
		'jquery.maskedInput' : {
			deps : [ 'jquery' ]
		},
		'underscore' : {
			exports : '_'
		},
		'backbone' : {
			deps : [ 'underscore', 'jquery' ],
			exports : 'Backbone'
		},
		'thirdparty/selectToUISlider.jQuery' : {
			deps : [ 'jquery', 'jquery-ui' ]
		},
		'common/connecture.ui' : {
			deps : [ 'connecture' ]
		},
		'amplify' : {
			deps : [ 'jquery' ],
			exports : 'amplify'
		},
		'underscore.deep-extend' : {
			deps : [ 'underscore' ]
		},
		'log4javascript' : {
			exports : 'log4javascript'
		},
		'mockjax' : {
			deps : [ 'jquery' ]
		},
		'sinon' : {
			exports : 'sinon'
		}
	},
	config : {
		'components/list/component.product.list' : {
			id : 'b718c2e2-2119-4053-ad7b-99b1daa45be6'
		},
		'components/cart/component.cart' : {
			id : '211ce84d-9dec-4d8e-8cc5-c77a44f6a9be'
		},
		'components/progress/component.progress' : {
			id : '513211f5-ca6c-47c9-9094-1b3a208f4b38'
		},
		'components/account/component.account' : {
			id : 'cbb55234-1fac-4ab0-920c-605d9ac0c032'
		},
		'components/header/component.header' : {
			backUrl : ''
		},
		'common/application/proxy' : {
			requests : {
				'component:get' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					params : [ 'transactionId' ]
				},
				'component:persist' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					params : [ 'transactionId' ]
				},
				'component:rulesEngine' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					params : [ 'ruleSetId' ]
				},
				'logout' : {
					type : 'redirect',
					location : 'Logout.action',
				},
				'login' : {
					type : 'redirect',
					location : 'login.html'
				}
			}
		},
		'common/application/proxy.filter' : {
			context : 'employee',
			stores : [ {
				type : 'planAdvisor',
				items : [ 'questionnaires', 'questions', 'workflows', 'filters', 'questionEvaluations', 'widgets' ]
			}, {
				type : 'consumerDemographics',
				items : [ 'members', 'productLines', 'genders', 'relationships', 'employer', 'employee' ]
			}, {
				type : 'account',
				items : [ 'account' ]
			}, {
				type : 'rulesEngine',
				items : [ 'rulesEngine' ]
			}, {
				type : 'costEstimatorConfig',
				items : [ 'costEstimatorConfig' ]
			}, {
				type : 'costEstimatorCosts',
				items : [ 'costEstimatorCosts' ]
			}, {
				type : 'eligibility',
				items : [ 'eligibility' ]
			}, {
				type : 'products',
				items : [ 'plans', 'planConfig' ]
			}, {
				type : 'providerSearch',
				items : [ 'providers' ]
			}, {
				type : 'applicationConfig',
				items : [ 'applicationConfig' ]

			}, {
				type : 'actor',
				items : [ 'actor' ]

			} ]
		},
		'common/application/application' : {

		}
	}
});