/** License: MIT License Url: http://www.opensource.org/licenses/mit-license.php */
define([ 'module' ], function(module) {
	var text = {

		/**
		 * Parses a resource name into its component parts. Resource names look like: module/name.ext!strip, where the !strip part is optional.
		 * 
		 * @param {String}
		 *            name the resource name
		 * @returns {Object} with properties "moduleName", "ext" and "strip" where strip is a boolean.
		 */
		parseName : function(name) {
			var strip = false, index = name.indexOf("."), modName = name.substring(0, index), ext = name.substring(index + 1, name.length);

			index = ext.indexOf("!");
			if (index !== -1) {
				// Pull off the strip arg.
				strip = ext.substring(index + 1, ext.length);
				strip = strip === "strip";
				ext = ext.substring(0, index);
			}

			return {
				moduleName : modName,
				ext : ext,
				strip : strip
			};
		},
		load : function(name, req, onLoad, config) {
			var parsed = text.parseName(name), nonStripName = parsed.moduleName + '.' + parsed.ext;
			var content = '<div></div>';
			onLoad(content);
		},
	};

	return text;
});