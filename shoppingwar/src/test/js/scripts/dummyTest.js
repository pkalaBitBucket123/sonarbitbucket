module("scripts.dummy", {
	setup:function(){
		this.a = 1;
		this.createTestUser = function() {
			return { name : "Bill" };
		};
	},
	teardown:function(){

	}
});

test("testNothing", function(){	
	ok(1, 1);
});

test("testSomething", function(){
	var b = this.a;
	equal(this.a, b, "a and b are ===");
});

test("testEceptionThrowerFail", function(){
	throws(function(){
		exceptionThrower(1);
	}, /error!/, "properly threw error");
});

test("testEceptionThrowerPass", function(){
	var ret = exceptionThrower(2);
	ok(ret, 3);
});

function exceptionThrower(a)
{
	if(a == 1)
	{
		throw "error!";
	}
	return a + 1;
}

test("garacci", function() {
	equal(2+2, "4", "addition test");
	
	var stupidFunction = function(param) {
		if (!param) {
			throw "missing";
		}
		else if (param == 1) {
			throw "bad";
		}
		return param;
	};
	
	throws(function () { stupidFunction(); }, /missing/); // missing
	throws(function () { stupidFunction(1); }, /bad/); // 1 
	equal(stupidFunction(2), 2); // good
	
	equal(this.createTestUser().name, "Bill");
});