/*
 * Qt+WebKit powered headless test runner using Phantomjs
 *
 * Phantomjs installation: http://code.google.com/p/phantomjs/wiki/BuildInstructions
 *
 * Run with:
 *  phantomjs runner.js [url-of-your-qunit-testsuite]
 *
 * E.g.
 *      phantomjs runner.js http://localhost/qunit/test
 */

var url = phantom.args[0];

var page = require('webpage').create();
var logList = [];

// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
	// console.log(msg);
	logList.push(msg);
};

page.onInitialized = function() {
	page.evaluate(addLogging);
};
page.open(url, function(status){
	if (status !== "success") {
		console.log("Unable to access network: " + status);
		phantom.exit(1);
	} else {
		// console.log("Running tests...");
		// page.evaluate(addLogging);
		var interval = setInterval(function() {
			if (finished()) {
				clearInterval(interval);
				onfinishedTests();
			}
		}, 500);
	}
});

function finished() {
	return page.evaluate(function(){
		return !!window.qunitDone;
	});
}

function onfinishedTests() {
	var qunitDone = page.evaluate(function() {
		return JSON.stringify(window.qunitDone);
	});
    var output = page.evaluate(function() {
        return window.output;
    });
    for(var i = 0; i < output.length; ++i) {
        console.log(output[i]);
    }
    if(logList.length > 0) {
        console.log("<!-- console.log() output:");
        for(var i = 0; i < logList.length; ++i) {
            console.log(logList[i]);
        }
        console.log("-->")
    }
	phantom.exit(JSON.parse(qunitDone).failed > 0 ? 1 : 0);
}

function addLogging() {
	window.document.addEventListener( "DOMContentLoaded", function() {
		var props = {source: 'QUnit tests'};
        var tests = [];
        var fails = [];
        var suites = [];
        var module = {
                name: props.source,
                start: new Date()
        }
        var testStart = new Date();
        totalFails = 0;

        var closeModule = function(vals) {
                var now = new Date();
                vals.ts = now;
                vals.time = (now.getTime() - vals.start.getTime()) * 0.001;
                vals.failed = fails.length;
                vals.total = tests.length;
                suites.push(suiteXml(vals, tests.join('')));
                tests = [];
                fails = [];
        }

        QUnit.moduleStart = function(context) {
                module.name = context.name;
                module.start = new Date();
        };

        QUnit.moduleDone = function(context) {
                context.start = module.start;
                closeModule(context);
                module.start = new Date();
        };

        QUnit.testStart = function(context) {
                testStart = new Date();
        };

        QUnit.log = function(context) {
                if ( !context.result ) {
                        fails.push(failXml(context));
                        totalFails++;
                }      
        };

        QUnit.testDone = function(context) {
                context.time = (new Date() - testStart) * 0.001;
                if ( !context.module ) {
                        context.module = module.name;
                }
                tests.push(testXml(context,fails.join('')));
        };
        var reportXml = function(props,suites) {
				window.output = [];
                window.output.push('<?xml version="1.0" encoding="UTF-8" ?>');
                window.output.push('<testsuites name="'+props.source+'">');
                //console.log(propsXml(props));
                window.output.push(suites);
                window.output.push('</testsuites>');
        };
        var propsXml = function(props) {
                var r = [];
                for (var key in props) {
                  if (props.hasOwnProperty(key)) {
                                r.push('<property name="'+key+'" value="'+props[key]+'" />\n');
                  }
                }
                return '<properties>\n'+r.join('')+'</properties>';
        };

        var suiteXml = function(vals, tests) {
                var r = [];
                r.push('<testsuite');
                r.push(' failures="'+vals.failed+'"');
				if (vals.name) {
					r.push(' name="'+vals.name+'"');
				}
                r.push(' tests="'+vals.total+'"');
                r.push(' time="'+vals.time+'"');
                r.push(' timestamp="'+vals.ts+'">\n');
                r.push(tests);
                r.push('</testsuite>\n');
                return r.join('');
        };

        var testXml = function(vars,fails) {
                var r = [];
                if ( fails.length ) {
                        r.push('<testcase');
						if (vars.module) {
                          r.push(' classname="'+vars.module+'"');
						} else {
                          r.push(' classname="'+props.source+'"');
						}
                        r.push(' name="'+entitize(vars.name)+'"');
                        r.push(' time="'+vars.time+'">\n');
                        r.push(fails);
                        r.push('</testcase>\n');
                } else {
                        r.push('<testcase');
						if (vars.module) {
                          r.push(' classname="'+vars.module+'"');
						} else {
                          r.push(' classname="'+props.source+'"');
						}
                        r.push(' name="'+entitize(vars.name)+'" ');
                        r.push(' time="'+vars.time+'" />\n');
                }
                return r.join('');
        };

        var failXml = function(vars) {
                var type = 'phantomjs-qunit-runner';
				var msg = 'Assertion failed';
				if (vars.message) {
					var msg = entitize(vars.message);
				} 
                var r = [];
                msg += ':  expected &lt;'+ entitize(vars.expected) + '&gt;, but was: &lt;' + entitize(vars.actual) + '&gt;';
                r.push('<failure message="'+msg+'" type="'+type+'">');
                r.push(type+': '+msg);
                r.push('</failure>\n');
                return r.join('');
        };

        var entitize = function(str) {
				if (str) {
					return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;');
				} else {
					return '';
				}
        }
		QUnit.done(function(result){
			if ( tests.length ) {
					closeModule(module);
			}

			reportXml(props,suites.join(''));
			window.qunitDone = result;
		});
	}, false );
}
