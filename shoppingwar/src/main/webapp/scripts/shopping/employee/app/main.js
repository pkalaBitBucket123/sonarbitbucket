/**
 * Copyright (C) 2012 Connecture
 */
requirejs.config({
	paths : {
		'html/employee' : '../html/shopping/employee'
	},
	map : {
		'*' : {
			// Replace the loading summary with an employee specific one
			'html/shopping/components/loader/view.loader.summary.dust' : 'html/employee/components/loader/view.loader.summary.dust',
			'html/shopping/components/list/view.product.list.navigation.dust' : 'html/employee/components/list/view.product.list.navigation.dust',
			'components/loader/views/view.loader.loading' : 'shopping/employee/components/loader/views/view.loader.loading',
			'components/header/views/view.header.links.internal' : 'shopping/employee/components/header/views/view.header.links.internal'
		},
		'common/application/proxy.filter' : {
			'common/application/datamodel' : 'shopping/employee/app/datamodel'
		},
		'components/account/component.account' : {
			'components/account/views/view.account.questionnaire.members.summary' : 'shopping/employee/components/account/views/view.account.questionnaire.members.summary',
			'components/account/workflows/workflow.item.whoscovered' : 'shopping/employee/components/account/workflows/workflow.item.whoscovered'
		},
		'components/loader/component.loader' : {
			// We need to process additional data for the loader
			'components/loader/views/view.loader.summary' : 'shopping/employee/components/loader/views/view.loader.summary'
		},
		'components/header/component.app.header' : {
			'components/header/views/view.app.header.logo' : 'shopping/employee/components/header/views/view.app.header.logo'
		},
		'widget/definition' : {
			'components/account/widgets/widget.providersearch' : 'shopping/employee/components/account/widgets/widget.providersearch',
			'components/account/widgets/widget.costestimator' : 'shopping/employee/components/account/widgets/widget.costestimator'
		},
		'components/account/views/view.account.summary' : {
			'components/account/views/view.account.questionnaire.members.summary' : 'shopping/employee/components/account/views/view.account.questionnaire.members.summary'
		},
		'components/account/views/view.account.question.summary' : {
			'components/account/widgets/widget.providersearch' : 'shopping/employee/components/account/widgets/widget.providersearch',
			'components/account/widgets/widget.costestimator' : 'shopping/employee/components/account/widgets/widget.costestimator'
		}
	},
	config : {
		'common/application/proxy.filter' : {
			context : 'employee',
			stores : [ {
				type : 'consumerDemographics',
				items : [ 'genders', 'relationships', 'employer', 'accountHolder' ]
			}, {
				type : 'account',
				items : [ 'account' ],
				queue : [ 'post' ]
			}, {
				type : 'rulesEngine',
				items : [ 'rulesEngine' ]
			}, {
				type : 'costEstimatorConfig',
				items : [ 'costEstimatorConfig' ]
			}, {
				type : 'costEstimatorCosts',
				items : [ 'costEstimatorCosts' ]
			}, {
				type : 'eligibility',
				items : [ 'eligibility' ]
			}, {
				type : 'productLines',
				items : [ 'productLines' ]
			}, {
				type : 'products',
				items : [ 'plans', 'planConfig' ]
			}, {
				type : 'povertyIncomeLevel',
				items : [ 'povertyIncomeLevel' ]
			}, {
				type : 'providerSearch',
				items : [ 'providers' ]
			}, {
				type : 'applicationConfig',
				items : [ 'applicationConfig', 'filters', 'questionEvaluations', 'widgets', 'workflowItems', 'workflow' ]
			}, {
				type : 'actor',
				items : [ 'actor' ]

			} ]
		}
	}
});