/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: The one thing we will need to decide is the best way to pull in components dynamically, to get things working I am putting them here
 * 
 * We can actually update this to use something like this:
 * 
 * http://requirejs.org/docs/whyamd.html#sugar
 * 
 * Which might help simplify this
 */
/*
 * This config will need to be here for development purposes. This will also be the main build file for the individual application as a whole
 */
define([ 'require', 'backbone', 'app/app.shopping' ], function(require, Backbone, application) {
	return application;
});