/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: This needs to get moved out of common
 * 
 * this will control how data is packaged and requested for your application
 * 
 * This should have the store stuff
 * 
 * This should also have the evenlope stuff
 * 
 * I think this should get referenced in the proxy
 * 
 * So then the proxy will call it's datamodel class to process each request
 * 
 * Based on the request the data model can decide what to do
 * 
 * 
 */
define([ "module", 'backbone', 'common/application/datamodel', 'shopping/common/models/model.account', 'components/account/models/model.costestimatorconfig',
		'components/account/models/model.eligibility', 'components/list/models/model.planconfig', 'shopping/common/collections/collection.product', 'components/list/models/model.costestimatorcosts',
		'shopping/common/models/model.employer', 'shopping/common/models/model.member', 'shopping/common/collections/collection.widget', 'shopping/common/collections/collection.app.workflow',
		'shopping/common/models/model.workflow' ], function(module, Backbone, DataModel, AccountModel, CostEstimatorConfigModel, EligibilityModel, PlanConfigModel, ProductList,
		CostEstimatorCostsModel, EmployerModel, MemberModel, WidgetList, AppWorkflowList, WorkflowModel) {

	var mappings = {
		'account' : {
			Model : AccountModel
		},
		'widgets' : {
			Collection : WidgetList
		},
		'costEstimatorConfig' : {
			Model : CostEstimatorConfigModel
		},
		'members' : {
			Collection : Backbone.Collection
		},
		'productLines' : {
			Collection : Backbone.Collection
		},
		'eligibility' : {
			Model : EligibilityModel
		},
		'relationships' : {
			Collection : Backbone.Collection
		},
		'genders' : {
			Collection : Backbone.Collection
		},
		'plans' : {
			Collection : ProductList
		},
		'planConfig' : {
			Model : PlanConfigModel
		},
		'filters' : {
			Model : Backbone.Model
		},
		'costEstimatorCosts' : {
			Model : CostEstimatorCostsModel
		},
		'applicationConfig' : {
			Model : Backbone.Model
		},
		'actor' : {
			Model : Backbone.Model
		},
		'employer' : {
			Model : EmployerModel,
		},
		'accountHolder' : {
			Model : MemberModel
		},
		'questionEvaluations' : {
			Collection : Backbone.Collection
		},
		'workflowItems' : {
			Collection : AppWorkflowList
		},
		'workflow' : {
			Model : WorkflowModel
		},
		'plugins' : {
			Collection : Backbone.Collection
		}
	};

	return new DataModel(mappings);

});