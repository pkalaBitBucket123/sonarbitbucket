/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/widgets/widget.costestimator' ], function(require, Backbone, templateEngine,
		dustHelpers, connecture, BaseCostEstimatorWidget) {

	var EmployeeCostEstimatorWidget = function(options) {
		BaseCostEstimatorWidget.call(this, options);
	};

	EmployeeCostEstimatorWidget.prototype = Object.create(BaseCostEstimatorWidget.prototype);

	EmployeeCostEstimatorWidget.prototype.getMembersForEstimation = function(members) {
		var membersForEstimation = [];

		var estimationProductLineCode =  this.model.get('config').productLineForEstimation;

		for ( var m = 0; m < members.length; m++) {
			var productMatched = false;
			for ( var p = 0; members[m].products && p < members[m].products.length && !productMatched; p++) {
				productMatched = members[m].products[p].productLineCode == estimationProductLineCode;
				if (productMatched) {
					membersForEstimation.push(members[m]);
				}
			}
		}

		return membersForEstimation;
	};

	EmployeeCostEstimatorWidget.prototype.isCoverageSelected = function(member) {
		return member.coverageSelected;
	};

	return EmployeeCostEstimatorWidget;
});