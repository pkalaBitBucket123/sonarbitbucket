/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'dust', 'dust-extensions' ], function(require, dust, dustExtensions) {

	dust.helpers['hasValue'] = function(chk, ctx, bodies, params) {
		
		var missingValue = false;
		
		var fieldName = params.fieldName;
		
		var modelData = ctx.current();
		
		var missingValues = ctx.get("missingValues");
		var memberRefName = modelData.memberRefName;
		
		missingValue = (missingValues && missingValues[memberRefName] && missingValues[memberRefName][fieldName]);

		if (missingValue && (missingValue == 'missing')) {
			chk = chk.render(bodies['else'], ctx);			
		} else {
			chk = chk.render(bodies.block, ctx);
		}
	
		return chk;
	};
});