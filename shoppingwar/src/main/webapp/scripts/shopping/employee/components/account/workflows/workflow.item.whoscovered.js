/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'components/account/workflows/workflow.item.whoscovered' ], function(require, Backbone, BaseWorkflowItem) {

	var WhosCoveredWorkflowItem = function(configuration, widgets, options) {

		BaseWorkflowItem.call(this, configuration, widgets, options);
	};

	WhosCoveredWorkflowItem.prototype = Object.create(BaseWorkflowItem.prototype);

	WhosCoveredWorkflowItem.prototype.isCostEstimatorCoverageSelected = function() {
		var retVal = false;
		if (this.costEstimatorIncluded) {
			var estimationProductLineCode = this.costEstimatorWidgetModel.get('config').productLineForEstimation;
			retVal = this.options.models.account.isCoverageSelected(estimationProductLineCode);
		}

		return retVal;
	};

	return WhosCoveredWorkflowItem;

});