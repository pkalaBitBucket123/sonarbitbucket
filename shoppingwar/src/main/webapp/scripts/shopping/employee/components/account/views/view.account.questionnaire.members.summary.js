/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'components/account/views/view.account.questionnaire.members.summary' ], function(require, Backbone, BaseQuestionnaireSummaryView) {

	var EmployeeQuestionnaireSummaryView = BaseQuestionnaireSummaryView.extend({
		initialize : function(options) {
			BaseQuestionnaireSummaryView.prototype.initialize.call(this, options);
		}
	});
	
	EmployeeQuestionnaireSummaryView.prototype = Object.create(BaseQuestionnaireSummaryView.prototype);

	EmployeeQuestionnaireSummaryView.prototype.isMemberCovered = function(member) {
		return member.get('coverageSelected');
	}

	return EmployeeQuestionnaireSummaryView;
});