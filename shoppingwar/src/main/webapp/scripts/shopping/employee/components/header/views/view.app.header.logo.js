/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'components/header/views/view.app.header.logo' ], function(require, Backbone, BaseView) {

	AppHeaderLogoView = BaseView.extend({
		initialize : function(options) {
			var employerModel = this.options.models.employer;
			this.logoData = {};
			this.logoData.name = employerModel.get("name");
			this.logoData.logoPath = employerModel.get("logoPath");
		}
	});

	return AppHeaderLogoView;
});