/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/widgets/widget.providersearch' ], function(require, Backbone, templateEngine,
		dustHelpers, connecture, BaseProviderSearchWidget) {

	var ProviderSearchWidget = function(options) {
		// set this first then call the constructor, that is because
		// the constructor is calling a method that needs this
		BaseProviderSearchWidget.call(this, options);
	};

	ProviderSearchWidget.prototype = Object.create(BaseProviderSearchWidget.prototype);

	/**
	 * Default all members are eligible for provider lookup
	 * 
	 * @param members
	 * @returns
	 */
	ProviderSearchWidget.prototype.getMembersForProviderLookup = function(members) {
		var membersForLookup = [];

		var estimationProductLineCode = this.model.get('config').productLineForProviderLookup;

		for ( var m = 0; m < members.length; m++) {
			var productMatched = false;
			for ( var p = 0; members[m].products && p < members[m].products.length && !productMatched; p++) {
				productMatched = members[m].products[p].productLineCode == estimationProductLineCode;
				if (productMatched) {
					membersForLookup.push(members[m]);
				}
			}
		}

		return membersForLookup;
	};
	
	/**
	 * 
	 * @param member - Backbone.Model
	 * @returns {Boolean}
	 */
	ProviderSearchWidget.prototype.isMemberCovered = function(member) {
		var retVal = false;

		var estimationProductLineCode = this.model.get('config').productLineForProviderLookup;
		var products = member.get('products');
		for ( var p = 0; products && p < products.length && !retVal; p++) {
			if (products[p].productLineCode == estimationProductLineCode) {
				retVal = true;
			}
		}

		return retVal;
	};

	return ProviderSearchWidget;
});