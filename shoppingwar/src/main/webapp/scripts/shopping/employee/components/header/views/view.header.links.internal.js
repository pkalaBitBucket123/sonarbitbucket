/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'text!html/employee/components/header/view.header.links.internal.dust' ], function(require, Backbone,
		templateEngine, dustHelpers, connecture, headerTemplate) {

	templateEngine.load(headerTemplate, "view.header.links.internal");

	HeaderLinksView = connecture.view.Base.extend({
		template : "view.header.links.internal",
		events : {
			'click ._backButton' : 'back',
			'click ._logout' : 'logout'
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
		},
		render : function() {
			var self = this;

			var renderedHTML = this.renderTemplate({
				data : self.options.data
			});

			$(self.el).html(renderedHTML);

			return this;
		},
		back : function() {
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.data.rosterURL
			});
		},
		logout : function() {
			this.eventManager.trigger('view:logout');
		}
	});

	return HeaderLinksView;
});