/**
 * Copyright (C) 2012 Connecture
 *
 */
define([ 'require', 'backbone', 'components/loader/views/view.loader.summary' ], function(require, Backbone, BaseLoaderSummaryView) {

	var ONE_DAYS_WORTH_OF_MILLISECONDS = 1000 * 60 * 60 * 24;
	
	var EmployeeLoaderSummaryView = BaseLoaderSummaryView.extend({
		className : 'callout',
		initialize : function(options) {
			BaseLoaderSummaryView.prototype.initialize.call(this, options);

			this.data = {
				productLines : options.models['productLines'].toJSON(),
				employer : options.models['employer'].toJSON()
			};
			
			var expirationTime = options.models['account'].get('expirationTimeMs');
			
			if (expirationTime) {
				//they have until the end of the day
				var expirationDate = new Date(expirationTime);
				expirationTime = new Date(expirationDate.getFullYear(), expirationDate.getMonth(), expirationDate.getDate()).getTime();
				expirationTime += (ONE_DAYS_WORTH_OF_MILLISECONDS - 1);
				
				var currentTime = new Date().getTime();

				this.data['daysLeft'] = 0;
			    if (expirationTime > currentTime) {
				    var time = expirationTime - currentTime;
    				// Always round down to the next whole number
	    			var daysLeft = Math.floor(time / ONE_DAYS_WORTH_OF_MILLISECONDS);
		    		this.data['daysLeft'] = daysLeft;
			    }
			}
		}
	});
	
	return EmployeeLoaderSummaryView;
});
