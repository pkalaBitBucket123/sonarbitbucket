/**
 * Copyright (C) 2012 Connecture
 *
 */
define([ 'require', 'backbone', 'common/views/base' ], function(require, backbone, BaseView) {
	var WelcomeSummaryLoadingView = BaseView.extend({
		className : 'callout',
		initialize : function(options) {

		},
		render : function() {
			var self = this;
			$(self.el).html('<div class="section days-left"><div class="action-indicator"><div class="indicator"></div><div class="message">Loading your benefits</div></div></div>');
			return this;
		}
	});

	return WelcomeSummaryLoadingView;
});
