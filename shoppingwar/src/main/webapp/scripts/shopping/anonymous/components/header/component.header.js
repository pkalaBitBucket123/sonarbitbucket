/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the top level component header. This is the main header that will be customized depending on where shopping is included.
 * 
 */
define([ 'module', 'log', 'backbone', 'templateEngine', 'common/component/component', 'shopping/individual/components/header/component.header'], function(module, log, Backbone,
		templateEngine, connecture, IndividualHeaderComponent) {

	var HeaderComponent = function(configuration) {
		IndividualHeaderComponent.call(this, configuration);

	};

	HeaderComponent.prototype = Object.create(IndividualHeaderComponent.prototype);

	HeaderComponent.prototype.changeQuotePlans = function(options) {
		if (!options.remove) {
			this.showQuote(true);
		}
	};
	
	HeaderComponent.prototype.removeQuotePlan = function(data) {
		var plans = this.models.quote.get("plans");
		for ( var p = 0; p < plans.length; p++) {
			if (plans[p].planId == data.planId) {
				plans.splice(p, 1);
			}
		}
		var riders = this.models.quote.get('selectedRiders');
		if(riders && riders['DENTAL']){
			delete riders['DENTAL'][data.planId];
		}
		this.models.quote.unset('selectedRiders', {
			silent : true
		});
		
		this.models.quote.set({
			'selectedRiders' : riders
		}, {
			remove : true
		});
		
		this.models.quote.unset('plans', { silent : true });
		this.models.quote.set({ 'plans' : plans }, { remove : true });

		// Re-render and Show Quote Receipt
		this.showQuote();
	};

	HeaderComponent.prototype.saveQuote = function() {
		this.saveAndExit();
	};
	
	return HeaderComponent;
});