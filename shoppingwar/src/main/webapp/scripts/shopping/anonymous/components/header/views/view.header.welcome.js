/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'components/header/views/view.header.welcome' ], function(require, Backbone, BaseView) {

	HeaderWelcomeView = BaseView.extend({
		events : {
			'click ._login' : 'login'
		},
		login : function(event) {
			this.eventManager.trigger('view:login');
		}
	});
	
	return HeaderWelcomeView;
});