/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {

	var proxyModel = ProxyModel.extend({
		initialize : function(options) {
			ProxyModel.prototype.initialize.call(this, {
				storeItem : 'account'
			});
		}
	});
	
	// Setting up AccountModel like this so that it can
	// get mixed in with other types if necessary
	var AccountModel = Backbone.Model.extend(_.extend({
		hasMembers : function() {
			return !_.isEmpty(this.get('members'));
		},
		/**
		 * Given a product id, this will determine if it has been added to the cart already
		 * 
		 * @param productId
		 * @returns {Boolean}
		 */
		inCart : function(productId) {
			var retVal = false;
			var account = this.toJSON();
			// make sure we have these properties
			if (account.hasOwnProperty('cart')) {
				if (account.cart.hasOwnProperty('productLines')) {
					_.each(account.cart.productLines, function(productLine) {
						_.each(productLine.products, function(product) {
							if (product.id === productId) {
								retVal = true;
							}
						});
					});
				}
			}

			return retVal;
		},
		/**
		 * Given a product id, this will determine if there is a product for that line in the cart already
		 * 
		 * @param productLine
		 * @returns {Boolean}
		 */
		productLineInCart : function(productLineCode) {
			var retVal = false;
			var account = this.toJSON();
			// make sure we have these properties
			if (account.hasOwnProperty('cart')) {
				if (account.cart.hasOwnProperty('productLines')) {
					_.each(account.cart.productLines, function(product, productLineKey) {
						// check for a matching product line
						if (productLineKey === productLineCode && !_.isEmpty(product)) {
							retVal = true;
						}
					});
				}
			}
			return retVal;
		},
		isCoverageSelected : function(productLineCode) {
			var retVal = false;
			var accountMembers = this.get("members");

			_.each(accountMembers, function(member) {
				_.each(member.products, function(productLine) {
					if (productLine.productLineCode == productLineCode) {
						retVal = true;
					}
				});
			});

			return retVal;
		}
	}, new proxyModel()));
	return AccountModel;
});