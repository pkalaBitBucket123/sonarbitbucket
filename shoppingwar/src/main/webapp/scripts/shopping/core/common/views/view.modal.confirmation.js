/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/views/view.confirmation.dust' ], function(require, Backbone, templateEngine, BaseView, template) {

	templateEngine.load(template, "view.confirmation");

	ConfrimationModalView = BaseView.extend({
		template : "view.confirmation",
		events : {
			'click ._btnClose' : 'closeModal'
		},
		initialize : function() {

		},
		render : function() {

			var self = this;

			var renderedHTML = this.renderTemplate({
				data :  self.options.data
			});

			$(self.el).html(renderedHTML);

			return this;
		},
		closeModal : function(event) {	
			event.preventDefault();			
			var self = this;	
			self.options.eventManager.trigger('view:closeConfirmationModal');		
		}
	});

	return ConfrimationModalView;
});