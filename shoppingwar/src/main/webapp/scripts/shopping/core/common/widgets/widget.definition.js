/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'components/account/widgets/widget.configuredquestion', 'components/account/widgets/widget.providersearch', 'components/account/widgets/widget.costestimator',
		'components/account/widgets/widget.method', 'components/account/widgets/widget.subsidy', 'components/account/widgets/widget.demographics',
		'components/account/widgets/widget.specialenrollment', 'components/account/widgets/widget.enrollmentdate' ], function(require, Backbone, ConfiguredQuestionWidget, ProviderSearchWidget,
		CostEstimatorWidget, MethodWidget, SubsidyWidget, DemographicsWidget, SpecialEnrollmentWidget, EnrollmentDateWidget) {

	return {
		createWidget : function(widgetType, configuration) {
			var retVal = null;
			if (widgetType === "ProviderLookup") {
				retVal = new ProviderSearchWidget(configuration);
			} else if (widgetType === "CostEstimator") {
				retVal = new CostEstimatorWidget(configuration);
			} else if (widgetType === "Method") {
				retVal = new MethodWidget(configuration);
			} else if (widgetType === "SubsidyQuickCheck") {
				retVal = new SubsidyWidget(configuration);
			} else if (widgetType === "Demographics") {
				retVal = new DemographicsWidget(configuration);
			} else if (widgetType === "SpecialEnrollment") {
				retVal = new SpecialEnrollmentWidget(configuration);
			} else if (widgetType === "EnrollmentDate") {
				retVal = new EnrollmentDateWidget(configuration);
			} else { // TODO: Remove this default
				retVal = new ConfiguredQuestionWidget(configuration);
			}

			return retVal;
		}
	};
});
