/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: This is going away with the config redesign
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {

	var WorkflowList = Backbone.Collection.extend({
		initialize : function(models, options) {

		},
		getDefaultWorkflow : function() {
			var retVal = null;
			this.each(function(workflow) {
				if (workflow.get('defaultWorkflow') === true) {
					retVal = workflow;
				}
			});

			return retVal;
		},
		getWorkflow : function(id) {
			var retVal = null;
			this.each(function(workflow) {
				if (workflow.get('workflowRefId') == id) {
					retVal = workflow;
				}
			});

			return retVal;
		}
	});

	return WorkflowList;
});