/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'backbone', 'backbone.paginator', 'backbone.mixin', 'common/application/proxy.collection', 'shopping/common/models/model.product' ], function(require, log, Backbone,
		BackbonePaginator, Cocktail, ProxyCollection, ProductModel) {

	// private function
	function getLowestCostForProduct(product) {
		var productCost = product['cost'];
		var finalCost = null;
		if (productCost) {
			var monthlyCost = productCost.monthly;
			var monthlyRate = monthlyCost.rate;
			var monthlyRateAmount = monthlyRate.amount;
			var monthlyContributionAmount = 0;
			var monthlyContribution = monthlyCost.contribution;
			if (monthlyContribution) {
				monthlyContributionAmount = monthlyContribution.amount;
			}

			finalCost = monthlyRateAmount - monthlyContributionAmount;
		}

		return finalCost;
	}

	var ProductProxyCollection = ProxyCollection.extend({
		initialize : function(models, options) {
			ProxyCollection.prototype.initialize.call(this, models, {
				storeItem : 'plans'
			});
		}
	});

	/**
	 * I think we want to maintain two collections of our models. We need to do this so we have one main one that stores ALL and then one that we filter against. We have to do this because we are
	 * going to be constantly adding and removing products based on what they select
	 */
	var ProductList = Backbone.Paginator.clientPager.extend({ // ProxyCollection.extend({
		model : ProductModel,

		paginator_ui : { // the lowest page index your API allows to be accessed
			firstPage : 1,

			// which page should the paginator start from
			// (also, the actual page the paginator is on)
			currentPage : 1,

			// how many items per page should be shown
			perPage : 6,

			// a default number of total pages to query in case the API or
			// service you are using does not support providing the total
			// number of pages for us.
			// 10 as a default in case your service doesn't return the total
			totalPages : 10,

			// The total number of pages to be shown as a pagination
			// list is calculated by (pagesInRange * 2) + 1.
			pagesInRange : 4
		},
		initialize : function(models, options) {
			Backbone.Paginator.clientPager.prototype.initialize.call(this, options);
			// after we init the parent, we need to calculate total pages
			this.totalPages = Math.ceil(_.size(models) / this.perPage);

			// The default should be sortScore, if
			// one is not passed in
			if (options && options.sort) {
				this.sortProp = options.sort;
			} else {
				// hardcoded default for now
				this.sortProp = {
					id : 'recommend',
					what : 'sortScore',
					label : 'Recommended',
					how : 'desc',
					selected : true
				};
			}
			// on initialize set
			this.assignLowestCostProduct(models);

			// on reset of collection, process
			this.on('reset', this.assignLowestCostProduct, this);
		},
		parse : function(response) {

			return response;
		},
		assignLowestCostProduct : function(models) {
			// this shouldn't change because plan rate and contribution don't change
			// should be a one-time call on load of the component
			if (typeof models === 'undefined' || !_.isArray(models)) {
				models = this.toJSON();
			}

			var lowestCostProducts = {};
			var currentLowestCost = {};

			_.each(models, function(plan) {
				// find lowest cost for each product line
				plan["isLowestCost"] = false;
				var planProductLine = plan["productLine"];

				if (!lowestCostProducts[planProductLine]) {

					var lowCost = getLowestCostForProduct(plan);
					if (lowCost) {

						currentLowestCost[planProductLine] = lowCost;
						lowestCostProducts[planProductLine] = plan;
					}
				} else {

					var lowCost = getLowestCostForProduct(plan);
					if (lowCost && (lowCost < currentLowestCost[planProductLine])) {

						currentLowestCost[planProductLine] = lowCost;
						lowestCostProducts[planProductLine] = plan;
					}
				}
			});

			if (lowestCostProducts) {
				// set lowest cost value for each product line entry
				for ( var lowestCostProductLine in lowestCostProducts) {
					lowestCostProducts[lowestCostProductLine]["isLowestCost"] = true;
				}
			} else {
				log.error("error determining lowest cost product");
			}

		},
		/**
		 * Overriding this method to determine if any plan has been scored or preference matched
		 * 
		 * @param options
		 */
		sort : function(options) {
			var self = this;
			// default the rank to false, unless we find something
			this.ranked = false;
			this.every(function(product) {
				if (product.get('isBestMatch') || product.get('sortScore') !== 0 || !_.isEmpty(product.get('preferenceMatches'))) {
					self.ranked = true;
					return;
				}
			});

			ProxyCollection.prototype.sort.call(this, options);
		},
		// Need a way to automatically add this to a collection
		// given some configuration
		// Define configuration for collections
		comparator : function(productA, productB) {
			var sortPropA = sortPropB = this.sortProp.what;
			var sortDirection = this.sortProp.how;

			// if this collection is not ranked, then
			// suggested plan should always come first
			// if it is ranked though, ignore this property completely
			if (!this.ranked) {
				if (productA.get('isSuggestedPlan') === true) {// if neither are best match, then proceed
					return -1;
				} else if (productB.get('isSuggestedPlan') === true) {
					return 1;
				}
			} else if (productA.get('isBestMatch') === true) {// if neither are best match, then proceed
				return -1;
			} else if (productB.get('isBestMatch') === true) {
				return 1;
			}

			// if we are checking score and they are equal default to sortOrder
			if (this.sortProp.what === 'sortScore' && (productA.get('sortScore') === productB.get('sortScore'))) {
				sortPropA = 'sortOrder';
				sortPropB = 'sortOrder';
				sortDirection = 'asc';
			} else {
				// This code will probably never get hit and I am not sure it will work like we think it does
				// anymore
				if (sortPropA === 'sortScore' && typeof productA.get('sortScore') === 'undefined') {
					sortPropA = 'sortOrder';
				}

				if (sortPropB === 'sortScore' && typeof productB.get('sortScore') === 'undefined') {
					sortPropB = 'sortOrder';
				}
			}

			var productAVal = productA.get(sortPropA);
			var productBVal = productB.get(sortPropB);

			// Now we need to determine how we should sort based on configuration.
			if (productAVal > productBVal) {
				if (sortDirection == 'asc') {
					return 1;
				} else {
					return -1;
				}
			} else if (productAVal < productBVal) {
				if (sortDirection == 'asc') {
					return -1;
				} else {
					return 1;
				}
			} else {
				return 0;
			}
		},
		/**
		 * We probably want to put this at a higher level. This is going to take in an object that contains a map of filters that we need to apply
		 * 
		 * @param filter
		 * @returns
		 */
		filterProducts : function(filters) {
			return this.filter(function(model) {
				var isFilter = true;
				_.each(filters, function(filter, key) {
					if (!filter.filter(model)) {
						isFilter = false;
						return;
					}
				});

				return isFilter;
			});
		}
	});

	Backbone.mixin(ProductList, ProductProxyCollection.prototype);

	return ProductList;
});