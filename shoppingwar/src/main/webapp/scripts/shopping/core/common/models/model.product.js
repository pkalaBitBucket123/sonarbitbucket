/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone.deep-model', 'underscore.deep-extend', ], function(require, backbone) {
	var ProductModel = Backbone.DeepModel.extend({
		initialize : function() {

		}
	});

	return ProductModel;
});