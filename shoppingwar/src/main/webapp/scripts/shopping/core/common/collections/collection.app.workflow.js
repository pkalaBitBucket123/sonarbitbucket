/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the new version of workflows, and should replace collection.workflow.js
 * 
 */
define([ 'require', 'backbone', 'shopping/common/models/model.workflow.item' ], function(require, Backbone, WorkflowItem) {

	var WorkflowList = Backbone.Collection.extend({
		model : WorkflowItem,
		initialize : function(models, options) {

		}
	});

	return WorkflowList;
});