/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/workflows/workflow.item', 'components/account/workflows/workflow.item.questions', 'components/account/workflows/workflow.item.whoscovered',
		'components/account/workflows/workflow.item.summary' ], function(require, Backbone, WorkflowItem, QuestionsWorkflowItem, WhosCoveredWorkflowItem, SummaryWorkflowItem) {

	return {
		createWidget : function(widgetType, configuration, widgets, options) {
			var retVal = null;
			if (widgetType === "QuestionsItem") {
				retVal = new QuestionsWorkflowItem(configuration, widgets, options);
			} else if (widgetType === "WhosCoveredItem") {
				retVal = new WhosCoveredWorkflowItem(configuration, widgets, options);
			} else if (widgetType === "Summary") {
				retVal = new SummaryWorkflowItem(configuration, widgets, options);
			} else {
				retVal = new WorkflowItem(configuration, widgets, options);
			}

			return retVal;
		}
	};
});
