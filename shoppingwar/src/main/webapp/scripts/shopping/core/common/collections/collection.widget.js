/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {

	var WidgetList = Backbone.Collection.extend({
		initialize : function(models, options) {

		},
		/**
		 * 
		 * @param id
		 * @param type -
		 *            optional legacy stuff
		 * @returns
		 */
		getWidget : function(id, type) {
			var retVal = null;

			this.each(function(widget) {
				// referring to the old question widget
				if (type === 'widget') {
					if (widget.get('config').widgetId === id) {
						retVal = widget;
					}
				} else {
					if (widget.get('widgetId') === id) {
						retVal = widget;
					}
				}
			});

			return retVal;
		},
		isCostEstimatorIncluded : function() {
			return this.getWidget('CostEstimator', 'widget') !== null;
		},
		getCostEstimatorId : function() {
			var costEstimatorQuestionRefId = null;
			var questions = this.toJSON();
			for ( var q = 0; q < questions.length && costEstimatorQuestionRefId == null; q++) {
				if (questions[q].widgetId == "CostEstimator") {
					costEstimatorQuestionRefId = questions[q].questionRefId;
				}
			}

			return costEstimatorQuestionRefId;
		},
		/**
		 * Right now, if the widgetIds coming in is empty, then we return the whole list. Not sure if this is the behavior we want going forward.
		 * 
		 * @param widgetIds
		 * @param configField
		 * @returns {WidgetList}
		 */
		getFilteredWidgets : function(widgetIds, configField) {
			var self = this;

			// this is probably a terrible decision, API wise.
			if (_.isEmpty(widgetIds)) {
				return new WidgetList(this.models);
			}

			var retVal = new WidgetList();

			_.each(widgetIds, function(widgetId) {

				var id = _.isObject(widgetId) ? widgetId.id : widgetId;

				self.each(function(widget) {
					if ((configField && id === widget.get('config')[configField]) || id === widget.get('widgetId')) {
						retVal.add(widget);
					}
				});
			});

			return retVal;
		}

	});

	return WidgetList;
});