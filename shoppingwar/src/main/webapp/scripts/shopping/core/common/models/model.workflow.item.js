/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {

	var WorkflowItem = Backbone.Model.extend({
		initialize : function() {

		},
		/**
		 * Given a widgetId, find a workflow item if one exists
		 * 
		 * @param widgetId
		 */
		getWdigetWorkflow : function(widgetId) {
			var retVal = null;
			// if workflows exist, loop through all of them
			// and see if we can find a widget, then return the
			// workflow
			// depending on how many times we have to look this up
			// we might have to offload this to the server and send down a map
			if (this.has('workflows')) {
				_.each(this.get('workflows'), function(workflow) {
					_.each(workflow.widgets, function(widget) {
						if (widgetId === widget) {
							retVal = workflow;
						}
					});
				});
			}

			return retVal;
		},
		/**
		 * Given the current sub-workflow id, this will return the previous one
		 * 
		 * @param workflowId
		 * @returns
		 */
		getPreviousWorkflow : function(workflowId) {
			var previousWorkflow = null;
			if (this.has('workflows')) {
				var index = null;
				_.each(this.get('workflows'), function(workflow, workflowIndex) {
					if (workflow.id === workflowId) {
						index = workflowIndex;
					}
				});

				if (index > 0) {
					previousWorkflow = this.get('workflows')[index - 1];
				}

			}

			return previousWorkflow;
		},
		/**
		 * Given the current sub-workflow id, this will return the next one
		 * 
		 * @param workflowId
		 * @returns
		 */
		getNextWorkflow : function(workflowId) {
			var nextWorkflow = null;
			if (this.has('workflows')) {
				var index = null;
				_.each(this.get('workflows'), function(workflow, workflowIndex) {
					if (workflow.id === workflowId) {
						index = workflowIndex;
					}
				});

				if (index < (_.size(this.get('workflows')) - 1)) {
					nextWorkflow = this.get('workflows')[index + 1];
				}

			}

			return nextWorkflow;
		},
		/**
		 * Given a workflow id, determines if it is the last one
		 * 
		 * @param workflowId
		 */
		isLastWorkflow : function(workflowId) {
			var retVal = false;
			if (this.has('workflows')) {

				var index = null;
				_.each(this.get('workflows'), function(workflow, workflowIndex) {
					if (workflow.id === workflowId) {
						index = workflowIndex;
					}
				});

				if (index === (_.size(this.get('workflows')) - 1)) {
					retVal = true;
				}
			}

			return retVal;
		},
		/**
		 * Given a widgetId, if this workflow item has subworkflows, return that one
		 * 
		 * @param widgetId
		 */
		getWorkflow : function(id) {
			var retVal = null;
			if (this.has('workflows')) {
				_.each(this.get('workflows'), function(workflow, workflowIndex) {
					_.each(workflow.widgets, function(widgetId) {
						if (id === widgetId && retVal === null) {
							retVal = workflow;
						}
					});

				});
			}

			return retVal;

		}
	});

	return WorkflowItem;
});