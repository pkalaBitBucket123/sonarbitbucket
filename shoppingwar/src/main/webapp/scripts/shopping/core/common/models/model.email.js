/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, backbone, ProxyModel) {

	var EmailModel = ProxyModel.extend({
		initialize : function(options) {
			ProxyModel.prototype.initialize.call(this, {
				requestKey : 'email'
			});
		},
		// defaults
		validation : {
			"toEmail" : {
				required : true,
				pattern : 'email',
				maxLength : 50
			},
			"firstName" : {
				required : false,
				namePattern : true,
				maxLength : 25
			},
			"lastName" : {
				required : false,
				namePattern : true,
				maxLength : 25
			},
			"phone" : {
				required : false
			}
		},
		labels : {
			"toEmail" : "Email",
			"email" : "Email must be in the format xx@xx.xxx"
		}
	});

	return EmailModel;
});