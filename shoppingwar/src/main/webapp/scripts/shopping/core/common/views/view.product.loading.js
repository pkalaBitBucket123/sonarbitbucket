/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/views/view.product.loading.dust' ], function(require, Backbone, templateEngine, connecture, template) {

	templateEngine.load(template, "view.product.loading");

	ProductLoadingView = connecture.view.Base.extend({
		template : "view.product.loading",
		events : {

		},
		initialize : function() {

		},
		render : function() {

			var self = this;

			var renderedHTML = this.renderTemplate({
				data : {}
			});

			$(self.el).html(renderedHTML);

			return this;
		}
	});

	return ProductLoadingView;
});