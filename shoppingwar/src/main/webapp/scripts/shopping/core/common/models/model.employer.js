/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, backbone) {

	var EmployerModel = Backbone.Model.extend({
		initialize : function() {

		}
	});

	return EmployerModel;
});