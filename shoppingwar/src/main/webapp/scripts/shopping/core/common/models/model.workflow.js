/**
 * Copyright (C) 2012 Connecture
 * 
 * This model represents the configuration around all of the workflows for a given application.
 * 
 */
define([ 'require', 'backbone' ], function(require, backbone) {

	var WorkflowModel = Backbone.Model.extend({
		initialize : function() {

		},
		indexOfWorkflowItem : function(workflowId) {
			var retVal = -1;

			_.every(this.get('items'), function(item, index) {
				if (_.isObject(item)) {
					if (item.item === workflowId) {
						retVal = index;
						return;
					}
				} else if (item === workflowId) {
					retVal = index;
					return;
				}

				return true;
			});

			return retVal;
		},
		getWorkflowItem : function(workflowId) {
			var retVal = null;

			_.every(this.get('items'), function(item, index) {
				if (_.isObject(item)) {
					if (item.item === workflowId) {
						retVal = item;
						return;
					}
				} else if (item === workflowId) {
					retVal = item;
					return;
				}

				return true;
			});

			// if it isn't an object, turn it into one
			if (!_.isObject(retVal)) {
				retval = {
					item : retVal,
					// nulls will be default behavior
					next : null,
					previous : null
				};
			}

			return retVal;
		},
		getWorkflowItemByAlias : function(workflowAlias, allWorkflowItems) {
			
			var retVal = null;
			var self = this;
			
			// find id for alias...
			_.each(allWorkflowItems, function(workflowItem){
				
				if (workflowAlias == workflowItem.get("alias")){				
					var workflowId = workflowItem.get("id");
					
					// if the id exists in the config items, then we have a real match
					_.every(self.get('items'), function(item){
						if (workflowId === item.item){
							retVal = item;
							return;
						} else if (workflowId === item){
							retVal = item;
							return;
						}
						return true;
					});
				}
			});
			
			// if it isn't an object, turn it into one
			if (!(_.isObject(retVal))) {
				var retValObj = {
					item : retVal,
					// nulls will be default behavior
					next : null,
					previous : null
				};
				
				retVal = retValObj;
			}
		
			return retVal;
		},
		getDefaultItem : function() {
			if (this.get('default')) {
				return this.get('default');
			}

			var retVal = _.first(this.get('items'));

			if (_.isObject(retVal)) {
				retVal = retVal.item;
			}

			return retVal;
		}

	});

	return WorkflowModel;
});