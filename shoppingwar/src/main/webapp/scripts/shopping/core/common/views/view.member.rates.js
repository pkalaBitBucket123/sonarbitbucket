/**
 * Copyright (C) 2012 Connecture
 * 
 * This is in the common section because it is referenced across components
 * 
 */
define([ 'require', 'backbone', 'jquery-formatCurrency', 'templateEngine', 'common/component/component',
        'text!html/shopping/views/view.member.rates.dust' ], function(require, Backbone, formatCurrency,
        templateEngine, connecture, template) {

    templateEngine.load(template, 'view.member.rates');

    // These are the workflows
    var MemberLevelRates = connecture.view.Base.extend({
        template : 'view.member.rates',
        events : {

        },
        initialize : function(options) {
            this.eventManager = options.eventManager;
            this.account = this.options.account;
        },
        render : function() {
            var self = this;
            var data = {};
            data.rate = this.model.get('cost.monthly.rate.amount');
            data.name = this.model.get('name');
            data.members = [];
            var memberRates = this.model.get('cost.monthly.memberLevelRating.memberRates');
            var members = this.account.get('members');
            // loop through all of the members

            _.each(members, function(member) {
                var rate = 'included';
                var tobaccoSurcharge = null;
                _.each(memberRates, function(memberRate) {
                    if (member.memberExtRefId == memberRate.memberId || member.memberRefName == memberRate.memberId) {
                        if (memberRate.amount !== 'included'
                                && (!isNaN(parseFloat(memberRate.amount)) && parseFloat(memberRate.amount) !== 0)) {
                            rate = memberRate.amount;
                        }
                        // only add a value for tobacco surcharge if we have a numeric value
                        if (memberRate.tobaccoSurcharge && memberRate.tobaccoSurcharge !== 'included'
                                && parseFloat(memberRate.tobaccoSurcharge) !== 0) {
                            tobaccoSurcharge = memberRate.tobaccoSurcharge;
                        }
                    }
                });
                data.members.push({
                    name : member.name,
                    relationship : member.memberRelationship,
                    rate : rate,
                    tobaccoSurcharge : tobaccoSurcharge
                });
            });

            $(self.el).html(this.renderTemplate({
                "data" : data
            }));

            $('.rate', self.el).formatCurrency({
                roundToDecimalPlace : 2
            });
            $('.member-rate', self.el).formatCurrency({
                roundToDecimalPlace : 2
            });

            return this;
        }
    });

    return MemberLevelRates;
});