/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {

	var proxyModel = ProxyModel.extend({
		initialize : function(options) {
			ProxyModel.prototype.initialize.call(this, {
				storeItem : 'quote'
			});
		}
	});
	
	// Setting up QuoteModel like this so that it can
	// get mixed in with other types if necessary
	var QuoteModel = Backbone.Model.extend(_.extend({
		hasQuotePlans : function() {
			return !_.isEmpty(this.get('plans'));
		},
		/**
		 * Given a product id, this will determine if it has been added to the quote already
		 * 
		 * @param productId
		 * @returns {Boolean}
		 */
		inQuote : function(productId) {
			var retVal = false;
			
			var quote = this.toJSON();
			// make sure we have these properties
			if (quote.hasOwnProperty('plans')) {
				retVal = _.any(quote['plans'], function(plan) {
					if (plan.planId == productId) {
						return true;
					}
				});
			}

			return retVal;
		},
		togglePlanInclusion : function(productId) {
			var plans = this.get("plans");
			
			var inQuote = false;
			for (var i = 0; i < plans.length && !inQuote; i++) {
				if (plans[i].planId == productId) {
					inQuote = true;
				}
			}
			// Add to Quote if it's not there
			if (!inQuote) {
				plans.push({ planId : productId });
			}
		
			
			if(plans.length>1) {
			   for (var i = 0; i < plans.length; i++) {
				  for(var j =i+1;j<plans.length;){
			         if (plans[j].planId == plans[i].planId) {
				        plans.splice(plans, j); 
			         }
			         else {
				        j++;
			         }
				  }
			   }
			}
			this.unset("plans", { silent : true } );
			this.set("plans", plans);
		}
	}, new proxyModel()));
	
	return QuoteModel;
});