/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'common/widget.factory' ], function(require, Backbone, templateEngine, connecture, WidgetFactory) {

	var QuestionSummaryView = connecture.view.Base.extend({
		initialize : function(options) {
			// these two might not always be here, cost estimator only
			this.costEstimatorConfig = options.costEstimatorConfig;
			this.member = options.member;
			this.account = options.account;
		},
		render : function() {
			var self = this;
			var data = this.model.toJSON();
			if (this.member) {
				data.member = this.member.toJSON();
			}

			var widgetFactory = new WidgetFactory();
			// add the minimum amount of data necessary for now
			// this will eventually get merged into the new
			// flow
			var widgetModel = new Backbone.Model(data);
			// TODO: I think this way of getting the widget is temporary until
			// we refactor summary
			var widget = widgetFactory.getWidget(widgetModel.get('config').widgetId, {
				"widgetModel" : widgetModel,
				"attributes" : {
					account : this.account,
					config : widgetModel.toJSON().config
				}
			});

			// TODO: I think eventually when these questions are already in memory
			// we won't have to deal with this mess
			var summaryView = widget.getSummaryView({
				"questionRefId" : data.config.questionRefId,
				"responseValue" : data.value,
				"title" : data.config.title,
				'responseData' : data.data,
				"answers" : data.config.answers,
				"memberRefName" : data.memberRefName,
				// TODO: last three are really only needed for cost estimator
				"costEstimatorConfig" : this.costEstimatorConfig,
				'members' : data.members,
				'member' : this.member
			});

			$(self.el).append(summaryView.render().el);

			// link the widget view to this view so they will all destroy properly
			this.views.push(summaryView);

			return this;
		}
	});

	return QuestionSummaryView;

});