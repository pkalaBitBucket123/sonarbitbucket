/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/components/list/email/view.email.dust' ], function(require, Backbone, templateEngine, BaseView, template) {

	templateEngine.load(template, "view.email");

	EmailModalView = BaseView.extend({
		template : "view.email",
		events : {
			'click ._btnEmail' : 'emailPlan',
			'click ._btnCancel' : 'closeModal'
		},
		initialize : function(options) {
			this.model.on("error", this.showError, this);
			this.accountHolder = options.accountHolder;
			this.products = options.products;
		},
		showError : function(model, error) {

		},
		render : function() {
			var self = this;

			// build model, including errors
			var dustJSON = {};

			dustJSON.data = self.model.toJSON();
			dustJSON.data.sender = this.accountHolder.toJSON();
			dustJSON.data.products = this.products.toJSON();

			var renderedHTML = this.renderTemplate({
				data : dustJSON
			});

			$(self.el).html(renderedHTML);

			// bind default text
			self.bindDefaultText($(self.el).find('#_toEmail'));

			return this;
		},
		emailPlan : function(event) {
			event.preventDefault();

			// move attributes from the view into the model

			this.model.attributes.toEmail = $(this.el).find('#_toEmail').hasClass('default-text') ? '' : $(this.el).find('#_toEmail').val();
			this.model.attributes.note = $(this.el).find('#_note').val();
			this.model.attributes.copyMe = $(this.el).find('#_copyMe').is(':checked');

			// validate the model
			this.hideErrors();

			// based on validation, show errors or process
			if (this.model.validate(this.model.attributes)) {
				this.showErrors(this.model.errors);
			} else {
				// build the request data to be used to build the email
				var request = {};

				request.sender = this.accountHolder.toJSON();
				request.toEmail = this.model.get('toEmail');
				request.note = this.model.get('note');
				request.copyMe = this.model.get('copyMe');
				request.productData = this.products.toJSON();
				request.costViewCode = this.model.get('costViewCode');

				this.options.eventManager.trigger('view:email:send', request);
			}
		},
		closeModal : function(event) {
			event.preventDefault();
			var self = this;
			self.options.eventManager.trigger('view:closeModal');
		}
	});

	EmailModalView.prototype.hideErrors = function(errors) {
		$(this.el).find('#_errors').html('');
		$(this.el).find('#_errors').hide();
	};

	EmailModalView.prototype.showErrors = function(errors) {

		var self = this;

		if (errors.length > 0) {
			for ( var i = 0; i < errors.length; i++) {
				$(self.el).find('#_errors').append(errors[i].value);
			}
			$(self.el).find('#_errors').fadeIn();
		}
	};

	EmailModalView.prototype.bindDefaultText = function(element) {
		var defaultMessageText = $(element).val();
		$(element).addClass('default-text');

		// remove default text on focus
		$(element).focus(function() {
			if ($(element).val() == defaultMessageText) {
				$(element).val('');
				$(element).removeClass('default-text');
			}
		});

		// restore default text on blur, if empty
		$(element).blur(function() {
			if ($(element).val() == '') {
				$(element).addClass('default-text');
				$(element).val(defaultMessageText);
			}
		});
	};

	return EmailModalView;
});