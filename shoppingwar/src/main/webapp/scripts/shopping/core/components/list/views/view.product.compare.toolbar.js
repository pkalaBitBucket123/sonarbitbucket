/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/list/view.product.compare.toolbar.dust' ], function(require, Backbone, templateEngine,
		connecture, template) {
	templateEngine.load(template, 'view.product.compare.toolbar');

	var ProductCompareToolBarView = connecture.view.Base.extend({
		template : 'view.product.compare.toolbar',
		events : {
			'click ._browse' : 'showProducts',
			'click ._compare' : 'showCompare',
			'click ._details' : 'showDetail',
			'click ._productLineSelector' : 'toggleProductLine',
			'click ._productLineDropDown ._option' : 'selectProductLine',
			'click ._monthly' : 'showMonthlyCost',
			'click ._paycheck' : 'showPaycheckCost'
		},
		initialize : function() {

		},
		render : function() {
			var self = this;

			$(self.el).html(this.renderTemplate({
				data : {
					toggleCost : this.options.toggleCost,
					costViewCode : this.options.costViewCode,
					filter : this.options.filter,
					productListSize : this.options.productListSize,
					filteredProductListSize : this.options.filteredProductListSize
				}
			}));
			$('._productLineDropDown', self.el).hide();
			$('a[title]', self.el).tooltip();

			return this;
		},
		close : function() {

		},
		showMonthlyCost : function(event) {
			// set on parent
			event.preventDefault();
			this.options.eventManager.trigger('view:showMonthlyCost');
		},
		showPaycheckCost : function(event) {
			// set on parent
			event.preventDefault();
			this.options.eventManager.trigger('view:showPaycheckCost');
		},
		showProducts : function(event) {
			// change by rsingh for user stories 12, 13 & 15
			//$('#planWrap').css('cssText','width: 1025px !important'); 
			//$('#planWrap').find('#contentSidebar').css('cssText','width: 238px !important','float: left !important');
			//$('#planWrap').find('#contentSection').css('cssText','width: 774px !important','float: left !important');
			//$('.content-toolbar.actions-row').css('cssText','width:1016px !important'); 
			
			/*NE Branding: becuase of width plan page UI is distorted */
			
			this.options.eventManager.trigger('view:showProductList');
			event.preventDefault();
			
		},
		showCompare : function(event) {
			this.options.eventManager.trigger('view:showTopProducts');
			event.preventDefault();
		},
		showDetail : function(event) {
			// change by rsingh for user stories 12, 13 & 15
			//$('#planWrap').css('cssText','width: 100% !important');
			//$('#planWrap').find('#contentSidebar').css('cssText','width: 0% !important');
			//$('#planWrap').find('#contentSection').css('cssText','width: 100% !important');
			this.options.eventManager.trigger('view:showProductDetail');
			event.preventDefault();
			//$('.content-toolbar.actions-row').css('cssText','width:100% !important');
			
			/*NE Branding: becuase of width plan page UI is distorted */
		},
		toggleProductLine : function(event) {
			if ($('._productLineDropDown', self.el).children('div').length > 0) {
				$('._productLineDropDown', self.el).toggle();
				$('._productLineSelector', self.el).toggleClass("active");
			}
			event.preventDefault();
		},
		selectProductLine : function(event) {
			event.preventDefault();
			$('._productLineDropDown', self.el).hide();
			$('._productLineSelector', self.el).removeClass("active");
			this.options.eventManager.trigger('view:productLineChanged', event);
		}
	});

	return ProductCompareToolBarView;
});