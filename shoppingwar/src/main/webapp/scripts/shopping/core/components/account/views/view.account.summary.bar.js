/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/account/view.summary.bar.dust', ], function(require, Backbone, templateEngine,
		connecture, template) {

	templateEngine.load(template, 'view.summary.bar');
// by rsingh for user stories 228 & 230 
	var SummaryBarView = connecture.view.Base.extend({
		className : 'loadable-nav',
		template : 'view.summary.bar',
		events : {
			'click ._editPreferences' : 'edit',
			'click ._closePreferences' : 'closePreferences',
			'click ._showQuestions' : 'showQuestions'
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.summaryDetailsVisible = options.summaryDetailsVisible;
			this.questionsAnswered = options.questionsAnswered;
			this.coverageType = options.coverageType;
		},
		render : function() {
			var data = {
				summaryDetailsVisible : this.summaryDetailsVisible,
				questionsAnswered : this.questionsAnswered
			};
			
			if(this.coverageType == "STH" || this.coverageType == "DEN"){
				$("#account-component").hide();
			}
			
			$(this.el).html(this.renderTemplate({
				data : data,
			}));

			return this;
		},
		edit : function() {
			this.eventManager.trigger('view:summary:edit');
		},
		closePreferences : function() {
			this.eventManager.trigger('view:summary:close');
		},
		showQuestions : function(){
			this.eventManager.trigger('view:summary:show:questions');
		}
	});

	return SummaryBarView;
});
