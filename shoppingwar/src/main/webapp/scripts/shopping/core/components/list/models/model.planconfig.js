/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {
	var CostEstimatorCalculatorModel = Backbone.Model.extend({});

	return CostEstimatorCalculatorModel;
});