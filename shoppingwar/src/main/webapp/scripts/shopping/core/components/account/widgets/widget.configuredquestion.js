/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/widgets/widget.question',
		'text!html/shopping/components/account/widget.configuredquestion.dust', 'text!html/shopping/components/account/view.question.checkbox.dust',
		'text!html/shopping/components/account/view.question.dropdown.dust', 'text!html/shopping/components/account/view.question.radiobutton.dust',
		'text!html/shopping/components/account/view.question.slider.dust', 'text!html/shopping/components/account/widget.configuredquestion.summaryview.dust', 'jquery.selectToUISlider' ], function(
		require, Backbone, templateEngine, dustHelpers, connecture, QuestionWidget, questionTemplate, checkboxQuestionTemplate, dropdownQuestionTemplate, radiobuttonQuestionTemplate,
		sliderQuestionTemplate, summaryViewTemplate) {

	templateEngine.load(questionTemplate, "widget.configuredquestion");
	templateEngine.load(checkboxQuestionTemplate, "view.question.checkbox");
	templateEngine.load(dropdownQuestionTemplate, "view.question.dropdown");
	templateEngine.load(radiobuttonQuestionTemplate, "view.question.radiobutton");
	templateEngine.load(sliderQuestionTemplate, "view.question.slider");
	templateEngine.load(summaryViewTemplate, "widget.configuredquestion.summaryview");

	var configuredQuestionModel = Backbone.Model.extend({});

	var ConfiguredQuestionSummaryView = connecture.view.Base.extend({
		template : 'widget.configuredquestion.summaryview',
		initialize : function(options) {
			this.summaries = options.summaries;
			this.questionRefId = options.questionRefId;
			this.title = options.title;
			this.member = options.member;
		},
		render : function() {
			$(this.el).html(this.renderTemplate({
				data : {
					summaries : this.summaries,
					questionRefId : this.questionRefId,
					title : this.title,
					member : this.member
				}
			}));

			return this;
		}
	});

	var ConfiguredQuestionView = connecture.view.Base.extend({
		template : "widget.configuredquestion",
		checkboxTemplate : "view.question.checkbox",
		dropdownTemplate : "view.question.dropdown",
		radiobuttonTemplate : "view.question.radiobutton",
		sliderTemplate : "view.question.slider",
		templateLookup : {
			"Radio Button" : "radiobuttonTemplate",
			"Check Box" : "checkboxTemplate",
			"Dropdown" : "dropdownTemplate",
			"Slider" : "sliderTemplate"
		},
		events : {
			"slidestop .ui-slider" : "questionValueSelected",
			"change ._questionSelection" : "questionValueSelected",
			"change .checkBoxFields > .clearfix > ._questionValue > input" : "questionValueSelected",
			"change .radioFields > .clearfix > ._questionValue > input" : "questionValueSelected"
		},
		initialize : function(options) {
			// This is kind of temp until we can get rid of the configuredQuestionModel
			this.widgetConfig = options.widgetConfig;
		},
		render : function() {
			var self = this;

			var localData = _.extend({}, {
				"account" : this.model.get("account").toJSON(),
			}, this.widgetConfig.toJSON().config);

			var viewHTML = this.renderTemplate({
				"data" : localData,
				"functions" : {
					renderQuestion : function(chunk, context, bodies, params) {
						self.renderQuestion(chunk, context, bodies, params);
					}
				}
			});

			self.el.className = "_questionWidget";
			$(self.el).html(viewHTML);

			var displayType = this.widgetConfig.get("config").displayType;
			this.adjustForMinMax(displayType);

			this.bindQuestionRenderer();
			
			$('.helpicon span[title]', self.el).tooltip();

			return self;
		},
		renderQuestion : function(chunk, context, bodies, params) {
			var self = this;

			var displayType = context.get("displayType");
			var data = this.widgetConfig.toJSON().config;
			data.responseValue = this.model.get('responseValue');
			var questionHTML = this.renderTemplate({
				"template" : self.templateLookup[displayType],
				"data" : data
			});

			chunk.write(questionHTML);
		},
		bindQuestionRenderer : function() {
			var self = this;

			var displayType = this.widgetConfig.get("config").displayType;

			if (displayType == "Slider") {
				$('.question-section > .dataGroup > ._questionSelection', self.el).selectToUISlider().hide();

				var answers = this.widgetConfig.get("config").answers;
				var disableTooltip = true;
				for ( var a = 0; a < answers.length && disableTooltip; a++) {
					if (answers[a].helpText) {
						disableTooltip = false;
					}
				}

				if (disableTooltip) {
					$('.question-section > .dataGroup .ui-slider-tooltip', self.el).remove();
				}
			}
		},
		adjustForMinMax : function(displayType) {
			// Currently unused
			// var minimumAnswers = this.model.get("minimumAnswers");
			var maximumAnswers = this.widgetConfig.get("config").maximumAnswers;

			// Only apply when it's a checkbox, all others are single value only.
			if (displayType == "Check Box" && maximumAnswers) {
				var questionResponse = this.getQuestionResponse();
				if (questionResponse) {
					var values = questionResponse.value;
					var answers = this.widgetConfig.get("config").answers;
					for ( var a = 0; a < answers.length; a++) {
						var isAnswerSelected = false;
						for ( var v = 0; v < values.length && !isAnswerSelected; v++) {
							isAnswerSelected = values[v] == answers[a].answerId;
						}

						// Enabled/Disable answers based on selection matching the maximum allowed
						$('.dataGroup .checkBoxFields ._questionValue #' + answers[a].answerId, this.el)[0].disabled = !isAnswerSelected && values.length == maximumAnswers;
					}
				}
			}
		},
		getQuestionResponse : function() {
			var questions = this.model.get("account").get("questions");
			var questionRefId = this.widgetConfig.get("config").questionRefId;

			var questionResponse = null;

			// Ensure there are actually responses before trying to find the response
			if (questions) {
				for ( var qr = 0; qr < questions.length && null == questionResponse; qr++) {
					if (questions[qr].questionRefId == questionRefId) {
						questionResponse = questions[qr];
					}
				}
			}

			return questionResponse;
		},
		questionValueSelected : function(event) {
			// since this is a simple internal event, just trigger it on the view for now
			this.trigger('view:question:selected', event);
		}
	});

	var ConfiguredQuestionWidget = function(options) {
		QuestionWidget.call(this, options);
		this.eventManager = options.eventManager;
		this.configuredQuestionModel = new configuredQuestionModel(options.attributes);
	};

	ConfiguredQuestionWidget.prototype = Object.create(QuestionWidget.prototype);

	ConfiguredQuestionWidget.prototype.start = function() {
		this.view = new ConfiguredQuestionView({
			model : this.configuredQuestionModel,
			widgetConfig : this.model,
			eventManager : this.eventManager
		});

		this.view.on('view:question:selected', this.questionValueSelected, this);
	};

	ConfiguredQuestionWidget.prototype.questionValueSelected = function(event) {
		var self = this;

		var $selectedValue = $(event.currentTarget);

		var displayType = this.model.get("config").displayType;

		var responseValue = this.getResponseValue(displayType, $selectedValue);

		this.setResponseValue(responseValue);

		this.view.adjustForMinMax(displayType);

		this.persistQuestions();
	};

	ConfiguredQuestionWidget.prototype.adjustForMinMax = function(displayType) {
		// Currently unused
		// var minimumAnswers = this.model.get("minimumAnswers");
		var maximumAnswers = this.model.get("config").maximumAnswers;

		// Only apply when it's a checkbox, all others are single value only.
		if (displayType == "Check Box" && maximumAnswers) {
			var questionResponse = this.getQuestionResponse();
			var values = questionResponse.value;
			var answers = this.model.get("config").answers;
			for ( var a = 0; a < answers.length; a++) {
				var isAnswerSelected = false;
				for ( var v = 0; v < values.length && !isAnswerSelected; v++) {
					isAnswerSelected = values[v] == answers[a].answerId;
				}

				// Enabled/Disable answers based on selection matching the maximum allowed
				$('.dataGroup .checkBoxFields ._questionValue #' + answers[a].answerId, this.el)[0].disabled = !isAnswerSelected && values.length == maximumAnswers;
			}
		}
	};

	ConfiguredQuestionWidget.prototype.getResponseValue = function(displayType, value) {
		var responseValue;

		if (displayType == "Slider") {
			responseValue = value.prev('select').children('option:selected').attr('data-id');
		} else if (displayType == "Dropdown") {
			responseValue = value.children('option:selected').attr('data-id');
		} else if (displayType == "Check Box") {
			responseValue = value.attr('data-id');
		} else if (displayType == "Radio Button") {
			responseValue = value.attr('data-id');
		}

		return responseValue;
	};

	ConfiguredQuestionWidget.prototype.setResponseValue = function(value) {
		var account = this.configuredQuestionModel.get("account");
		if (!account.get("questions")) {
			account.set({
				"questions" : []
			}, {
				silent : true
			});
		}

		var questionResponse = this.getOrCreateQuestionResponse();

		var questionRefId = this.model.get("config").questionRefId;
		var displayType = this.model.get("config").displayType;

		if (!questionResponse.value) {
			var values = value;
			if (displayType == "Check Box") {
				values = [ value ];
			}

			questionResponse.value = values;
		} else {
			var values = questionResponse.value;

			if (displayType == "Check Box") {
				var existingValueIndex = null;
				for ( var v = 0; v < values.length && existingValueIndex == null; v++) {
					if (values[v] == value) {
						existingValueIndex = v;
					}
				}

				if (existingValueIndex == null) {
					// Value was added
					values[values.length] = value;
				} else {
					// Value was removed
					values.splice(existingValueIndex, 1);
				}
			} else {
				values = value;
			}

			questionResponse.value = values;
		}

		this.configuredQuestionModel.set({
			"responseValue" : questionResponse.value
		}, {
			silent : true
		});
	};

	ConfiguredQuestionWidget.prototype.clearCurrentQuestion = function() {
		this.clearQuestionResponse();

		this.configuredQuestionModel.unset("responseValue");

		this.render();

		this.persistQuestions();
	};

	ConfiguredQuestionWidget.prototype.getSummaryView = function(options) {

		var summaries = [];

		var responseValue = options.responseValue;

		if ($.isArray(responseValue)) {
			for ( var r = 0; r < responseValue.length; r++) {
				var summaryText = QuestionWidget.prototype.getSummaryTextOfValue(options.questionRefId, responseValue[r], options.answers);
				if (summaryText && "" != summaryText.trim()) {
					summaries.push(summaryText);
				}
			}
		} else {
			var summaryText = QuestionWidget.prototype.getSummaryTextOfValue(options.questionRefId, responseValue, options.answers);
			if (summaryText && "" != summaryText.trim()) {
				summaries.push(summaryText);
			}
		}

		return new ConfiguredQuestionSummaryView({
			summaries : summaries,
			questionRefId : options.questionRefId,
			member : options.member,
			title : options.title
		});
	};

	ConfiguredQuestionWidget.prototype.render = function() {
		return this.view.render().el;
	};

	ConfiguredQuestionWidget.prototype.getView = function() {
		return this.view;
	};

	ConfiguredQuestionWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return ConfiguredQuestionWidget;
});