/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/views/base', 'text!html/shopping/components/header/view.header.welcome.dust' ], function(require, Backbone, templateEngine,
		dustHelpers, BaseView, headerTemplate) {

	templateEngine.load(headerTemplate, "view.header.welcome");

	HeaderWelcomeView = BaseView.extend({
		template : "view.header.welcome",
		events : {
			'click a._myAccount' : 'toMyAccount',
			'click a._accountSettings' : 'toAccountSettings',
			'click ._logout' : 'logout',
			'click ._backButton' : 'back'
		},
		initialize : function() {
			var self = this;
			this.eventManager = self.options.eventManager;
		},
		render : function() {
			var self = this;
			var data = self.options.data;
			data.config = this.options.configData;

			var renderedHTML = this.renderTemplate({
				data : self.options.data
			});

			$(self.el).html(renderedHTML);

			return this;
		},
		logout : function(event) {
			// event.preventDefault();
			this.eventManager.trigger('view:logout');
		},
		back : function() {
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.configData.myAccountURL
			});
		},
		toMyAccount : function(event) {
			event.preventDefault();
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.configData.myAccountURL
			});
		},
		toAccountSettings : function(event) {
			event.preventDefault();
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.configData.userProfileURL,
				// this is needed for whatever reason...lol
				params : {
					fromUserProfile : true
				}
			});
		},
	});

	return HeaderWelcomeView;
});