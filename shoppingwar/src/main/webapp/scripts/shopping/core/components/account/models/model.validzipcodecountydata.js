/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {
	var ValidZipCodeCountyDataModel = ProxyModel.extend({
		initialize : function(options) {
			var zipCode = options.zipCode ? options.zipCode : '';

			ProxyModel.prototype.initialize.call(this, {
				storeItem : {
					item : 'validZipCodeCountyData',
					params : [ 
					    { key : "zipCode", value : zipCode } 
					]
				}
			});
		}
	});

	return ValidZipCodeCountyDataModel;
});