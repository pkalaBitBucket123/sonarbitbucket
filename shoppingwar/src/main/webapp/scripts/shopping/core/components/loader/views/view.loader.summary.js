/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/loader/view.loader.summary.dust' ], function(require, Backbone, templateEngine,
		connecture, summaryTemplate) {

	templateEngine.load(summaryTemplate, 'view.loader.summary');

	var BaseLoaderSummaryView = connecture.view.Base.extend({
		template : 'view.loader.summary',
		tagName : "div",
		data : {},
		events : {
			'click ._engage' : 'engage'
		},
		initialize : function(options) {
// Changes for SHSBCBSNE-5 and 6
			var self = this;
			this.eventManager = options.eventManager;
			this.models = options.models;
			
			try{			
				$("a[name=denCoverage]").on('click', function(){
					
					self.models.account.set("coverageType", "DEN");	
					self.models.account.set("covType", "DEN");	
					self.models.account.set("childOnly", "No");	
					self.eventManager.trigger('view:component:welcomeDone');
				});
				$("a[name=tempCoverage]").on('click', function(){
					
					self.models.account.set("coverageType", "STH");	
					self.models.account.set("covType", "STH");	
					self.models.account.set("childOnly", "No");	
					self.eventManager.trigger('view:component:welcomeDone');
				});
				$("a[name=planfinder]").on('click', function(){
					var url = "../planfinder/quote.html";
					if(self.models.account.has("userRole")){
						if(self.models.account.get("userRole").indexOf("fb")==0){
							var userRole = "fb";
							url = "../planfinder/quote.html?profile=fbifp";
						}}
					
						$(location).attr('href',url);
				});
				$("a[name=specialEnroll]").on('click', function(){
				// start changes for SHSBCBSNE-1466	
                   if( !(self.models.account.has("userRole") && self.models.account.get("userRole").indexOf("fb")==0) ){	
                	   self.models.account.set("specialEnroll", "Yes");							
                	   self.eventManager.trigger('view:component:welcomeDone');
                   }
                // end changes for SHSBCBSNE-1466   
				});
								
			
			}catch(e){}
		},
		render : function() {
			var self = this;

			$(self.el).html(this.renderTemplate({
				data : this.data
			}));
			
			this.postRender();
			
			return this;
		},
		postRender : function() {
		},
		engage : function() {
			this.models.account.set("covType", "IFP");	
			this.models.account.set("childOnly", "No");	
			this.eventManager.trigger('view:component:welcomeDone');
		}
	});

	return BaseLoaderSummaryView;
});
