/**
 * Copyright (C) 2012 Connecture
 * 
 * Going to pull in all of the views for now to get it rendering, but we might want to end up breaking this out into multiple components, and have sub components of components
 * 
 * 
 */
 	//<BCBSNE US=RIDERS FOR PROPOSAL MI>
define([ 'module', 'log', 'backbone', 'templateEngine','common/component/component','text!html/shopping/components/quote/view.quote.finalize.dust'], function(module, log, Backbone, templateEngine,connecture, quoteTemplate) {

	// These two are for the main component view
	templateEngine.load(quoteTemplate, 'view.quote.finalize');

	var FinalizeQuoteView = connecture.component.core.View.extend({
		template : 'view.quote.finalize',
		
		// Right now it will handle navigation events, we will need
		// the global event for the component to handle
		// going right or left
		events : {
			'click ._addRidersToQuote' : 'addRidersToQuote',
			'click ._removeRiderFromQuote' : 'removeRiderFromQuote',
			'click ._quoteSave' : 'saveQuote',
		},
		initialize : function(options) {
			var self = this;	
			
		},
		saveQuote: function(){
			this.eventManager.trigger('view:quoteReceipt:save');
		},
		
		removeRiderFromQuote : function(event){
			var linkElement = event.currentTarget;
			var riderId = $('._selRiderId', linkElement).val();
			var planId =  $(event.target).attr('plan-id');
			
			var selectedRiders = {};
			if(this.models.quote.get('selectedRiders'))
				selectedRiders = this.models.quote.get('selectedRiders');
			
			if(selectedRiders['DENTAL']){
				delete selectedRiders['DENTAL'][planId];
			}
			this.render();
		},
		addRidersToQuote : function(event){
			var linkElement = event.currentTarget;
			var riderId = $('._selRiderId', linkElement).val();
			var planId =  $(event.target).attr('plan-id');
			event.preventDefault();
			var selectedRiders = {};
			if(this.models.quote.get('selectedRiders'))
				selectedRiders = this.models.quote.get('selectedRiders');
			
			if(!selectedRiders['DENTAL'])
				selectedRiders['DENTAL'] = {};
				
			selectedRiders['DENTAL'][planId] =riderId; 
			
			this.models.quote.set('selectedRiders', selectedRiders);
			this.render();
		},
		saveQuote : function() {
			var self = this;

			if (this.hasTransactionId()) {
				this.models.quote.save({}, {
					failure : function() {
						// TODO Display an error to the user?
						// Roll back the toggle?
					},
					success : function() {
						self.saveAndExit();
					}
				});
			} else {
				this.saveAndExit();
			}
		},

		saveAndExit : function() {
			var priorToApplyURL = this.models.applicationConfig.get("ffmPriorToApplyURL");
			if (priorToApplyURL) {
				var params = this.assembleParams();
				this.eventManager.trigger('exit', {
					key : 'back',
					location : priorToApplyURL,
					params : params
				});
			}
		},
		hasTransactionId : function() {
			var url = $.url.parse();
			return url.params && url.params.hasOwnProperty("transactionId");
		},
		assembleParams : function() {
			var params = {};
			var plans = this.models.quote.get("plans");
			if(plans.length>1) {
				   for (var i = 0; i < plans.length; i++) {
					  for(var j =i+1;j<plans.length;){
				         if (plans[j].planId == plans[i].planId) {
					        plans.splice(plans, j); 
				         }
				         else {
					        j++;
				         }
					  }
				   }
				}	
			var selectedRiders = {};
			if(this.models.quote.get('selectedRiders'))
				selectedRiders = this.models.quote.get('selectedRiders');			
			var riderMap ='';
			if (plans.length > 0) {
				var quotePlanIdList = "";
				for ( var i = 0; i < plans.length; i++) {
					quotePlanIdList = quotePlanIdList + plans[i].planId;
					if (i < plans.length - 1) {
						quotePlanIdList = quotePlanIdList + "%2C";
					}
					if(selectedRiders['DENTAL'] && selectedRiders['DENTAL'][plans[i].planId]){
						if(!params.ridersMap)
						params.ridersMap = {};
						params.ridersMap[plans[i].planId] = selectedRiders['DENTAL'][plans[i].planId]
						
					}
				}
				params.quotePlanIds = quotePlanIdList;
			}
			return params;
		},
		render : function() {
			var self = this;
			if(!this.models || !this.models.quote.get('plans')){
				$('._finalizeQuotePlan').hide();
				return;
			}
			if( this.models){
				var childOnly=this.models.account.get("childOnly");
			}
			$('._list').hide();
			$('._summaryBar').hide();
			$('._acccount').hide();
			if(!this.models)
				return this;
			var plans = new Array();
			var quoteData = {};			
			//var selectedProductLine = {};
			if(_.size(this.models.quote.get('plans'))>0)
			quoteData.productLines={};
			quoteData.childOnly=childOnly;

			var selectedRiders = {};
			var riders ={};
			if(this.models.quote.get('selectedRiders'))
				selectedRiders = this.models.quote.get('selectedRiders');
			
			if(selectedRiders['DENTAL'])
				riders = selectedRiders['DENTAL'];
			
			var self =this;
			_.each(this.models.quote.get('plans'), function(planId) {
				_.each(self.models.plans.models, function(plan){
					if(plan.get('planId') == planId.planId){					
						//plans[plans.length] = plan;
						if(!quoteData.productLines[plan.get('productLineCode')] || !quoteData.productLines[plan.get('productLineCode')].products ){
							quoteData.productLines[plan.get('productLineCode')] = {};
							quoteData.productLines[plan.get('productLineCode')].code = plan.get('productLineCode');
							quoteData.productLines[plan.get('productLineCode')].name = plan.get('productLine');
							quoteData.productLines[plan.get('productLineCode')].products = [];
							quoteData.childOnly=childOnly;
						}
						
						var planJson = plan.toJSON();
						if(riders[plan.get('planId')]){
							var rider = null;
							$.each(planJson.riders, function(i,rid){
								if(rid.riderId == riders[plan.get('planId')]){
									rider =rid;
									return;
								}
							});
							planJson['selectedRider']= rider ;
							
						}
						quoteData.productLines[plan.get('productLineCode')].products[quoteData.productLines[plan.get('productLineCode')].products.length]= planJson;
						quoteData.childOnly=childOnly;
					}
				});
			});
			
			$(this.el).html(this.renderTemplate({				
				data : quoteData,
				childOnly :childOnly
			}));
			return this;
		},
		close : function() {

		},
		
	});



	/**
	 * TODO: We should only be maintaining one product list object for the life time of this component, for the main component list view.
	 * 
	 * This is because we will also want to go back to what is sorted and/or filtered if we switch views
	 * 
	 * We will want a sorting object or sort configuration object. Basically something that will sit at the collection layer to see what sort is active or should be used per collection. These will get
	 * turned on and off independently of the collection, or from the outside, say from an event
	 * 
	 */
	var FinalizeQuoteComponent = function(configuration) {
		var self = this;
		// The id is coming from component configuration
		// client side, we need to make sure to merge this in
		configuration = _.extend(configuration, {
			
		});

		connecture.component.core.Base.call(this, configuration);
		
		this.items =  [ {
			alias : 'account',
			dependency : true,
		}, {
			alias : 'plans',
			dependency : true,
		}, {
			alias : 'quote',
			dependency : true
		},
		{
			alias : 'applicationConfig',
			dependency : true
		}];
		// //////////////////////////////////////
		// Store items
		// /////////////////////////////////////
				// handle events that may come from other components here:
		this.subscriptions = _.extend({}, this.subscriptions, {
			'component:finalizeQuote' : function(id) {
				this._showFinalizeQuote();
			},
		});

		this.publish = _.extend({}, this.publish, {
			'view:finalizeQuote' : 'component:finalizeQuote',
		});
		this.configuration.layout = _.extend({}, this.configuration.layout, {
			View : FinalizeQuoteView,
			options : {
				renderNavigation : true
			},
			regions : {
				'navigation' : '.content-navigation',
				'tools' : '.content-tools',
				'content' : '._finalizeQuotePlan',
				'contentSidebar' : '.content-sidebar'
			}
		});

		this.routes = [ {
			name : '',
			route : 'view/' + this.id + '/detail/:finalizeQuote',
			callback : function(productId) {
				// the route callback must return the properties
				// around this route, this will come back through
				// the start/update events
				return {
					id : self.id,
					view : 'finalizeQuote',
					
				};
			}
		}];		
	};
	FinalizeQuoteComponent.prototype = Object.create(connecture.component.core.Base.prototype);
	FinalizeQuoteComponent.prototype._showFinalizeQuote = function() {
		
		var view = new FinalizeQuoteView();
		view.models = this.models;
		view.eventManager=this.eventManager;
		this.regionManager.content.$el=$('._finalizeQuotePlan');
		this.regionManager.content.show(view);
                $("._componentRegion._cart").hide();
	};
	FinalizeQuoteComponent.prototype.registerEvents = function() {

		this.eventManager.on("start", function(options) {
			var self = this;
		}, this);
this.eventManager.on('view:quote:close', function() {
			// Release our seize request
			this.eventManager.trigger('component:release');
			// then minimize the cart
			this.hide();
			Backbone.history.navigate('view/' + this.id + '/list');
			$("._componentRegion._cart").hide();
			
			$('._quoteDropdown', self.el).css({
				'height' : '0px',
				'opacity' : '0'
			});
			
			$("._componentRegion._acccount").show();
			$("._summaryBar").show();
		}, this);
	};
	return FinalizeQuoteComponent;
});