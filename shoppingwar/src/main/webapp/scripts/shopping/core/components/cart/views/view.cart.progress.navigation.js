/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/cart/view.cart.progress.navigation.dust' ], function(require, Backbone, templateEngine,
		connecture, template) {

	templateEngine.load(template, 'view.cart.progress.navigation');

	// These are the workflows
	var ProgressNavigation = connecture.view.Base.extend({
		template : 'view.cart.progress.navigation',
		tagName : 'li',
		events : {
			'click a' : 'show'
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.componentId = options.componentId;
			this.account = options.account;
		},
		render : function() {
			var self = this;

			var data = {};

			if (this.account.get('progress') && this.account.get('progress').cart) {
				data.visited = true;
			}

			$(self.el).html(this.renderTemplate({
				"data" : data
			}));

			return this;
		},
		show : function() {
			Backbone.history.navigate('view/' + this.componentId + '/open', {
				trigger : true
			});
		}
	});

	return ProgressNavigation;
});