/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/list/view.product.list.empty.dust' ], function(require, Backbone, templateEngine,
		connecture, template) {
	templateEngine.load(template, 'view.product.list.empty');

	var ProductListEmptyView = connecture.view.Base.extend({
		template : 'view.product.list.empty',
		initialize : function() {

		},
		render : function() {
			var self = this;
			$(self.el).html(this.renderTemplate());
			$('a[title]', self.el).tooltip();

			return this;
		}
	});

	return ProductListEmptyView;
});