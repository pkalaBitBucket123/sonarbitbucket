/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'dust', 'dust-extensions' ], function(require, dust, dustExtensions) {

	dust.helpers['benefitValue'] = function(chk, ctx, bodies, params) {

		var obj = ctx.current();

		if (params.categoryName) {
			var category = obj.benefitCategories[params.categoryName];

			if (params.benefitCode) {
				var benefit = category[params.benefitCode];

				if (params.valueCode) {
					var benefitValue = benefit.valueMap[params.valueCode];

					var returnValue = "";

					if (benefitValue.numericValue) {
						returnValue = benefitValue.numericValue;
					} else {
						returnValue = benefitValue.value;
					}

					chk = chk.render(bodies.block, ctx.push({
						benefitValue : returnValue
					}));

				}
			}
		}

		return chk;
	};

	dust.helpers['aptcApplied'] = function(chk, ctx, bodies, params) {

		var costContext = ctx.get("cost");

		var aptcApplied = (costContext.monthly.subsidyData && costContext.monthly.subsidyData.appliedSubsidy && costContext.monthly.subsidyData.appliedSubsidy > 0);

		if (!aptcApplied) {
			chk = chk.render(bodies['else'], ctx);
		} else {
			chk = chk.render(bodies.block, ctx);
		}

		return chk;
	};

	dust.helpers['calculatePlanCost'] = function(chunk, context, bodies, params) {
		var costContext = context.get("cost");

		// set monthly
		var monthlyRate = costContext.monthly.rate;
		var monthlyContribution = costContext.monthly.contribution;

		var monthlyContributionAmount = 0;

		if (monthlyContribution) {
			monthlyContributionAmount = monthlyContribution.amount;

			// don't let contribution appear larger than rate,
			// giving a negative value
			if (monthlyContributionAmount > monthlyRate.amount) {
				monthlyContributionAmount = monthlyRate.amount;
			}
		}

		var monthlyCost = monthlyRate.amount - monthlyContributionAmount;
		
		// get cost estimator values; apply aptc if available
		var estimatedMonthlyCost = 0;
		if (costContext.monthly.subsidyData){
			estimatedMonthlyCost = costContext.monthly.subsidyData.netRate;
		} else {
			estimatedMonthlyCost = monthlyCost;
		}

		// set the yearly plan cost
		var planYearCost = estimatedMonthlyCost * 12;
		var maximumCost = context.get("maximumCost") + planYearCost;
		var maximumIsActual = context.get("maximumIsActual");
		var estimatedCost = context.get("estimatedCost") + planYearCost;
		if (maximumIsActual && estimatedCost > maximumCost) {
			estimatedCost = maximumCost;
		}

		var estimatedCostPercentage = 0;
		if (maximumIsActual) {
			estimatedCostPercentage = 40 + (60 * (estimatedCost - planYearCost)) / maximumCost;
		} else {
			estimatedCostPercentage = 40 + (60 * (estimatedCost - planYearCost)) / estimatedCost;
		}

		// TODO | this should be one call to chunk.render
		// with an object containing multiple other objects;
		// creation of a root result object would cause a change in all
		// current dust templates showing this cost calculation;
		// make change when time allows

		// set paycheck cost, if available
		if (costContext.perPaycheck) {
			var perPaycheckCost = costContext.perPaycheck;
			var perPaycheckRate = perPaycheckCost.rate;
			var perPaycheckContribution = perPaycheckCost.contribution;
			if (perPaycheckContribution.amount > perPaycheckRate.amount) {
				perPaycheckContribution = perPaycheckRate;
			}

			var perPaycheckCost = perPaycheckRate.amount - perPaycheckContribution.amount;

			chunk = chunk.render(bodies.block, context.push({
				monthlyCost : monthlyCost,
				perPaycheckCost : perPaycheckCost,
				planYearCost : planYearCost,
				estimatedCost : estimatedCost,
				maximumCost : maximumCost,
				maximumIsActual : maximumIsActual,
				estimatedCostPercentage : estimatedCostPercentage
			}));
		} else {
			// no perPaycheck cost

			chunk = chunk.render(bodies.block, context.push({
				monthlyCost : monthlyCost,
				planYearCost : planYearCost,
				estimatedCost : estimatedCost,
				maximumCost : maximumCost,
				maximumIsActual : maximumIsActual,
				estimatedCostPercentage : estimatedCostPercentage
			}));
		}

		return chunk;
	};

	dust.helpers['categoryDetailBenefits'] = function(chk, ctx, bodies, params) {

		// build a list of "benefitsCategories" with "benefits", up to a certain limit
		var count = 0;
		var limit = 0;
		var toggleLimit = 0;
		var doToggle = false;

		if (params.limit) {
			limit = params.limit;
		}

		if (params.toggleLimit) {
			toggleLimit = params.toggleLimit;
			doToggle = true;
		}

		// this will be the result object
		var limitedCategories = new Array();

		// this will be the current (full) collection of categories and benefits
		var benefitCategories = ctx.current();

		// object should be benefitCategories
		for ( var i = 0; i < benefitCategories.length; i++) {
			var limitedCategory = {};
			limitedCategory.name = benefitCategories[i].categoryName;
			limitedCategory.benefits = null;

			var currentBenefitArray = benefitCategories[i].benefits;
			var limitedBenefitArray = {};

			var underLimitCategories = (limit > 0 && count <= limit);
			if (underLimitCategories) {

				var benefitCount = 0;

				for ( var j = 0; j < currentBenefitArray.length; j++) {

					count++;

					var underLimit = (limit > 0 && count <= limit);
					if (underLimit) {
						benefitCount++;

						// set extra if needed
						if (doToggle && count > toggleLimit) {
							currentBenefitArray[j].extra = true;
						}

						limitedBenefitArray[j] = currentBenefitArray[j];

					} else {
						// return;
					}
				}

				if (benefitCount > 0) {
					limitedCategory.benefits = limitedBenefitArray;
				}
			} else {
				// return;
			}

			if (limitedCategory.benefits) {
				limitedCategories[limitedCategories.length] = limitedCategory;
			}
		}

		chk = chk.render(bodies.block, ctx.push({
			benefitCategories : limitedCategories
		}));

		return chk;

	};

});