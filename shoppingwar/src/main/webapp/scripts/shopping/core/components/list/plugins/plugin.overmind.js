/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'backbone', 'templateEngine', 'common/plugin', 'shopping/common/models/model.product', 'shopping/common/collections/collection.product', ], function(require, log, Backbone,
		templateEngine, Plugin, ProductModel, ProductList) {

	// //////////////////////////////////////////////
	// Private Helper Methods
	// //////////////////////////////////////////////
	function addProductScore(scores, key, value, score, text, operator) {
		scores.push({
			key : key,
			value : value,
			score : score,
			text : text,
			operator : operator
		});
	}

	function addProductPreferenceMatch(matches, key, value, match) {
		matches.push({
			key : key,
			value : value,
			match : match
		});
	}

	function isScoreMatch(operator, modelValue, answerValue) {
		retVal = false;
		switch (operator) {
		case "=":
			// Ok we are going to attempt to be smart about comparisons
			var floatModelValue = parseFloat(modelValue);
			var floatAnswerValue = parseFloat(answerValue);

			// If both of them converted
			if (!isNaN(floatModelValue) && !isNaN(floatAnswerValue)) {
				answerValue = floatAnswerValue;
				modelValue = floatModelValue;
			}

			var filters = _.isArray(answerValue) ? answerValue : [ answerValue ];
			retVal = _.include(filters, modelValue);
			break;
		case ">":
			retVal = parseFloat(modelValue) > parseInt(answerValue);
			break;
		case "<":
			retVal = parseFloat(modelValue) <= parseInt(answerValue);
			break;
		case "between":
			var limits = answerValue.split('|');
			retVal = (parseFloat(modelValue) > parseFloat(limits[0])) && (parseFloat(modelValue) <= parseFloat(limits[1]));
			break;
		}

		return retVal;
	}

	function compareProducts(bestMatchProducts, product) {

		var isBetterMatch = false;

		var currentBestFitMatches = product.get("positivePreferenceMatches");

		if (currentBestFitMatches > 0) {
			// determine if the productLine has a best match;
			// see if positiveMatch for this plan is greater than stored;
			// if so, check sortScore
			var productLineBestMatch = bestMatchProducts[product.get("productLineCode")];
			if (productLineBestMatch) {
				// compare the current match product with the current product
				if (product.get("positivePreferenceMatches") > productLineBestMatch.get("positivePreferenceMatches")) {
					isBetterMatch = true;
				} else if (product.get("positivePreferenceMatches") == productLineBestMatch.get("positivePreferenceMatches")) {
					isBetterMatch = compareSortScore(product, productLineBestMatch);
				}
			} else {
				// add a new value for the product line ... current product had
				// matches while "best fit" for product line did not
				isBetterMatch = true;
			}
		} else {
			// any matches for the current product line "best fit" product?
			// if not, compare based on sortScore
			var productLineBestMatch = bestMatchProducts[product.get("productLineCode")];
			if (productLineBestMatch) {
				if (!productLineBestMatch.get("positivePreferenceMatches") || productLineBestMatch.get("positivePreferenceMatches") < 1) {
					isBetterMatch = compareSortScore(product, productLineBestMatch);
				}
			}
		}

		if (isBetterMatch) {
			bestMatchProducts[product.get("productLineCode")] = product;
		}

	}

	function compareSortScore(productA, productB) {
		var isBetterMatch = false;

		if (productA.get('sortScore') === productB.get('sortScore')) {
			// check
			if (productA.get('sortOrder') < productB.get('sortOrder')) {
				isBetterMatch = true;
			}

		} else {
			// check
			if (productA.get('sortScore') > productB.get('sortScore')) {
				isBetterMatch = true;
			}
		}

		return isBetterMatch;
	}

	var OvermindPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'component';
		this.id = 'Overmind';
		// this is specific to component plugins, tells us which components
		// to plug into
		this.pluggedIds = [ 'b718c2e2-2119-4053-ad7b-99b1daa45be6' ];
	};

	OvermindPlugin.prototype = Object.create(Plugin.prototype);

	OvermindPlugin.prototype.initialize = function(options) {
		Plugin.prototype.initialize.call(this, options);
		var self = this;
		// Plug in some data that this plugin requires to work
		// this.plugged.items.push({
		// alias : 'widgets',
		// dependency : true,
		// });
		//
		// this.plugged.items.push({
		// alias : 'questionEvaluations',
		// dependency : true,
		// });
		//
		// // requires the account for renewing default
		// this.plugged.items.push({
		// alias : 'account',
		// dependency : true,
		// });

		// This component handles some global events
		this.subscriptions = _.extend({}, this.subscriptions, {
			// This will trigger when account members have been updated
			'component:members:updated' : function(data) {
				// When the members change, we need to get a new list of
				// products. Since this event is running async there COULD
				// be a problem where there is an account update method
				// happening while this is happening...rare but it is a possibility

				if (self.config.process === 'realtime' || self.config.show === 'available') {
					// Instead of fetching just plans, also fetch product lines as well
					if (this.models.productLines && this.models.productLines.isEmpty()) {
						this.eventManager.trigger('get:batch', this.models.plans, this.models.productLines);
					} else {
						// no need to batch if it is just one
						// this.models.plans.fetch();
						// TODO: Trying this out because I think doing a plan fetch is broken
						// right now with the sync event
						this.eventManager.trigger('get:batch', this.models.plans);
					}
				}
			}
		});

		this.publish = _.extend({}, this.publish, {
			'view:questions:show' : 'component:questions:show'
		});
	};
	OvermindPlugin.prototype.addEvents = function() {
		// This handles on start up of the plugin, to process questions
		this.eventManager.on("initialize:before", function() {
			// do not trigger a reset here
			this.plugged.models.account.on('change:questions', function() {
				if (this.config.process === 'realtime' || this.plugged.active) {
					this.processAnswers.call(this.plugged, {
						account : this.plugged.models.account.toJSON()
					}, this.plugged.active);
				}
			}, this);

		}, this);

		this.eventManager.on("initialize:after", function() {
			// do not trigger a reset here
			this.processAnswers.call(this.plugged, {
				account : this.plugged.models.account.toJSON()
			}, false);
		}, this);

		// This will handle when we have fetched another set of
		// plans from the server
		this.eventManager.on('component:product:list:reset', function() {
			this.processAnswers.call(this.plugged, {
				account : this.plugged.models.account.toJSON()
			}, false);
		}, this);
	};

	OvermindPlugin.prototype.load = function(data) {
		// now let's make this easier to work with, do this once
		// and then we will cache it
		// Also combining some of the other data we
		// are getting from the request so it is easier to
		// deal with
		this.plugged.config = {};

		var questions = {};

		this.plugged.models.questionEvaluations.each(function(question) {

			var answers = {};
			// We can keep the product refs as an array
			_.each(question.get('answers'), function(answer) {
				answers[answer.answerRefId] = answer;
			});
			questions[question.get('questionRefId')] = question.toJSON();
			// overwrite answers
			questions[question.get('questionRefId')]['answers'] = answers;

		});

		this.plugged.questions = questions;
	};

	// right now this method should get called with the context of the component
	// being passed in
	OvermindPlugin.prototype.processAnswers = function(data, triggerReset) {

		var self = this;
		// Pull the answers out of here
		// For now, loop through the plans and plan evaluation
		// information and score all of the plans

		log.debug('sorting plans');
		// array with a questionRefId and value
		log.debug(data.account);
		var productScores = [];
		var preferenceMatches = [];
		// make sure something has been answered and we actually have plans
		// 7/3/13, I removed the check to see if have answered any questions because this is turning into
		// more for a processor for all products
		if (this.models.plans) {
			// Loop through all of the questions that have been answered
			_.each(data.account.questions, function(question) {
				// Given a question, grab the evaluation config for that question
				var questionConfig = self.questions[question.questionRefId];
				// Make sure we have something
				if (questionConfig) {
					// Now let's grab the answer config, using the id of the answer
					// that was passed in

					// Need to check if value is an array or not
					// because some questions can have multiple answers

					// The value from the question is the answer ref id
					if (_.isArray(question.value)) {
						// The value is going to be the answer ref id
						_.each(question.value, function(answerRefId) {
							// Grab the answer configuration, this will contain, answers
							// for each question that will have an object that maps
							// product ids to some MORE configuration :)
							var answerConfig = questionConfig.answers[answerRefId];

							if (answerConfig) {
								// Each answer as a series of values, it might be just a single
								// or it might be an array. If there is a single value, compare that
								// otherwise, if it is an array of values loop through those

								// then we need to figure out if it is a match or a score
								if (_.isArray(answerConfig.value)) {
									_.each(answerConfig.value, function(value) {
										// For these, right now we are assuming they are preference matches
										// so add one
										// Also attach the score but that will be processed down below
										if (value.match !== null) {
											addProductPreferenceMatch(preferenceMatches, questionConfig.key, value.value, {
												label : answerConfig.text,
												value : value.match,
												score : value.score
											});
										}
									});
								} else {
									// assuming it is a string
									// if it is a string, right now I am assuming score only
									addProductScore(productScores, questionConfig.key, answerConfig.value, answerConfig.score, answerConfig.text, answerConfig.operator);
								}
							}

						});
					} else {
						// Grab the answer configuration, this will contain, answers
						// for each question that will have an object that maps
						// product ids to some MORE configuration :)
						var answerConfig = questionConfig.answers[question.value];

						if (answerConfig) {
							// Each answer as a series of values, it might be just a single
							// or it might be an array. If there is a single value, compare that
							// otherwise, if it is an array of values loop through those

							// then we need to figure out if it is a match or a score
							if (_.isArray(answerConfig.value)) {
								_.each(answerConfig.value, function(value) {
									// For these, right now we are assuming they are preference matches
									// so add one
									// Also attach the score but that will be processed down below
									if (value.match !== null) {
										addProductPreferenceMatch(preferenceMatches, questionConfig.key, value.value, {
											label : answerConfig.text,
											value : value.match,
											score : value.score
										});
									}
								});
							} else {
								// assuming it is a string
								// if it is a string, right now I am assuming score only
								addProductScore(productScores, questionConfig.key, answerConfig.value, answerConfig.score, answerConfig.text, answerConfig.operator);
							}
						}
					}
				}
			});

			// Now that we have processed all of the question scores, lets update
			// our plans

			// "best match" products, per product line
			var bestMatchProducts = {};

			var renewingDefaultPlanId = this.models.account.get("renewingDefaultPlanId");

			_.each(this.models.plans.models, function(product) {
				var id = product.get('id');

				// set the suggested plan here
				product.set({
					"isSuggestedPlan" : id === renewingDefaultPlanId
				}, {
					silent : true
				});

				// reset "best match" value
				product.set({
					"isBestMatch" : false
				}, {
					silent : true
				});

				// Always start fresh
				product.set({
					'sortScore' : 0
				}, {
					silent : true
				});

				// reset
				product.set({
					'preferenceMatches' : []
				}, {
					silent : true
				});
				var total = 0;
				var positiveMatches = 0;
				var productPreferenceMatches = [];

				// for each product we need to loop through the scores
				// to see if
				_.each(productScores, function(score) {
					if (product.get(score.key)) {
						if (isScoreMatch(score.operator, product.get(score.key), score.value)) {

							log.debug('Scoring plan ' + id + ' with score ' + score.score + ' based on key ' + score.key + ' and value ' + score.value);

							product.set({
								'sortScore' : parseInt(product.get('sortScore')) + parseInt(score.score)
							}, {
								silent : true
							});

							// log.debug('Matching plan ' + id + ' with value ' + preferenceMatch.value + ' with a matching value of ' + preferenceMatch.match.value);
							total = total + 1;
							positiveMatches = positiveMatches + 1;
							productPreferenceMatches.push({
								label : score.text,
								value : true
							});

						} else {
							// else if it does not equal then we should add a false match
							total = total + 1;
							productPreferenceMatches.push({
								label : score.text,
								value : false
							});
						}
					}
				});

				// Now handle preference matching
				_.each(preferenceMatches, function(preferenceMatch) {
					// See if we have some sort of match
					if (product.get(preferenceMatch.key)) {
						// Make sure that the value we are checking against, is configured
						// most likely looking at id and making sure that the id exists
						if (product.get(preferenceMatch.key) === preferenceMatch.value) {
							log.debug('Matching plan ' + id + ' with value ' + preferenceMatch.value + ' with a matching value of ' + preferenceMatch.match.value);
							total = total + 1;
							if (preferenceMatch.match.value) {
								positiveMatches = positiveMatches + 1;
							}
							productPreferenceMatches.push(preferenceMatch.match);

							// If it has a score
							if (preferenceMatch.match.score) {
								log.debug('Scoring for match plan' + id + ' with value ' + preferenceMatch.value + ' with a matching value of ' + preferenceMatch.match.value + ' adding a score of '
										+ preferenceMatch.match.score);
								product.set({
									'sortScore' : parseInt(product.get('sortScore')) + parseInt(preferenceMatch.match.score)
								}, {
									silent : true
								});
							}
						}
					}

				});

				log.debug('Final score for plan ' + id + ' is ' + product.get('sortScore'));
				log.debug('Final matches for plan ' + id + ': total = ' + total + '  total positive = ' + positiveMatches);

				product.set({
					'totalPreferenceMatches' : total,
					'positivePreferenceMatches' : positiveMatches,
					'preferenceMatches' : productPreferenceMatches
				}, {
					silent : true
				});

				compareProducts(bestMatchProducts, product);

			});

			_.each(bestMatchProducts, function(bestMatchProduct) {
				bestMatchProduct.set("isBestMatch", true);
				log.debug('best plan found for ' + bestMatchProduct.get("productLineCode"));
			});

			log.debug('sorting plans complete');
			// I wonder if it is better for me to return a collection
			// in case we need to remove a plan?
			if (triggerReset) {
				this.models.plans.trigger('updated');
			}

		} else {
			// Should we go through and make sure that there
			// is no score?
			log.debug('nothing to sort, not triggering event');
		}
	};

	return OvermindPlugin;
});