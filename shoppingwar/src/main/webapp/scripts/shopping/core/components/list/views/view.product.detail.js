/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'components/list/views/view.product.base', 'text!html/shopping/components/list/view.product.detail.dust',
		'text!html/shopping/components/list/view.product.estimatedCost.dust', 'text!html/shopping/components/list/view.product.preferenceMatch.dust',
		'text!html/shopping/components/list/view.product.preferredProviders.dust' ], function(require, Backbone, templateEngine, BaseView, template, estimatedCostTemplate, preferenceMatchTemplate,
		preferredProvidersTemplate) {

	templateEngine.load(template, "view.product.detail");
	templateEngine.load(estimatedCostTemplate, 'view.product.estimatedCost');
	templateEngine.load(preferenceMatchTemplate, 'view.product.preferenceMatch');
	templateEngine.load(preferredProvidersTemplate, 'view.product.preferredProviders');

	ProductDetailView = BaseView.extend({
		template : "view.product.detail",
		initialize : function(options) {
			BaseView.prototype.initialize.call(this, options);
		},
		events : {
			'click ._selectLink' : 'selectProduct',
			'click ._selectQuoteLink' : 'selectForQuote',
			'click ._showQuestions' : 'showQuestions',
			'click ._estimateCosts' : 'estimateCosts',
			'click ._providerSearch' : 'providerSearch',
			'click .section-toggle' : 'toggleSection',
			'click ._costBreakdown' : 'showCostBreakdown'
		},
		render : function() {
			BaseView.prototype.render.call(this);
			$(this.el).removeClass('after-sidebar');

			$('.cost-estimation-chart .bar-wrapper .bar .ui-progressbar-value[title]', self.el).tooltip();

			$('._currency', self.el).formatCurrency();
			$('._costEstimateCurrency', self.el).formatCurrency({
				roundToDecimalPlace : 0
			});

			$('.details-section .equal-height', self.el).equal_height('start');

			return this;
		},
		close : function() {

		},
		selectProduct : function(event) {
			// This is going to trigger a component event to do something
			// Since this
			event.preventDefault();
			// TODO: This needs to get update to pass an id instead
			this.options.eventManager.trigger('view:selectProduct', this.model.toJSON());
		},
		selectForQuote : function(event) {
			if (!$('.btn-utility', $(event.currentTarget)).hasClass('disabled-button')) {
				this.options.eventManager.trigger('view:selectForQuote', this.model.toJSON().planId);
			}
			event.preventDefault();
		},
		estimateCosts : function(event) {
			this.options.eventManager.trigger('view:estimateCosts', {});
		},
		showQuestions : function(event) {
			this.options.eventManager.trigger('view:questions:show');
		},
		providerSearch : function() {
			this.options.eventManager.trigger('view:provider.search');
		},
		toggleSection : function(event) {
			event.preventDefault();
			var section = $(event.currentTarget).attr('data-toggle-section');
			this.trigger('view:product:section:toggle', section);
		},
		showCostBreakdown : function() {
			this.options.eventManager.trigger('view:product:rates:show', this.model.get('id'));
		}
	});

	return ProductDetailView;
});