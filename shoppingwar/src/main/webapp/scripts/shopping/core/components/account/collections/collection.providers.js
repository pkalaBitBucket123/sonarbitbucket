/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.collection', 'components/account/models/model.provider' ], function(require, Backbone, ProxyCollection, ProviderModel) {

	/**
	 * I think we want to maintain two collections of our models. We need to do this so we have one main one that stores ALL and then one that we filter against. We have to do this because we are
	 * going to be constantly adding and removing products based on what they select
	 */
	var ProvidersList = ProxyCollection.extend({
		model : ProviderModel,
		initialize : function(models, options) {
			var data = {};

			if (options && options.search) {
				data['search'] = options.search;
			}

			ProxyCollection.prototype.initialize.call(this, models, {
				storeItem : {
					item : 'providers',
					data : data
				}
			});
		},
		/**
		 * Overriding this method to make it easier to attach search data
		 * 
		 * @param data
		 */
		setData : function(data) {
			ProxyCollection.prototype.setData({
				search : data
			});
			return this;
		}
	});

	return ProvidersList;
});