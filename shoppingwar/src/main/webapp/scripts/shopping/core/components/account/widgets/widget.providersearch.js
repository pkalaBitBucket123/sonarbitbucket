/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 * TODO: I need to make sure loading works, so if they have done a search before, I need to persist that search and the results and then smash the answers into that. We can add whatever want to the
 * question widget section. We will want to add some data by members like what they selected and then also add the search results
 * 
 * Then I need to create a plugin for the list and most like another event that will be triggered when we have successfully selected an answer to handle display stuff... for this one we will just pass
 * a list of the current network ids that have been selected...then the plugin will loop through each one figured out if it is in network or not
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/widgets/widget.question', 'components/account/collections/collection.providers',
		'text!html/shopping/components/account/widget.providersearch.question.dust', 'text!html/shopping/components/account/widget.providersearch.provider.dust',
		'text!html/shopping/components/account/widget.providersearch.providers.dust', 'text!html/shopping/components/account/widget.providersearch.nomembers.dust',
		'text!html/shopping/components/account/widget.providersearch.summaryview.dust' ], function(require, Backbone, templateEngine, dustHelpers, connecture, QuestionWidget, ProviderList,
		questionTemplate, providerTemplate, providersTemplate, noMembersTemplate, summaryViewTemplate) {

	templateEngine.load(questionTemplate, "widget.providersearch.question");
	templateEngine.load(providerTemplate, "widget.providersearch.provider");
	templateEngine.load(providersTemplate, "widget.providersearch.providers");
	templateEngine.load(noMembersTemplate, "widget.providersearch.nomembers");
	templateEngine.load(summaryViewTemplate, "widget.providersearch.summaryview");

	var ProviderSearchModel = Backbone.Model.extend({});

	var ProviderSearchSummaryView = connecture.view.Base.extend({
		template : 'widget.providersearch.summaryview',
		initialize : function(options) {
			this.summaries = options.summaries;
			this.questionRefId = options.questionRefId;
			this.title = options.title;
			this.member = options.member;
		},
		render : function() {
			$(this.el).html(this.renderTemplate({
				data : {
					summaries : this.summaries,
					questionRefId : this.questionRefId,
					title : this.title,
					member : this.member
				}
			}));

			return this;
		}
	});

	var ProviderView = connecture.view.Base.extend({
		tagName : "tr",
		template : 'widget.providersearch.provider',
		initialize : function(options) {
			var self = this;
			connecture.view.Base.prototype.initialize.call(this, options);

			// loop through each questionResponses.members
			// I need to get the name out of the this.members
			// and then for each provider add members object
			// that has a selected attribute and name
			var provider = this.model.toJSON();

			this.providerMembers = [];
			_.each(options.questionResponse.members, function(questionMember) {
				var memberName = '';
				var isQuestionMember = false;
				_.each(options.members, function(member) {
					if (member.memberRefName == questionMember.memberRefName) {
						memberName = member.name;
						isQuestionMember = true;
						return;
					}
				});
				var selected = false;
				if (_.contains(questionMember.value, provider.xrefid)) {
					selected = true;
				}
				if (isQuestionMember) {
					self.providerMembers.push({
						name : memberName,
						selected : selected,
						memberRefName : questionMember.memberRefName
					});
				}
			});

		},
		render : function() {
			var self = this;
			var data = this.model.toJSON();
			// At this point, I have the provider and the members
			$(self.el).html(this.renderTemplate({
				"data" : {
					provider : data,
					members : this.providerMembers
				}
			}));
			return self;
		}
	});

	var ProvidersView = connecture.view.Base.extend({
		tagName : "table",
		template : 'widget.providersearch.providers',
		initialize : function(options) {
			connecture.view.Base.prototype.initialize.call(this, options);
			this.collection.on('reset', this.render, this);
			this.collection.on('sync', this.render, this);
			this.questionResponse = options.questionResponse;
		},
		render : function() {
			var self = this;
			if (!this.collection.isEmpty()) {
				var members = this.model.get("members");
				// this renders the main view and will
				// render each available member
				$(self.el).html(this.renderTemplate({
					"data" : {
						members : members
					}
				}));

				// Given the providers, this will render each provider
				// We supply the members so we can build out the controls
				// to select a provider per member
				this.collection.each(function(provider) {
					$('tbody', self.el).append(new ProviderView({
						model : provider,
						members : members,
						questionResponse : self.questionResponse
					}).render().el);
				});
			} else {
				// TODO: Make this cleaner
				$('tbody', self.el).append('<tr><td>No results found</td></tr>');
			}

			return self;
		}
	});

	var ProviderSearchView = connecture.view.Base.extend({
		template : "widget.providersearch.question",
		noMembers : "widget.providersearch.nomembers",
		templates : [ "template", "noMembers" ],
		events : {
			'click ._providerSearch' : 'search'
		},
		initialize : function(options) {
			connecture.view.Base.prototype.initialize.call(this, options);
			// This will store the answers by member
			this.questionResponse = options.questionResponse;
			this.widgetModel = options.widgetModel;
		},
		render : function() {
			var self = this;

			var templateId = 1;
			if (this.model.get("members").length > 0) {
				templateId = 0;
			}

			var viewHTML = this.renderTemplate({
				"template" : this.templates[templateId],
				"data" : this.widgetModel.toJSON(),
			});

			self.el.className = "_questionWidget";
			$(self.el).html(viewHTML);

			$('._providers', self.el).html(new ProvidersView({
				collection : this.collection,
				model : this.model,
				widgetModel : this.widgetModel,
				questionResponse : this.questionResponse
			}).render().el);

			return self;
		},
		search : function(event) {
			event.preventDefault();

			var searchValue = $('._providerName', self.el).val();

			if ($.trim(searchValue) !== '') {
				// Every time they search, we need to fetch a new list
				this.collection.setData(searchValue).fetch();
			}
		}
	});

	var ProviderSearchWidget = function(options) {
		QuestionWidget.call(this, options);
		this.eventManager = options.eventManager;
		this.providerSearchModel = new ProviderSearchModel(options.attributes);
		// We only want the members with the product line attributed to the Cost Estimator in the config
		this.providerSearchModel.set({
			"members" : this.getMembersForProviderLookup(options.attributes.account.get("members"))
		});
	};

	ProviderSearchWidget.prototype = Object.create(QuestionWidget.prototype);

	ProviderSearchWidget.prototype.start = function() {

		this.initializeMemberAnswers();

		var questionResponse = this.getOrCreateQuestionResponse();
		var storage = this.getQuestionStorage();

		// Create a new collection and potentially initialize
		// with existing data if they went through this before
		if (storage.providerSearchResults) {
			this.collection = new ProviderList(storage.providerSearchResults);
		} else {
			this.collection = new ProviderList();
		}

		// When this collection changes, we also need to persist
		// the results
		this.collection.on('reset', this.search, this);
		this.collection.on('sync', this.search, this);

		this.view = new ProviderSearchView({
			model : this.providerSearchModel,
			widgetModel : this.model,
			eventManager : this.eventManager,
			collection : this.collection,
			questionResponse : questionResponse
		});

		var events = {
			'change ._providerSelect' : 'selectProvider'
		};

		this.addEvents(events, this.view);

		this.persistQuestions();
	};

	ProviderSearchWidget.prototype.search = function() {
		var searchResults = this.collection.toJSON();

		this.clearQuestionResponse();
		this.initializeMemberAnswers();

		this.clearQuestionStorage();
		var storage = this.getQuestionStorage();

		storage['providerSearchResults'] = searchResults;

		this.persistQuestions();
	};

	ProviderSearchWidget.prototype.initializeMemberAnswers = function() {
		var members = this.providerSearchModel.get("members");

		var questionResponse = this.getOrCreateQuestionResponse();

		// For each member
		for ( var m = 0; m < members.length; m++) {
			var memberRefName = members[m].memberRefName;

			// Initialize the member response values
			this.getOrCreateQuestionResponseMember(questionResponse, memberRefName);
		}
	};

	/**
	 * Default all members are eligible for provider lookup
	 * 
	 * @param members
	 * @returns
	 */
	ProviderSearchWidget.prototype.getMembersForProviderLookup = function(members) {
		return members;
	};

	ProviderSearchWidget.prototype.selectProvider = function(event) {

		var $element = $(event.currentTarget);
		var isChecked = $element.is(':checked');
		var netWorkId = $element.attr('data-provider-id');
		var memberRefId = $element.attr('data-member-id');

		var account = this.attributes.account;
		if (!account.get("questions")) {
			account.set({
				"questions" : []
			}, {
				silent : true
			});
		}

		var questionResponse = this.getOrCreateQuestionResponse();
		var memberResponse = this.getQuestionResponseMember(questionResponse, memberRefId);

		if (!_.isArray(memberResponse.value)) {
			memberResponse.value = [];
		}

		if (!questionResponse.value) {
			questionResponse.value = [];
		}

		// Here we have to maintain the responses in
		// two different locations. One for the question
		// responses so they can be evaluated later and then
		// the other for the widget to persist by member
		// so we know what is selected
		if (isChecked) {
			memberResponse.value.push(netWorkId);
		} else {
			var memberIndex = memberResponse.value.indexOf(netWorkId);

			if (memberIndex !== -1) {
				memberResponse.value.splice(memberIndex, 1);
			}
		}

		// At the question level, we only care about the unique
		// provider id values, and not the member level, we need
		// to save a list of the unique provider ids, just because
		// one member removed it doesn't mean another member has it selected
		var questionValues = [];

		_.each(questionResponse.members, function(member) {
			if (_.isArray(member.value)) {
				questionValues = questionValues.concat(member.value);
			}
		});

		questionValues = _.uniq(questionValues);

		questionResponse.value = questionValues;

		// Now we need to trigger an event that says we have changed
		// a provider and pass the responses
		// TODO: This is trigger a provider change event AND a persist event which
		// is trigger an change questions event, this one should only kick off
		// one event, or we need to make sure that the save for account is only
		// kicking off events when a question actually changes
		this.eventManager.trigger('widget:provider:changed', questionResponse);

		this.persistQuestions();

	};

	ProviderSearchWidget.prototype.render = function() {
		return this.view.render().el;
	};

	ProviderSearchWidget.prototype.getView = function() {
		return this.view;
	};

	// TODO: added this override to fix an issue with provider search
	// not clearing and triggering the appropriate event on the summary page
	// however there is now duplicate code between this and clearCurrentQuestion
	ProviderSearchWidget.prototype.clearQuestionResponse = function(memberRefId) {
		// We need to clear our the member answers but we want to keep
		var questionResponse = this.getOrCreateQuestionResponse();

		// this is apparently being used as well, might be old
		// we only want to clear the ones that were cleared from the members
		questionResponse.value = [];

		if (questionResponse.members) {
			_.each(questionResponse.members, function(member) {
				if (memberRefId) {
					// TODO: This is an == on purpose, there are cases apparently when the two properties
					// are not the same type, one is a string, one is an int
					if (memberRefId == member.memberRefName) {
						member.value = [];
					}
				} else {
					// if there is no member ref id, just clear all
					member.value = [];
				}

			});
			// once we are done processing we want a unique set of
			// providers
			_.each(questionResponse.members, function(member) {
				questionResponse.value = questionResponse.value.concat(member.value);
			});

			questionResponse.value = _.uniq(questionResponse.value);

		}

		this.eventManager.trigger('widget:provider:changed', questionResponse);
	};

	ProviderSearchWidget.prototype.clearCurrentQuestion = function() {
		// We need to clear our the member answers but we want to keep
		var questionResponse = this.getOrCreateQuestionResponse();

		if (questionResponse.members) {
			_.each(questionResponse.members, function(member) {
				member.value = [];
			});
		}

		// this is apparently being used as well, might be old
		questionResponse.value = [];

		this.render();

		var questionResponse = this.getOrCreateQuestionResponse();
		this.eventManager.trigger('widget:provider:changed', questionResponse);

	};

	ProviderSearchWidget.prototype.isMemberCovered = function(member) {
		return true;
	};

	ProviderSearchWidget.prototype.getSummaryView = function(options) {

		var summaries = [];
		// still render but don't show anything
		if (this.isMemberCovered(options.member)) {
			var summaryMember = null;
			// Moving this code here cause it is specific to provider search
			if (options.members) {
				_.each(options.members, function(member) {
					// TODO: This is an == on purpose, there are cases apparently when the two properties
					// are not the same type, one is a string, one is an int
					if (member.memberRefName == options.member.get('memberRefName') && summaryMember == null) {
						summaryMember = member;
					}
				});
			}

			if (summaryMember) {
				var responseValue = summaryMember.value;
				_.each(responseValue, function(response) {
					_.each(options.responseData.providerSearchResults, function(provider) {
						if (response === provider.xrefid) {
							var name = "";
							if (provider.name.prefix && provider.name.prefix !== '') {
								name += provider.name.prefix;
							}
							if (provider.name.last && provider.name.last !== '') {
								name += ' ' + provider.name.last;
							}
							if (provider.name.first && provider.name.first !== '') {
								name += ', ' + provider.name.first;
							}

							summaries.push(name);
							return;
						}
					});

				});
			}
		}

		return new ProviderSearchSummaryView({
			summaries : summaries,
			questionRefId : options.questionRefId,
			member : options.member.toJSON(),
			title : options.title
		});
	};

	return ProviderSearchWidget;
});