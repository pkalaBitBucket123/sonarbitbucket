/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'backbone', 'templateEngine', 'common/plugin', 'components/list/models/model.availability' ], function(require, log, Backbone, templateEngine, Plugin,
		ProductAvailabilityModel) {

	var ProductAvailabilityPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'component';
		this.id = 'ProductAvailability';
		// this is specific to component plugins, tells us which components
		// to plug into
		this.pluggedIds = [ 'b718c2e2-2119-4053-ad7b-99b1daa45be6' ];
	};

	ProductAvailabilityPlugin.prototype = Object.create(Plugin.prototype);

	ProductAvailabilityPlugin.prototype.initialize = function(options) {
		Plugin.prototype.initialize.call(this, options);
		var plugin = this;
		this.plugged.listDeferreds.push(function() {
			return $.Deferred(function() {
				var self = this;

				plugin.productAvailabilityModel.set({
					plans : plugin.plugged.models.plans.toJSON()
				}, {
					silent : true
				});

				plugin.productAvailabilityModel.applyAvailability({
					silent : true,
					success : function() {
						plugin.applyAvailability(true);
						self.resolve();
					}
				});

			});
		});
	};

	ProductAvailabilityPlugin.prototype.addEvents = function() {
		// this is here because we don't have access
		// to the models at this point...should work on getting that
		// event into the life cycle so I don't have to do this
		this.eventManager.on("initialize:before", function() {
			// TODO: This is running twice when replacing to the cart...need to investigate
			this.plugged.models.account.on('change:cart', function() {
				this.productAvailabilityModel.set({
					plans : this.plugged.models.plans.toJSON()
				}, {
					silent : true
				});

				this.productAvailabilityModel.applyAvailability();
			}, this);

		}, this);

		// This will handle startup
//		this.eventManager.on("initialize:after", function() {
//			if (!this.plugged.models.plans.isEmpty()) {
//				this.productAvailabilityModel.set({
//					plans : this.plugged.models.plans.toJSON()
//				}, {
//					silent : true
//				});
//				this.productAvailabilityModel.applyAvailability();
//			}
//		}, this);
	};

	ProductAvailabilityPlugin.prototype.applyAvailability = function(triggerReset) {
		var availabilityProducts = this.productAvailabilityModel.get("plans");

		if (availabilityProducts && !_.isEmpty(availabilityProducts)) {
			var productPlans = this.plugged.models.plans;
			productPlans.each(function(product) {
				var productId = product.get("id");

				//
				var availabilityProduct = availabilityProducts[productId];

				product.set({
					availability : availabilityProduct.availabilityCode
				}, {
					silent : true
				});
			});

			if (triggerReset) {
				this.plugged.models.plans.trigger('updated');
			}
		}
	};

	ProductAvailabilityPlugin.prototype.load = function() {
		var self = this;

		// attach the model to the plugin since we don't
		// need to expose this
		this.productAvailabilityModel = new ProductAvailabilityModel();

		this.productAvailabilityModel.on('change', function() {
			self.applyAvailability(true);
		});

	};

	return ProductAvailabilityPlugin;
});