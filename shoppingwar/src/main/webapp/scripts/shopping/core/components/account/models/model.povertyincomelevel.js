/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {
	var PovertyIncomeLevelModel = ProxyModel.extend({
		initialize : function() {
			ProxyModel.prototype.initialize.call(this, {
				storeItem : 'povertyIncomeLevel'
			});
		}
	});

	return PovertyIncomeLevelModel;
});