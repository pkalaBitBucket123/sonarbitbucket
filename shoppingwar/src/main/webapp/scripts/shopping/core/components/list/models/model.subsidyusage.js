/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, backbone, ProxyModel) {
	var SubsidyUsageModel = ProxyModel.extend({
		initialize : function(options) {
			ProxyModel.prototype.initialize.call(this, {
				storeItem : {
					item : 'subsidyUsage',
					params : []
				}
			});
		},
		applySubsidy : function(options) {
			// plan collection needs to come through here
			return this.save({}, options);
		}
	});

	return SubsidyUsageModel;
});