/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'components/account/views/view.account.questionnaire.member.summary',
		'text!html/shopping/components/account/view.questionnaire.members.summary.dust' ], function(require, Backbone, templateEngine, connecture, MemberSummaryView, questionnaireSummaryTemplate) {

	templateEngine.load(questionnaireSummaryTemplate, "view.questionnaire.members.summary");

	var QuestionnaireMembersSummaryView = connecture.view.Base.extend({
		template : 'view.questionnaire.members.summary',
		initialize : function(options) {
			this.questions = options.questions;
			this.members = options.members;
			this.costEstimatorConfig = options.costEstimatorConfig;
			this.account = options.account;
		},
		render : function() {
			var self = this;
			var data = {
				title : "Member Preferences"
			};
			data.questionTitles = [];
			// now render the questions
			this.questions.each(function(question) {
				data.questionTitles.push(question.get('config').title);
			});

			// render the base template for this view
			$(self.el).html(self.renderTemplate({
				data : data
			}));

			this.members.each(function(member) {
				if (self.isMemberCovered(member)) {
					// We could probably refactor this a bit so it doe
					var memberView = new MemberSummaryView({
						model : member,
						questions : self.questions,
						costEstimatorConfig : self.costEstimatorConfig,
						account : self.account
					});

					$('table tbody', self.el).append(memberView.render().el);

					self.views.push(memberView);
				}
			});

			return this;
		}
	});

	return QuestionnaireMembersSummaryView;

});