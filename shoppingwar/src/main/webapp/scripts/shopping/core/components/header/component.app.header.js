/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the shopping application header component.
 * 
 */
define([ 'module', 'backbone', 'templateEngine', 'common/component/component', 'shopping/common/models/model.account', 'components/header/views/view.app.header',
		'components/header/views/view.app.header.logo' ], function(module, Backbone, templateEngine, connecture, AccountModel, AppHeaderView, HeaderLogoView) {

	var AppHeaderComponent = function(configuration) {
		connecture.component.core.Base.call(this, configuration);

		// need info for employer logo / name
		this.items = module.config().items;

		this.configuration.layout = _.extend({}, this.configuration.layout, {
			View : AppHeaderView,
			regions : {
				'logo' : '.logo'
			}
		});
	};

	AppHeaderComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	// register events : events coming from this component's view(s)
	AppHeaderComponent.prototype.registerEvents = function() {

		this.eventManager.on("start", function() {
			this.regionManager.logo.show(new HeaderLogoView({
				models : this.models
			}));
		}, this);

	};

	AppHeaderComponent.prototype.load = function(data) {
		connecture.component.core.Base.prototype.load.call(this, data);
	};

	AppHeaderComponent.prototype.hide = function() {
		// do nothing, always show
	};
	AppHeaderComponent.prototype.show = function() {
		// do nothing, always show
	};
	AppHeaderComponent.prototype.minimize = function() {
		// do nothing, always show
	};

	return AppHeaderComponent;
});