/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'text!html/shopping/components/menu/view.menu.mega.dust' ], function(require, Backbone,
		templateEngine, dustHelpers, connecture, template) {

	templateEngine.load(template, 'view.menu.mega');

	var ProgressBarView = connecture.view.Base.extend({
		template : 'view.menu.mega',
		events : {

		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.navigation = options.navigation;
		},
		render : function() {
			var self = this;

			$(self.el).html(this.renderTemplate({
				data : {}
			}));

			_.each(this.navigation.views, function(menu) {
				var view = new menu.View(menu.options);

				$('ul._navigation', self.el).append(view.render().el);

				self.views.push(view);
			});
			/*NE Branding*/
			var secondA = $("ul._navigation li a._showProductList", self.el); 
            secondLi=secondA.parent(); 
            secondLi.prependTo($("ul._navigation li:first", self.el).parent());
			/*NE Branding*/
			return this;
		}
	});

	return ProgressBarView;

});