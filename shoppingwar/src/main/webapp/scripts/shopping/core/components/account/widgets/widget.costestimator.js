/**
 * Copyright (C) 2012 Connecture
 * 
 * NOTE: I updated all account saves in here to be silent for now, it was causing an issue with component.product.list getting the cart data triggered as updated, apparently if you do a save an have
 * objects in your model, it also thinks those changed...this might break stuff so noting here so you know why for now
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/widgets/widget.question',
		'text!html/shopping/components/account/widget.costestimator.selection.dust', 'text!html/shopping/components/account/widget.costestimator.customization.dust',
		'text!html/shopping/components/account/widget.costestimator.nomembers.dust', 'text!html/shopping/components/account/widget.costestimator.summaryview.dust',
		'text!html/shopping/components/account/widget.costestimator.navigation.dust' ], function(require, Backbone, templateEngine, dustHelpers, connecture, QuestionWidget, selectionTemplate,
		customizationTemplate, noMembersTemplate, summaryViewTemplate, navigationTemplate) {

	templateEngine.load(selectionTemplate, "widget.costestimator.selection");
	templateEngine.load(customizationTemplate, "widget.costestimator.customization");
	templateEngine.load(noMembersTemplate, "widget.costestimator.nomembers");
	templateEngine.load(summaryViewTemplate, "widget.costestimator.summaryview");
	templateEngine.load(navigationTemplate, "widget.costestimator.navigation");

	var CostEstimatorModel = Backbone.Model.extend({});

	var CostEstimatorSummaryView = connecture.view.Base.extend({
		template : 'widget.costestimator.summaryview',
		initialize : function(options) {
			this.profileName = options.profileName;
			this.questionRefId = options.questionRefId;
			this.title = options.title;
			this.member = options.member;
		},
		render : function() {
			$(this.el).html(this.renderTemplate({
				data : {
					profileName : this.profileName,
					questionRefId : this.questionRefId,
					title : this.title,
					member : this.member
				}
			}));

			return this;
		}
	});

	var CostEstimatorView = connecture.view.Base.extend({
		selectionTemplate : "widget.costestimator.selection",
		noMembersTemplate : "widget.costestimator.nomembers",
		customizationTemplate : "widget.costestimator.customization",
		templates : [ "selectionTemplate", "customizationTemplate", "noMembersTemplate" ],
		render : function() {
			var self = this;

			var localData = {};
			// If there are members with Cost Estimator related coverage
			if (null != this.model.get("currentMember")) {
				var selectedProfile = this.model.get("selectedProfile");
				var questionResponseMember = this.model.get("questionResponseMember");

				// Determine if we need to indicate the selected profile is currently customized
				// (having values that don't match the defaults for the profile)
				var customized = false;
				if (questionResponseMember.value.customized) {
					customized = questionResponseMember.value.customized;
				}

				var memberUsageData = questionResponseMember.value.usageData;
				// If there are temporary Customizations, render with those
				if (this.model.get("tempCustomizations")) {
					memberUsageData = this.model.get("tempCustomizations");
				}

				localData = _.extend({}, {
					"currentProfile" : selectedProfile
				}, {
					"costEstimatorConfig" : this.options.configModel.attributes
				}, {
					"widgetModel" : this.options.widgetModel.toJSON().config,
				}, {
					"currentMember" : this.model.get("currentMember")
				}, {
					"members" : this.model.get("members")
				}, {
					"selectedProfileId" : (selectedProfile) ? selectedProfile.id : null
				}, {
					"questionResponse" : this.model.get("questionResponse")
				}, {
					"memberUsageData" : memberUsageData
				}, {
					"customized" : customized
				});
			} else {
				localData = _.extend({}, {
					"costEstimatorConfig" : this.options.configModel.attributes,
					"widgetModel" : this.options.widgetModel.toJSON().config
				});
			}

			var viewHTML = this.renderTemplate({
				"template" : this.templates[this.model.get("currentTemplate")],
				"data" : localData
			});

			self.el.className = "_questionWidget";
			$(self.el).html(viewHTML);

			if (this.model.get("currentTemplate") == 1) {
				$(".categories .health-category ._value", self.el).bind(
						{
							keydown : function(event) {
								var $selectedElement = $(event.currentTarget);

								var keyCode = ('which' in event) ? event.which : event.keyCode;

								var isNumeric = (keyCode >= 48 /* KeyboardEvent.DOM_VK_0 */&& keyCode <= 57 /* KeyboardEvent.DOM_VK_9 */)
										|| (keyCode >= 96 /* KeyboardEvent.DOM_VK_NUMPAD0 */&& keyCode <= 105 /* KeyboardEvent.DOM_VK_NUMPAD9 */);
								var isBackspace = keyCode == 8;
								var isTab = keyCode == 9;
								var modifiers = (event.altKey || event.ctrlKey || event.shiftKey);
								var shiftTab = isTab && event.shiftKey;
								var validKey = (isNumeric || isBackspace || isTab) && (!modifiers || shiftTab);

								var value = $selectedElement.val();
								var validValue = value.length < 3 || isBackspace;

								return validKey && validValue;
							}
						});
			}

			return self;
		}
	});

	var CostEstimatorWidget = function(options) {
		QuestionWidget.call(this, options);
		this.eventManager = options.eventManager;

		this.costEstimatorModel = new CostEstimatorModel(options.attributes);

		// handle any route coming in, kind of hacked to work with existing
		// flow

		if (options.route && !_.isEmpty(options.route.parameters)) {
			// for this widget the first parameter can be the member ref id
			this.costEstimatorModel.set("currentMemberRefName", options.route.parameters[0]);
		}

		this.costEstimatorConfigModel = options.attributes.costEstimatorConfigModel;

		// We only want the members with the product line attributed to the Cost Estimator in the config
		this.costEstimatorModel.set({
			"members" : this.getMembersForEstimation(options.attributes.account.get("members"))
		});
	};

	CostEstimatorWidget.prototype = Object.create(QuestionWidget.prototype);

	CostEstimatorWidget.prototype.start = function() {
		this.view = new CostEstimatorView({
			model : this.costEstimatorModel,
			widgetModel : this.model,
			configModel : this.costEstimatorConfigModel,
			eventManager : this.eventManager
		});

		var events = {
			"click ._memberNav ._memberIcon" : "memberSelected",
			"click ._profile" : "profileSelected",
			"click ._applyChanges" : "applyChanges",
			"click ._cancelCustomize" : "cancelChanges",
			"click ._customizeButton" : "customizeProfile",
			"click ._selectSettingsButton" : "selectProfile",
			"change ._category ._value" : "valueCustomized"
		};

		this.addEvents(events, this.view);

		var members = this.costEstimatorModel.get("members");

		var currentMember = null;
		// See if we have a current member to display
		if (this.costEstimatorModel.get("currentMemberRefName")) {
			var currentMemberRefName = this.costEstimatorModel.get("currentMemberRefName");
			for ( var m = 0; m < members.length && null == currentMember; m++) {
				if (members[m].memberRefName == currentMemberRefName) {
					currentMember = members[m];
				}
			}
		}

		if (null == currentMember) {
			// The default member is the first to have coverage selected.
			currentMember = members[0];
		}

		this.costEstimatorModel.set("currentMember", currentMember);

		var currentTemplateId = 0;
		if (null == currentMember) {
			currentTemplateId = 2;
		}
		// Just pick the first template, the select settings page
		this.costEstimatorModel.set("currentTemplate", currentTemplateId);

		this.initializeMemberAnswers();

		this.attributes.account.save({}, {
			silent : true
		});
	};

	CostEstimatorWidget.prototype.initializeMemberAnswers = function() {
		var members = this.costEstimatorModel.get("members");

		var questionResponse = this.getOrCreateQuestionResponse();

		// For each member
		for ( var m = 0; m < members.length; m++) {
			var memberRefName = members[m].memberRefName;

			// Initialize the member response values
			var member = this.getOrCreateQuestionResponseMember(questionResponse, memberRefName);

			if (!member.value) {
				member.value = {};
			}
		}
	};

	CostEstimatorWidget.prototype.canMovePrev = function() {
		var currentMember = this.costEstimatorModel.get("currentMember");

		// If there isn't a currentMember, no users with the CostEstimator applicable coverage, then allow the user to move away
		if (null == currentMember) {
			return true;
		}

		var prevMember = this.getPrevMember(currentMember);
		while (null != prevMember && !this.isCoverageSelected(prevMember)) {
			prevMember = this.getPrevMember(prevMember);
		}

		var canMovePrev = null == prevMember;
		if (!canMovePrev) {
			this.costEstimatorModel.set({
				"currentMember" : prevMember
			});

			this.costEstimatorModel.set({
				currentTemplate : 0
			});

			this.render();
		}

		return canMovePrev;
	};

	// @param currentMember where to start looking for the previous member
	// @return the previous member, or null if there is no previous member
	CostEstimatorWidget.prototype.getPrevMember = function(currentMember) {
		var member;

		var memberRefName = currentMember.memberRefName;

		var members = this.costEstimatorModel.get("members");
		for ( var m = 0; m < members.length && null == member; m++) {
			if (members[m].memberRefName == memberRefName && m > 0) {
				// Use the previous member
				member = members[m - 1];
			}
		}

		return member;
	};

	CostEstimatorWidget.prototype.canMoveNext = function() {
		var currentMember = this.costEstimatorModel.get("currentMember");

		// If there isn't a currentMember, no users with the CostEstimator applicable coverage, then allow the user to move away
		if (null == currentMember) {
			return true;
		}

		var nextMember = this.getNextMember(currentMember);
		while (null != nextMember && !this.isCoverageSelected(nextMember)) {
			nextMember = this.getNextMember(nextMember);
		}

		var canMoveNext = null == nextMember;

		if (!canMoveNext) {
			this.costEstimatorModel.set({
				"currentMember" : nextMember
			});

			this.costEstimatorModel.set({
				currentTemplate : 0
			});

			this.render();
		}

		return canMoveNext;
	};

	// @param currentMember where to start looking for the next member
	// @return the next member, or null if there is no next member
	CostEstimatorWidget.prototype.getNextMember = function(currentMember) {
		var member;

		var memberRefName = currentMember.memberRefName;

		var members = this.costEstimatorModel.get("members");
		for ( var m = 0; m < members.length && null == member; m++) {
			if (members[m].memberRefName == memberRefName) {
				if (m < members.length - 1) {
					// Use the next member
					member = members[m + 1];
				}
			}
		}

		return member;
	};

	CostEstimatorWidget.prototype.memberSelected = function(event) {
		var $selectedValue = $(event.currentTarget);

		var selectedMemberRefName = $selectedValue.attr("data-id");

		this.costEstimatorModel.set({
			currentMember : this.getMember(selectedMemberRefName)
		});

		this.costEstimatorModel.set({
			currentTemplate : 0
		});

		this.render();
	};

	CostEstimatorWidget.prototype.getMember = function(memberRefName) {
		var member;

		var members = this.costEstimatorModel.get("members");

		for ( var m = 0; m < members.length && null == member; m++) {
			if (members[m].memberRefName == memberRefName) {
				member = members[m];
			}
		}

		return member;
	};

	CostEstimatorWidget.prototype.getCurrentMemberProfileId = function() {
		var profileId;

		var questionResponse = this.getOrCreateQuestionResponse();
		if (null != questionResponse) {
			var currentMember = this.costEstimatorModel.get("currentMember");
			var member = this.getOrCreateQuestionResponseMember(questionResponse, currentMember.memberRefName);
			if (null != member && member.value) {
				profileId = member.value.profileId;
			}
		}

		return profileId;
	};

	CostEstimatorWidget.prototype.getSelectedProfile = function(selectedProfileId) {
		var selectedProfile;
		var profile = this.costEstimatorConfigModel.get("profile");
		for ( var s = 0; s < profile.length && null == selectedProfile; s++) {
			if (profile[s].id == selectedProfileId) {
				selectedProfile = profile[s];
			}
		}
		return selectedProfile;
	};

	CostEstimatorWidget.prototype.profileSelected = function(event) {
		var $selectedValue = $(event.currentTarget);

		var selectedProfileId = $selectedValue.children("input").attr("data-id");

		this.selectProfileId(selectedProfileId, false);
	};

	CostEstimatorWidget.prototype.selectProfileId = function(selectedProfileId, tempChange) {
		var self = this;

		var member = this.getMemberResponse();
		var previousProfileId = member.value.profileId;
		var tempCustomizations = this.costEstimatorModel.get("tempCustomizations");

		if (!tempCustomizations) {
			tempCustomizations = [];
			this.costEstimatorModel.set("tempCustomizations", tempCustomizations);
		}

		// Only clear out the values if this is not a temporary change
		// Temporary changes occur from the Customize page when a Reset is done
		if (!tempChange) {
			// Clear out any previous values (usageData)
			member.value = {
				profileId : selectedProfileId,
				usageData : []
			};
		}

		// Apply default values for member
		var selectedProfile = this.getSelectedProfile(selectedProfileId);
		var utilizations = this.getMemberUtilizations(member, selectedProfile);

		if (!tempChange) {
			member.value.usageData = utilizations;
		} else {
			tempCustomizations = utilizations;
			this.costEstimatorModel.set("tempCustomizations", tempCustomizations);
		}

		var wasCustomized = member.value.customized ? true : false;

		if (!tempChange) {
			// Utilizations are no longer customized
			member.value.customized = false;
		}

		this.render();

		this.attributes.account.save({}, {
			silent : true,
			success : function() {
				if ((wasCustomized || previousProfileId != selectedProfileId) && !tempChange) {
					self.updateCalculatedCosts();
				}
			}
		});
	};

	CostEstimatorWidget.prototype.getMemberUtilizations = function(member, profile) {
		var utilizations = [];

		var valueIndex = 0;
		for ( var uc = 0; uc < profile.usageCategory.length; uc++) {
			var usageCategory = profile.usageCategory[uc];
			for ( var ud = 0; ud < usageCategory.usageData.length; ud++) {
				var usageData = usageCategory.usageData[ud];
				var valueRefId = usageData.key;
				var consumerValues = usageData.consumer
				var value = null;
				for ( var cv = 0; cv < consumerValues.length && null == value; cv++) {
					var consumerValue = consumerValues[cv];
					if (member.memberRefName == consumerValue.xid) {
						utilizations[valueIndex] = {
							"key" : valueRefId,
							"value" : Math.round(consumerValue.value)
						};
					}
				}

				valueIndex++;
			}
		}

		return utilizations;
	};

	CostEstimatorWidget.prototype.getMemberResponse = function() {
		var questionResponse = this.getOrCreateQuestionResponse();
		var currentMemberRefName = this.costEstimatorModel.get("currentMember").memberRefName;
		return this.getOrCreateQuestionResponseMember(questionResponse, currentMemberRefName);
	};

	CostEstimatorWidget.prototype.updateCalculatedCosts = function() {
		var member = this.getMemberResponse();

		if (member.value.customized) {
			this.eventManager.trigger('widget:estimateCosts');
		} else {
			// TODO Update this for calculating all default profile cost data ahead of time
			// e.g. Usage a different event...
			// JWG
			// 24-JAN-2013
			this.eventManager.trigger('widget:estimateCosts');
		}
	};

	CostEstimatorWidget.prototype.valueCustomized = function(event) {
		var $selectedValue = $(event.currentTarget);

		var key = $selectedValue.attr("data-id");

		var newValue = $selectedValue.val();

		if (!newValue) {
			newValue = 0;
		}

		var member = this.getMemberResponse();
		var memberUsageData = member.value.usageData;

		var tempCustomizations = this.costEstimatorModel.get("tempCustomizations");
		if (!tempCustomizations) {
			tempCustomizations = [];

			for ( var v = 0; v < memberUsageData.length; v++) {
				tempCustomizations[v] = {
					key : memberUsageData[v].key,
					value : memberUsageData[v].value
				};
			}

			this.costEstimatorModel.set("tempCustomizations", tempCustomizations);
		}

		var customizedUsageData;
		for ( var v = 0; v < tempCustomizations.length && null == customizedUsageData; v++) {
			if (tempCustomizations[v].key == key) {
				customizedUsageData = tempCustomizations[v];
			}
		}

		customizedUsageData.value = newValue;
	};

	CostEstimatorWidget.prototype.applyChanges = function() {
		var self = this;

		// Apply the value
		var member = this.getMemberResponse();

		var memberUsageData = member.value.usageData;
		var tempCustomizations = this.costEstimatorModel.get("tempCustomizations");

		// If there are any customizations
		if (tempCustomizations) {
			var isDirty = false;
			for ( var v = 0; v < memberUsageData.length; v++) {
				if (!isDirty && memberUsageData[v].value != tempCustomizations[v].value) {
					isDirty = true;
				}
				memberUsageData[v].value = tempCustomizations[v].value;
			}

			if (isDirty) {
				member.value.customized = this.isUtilizationCustomized();

				this.attributes.account.save({}, {
					silent : true,
					success : function() {
						self.updateCalculatedCosts();
					}
				});
			}
		}

		this.selectProfile();
	};

	CostEstimatorWidget.prototype.isUtilizationCustomized = function() {
		var selectedProfile = this.getSelectedProfile(this.getCurrentMemberProfileId());

		var member = this.getMemberResponse();

		var utilizationCustomized = false;
		var valueIndex = 0;
		for ( var uc = 0; uc < selectedProfile.usageCategory.length && !utilizationCustomized; uc++) {
			var usageCategory = selectedProfile.usageCategory[uc];
			for ( var ud = 0; ud < usageCategory.usageData.length && !utilizationCustomized; ud++) {
				var usageData = usageCategory.usageData[ud];
				var valueRefId = usageData.key;
				var consumerValues = usageData.consumer
				var value = null;
				for ( var cv = 0; cv < consumerValues.length && !utilizationCustomized && null == value; cv++) {
					var consumerValue = consumerValues[cv];
					if (member.memberRefName == consumerValue.xid) {
						utilizationCustomized = Math.round(consumerValue.value) != member.value.usageData[valueIndex].value;
					}
				}
				valueIndex++;
			}
		}

		return utilizationCustomized;
	};

	CostEstimatorWidget.prototype.cancelChanges = function() {
		this.selectProfile();
	};

	CostEstimatorWidget.prototype.customizeProfile = function() {
		this.costEstimatorModel.set({
			currentTemplate : 1
		});

		// Clear any previous customizations for the current member
		this.costEstimatorModel.unset("tempCustomizations");

		this.render();
	};

	CostEstimatorWidget.prototype.selectProfile = function() {
		this.costEstimatorModel.set({
			currentTemplate : 0
		});

		// Clear any previous customizations for the current member
		this.costEstimatorModel.unset("tempCustomizations");

		this.render();
	};

	CostEstimatorWidget.prototype.render = function() {
		this.update();

		// trigger an event to let someone know that
		// we are rendering the widget
		this.eventManager.trigger('widget:render:internal');

		return this.view.render().el;
	};

	CostEstimatorWidget.prototype.getView = function() {
		// TODO: This might have to call something like update
		// to get the appriopriate view.
		this.update();
		return this.view;
	};

	CostEstimatorWidget.prototype.clearQuestionResponse = function(memberRefId) {
		var self = this;
		// ugh lazy, only clear if member ref id given
		if (memberRefId) {
			// eh doing this to reuse existing stuff
			this.costEstimatorModel.set("currentMember", {
				memberRefName : memberRefId
			});
			var member = this.getMemberResponse();
			this.clearQuestionMemberResponse();
			member.value.customized = false;

			this.attributes.account.save({}, {
				silent : true,
				success : function() {
					self.updateCalculatedCosts();
				}
			});

		}

	};

	CostEstimatorWidget.prototype.clearCurrentQuestion = function() {
		var self = this;

		var member = this.getMemberResponse();

		if (this.costEstimatorModel.get("currentTemplate") == 0) {
			// Clear any selections
			this.clearQuestionMemberResponse();
			member.value.customized = false;

			this.render();

			this.attributes.account.save({}, {
				silent : true,
				success : function() {
					self.updateCalculatedCosts();
				}
			});
		} else {
			// Only reset if it was customized
			if (member.value.customized || this.costEstimatorModel.get("tempCustomizations")) {
				var selectedProfileId = this.getCurrentMemberProfileId();

				// Reselect the default values for the currently selected profile
				this.selectProfileId(selectedProfileId, true);
			}
		}
	};

	CostEstimatorWidget.prototype.getSummaryView = function(options) {

		var profileName = null;

		var costEstimatorConfig = options.costEstimatorConfig;
		var summaryMember = null;
		// Moving this code here cause it is specific to cost estimator
		_.each(options.members, function(member) {
			// TODO: This is an == on purpose, there are cases apparently when the two properties
			// are not the same type, one is a string, one is an int
			if (member.memberRefName == options.member.get('memberRefName') && summaryMember == null) {
				summaryMember = member;
			}
		});

		if (summaryMember) {
			var responseValue = summaryMember.value;
			var profileId = summaryMember.profileId;

			// If a profile was selected
			if (responseValue && responseValue.profileId) {
				var profileId = responseValue.profileId;
				var usageData = responseValue.usageData;

				var profiles = costEstimatorConfig.get("profile");
				for ( var p = 0; p < profiles.length && profileName == null; p++) {
					if (profiles[p].id == profileId) {
						profileName = profiles[p].name;
					}
				}
			}
		}

		return new CostEstimatorSummaryView({
			profileName : profileName,
			questionRefId : options.questionRefId,
			member : options.member.toJSON(),
			title : options.title
		});
	};

	CostEstimatorWidget.prototype.update = function() {
		var currentMember = this.costEstimatorModel.get("currentMember");

		if (null != currentMember) {
			var selectedProfileId = this.getCurrentMemberProfileId();
			var selectedProfile = this.getSelectedProfile(selectedProfileId);
			this.costEstimatorModel.set({
				"selectedProfile" : selectedProfile
			});

			var questionResponse = this.getOrCreateQuestionResponse();
			this.costEstimatorModel.set({
				"questionResponse" : questionResponse
			});

			var memberRefName = this.costEstimatorModel.get("currentMember").memberRefName;
			var questionResponseMember = this.getOrCreateQuestionResponseMember(questionResponse, memberRefName);
			this.costEstimatorModel.set({
				"questionResponseMember" : questionResponseMember
			});
		}
	};

	CostEstimatorWidget.prototype.clearQuestionMemberResponse = function() {
		var questionResponse = this.getOrCreateQuestionResponse();
		var currentMemberRefName = this.costEstimatorModel.get("currentMember").memberRefName;

		var member;
		for ( var m = 0; m < questionResponse.members.length && null == member; m++) {
			if (questionResponse.members[m].memberRefName == currentMemberRefName) {
				member = questionResponse.members[m];
				questionResponse.members.splice(m, 1);
			}
		}
	};

	CostEstimatorWidget.prototype.state = function(state) {
		// setup to only look for end state for now
		if (state !== 'end') {
			return false;
		}

		// grab the current member
		var currentMember = this.costEstimatorModel.get("currentMember");
		// grab our members
		var members = this.costEstimatorModel.get("members");

		var index = _.indexOf(members, _.findWhere(members, {
			memberRefName : currentMember.memberRefName
		}));

		return index === _.size(members) - 1;
	};

	return CostEstimatorWidget;
});