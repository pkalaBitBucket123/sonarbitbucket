/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/list/view.product.list.pagination.dust', 'jquery-formatCurrency', 'text!html/shopping/components/list/plansDisclaimer.dust' ,'text!html/shopping/components/list/plansExtraInfoDisclaimer.dust'], function(require,
		Backbone, templateEngine, connecture, PaginationTemplate, plansDisclaimerTemplate, plansExtraInfoDisclaimerTemplate) {

	templateEngine.load(PaginationTemplate, "view.product.list.pagination");
	templateEngine.load(plansDisclaimerTemplate, 'plansDisclaimer');
	templateEngine.load(plansExtraInfoDisclaimerTemplate, "plansExtraInfoDisclaimer");

	var ProductListView = connecture.view.Base.extend({
		template : 'view.product.list.pagination',
		events : {
			'click li._first' : 'gotoFirst',
			'click li._previous' : 'gotoPrev',
			'click li._next' : 'gotoNext',
			'click li._last' : 'gotoLast',
			'click li._goToPage' : 'gotoPage',
			'change ._changeCount select' : 'changeCount',
		},
		initialize : function(options) {
			this.animate = this.options.animate ? true : false;
			this.options.account ? this.account = this.options.account : this.account = null;
			this.options.actor ? this.actor = this.options.actor : this.actor = null;
			this.options.quote ? this.quote = this.options.quote : this.quote = null;
			this.options.quotingEnabled ? this.quotingEnabled = this.options.quotingEnabled : false;
			this.options.displayPlansDisclaimer ? this.displayPlansDisclaimer = this.options.displayPlansDisclaimer : this.displayPlansDisclaimer = false;
			this.session = options.session;

            var boostrapOptions = {};
			if (this.session.get('productListSelectedPer')) {
				boostrapOptions['perPage'] = this.session.get('productListSelectedPer');
			}
			// by rsingh for user story 460 
			var hasCatastrophic="";
			var tempModel="";
			var metalTier="";
			var models = this.collection.models;
			for(var i=0;i<models.length;i++){
			   metalTier = models[i].get("planMetalTier");
			   if( metalTier.toUpperCase() == "CATASTROPHIC" )
				   hasCatastrophic="Yes";
			}
			if( hasCatastrophic == "Yes" )
			{
				for(var j=0; j<models.length-1 ; j++){
					tempModel = models[j];
					models[j] = models[j+1];
					models[j+1] = tempModel;
				}
			}
			// startup our pagination here
			this.collection.bootstrap(boostrapOptions);

			this.listenTo(this.collection, 'reset', this.render, this);
		},
		render : function() {
			var self = this;

			$(self.el).addClass('after-sidebar');
			$(self.el).attr("data-liffect", "fadeIn");

			// upon rendering, clear everything out because
			// there is a chance of rerendering from within the view
			$(self.el).empty();

			var paginationData = {
				totalPages : this.collection.information.totalPages,
				currentPage : this.collection.information.currentPage,
				pagesRange : this.collection.information.pageSet,
				firstPage : this.collection.firstPage,
				perPage : this.collection.information.perPage,
				containerClass : 'pagination-top',
				count : this.collection.information.totalRecords
			};
			// render this both top - TODO make this a view object
			$(self.el).append(this.renderTemplate({
				data : paginationData
			}));

			this.collection.each(function(item) {
				var view = new ProductSummaryView({
					costViewCode : self.options.costViewCode,
					model : item,
					account : self.account,
					quote : self.quote,
					quotingEnabled : self.quotingEnabled,
					actor : self.actor,
					eventManager : self.options.eventManager
				});

				$(self.el).append(view.render().el);

				self.views.push(view);
			});

			paginationData.containerClass = 'pagination-bottom';
			// and bottom - TODO make this a view object
			$(self.el).append(this.renderTemplate({
				data : paginationData
			}));
           
           if (this.displayPlansDisclaimer == true) {
        	   $(self.el).append(templateEngine.render("plansExtraInfoDisclaimer", {}));
				$(self.el).append(templateEngine.render('plansDisclaimer', {}));
			}
           

			$(".content-section[data-liffect] .plan-card").each(
					function(i) {
						$(this).attr(
								"style",
								"-webkit-animation-delay:" + i * 200 + "ms;" + "-moz-animation-delay:" + i * 200 + "ms;" + "-o-animation-delay:" + i * 200 + "ms;" + "animation-delay:" + i * 200
										+ "ms;");
						if (i == $(".content-section[data-liffect] .plan-card").size() - 1) {
							$(".content-section[data-liffect]").addClass("play");
						}
					});

			$('a[title]', self.el).tooltip();
			// $('.plan-card .section._details .dataGroup._benefits', self.el).equal_height();
			// $('.plan-card .plan-name', self.el).equal_height('start');
			// $('.plan-card .section.plan-rate', self.el).equal_height('start');
			$('.plan-card .section.plan-details', self.el).equal_height('start');
			$('.plan-card .section.plan-preference', self.el).equal_height('start');
			$('.plan-card .section.providers', self.el).equal_height('start');
			$('.plan-card .plan-card-details', self.el).equal_height('start');
			$('.plan-card', self.el).equal_height('start');

			$('.cost-estimation-chart .bar-wrapper .bar .ui-progressbar-value[title]', self.el).tooltip();

			$('._currency', self.el).formatCurrency();

			return this;
		},
		close : function() {

		},
		gotoFirst : function(e) {
			e.preventDefault();
			this.collection.goTo(1);
		},

		gotoPrev : function(e) {
			e.preventDefault();
			this.collection.previousPage();
		},

		gotoNext : function(e) {
			e.preventDefault();
			this.collection.nextPage();
		},

		gotoLast : function(e) {
			e.preventDefault();
			this.collection.goTo(this.collection.information.lastPage);
		},

		gotoPage : function(e) {
			e.preventDefault();
			var page = $(e.currentTarget).find("a").text();
			this.session.set({
				productListSelectedPage : page
			});
			this.collection.goTo(page);
		},

		changeCount : function(e) {
			e.preventDefault();
			var per = $(e.target.options[e.target.selectedIndex]).text();
			this.session.set({
				productListSelectedPer : per
			});
			this.collection.howManyPer(per);
		},
	});

	return ProductListView;
});