/**
 * Copyright (C) 2012 Connecture
 * 
 * Component Template
 * 
 * 
 */
define([ 'module', 'backbone', 'templateEngine', 'common/component/component', 'components/loader/views/view.loader.loading', 'components/loader/views/view.loader.summary',
		'text!html/shopping/components/loader/view.loader.dust' ], function(module, Backbone, templateEngine, connecture, LoaderView, WelcomeSummaryView, template) {

	templateEngine.load(template, 'view.loader');

	var LoadingComponentView = connecture.component.core.View.extend({
		template : 'view.loader',
		// override base cause we don't want all of the component stuff for
		// this guy
		render : function() {
			var self = this;
			$(self.el).html(this.renderTemplate());
			// You have been a bad boy, someone put style on a product class.
			$(self.el).removeClass('_component');
			return this;
		},
	});

	var LoadingComponent = function(configuration) {
		connecture.component.core.Base.call(this, configuration);

		this.items = module.config().items;
		this.publish = {};
		// Setting a specific id here, cause this component
		this.id = "WelcomeComponent";

		this.configuration.layout = _.extend({}, this.configuration.layout, {
			el : '._loaderComponent',
			View : LoadingComponentView,
			regions : {
				'content' : '._content',
			}
		});
	};

	LoadingComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	LoadingComponent.prototype.load = function(data) {

	};

	LoadingComponent.prototype.update = function(data) {

	};

	LoadingComponent.prototype.registerEvents = function() {
		// I feel like a region manager might be overkill for this
		this.eventManager.on("start", function() {
			//this.regionManager.render();
			this.regionManager.content.show(new LoaderView());
		}, this);

		this.eventManager.on("test", function() {
			this.regionManager.content.show(new WelcomeSummaryView({
				eventManager : this.eventManager,
				models : this.models
			}));
		}, this);
	};

	return LoadingComponent;
});