/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'module', 'backbone', 'common/widget.factory', 'common/workflows/workflow.item', 'components/account/workflows/views/view.workflow.item.whoscovered', 'shopping/individual/components/account/models/model.demographics.member' ], function(module, Backbone,
		WidgetFactory, WorkflowItem, WhosCoveredWorkfowTemplate, DemoMemberModel) {
	/**
	 * I removed the config checks determining when we trigger members updated put back as we figure out configuration.
	 */
	var WhosCoveredWorkflowItem = function(configuration, widgets, options) {
		
		// TODO: This is kind of a hack for now but we have no place to put cost estimator,
		// it is kind of the bastard of the family. Ideally we might have some sort of generic
		// work flow item callbacks once it is done.
		if (widgets.isCostEstimatorIncluded()) {
			this.costEstimatorWidgetModel = widgets.getWidget('CostEstimator', 'widget');
			this.costEstimatorIncluded = true;
		} else {
			this.costEstimatorIncluded = false;
		}

		WorkflowItem.call(this, configuration, widgets, options);
	};

	WhosCoveredWorkflowItem.prototype = Object.create(WorkflowItem.prototype);

	WhosCoveredWorkflowItem.prototype.buildInitData = function() {
		// override as needed
	};
	
	WhosCoveredWorkflowItem.prototype.initialize = function() {
		var self = this;

		WorkflowItem.prototype.initialize.call(this);

		// set the base data that all widgets in this workflow will be using
		self.buildInitData();
	
		// Going to handle this at the workflow item and not the widget level
		// because the workflow should have access to all of the widgets and
		// can handle some of the cost estimator specific things
		this.eventManager.on('view:whoscovered:finished', function(response) {
			var self = this;

			// trigger that we have updated members.
			this.options.eventManager.trigger('view:members:updated');

			if (this.costEstimatorIncluded && this.isCostEstimatorCoverageSelected()) {
				this.options.models.costEstimatorConfig.fetch({
					success : function() {
						// Re-apply Utilizations to members with a selected profileId and
						// whose utilizations were cleared due to a demographics change.
						var account = self.options.models.account;
						var members = account.get("members");
						var saveRequired = false;
						for ( var m = 0; m < members.length; m++) {
							var memberResponse = self.getCostEstimatorDemographicsMemberResponse(members[m]);
							if (memberResponse && memberResponse.value && memberResponse.value.profileId != null && memberResponse.value.usageData.length == 0) {
								// TODO: I cannot figure out where this method was originally
								var profile = self.getCostEstimatorProfile(memberResponse.value.profileId);
								memberResponse.value.usageData = self.getMemberUtilizations(memberResponse, profile);
								saveRequired = true;
							}
						}
						// Changes by rsingh for user story 228 & 230	 
						// Now save any member profile changes
						if (saveRequired) {
							self.options.models.account.save({}, {
								success : function() {
									if(response!=null && response == 'WF2'){
										self.escapeMethodStep(response);
									}else {
									self.exit('next');
								}
								}
							});
						} else {
							if(response!=null &&  response == 'WF2'){
								self.escapeMethodStep(response);
							}else {
							self.exit('next');
						}
					}
					}
				});
			} else {
				if(response!=null &&  response == 'WF2'){
					self.escapeMethodStep(response);
				}else {
				self.exit('next');
			}
			}

		}, this);

		this.eventManager.on('component:demographicsUpdated', function(member, attribute) {

			if (this.costEstimatorIncluded) {
				// Cost Estimator specific handling
				if (attribute == "birthDate" || attribute == "gender" || attribute == "relationshipType") {
					var memberResponse = this.getCostEstimatorDemographicsMemberResponse(member);

					if (null != memberResponse) {
						memberResponse.value.usageData = [];
						this.options.models.account.save();
					}
				}
			}
		}, this);

		this.eventManager.on('widget:showWhosCovered', function(demographicsData, triggerHistory) {

			var self = this;

			self.eventManager.trigger('widget:takeover', 'Demographics', demographicsData);

		}, this);

		this.eventManager.on('widget:showSpecialEnrollment', function(demographicsEventData, triggerHistory) {

			var self = this;

			var demographicsData = self.demographicsData;

			// If a Special Enrollment eventType was specified, supply
			// the corresponding parameters to the model
			if (demographicsEventData) {
				demographicsData.eventType = demographicsEventData.eventType;
				demographicsData.eventDate = demographicsEventData.eventDate;
			} else {
				// also we may have removed the data, so remove it here
				delete demographicsData.eventType;
				delete demographicsData.eventDate;
			}

			self.eventManager.trigger('widget:takeover', 'SpecialEnrollment', demographicsData);

		}, this);

		this.eventManager.on('widget:cancelSpecialEnrollment', function() {

			var self = this;

			var demographicsData = self.demographicsData;

			delete demographicsData.eventType;
			delete demographicsData.eventDate;

			self.eventManager.trigger('widget:takeover', 'Demographics', demographicsData);

		}, this);

	};

	WhosCoveredWorkflowItem.prototype.buildWidgetData = function() {
		return {
			session : this.options.session,
			account : this.options.models.account,
			demographicsData : {},
			eligibility : this.options.models.eligibility,
			genders : this.options.models.genders,
			productLines : this.options.models.productLines,
			relationships : this.options.models.relationships,
			componentId : this.options.history.id
		};
	};

	/**
	 * Escape this flow and call plans directly.
	 * 
	 * @param options
	 */
	// Changes by rsingh for user story 228 & 230 
	WhosCoveredWorkflowItem.prototype.escapeMethodStep = function(response) {

		var activeWorkflow = true;
		var questionRefIds = [];
		
		// might have to store this for when we render the method page again
		this.options.session.set("activeWorkflowRefNumber", response);
		this.options.session.set("workflowFilters", questionRefIds);
		this.options.session.set("activeWorkflow", activeWorkflow);
		// if we don't have any questions, then trigger an exit
		this.exit('exit');
	};
	
	/**
	 * Overriding here because we need a new template for this workflow item. This will work for now but could be better
	 * 
	 * @param options
	 * @returns {WhosCoveredWorkfowTemplate}
	 */
	WhosCoveredWorkflowItem.prototype.render = function(options) {

		return new WhosCoveredWorkfowTemplate();
	};

	/**
	 * Override as needed
	 * 
	 * @returns {Boolean}
	 */
	WhosCoveredWorkflowItem.prototype.isCostEstimatorCoverageSelected = function() {
		return true;
	};

	WhosCoveredWorkflowItem.prototype.getCostEstimatorDemographicsMemberResponse = function(member) {
		var memberResponse = null;

		var account = this.options.models.account;

		// Find the account question (answer)
		var questions = account.get("questions");
		// If there are answers, otherwise there's nothing to do
		if (questions) {
			// Find the cost estimator question
			var costEstimatorQuestionRefId = this.costEstimatorWidgetModel.get('config').questionRefId;

			var question = null;
			for ( var q = 0; q < questions.length && question == null; q++) {
				if (questions[q].questionRefId == costEstimatorQuestionRefId) {
					question = questions[q];
				}
			}

			// If there is an answer to the cost estimator question, otherwise there's nothing to do
			if (null != question) {
				// Find the member response
				var members = question.members;
				if (members) {
					for ( var m = 0; m < members.length && memberResponse == null; m++) {
						if (members[m].memberRefName == member.memberRefName) {
							memberResponse = members[m];
						}
					}
				}
			}
		}

		return memberResponse;
	};

	WhosCoveredWorkflowItem.prototype.getMemberUtilizations = function(member, profile) {
		var utilizations = [];

		var valueIndex = 0;
		for ( var uc = 0; uc < profile.usageCategory.length; uc++) {
			var usageCategory = profile.usageCategory[uc];
			for ( var ud = 0; ud < usageCategory.usageData.length; ud++) {
				var usageData = usageCategory.usageData[ud];
				var valueRefId = usageData.key;
				var consumerValues = usageData.consumer;
				var value = null;
				for ( var cv = 0; cv < consumerValues.length && null == value; cv++) {
					var consumerValue = consumerValues[cv];
					if (member.memberRefName == consumerValue.xid) {
						utilizations[valueIndex] = {
							"key" : valueRefId,
							"value" : Math.round(consumerValue.value)
						};
					}
				}

				valueIndex++;
			}
		}

		return utilizations;
	};

	WhosCoveredWorkflowItem.prototype.getCostEstimatorProfile = function(profileId) {
		var profile = null;
		var profiles = this.options.models.costEstimatorConfig.get("profile");
		for ( var s = 0; s < profiles.length && null == profile; s++) {
			if (profiles[s].id == profileId) {
				profile = profiles[s];
			}
		}
		return profile;
	};

	return WhosCoveredWorkflowItem;

});