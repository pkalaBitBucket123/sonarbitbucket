/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the old version of model.email. Please use the common/models/model.email going forward
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {

	var EmailModel = ProxyModel.extend({
		initialize : function(options) {
			ProxyModel.prototype.initialize.call(this, {
				requestKey : 'email'
			});
		},
		/**
		 * Updated, this method has been updated to follow the validate spec
		 * 
		 * @param attrs
		 * @returns {Boolean}
		 */
		validate : function(attrs) {
			// check email format
			// TODO | could take in a collection of error messages, identified by key or code?
			this.errors = [];

			if (!attrs.toEmail) {
				// TODO | get message from db messages
				this.errors[this.errors.length] = {
					field : "_toEmail",
					value : "no recipients"
				};
			} else {
				var badEmails = [];
				// parse by semicolon, message for each email address
				var addresses = attrs.toEmail.split(";");
				for ( var i = 0; i < addresses.length; i++) {
					if (!validateEmail(addresses[i].trim())) {
						badEmails[badEmails.length] = addresses[i].trim();
					}
				}

				if (badEmails.length > 0) {
					// TODO | get message from db messages
					var message = "The email address format is invalid. Please make sure the format is x@x.xxx  or x@x.xx  or x@x.x.xxx  or x@x.x.xx";
					for ( var j = 0; j < badEmails.length; j++) {
						message += badEmails[j];
						if (badEmails.length - j > 1) {
							message += ", ";
						}
					}
					this.errors[this.errors.length] = {
						field : "_toEmail",
						value : message
					};
				}
			}

			if (this.errors.length > 0) {
				return this.errors;
			}
		}
	});

	// TODO | import validation scripts from another js file
	function validateEmail(string) {
		if (string == null || string == "") {
			return true;
		} else {
			var regExp = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;
			return regExp.test(string);
		}
	}

	return EmailModel;
});