/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'components/account/views/view.account.question.summary',
		'text!html/shopping/components/account/view.questionnaire.summary.dust' ], function(require, Backbone, templateEngine, connecture, QuestionSummaryView, questionnaireSummaryTemplate) {

	templateEngine.load(questionnaireSummaryTemplate, "view.questionnaire.summary");

	var QuestionnaireSummaryView = connecture.view.Base.extend({
		template : 'view.questionnaire.summary',
		initialize : function(options) {
			this.title = options.title;
			this.questions = options.questions;
			this.account = options.account;
		},
		render : function() {
			var self = this;

			// render the base template for this view
			$(self.el).html(self.renderTemplate({
				title : self.title
			}));

			// now render the questions
			this.questions.each(function(question) {
				var questionView = new QuestionSummaryView({
					model : question,
					account : self.account
				});

				$('.dataGroup.multiGroup', self.el).append(questionView.render().el);

				self.views.push(questionView);
			});

			return this;
		}
	});

	return QuestionnaireSummaryView;

});