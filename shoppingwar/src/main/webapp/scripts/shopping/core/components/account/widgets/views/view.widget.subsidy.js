/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'components/account/models/model.povertyincomelevel', 'text!html/shopping/components/account/view.subsidy.dust',
         'text!html/shopping/components/account/view.subsidyQuestions.dust', 'text!html/shopping/components/account/view.subsidy.closedEnrollment.dust' ], function(require, Backbone, templateEngine, connecture, PovertyIncomeLevelModel, viewSubsidyBranchTemplate,
        	viewSubsidyQuestionsTemplate, viewSubsidyClosedEnrollmentTemplate) {

	templateEngine.load(viewSubsidyBranchTemplate, "view.subsidy");
	templateEngine.load(viewSubsidyQuestionsTemplate, "view.subsidyQuestions");
	templateEngine.load(viewSubsidyClosedEnrollmentTemplate, "view.subsidy.closedEnrollment");

	// These are the workflows
	var SubsidyView = connecture.view.Base.extend({
		subsidyTemplate : "view.subsidy",
		subsidyQuestionsTemplate : "view.subsidyQuestions",
		subsidyClosedEnrollmentTemplate : "view.subsidy.closedEnrollment",
		events : {
			"click ._help" : "showHelp",
			"click ._subsidyCheck" : "subsidyCheck",
			"click ._subsidyPrev" : "subsidyPrev",
			"click ._subsidySkip" : "subsidySkip",
			"click ._subsidyClear" : "subsidyClear",
			"click ._subsidyContinue" : "continueToNext",
			"change ._householdMakeup" : "householdMakeupChanged",
			"change ._assistanceEligibility > input" : "assistanceEligibilityChanged",
			"change ._householdIncome > input" : "householdIncomeChanged",
			'click ._toFFM' : 'exitToFFM',
			'click ._directFFMURL' : 'directToFFM'
		},
		initialize : function(options) {
			var collection = options.model.collection._byId;
			var daysLeftToEnroll = undefined;
			$.each(collection, function(){
			   var config = this.get("config");
			   if(config.daysLeftToEnroll != undefined){
				   daysLeftToEnroll = config.daysLeftToEnroll;
			   }
			});
			//this.enrollmentStartdate = this.options.model.collection.getWidget("Demographics").attributes.config.enrollmentStartDate;
			this.eventManager = options.eventManager;
			this.applicationConfig = options.applicationConfig;
			this.account = options.account;
			this.actor = options.actor;
			this.session = options.session;

			this.applyTempModel();

			if (daysLeftToEnroll == -1) {
				this.options.page = "subsidyClosedEnrollment";
			} else if (!this.options.page) {
				this.options.page = "subsidy";
			}
			
			this.ffmPriorToApplyURL = this.applicationConfig.get("ffmPriorToApplyURL");
		},
		applyTempModel : function() {
			var tempModel = this.getTempAnswers();
			if (!tempModel) {
				if (!this.account.get("subsidyAnswers")) {
					// Use a default value of 1 if there is no demographics
					// when this page is displayed
					var initialHouseholdCount = 1;
					if (this.account.get("members")) {
						initialHouseholdCount = this.account.get("members").length;
					}
					tempModel = {
						householdCount : initialHouseholdCount
					};
				} else {
					tempModel = _.extend({}, this.account.get("subsidyAnswers"));
				}

				this.account.set("tempSubsidyAnswers", tempModel);
			}

			if (!tempModel.userPickedHouseholdCount) {
				if (this.account.get("members")) {
					tempModel.householdCount = this.account.get("members").length;
				} else {
					tempModel.householdCount = 1;
				}

			}
		},
		getTempAnswers : function() {
			return this.account.get("tempSubsidyAnswers");
		},
		render : function(skipPovertyUpdate) {
// changes for SHSBCBSNE-5 and 6
			var coverageType = this.account.get("coverageType");
			if (coverageType=='DEN' || coverageType == 'STH'){
				this.eventManager.trigger('view:subsidy:close');
				return;
			}
			var self = this;

			var answers = this.getTempAnswers();

			if ($('._subsidyWidget', self.el).length == 0) {
				$(self.el).append("<div class=\"_subsidyWidget\"></div>");
			}

			var isFFMEnabled = this.applicationConfig.get("ffmEnabled") || false;
			this.account.set("ffmEnabled",isFFMEnabled);
			var isUserFFMEnabled = isFFMEnabled;
			// if it is enabled and we are an internal user, do an extra check
			if (isFFMEnabled && this.actor.get("isInternalUser")) {
				isUserFFMEnabled = this.actor.get("ffmEnabled");
			}

			var localData = _.extend({
				assistanceEligible : answers.assistanceEligibility ? answers.assistanceEligibility && answers.assistanceEligibility == 1 : true,
				assistanceElibility : answers.assistanceEligibility,
				eligibilitySelectionMade : answers.assistanceEligibility ? true : false,
				isFFMEnabled : isFFMEnabled,
				isUserFFMEnabled : isUserFFMEnabled,
				ffmPriorToApplyURL : this.ffmPriorToApplyURL,
				isEligible : answers.isEligible,
				enrollmentStartdate : this.options.model.collection.getWidget("Demographics").attributes.config.enrollmentStartDate,
				povertyIncomeLevel : this.povertyIncomeLevel ? this.povertyIncomeLevel : "(Calculating)",
				eligibilityDecisionMade : answers.eligibilityDecisionMade,
				householdIncomeEligible : answers.householdIncomeEligibility ? answers.householdIncomeEligibility && answers.householdIncomeEligibility == 0 : true,
				householdIncomeEligibility : answers.householdIncomeEligibility,
				householdIncomeSelectionMade : answers.householdIncomeEligibility ? true : false,
				isConsumer : !(this.actor.get("isInternalUser"))
			}, this.model.get('config'));

			var output = this.renderTemplate({
				"data" : localData,
				"template" : self.options.page + "Template",
				"functions" : {
					householdOptions : function(chunk, context, bodies, params) {
						// TODO: We should not be doing stuff like this, accessing functions
						// out side of the context, this is not portable anyore
						self.householdOptions(chunk, context, bodies, params);
					}
				}
			});

			$('._subsidyWidget', self.el).html(output);

			if (this.povertyIncomeLevel) {
				$('._povertyIncomeLevel', self.el).formatCurrency({
					roundToDecimalPlace : 0
				});
				$('._povertyIncomeLevel', self.el).css('font-style', 'normal');

				$('._povertyIncomeLevelInGraph', self.el).formatCurrency({
					roundToDecimalPlace : 0
				});
				$('._povertyIncomeLevelInGraph', self.el).css('font-style', '');
			} else {
				$('._povertyIncomeLevel', self.el).css('font-style', 'italic');
				$('._povertyIncomeLevelInGraph', self.el).css('font-style', 'italic');
			}

			this.account.save({}, {
				success : function() {
					if (!(skipPovertyUpdate === true)) {
						self.updatePovertyIncomeLevelDisplay();
					}
				}
			});

			$('a[title]', self.el).tooltip();
			
			// since some of these links are anchor tags
			// we can't disable them via HTML
			if(!isUserFFMEnabled) {
				$('._exchangeLink', this.el).on('click', function(event){
					event.preventDefault();
				});
			}
			

			return this;
		},
		updatePovertyIncomeLevelDisplay : function() {
			var self = this;

			$('._householdIncome > input', self.el).prop('disabled', true);

			$('._povertyIncomeLevel', self.el).html("(Calculating)");
			$('._povertyIncomeLevel', self.el).css('font-style', 'italic');
			$('._povertyIncomeLevelInGraph', self.el).html("(Calculating)");
			$('._povertyIncomeLevelInGraph', self.el).css('font-style', 'italic');

			var povertyIncomeLevelModel = new PovertyIncomeLevelModel();
			povertyIncomeLevelModel.fetch({
				// we do not want to post this change, since we
				// we are manually calling applyCosts...we might be able
				// to refactor this and just call fetch and not have
				// the success callback
				silent : true,
				success : function() {
					self.povertyIncomeLevel = povertyIncomeLevelModel.get("povertyIncomeLevel");

					$('._povertyIncomeLevel', self.el).css('font-style', 'normal');
					$('._povertyIncomeLevel', self.el).html(self.povertyIncomeLevel);
					$('._povertyIncomeLevel', self.el).formatCurrency({
						roundToDecimalPlace : 0
					});
					$('._povertyIncomeLevelInGraph', self.el).css('font-style', '');
					$('._povertyIncomeLevelInGraph', self.el).html(self.povertyIncomeLevel);
					$('._povertyIncomeLevelInGraph', self.el).formatCurrency({
						roundToDecimalPlace : 0
					});
					$('._householdIncome > input', self.el).prop('disabled', false);
				}
			});
		},
		householdOptions : function(chunk, context, bodies, params) {
			var currentCount = this.getTempAnswers().householdCount;

			var options = "";
			// Render an option for each of the defined gender types
			for ( var i = 1; i < 10; i++) {
				var selected = "";
				// If the count matches mark it as selected
				if (currentCount == i) {
					selected = " selected";
				}

				options += "<option count=\"" + i + "\"" + selected + ">" + i + "</option>";
			}
			var selected = "";
			if (currentCount > 9) {
				selected = "selected";
			}
			options += "<option count=\"10\"" + selected + ">10+</option>";

			chunk.write(options);
		},
		householdMakeupChanged : function(event) {
			var self = this;

			var $selectedElement = $(event.currentTarget);
			var householdCount = $selectedElement.children('select').children('option:selected').attr('count');
			this.getTempAnswers().householdCount = householdCount;
			this.getTempAnswers().userPickedHouseholdCount = true;

			this.account.save({}, {
				success : function() {
					self.updatePovertyIncomeLevelDisplay();
				}
			});
		},
		assistanceEligibilityChanged : function(event) {
			var $selectedElement = $(event.currentTarget);
			var assistanceEligibility = $selectedElement.attr('data-id');
			var subsidyAnswers = this.getTempAnswers();
			subsidyAnswers.assistanceEligibility = assistanceEligibility;

			if (assistanceEligibility == "1") {
				subsidyAnswers.eligibilityDecisionMade = true;
				subsidyAnswers.isEligible = false;
				delete subsidyAnswers.householdIncomeEligibility;
			} else {
				subsidyAnswers.eligibilityDecisionMade = false;
				delete subsidyAnswers.isEligible;
			}

			this.render();
		},
		householdIncomeChanged : function(event) {
			var $selectedElement = $(event.currentTarget);
			var householdIncomeEligibility = $selectedElement.attr('data-id');
			var subsidyAnswers = this.getTempAnswers();
			subsidyAnswers.householdIncomeEligibility = householdIncomeEligibility;

			subsidyAnswers.eligibilityDecisionMade = true;
			subsidyAnswers.isEligible = true;
			if (householdIncomeEligibility == "1") {
				subsidyAnswers.isEligible = false;
			}

			this.render(true);
		},
		subsidyCheck : function(event) {
			this.eventManager.trigger('view:subsidy:change:state', "subsidyQuestions");
		},
		subsidyPrev : function(event) {
			this.eventManager.trigger('view:subsidy:change:state', "subsidy");
		},
		subsidySkip : function(event) {
			this.eventManager.trigger('view:subsidy:close');
		},
		subsidyClear : function(event) {
			this.account.unset("tempSubsidyAnswers");
			this.account.unset("subsidyAnswers");

			this.applyTempModel();

			this.render();
		},
		continueToNext : function(event) {
			// Actually update the account.subsidyAnswers
			this.account.set("subsidyAnswers", this.getTempAnswers());
			this.account.unset("tempSubsidyAnswers");

			this.eventManager.trigger('view:subsidy:close');
		},
		showHelp : function() {
			this.eventManager.trigger('view:help:show');
		},
		close : function() {

		},
		exitToFFM : function() {
		
			var self = this;			
			self.eventManager.trigger('exit', {
				key : 'ffm',
				location : self.ffmPriorToApplyURL,
				params : {
					toFFM : true
				}
			});
		},
		directToFFM : function() {
			window.location = this.applicationConfig.get("ffmURL");
		}
	});

	return SubsidyView;
});