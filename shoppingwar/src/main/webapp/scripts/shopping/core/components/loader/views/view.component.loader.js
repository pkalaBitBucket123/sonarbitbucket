/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/components/view.component.loader.dust' ], function(require, backbone, templateEngine, BaseView, template) {

	templateEngine.load(template, 'view.component.loader');

	var LoadingView = BaseView.extend({
		template : 'view.component.loader',
		render : function() {
		
			$(this.el).html(this.renderTemplate());
			return this;
		},
		//Changes for SHSBCBSNE-264
		initialize : function() {
			var self = this;
			 var tohideDivDate= new Date();
			 var toshowDivDate= new Date();
			    tohideDivDate.setFullYear(2014,00,01);
			    tohideDivDate.setHours(0,0,0,0);
			    toshowDivDate.setFullYear(2014,10,15);
			    toshowDivDate.setHours(0,0,0,0);
			    var today = new Date();
			    if(today >= tohideDivDate )
			    	var hide="true";
				else
					var hide="false";
			    tohideDivDate.setFullYear(2015,01,16);
			    if(today >= toshowDivDate && today < tohideDivDate )
			    	var hideSpecial="false";
				else
					var hideSpecial="true";
			   this.data = {
				hide : hide,
				hideSpecial : hideSpecial
				}
			}

	});

	return LoadingView;
});