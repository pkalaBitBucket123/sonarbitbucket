/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'text!html/shopping/components/header/view.header.dust' ], function(require, Backbone, templateEngine,
		dustHelpers, connecture, headerTemplate) {

	templateEngine.load(headerTemplate, "view.header");

	HeaderView = connecture.component.core.View.extend({
		template : "view.header",
		frame_localization : {
			title : ''
		},
		events : {},
		initialize : function() {
			connecture.component.core.View.prototype.initialize.call(self);
		},
		render : function() {
			var self = this;

			var renderedHTML = this.renderTemplate({});

			$(self.el).html(renderedHTML);

			return this;
		}
	});

	return HeaderView;
});