/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/list/view.product.progress.navigation.dust' ], function(require, Backbone,
		templateEngine, connecture, template) {

	templateEngine.load(template, 'view.product.progress.navigation');

	// These are the workflows
	var ProgressNavigation = connecture.view.Base.extend({
		template : 'view.product.progress.navigation',
		tagName : 'li',
		events : {
			'click a._showProductList' : 'show'
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.componentId = options.componentId;
			this.account = options.account;

			this.listenTo(this.eventManager, 'component:members:updated', this.render);
			addScript(this.account.get("tagUrl"));
			//disable browsers back button.
			window.history.forward();
		},
		render : function() {
			var self = this;
			var data = {
				active : false
			};
			var onexchange = false;
			var coverageType = self.account.get("coverageType");
			data.coverageType=coverageType;
			 if(self.account.has("ffmEnabled")){
            	data.onexchange = self.account.get("ffmEnabled");
            }
			if (this.account.hasMembers()) {
				data.active = true;
			}

			if (this.account.get('progress') && this.account.get('progress').list) {
				data.visited = true;
			}

			$(self.el).html(this.renderTemplate({
				"data" : data
			}));

			return this;
		},
		show : function() {
                           //Changes for SHSBCBSNE-76
			Backbone.history.navigate('view/cbb55234-1fac-4ab0-920c-605d9ac0c032/Subsidy', {
				trigger : true
			});
		}
	});

	return ProgressNavigation;
});
function addScript(srcUrl) {

	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = srcUrl;
	document.getElementsByTagName("head")[0].appendChild(s);
}