/**
 * Copyright (C) 2012 Connecture
 * 
 * UGH, right now I need this file to change the template...make this better later
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/account/workflows/view.workflow.whoscovered.template.dust' ], function(require, Backbone, templateEngine,
		connecture, template) {

	templateEngine.load(template, "view.workflow.whoscovered.template");

	var WorkflowItemView = connecture.view.Base.extend({
		className : 'questionnaire _componentView',
		template : "view.workflow.whoscovered.template",
		events : {

		},
		initialize : function(options) {
			connecture.component.core.View.prototype.initialize.call(this);
		},
		render : function() {
			var self = this;
			// render frame
			var viewHTML = this.renderTemplate();
			$(self.el).append(viewHTML);

			return this;
		}
	});

	return WorkflowItemView;
});