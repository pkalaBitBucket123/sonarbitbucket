/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'components/account/views/view.account.questionnaire.summary',
		'components/account/views/view.account.questionnaire.members.summary', 'text!html/shopping/components/account/view.summary.dust', ], function(require, Backbone, templateEngine, connecture,
		QuestionnaireSummaryView, QuestionnaireMembersSummaryView, summaryTemplate) {

	templateEngine.load(summaryTemplate, "view.summary");

	var SummaryView = connecture.view.Base.extend({
		className : 'carousel-container',
		template : "view.summary",
		templateQuestionnaire : 'view.questionnaire.summary',
		templateQuestion : 'view.question.summary',
		events : {
			"click ._summary-close a" : "summaryClose",
			"click ._questionEdit" : "editQuestionSelected",
			"click ._questionRemove" : "clearQuestionSelected"
		},
		initialize : function(options) {
			this.allQuestions = options.allQuestions;
			this.questionnaires = options.questionnaires;
			this.account = options.account;
			this.costEstimatorConfigModel = options.costEstimatorConfigModel;
		},
		render : function() {
			var self = this;

			// Render the base elements, these will be appended to later
			$(self.el).append(this.renderTemplate());

			var memberLevelQuestions = [];
			var nonMemberLevelQuestions = [];
			var nonMemberLevelQuestionnaires = [];

			var questions = this.allQuestions;
			// Sort the member and non-member level questions
			questions.each(function(question) {
				var questionConfig = question.toJSON().config;

				if (questionConfig.memberLevelQuestion) {
					memberLevelQuestions.push(questionConfig);
				} else {
					nonMemberLevelQuestions.push(questionConfig);

					// Collate the Non-Member Level Questionnaires
					var questionnaires = self.questionnaires.toJSON().workflows;
					for ( var qnr = 0; qnr < questionnaires.length; qnr++) {
						if (questionConfig.questionnaireRefId == questionnaires[qnr].config.questionnaireRefId) {
							nonMemberLevelQuestionnaires.push(questionnaires[qnr].config);
						}
					}
				}
			});

			// We want a unique set of Non-Member Level Questionnaires
			nonMemberLevelQuestionnaires = _.uniq(nonMemberLevelQuestionnaires);

			// Render the Member Level Question Summaries
			var questionData = this.getQuestionDataFromAccountAndConfig(this.account.get("questions"), memberLevelQuestions);
			var questionnaireView = new QuestionnaireMembersSummaryView({
				members : new Backbone.Collection(this.account.get("members")),
				questions : new Backbone.Collection(questionData),
				costEstimatorConfig : self.costEstimatorConfigModel,
				// This is here now because of the widgets needed
				// account
				account : this.account
			});

			$('._memberQuestions', self.el).append(questionnaireView.render().el);

			self.views.push(questionnaireView);

			// Render the Non-Member Level Question Summaries
			var questionData = this.getQuestionDataFromAccountAndConfig(this.account.get("questions"), nonMemberLevelQuestions);
			var nonMemberLevelQuestionnairesCollection = new Backbone.Collection(nonMemberLevelQuestionnaires);
			nonMemberLevelQuestionnairesCollection.each(function(questionnaire) {
				var questions = [];
				// Filter down to just this questionnaire's questions
				new Backbone.Collection(questionData).each(function(questionInfo) {
					if (questionInfo.get("config").questionnaireRefId == questionnaire.get("questionnaireRefId")) {
						questions.push(questionInfo);
					}
				});

				var questionnaireView = new QuestionnaireSummaryView({
					title : questionnaire.title,
					questions : new Backbone.Collection(questions),
					// This is here now because of the widgets needed
					// account
					account : self.account
				});

				$('._otherQuestions', self.el).append(questionnaireView.render().el);

				self.views.push(questionnaireView);
			});

			$('a[title]', self.el).tooltip();

			$('.full-section .dataGroup.multiGroup .dataGroup').equal_height('start');
			$('.half-section.equal-height', self.el).equal_height('start');

			return this;
		},
		getQuestionDataFromAccountAndConfig : function(accountQuestions, allQuestions) {
			var questionData = [];
			if (_.isArray(accountQuestions) && _.isArray(allQuestions)) {
				for ( var q = 0; q < allQuestions.length; q++) {
					var allQuestion = allQuestions[q];
					var question = null;
					for ( var aq = 0; aq < accountQuestions.length && null == question; aq++) {
						if (accountQuestions[aq].questionRefId == allQuestions[q].questionRefId) {
							question = accountQuestions[aq];
						}
					}
					// pass in all of the question data as config, this will probably
					// get adjusted once we convert the summary over
					questionData.push(_.extend({}, question, {
						config : allQuestion
					}));
				}
			}

			return questionData;
		},
		summaryClose : function() {
			this.options.eventManager.trigger('view:summary:close');
		},
		editQuestionSelected : function(event) {
			var $selectedQuestionEdit = $(event.currentTarget);

			// TODO: These should be data- attribute to comply with HTML5
			var questionRefId = $selectedQuestionEdit.attr('questionRefId');

			var memberRefName = $selectedQuestionEdit.attr('memberRefName');

			// TODO: We should be pushing this down to the view to render
			Backbone.history.navigate('view/' + this.options.componentId + '/Questions/' + questionRefId + '/' + memberRefName, {
				trigger : true
			});

		},
		clearQuestionSelected : function(event) {

			var $selectedQuestionEdit = $(event.currentTarget);

			// TODO: These should be data- attribute to comply with HTML5
			var questionRefId = $selectedQuestionEdit.attr('questionRefId');

			var memberRefName = $selectedQuestionEdit.attr('memberRefName');

			this.options.eventManager.trigger('view:summary:widget:clear', questionRefId, memberRefName);
		},
		close : function() {

		}
	});

	return SummaryView;

});