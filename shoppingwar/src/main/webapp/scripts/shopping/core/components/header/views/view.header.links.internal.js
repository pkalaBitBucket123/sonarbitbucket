/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/views/base', 'common/component/component', 'components/header/views/view.help.faq',
		'text!html/shopping/components/header/view.header.links.internal.dust' ], function(require, Backbone, templateEngine, dustHelpers, BaseView, connecture, HelpFAQView, headerTemplate) {

	templateEngine.load(headerTemplate, "view.header.links.internal");

	HeaderLinksView = BaseView.extend({
		template : "view.header.links.internal",
		events : {
			'click ._help' : 'triggerHelp',
			'click ._liveChat' : 'showChat',
			'click ._callUs' : 'showCallUs',
			'click ._cart' : 'showCart',
			'click ._quoteShow' : 'showQuote',
			'click ._quoteHide' : 'hideQuote',
			'click ._removePlan' : 'removePlan',
			'click ._quoteSave' : 'saveQuote',
			'click ._messages' : 'showMessages',
				//<BCBSNE US=RIDERS FOR PROPOSAL MI>
			//'click ._quoteFinalize' : 'finalizeQuote',
			'click ._quoteFinalize' :'finalizeQuote',
			//</BCBSNE US=RIDERS FOR PROPOSAL MI>
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.account = options.account;
			this.actor = options.actor || null;
			this.plans = options.plans;
			this.quote = options.quote;
			this.quotingEnabled = options.data.quotingEnabled;
            this.models = options.models;
			this.listenTo(this.account, 'change:cart', this.render);
		},
		render : function() {
			var self = this;

			var productCount = 0;

			if (self.account.get("cart")) {
				_.each(self.account.get("cart").productLines, function(productLine) {
					if (productLine.products) {
						productCount = (productCount + productLine.products.length);
					}
				});
			}

			var headerLinkData = {
				productCount : productCount,
				isConsumer : !(this.actor.get("isInternalUser")),
				quotingEnabled : self.quotingEnabled
			};

			var renderedHTML = this.renderTemplate({
				data : headerLinkData
			});

			$(self.el).html(renderedHTML);

			return this;
		},
		showMessages : function() {
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.data.messagesURL
			});
		},
		showCallUs : function() {

		},
		showCart : function() {
			this.eventManager.trigger('view:cart:show');
		},
		showQuote : function() {
			this.eventManager.trigger('view:quoteReceipt:show');
		},
		hideQuote : function() {
			this.eventManager.trigger('view:quoteReceipt:hide');
		},
		saveQuote : function() {
			this.eventManager.trigger('view:quoteReceipt:save');
		},
		removePlan : function(event) {
			var planId = $(event.currentTarget).attr("plan-id");

			this.eventManager.trigger('view:quoteReceipt:removePlan', {
				planId : planId
			});
			//SHSBCBSNE-600
		//	if($('._finalizeQuotePlan').is(':visible'))
			this.eventManager.trigger('view:finalizeQuote');
		},
		showChat : function() {

			// for analytics
			this.eventManager.trigger('view:chat:show');
			return false;
		},
		triggerHelp : function() {
			this.eventManager.trigger('view:help:show');
			this.showHelp();
		},
		showHelp : function() {

			var self = this;

			var width = $(window).width();
			var pos = 'right top';
			if (width >= 1600) {
				pos = 'left top';
			}

			// check that help isn't already open (not modal)
			if (!self.faqDialog || (self.faqDialog && !(self.faqDialog.is(":visible")))) {
				var faqHTML = new HelpFAQView();
				var faqHTMLRendered = faqHTML.render().el;

				var firstCategory = $('.qaCategory', faqHTML.el).first();
				var firstAnswer = $('dd', firstCategory).first();
				firstAnswer.css('display', 'block');
				firstCategory.css('display', 'block');

				self.faqDialog = $(faqHTMLRendered).dialog({
					title : 'Help',
					width : 300,
					minheight : 400,
					draggable : false,
					dialogClass : 'helpDialog'
				}).dialog('widget').position({
					my : pos,
					at : 'right top',
					of : $(".appContainer")
				});
			}

			return false;
		},
		finalizeQuote : function(){
			this.eventManager.trigger('view:finalizeQuote');	
		},
		addRiderToQuotePlan : function(){
			alert('rider added');
		}
	});

	return HeaderLinksView;
});