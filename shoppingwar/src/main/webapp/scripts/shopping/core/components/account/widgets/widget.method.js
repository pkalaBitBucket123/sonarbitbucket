/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'components/account/widgets/views/view.widget.methods', 'shopping/common/collections/collection.workflow' ], function(require, Backbone, Widget,
		MethodsView, WorkflowCollection) {

	var MethodWidget = function(options) {
		Widget.call(this, options);
	};

	MethodWidget.prototype = Object.create(Widget.prototype);

	MethodWidget.prototype.initialize = function() {
		this.eventManager.on('view:method:selected', function(response) {
			// OK we need to determine here if we have question refs id to
			// filter on, if there are none then that means we don't show questions

			// we don't want to set the activeWorkflowRefNumber in the session anymore
			// instead we will set whether there is an active workflow, (only for workflows
			// with questions) and then what questions we need to filter on.

			var workflows = new WorkflowCollection(this.model.get('config').workflows);
			var workflow = workflows.getWorkflow(response);

			var activeWorkflow = false;
			var questionRefIds = [];
			var exit = false;
			// right now we are considering any workflow selected
			// to make it active. If a workflow doesn't have any question
			// ids and someone uses the mega menu, it will show all
			if (workflow) {
				activeWorkflow = true;
				questionRefIds = workflow.get('questionRefIds');
				// check here if we are gonna exit
				exit = _.isEmpty(questionRefIds);

				// if we are going to exit let's put them into the
				// default work flow in case they go back
				if (exit) {
					var defaultWorkflow = workflows.getDefaultWorkflow();
					if (defaultWorkflow && !_.isEmpty(defaultWorkflow.get('questionRefIds'))) {
						questionRefIds = defaultWorkflow.get('questionRefIds');
					}
				}

			}

			// might have to store this for when we render the method page again
			this.attributes.session.set("activeWorkflowRefNumber", response);
			this.attributes.session.set("workflowFilters", questionRefIds);
			this.attributes.session.set("activeWorkflow", activeWorkflow);
			// if we don't have any questions, then trigger an exit
			if (exit) {
				this.eventManager.trigger('widget:done', 'exit');
			} else {
				this.eventManager.trigger('widget:done');
			}

		}, this);
	};

	MethodWidget.prototype.start = function() {
		this.view = new MethodsView({
			model : this.model,
			account : this.attributes.account,
			eventManager : this.eventManager,
			session : this.attributes.session
		});
	};

	MethodWidget.prototype.getView = function() {
		return this.view;
	};

	MethodWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return MethodWidget;
});