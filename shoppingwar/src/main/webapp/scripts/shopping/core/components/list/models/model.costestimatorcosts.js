/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {
	var CostEstimatorCostsModel = ProxyModel.extend({
		initialize : function() {
			ProxyModel.prototype.initialize.call(this, {
				storeItem : 'costEstimatorCosts'
			});
		}
	});

	return CostEstimatorCostsModel;
});