/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'dust', 'dust-extensions' ], function(require, dust, dustExtensions) {

	dust.helpers['categoryDetailBenefits'] = function(chk, ctx, bodies, params) {

		// build a list of "benefitsCategories" with "benefits", up to a certain limit

		var count = 0;
		var limit = 0;
		var toggleLimit = 0;
		var doToggle = false;

		if (params.limit) {
			limit = params.limit;
		}

		if (params.toggleLimit) {
			toggleLimit = params.toggleLimit;
			doToggle = true;
		}

		// this will be the result object
		var limitedCategories = new Array();

		// this will be the current (full) collection of categories and benefits
		var benefitCategories = ctx.current();

		// object should be benefitCategories
		for ( var i = 0; i < benefitCategories.length; i++) {
			var limitedCategory = {};
			limitedCategory.name = benefitCategories[i].categoryName;
			limitedCategory.benefits = null;

			var currentBenefitArray = benefitCategories[i].benefits;
			var limitedBenefitArray = {};

			var underLimitCategories = (limit > 0 && count <= limit);
			if (underLimitCategories) {

				var benefitCount = 0;

				for ( var j = 0; j < currentBenefitArray.length; j++) {

					count++;

					var underLimit = (limit > 0 && count <= limit);
					if (underLimit) {
						benefitCount++;

						// set extra if needed
						if (doToggle && count > toggleLimit) {
							currentBenefitArray[j].extra = true;
						}

						limitedBenefitArray[j] = currentBenefitArray[j];

					} else {
						// return;
					}
				}

				if (benefitCount > 0) {
					limitedCategory.benefits = limitedBenefitArray;
				}
			} else {
				// return;
			}

			if (limitedCategory.benefits) {
				limitedCategories[limitedCategories.length] = limitedCategory;
			}
		}

		chk = chk.render(bodies.block, ctx.push({
			benefitCategories : limitedCategories
		}));

		return chk;
	};
	dust.helpers['displayProviders'] = function(chunk, context, bodies, params) {
		var inNetworkProviders = new Array();
		var outOfNetworkProviders = new Array();

		// if providers exist, determine if they are "in/out of Network"
		if (context.get("providers") && context.get("providers").length > 0) {
			if (context.get("networks")) {
				var planNetworks = context.get("networks");

				$.each(context.get("providers"), function(index, provider) {
					var providerNetwork = provider.provider.xrefid;
					var inNetwork = false;
					for ( var n = 0; planNetworks && !inNetwork && n < planNetworks.length; n++) {
						inNetwork = planNetworks[n] == providerNetwork;
					}
					if (inNetwork) {
						inNetworkProviders.push(provider.provider);
					} else {
						outOfNetworkProviders.push(provider.provider);
					}
				});
			}
		}

		return chunk.render(bodies.block, context.push({
			hasProviders : inNetworkProviders.length > 0 || outOfNetworkProviders.length > 0,
			inNetworkProviders : inNetworkProviders,
			outOfNetworkProviders : outOfNetworkProviders
		}));
	};
	 /*NE Change: [IN:067104]:[Begin]*/
	dust.helpers['calculateMonthlyPlanCostAmt'] = function(chunk, context, bodies, params) {

		var costAmt = 0;
		var costContext = context.get("cost");
		var monthlyCost = costContext.monthly;		
		var rate = monthlyCost.rate;			
		costAmt = rate.amount;		
		return chunk.write(costAmt.toFixed(2));
	};
	/*NE Change: [IN:067104]:[End]*/
	
	dust.helpers['calculateMonthlyPlanCostAmtRiders'] = function(chunk, context, bodies, params) {

		var costAmt = 0;
		var costContext = context.get("monthlyRate");
		return chunk.write(costContext.toFixed(2));
	};
	dust.helpers['calculateMonthlyPlanCost'] = function(chunk, context, bodies, params) {

		var cost = 0;

		var costContext = context.get("cost");
		var monthlyCost = costContext.monthly;

		var subsidyData = monthlyCost.subsidyData;
		if (subsidyData) {
			cost = subsidyData.netRate;
		} else {

			var rate = monthlyCost.rate;			
			cost = rate.amount;

			// contribution may or may not exist
			if (monthlyCost.contribution) {
				var contribution = monthlyCost.contribution;

				cost = rate.amount - contribution.amount;
				if (cost < 0) {
					cost = 0;
				}
			}
		}
		
		return chunk.write(cost.toFixed(2));
	};
	dust.helpers['styleProductLine'] = function(chunk, context, bodies, params) {
		var result = "";
		var currentContext = context.current();
		if (currentContext && currentContext.productLine) {
			result = currentContext.productLine.toLowerCase();
		}
		return result;
	};
	dust.helpers['calculateTotalCost'] = function(chunk, context, bodies, params) {

		var total = 0.0;

		var productLines = context.get("productLines");

		if (productLines) {
			_.each(productLines, function(productLine) {

				_.each(productLine.products, function(product) {

					var selectedProduct = product;

					var cost = 0;

					var costContext = selectedProduct.cost;
					var monthlyCost = costContext.monthly;

					var subsidyData = monthlyCost.subsidyData;
					if (subsidyData) {
						cost = subsidyData.netRate;
					} else {

						var rate = monthlyCost.rate;
						cost = rate.amount;

						// contribution may or may not exist
						if (monthlyCost.contribution) {
							var contribution = monthlyCost.contribution;
							cost = rate.amount - contribution.amount;
						}
					}
					if (cost > 0) {
						total += cost;
					}

				})
				var riders = productLine.selectedDentalRiders;
				var riderRate = 0.0;
				if(riders)
				//$.each(riders, function(k, v)	{
					if(riders.monthlyRate){
						riderRate = riderRate + parseFloat(riders.monthlyRate);
					}
				//});
				total = total + riderRate;
			});

		}

		return chunk.write(total);
	};

});