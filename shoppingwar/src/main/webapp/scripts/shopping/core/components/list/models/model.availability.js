/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, backbone, ProxyModel) {
	var AvailabilityModel = ProxyModel.extend({
		initialize : function(options) {
			ProxyModel.prototype.initialize.call(this, {
				storeItem : {
					item : 'availability',
					params : []
				}
			});
		},
		applyAvailability : function(options) {
			// plan collection needs to come through here
			this.save({}, options);
		}
	});

	return AvailabilityModel;
});