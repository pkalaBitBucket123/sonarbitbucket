/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the base view for render any product plan card, whether it is full detail, summary or compare.
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'jquery.charts', 'jquery-ui', 'text!html/shopping/components/list/plansDisclaimer.dust','text!html/shopping/components/list/plansExtraInfoDisclaimer.dust' ], function(require, backbone, templateEngine, BaseView, plansDisclaimerTemplate, plansExtraInfoDisclaimerTemplate) {

	BaseProductView = BaseView.extend({
		initialize : function(options) {
			this.options.account ? this.account = this.options.account : this.account = null;
			this.options.quote ? this.quote = this.options.quote : this.quote = null;
			this.options.actor ? this.actor = this.options.actor : this.actor = null;
			this.options.quotingEnabled ? this.quotingEnabled = this.options.quotingEnabled : false;
			this.options.displayPlansDisclaimer ? this.displayPlansDisclaimer = this.options.displayPlansDisclaimer : this.displayPlansDisclaimer = false;

			if (options.eventManager)
				this.eventManager = options.eventManager;

			// Upon the trigger of an event for this view, toggle
			// the given section
			this.on('view:product:section:toggle', this.toggleSelf, this);
		},
		events : {
			'click .section-toggle' : 'toggleSection'
		},
		render : function() {
			var self = this;
			var data = this.model.toJSON();
			// if we have an account, we also need to check to
			// see if this product is in the cart
			var inCart = false;
			var productLineInCart = false;
			if (this.account) {
				inCart = this.account.inCart(data.id);
				// We also need to check, if this isn't in cart but another one
				// part of this product line might be
				if (!inCart) {
					productLineInCart = this.account.productLineInCart(data.productLineCode);
				}
			}
			data['inCart'] = inCart;
			data['productLineInCart'] = productLineInCart;

			var inQuote = false;
			if (this.quote) {
				inQuote = this.quote.inQuote(data.planId);
			}
			data['inQuote'] = inQuote;
			data['isConsumer'] = true;
			if (this.actor) {
				data['isConsumer'] = !(this.actor.get("isInternalUser"))
			}
			
			// check quoting
			// do the consumer check, just to be safe
			data['quotingEnabled'] = self.quotingEnabled && !data['isConsumer'];
            data['displayPlansDisclaimer'] = self.displayPlansDisclaimer;

			// estimation product line
			var estimationProductLineCode = self.options.estimationProductLineCode;
			data.estimationProductLineCode = estimationProductLineCode;

			// provider product line
			var providerProductLineCode = self.options.providerProductLineCode;
			data.providerProductLineCode = providerProductLineCode;

			if (self.options.linkData) {
				data.linkData = self.options.linkData;
			}

			// cost view code
			// Changes by rsingh for user story 229   
			var costViewCode = self.options.costViewCode;
			data.costViewCode = costViewCode;
			data.description = self.options.model.get("description");
			
			this.setDataCoverageType(self.options.account, data);
			
			$(self.el).html(this.renderTemplate({
				data : data
			}));

			if (typeof data.totalPreferenceMatches !== 'undefined' && typeof data.positivePreferenceMatches !== 'undefined' && data.totalPreferenceMatches > 0) {

				// TODO: Global config somewhere
				$("._chart", self.el).pieChart({
					radius : 15,
					color : "#8dc4e2",
					stroke : "#555555",
					strokeWidth : 0,
					bgColor : "#366c88",
					bgRadius : 15,
					bgStroke : "#555555",
					bgStrokeWidth : 0,
					percent : data.positivePreferenceMatches / data.totalPreferenceMatches
				});
				// connecture.charts.percentagepie($("._chart", self.el), data.positivePreferenceMatches / data.totalPreferenceMatches, options);
			}

			$('.section-toggle', self.el).tooltip();

			return this;
		},
		/**
		 * This will get call when this view is informed that it needs to toggle a specific section
		 * 
		 * @param section
		 */
		toggleSelf : function(section) {
			// Collapse section
			var $collapser = $('.section-toggle[data-toggle-section="' + section + '"]', this.el);
			var $collapsibleSection = $collapser.siblings('.collapsible-section');

			$collapsibleSection.toggle().toggleClass('collapsed');

			// Update '.section-toggle'
			var isVisible = !$collapsibleSection.hasClass('collapsed'), displayTitle = isVisible ? 'Collapse this Section' : 'Show this Section', displayText = isVisible ? '&ndash;' : '+';

			$collapser.attr('title', displayTitle).html(displayText);
		},
		
		
		/**
		 * This method gets called when the DOM triggers an event to toggle a section. It will figure out which section to trigger then send out a global event to toggle all section of this type.
		 * 
		 * @param event
		 */
		toggleSection : function(event) {
			event.preventDefault();
			var section = $(event.currentTarget).attr('data-toggle-section');
			this.eventManager.trigger('view:product:section:toggle', section);
		},
		
		setDataCoverageType : function(optionsAccount, data){
			if(optionsAccount != undefined )
				data.coverageType = optionsAccount.get("coverageType");
		}
		
	});

	
	return BaseProductView;
});