/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'templateEngine', 'common/plugin', 'common/component/component', 'common/filters', 'shopping/common/collections/collection.product' ], function(require, Backbone,
		templateEngine, Plugin, connecture, Filters, ProductList) {

	/**
	 * Plugin should get access to the component that it is plugging into
	 * 
	 * I will attach stuff to the component itself, or it can if it so desires
	 */
	var FilterPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'component';
		this.id = 'Filters';
		// this is specific to component plugins, tells us which components
		// to plug into
		this.pluggedIds = [ 'b718c2e2-2119-4053-ad7b-99b1daa45be6' ];
	};

	FilterPlugin.prototype = Object.create(Plugin.prototype);
	// by rsingh for user stories 228 & 230
	FilterPlugin.prototype.addEvents = function() {
		this.eventManager.on("initialize:after", function() {
			// Going to attach the filters to the component
			if (this.models.plans) {
				this.filters = new Filters(this.models.plans, {
					filters : this.filterConfigurations
				}, this.models.account.get("coverageType") );

				// set the active group values on startup
				this.filters.setActiveGroupValues();

				// then take the list and process initial filters
				var activeSort = null;
				_.each(this.sortConfig.sorts, function(sort) {
					if (sort.selected) {
						activeSort = sort;
						return;
					}
				});

				// When we filter, we need to create a new current list, using the main
				// collection to filter against
				this.currentProductList = new ProductList(this.models.plans.filterProducts(this.filters.getActiveFilters()), {
					sort : activeSort
				});
			}

		}, this.plugged);

		this.eventManager.on('component:filters:reset', function(options) {

			if (this.models.plans) {

				// clear "hide" from models
				if (this.models.plans.models) {
					_.each(this.models.plans.models, function(model) {
						model.set({
							"hide" : false
						});
					});
				}

				// re-create filters from configuration
				this.filters.reset();
			}

			this.filters.setActiveGroupValues();

			this.eventManager.trigger('component:filter:product', this.filters.getActiveFilters(), options);

		}, this.plugged);

		this.eventManager.on('view:filters:clear', function(options) {
			// clear "hide" from models
			if (this.models.plans.models) {
				_.each(this.models.plans.models, function(model) {
					model.set({
						"hide" : false
					});
				});
			}

			// re-create filters from configuration
			this.filters.clear();

			this.filters.setActiveGroupValues();

			this.eventManager.trigger('component:filter:product', this.filters.getActiveFilters(), options);
		}, this.plugged);

		// This gets called when the filtering is complete, so we can then
		// update the view
		this.eventManager.on('component:filter:product:complete', function(options) {
			// default is true
			var refresh = true;
			if (options) {
				refresh = options.refresh ? true : false;
			}
			if (refresh) {
				this.eventManager.trigger('component:refreshCurrentView');
			}

		}, this);

		this.eventManager.on('view:toggleHidden', function(event, isVisible) {

			// toggles whether a filterGroup is collapsed, not whether a product is hidden
			// TODO | should make this naming more clear
			var filterGroup = this.filters.getFilterGroup(event);
			filterGroup.configuration.view.attributes.hidden = !isVisible;

		}, this.plugged);

		this.eventManager.on('view:processFilter', function(event) {
			// Process it on the filters object
			this.filters.processEvent(event);

			this.filters.setActiveGroupValues();

			// Then trigger an event to actually filter
			this.eventManager.trigger('component:filter:product', this.filters.getActiveFilters());
		}, this.plugged);

		this.eventManager.on('view:filter:trigger', function() {
			// Process it on the filters object
			// First one should always be the event object
			this.filters.trigger.apply(this.filters, arguments);
			// Then trigger an event to actually filter

			this.filters.setActiveGroupValues();
			
			this.eventManager.trigger('component:filter:product', this.filters.getActiveFilters());
		}, this.plugged);

		// This is the incoming event that we need to filter our product list
		// TODO: There is probably a better way to do this
		this.eventManager.on('component:filter:product', function(filters, options) {
			if (filters.length > 0) {
				var activeSort = null;
				_.each(this.sortConfig.sorts, function(sort) {
					if (sort.selected) {
						activeSort = sort;
						return;
					}
				});

				// When we filter, we need to create a new current list, using the main
				// collection to filter aginst
				this.currentProductList = new ProductList(this.models.plans.filterProducts(filters), {
					sort : activeSort
				});

			} else {
				// If there are no filters, make sure we clear
				this.currentProductList = this.models.plans;
			}

			// this call should then refresh the current view
			// update the filters data
			this.filters.update(this.currentProductList, this.models.plans.models);
			this.eventManager.trigger('component:filter:product:complete', options);
		}, this.plugged);

		this.eventManager.on('component:toggleHiddenProductFilter', function(event, targetValue) {
			// find the matching model..
			var modelMatch = null;

			_.each(this.models.plans.models, function(model) {

				if (model.id == targetValue) {
					modelMatch = model;
					return;
				}
			});

			if (modelMatch) {
				var hide = false;

				if (typeof modelMatch.get('hide') === 'undefined') {
					hide = true;
				} else if (!modelMatch.get('hide')) {
					hide = true;
				}

				modelMatch.set({
					"hide" : hide
				});
				this.eventManager.trigger('view:filter:trigger', event, hide);
			}
		}, this.plugged);
		
		// by rsingh for user stories 228 & 230
		this.eventManager.on('component:filter:add', function(id, configuration, coverageType) {
			var coverageType = this.models.account.get("coverageType");
			this.filters.add(id, configuration, coverageType);
		}, this.plugged);

	};

	return FilterPlugin;

});