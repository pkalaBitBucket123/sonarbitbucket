/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'components/list/views/view.product.compare.summary' ], function(require, Backbone, templateEngine, connecture,
		ProductCompareSummaryView) {
	/**
	 * This is a view for the product compare list, it will display the product compare view for each product we are comparing
	 */

	var ProductCompareView = connecture.view.Base.extend({
		initialize : function() {
			this.options.account ? this.account = this.options.account : this.account = null;
			this.options.actor ? this.actor = this.options.actor : this.actor = null;
			this.options.quote ? this.quote = this.options.quote : this.quote = null;
			this.options.quotingEnabled ? this.quotingEnabled = this.options.quotingEnabled : false;
			this.options.displayPlansDisclaimer ? this.displayPlansDisclaimer = this.options.displayPlansDisclaimer : this.displayPlansDisclaimer = false;

			// Bind on the global view event of toggling a section. This will get triggered
			// from a sub view so that we can tell other sub views to toggle a section
			this.listenTo(this.options.eventManager, 'view:product:section:toggle', this.toggleSection);
		},
		events : {
			'click ._benefitToggleLink' : 'toggleBenefitDetailsParent'
		},
		render : function() {
			var self = this;

			$(self.el).removeClass('after-sidebar');

			this.collection.each(function(item) {
				var view = new ProductCompareSummaryView({
					estimationProductLineCode : self.options.estimationProductLineCode,
					providerProductLineCode : self.options.providerProductLineCode,
					costViewCode : self.options.costViewCode,
					model : item,
					account : self.account,
					actor : self.actor,
					quote : self.quote,
					quotingEnabled : self.options.quotingEnabled,
					eventManager : self.options.eventManager
				});

				$(self.el).append(view.render().el);

				// store a reference for disposing later
				self.views.push(view);
			});
			var displayPlansDisclaimer = this.displayPlansDisclaimer;
			if (displayPlansDisclaimer == true) {
				$(self.el).append(templateEngine.render("plansExtraInfoDisclaimer", {}));
				$(self.el).append(templateEngine.render('plansDisclaimer', {
					quotingEnabled : true
				}));
			}
			// The compare/three-up view is usually only 3 but
			// if only two is passed in, add a special class.

			$('._currency', self.el).formatCurrency();
			$('._costEstimateCurrency', self.el).formatCurrency({
				roundToDecimalPlace : 0
			});

			$('a[title]', self.el).tooltip();
			//$('.plan-card .plan-name', self.el).equal_height('start');
			$('.plan-card .plan-preference .match-list', self.el).equal_height('start');
			$('.plan-card .plan-details .dataGroup', self.el).equal_height('start');

			if (self.options.collapseBenefits) {
				// expand benefits by default
				$('._benefitToggle', self.el).toggle(true);
				$('._benefitToggleLink', self.el).text('Less');
				$('._benefitToggleLink', self.el).attr('title', 'See Less Plan Highlights');
			}

			$('.cost-estimation-chart .bar-wrapper .bar .ui-progressbar-value[title]', self.el).tooltip();

			return this;
		},
		toggleBenefitDetailsParent : function(event) {

			event.preventDefault();

			// expand / collapse all extended benefit areas

			var self = this;

			self.benefitsVisible = this.benefitsVisible ? false : true;

			$('._benefitToggle', self.el).toggle(self.benefitsVisible);

			if (self.benefitsVisible) {
				$('._benefitToggleLink', self.el).text('Less');
				$('._benefitToggleLink', self.el).attr('title', 'See Less Plan Highlights');
			} else {
				$('._benefitToggleLink', self.el).text('More');
				$('._benefitToggleLink', self.el).attr('title', 'See More Plan Highlights');
			}
		},
		/**
		 * This gets called when a sub view toggles a section
		 * 
		 * @param event
		 */
		toggleSection : function(section) {
			// Loop through each one of our views and trigger an event
			// to tell it to toggle a given section
			_.each(this.views, function(view) {
				view.trigger('view:product:section:toggle', section);
			});
		}
	});
	return ProductCompareView;
});