/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'components/list/views/view.product.base', 'text!html/shopping/components/list/view.product.compare.summary.dust',
		'text!html/shopping/components/list/view.product.estimatedCost.dust' ], function(require, backbone, templateEngine, BaseView, productCompareSummaryTemplate, estimatedCostTemplate) {

	templateEngine.load(productCompareSummaryTemplate, 'view.product.compare.summary');
	templateEngine.load(estimatedCostTemplate, 'view.product.estimatedCost');

	/**
	 * This is the view for
	 */
	var ProductCompareSummaryView = BaseView.extend({
		tagName : 'div',
		className : 'plan-card',
		template : 'view.product.compare.summary',
		events : {
			'click ._detailsLink' : 'showDetail',
			'click ._planName' : 'showDetail',
			'click ._selectLink' : 'selectProduct',
			'click ._selectQuoteLink ' : 'selectForQuote',
			'click ._estimateCosts' : 'estimateCosts',
			'click ._planSelector' : 'togglePlanChecked',
			'click ._toggle' : 'toggleProduct',
			'click ._showQuestions' : 'showQuestions',
			'click ._providerSearch' : 'providerSearch',
			'click ._costBreakdown' : 'showCostBreakdown'
		},
		initialize : function(options) {
			this.events = _.extend({}, BaseView.prototype.events, this.events);
			BaseView.prototype.initialize.call(this, options);
			
			// add additional classes at the plan-card level
			if (this.model.get("isSuggestedPlan")) {
				$(this.el).addClass("suggested-plan");	
			}
			
			if (this.model.get("isBestMatch") || this.model.get("isLowestCost")) {
				$(this.el).addClass("marked-plan");
			}
		},
		render : function() {
			BaseView.prototype.render.call(this);

			$('._benefitToggle', this.el).hide();
			this.benefitsVisible = false;
			return this;
		},
		showDetail : function(event) {
			// Trigger a parent event that says we want to show the detail
			this.options.eventManager.trigger('view:showProductDetail', this.model.id);
			event.preventDefault();
		},
		selectForQuote : function(event) {
			if (!$('.btn-utility', $(event.currentTarget)).hasClass('disabled-button')) {
				this.options.eventManager.trigger('view:selectForQuote', this.model.toJSON().planId);
			}
			event.preventDefault();
		},
		selectProduct : function(event) {
			// This is going to trigger a component event to do something
			// Since this
			// TODO: Eventually switch to id
			this.options.eventManager.trigger('view:selectProduct', this.model.toJSON());
			event.preventDefault();
		},
		estimateCosts : function(event) {
			this.options.eventManager.trigger('view:estimateCosts', {});
		},
		showQuestions : function(event) {
			this.options.eventManager.trigger('view:questions:show');
		},
		togglePlanChecked : function(event) {

			var $selectedAction = $(event.currentTarget);

			if ($selectedAction.is(':checked')) {
				this.model.set({
					'checked' : true
				});
			} else {
				this.model.set({
					'checked' : false
				});
			}

			this.options.eventManager.trigger('view:planCheckedToggle', this.model.toJSON().id);
		},
		toggleProduct : function(event) {
			$(event.currentTarget).parent().attr("data-liffect", "fadeOut");
			$(event.currentTarget).parent().addClass("play");

			var hide = false;

			if (typeof this.model.get('hide') === 'undefined') {
				hide = true;
			} else if (!this.model.get('hide')) {
				hide = true;
			}

			this.model.set({
				"hide" : hide
			});

			this.options.eventManager.trigger('view:filter:trigger', event, hide);
		},
		providerSearch : function() {
			this.options.eventManager.trigger('view:provider.search');
		},
		showCostBreakdown : function() {
			this.options.eventManager.trigger('view:product:rates:show', this.model.get('id'));
		}
	});

	return ProductCompareSummaryView;
});