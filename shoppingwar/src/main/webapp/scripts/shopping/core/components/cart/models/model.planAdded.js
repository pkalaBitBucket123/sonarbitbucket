/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {
	var PlanAddedModel = Backbone.Model.extend({
		initialize : function() {

		}
	});

	return PlanAddedModel;
});