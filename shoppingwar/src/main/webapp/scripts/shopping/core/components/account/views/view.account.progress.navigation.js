/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/application/router', 'common/component/component', 'text!html/shopping/components/account/view.account.progress.navigation.dust' ], function(
		require, Backbone, templateEngine, Router, connecture, template) {

	templateEngine.load(template, 'view.account.progress.navigation');

	// These are the workflows
	var ProgressNavigation = connecture.view.Base.extend({
		template : 'view.account.progress.navigation',
		tagName : 'li',
		events : {
			'click a._show' : 'show',
			'click a._questionnaire' : 'showQuestionnaire',
			'click a._question' : 'showQuestion',
			'click a._summary' : 'showSummary'
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.session = options.session;
			// not sure we need this one anymore
			this.widgets = options.widgets;
			this.workflowItems = options.workflowItems;
			this.workflow = options.workflow;
			this.account = options.account;
			this.componentId = options.componentId;
			// when we get past the whos covered page, render the menu again
			this.listenTo(this.eventManager, 'component:members:updated', this.render);
			this.listenTo(this.eventManager, 'workflow:item:finished', this.render);
		},
		render : function() {
			var self = this;
            var coverageType = self.account.get("coverageType");  
			var data = {};

			// this could probably be moved in a more common area
			var selectedWorkflow = null;
			var selectedWidget = null;
			if (Backbone.history.fragment) {
				var fragments = Backbone.history.fragment.split('/');
				// we need at least 3 fragments to process the selected one
				if (fragments.length >= 2) {
					selectedWorkflow = fragments[2];
				}
				if (fragments.length >= 3) {
					selectedWidget = fragments[3];
				}
			}

			if (this.account.hasMembers()) {
				data.renderMenu = true;

				// This will be kind of configurable for now, we will have the main
				// menu to dump single widgets to and ones that want to expand out can.
				var coreMenu = [];
				var workflowMenus = [];
				// loop
				_.each(this.workflow, function(key) {
					if (_.isObject(key)) {
						key = key.item;
					}

					var config = self.workflowItems.get(key);
					var megaMenu = config.get('megaMenu');
					if (megaMenu && megaMenu.enabled) {
						// check to see if this work flow has workflows,
						// if it doesn't then it is simple
						if (_.isEmpty(config.get('workflows'))) {
							// by rsingh for user story 76
                            if(coverageType==="DEN" || coverageType==="STH"){
								if(config.get('label')!=='Subsidy' && config.get('label')!=='Summary' && config.get('label')!=='Method' ) {
									coreMenu.push({
										label : config.get('label'),
										route : '#view/' + self.componentId + '/' + config.get('id')
									});
								}	
						    }
						    else {
							if(config.get('label')!=='Subsidy' && config.get('label')!=='Summary') {
								coreMenu.push({
									label : config.get('label'),
									route : '#view/' + self.componentId + '/' + config.get('id')
								});
							}
                                                    }
						} else {
							// these are subworkflows, so confusing right?
							_.each(config.get('workflows'), function(subworkflow) {
								if (_.size(subworkflow.widgets) === 1) {
									var widget = self.widgets.getWidget(subworkflow.widgets[0]);
								
								// by rsingh for user story 76
									if(coverageType!=="DEN" && coverageType!=="STH"){
										if(subworkflow.config.title==='Your Preferences'){
											coreMenu.push({
												label : subworkflow.config.title,
												route : '#view/' + self.componentId + '/' + config.get('id')+ '/' + self.widgets.getWidget(subworkflow.widgets[0]).get('widgetId'),
											});
										}
									}
								/*	workflowMenus.push({
										items : {
											label : subworkflow.config.title,
											route : '#view/' + self.componentId + '/' + config.get('id') + '/' + widget.get('widgetId'),
											selected : selectedWidget === widget.get('widgetId')
										}
									}); */
								} else if (_.size(subworkflow.widgets) > 1) {
									var items = [];
									_.each(subworkflow.widgets, function(widgetId) {
										var widget = self.widgets.getWidget(widgetId);
										items.push({
											label : widget.get('config').title,
											route : '#view/' + self.componentId + '/' + config.get('id') + '/' + widget.get('widgetId'),
											selected : selectedWidget === widget.get('widgetId')
										});
									});

									// by rsingh for user story 76
									if(coverageType!=="DEN" && coverageType!=="STH"){
									if(subworkflow.config.title==='Your Preferences'){
										coreMenu.push({
											label : subworkflow.config.title,
											route : '#view/' + self.componentId + '/' + config.get('id')+ '/' + self.widgets.getWidget(subworkflow.widgets[0]).get('widgetId'),
										});
									}
									}
									/*workflowMenus.push({
										items : {
											label : subworkflow.config.title,
											route : '#view/' + self.componentId + '/' + config.get('id') + '/' + self.widgets.getWidget(subworkflow.widgets[0]).get('widgetId'),
											subItems : items,
											selected : selectedWorkflow === config.get('id')
										}
									}); */
								}

							});
						}
					}
				});

				data.menu = [ {
					items : coreMenu
				} ];
// by rsingh for user story 76
		//		data.menu = data.menu.concat(workflowMenus);

			} else {
				data.renderMenu = false;
			}

			data.coverageType = coverageType;
			$(self.el).html(this.renderTemplate({
				"data" : data
			}));

			return this;
		},
		show : function() {
			var firstWorkflow = this.workflow[0];

			Backbone.history.navigate('view/' + this.componentId + '/' + firstWorkflow, {
				trigger : true
			});
		},
		showSummary : function() {
			Backbone.history.navigate('view/' + this.componentId + '/summary', {
				trigger : true
			});
		},
		showQuestionnaire : function(event) {
			var $selectedAction = $(event.currentTarget);
			var questionnaireRefId = $selectedAction.attr('data-id');
			Backbone.history.navigate('view/' + this.componentId + '/questionnaire/' + questionnaireRefId, {
				trigger : true
			});
		},
		showQuestion : function(event) {
			var $selectedAction = $(event.currentTarget);
			var questionRefId = $selectedAction.attr('data-id');
			Backbone.history.navigate('view/' + this.componentId + '/question/' + questionRefId, {
				trigger : true
			});
		}

	});

	return ProgressNavigation;
});