/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'backbone', 'templateEngine', 'common/plugin', 'components/list/models/model.costestimatorcosts' ], function(require, log, Backbone, templateEngine, Plugin,
		CostEstimatorCostsModel) {

	var CostEstimatorPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'component';
		this.id = 'CostEstimator';
		// this is specific to component plugins, tells us which components
		// to plug into
		this.pluggedIds = [ 'b718c2e2-2119-4053-ad7b-99b1daa45be6' ];
	};

	CostEstimatorPlugin.prototype = Object.create(Plugin.prototype);

	CostEstimatorPlugin.prototype.initialize = function(options) {
		Plugin.prototype.initialize.call(this, options);
		var plugin = this;
		this.plugged.items.push({
			alias : 'costEstimatorConfig',
			dependency : true,
		});

		// This component handles some global events
		this.subscriptions = _.extend({}, this.subscriptions, {
			'component:updateCosts' : function() {
				if (plugin.isCostEstimatorIncluded() && (plugin.config.process === 'realtime' || this.active)) {
					log.debug("updating costs");
					plugin.plugged.models.costEstimatorCosts.fetch();
				}
			}
		});

		this.publish = _.extend({}, this.publish, {
			'view:estimateCosts' : 'account:estimateCosts'
		});

		this.plugged.listDeferreds.push(function() {
			return $.Deferred(function() {
				var self = this;

				plugin.plugged.models.costEstimatorCosts.fetch({
					// we do not want to post this change, since we
					// we are manually calling applyCosts...we might be able
					// to refactor this and just call fetch and not have
					// the success callback
					silent : true,
					success : function() {
						// do not trigger a reset here, because this is happening
						// before the list is rendered
						plugin.applyCostsToPlans.call(plugin.plugged, true);
						self.resolve();
					}
				});
			});
		});
	};

	CostEstimatorPlugin.prototype.addEvents = function() {
		var self = this;

		this.eventManager.on("initialize:before", function() {
			if (self.isCostEstimatorIncluded()) {
				// Whenever the cost estimator cost model changes, update the costs
				this.models.costEstimatorCosts.on('change', function() {
					self.applyCostsToPlans.call(this, true);
				}, this);
			}
		}, this.plugged);

		// This will handle startup
//		this.eventManager.on("initialize:after", function() {
//			if (self.isCostEstimatorIncluded()) {
//				// TODO: Right now this is going to force everything to recalculate
//				// on startup because the fetch updates the costs and then triggers
//				// an update to plans, which forces everything else to recalculate
//				// and render
//
//				// On startup, if there are no plans, there is no
//				// point in fetching costs
//				if (!this.models.plans.isEmpty()) {
//					this.models.costEstimatorCosts.fetch();
//				}
//
//			}
//		}, this.plugged);
	};

	CostEstimatorPlugin.prototype.applyCostsToPlans = function(triggerReset) {
		var products = this.models.costEstimatorCosts.get("product");

		if (products && products.length > 0) {
			var productPlans = this.models.plans;
			for ( var p = 0; p < productPlans.length; p++) {
				var productPlan = productPlans.models[p];

				costResult = null;
				for ( var c = 0; c < products.length && null == costResult; c++) {
					if (products[c].xid == productPlan.get("id")) {
						costResult = products[c];
					}
				}
				if (null != costResult) {
					productPlan.set({
						"estimatedCost" : costResult.cost,
						"maximumCost" : costResult.deductible + costResult.maxOOPCost,
						"maximumIsActual" : costResult.maxOOPCost > 0,
						"consumer" : costResult.consumer
					}, {
						silent : true
					});

				}
			}

			if (triggerReset) {
				this.models.plans.trigger('updated');
			}
		}
	};

	CostEstimatorPlugin.prototype.load = function() {
		if (this.isCostEstimatorIncluded()) {
			this.plugged.addModel(new CostEstimatorCostsModel(), 'costEstimatorCosts');
		}
	};

	CostEstimatorPlugin.prototype.isCostEstimatorIncluded = function() {
		return this.plugged.models.widgets.isCostEstimatorIncluded();
	};

	return CostEstimatorPlugin;
});