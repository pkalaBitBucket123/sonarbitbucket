/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'backbone', 'templateEngine', 'common/plugin', 'components/list/models/model.subsidyusage' ], function(require, log, Backbone, templateEngine, Plugin, SubsidyUsageModel) {

	var SubsidyPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'component';
		this.id = 'Subsidy';
		// this is specific to component plugins, tells us which components
		// to plug into
		this.pluggedIds = [ 'b718c2e2-2119-4053-ad7b-99b1daa45be6' ];
	};

	SubsidyPlugin.prototype = Object.create(Plugin.prototype);

	SubsidyPlugin.prototype.initialize = function(options) {
		Plugin.prototype.initialize.call(this, options);
		var plugin = this;
		this.plugged.listDeferreds.push(function() {
			return $.Deferred(function() {
				var self = this;

				if (plugin.plugged.models.plans.isEmpty()) {
					self.resolve();
					return;
				}

				plugin.subsidyModel.set({
					plans : plugin.plugged.models.plans.toJSON()
				}, {
					silent : true
				});
				plugin.subsidyModel.applySubsidy({
					silent : true,
					success : function() {
						plugin.applySubsidy(true);
						self.resolve();
					}
				});

			});
		});
	};

	SubsidyPlugin.prototype.addEvents = function() {
		// this is here because we don't have access
		// to the models at this point...should work on getting that
		// event into the life cycle so I don't have to do this
		this.eventManager.on("initialize:before", function() {

			this.plugged.models.account.on('change:cart', function() {
				// TODO: Do what you need to do here when the cart changes
			}, this);

		}, this);
	};

	SubsidyPlugin.prototype.applySubsidy = function(triggerReset) {
		var subsidyProducts = this.subsidyModel.get("plans");

		if (subsidyProducts && !_.isEmpty(subsidyProducts)) {
			var productPlans = this.plugged.models.plans;
			productPlans.each(function(product) {

				var productId = product.get("id");

				// update the product cost with subsidyData
				var cost = product.get("cost");

				var subsidyData = subsidyProducts[productId];

				if (subsidyData && subsidyData.appliedSubsidy !== 0) {
					var planSubsidyData = {};
					planSubsidyData.appliedSubsidy = subsidyData.appliedSubsidy;
					planSubsidyData.netRate = subsidyData.netRate;
					planSubsidyData.baseRate = subsidyData.baseRate;
					// update cost subsidy
					cost.monthly.subsidyData = planSubsidyData;
				} else {
					// update cost subsidy
					delete cost.monthly.subsidyData;
				}

				product.set({
					cost : cost
				}, {
					silent : true
				});
			});

			if (triggerReset) {
				this.plugged.models.plans.trigger('updated');
			}
		}
	};

	SubsidyPlugin.prototype.load = function() {
		var self = this;

		// attach the model to the plugin since we don't
		// need to expose this
		this.subsidyModel = new SubsidyUsageModel();

		this.subsidyModel.on('change', function() {
			self.applySubsidy(true);
		});

	};

	return SubsidyPlugin;
});