/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 * 
 * Common methods: - Common method to compile and store dust templates - Common method to render dust templates - Need to figure out a way to use a router to control multiple components
 * 
 * 
 */
define(
		[ 'module', 'log', 'backbone', 'templateEngine', 'jquery-formatCurrency', 'common/component/component', 'shopping/common/models/model.application.config',
				'components/cart/models/model.planAdded', 'components/cart/views/view.cart.add', 'text!html/shopping/components/cart/view.cart.dust',
				'text!html/shopping/components/cart/view.cart.navigation.dust', 'components/cart/views/view.cart.summary', 'components/list/views/view.product.summary',
				'shopping/common/views/view.modal.notification', 'components/cart/views/view.cart.progress.navigation', 'shopping/common/views/view.member.rates',
				'shopping/common/views/view.product.loading' ], function(module, log, Backbone, templateEngine, formatCurrency, connecture, AppConfigurationModel, PlanAddedModel, AddToCartView,
				ProductDecideTemplate, ProductDecideNavigationTemplate, ProductElectionSummaryView, ProductSummaryView, NotificationView, ProgressNavigation, MemberRatesView, ProductLoadingView) {

			templateEngine.load(ProductDecideTemplate, "view.cart");
			templateEngine.load(ProductDecideNavigationTemplate, 'view.cart.navigation');

			var ProductDecideNavigationView = connecture.view.Base.extend({
				template : 'view.cart.navigation',
				events : {
					'click ._btnCloseCart' : 'closeCart',
				},
				initialize : function() {
				},
				render : function() {

					// TODO | this is duplicated in view.header.links

					var productCount = 0;

					if (this.model.get("cart")) {
						_.each(this.model.get("cart").productLines, function(productLine) {
							if (productLine.products) {
								productCount = (productCount + productLine.products.length);
							}
						});
					}

					var data = {
						productCount : productCount
					};

					// Since this is partial...kind of hack it in there
					$(this.el).html(this.renderTemplate({
						data : data
					}));

					// apply tooltips
					$('a[title]', this.el).tooltip();

					return this;
				},
				closeCart : function(event) {
					event.preventDefault();
					// we also want to stop immediate propapgration because
					// the nav also has an event of toggling the component
					event.stopImmediatePropagation();
					this.options.eventManager.trigger('view:cart:close');
				}
			});
			

			var ProductDecideView = connecture.component.core.View.extend({
				template : "view.cart",
				frame_navigation : 'view.cart.navigation',
				frame_localization : {
					title : 'Items In Your Cart',
					componentId : 'decide-component',
					configClass : 'cart-section'
				},
				active : false,
				events : {
					'click ._continueShopping' : 'continueShopping',
					'click ._closeCart' : 'continueShopping',
					'click .cart-button' : 'showCart'
				},
				initialize : function() {
					var self = this;
					this.events = _.extend({}, connecture.component.core.View.prototype.events, this.events);
					connecture.component.core.View.prototype.initialize.call(self);
				},
				continueShopping : function(event) {
					event.preventDefault();
					this.options.eventManager.trigger('view:cart:close');
				},
				showCart : function() {
					this.options.eventManager.trigger('view:cart:show');
				},
				render : function(options) {
					var self = this;
					connecture.component.core.View.prototype.render.call(self, options);
					this.renderToBody();
					$('a[title]', self.el).tooltip();

					return this;
				},
				hide : function() {
					connecture.component.core.View.prototype.hide.call(this);
					$('.data-section.cart-section', this.el).removeClass('active');
				},
				show : function() {
					connecture.component.core.View.prototype.show.call(this);
					$('.data-section.cart-section', this.el).addClass('active');
					$('.content-header .content-navigation ._closeCart', this.el).show();
					$('.content-header .content-navigation ._cartDetails', this.el).hide();

				},
				minimize : function() {
					connecture.component.core.View.prototype.minimize.call(this);
					$('.data-section.cart-section', this.el).removeClass('active');
					$('.content-header .content-navigation ._closeCart', this.el).hide();
					$('.content-header .content-navigation ._cartDetails', this.el).show();
					this.hide();
				},
				/**
				 * Overriding this method because when this one is being selected we want it to seize the view
				 * 
				 * @param event
				 */
				toggleComponent : function(event) {
					// this is here because we want to toggle components and close others
					if (this.active) {
						// if it is currently visible, then hide...can't use toggle cause we want to do other things
						if ($(event.currentTarget).closest('._component').attr('data-visible') === 'true') {

						} else {
							$('._componentContent', this.el).show();
							$(this.el).attr('data-visible', true);
							this.options.eventManager.trigger('component:seize', $(this.el).attr('data-component-id'));
						}
					}
				},
			});

			var CartComponent = function(configuration) {

				var self = this;
				// The id is coming from component configuration
				// client side, we need to make sure to merge this in
				configuration = _.extend(configuration, {
					id : module.config().id
				});
				connecture.component.core.Base.call(self, configuration);

				this.productAddedModalId = '_productAddedModal';

				// need account info for reading/storing cart selections
				this.items = module.config().items;

				// component handles the following events:
				this.subscriptions = _.extend({}, this.subscriptions, {
					'component:showCart' : function() {
						var cartData = this.buildCartData();
						var isChildOnly = this.models.account.get("childOnly");
						cartData.childOnly=isChildOnly;
						
						this.regionManager.products.show(new ProductElectionSummaryView({
							model : cartData,
							eventManager : this.eventManager,
							account: this.models.account,
							childOnly:isChildOnly
						}));
						this.eventManager.trigger('component:seize');
						Backbone.history.navigate('view/' + this.id + '/open');
					},
					'component:removeProduct' : function(productLineCode, productId) {

						var self = this;

						self.removeProduct(productLineCode, productId);
						this.eventManager.trigger('component:cart:updated');
						self.refreshCartCount();
					},
					'component:selectProduct' : function(addedProduct, bestFitProducts) {

						var self = this;

						// add a product to the cart and display the productAdded modal
						self.productAdded(addedProduct);

						var productAddedModel = this.buildCartModalData(addedProduct, bestFitProducts);

						this.modal.show(new AddToCartView({
							eventManager : this.eventManager,
							model : productAddedModel
						}));

						this.eventManager.trigger('component:cart:updated');

						self.refreshCartCount();
					}
				});

				// when the following events are found, component triggers corresponding event

				// triggers :
				// something happened in one of the component's views that requires us to
				// trigger an event to be handled by another component
				this.publish = _.extend({}, this.publish, {
					'view:cart:show' : 'component:showCart',
					'view:showProductListDetail' : 'component:showProductListDetail',
					'view:showProductList' : 'component:showProductList',
					'view:selectProductLine' : 'component:selectProductLine',
					'view:coverages:show' : 'component:coverages:show',
					'view:provider.search' : 'component:provider.search',
					'component:mark:progress' : 'component:mark:progress',
				});

				this.configuration.layout = _.extend({}, this.configuration.layout, {
					View : ProductDecideView,
					options : {
						renderNavigation : true
					},
					regions : {
						'products' : '.content-section',
						'navigation' : '.content-navigation'
					}
				});

				this.routes = [ {
					// This route will be used to automatically add this
					// product to the cart
					// using a pretty name here
					route : 'view/cart/add/:item',
					callback : function(productId) {
						return {
							id : self.id,
							view : 'add',
							item : {
								id : productId
							}
						};
					}
				}, {
					// This route will be used to automatically add this
					// product to the cart
					// using a pretty name here
					route : 'view/cart/open',
					callback : function() {
						return {
							id : self.id,
							view : 'open',
							item : {}
						};
					}
				} ];
			};

			CartComponent.prototype = Object.create(connecture.component.core.Base.prototype);

			// register events : events coming from this component's view(s)
			CartComponent.prototype.registerEvents = function() {

				this.eventManager.on('initialize:after', function() {
					this.navigation.addView(ProgressNavigation, {
						eventManager : this.eventManager,
						componentId : this.id,
						account : this.models.account
					});

					// At this point, we need to also check to see if the URL has any
					// things we need to add to the cart
				}, this);

				this.eventManager.on("start", function(options) {
					var self = this;

					// self.refreshCartView();

					this.regionManager.navigation.show(new ProductDecideNavigationView({
						eventManager : self.eventManager,
						model : self.models.account
					}));
					// if there is a route handle that
					if (options.route) {
						if (options.route.view === 'open') {
							this.eventManager.trigger('view:cart:show');
						} else if (options.route.view === 'add') {
							// we need to check if we can add this to the cart first
							// for now, if it is empty, do not add
							if (!this.models.plans.isEmpty()) {
								var addProduct = this.models.plans.get(options.route.item.id);
								self.productAdded(addProduct);
								this.eventManager.trigger('component:cart:updated');
								self.refreshCartCount();
								this.eventManager.trigger('view:cart:show');
							}
						}
					} else {
						// otherwise render the hidden cart so that it is there
						var cartData = this.buildCartData();
						var isChildOnly = this.models.account.get("childOnly");
						cartData.childOnly=isChildOnly;

						this.regionManager.products.show(new ProductElectionSummaryView({
							model : cartData,
							eventManager : this.eventManager,
							account: this.models.account
						}));
					}

				}, this);

				this.eventManager.on("update", function(options) {
					if (options.route) {
						if (options.route.view === 'open') {
							this.eventManager.trigger('view:cart:show');
						} else if (options.route.view === 'closed') {
							this.eventManager.trigger('component:release');
						}
					} else {
						// By default, if there is an update, I think we might
						// have to release
						// TODO: This is messing up cases where the component list is being shown when it should not be
						// maybe this release looks to see if the component is active first before triggering a show?
						this.eventManager.trigger('component:release');
						// then minimize the cart
						this.minimize();
					}

				}, this);

				this.eventManager.on('view:product:rates:show', function(productId) {
					var product = this.models.plans.get(productId);
					this.modal.show(new MemberRatesView({
						eventManager : this.eventManager,
						account : this.models.account,
						model : product
					}));
				}, this);

				this.eventManager.on('view:cart:close', function() {
					// Release our seize request
					this.eventManager.trigger('component:release');
					// then minimize the cart
					this.minimize();

					// We also need to rerender the navigation to show any updates to the
					// cart totals
					this.regionManager.navigation.show(new ProductDecideNavigationView({
						eventManager : this.eventManager,
						model : this.models.account
					}));

					Backbone.history.navigate('view/' + this.id + '/closed');
				}, this);

				this.eventManager.on('view:addSuggestedProduct', function(addedProduct, bestFitProducts) {

					var self = this;

					// refresh the cart and the productAdded modal
					self.productAdded(addedProduct);

					var productAddedModel = this.buildCartModalData(addedProduct, bestFitProducts);

					this.modal.show(new AddToCartView({
						eventManager : this.eventManager,
						model : productAddedModel
					}));

					// We also need to rerender the navigation to show any updates to the
					// cart totals
					this.regionManager.navigation.show(new ProductDecideNavigationView({
						eventManager : self.eventManager,
						model : self.models.account
					}));

					this.eventManager.trigger('component:cart:updated');

				}, this);
				//<NEBRASKA RIDER SUPPORT MI>
				this.eventManager.on('view:addRiderToProduct', function(addedRider, productCode, planId) {

					var account = this.models.account;

					// if no cart exists for the account, create it
					var accountCart = account.toJSON().cart;
					if(accountCart.productLines[productCode]){
						accountCart.productLines[productCode].selectedRiders={};
						accountCart.productLines[productCode].selectedRiders['DENTAL']= addedRider;
					}
					account.unset('cart', {
						silent : true
					});

					account.set({
						cart : accountCart
					});
					this.eventManager.trigger('component:cart:updated');
					this.eventManager.trigger('view:cart:show');
				}, this);
				this.eventManager.on('view:removeRiderFromProduct', function(productCode , planId, riderId) {

					var account = this.models.account;

					// if no cart exists for the account, create it
					var accountCart = account.toJSON().cart;
					if(accountCart.productLines[productCode]){						
						delete accountCart.productLines[productCode].selectedRiders
					}
					account.unset('cart', {
						silent : true
					});

					account.set({
						cart : accountCart
					});
					this.eventManager.trigger('component:cart:updated');
					this.eventManager.trigger('view:cart:show');
				}, this);
//</NEBRASKA RIDER SUPPORT MI>
				this.eventManager.on('view:enroll', function() {

					var self = this;
					self.enroll();

				}, this);

				this.eventManager.on('component:cart:updated', function() {
					// TODO: This is a hack right now, we need to trigger that
					// we are updating the cart here but really we should be passing
					// in the actual cart object we are saving, not basically
					// taking what is there and passing it back in.

					// I think this component needs to be restructured a bit
					// so we have better points of knowing when we are setting
					// stuff into the account and saving
					var self = this;
					// we did something in the cart, trigger this progress
					this.eventManager.trigger('component:mark:progress', 'cart');
					
					this.models.account.save({}, {
						success : function() {
							self.models.account.trigger('change:cart');
						}
					});
				}, this);

				this.eventManager.on("view:closeAddToCartModal", function() {
					this.modal.close();
				}, this);

				this.eventManager.on("view:removeCartProduct", function(productLineCode, productId) {
					var self = this;
					self.removeProduct(productLineCode, productId);
					this.eventManager.trigger('component:cart:updated');
				}, this);

				this.eventManager.on("view:closeNotificationModal", function() {
					this.modal.close();
				}, this);

				/**
				 * 
				 */
				this.models.account.on("change:cart", function() {
					// build the cart data based on identifiers stored within the account
					var cartData = this.buildCartData();
					var isChildOnly = this.models.account.get("childOnly");
					cartData.childOnly=isChildOnly;

					this.regionManager.products.show(new ProductElectionSummaryView({
						model : cartData,
						eventManager : this.eventManager,
						account: this.models.account
					}));
				}, this);

				this.models.plans.on('request', function() {
					this.regionManager.block(new ProductLoadingView());
				}, this);

				// we need to unblock on the sync because the
				// reset might not always be called depending
				// on if nothing changed
				this.models.plans.on('sync', function() {
					this.regionManager.unblock();
				}, this);

				this.models.plans.on('reset', function() {
					var self = this;
					self._handleProductListUpdates();
				}, this);

			};

			CartComponent.prototype._handleProductListUpdates = function() {
				var self = this;

				var changeMessage = "Based on your coverage change, your plan rates have updated. ";

				var changeNotifications = self._checkCartUpdates();

				if (changeNotifications && changeNotifications.length > 0) {

					var notificationData = {};
					notificationData.message = changeMessage;
					notificationData.notifications = changeNotifications;

					this.modal.show(new NotificationView({
						eventManager : self.eventManager,
						data : notificationData
					}));
				}
			};

			/*
			 * checkCartUpdates
			 * 
			 * This method is intended to compare the products in the cart with products in the current product list. If a product rate in the list does not match the rate in the cart, or if the cart
			 * product is no longer available in the list, then changes and notification will be handled here. TODO : could use some refactoring, as the model work and the messaging should probably be
			 * separate.
			 */
			CartComponent.prototype._checkCartUpdates = function() {
				var self = this;

				var currentProducts = this.models.plans;
				var cartModel = this.models.account.get("cart");

				// TODO : maybe make collections of types of changes
				// made instead of building the messaging here?
				var changeNotifications = new Array();

				// only do check if something is in the cart
				if (cartModel && cartModel.productLines) {
					var productLines = cartModel.productLines;
					// product lines is a map-type so must iterate over properties
					for (k in productLines) {

						if (productLines.hasOwnProperty(k)) {

							var productLine = productLines[k];

							_.each(productLine.products, function(product) {

								var found = false;
								var rateChanged = false;
								var rateMessage = "";

								var productId = product.id;

								var productPlanCost = product.cost;
								var productPlanMonthlyCost = productPlanCost.monthly;
								var productPlanMonthlyRate = productPlanMonthlyCost.rate;
								var productPlanMonthlyRateAmt = productPlanMonthlyRate.amount;

								var productMonthlyContrib = productPlanMonthlyCost.contribution;
								var productMonthlyContribAmt = 0.00;
								if (productMonthlyContrib) {
									productMonthlyContribAmt = productMonthlyContrib.amount;
								}

								var productPremium = 0.00;
								if (productMonthlyContribAmt <= productPlanMonthlyRateAmt) {
									productPremium = (productPlanMonthlyRateAmt - productMonthlyContribAmt).toFixed(2);
								}

								// check these against currentProductList;
								var currentProductModels = currentProducts.models;

								// look through the list of products to find the cart product
								_.each(currentProductModels, function(currentModel) {
									if (found) {
										return;
									} else {
										var currentId = currentModel.get("id");
										if (currentId == productId) {
											found = true;
											if (found) {

												// 1. identify changes to plan rates for items in cart
												var currentPlanCost = currentModel.get("cost");
												var currentPlanMonthlyCost = currentPlanCost.monthly;
												var currentPlanMonthlyRate = currentPlanMonthlyCost.rate;
												var currentPlanMonthlyRateAmt = currentPlanMonthlyRate.amount;

												var currentMonthlyContrib = currentPlanMonthlyCost.contribution;

												var currentMonthlyContribAmt = 0.00;
												if (currentMonthlyContrib) {
													currentMonthlyContribAmt = currentMonthlyContrib.amount;
												}

												var currentPremium = 0.00;
												if (currentMonthlyContribAmt <= currentPlanMonthlyRateAmt) {
													currentPremium = (currentPlanMonthlyRateAmt - currentMonthlyContribAmt).toFixed(2);
												}

												if (currentPremium != productPremium) {
													rateChanged = true;

													var currentPremiumCurrency = $($('<div></div>').html(currentPremium)).formatCurrency().html();
													var productPremiumCurrency = $($('<div></div>').html(productPremium)).formatCurrency().html();

													if (parseFloat(currentPremium) > parseFloat(productPremium)) {
														// increase
														rateMessage = product.name + ' has increased from ' + productPremiumCurrency + ' to ' + currentPremiumCurrency + '.';
													} else {
														// decrease
														rateMessage = product.name + ' has decreased from ' + productPremiumCurrency + ' to ' + currentPremiumCurrency + '.';
													}
												}

												// 2. refresh the cart with potential new covered members and/or rates
												self.setProduct(currentModel.toJSON());
											}
										}
									}
								});

								// track changes
								if (!found) {
									self.removeProduct(productLine.code, productId);
									changeNotifications[changeNotifications.length] = product.name + ' is no longer available and has been removed.';
								} else if (rateChanged) {
									changeNotifications[changeNotifications.length] = rateMessage;
								}

							});
						}
					}

					// trigger a save to update any changes in coveredMembers and rates
					this.eventManager.trigger('component:cart:updated');

					return changeNotifications;
				}
			};

			/**
			 * This is used to build the data necessary for the view
			 * 
			 * @returns {___anonymous16105_16106}
			 */
			CartComponent.prototype.buildCartData = function() {

				var self = this;

				// This grabs the product ids out of the account
				var productIds = self.getSelectedProductIds();

				var cartData = {};

				// add cart configuration for views
				// provider product line
				if (self.models.widgets) {
					var widgetModel = self.models.widgets.getWidget('ProviderLookup', 'widget');
					if (widgetModel) {
						cartData.providerProductLine = widgetModel.get('config').productLineForProviderLookup;
					}
				}

				// current product models
				var productModels = self.models.plans.models;

				// get current cart model
				var account = self.models.account;

				// end result products
				var cartProducts = [];

				// find the added product within the shared "plans" model
				_.each(productIds, function(productId) {

					var productFound = false;

					_.each(productModels, function(plan) {
						if (!productFound) {
							if (plan.id == productId) {
								cartProducts[cartProducts.length] = plan;
								productFound = true;
								return;
							}
						}
					});
				});

				// now that we have the selected product details, construct the cart
				_.each(cartProducts, function(cartProductModel) {

					var cartProduct = cartProductModel.toJSON();

					// create productLine entry for selectedPlan

					var selectedProductLine = {};
					selectedProductLine.name = cartProduct.productLine;
					selectedProductLine.code = cartProduct.productLineCode;
					if(account.get('cart').productLines && account.get('cart').productLines[cartProduct.productLineCode] &&
							 account.get('cart').productLines[cartProduct.productLineCode].selectedRiders)
					{
						var riders = account.get('cart').productLines[cartProduct.productLineCode].selectedRiders;
						selectedProductLine.selectedDentalRiders = riders['DENTAL'];
						selectedProductLine.isRiderSelected=true;
						$.each(cartProduct.riders, function(i,r){
							if(r.riderId == riders['DENTAL'].riderId){
								r.selected=true;
							}
						});
					}
					// TODO | no more "product", but we don't have the
					// criteria set for how many plans you can have and
					// how many of what type you can have;
					// don't even know what "type" criteria is yet.
					// so for now, proceed as before, just using a
					// new structure
					// selectedProductLine.product = cartProduct;
					selectedProductLine.products = [];
					selectedProductLine.products[0] = cartProduct;

					// determine covered members for a the selected product line
					var coveredMembers = self.buildCoveredMembers(account.get("members"), cartProduct);
					selectedProductLine.coveredMembers = coveredMembers;

					// overrides current selection for productLine

					if (!cartData.productLines) {
						cartData.productLines = {};
					}
					cartData.productLines[cartProduct.productLineCode] = selectedProductLine;
				});

				// include productLineSortOrders on cartData
				var productLineSortOrders = {};
				var childOnly="";
				_.each(self.productLines, function(productLine) {

					var productLineCode = productLine.productLineCode;
					var productLineSort = productLine.sortOrder;
					productLineSortOrders[productLineCode] = productLineSort;
				});

				cartData.productLineSortOrders = productLineSortOrders;
                cartData.displayPlansDisclaimer = self.models.applicationConfig.get("displayPlansDisclaimer");

				return cartData;
			};

			CartComponent.prototype.buildCoveredMembers = function(membersJSON, cartProduct) {
				var coveredMembers = [];
				_.each(membersJSON, function(member) {
					_.each(member.products, function(product) {
						if (product.productLineCode == cartProduct.productLineCode) {

							// copy member data for the cart display
							// and enrollment usage
							var coveredMember = {};
							coveredMember.memberRefName = member.memberRefName;
							coveredMember.name = member.name;

							coveredMembers[coveredMembers.length] = coveredMember;
						}
					});
				});

				return coveredMembers;
			};

			CartComponent.prototype.refreshCartCount = function() {

				this.regionManager.navigation.show(new ProductDecideNavigationView({
					eventManager : this.eventManager,
					model : this.models.account
				}));

			};

			// converted
			CartComponent.prototype.getSelectedProductIds = function() {

				// note that productIds should actually be product internal codes,
				// if everything is working properly

				var selectedProductIds = new Array();

				// get account
				var accountModel = this.models.account;

				// get cart from the account
				if (accountModel.has("cart")) {
					var cartModel = accountModel.get("cart");
					if (cartModel.productLines) {
						var cartProductLines = cartModel.productLines;
						// iterate over cart product lines
						_.each(cartProductLines, function(cartProductLine) {
							// iterate over cart product line products
							_.each(cartProductLine.products, function(cartProduct) {
								// add each cart product to selectedProductIds
								selectedProductIds[selectedProductIds.length] = cartProduct.id;
							});
						});
					}
				}

				return selectedProductIds;

			};

			CartComponent.prototype.enroll = function() {

				// //////////////////////////////////////////
				// GTM Hack!
				// This is in here for now to get a GTM to fire
				// for enroll. Remove this once we can better configure
				// accessing the data
				// //////////////////////////////////////////
				this.eventManager.trigger('enroll', this.models.account.toJSON());
				// //////////////////////////////////////////
				// End GTM Hack!
				// //////////////////////////////////////////

				this.eventManager.trigger('exit', {
					key : 'checkoutEmployee'
				});
			};

			CartComponent.prototype.removeProduct = function(productLineCode, productId) {
				var cartModel = this.models.account.toJSON().cart;
				var cartProductLine = cartModel.productLines[productLineCode];
				if (cartProductLine) {
					// find the product matching the id and remove it,
					// although really there should only be one product for the productLine
					// -- haha, not ANYMORE!!!

					var deleteProductLine = false;
					var productToDelete = null;

					_.every(cartProductLine.products, function(product, idx) {
						if (product.id == productId) {
							productToDelete = product;
							if (cartProductLine.products.length <= 1) {
								deleteProductLine = true;
							}
							return;
						}
						return true;
					});

					delete productToDelete;
					if (deleteProductLine) {
						delete cartModel.productLines[productLineCode];
					}

					// if (cartProductLine.product && cartProductLine.product.id == productId) {
					// delete cartProductLine.product;
					// and because there are no products in the product line...
					// need to delete this within the context of the part object
					// delete cartModal.productLines[productLineCode];
					// }

					this.models.account.unset('cart', {
						silent : true
					});

					// 8-27-13 - Setting this to silent as well to stop a double
					// change trigger.
					this.models.account.set({
						cart : cartModel
					}, {
						silent : true
					});

					// this.eventManager.trigger('component:cart:updated');
				}
			};
			/**
			 * This function is used to update / replace a product (JSON) in the cart when either a new product is selected or the rate / coverage changes. Updating / replacing will most likely occur
			 * as a result of the product's tier / coverage type changing because the user selected to cover / not cover a member, or decided to remove a member altogether.
			 * 
			 * @param selectedProductJSON
			 */
			CartComponent.prototype.setProduct = function(selectedProductJSON) {
				var self = this;

				var productModel = selectedProductJSON;
				// get models to work with
				var account = self.models.account;

				// if no cart exists for the account, create it
				var accountCart = account.toJSON().cart;

				if (!accountCart) {
					accountCart = {};
					if (!accountCart.productLines) {
						accountCart.productLines = {};
					}
				}

				// create productLine entry for selectedPlan

				var selectedProductLine = {};
				selectedProductLine.name = productModel.productLine;
				selectedProductLine.code = productModel.productLineCode;

				// get time of addition
				var currentDate = new Date();
				var addTime = currentDate.getTime();

				var selectedProduct = {};
				selectedProduct.id = productModel.id;
				selectedProduct.name = productModel.name;
				selectedProduct.cost = productModel.cost;
				selectedProduct.premium = productModel.premium;
				selectedProduct.tier = productModel.tier;
				selectedProduct.planType = productModel.planType;
				selectedProduct.addedDate = addTime;
				// added because it is being used in the buildCoveredMembers call
				selectedProduct.productLineCode = productModel.productLineCode;

				// TODO | moving to product level, should remove placement in productLine (below)
				var productCoveredMembers = self.buildCoveredMembers(account.get("members"), selectedProduct);
				selectedProduct.coveredMembers = productCoveredMembers;

				// TODO | add the selected product to the product collection on the productLine
				// CHECK: productLine products exists?
				if (selectedProductLine.products) {
					// YES:
					// TODO | how do we determine if we can have multiple products for the selectedProductLine?
					// CHECK: are able to have multiple products per product line?
					// YES : make sure products are not the same, then add the additional product
					// NO : replace the current product
					// TODO | for now, always choose NO
					selectedProductLine.products[0] = selectedProduct;

				} else {
					// NO: create the array and set the product
					selectedProductLine.products = [];
					selectedProductLine.products[0] = selectedProduct;
				}

				// OLD; removed in favor of above
				// selectedProductLine.product = selectedProduct;

				// overrides current selection for productLine

				if (!accountCart.productLines) {
					accountCart.productLines = {};
				}

				// set covered members on the product line
				var coveredMembers = self.buildCoveredMembers(account.get("members"), selectedProduct);

				selectedProductLine.coveredMembers = coveredMembers;

				// this is the point where we are setting the product to the cart data
				accountCart.productLines[productModel.productLineCode] = selectedProductLine;

				// This will trigger update events. Ideally
				// we would update the account, then show the modal
				// then process the events so the UI is faster.
				account.unset('cart', {
					silent : true
				});
				// 8-27-13 - Setting this to silent as well to stop a double
				// change trigger.
				account.set({
					cart : accountCart
				}, {
					silent : true
				});
			};

			CartComponent.prototype.productAdded = function(addedProduct) {
				var productModels = this.models.plans.models;

				// find the added product within the shared "plans" model
				var selectedProductModel = null;

				_.each(productModels, function(plan) {
					if (plan.id == addedProduct.id) {
						selectedProductModel = plan;
						return;
					}
				});

				// set the selected product into the cart (also refreshes the cart view)
				// This also saves the cart in the account
				this.setProduct(selectedProductModel.toJSON());
			};

			CartComponent.prototype.buildCartModalData = function(addedProduct, bestFitProducts) {
				// return the planAddedModel
				// This is data for the view
				var cartData = this.buildCartData();
				var isChildOnly = this.models.account.get("childOnly");
				cartData.childOnly=isChildOnly;
				// get product count for display;
				// this is data for the view

				var productCount = 0;

				_.each(cartData.productLines, function(productLine) {
					if (productLine.products) {
						productCount = (productCount + productLine.products.length);
					}
				});

				// determine which product lines are still missing from the cart
				// this is data for the view
				var remainingProductLines = [];

				this.models.productLines.each(function(model) {
					var productLine = model.toJSON();

					var productLineFound = false;

					_.each(cartData.productLines, function(cartProductLine) {
						if (productLine.productLineCode == cartProductLine.code) {
							productLineFound = true;
							return;
						}
					});

					if (!productLineFound) {

						// find best-fit product for a non-selected product line

						var bestFitProduct = bestFitProducts[productLine.productLineCode];

						var remainingProductLine = {};
						remainingProductLine.product = bestFitProduct;
						remainingProductLine.displayName = productLine.productLineName;
						remainingProductLine.code = productLine.productLineCode;
						remainingProductLines[remainingProductLines.length] = remainingProductLine;
					}
				});

				// find the added product within the shared "plans" model
				// this is what we send to store in the account
				var selectedProductModel = null;

				_.each(this.models.plans.models, function(plan) {
					if (plan.id == addedProduct.id) {
						selectedProductModel = plan;
						return;
					}
				});

				// create productLine entry for selectedPlan
				// this is data for the view
				var selectedProductLine = {};
				selectedProductLine.name = selectedProductModel.get("productLine");
				selectedProductLine.code = selectedProductModel.get("productLineCode");
				selectedProductLine.product = selectedProductModel.toJSON();

				var addToCartModel = new PlanAddedModel();
				addToCartModel.set("productCount", productCount);
				addToCartModel.set("bestFitProducts", bestFitProducts);
				addToCartModel.set("selectedProductLine", selectedProductLine);
				addToCartModel.set("remainingProductLines", remainingProductLines);
				addToCartModel.set("cart", cartData);
				var isChildOnly = this.models.account.get("childOnly");
				addToCartModel.set("childOnly", isChildOnly);
				

				return addToCartModel;
			};

			CartComponent.prototype.getParams = function(requestParams) {
				var retVal = {};
				var url = $.url.parse();
				if (url.params) {
					_.each(requestParams, function(requestedParam) {
						if (url.params.hasOwnProperty(requestedParam)) {
							retVal[requestedParam] = url.params[requestedParam];
						}
					});
				}

				return retVal;
			};

			CartComponent.prototype.load = function(data) {

				this.productLines = data.productLines;

				connecture.component.core.Base.prototype.load.call(this, data);
			};

			CartComponent.prototype.minimize = function(data) {
				this.hide();
			};

			return CartComponent;
		});