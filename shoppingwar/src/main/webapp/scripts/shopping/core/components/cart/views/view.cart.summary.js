/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/components/cart/view.cart.summary.dust', 'components/cart/dust/helpers.cart', 'text!html/shopping/components/list/plansDisclaimer.dust','text!html/shopping/components/list/plansExtraInfoDisclaimer.dust' ], function(require, Backbone,
		templateEngine, BaseView, template, cartHelpers, plansDisclaimerTemplate ,plansExtraInfoDisclaimerTemplate) {

	templateEngine.load(template, "view.cart.summary");
	templateEngine.load(plansDisclaimerTemplate, "plansDisclaimer");
	templateEngine.load(plansExtraInfoDisclaimerTemplate, "plansExtraInfoDisclaimer");
	ProductElectionSummaryView = BaseView.extend({
		tagName : "div",
		className : "selected-products",
		template : "view.cart.summary",
		events : {
			'click ._detailsLink' : 'showDetail',
			'click ._planName' : 'showDetail',
			'click ._changePlanLink' : 'changePlan',
			'click ._removePlanLink' : 'removePlan',
			'click ._btnEnroll' : 'enroll',
			'click ._whoscovered' : 'editCoverages',
			'click ._providerSearch' : 'providerSearch',
			'click ._costBreakdown' : 'showCostBreakdown',
			//<NEBRASKA RIDER SUPPORT MI>
			'click ._addRiders' : 'addRiders',
			'click ._removeRiderLink': 'removeRider',
			'click ._benefitOverview' : 'showBenefitOverview'
		//</NEBRASKA RIDER SUPPORT MI>
		},
		//<NEBRASKA RIDER SUPPORT MI>
		addRiders : function(event) {

			var self = this;

			// stop the link event from firing
			event.preventDefault();

			// target the link
			var linkElement = event.currentTarget;

			// get the product line
			var productLine = $('._productLine', linkElement).val();
			
			var code= $(linkElement).attr('product-code');

			var selectedRider = null;

			// find the product for the productLine
			_.each(self.model.productLines[code].products[0].riders, function(rider) {
				if (productLine == rider.riderId) {
					selectedRider = rider;
				}
			});

			if (selectedRider) {
				this.options.eventManager.trigger('view:addRiderToProduct', selectedRider,$(linkElement).attr('product-code'),$(linkElement).attr('plan-id'));
			} else {
				log.error('error adding rider to cart');
			}
		},
		removeRider : function(event){
			var self = this;

			// stop the link event from firing
			event.preventDefault();

			// target the link
			var linkElement = event.currentTarget;

			// get the product line
			var riderId = $('._riderId', linkElement).val();
			
			
			this.options.eventManager.trigger('view:removeRiderFromProduct', $(linkElement).attr('product-code'),$(linkElement).attr('plan-id'), riderId);

		},
		//</NEBRASKA RIDER SUPPORT MI>
		showDetail : function(event) {

			// Trigger a parent event that says we want to show the detail
			event.preventDefault();
			var target = event.currentTarget;
			var productLine = $('._productLine', target).val();

			var cartData = this.model;

			// find the selected product in the cart for the productLine
			if (cartData.productLines && cartData.productLines[productLine]) {
				var productModel = cartData.productLines[productLine].product;
				if (productModel) {
					this.options.eventManager.trigger('view:showProductListDetail', productModel.id);
					this.options.eventManager.trigger('view:cart:close');
				}
			}
		},
		showBenefitOverview: function(event) {
			// Trigger a parent event that says we want to show the detail
			event.preventDefault();
			var target = event.currentTarget;
			var productLine = $('._productLine', target).val();
			var cartData = this.model;

			// find the selected product in the cart for the productLine
			if (cartData.productLines && cartData.productLines[productLine]) {
				var productModel = cartData.productLines[productLine].products;
				if (productModel) {
					//var planId = productModel[0].planId;
					//var effectiveMs = this.options.account.get("effectiveMs");
					//window.open("/bcbst/shopping/shoppingPlanBenefitOverviewPdf.action?planId="+planId+"&effectiveMs="+effectiveMs);
					var benefitSummaryUrl = productModel[0].benefitSummaryUrl;	
					var brochureUrl = productModel[0].brochureUrl;
					if(benefitSummaryUrl != null && benefitSummaryUrl != "" && benefitSummaryUrl!=undefined) {
						window.open("../resources/"+benefitSummaryUrl);
					}
					if(benefitSummaryUrl==undefined && brochureUrl!=null && brochureUrl!="" ){
						window.open("../resources/"+brochureUrl);
					}
				}
			}
		},
		editCoverages : function(event) {
			// Trigger a parent event that says we want to edit coverages
			event.preventDefault();
			this.options.eventManager.trigger('view:cart:close');
			this.options.eventManager.trigger('view:coverages:show');
		},
		changePlan : function(event) {
			// Trigger a parent event that says we want to change plan
			event.preventDefault();
			var target = event.currentTarget;
			var productLine = $('._productLine', target).val();

			var cartData = this.model;

			if (cartData && cartData.productLines[productLine]) {
				var productLineCode = cartData.productLines[productLine].code;
				this.options.eventManager.trigger('view:selectProductLine', productLineCode);
				this.options.eventManager.trigger('view:cart:close');
			}
		},
		removePlan : function(event) {
			// trigger an event on the cart component, passing productLine / productId
			event.preventDefault();
			var target = event.currentTarget;
			var productLine = $('._productLine', target).val();
			var productId = $(target).attr('data-id');

			var cartData = this.model;

			// find the selected product in the cart for the productLine
			if (cartData.productLines && cartData.productLines[productLine]) {
				var productModel = cartData.productLines[productLine].products[0];
				if (productModel) {
					this.options.eventManager.trigger('view:removeCartProduct', productLine, productModel.id);
				}
			}
		},
		initialize : function() {

		},
		render : function() {
			var self = this;
			var productCount = 0;

			var cartModelData = self.model;

			// get sorted cart as array...
			var cartData = self.getSortedCart(cartModelData);

			_.each(cartData.productLines, function(productLine) {
				if (productLine.products) {
					productCount = (productCount + productLine.products.length);
				}
			});

			cartData.productCount = productCount;

			// set configuration from model
			if (cartModelData.providerProductLine) {
				cartData.providerProductLine = cartModelData.providerProductLine;
			}
		
			if(this.options.account.has("childOnly"))
			var isChildOnly = this.options.account.get("childOnly");
			 cartData.childOnly=isChildOnly;

			var renderedHTML = this.renderTemplate({
				data : cartData,
			});

			$(self.el).html(renderedHTML);
            if (cartModelData['displayPlansDisclaimer'] == true) {
            	$(self.el).append(templateEngine.render("plansExtraInfoDisclaimer", {}));
				$(self.el).append(templateEngine.render('plansDisclaimer', {
					quotingEnabled : true
				}));
			}
			$('._currency', self.el).formatCurrency();

			return this;
		},
		enroll : function(event) {
			event.preventDefault();
			// call component to pass relevant data to
			this.options.eventManager.trigger('view:enroll');
		},
		providerSearch : function() {
			this.options.eventManager.trigger('view:provider.search');
			this.options.eventManager.trigger('view:cart:close');
		},
		findProductLine : function(productLineCode) {
			var self = this;
			var productLinesArray = self.model.productLines;

			var resultProductLine = null;
			_.each(productLinesArray, function(productLine) {
				if (productLine.code == productLineCode) {
					resultProductLine = productLine;
				}
			});

			return resultProductLine;
		},
		getSortedCart : function(cartData) {

			var self = this;

			var sortedProductLines = {};
			var childOnly="";
			var sortedCart = {};
			sortedCart.productLines = new Array();

			// get productLineSort
			// build map of product lines and sort orders
			var productLineSortOrders = cartData.productLineSortOrders;

			var productCount = 0;
			_.each(cartData.productLines, function(productLine) {
				if (productLine.products) {
					productCount = (productCount + productLine.products.length);
				}
			});

			for ( var i = 0; i < productCount; i++) {

				var lowestProductLine = null;

				// find lowest value not previously added
				_.each(cartData.productLines, function(productLine) {

					if (!sortedProductLines[productLine.code]) {

						if (!lowestProductLine) {
							lowestProductLine = productLine;
						} else {
							// compare
							var currentProductLineSort = productLineSortOrders[productLine.code];
							var lowestProductLineSort = productLineSortOrders[lowestProductLine.code];
							if (currentProductLineSort < lowestProductLineSort) {
								lowestProductLine = productLine;
							}
						}
					}
				});

				sortedProductLines[lowestProductLine.code] = lowestProductLine;
				sortedCart.productLines[sortedCart.productLines.length] = lowestProductLine;
			}
			sortedCart.childOnly = "";
			return sortedCart;
		},
		showCostBreakdown : function(event) {
			var productId = $(event.currentTarget).attr('data-id');
			this.options.eventManager.trigger('view:product:rates:show', productId);
		}
	});

	return ProductElectionSummaryView;
});