/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'components/account/views/view.account.question.summary',
		'text!html/shopping/components/account/view.questionnaire.member.summary.dust' ], function(require, Backbone, templateEngine, connecture, QuestionSummaryView, questionnaireSummaryTemplate) {

	templateEngine.load(questionnaireSummaryTemplate, "view.questionnaire.member.summary");

	var MemberSummaryView = connecture.view.Base.extend({
		template : 'view.questionnaire.member.summary',
		tagName : 'tr',
		initialize : function(options) {
			this.questions = options.questions;
			this.costEstimatorConfig = options.costEstimatorConfig;
			this.account = options.account;
		},
		render : function() {
			var self = this;
			var data = this.model.toJSON();
			data.questionTitles = [];

			// render the base template for this view
			$(self.el).html(self.renderTemplate({
				data : data
			}));

			// This might be a little goofy because now we have separate view
			// inside of a row column
			this.questions.each(function(question) {
				// Also passing in the member we are acting on
				var questionView = new QuestionSummaryView({
					model : question,
					member : self.model,
					costEstimatorConfig : self.costEstimatorConfig,
					account : self.account
				});

				$(self.el).append($('<td></td>').html(questionView.render().el));

				self.views.push(questionView);
			});

			return this;
		}
	});

	return MemberSummaryView;

});