/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'backbone', 'templateEngine', 'components/cart/dust/helpers.cart', 'common/component/component', 'text!html/shopping/components/cart/view.cart.planAdded.dust' ], function(
		require, log, Backbone, templateEngine, cartHelpers, connecture, template) {
	templateEngine.load(template, "view.cart.planAdded");

	var AddToCartView = connecture.view.Base.extend({
		template : "view.cart.planAdded",
		frame_localization : {
			title : 'TESTING',
			componentId : 'addToCart-component',
			configClass : 'cart-section'
		},
		active : false,
		events : {
			'click ._viewCart' : 'viewCart',
			'click ._viewProducts' : 'viewProducts',
			'click ._shopProductLine' : 'shopProductLine',
			//<NEBRASKA RIDER SUPPORT MI>
			'click ._addProduct' : 'addProduct',
			'click ._addRiders' : 'addRiders'
		//</NEBRASKA RIDER SUPPORT MI>
		},
		initialize : function() {
			var self = this;
			connecture.view.Base.prototype.initialize.call(self);
		},
		viewCart : function() {

			// close modal
			this.options.eventManager.trigger('view:closeAddToCartModal');
			// show cart component
			this.options.eventManager.trigger('view:cart:show');
		},
		viewProducts : function() {

			// close modal
			this.options.eventManager.trigger('view:closeAddToCartModal');
			// show product list component
			this.options.eventManager.trigger('view:showProductList');
		},
		shopProductLine : function(event) {

			// stop the link event from firing
			event.preventDefault();

			// target the link
			var linkElement = event.currentTarget;

			// get the product line
			var productLine = $('._productLine', linkElement).val();

			// close modal
			this.options.eventManager.trigger('view:closeAddToCartModal');

			// show product list component
			this.options.eventManager.trigger('view:selectProductLine', productLine);
		},
		addProduct : function(event) {

			var self = this;

			// stop the link event from firing
			event.preventDefault();

			// target the link
			var linkElement = event.currentTarget;

			// get the product line
			var productLine = $('._productLine', linkElement).val();

			var selectedProduct = null;

			// find the product for the productLine
			_.each(self.model.get("remainingProductLines"), function(remainingProductLine) {
				if (productLine == remainingProductLine.code) {
					selectedProduct = remainingProductLine.product;
				}
			});

			if (selectedProduct) {
				this.options.eventManager.trigger('view:addSuggestedProduct', selectedProduct, self.model.get("bestFitProducts"));
			} else {
				log.error('error adding recommended product to cart');
			}
		},
		//<NEBRASKA RIDER SUPPORT MI>
		addRiders : function(event) {

			var self = this;

			// stop the link event from firing
			event.preventDefault();

			// target the link
			var linkElement = event.currentTarget;

			// get the product line
			var productLine = $('._productLine', linkElement).val();

			var selectedRider = null;

			// find the product for the productLine
			_.each(self.model.get('selectedProductLine').product.riders, function(rider) {
				if (productLine == rider.riderId) {
					selectedRider = rider;
				}
			});

			if (selectedRider) {
				this.options.eventManager.trigger('view:addRiderToProduct', selectedRider,$(linkElement).attr('product-code'),$(linkElement).attr('plan-id'));
			} else {
				log.error('error adding rider to cart');
			}
		},
		//</NEBRASKA RIDER SUPPORT MI>
		render : function() {
			var self = this;

			var dataJSON = self.model.toJSON();

			var renderedHTML = this.renderTemplate({
				data : dataJSON
			});

			$(self.el).html(renderedHTML);

			$('._currency', self.el).formatCurrency();

			$('a[title]', self.el).tooltip();

			return this;
		}
	});

	return AddToCartView;

});