/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/workflows/workflow.item', 'shopping/common/collections/collection.widget' ], function(require, Backbone, WorkflowItem, WidgetList) {

	var QuestionsWorkflowItem = function(configuration, widgets, options) {
		// this is the prefiltered list of widgets, mainly used for questions
		this.baseWidgets = widgets.getFilteredWidgets(configuration.get('widgets'));
		WorkflowItem.call(this, configuration, widgets, options);

	};

	QuestionsWorkflowItem.prototype = Object.create(WorkflowItem.prototype);

	QuestionsWorkflowItem.prototype.start = function(options) {
		options = options || {};

		if (options.widget && this.options.session.get("activeWorkflow")) {

			var questionFilters = this.options.session.get("workflowFilters");

			if (!_.contains(questionFilters, options.widget)) {
				// start fresh with both
				this.widgets = new WidgetList(this.allWidgets.toJSON());
				this.baseWidgets = new WidgetList(this.allWidgets.toJSON());
			}
		}

		WorkflowItem.prototype.start.call(this, options);

	};

	QuestionsWorkflowItem.prototype.initialize = function() {
		var self = this;
		var questionFilters = [];
		// If we are given a question ref number to display along with the questionnaire

		// At this point we are trying to render a specific question in the questionnaire
		// Let's make sure that this questionnaire is in the active workflow, if it is
		// not then we will change the workflow
		if (this.options.session.get("activeWorkflow")) {
			questionFilters = this.options.session.get("workflowFilters");
		}

		// filter down the widgets first based on the filters from
		// the session which come from the method page technically
		if (questionFilters && !_.isEmpty(questionFilters)) {
			// do this twice I think
			this.widgets = this.widgets.getFilteredWidgets(questionFilters);
			// base widgets is really
			this.baseWidgets = this.baseWidgets.getFilteredWidgets(questionFilters);
		}

		// this is from some old code but I think the old
		// widgets rely on this to not explode
		var account = this.options.models.account;
		if (!account.get("questions")) {
			account.set({
				"questions" : []
			}, {
				silent : true
			});
		}

		WorkflowItem.prototype.initialize.call(this);

		// These are events that are specific to questions
		this.eventManager.on('view:question:show', function(widgetId) {
			// right now send this out to go back in,
			// this is not returning a model for some reason
			var workFlow = self.configuration.getWdigetWorkflow(widgetId);

			// cause this isn't confusing,
			self.options.eventManager.trigger('view:questionnaire:show', workFlow.config.questionnaireRefId, widgetId);
		}, this);

		// I am still not sure the best way to handle this but working with the old
		// system for now until we can come up with a better solution - TODO
		// most of these could be replaced with bindings to the account
		this.eventManager.on('view:workflow:widget:save', function(namespace) {
			// trigger global event for now
			self.options.eventManager.trigger('component:accountUpdated', namespace);
		}, this);
		// bubble these up as well
		this.eventManager.on('widget:provider:changed', function(response) {
			self.options.eventManager.trigger('widget:provider:changed', response);
		});
		this.eventManager.on('widget:estimateCosts', function(response) {
			self.options.eventManager.trigger('widget:estimateCosts', response);
		});
	};

	QuestionsWorkflowItem.prototype.buildWidgetData = function() {
		var localData = {
			"account" : this.options.models.account,
			"members" : this.options.models.account.get("members"),
			"costEstimatorConfigModel" : this.options.models.costEstimatorConfig,
			"currentMemberRefName" : this.options.parameters.currentMemberRefName,
		};

		return localData;
	};

	/**
	 * If we can't render, we need to determine whether to go back or close.
	 * 
	 * At some point we should handle if there are no questions
	 * 
	 * @returns {Boolean}
	 */
	QuestionsWorkflowItem.prototype.canRender = function() {
		var retVal = false;
		
		// This is used in multiple spots, when clicking the browse 
		// all button on the method page, it will ask if we can
		// render the questions, which we can't
		
		// This is also used for the mega-menu, to see if we can render 
		// questions
		
		// This is used for the summary bar to start questions if 
		// we have not answered them yet
		if (this.options.session.get("activeWorkflow")) {
			questionFilters = this.options.session.get("workflowFilters");

			if (!_.isEmpty(questionFilters)) {
				retVal = true;
			} else {
				retVal = {
					render : false,
					position : 'previous'
				};
			}
		} else {
			retVal = {
				render : false,
				position : 'previous'
			};
		}

		return retVal;
	};

	QuestionsWorkflowItem.prototype.exit = function(position) {
		// if we are exiting and it is the next, we need to render the summary
		if (position === 'next') {
			// upon exiting and showing the summary, trigger the close event
			this.options.eventManager.trigger('widget:questions:complete');
		}

		WorkflowItem.prototype.exit.call(this, position);

	};

	QuestionsWorkflowItem.prototype.filter = function() {
		// we started with one or ended up with one, do some filtering

		// TODO: There is an issue here
		if (this.activeWorkflowId) {
			this.setWorkflowStep(this.activeWorkflowId);
			// If we are given an active work flow it, filter down futher, this is really only used
			// for the questions workflow item, should extract out

			// we need to act on the base widgets, in cases where we might be changing a sub workflow
			this.widgets = this.baseWidgets.getFilteredWidgets([ this.activeWorkflowId ], 'questionnaireRefId');
		}
	};

	return QuestionsWorkflowItem;

});