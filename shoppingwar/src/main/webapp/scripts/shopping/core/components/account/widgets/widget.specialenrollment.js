/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'shopping/individual/components/account/widgets/views/view.widget.specialenrollment.enrollment.reason',
		'shopping/individual/components/account/widgets/views/view.widget.specialenrollment' ], function(require, Backbone, Widget, WhosCoveredEnrollmentView, SpecialEnrollmentView) {
	/**
	 * 
	 * This is going to have two views. One that will be used to draw the initial header on the WhosCovered view. Then it will have another view that will overtake the workflow item view to display
	 * the special enrollment stuff. Eventually it will need to be configurable somewhere to determine
	 * 
	 */
	var SpecialEnrollmentWidget = function(options) {
		Widget.call(this, options);
	};

	SpecialEnrollmentWidget.prototype = Object.create(Widget.prototype);

	SpecialEnrollmentWidget.prototype.initialize = function() {

		var self = this;
		this.demographicsData = this.attributes.demographicsData;

		this.eventManager.on('view:whoscovered:show', function(demographicsData) {
			this.eventManager.trigger('widget:showWhosCovered', demographicsData);
		}, this);

		this.eventManager.on('view:specialEnrollment:show', function(demographicsData) {
			this.eventManager.trigger('widget:showSpecialEnrollment', demographicsData);
		}, this);

		this.eventManager.on('view:specialEnrollment:cancel', function() {
			delete self.demographicsData.eventType;
			delete self.demographicsData.eventDate;
			this.eventManager.trigger('widget:cancelSpecialEnrollment');
		}, this);

		this.eventManager.on('view:whoscovered:close', function(response) {
			this.eventManager.trigger('widget:done');
		}, this);
	};

	SpecialEnrollmentWidget.prototype.canRender = function() {
		return true;
	};

	// Testing code, however the way special enrollment is coded now
	// I don't think this is necessary yet
	SpecialEnrollmentWidget.prototype.isComplete = function() {
		// if it isn't required, return ASAP
		if (!this.model.get('required')) {
			return true;
		}

		if (this.demographicsData.eventType && this.demographicsData.eventDate) {
			return true;
		} else {
			return false;
		}
	};

	SpecialEnrollmentWidget.prototype.getView = function(demographicsData) {

		this.view = null;

		var viewData = this.attributes;
		viewData.demographicsConfig = this.model;
		viewData.eventManager = this.eventManager;
		if (demographicsData) {
			viewData.demographicsData = demographicsData;
		} else {
			viewData.demographicsData = this.demographicsData;
		}

		if (this.isPrimary) {
			this.view = new SpecialEnrollmentView(viewData);
		} else {
			this.view = new WhosCoveredEnrollmentView(viewData);
		}

		return this.view;
	};

	SpecialEnrollmentWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return SpecialEnrollmentWidget;
});