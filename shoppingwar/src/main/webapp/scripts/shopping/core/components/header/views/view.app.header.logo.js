/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/views/base', 'text!html/shopping/components/header/view.app.header.logo.dust' ], function(require, Backbone, templateEngine,
		dustHelpers, BaseView, headerTemplate) {

	templateEngine.load(headerTemplate, "view.app.header.logo");

	AppHeaderLogoView = BaseView.extend({
		template : "view.app.header.logo",
		events : {},
		initialize : function() {
			this.logoData = {};
		},
		render : function() {

			var self = this;
			var renderedHTML = this.renderTemplate({
				data : this.logoData
			});

			$(self.el).html(renderedHTML);

			return this;
		}
	});

	return AppHeaderLogoView;
});