/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'module', 'jquery-ui', 'backbone', 'templateEngine', 'common/component/component', 'components/menu/views/view.menu.mega', 'text!html/shopping/components/menu/view.menu.dust' ], function(
		module, jqueryUI, Backbone, templateEngine, connecture, ProgressView, headerViewTemplate) {

	templateEngine.load(headerViewTemplate, 'view.menu');

	// This is the core view for the component, acting as the controller
	// for now. We will probably want to break this out eventually
	var MenuComponentView = connecture.component.core.View.extend({
		template : 'view.menu',
		className : '_component two-third-section',
		frame : null,
		initialize : function() {
			var self = this;
			connecture.component.core.View.prototype.initialize.call(self);
		},
		render : function() {
			var self = this;

			$(self.el).html(this.renderTemplate({
				data : {}
			}));

			return this;
		}
	});

	var MenuComponent = function(configuration) {
		var self = this;
		// The id is coming from component configuration
		// client side, we need to make sure to merge this in
		configuration = _.extend(configuration, {
			id : module.config().id
		});
		connecture.component.core.Base.call(this, configuration);

		// need account data; this is where progress level will be saved?
		this.items = module.config().items;

		this.subscriptions = _.extend({}, this.subscriptions, {
			'component:mark:progress' : function(componentKey) {
				var progress = self.models.account.get("progress") || {};

				progress[componentKey] = true;

				// don't save, just set, no need to waste calls on this
				// when it most cases another save will happen
				this.models.account.set({
					"progress" : progress
				}, {
					silent : true
				});

				this.regionManager.progressBar.show(new ProgressView({
					model : this.models.account,
					navigation : this.navigation,
					eventManager : this.eventManager
				}));
			}
		});

		this.publish = {
			'view:showProductList' : 'component:showProductList',
		};

		this.configuration.layout = _.extend({}, this.configuration.layout, {
			View : MenuComponentView,
			regions : {
				'progressBar' : '.appProgBar'
			}
		});
	};

	MenuComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	MenuComponent.prototype.registerEvents = function() {
		this.eventManager.on("start:after", function() {
			this.regionManager.progressBar.show(new ProgressView({
				model : this.models.account,
				navigation : this.navigation,
				eventManager : this.eventManager
			}));

		}, this);

		this.eventManager.on("update:after", function(options) {
			this.regionManager.progressBar.show(new ProgressView({
				model : this.models.account,
				navigation : this.navigation,
				eventManager : this.eventManager
			}));
		}, this);
	};

	MenuComponent.prototype.load = function(data) {
		// do nothing for now
	};

	MenuComponent.prototype.hide = function() {
		// do nothing, always show
	};
	MenuComponent.prototype.show = function() {
		// do nothing, always show
	};
	MenuComponent.prototype.minimize = function() {
		// do nothing, always show
	};

	return MenuComponent;
});