/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */
define([ 'module', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'common/workflows/workflow.item.factory', 'components/account/views/view.account.progress.navigation',
		'common/workflows/workflow.item', 'components/account/workflows/workflow.item.questions', 'components/account/workflows/workflow.item.whoscovered',
		'text!html/shopping/components/account/view.account.dust', 'jquery.selectToUISlider' ], function(module, Backbone, templateEngine, dustHelpers, connecture, WorkflowFactory,
		ProgressNavigation, WorkflowItem, QuestionsWorkflowItem, WhosCoveredWorkflowItem, accountTemplate) {

	templateEngine.load(accountTemplate, "view.account");

	/**
	 * This is the main component view, it defines all of the regions for the component.
	 */
	var AccountComponentView = connecture.component.core.View.extend({
		template : "view.account",
		frame_localization : {
			title : 'Provide Healthcare Needs',
			componentId : 'account-component',
			configClass : 'account-section'
		},
		active : true,
		events : {

		},
		initialize : function() {
			var self = this;
			this.events = _.extend({}, connecture.component.core.View.prototype.events, this.events);
			connecture.component.core.View.prototype.initialize.call(self);
			this.on('change:title', this.updateTitle, this);
		},
		render : function() {
			var self = this;
			connecture.component.core.View.prototype.render.call(self, {
				renderHeader : module.config().component.showHeader
			});
			this.renderToBody();
			return this;
		},
		updateTitle : function(title) {

		}
	});

	var AccountComponent = function(configuration) {
		var self = this;
		// The id is coming from component configuration
		// client side, we need to make sure to merge this in
		configuration = _.extend(configuration, {
			id : module.config().id
		});
		connecture.component.core.Base.call(this, configuration);

		this.items = module.config().items;

		this.configuration.layout = _.extend({}, this.configuration.layout, {
			View : AccountComponentView,
			regions : {
				'accountMain' : '.content-section',
			}
		});

		this.subscriptions = _.extend({}, this.subscriptions, {
			'account:estimateCosts' : function() {
				if (this.models.widgets.isCostEstimatorIncluded()) {
					// Since this method is from the outside and triggers
					// show cost estimator, we need to trigger an event to show this
					// component
					this.eventManager.trigger('show');
					var costEstimatorQuestion = this.models.widgets.getWidget('CostEstimator', 'widget');

					this.eventManager.trigger('workflow:item:done', 'Questions', null, {
						widget : costEstimatorQuestion.get('widgetId')
					});
				}
			},
			'component:provider.search' : function() {
				// Since this method is from the outside and triggers
				// show provider search, we need to trigger an event to show this
				// component
				this.eventManager.trigger('show');
				var providerSearchQuestion = this.models.widgets.getWidget('ProviderLookup', 'widget');

				this.eventManager.trigger('workflow:item:done', 'Questions', null, {
					widget : providerSearchQuestion.get('widgetId')
				});
			},
			'component:questions:show' : function() {
				// This is pretty much a non configurable hack
				// to get the question to show when clicked from the plan card
				// I do not endorse this but it needed to be done.
				this.eventManager.trigger('show');

				this.eventManager.trigger('workflow:item:done', 'Questions');
			},
			'component:coverages:show' : function() {
				// This is pretty much a non configurable hack
				// to get the question to show when clicked from the plan card
				// I do not endorse this but it needed to be done.
				this.eventManager.trigger('show');
				this.eventManager.trigger('workflow:item:done:alias', 'WhosCovered');
			}
		});

		this.publish = _.extend({}, this.publish, {
			'view:estimateCosts' : 'component:updateCosts',
			'view:provider:changed' : 'component:provider:changed',
			'component:members:updated' : 'component:members:updated',
			// informs the outside world that the account complete has completed
			'view:account:complete' : 'component:account:complete',
			'view:help:show' : 'component:showHelp'
		});

		this.routes = [ {
			name : '',
			route : 'view/account/WhosCovered',
			callback : function() {
				// the route callback must return the properties
				// around this route, this will come back through
				// the start/update events
				return {
					id : self.id,
					view : 'WhosCovered',
					alias : true
				};
			}
		} ];
	};

	AccountComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	AccountComponent.prototype.registerEvents = function() {

		this.eventManager.on('initialize:after', function() {
			if (this.models.account.get('coverageType') && (this.models.account.get('coverageType') === 'STH' || this.models.account.get('coverageType') === 'DEN'  )) {
				var newWF = new Array();
				newWF.push("Subsidy");
				newWF.push("WhosCoveredSEOpt");
				newWF.push("Method");
				newWF.push("Questions"); 
				newWF.push("Summary");
				this.models.workflow.attributes.items = newWF;
			} else if(this.models.account.get('specialEnroll') && (this.models.account.get('specialEnroll') === 'Yes' )){
				var newWF = new Array();
				newWF.push("WhosCoveredSE");
				newWF.push("Method");
				newWF.push("Questions"); 
				newWF.push("Summary");
				this.models.workflow.attributes.items = newWF;
			}
			else {
				var workflow = this.models.workflow.get('items');
			}
			this.navigation.addView(ProgressNavigation, {
				eventManager : this.eventManager,
				widgets : this.models.widgets,
				workflowItems : this.models.workflowItems,
				workflow : workflow,
				session : this.session,
				account : this.models.account,
				componentId : this.id
			});
		}, this);

		this.eventManager.on("start", function(options) {
			var workflowToStart = null;
			var initial = false;
			var alias = false;
			// var routeWidgetId = null;
			if (options.route && this._allowRoute()) {
				workflowToStart = options.route.view;
				alias = options.route.alias || false;
			} else {
				// at startup, if we don't have a route
				// grab the first
				workflowToStart = this.models.workflow.getDefaultItem();
				initial = true;
			}

			if (alias) {
				this.eventManager.trigger('workflow:item:done:alias', workflowToStart, null, options.route, initial);
			} else {
				// hate to add another parameter but we need one for now I think
				this.eventManager.trigger('workflow:item:done', workflowToStart, null, options.route, initial);
			}

		}, this);

		this.eventManager.on("update", function(options) {
			// if nothing matches leave as is
			if (options.route && this._allowRoute()) {
				if (this.activeWorkflowItem) {
					this.activeWorkflowItem.destroy();
					delete this.activeWorkflowItem;
				}

				var workflowToStart = options.route.view;

				this.eventManager.trigger('workflow:item:done', workflowToStart, null, options.route);
			}

		}, this);

		this.eventManager.on('workflow:item:done:alias', function(workflowAlias, position, route, initial) {

			// Translate the supplied workflowAlias into the actual workflow id using alias lookup;
			// for example, general WhosCovered becomes workflow-specific WhosCoveredSEOpt
			var workflowConfig = this.models.workflow.getWorkflowItemByAlias(workflowAlias, this.models.workflowItems.models);
			var workflowId = workflowConfig.item;

			this.eventManager.trigger('workflow:item:done', workflowId, position, route, initial);

		}, this);

		/**
		 * This is slowly turning into the done/next/previous/jump/start event :) TODO: Rename this guy
		 */
		this.eventManager.on('workflow:item:done', function(workflowId, position, route, initial) {
			// if we get an exit position, right away, LEAVE
			if (position === 'exit') {
				this.eventManager.trigger('workflow:exit');
				return;
			}

			var self = this;
			// this is the array of workflows
			var workflow = this.models.workflow.get('items');
			// we need to check the active one each time to see if there are special
			// instructions with what to do, given position
			if (this.activeWorkflowItem) {
				var activeWorkflowConfig = this.models.workflow.getWorkflowItem(this.activeWorkflowItem.configuration.id);

				// TODO: We could probably call the event again and change the parameters or something
				if (position === 'next' && activeWorkflowConfig.next) {
					workflowId = activeWorkflowConfig.next;
					position = 'jump';
				}

				if (position === 'previous' && activeWorkflowConfig.previous) {
					// if the previous is the route, then just use
					// history back
					if (activeWorkflowConfig.previous === 'route') {
						window.history.back();
						return;
					} else {
						workflowId = activeWorkflowConfig.previous;
						position = 'jump';
					}
				}

			}

			var nextWorkflowId = null;

			if (position === 'next' || position === 'previous') {

				var activeWorkflowIndex = this.models.workflow.indexOfWorkflowItem(this.activeWorkflowItem.configuration.id);

				if (position === 'next') {
					var proceed = activeWorkflowIndex !== -1 && activeWorkflowIndex + 1 <= _.size(workflow);

					if (proceed) {
						nextWorkflowId = workflow[workflow, activeWorkflowIndex + 1];
					}

				} else if (position === 'previous') {
					// if we are going previous, make sure the current one is greater than or equal to zero to proceed
					var proceed = activeWorkflowIndex !== -1 && activeWorkflowIndex - 1 >= 0;

					if (proceed) {
						nextWorkflowId = workflow[workflow, activeWorkflowIndex - 1];
					}
				}
			} else if (position === 'exit') {

			} else if (position === 'jump') {
				// eh
				nextWorkflowId = workflowId;
			} else {
				// just render the current one
				nextWorkflowId = workflowId;
			}

			if (_.isObject(nextWorkflowId)) {
				nextWorkflowId = nextWorkflowId.item;
			}

			// make sure we have found one
			if (nextWorkflowId) {
				// if we are rendering something new, destroy previous
				if (this.activeWorkflowItem) {
					this.activeWorkflowItem.destroy();
					delete this.activeWorkflowItem;
				}

				// grab the configuration for it
				var nextWorkflowItemConfig = this.models.workflowItems.get(nextWorkflowId);

				// TODO this should probably get returned as an object literal
				var workflowFactory = new WorkflowFactory();
				// create the item for it
				var nextWorkflowItem = workflowFactory.getWorkflow(nextWorkflowItemConfig.get('type'), nextWorkflowItemConfig, this.models.widgets, {
					models : this.models,
					eventManager : this.eventManager,
					session : this.session,
					history : {
						id : this.id
					},
					view : {
						el : this.regionManager.accountMain.el
					}
				});

				// set this anyway, in case we have to go back or
				// skip we know which one we are trying to render
				this.activeWorkflowItem = nextWorkflowItem;

				// ask it if we can render it
				var response = nextWorkflowItem.canRender();

				response = _.isObject(response) ? response : {
					render : response
				};

				if (response.render) {
					this.regionManager.accountMain.show(nextWorkflowItem.render());

					// route is either an object or false
					_.isObject(route) ? route.fromRoute = route.fromRoute || true : route = {};
					// if this is the initial workflow item, then we don't trigger
					route.trigger = initial ? false : true;

					nextWorkflowItem.start(route);
					var csrCode = $(this.models.account)[0].get("csrCode");
					var csrCodesArray = ["02","03","04","05","06"];
					if( $(this.models.account)[0].get("isOnExchangePlans")==true && csrCode!=undefined && (csrCodesArray.indexOf(csrCode) > -1) )
					{
						var finishSeeResultsButton = document.getElementById("finishAndSeeResults");
						if(finishSeeResultsButton!=null){
							finishSeeResultsButton.setAttribute("style","background: none repeat scroll 0 0 #E3E3E3; border: 2px solid #CCCCCC; color: #ACACAC; cursor:default; text-shadow:none !important;");
							$("#nextQuestionDiv").removeClass("_nextQuestion");
						}
					}
				} else {
					if (response.position) {
						this.eventManager.trigger('workflow:item:done', null, response.position);
					} else {
						this.eventManager.trigger('workflow:exit');
					}
				}
			}
		}, this);

		this.eventManager.on('workflow:exit', function() {
			this.eventManager.trigger('workflow:item:done', 'Summary');
			this.eventManager.trigger('widget:questions:complete');
		}, this);

		this.eventManager.on('widget:estimateCosts', function() {
			// this triggers updating of plan data
			this.eventManager.trigger('view:estimateCosts');
		}, this);

		this.eventManager.on('widget:provider:changed', function(data) {

			// update member provider data (currently for IFP 5/2/2013)
			var self = this;
			self.updateMemberProviders(data);

			// this triggers updating of plan data
			this.eventManager.trigger('view:provider:changed', data);
		}, this);

		this.eventManager.on('view:members:updated', function() {
			// this technically triggers a fetching of plans
			// won't trigger close
			this.eventManager.trigger('component:members:updated');
		}, this);

		/**
		 * TODO: this event should get renamed
		 */
		this.eventManager.on('widget:questions:complete', function() {
			// when the summary closes, right now we always
			// complete, but we do not minimize anymore
			// this.eventManager.trigger('minimize');
			this.eventManager.trigger('view:account:complete');
		}, this);

		// TODO: I would like to do away with this and just save on the model directly
		// I think the only place that still relies on callback event is the overmind
		this.eventManager.on('component:accountUpdated', function(changeNamespace) {
			var self = this;
			self.models.account.save({}, {
				wait : true,
				silent : true,
				success : function() {
					if (changeNamespace === 'questions') {
						self.models.account.trigger('change:questions');
					} else if (changeNamespace === 'members') {
						self.models.account.trigger('change:members');
					}
				}
			});
		}, this);
	};

	AccountComponent.prototype.updateMemberProviders = function(providerWidgetData) {
		// stubbed; override for IFP
	};

	AccountComponent.prototype.load = function(data) {

	};

	AccountComponent.prototype._allowRoute = function(route) {
		// ignoring the route for now, just need to make sure we have members
		// return this.models.account.hasMembers();
		return true;
	};

	AccountComponent.prototype.minimize = function() {
		// we don't want to minimize because the
		// summary bar will always be visible now
		// TODO - future enhancement. If minimize gets
		// called, close the current view and display the summary
		// bar

		this.eventManager.trigger('workflow:item:done', 'Summary', null, null, true);
	};

	return AccountComponent;
});