/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/account/view.methods.dust',
		'text!html/shopping/components/account/view.methods.choices.dust', 'text!html/shopping/components/account/view.methods.descriptions.dust' ], function(require, Backbone, templateEngine,
		connecture, template, methodsChoicesTemplate, methodsDescriptionsTemplate) {

	templateEngine.load(template, "view.methods");
	templateEngine.load(methodsChoicesTemplate, "view.methods.choices");
	templateEngine.load(methodsDescriptionsTemplate, "view.methods.descriptions");

	// These are the workflows
	var MethodsView = connecture.view.Base.extend({
		className : 'methods _componentView',
		template : "view.methods",
		methodsChoicesTemplate : "view.methods.choices",
		methodsDescriptionsTemplate : "view.methods.descriptions",
		events : {
			"click ._help" : "showHelp",
			"click .method-action" : "methodSelected"
		},
		initialize : function(options) {
			this.eventManager = this.options.eventManager;
			this.account = this.options.account;
			this.session = this.options.session;
		},
		render : function() {
			var self = this;

			var localData = {
				"workflows" : this.model.get('config').workflows,
				"account" : this.account.toJSON(),
				"session" : this.session.toJSON()
			};

			$(self.el).append(this.renderTemplate({
				"data" : localData
			}));

			$(".method-choices[data-liffect] .method-action").each(
					function(i) {
						$(this).attr(
								"style",
								"-webkit-animation-delay:" + i * 600 + "ms;" + "-moz-animation-delay:" + i * 600 + "ms;" + "-o-animation-delay:" + i * 600 + "ms;" + "animation-delay:" + i * 600
										+ "ms;");
						if (i == $(".method-choices[data-liffect] .method-action").size() - 1) {
							$(".method-choices[data-liffect]").addClass("play");
						}
					});

			return this;
		},
		methodSelected : function(event) {
			var $selectedMethod = $(event.currentTarget);

			var selectedWorkflowRefNumber = $selectedMethod.attr("data-id");

			this.eventManager.trigger('view:method:selected', selectedWorkflowRefNumber);
		},
		showHelp : function() {
			this.options.eventManager.trigger('view:help:show');
		}
	});

	return MethodsView;
});