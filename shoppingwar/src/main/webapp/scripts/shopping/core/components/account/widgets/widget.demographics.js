/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'components/account/widgets/views/view.widget.demographics'], function(require, Backbone, Widget, DemographicsView) {
	/**
	 * This widget will have two views and thus two states really.
	 * 
	 * We need a way to push out that this widget has to re-render based on state changes
	 * 
	 * Maybe the workflow item , listens to the widget and and reacts to a state change
	 */
	var DemographicsWidget = function(options) {
		Widget.call(this, options);
	};

	DemographicsWidget.prototype = Object.create(Widget.prototype);

	DemographicsWidget.prototype.initialize = function() {
		
		var self = this;
		// we want this to be null to begin with.
		self.demographicsInitialized = false;
		
		this.eventManager.on('view:specialenrollment:show', function(demographicsData) {
			// we are passing demographics outward, so we can't re-initialize from now on
			self.demographicsInitialized = true;
			this.eventManager.trigger('widget:showSpecialEnrollment', demographicsData);
		}, this);
	};

	DemographicsWidget.prototype.canRender = function(){
		return this.isPrimary;
	};
	
	DemographicsWidget.prototype.getView = function(demographicsData) {
		
		if (this.isPrimary){
			var viewData = this.attributes;
			viewData.demographicsConfig = this.model;
			viewData.eventManager = this.eventManager;
			viewData.demographicsInitialized = this.demographicsInitialized;
			if (demographicsData){
				viewData.demographicsData = demographicsData;	
			}

			// normal view
			this.view = new DemographicsView(viewData);

		} else {
			// empty view
			this.view = null;
		}

		return this.view;
	};

	DemographicsWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return DemographicsWidget;
});