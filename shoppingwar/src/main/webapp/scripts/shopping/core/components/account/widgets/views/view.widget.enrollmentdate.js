/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/account/view.whoscovered.enrollment.date.dust',], function(require, Backbone, templateEngine, connecture, template) {

	templateEngine.load(template, 'view.whoscovered.enrollment.date');

	var EnrollmentDateView = connecture.view.Base.extend({
		template : 'view.whoscovered.enrollment.date',
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.account = options.account;
			this.demographicsConfig = options.demographicsConfig.toJSON().config;
			this.demographicsData = options.demographicsData;
		},
		events : {

		},
		render : function() {

			var self = this;

			var data = {};

			if (this.demographicsData.eventDate) {

				// special enrollment data

				data.eventType = self.demographicsData.eventType;
				data.eventDate = self.demographicsData.eventDate;
				data.effectiveDate = self.demographicsData.effectiveDate;
				//data.effectiveDate = self.options.demographicsConfig.get("config").effectiveDate;

			} else if (this.demographicsData.effectiveDate) {

				// open enrollment data
				
				//data.daysLeftToEnroll = self.demographicsConfig.daysLeftToEnroll;
			//Changes by Priyanka M for SHSBCBSNE-2	
				data.nextOpenEnrollmentDate = self.demographicsConfig.enrollmentStartDate;
				data.enrollmentEndDate = self.demographicsConfig.enrollmentEndDate;
				//data.effectiveDate = self.demographicsData.effectiveDate;
				data.effectiveDate = self.options.demographicsConfig.get("config").effectiveDate;
				var dd=new Date(data.effectiveDate);
				var y =dd.getFullYear();
				// open enrollment data
				var oneDay = 24*60*60*1000;
				
				var startDateOE = new Date(self.demographicsConfig.startDateOE);
				var endDateOE = new Date(self.demographicsConfig.endDateOE);
				var today = new Date();
				today.setHours(0,0,0,0);
				var currYr= today.getFullYear();
				
				/**
				 * This code is introduced to find no of days left to enroll in OE as 
				 * we have OE start date and end date in different yrs - SHSBCBSNE-1279
				 */
				//Oct 1st 2013 - Dec 15th 2013
				if(today>=startDateOE && today<= endDateOE){
					data.daysLeftToEnroll= Math.round(Math.abs((endDateOE.getTime()-today.getTime())/(oneDay)));
				}else{
					data.daysLeftToEnroll = self.demographicsConfig.daysLeftToEnroll;
				}
				
				var d=dd.getDate();
				var m =dd.getMonth();
				var y =dd.getFullYear();
				var arr = new Array( "Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				var date = arr[m] + " " +d+ "," + " " + y;
				data.effectiveDateFormatted = date;
				data.isRenewing = self.account.get("renewingDefaultPlanId") ? true : false;
			}
			
			$(self.el).html(self.renderTemplate({
				data : data
			}));

			return this;
		},
		close : function() {
		}
	});

	return EnrollmentDateView;
});
// For SHSBCBSNE-2
var suffix = function(n) {
	var d = (n|0)%100;
	return d > 3 && d < 21 ? 'th' : ['th', 'st', 'nd', 'rd'][d%10] || 'th';
	};