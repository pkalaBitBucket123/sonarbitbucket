/**
 * Copyright (C) 2012 Connecture
 * 
 * Extending underscore with deep-extend
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'shopping/employee/components/account/dust/helpers.demographics','text!html/shopping/components/account/view.whoscovered.dust',
		'text!html/shopping/components/account/view.whoscovered.member.row.dust', 'underscore.deep-extend', 'jquery.maskedInput'], function(require, Backbone, templateEngine, connecture, demographicsHelpers, template, memberRowTemplate, maskedInput) {

	templateEngine.load(template, "view.whoscovered");
	templateEngine.load(memberRowTemplate, "view.whoscovered.member.row");

	var WhosCoveredView = connecture.view.Base.extend({
		template : "view.whoscovered",
		className : 'whoscovered _componentView',
		memberRowTemplate : "view.whoscovered.member.row",
		events : {
			"click ._whosCoveredEngage" : "continueToMethodsIfValid",
			// Demographic events
			"click .full-section > .dataGroup > ._addIndividual" : "addIndividual",
			"click .full-section > .dataGroup #remove" : "removeIndividual",
			"click .full-section > .dataGroup > table > tbody > tr > td > ._productLine" : "productLineSelected",
			"click .full-section > .dataGroup > table > thead > tr > th > ._productLineHeader" : "productLineSelectedForMembers",
			"change .full-section > .dataGroup ._whosCoveredField" : "newMemberValueChanged"
		},
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.productLines = options.productLines;
			this.eligibility = options.eligibility;
			this.account = options.account;
			this.relationships = options.relationships;
			this.genders = options.genders;

			if (!this.account.get("members")) {
				this.account.set({
					"members" : []
				}, {
					silent : true
				});
			}
		},
		addIndividual : function(event) {
			this.addIndividual(event);

			// this.renderNavBar();
		},
		removeIndividual : function(event) {
			this.removeIndividual(event);

			// this.renderNavBar();
		},
		productLineSelected : function(event) {
			this.productLineSelected(event);

			// this.renderNavBar();
		},

		productLineSelectedForMembers : function(event) {
			this.productLineSelectedForMembers(event);

			// this.renderNavBar();
		},
		newMemberValueChanged : function(event) {
			this.newMemberValueChanged(event);

			// this.persistMembers();
		},
		relationshipOptions : function(chunk, context, bodies, params) {
			var relationships = this.relationships.toJSON();

			var displayRelations = [];
			displayRelations[0] = {
				type : "",
				display : ""
			};
			for ( var r = 0; r < relationships.length; r++) {
				displayRelations.push(relationships[r]);
			}

			// Render an option for each of the defined relationship types
			for ( var i = 0; i < displayRelations.length; i++) {
				var selected = "";
				// If this is an existing member select the appropriate relationship
				if (context.get("memberRelationship")) {
					if (context.get("memberRelationship") == displayRelations[i].type) {
						selected = " selected";
					}
				}

				chunk.write("<option relationshipType=\"" + displayRelations[i].type + "\"" + selected + ">" + displayRelations[i].display + "</option>");
			}
		},
		genderOptions : function(chunk, context, bodies, params) {
			var genders = this.genders.toJSON();

			var displayGenders = [];
			displayGenders[0] = {
				key : "",
				display : ""
			};
			for ( var g = 0; g < genders.length; g++) {
				displayGenders.push(genders[g]);
			}

			// Render an option for each of the defined gender types
			for ( var i = 0; i < displayGenders.length; i++) {
				var selected = "";
				// If this is an existing member select the appropriate gender
				if (context.get("gender")) {
					if (context.get("gender") == displayGenders[i].key) {
						selected = " selected";
					}
				}

				chunk.write("<option gender=\"" + displayGenders[i].key + "\"" + selected + ">" + displayGenders[i].display + "</option>");
			}
		},
		renderGender : function(chunk, context, bodies, params) {
			var genders = this.genders.toJSON();

			var displayGenders = [];
			displayGenders[0] = {
				key : "",
				display : ""
			};
			for ( var g = 0; g < genders.length; g++) {
				displayGenders.push(genders[g]);
			}

			var gender = null;
			for ( var g = 0; g < displayGenders.length && gender == null; g++) {
				if (displayGenders[g].key == context.get("gender")) {
					gender = displayGenders[g];
				}
			}

			// Default to the first gender
			if (null == gender) {
				gender = displayGenders[0];
			}

			chunk.write(gender.display);
		},
		productSelection : function(chunk, context, bodies, params) {
			var memberRefName = context.get("memberRefName");
			var productLineCode = context.get("productLineCode");
			var memberProductEligibilities = context.get("memberProductEligibilities");
			var primaryDisabledByProduct = context.get("primaryDisabledByProduct");
			var account = context.get("account");
			var members = account.members;
			var productLines = this.productLines.toJSON();
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			var checked = "";
			var disabledText = "";

			if (members) {
				var matchingMemberIndex = null;
				for ( var rm = 0; rm < members.length && null == matchingMemberIndex; rm++) {
					var member = members[rm];
					if (member.memberRefName == memberRefName) {
						matchingMemberIndex = rm;
					}
				}

				var disabled = false;

				var matchingProductLineName = null;

				if (null != matchingMemberIndex) {
					var memberProductEligibility = memberProductEligibilities[matchingMemberIndex];
					// Only if eligibilities have been obtained for the member (will be null for Add Dependent flow)
					if (memberProductEligibility) {
						// Mark the product as disabled in the UI if the member is not eligible for it
						disabled = true;
						var matchingProductLineCode = null;
						for ( var mp = 0; mp < memberProductEligibility.eligibilities.length && null == matchingProductLineCode; mp++) {
							if (memberProductEligibility.eligibilities[mp].productLineCode == productLineCode) {
								disabled = !memberProductEligibility.eligibilities[mp].eligible;
								matchingProductLineCode = productLineCode;
								for ( var pl = 0; pl < productLines.length && null == matchingProductLineName; pl++) {
									if (productLines[pl].productLineCode == matchingProductLineCode) {
										matchingProductLineName = productLines[pl].productLineName;
									}
								}
							}
						}

						if (disabled) {
							disabledText = "disabled title=\"not eligible for " + matchingProductLineName + " coverage\"";
						}
					}

					// Only disable a Primary member that hasn't already been disabled based on eligibility
					if (members[matchingMemberIndex].isPrimary && !disabled) {
						var primaryProductDisabled = false;
						for ( var p = 0; primaryDisabledByProduct && p < primaryDisabledByProduct.length; p++) {
							if (primaryDisabledByProduct[p] == productLineCode) {
								disabledText = "disabled title=\"Primary required for " + matchingProductLineName + " coverage\"";
							}
						}
					}

					var products = members[matchingMemberIndex].products;

					// If this member has product selections
					if (products) {
						// Mark the product as selected in the UI if it is selected in the model
						var matchingProductLineCode = null;
						for ( var mp = 0; mp < products.length && null == matchingProductLineCode; mp++) {
							if (products[mp].productLineCode == productLineCode) {
								matchingProductLineCode = productLineCode;
							}
						}
						if (null != matchingProductLineCode) {
							checked = "checked ";
						}
					}
				}
			}

			var html = "<td class=\"product-cell\"><input type=\"checkbox\" class=\"_productLine\" productlinecode=\"" + productLineCode + "\" member-id=\"" + memberRefName + "\" " + checked + " "
					+ disabledText + "/></td>";
			chunk.write(html);
		},
		render : function() {
			var self = this;
			var coverageType = self.account.get("coverageType");
			// should this really happen here or during rendering at all?
			var valid = this.updateMemberEligibility();

			var disabledContinueButton = "";
			var helpText = "";
			if (!this.isCoverageSelected()) {
				disabledContinueButton = "disabled-button";
				helpText = " title=\"select coverage(s) to continue\"";
			}

			var account = self.account;

			var members = account.get("members");
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			var disabledProductLineSelection = this.getDisabledProductLineSelection();

			var selectedProducts = this.determineSelectedProductsFromMemberSelections();

			var primaryDisabledByProduct = this.determineIfPrimaryShouldBeDisabledPerProduct();

			var localData = jQuery.extend({
				"disabledContinueButton" : disabledContinueButton,
				"helpText" : helpText,
				"selectedProducts" : selectedProducts
			}, {
				"account" : account.toJSON()
			}, {
				"effectiveDate" : this.formatDate(new Date(account.get("effectiveMs")), "{Month:2}/{Date:2}/{FullYear}")
			}, {
				"productLines" : this.productLines.toJSON()
			}, {
				"memberProductEligibilities" : this.memberProductEligibilities
			}, {
				"disabledProductLineSelection" : disabledProductLineSelection
			}, {
				"primaryDisabledByProduct" : primaryDisabledByProduct
			}, {
				"relationships" : self.relationships.toJSON()
			}, {
				"genders" : self.genders.toJSON()
			}, {
				"members" : members
			});

			var output = this.renderTemplate({
				"data" : localData,
				"functions" : {
					relationshipOptions : function(chunk, context, bodies, params) {
						self.relationshipOptions(chunk, context, bodies, params);
					},
					genderOptions : function(chunk, context, bodies, params) {
						self.genderOptions(chunk, context, bodies, params);
					},
					renderGender : function(chunk, context, bodies, params) {
						self.renderGender(chunk, context, bodies, params);
					},
					productSelection : function(chunk, context, bodies, params) {
						self.productSelection(chunk, context, bodies, params);
					}
				}
			});

			if ($('._whosCoveredWidget', self.el).length == 0) {
				$(self.el).append("<div class=\"_whosCoveredWidget\"></div>");
			}

			$('._whosCoveredWidget', self.el).html(output);

			$(".dataGroup .coverage-table ._birthdate", self.el).bind({
				keydown : function(event) {
					return self.validateBirthDateKeyDown(event);
				}
			});

			// Update Continue button state
			this.updateContinueButton();

			this.validateDemographics();

			$('a[title]', self.el).tooltip();

			if (valid) {
				$("._validationErrors", self.el).hide();
			} else {
				var html = "<div class='error-instructions'>Product selections have changed based on dependent eligibility.</div><ul>Please review and press Continue</ul>";
				$("._validationErrors", self.el).html(html);
				$("._validationErrors", self.el).show();
			}
			
			this.applyDateMasking();
			
			return this;
		},
		determineSelectedProductsFromMemberSelections : function() {
			var selectedProducts = [];

			var memberSelectedProducts = {};
			var availableProductLines = this.productLines.toJSON();
			for ( var pl = 0; pl < availableProductLines.length; pl++) {
				memberSelectedProducts[availableProductLines[pl].productLineCode] = 0;
			}

			var members = this.account.get("members");
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			for ( var m = 0; m < members.length; m++) {
				var member = members[m];
				for ( var p = 0; member.products && p < member.products.length; p++) {
					var productLineCode = member.products[p].productLineCode;
					memberSelectedProducts[productLineCode] = memberSelectedProducts[productLineCode] + 1;
				}
			}

			for ( var pl = 0; pl < availableProductLines.length; pl++) {
				var productLineCode = availableProductLines[pl].productLineCode;
				if (memberSelectedProducts[productLineCode] == members.length) {
					selectedProducts.push({
						"productLineCode" : productLineCode
					});
				}
			}

			return selectedProducts;
		},
		determineIfPrimaryShouldBeDisabledPerProduct : function() {
			var primaryShouldBeDisabledForProducts = [];

			var nonPrimaryMemberProducts = {};

			var members = this.account.get("members");
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			for ( var m = 0; m < members.length; m++) {
				var member = members[m];
				if (!member.isPrimary) {
					for ( var p = 0; member.products && p < member.products.length; p++) {
						nonPrimaryMemberProducts[member.products[p].productLineCode] = true;
					}
				}
			}

			for (productLineCode in nonPrimaryMemberProducts) {
				primaryShouldBeDisabledForProducts.push(productLineCode);
			}

			return primaryShouldBeDisabledForProducts;
		},
		getPrimaryMember : function() {
			var members = this.account.get("members");
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			var primaryMember = null;
			for ( var m = 0; m < members.length && primaryMember == null; m++) {
				var member = members[m];

				if (member.isPrimary) {
					primaryMember = member;
				}
			}
			return primaryMember;
		},
		getDisabledProductLineSelection : function() {
			var productLines = this.productLines.toJSON();
			var memberProductEligibilities = this.memberProductEligibilities;
			var disabledProductLineSelection = [];
			for ( var p = 0; p < productLines.length; p++) {
				var productLineCode = productLines[p].productLineCode;
				var eligibleProductSelection = true;
				for ( var m = 0; m < memberProductEligibilities.length && eligibleProductSelection; m++) {
					var memberProductEligibility = memberProductEligibilities[m];
					for ( var e = 0; e < memberProductEligibility.eligibilities.length && eligibleProductSelection; e++) {
						if (memberProductEligibility.eligibilities[e].productLineCode == productLineCode) {
							eligibleProductSelection &= memberProductEligibility.eligibilities[e].eligible;
						}
					}
				}
				if (!eligibleProductSelection) {
					disabledProductLineSelection.push({
						productLineCode : productLineCode
					});
				}
			}

			return disabledProductLineSelection;
		},
		updateMemberEligibility : function() {
			var eligibility = this.eligibility.get("memberProductLines");
			var account = this.account;
			var productLines = this.productLines.toJSON();
			var members = account.get("members");

			var memberProductEligibilities = [];

			// Determine if member product selections are valid based on eligibility
			var valid = true;
			for ( var m = 0; m < members.length && valid; m++) {
				var member = members[m];
				var memberEligibility = null;

				memberProductEligibilities[m] = {
					memberRefName : member.memberRefName,
					eligibilities : []
				};

				for ( var e = 0; e < eligibility.length && null == memberEligibility; e++) {
					if (eligibility[e].memberKey == member.memberRefName) {
						memberEligibility = eligibility[e];
					}
				}
				if (memberEligibility) {
					var memberProducts = _.clone(member.products);
					for ( var p = 0; p < productLines.length; p++) {
						var productLineCode = productLines[p].productLineCode;
						var memberProductLineIndex = null;
						for ( var mp = 0; mp < memberProducts.length && memberProductLineIndex == null; mp++) {
							if (memberProducts[mp].productLineCode == productLineCode) {
								memberProductLineIndex = mp;
							}
						}

						var memberEligibleForProductLine = true;

						// Member Eligibility only has productLineName available
						if (memberEligibility.productLineEligibility[productLineCode]) {
							valid &= true;
						} else {
							// If there was a selection before, remove it
							if (memberProductLineIndex) {
								member.products.splice(memberProductLineIndex, 1);

								// Also mark the product selection as invalid for error reporting
								valid = false;
							}

							memberEligibleForProductLine = false;

							var products = _.clone(account.get("products"));
							for ( var sp = 0; products && sp < products.length; sp++) {
								if (products[sp].productLineCode == productLineCode) {
									account.get("products").splice(sp, 1);
								}
							}
						}

						memberProductEligibilities[m].eligibilities[p] = {
							productLineCode : productLineCode,
							eligible : memberEligibleForProductLine
						}
					}
				}
			}

			this.memberProductEligibilities = memberProductEligibilities;

			return valid;
		},
		validateBirthDateKeyDown : function(event) {
			var keyCode = ('which' in event) ? event.which : event.keyCode;

			isNumeric = (keyCode >= 48 /* KeyboardEvent.DOM_VK_0 */&& keyCode <= 57 /* KeyboardEvent.DOM_VK_9 */)
					|| (keyCode >= 96 /* KeyboardEvent.DOM_VK_NUMPAD0 */&& keyCode <= 105 /* KeyboardEvent.DOM_VK_NUMPAD9 */);
			isForwardSlash = keyCode == 191 || keyCode == 111;
			isBackspace = keyCode == 8;
			isTab = keyCode == 9;
			isTraversalKey = keyCode >= 33 && keyCode <= 40;
			isEditKey = keyCode == 45 || keyCode == 46;
			modifiers = (event.altKey || event.ctrlKey || event.shiftKey);
			shiftTab = isTab && event.shiftKey;

			var isValidKey = (isNumeric || isTraversalKey || isEditKey || isBackspace || isTab || isForwardSlash) && (!modifiers || shiftTab);

			return isValidKey;
		},
		formatDate : function(d, // Date instance
		f // Format string
		) {
			return f.replace( // Replace all tokens
			/{(.+?)(?::(.*?))?}/g, // {<part>:<padding>}
			function(v, // Matched string (ignored, used as local var)
			c, // Date component name
			p // Padding amount
			) {
				for (v = d["get" + c]() // Execute date component getter
						+ /h/.test(c) // Increment Mont(h) components by 1
						+ ""; // Cast to String
				v.length < p; // While padding needed,
				v = 0 + v)
					; // pad with zeros
				return v // Return padded result
			})
		},
		isCoverageSelected : function() {
			var coverageSelected = false;

			var members = this.account.get("members");
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			if (members) {
				for ( var rm = 0; rm < members.length && !coverageSelected; rm++) {
					coverageSelected = members[rm].products && members[rm].products.length > 0;
				}
			}

			return coverageSelected;
		},
		getTempDemographics : function() {
			return this.account.get("tempDemographics");
		},
		getOrCreateTempDemographics : function() {
			var tempDemographics = this.getTempDemographics();

			if (!tempDemographics) {
				tempDemographics = [];
				this.account.set({
					"tempDemographics" : tempDemographics
				}, {
					silent : true
				});

				var members = this.account.get("members");

				for ( var m = 0; m < members.length; m++) {
					tempDemographics[m] = _.deepClone(members[m]);
				}
			}

			return tempDemographics;
		},
		getNextMemberRefName : function(members) {
			var nextMemberRefName = members.length;

			for ( var m = 0; m < members.length; m++) {
				var currentMemberIndex = null;
				if (typeof members[m].memberRefName == "string") {
					currentMemberIndex = parseInt(members[m].memberRefName, 10);
				} else {
				    // TODO Remove this else at some point, as we shouldn't have integer values
					currentMemberIndex = members[m].memberRefName;
				    members[m].memberRefName = currentMemberIndex.toString()					
				}
				if (currentMemberIndex >= nextMemberRefName) {
					nextMemberRefName = currentMemberIndex + 1;
				}
			}

			return nextMemberRefName.toString();
		},
		addIndividual : function(event) {
			var self = this;

			var members = this.getOrCreateTempDemographics();

			// Will have a value if "New" is added
			var memberIndex = members.length;

			var newMemberRefName = this.getNextMemberRefName(members);

			// Add a new member
			var member = {
				"memberRefName" : newMemberRefName,
				"isSmoker" : "No",
				"memberRelationship" : "",
				"gender" : "",
				"isNew" : true
			};

			members[memberIndex] = member;

			// TODO ZipCode should be an input field
			// JWG
			// 12DEC12

			// For now, copy the zipCode of the first member, if set
			if (memberIndex > 0 && members[0].zipCode) {
				member.zipCode = members[0].zipCode;
			}

			var productLines = this.productLines.toJSON();

			member.products = [];

			for ( var p = 0; p < productLines.length; p++) {
				var productLineCode = productLines[p].productLineCode;

				member.products[p] = {
					"productLineCode" : productLineCode
				};
			}

			member.coverageSelected = true;

			// Create data for the current row, "New" or pre-existing
			var memberRowData = jQuery.extend({}, member, {
				"productLines" : this.productLines.toJSON(),
				"memberProductEligibilities" : this.memberProductEligibilities,
				"account" : this.account.toJSON(),
				"relationships" : this.relationships.toJSON(),
				"genders" : this.genders.toJSON()
			});

			// Add the row
			var newRowHTML = this.renderTemplate({
				"template" : "memberRowTemplate",
				"data" : memberRowData,
				"functions" : {
					relationshipOptions : function(chunk, context, bodies, params) {
						self.relationshipOptions(chunk, context, bodies, params);
					},
					genderOptions : function(chunk, context, bodies, params) {
						self.genderOptions(chunk, context, bodies, params);
					},
					renderGender : function(chunk, context, bodies, params) {
						self.renderGender(chunk, context, bodies, params);
					},
					productSelection : function(chunk, context, bodies, params) {
						self.productSelection(chunk, context, bodies, params);
					}
				}
			});
			$('.dataGroup > .coverage-table tbody').append(newRowHTML);

			var primaryMember = this.getPrimaryMember();
			var productLines = this.productLines.toJSON();
			for ( var p = 0; p < productLines.length; p++) {
				var productLineCode = productLines[p].productLineCode;

				// ******** Primary is required for any coverages for EE shopping ********
				// Now ensure that the Primary is selected for the product if any dependents are selected
				var primaryHasProduct = false;
				for ( var pr = 0; primaryMember.products && pr < primaryMember.products.length && !primaryHasProduct; pr++) {
					primaryHasProduct = primaryMember.products[pr].productLineCode == productLineCode;
				}

				if (!primaryHasProduct) {
					primaryMember.products.push({
						"productLineCode" : productLineCode
					});

					// Alter the selected product for all member
					$('.dataGroup > .coverage-table tbody > ._row' + primaryMember.memberRefName + " > td > ._productLine").each(function() {
						if ($(this).attr('productlinecode') == productLineCode) {
							$(this).prop('checked', true);
						}
					});
				}

				// ******** Primary is required for any coverages for EE shopping ********
				// Now determine if primary should be disabled based on member selections for the product
				var determineIfPrimaryShouldBeDisabledPerProduct = this.determineIfPrimaryShouldBeDisabledPerProduct();
				var disablePrimary = false;
				for ( var pr = 0; pr < determineIfPrimaryShouldBeDisabledPerProduct.length && !disablePrimary; pr++) {
					disablePrimary = determineIfPrimaryShouldBeDisabledPerProduct[pr] == productLineCode;
				}

				// Alter the enabled state of product for the primary
				$('.dataGroup > .coverage-table tbody > ._row' + primaryMember.memberRefName + " > td > ._productLine").each(function() {
					if ($(this).attr('productlinecode') == productLineCode) {
						// re-enable Primary if no dependents are selected
						$(this).prop('disabled', disablePrimary);
					}
				});
			}

			var productLines = this.productLines.toJSON();
			for ( var p = 0; p < productLines.length; p++) {
				this.updateProductSelectionsForIndividualChange(productLines[p].productLineCode, true);
			}

			// Update Continue button state
			this.updateContinueButton();

			$(".dataGroup .coverage-table ._birthdate", self.el).bind({
				keydown : function(event) {
					return self.validateBirthDateKeyDown(event);
				}
			});
			
			this.applyDateMasking();

			$('a[title]', self.el).tooltip();
		},
		applyDateMasking : function() {
			// add date masking
			$('._birthdate', this.el).mask("?99/99/9999", {
				placeholder : " "
			});
		},
		updateProductSelectionsForIndividualChange : function(productLineCode, isRemoveIndividual) {
			var members = this.getOrCreateTempDemographics();

			var productLineSelectionCount = 0;
			// Count all members with the selected productLineCode
			for ( var m = 0; m < members.length; m++) {
				var memberProducts = members[m].products;

				for ( var pr = 0; memberProducts && pr < memberProducts.length; pr++) {
					if (memberProducts[pr].productLineCode == productLineCode) {
						productLineSelectionCount++;
					}
				}
			}

			var productLineIndex = null;
			var account = this.account;
			var products = account.get("products");
			if (products) {
				for ( var p = 0; p < products.length && null == productLineIndex; p++) {
					if (productLineCode == products[p].productLineCode) {
						productLineIndex = p;
					}
				}
			} else {
				products = [];
				account.set("products", products);
			}

			// If the productLine was previously selected for all members
			if (null != productLineIndex) {
				// We'll never clear the state when an individual is removed
				if (!isRemoveIndividual) {
					// deselect it
					products.splice(productLineIndex, 1);
				}
			}
			// Else if all members have selected the product now
			// This includes the scenario where the only member without the product
			// line selected is removed
			else if (productLineSelectionCount == members.length) {
				// Select it
				products[products.length] = {
					"productLineCode" : productLineCode
				};
			}

			// Update the state of the product line selection for all Members checkbox
			this.updateProductLineSelectionForMembers(productLineCode, productLineSelectionCount, members.length);
		},
		removeIndividual : function(event) {
			var self = this;

			var $selectedRow = $(event.currentTarget);

			var memberRefName = $('.remove-link', $selectedRow).attr("data-id");

			var members = this.getOrCreateTempDemographics();

			// Find the "New" member being removed
			var newMemberIndex = null;
			for ( var i = 0; i < members.length && newMemberIndex == null; i++) {
				if (members[i].memberRefName == memberRefName) {
					newMemberIndex = i;
				}
			}

			if (null != newMemberIndex) {
				// Remove the "New" member
				members.splice(newMemberIndex, 1);

				// Clear out any question answers for the removed member
				var account = this.account;
				var questions = account.get("questions");
				for ( var q = 0; questions && q < questions.length; q++) {
					var questionMembers = questions[q].members;
					if (questionMembers) {
						var questionMember = null;
						for ( var qm = 0; qm < questionMembers.length && null == questionMember; qm++) {
							if (questionMembers[qm].memberRefName == memberRefName) {
								questionMember = questionMembers[qm];
								questionMembers.splice(qm, 1);
							}
						}
					}
				}
			}

			// Remove the row
			// TODO: Do not remove the row, rerender
			$('.dataGroup > .coverage-table tbody > ._row' + memberRefName).remove();

			var primaryMember = this.getPrimaryMember();
			var productLines = this.productLines.toJSON();
			for ( var p = 0; p < productLines.length; p++) {
				var productLineCode = productLines[p].productLineCode;

				// ******** Primary is required for any coverages for EE shopping ********
				// Now determine if primary should be disabled based on member selections for the product
				var determineIfPrimaryShouldBeDisabledPerProduct = this.determineIfPrimaryShouldBeDisabledPerProduct();
				var disablePrimary = false;
				for ( var pr = 0; pr < determineIfPrimaryShouldBeDisabledPerProduct.length && !disablePrimary; pr++) {
					disablePrimary = determineIfPrimaryShouldBeDisabledPerProduct[pr] == productLineCode;
				}

				// Alter the enabled state of product for the primary
				$('.dataGroup > .coverage-table tbody > ._row' + primaryMember.memberRefName + " > td > ._productLine").each(function() {
					if ($(this).attr('productlinecode') == productLineCode) {
						// re-enable Primary if no dependents are selected
						$(this).prop('disabled', disablePrimary);
					}
				});
			}

			for ( var p = 0; p < productLines.length; p++) {
				this.updateProductSelectionsForIndividualChange(productLines[p].productLineCode, true);
			}

			var validationErrors = this.validationErrors;
			if (validationErrors) {
				delete validationErrors[memberRefName];
				this.updateValidationErrors();
			}

			// Update Continue button state
			this.updateContinueButton();
		},
		productLineSelected : function(event) {
			var self = this;

			var $selectedRow = $(event.currentTarget);

			var productLineCode = $selectedRow.attr("productlinecode");
			var memberRefName = $selectedRow.attr("member-id");

			var account = this.account;
			var members = this.getOrCreateTempDemographics();

			var matchingMember = null;
			// Find the matching member
			for ( var rm = 0; rm < members.length && null == matchingMember; rm++) {
				var member = members[rm];
				if (member.memberRefName == memberRefName) {
					matchingMember = member;
				}
			}

			if (!matchingMember.products) {
				matchingMember.products = [];
			}

			var matchingProductLineIndex = null;
			// Find the matching member
			for ( var pr = 0; pr < matchingMember.products.length; pr++) {
				if (matchingMember.products[pr].productLineCode == productLineCode) {
					matchingProductLineIndex = pr;
				}
			}

			var productLineSelectionCount = 0;
			// Count all members with the selected productLineCode
			for ( var m = 0; m < members.length; m++) {
				var member = members[m];

				if (member.products) {
					for ( var pr = 0; pr < member.products.length; pr++) {
						if (member.products[pr].productLineCode == productLineCode) {
							productLineSelectionCount++;
						}
					}
				}
			}

			var primaryMember = this.getPrimaryMember();

			// If it wasn't found
			if (null == matchingProductLineIndex) {
				matchingMember.products[matchingMember.products.length] = {
					"productLineCode" : productLineCode
				};

				matchingMember.coverageSelected = true;

				// ******** Primary is required for any coverages for EE shopping ********
				// Now ensure that the Primary is selected for the product if any dependents are selected
				var primaryHasProduct = false;
				for ( var pr = 0; primaryMember.products && pr < primaryMember.products.length && !primaryHasProduct; pr++) {
					primaryHasProduct = primaryMember.products[pr].productLineCode == productLineCode;
				}

				if (!primaryHasProduct) {
					primaryMember.products.push({
						"productLineCode" : productLineCode
					});

					// Alter the selected product for all member
					$('.dataGroup > .coverage-table tbody > ._row' + primaryMember.memberRefName + " > td > ._productLine").each(function() {
						if ($(this).attr('productlinecode') == productLineCode) {
							$(this).prop('checked', true);
						}
					});

					productLineSelectionCount++;
				}

				productLineSelectionCount++;
			} else {
				// Found it, so remove it; it was selected before, it is being unselected
				matchingMember.products.splice(matchingProductLineIndex, 1);

				if (matchingMember.products.length == 0) {
					matchingMember.coverageSelected = false;
				}

				productLineSelectionCount--;
			}

			// ******** Primary is required for any coverages for EE shopping ********
			// Now determine if primary should be disabled based on member selections for the product
			var determineIfPrimaryShouldBeDisabledPerProduct = this.determineIfPrimaryShouldBeDisabledPerProduct();
			var disablePrimary = false;
			for ( var p = 0; p < determineIfPrimaryShouldBeDisabledPerProduct.length && !disablePrimary; p++) {
				disablePrimary = determineIfPrimaryShouldBeDisabledPerProduct[p] == productLineCode;
			}

			// Alter the enabled state of product for the primary
			$('.dataGroup > .coverage-table tbody > ._row' + primaryMember.memberRefName + " > td > ._productLine").each(function() {
				if ($(this).attr('productlinecode') == productLineCode) {
					// re-enable Primary if no dependents are selected
					$(this).prop('disabled', disablePrimary);
				}
			});

			var productLineIndex = null;
			var products = account.get("products");
			if (products) {
				for ( var p = 0; p < products.length && null == productLineIndex; p++) {
					if (productLineCode == products[p].productLineCode) {
						productLineIndex = p;
					}
				}
			} else {
				products = [];
				account.set("products", products);
			}

			// If the productLine was previously selected for all members
			if (null != productLineIndex) {
				// deselect it
				products.splice(productLineIndex, 1);
			}
			// Else if all members have selected the product now
			else if (productLineSelectionCount == members.length) {
				// Select it
				products.push({
					"productLineCode" : productLineCode
				});
			}

			// Update the state of the product line selection for all Members checkbox
			this.updateProductLineSelectionForMembers(productLineCode, productLineSelectionCount, members.length);

			// Update Continue button state
			this.updateContinueButton();
		},
		productLineSelectedForMembers : function(event) {
			var self = this;

			var $selectedRow = $(event.currentTarget);

			var productLineCode = $selectedRow.attr("productlinecode");

			var account = this.account;
			var members = this.getOrCreateTempDemographics();

			if (!account.get("products")) {
				account.set("products", []);
			}
			var products = account.get("products");

			var matchingProductIndex = null;
			for ( var ap = 0; ap < products.length && null == matchingProductIndex; ap++) {
				if (productLineCode == products[ap].productLineCode) {
					matchingProductIndex = ap;
				}
			}

			// If it was selected, deselect it in the model
			if (null != matchingProductIndex) {
				products.splice(matchingProductIndex, 1);
			} else {
				// select it in the model
				products.push({
					"productLineCode" : productLineCode
				});
			}

			// Apply the selected state to all members
			for ( var rm = 0; rm < members.length; rm++) {
				var member = members[rm];
				if (!member.products) {
					member.products = [];
				}

				var matchingMemberProductIndex = null;
				for ( var mp = 0; mp < member.products.length && null == matchingMemberProductIndex; mp++) {
					if (productLineCode == member.products[mp].productLineCode) {
						matchingMemberProductIndex = mp;
					}
				}

				// Alter the selected product for all member
				$('.dataGroup > .coverage-table tbody > ._row' + member.memberRefName + " > td > ._productLine").each(function() {
					if ($(this).attr('productlinecode') == productLineCode) {
						$(this).prop('checked', null == matchingProductIndex);
					}
				});

				if (null != matchingProductIndex) {
					if (null != matchingMemberProductIndex) {
						member.products.splice(matchingMemberProductIndex, 1);
					}
				} else {
					if (null == matchingMemberProductIndex) {
						member.products.push({
							"productLineCode" : productLineCode
						});
					}
				}

				member.coverageSelected = null == matchingProductIndex;
			}

			var primaryMember = this.getPrimaryMember();

			// ******** Primary is required for any coverages for EE shopping ********
			// Now determine if primary should be disabled based on member selections for the product
			var determineIfPrimaryShouldBeDisabledPerProduct = this.determineIfPrimaryShouldBeDisabledPerProduct();
			var disablePrimary = false;
			for ( var p = 0; p < determineIfPrimaryShouldBeDisabledPerProduct.length && !disablePrimary; p++) {
				disablePrimary = determineIfPrimaryShouldBeDisabledPerProduct[p] == productLineCode;
			}

			// Alter the enabled state of product for the primary
			$('.dataGroup > .coverage-table tbody > ._row' + primaryMember.memberRefName + " > td > ._productLine").each(function() {
				if ($(this).attr('productlinecode') == productLineCode) {
					// re-enable Primary if no dependents are selected
					$(this).prop('disabled', disablePrimary);
				}
			});

			// Update Continue button state
			this.updateContinueButton();
		},
		updateContinueButton : function() {
			var self = this;

			if (!this.isCoverageSelected()) {
				$('.btn_primary', self.el).addClass("disabled-button");
				$('.btn_primary', self.el).attr("title", "select coverage(s) to continue");
			} else {
				$('.btn_primary', self.el).removeClass("disabled-button");
				$('.btn_primary', self.el).attr("title", "");
			}
		},
		updateProductLineSelectionForMembers : function(productLineCode, productLineSelectionCount, membersCount) {
			// Alter the selected product for all member
			$('.dataGroup > .coverage-table thead > tr > th > ._productLineHeader').each(function() {
				if ($(this).attr('productlinecode') == productLineCode) {
					$(this).prop('checked', productLineSelectionCount == membersCount);
				}
			});
		},
		newMemberValueChanged : function(event) {
			var $selectedElement = $(event.currentTarget);

			var memberRefName = $selectedElement.attr("data-id");

			var members = this.getOrCreateTempDemographics();

			// Find the "New" member that changed
			var newMemberIndex = null;
			for ( var i = 0; i < members.length && newMemberIndex == null; i++) {
				if (members[i].memberRefName == memberRefName) {
					newMemberIndex = i;
				}
			}

			var newMember = members[newMemberIndex];

			var fieldId = $selectedElement.attr("field-id");

			if ("firstName" == fieldId) {
				newMember.name = {
					"first" : $selectedElement.children("input").val()
				};
				if (this.validateDemographicsField(newMember, "firstName")) {
					newMember.isDirty = true;
					this.notifyDemographicsChange(newMember, "firstName");
				}
			} else if ("birthDate" == fieldId) {
				var birthDate = $selectedElement.children("input").val();
				newMember.birthDate = birthDate;
				if (this.validateDemographicsField(newMember, "birthDate")) {
					var birthInDateForm = new Date(birthDate);
					// Reformat the date to match what we want
					newMember.birthDate = $.datepicker.formatDate("mm/dd/yy", birthInDateForm);
					// Now update the form field to match the reformated date
					$('.dataGroup .coverage-table ._whosCoveredField', this.el).each(function() {
						var dataId = $(this).attr("data-id");
						var fieldId = $(this).attr("field-id");
						if (newMember.memberRefName == dataId && "birthDate" == fieldId) {
							$(this).children()[0].value = newMember.birthDate;
						}
					});
					// We also need the date in millisecond form
					newMember.dateOfBirth = birthInDateForm.getTime();
					newMember.isDirty = true;
					this.notifyDemographicsChange(newMember, "birthDate");
				}
			} else if ("smoker" == fieldId) {
				newMember.isSmoker = $selectedElement.children('select').children('option:selected').val();
				if (this.validateDemographicsField(newMember, "smoker")) {
					newMember.isDirty = true;
					this.notifyDemographicsChange(newMember, "smoker");
				}
			} else if ("gender" == fieldId) {
				newMember.gender = $selectedElement.children('select').children('option:selected').attr('gender');
				if (this.validateDemographicsField(newMember, "gender")) {
					newMember.isDirty = true;
					this.notifyDemographicsChange(newMember, "gender");
				}
			} else if ("relationship" == fieldId) {
				newMember.memberRelationship = $selectedElement.children('select').children('option:selected').attr('relationshipType');
				if (this.validateDemographicsField(newMember, "relationship")) {
					newMember.isDirty = true;
					this.notifyDemographicsChange(newMember, "relationshipType");
				}
			}
		},
		notifyDemographicsChange : function(member, attribute) {
			this.eventManager.trigger('component:demographicsUpdated', member, attribute);
		},
		FieldValidator : {
			validateDate : function(field, value, errors, memberValidationErrors) {
				var valid = false;
				if (!value) {
					memberValidationErrors[field] = {
						text : errors.notSpecified
					};
				} else {
					var re = /([0-9]{1,2}\/){2}[0-9]{4}/;
					var matches = value.match(re);
					var valid = null != matches && matches[0] == value;
					if (!valid) {
						memberValidationErrors[field] = {
							text : errors.invalid
						};
					} else {
						// Check for a real date
						if (isNaN(Date.parse(value))) {
							memberValidationErrors[field] = {
								text : errors.invalid
							};
						} else {
							delete memberValidationErrors[field];
							valid = true;
						}
					}
				}
				return valid;
			},
			validateString : function(field, value, errors, memberValidationErrors) {
				var valid = false;
				if (!value || value == "" || (typeof value === "object" && Object.prototype.toString.call(value) == '[object Array]')) {
					memberValidationErrors[field] = {
						text : errors.notSpecified
					};
				} else {
					delete memberValidationErrors[field];
					valid = true;
				}
				return valid;
			},
			validateName : function(field, value, errors, memberValidationErrors) {
				var valid = this.validateString(field, value, errors, memberValidationErrors);
				if (valid) {
					var re = /[a-zA-Z\-\']+/;
					var matches = value.match(re);
					valid &= null != matches && matches[0] == value;
					if (!valid) {
						memberValidationErrors[field] = {
							text : errors.invalid
						};
					}
				}
				return valid;
			},
			validateAge : function(field, value, errors, memberValidationErrors) {
				var valid = this.validateDate(field, value, errors, memberValidationErrors);
				if (valid) {
					// calculate the Age as of today
					var age = this.calculateAgeAsOfDate(new Date(value), new Date());
					valid = age >= 0 && age <= 120;
				
					if (!valid) {
						memberValidationErrors[field] = {
							text : errors.outsideAgeRange
						};
					}
				}
			},
			calculateAgeAsOfDate : function(birthDate, asOfDate) {
				var memberBirthYear = birthDate.getFullYear();

				var asOfYear = asOfDate.getFullYear();

				// default for memberBirthMonth >= currentMonth
				var age = asOfYear - memberBirthYear;

				var asOfMonth = asOfDate.getMonth();
				var memberBirthMonth = birthDate.getMonth();

				if (asOfMonth < memberBirthMonth) {
					// one less year if we are before the birth month in the same year
					age--;
				} else if (asOfMonth == memberBirthMonth) {
					var asOfDay = asOfDate.getDate();
					var memberBirthDay = birthDate.getDate();

					if (asOfDay < memberBirthDay) {
						// One less if we are before the birth day in the same month in the same year
						age--;
					}
				}

				return age;
			}
		},
		validateDemographicsForMember : function(member, field, memberValidationErrors) {
			if (!memberValidationErrors) {
				memberValidationErrors = {};
			}
			if (!field || field == "firstName") {
				this.FieldValidator.validateName("firstName", member.name ? member.name.first : null, {
					notSpecified : "First Name must be specified",
					invalid : "The First Name entry is invalid, please use only a-z, A-Z, hyphen -, and apostrophe '"
				}, memberValidationErrors);
			}
			if (!field || field == "relationship") {
				this.FieldValidator.validateString("relationship", member.memberRelationship, {
					notSpecified : "Relationship must be specified"
				}, memberValidationErrors);
			}
			if (!field || field == "birthDate") {
				var currentDate = new Date();
				var earliestDate = new Date();
				earliestDate.setFullYear(currentDate.getFullYear() - 121);
				earliestDate.setDate(earliestDate.getDate() + 1);
				this.FieldValidator.validateAge("birthDate", member.birthDate, {
					notSpecified : "Birthdate must be specified",
					invalid : "Birthdate must be a valid date",
					outsideAgeRange : "Birthdate must not be before " + $.datepicker.formatDate("mm/dd/yy", earliestDate) + " or after today"
				}, memberValidationErrors);
			}
			if (!field || field == "gender") {
				this.FieldValidator.validateString("gender", member.gender, {
					notSpecified : "Gender must be specified"
				}, memberValidationErrors);
			}
			if (!field || field == "smoker") {
				this.FieldValidator.validateString("smoker", member.isSmoker, {
					notSpecified : "Tobacco must be specified"
				}, memberValidationErrors);
			}
			return memberValidationErrors;
		},
		validateDemographicsField : function(member, field) {
			// Validate the member and field
			var fieldValidationErrors = this.validateDemographicsForMember(member, field, {});

			var validationErrors = this.validationErrors;
			if (!validationErrors) {
				validationErrors = {};
			}
			var memberValidationErrors = validationErrors[member.memberRefName];

			var valid = true;
			if (fieldValidationErrors[field]) {
				valid = false;

				// Add the validation error
				if (!memberValidationErrors) {
					// if there aren't any for the member
					memberValidationErrors = fieldValidationErrors;
				} else {
					// if it isn't already in the members list of validation errors
					if (memberValidationErrors && !memberValidationErrors[field]) {
						memberValidationErrors[field] = fieldValidationErrors[field];
					}
				}
			} else {
				// Remove the validation error from the mail list if it existed
				if (memberValidationErrors) {
					delete memberValidationErrors[field];
				}
			}

			if (memberValidationErrors) {
				validationErrors[member.memberRefName] = memberValidationErrors;
			} else {
				delete validationErrors[member.memberRefName];
			}

			this.validationErrors = validationErrors;

			this.updateValidationErrors();

			return valid;
		},
		validateDemographics : function() {
			var members = this.account.get("members");
			if (this.getTempDemographics()) {
				members = this.getTempDemographics();
			}

			var validationErrors = this.validationErrors;
			if (!validationErrors) {
				validationErrors = {};
			}

			// Remove any validationErrors that are no longer applicable (member is gone)
			for ( var memberRefName in _.clone(validationErrors)) {
				var memberExists = false;
				for ( var m = 0; m < members.length && !memberExists; m++) {
					memberExists = members[m].memberRefName == memberRefName;
				}
				if (!memberExists) {
					delete validationErrors[memberRefName];
				}
			}

			var valid = true;

			// Validate all (new) members and fields
			for ( var rm = 0; rm < members.length; rm++) {
				//Made changes SUP :1060
				//if (members[rm].isNew) {
					var memberValidationErrors = this.validateDemographicsForMember(members[rm], null, validationErrors[members[rm].memberRefName]);
					for ( var error in memberValidationErrors) {
						valid = false;
					}
					validationErrors[members[rm].memberRefName] = memberValidationErrors;
				//}//Change End
			}

			if (!valid) {
				this.validationErrors = validationErrors;
			}

			this.updateValidationErrors();

			return valid;
		},
		updateValidationErrors : function() {
			var self = this;

			var validationErrors = this.validationErrors;
			if (!validationErrors) {
				validationErrors = {};
			}
			var valid = true;
			for ( var member in validationErrors) {
				for ( var error in validationErrors[member]) {
					valid = false;
				}
			}

			var html = "";
			if (!valid) {
				html = "<div class='error-instructions'>Please fix the following validation errors:</div><ul>";

				var validationSet = {};

				// Build up a unique set of validationErrors for display
				for ( var memberRefName in validationErrors) {
					var rowHtml = "";

					for ( var error in validationErrors[memberRefName]) {
						if (!validationSet[error]) {
							validationSet[error] = validationErrors[memberRefName][error].text;
						}
					}
				}

				for ( var error in validationSet) {
					html += "<li>" + validationSet[error] + "</li>";
				}
				html += "</ul>";
			}

			if (html == "") {
				$("._validationErrors", self.el).hide();
			} else {
				$("._validationErrors", self.el).html(html);
				$("._validationErrors", self.el).show();
			}

			// Now add/remove hasErrors class as appropriate
			$('.dataGroup > .coverage-table tbody > tr > td', self.el).each(function() {
				var memberRefName = $(this).attr('data-id');
				var memberExtRefId = $(this).attr('extRefId');
				var fieldId = $(this).attr('field-id');
				// Only operate on fields corresponding to new members
				if (memberRefName && !memberExtRefId) {
					var fieldInvalid = false;
					var memberValidationErrors = validationErrors[memberRefName];
					var classes = $(this).attr('class');
					for ( var error in memberValidationErrors) {
						// For the current field
						if (error == fieldId) {
							fieldInvalid = true;
						}
					}
					if (fieldInvalid) {
						$(this).addClass("hasErrors");
					} else {
						$(this).removeClass("hasErrors");
					}
				}
			});
		},
		applyChanges : function() {
			var tempMembers = this.getTempDemographics();
			if (tempMembers) {
				this.account.unset("tempDemographics");

				// Clear the dirty flags
				for ( var member in tempMembers) {
					tempMembers[member].isDirty = false;
				}

				this.account.set("members", tempMembers);
			}
			return tempMembers;
		},
		/**
		 * This method gets fired from the whoscovered view continue button
		 * 
		 * @param event
		 */
		continueToMethodsIfValid : function(event) {
			var self = this;

			var $button = $(event.currentTarget);
			if (!$button.hasClass('_disabled')) {
				$button.addClass('_disabled');
				$button.text('Saving...');

				// Only proceed if whosCovered reports that demographics are valid AND
				// if at least some coverage is selected.
				if (this.validateDemographics() && this.isCoverageSelected()) {
					if (this.applyChanges()) {
						this.account.save({}, {
							success : function() {
								self.validateEligibilityAndContinue($button);
							},
							failure : function() {
								// alert('unable to validate demographics!');
								$button.removeClass('_disabled');
								$button.text('Continue');
							}
						});
					} else {
						this.validateEligibilityAndContinue($button);
					}
				} else {
					$button.removeClass('_disabled');
					$button.text('Continue');
				}
			}
		},
		validateEligibilityAndContinue : function(button) {
			var self = this;

			this.eligibility.fetch({
				success : function() {

					// Update the whos covered page based on validity
					// 
					var valid = self.updateMemberEligibility();

					if (valid) {
						self.options.eventManager.trigger('view:whoscovered:finished');
					} else {
						// then render anyway?, which is going to call update member eligibility....this whole
						// coupling of updating member eligibilty and rendering needs to get fixed TODO
						self.render();
					}
				},
				failure : function() {
					// alert('unable to validate demographics!');
					button.removeClass('_disabled');
					button.text('Continue');
				}
			});
		}
	});

	return WhosCoveredView;
});