/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/views/base', 'common/component/component', 
		'text!html/shopping/components/header/view.help-faq.dust' ], function(require, Backbone, templateEngine, dustHelpers, BaseView, connecture, helpFAQTemplate) {

	templateEngine.load(helpFAQTemplate, "view.help-faq");

	var HelpFAQView = connecture.view.Base.extend({
		template : 'view.help-faq',
		events : {
			'click .categoryName' : 'toggleCategory',
			'click ._toggleIndvQuestion' : 'toggleQuestion',
			'click ._navigatorFaqSearch' : 'searchFAQ',
			'click ._backToAllqLink' : 'showAllFAQ'
		},
		initialize : function() {
		},

		showAllFAQ : function() {
			var self = this;
			$('._shoppingDialogFaqs', self.el).show();
			$('._searchResults', self.el).hide();
		},
		toggleCategory : function(event) {
			$(event.currentTarget).next('.qaCategory').toggle();
		},
		toggleQuestion : function(event) {
			$(event.currentTarget).next('dd').toggle();
		},
		searchFAQ : function(event) {

			var self = this;

			var $searchBox = $('input._searchBox', self.el);

			var faqData = $('._shoppingDialogFaq', self.el).clone();

			if ($searchBox.val().length > 1) {
				$('div._searchError', self.el).css('display', 'none');
				self.searchText(faqData, $searchBox.val())
			} else {
				$('div._searchError', self.el).css('display', 'block');
			}
		},
		searchText : function(faqData, val) {

			var self = this;
			// clear previous search
			$('._resultList', self.el).html('');

			var faqHTML = self.traverseFAQsForSearch(faqData, val);
			$('._resultList', self.el).html(faqHTML);
			$('._searchResults', self.el).css('display', 'block');
			$('._shoppingDialogFaqs', self.el).hide();
		},
		traverseFAQsForSearch : function(jsonObj, val) {

			var valueSpaces = val.replace(/\s{2,}/g, ' ');

			var valueArray = valueSpaces.trim().split(' ');

			var self = this;

			var catfaqHTML = '';

			// look at all qaCategories
			var categories = $('.categoryName', self.el);
			$.each(categories, function(index, categoryNameDiv) {

				var $qaCategory = $(categoryNameDiv).next('.qaCategory');
				var $categoryName = $(categoryNameDiv);

				var qaElementHTML = '';

				// look for all question/answer groups
				var categoryQuestions = $('dt._toggleIndvQuestion', $qaCategory.html());
				$.each(categoryQuestions, function(index, question) {

					var $question = $(question);

					// get question and answer as a pair
					var $answer = $(question).next('dd');

					// if question OR answer contains text, copy both
					var qaText = $question.html() + " " + $answer.html();

					if (qaText) {
						for ( var k = 0; k < valueArray.length; k++) {
							var val = valueArray[k];
							if (qaText.search(new RegExp(val, "i")) != -1) {
								qaElementHTML += $('<div></div>').append($question.clone()).html();
								qaElementHTML += $('<div></div>').append($answer.clone()).html();

								break;
							}
						}
					}
				});

				if (qaElementHTML && qaElementHTML != '') {
					var categoryClone = $($qaCategory).clone();

					$('dl', categoryClone).html(qaElementHTML);

					catfaqHTML += $('<div></div>').append($categoryName.clone()).html();
					catfaqHTML += $('<div></div>').append(categoryClone).html();
				}
			});

			if (catfaqHTML == '') {
				catfaqHTML += '<div> No results found for "' + val + '" </div>';
			}
			return catfaqHTML;
		},

		render : function() {
			var self = this;
			$(self.el).html(this.renderTemplate({}));
			return this;
		}
	});

	return HelpFAQView;
});