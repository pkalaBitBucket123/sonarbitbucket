/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */
define([ 'require', 'backbone', 'common/widget' ], function(require, Backbone, Widget) {

	var QuestionWidget = function(options) {
		// handle the question responses here
		// Get the response(s), if any
		var responseValue = null;
		var questionResponses = options.attributes.account.get("questions");
		if (questionResponses) {
			for ( var qr = 0; qr < questionResponses.length && responseValue == null; qr++) {
				if (questionResponses[qr].questionRefId == options.widgetModel.get('widgetId')) {
					responseValue = questionResponses[qr].value;
				}
			}
		}

		options.attributes['responseValue'] = responseValue;

		Widget.call(this, options);
	};

	QuestionWidget.prototype = Object.create(Widget.prototype);

	QuestionWidget.prototype.addEvents = function(events, view) {
		// The following is mostly borrowed from backbone.js:View::delegateEvents(events)
		var delegateEventSplitter = /^(\S+)\s*(.*)$/;

		for ( var key in events) {
			var method = events[key];
			if (!_.isFunction(method))
				method = this[events[key]];
			if (!method)
				throw new Error('Method "' + events[key] + '" does not exist');
			var match = key.match(delegateEventSplitter);
			var eventName = match[1], selector = match[2];
			method = _.bind(method, this);
			eventName += '.delegateEvents' + view.cid;
			if (selector === '') {
				view.$el.bind(eventName, method);
			} else {
				view.$el.delegate(selector, eventName, method);
			}
		}
	};

	QuestionWidget.prototype.getSummaryTextOfValue = function(questionRefId, answerId, answers) {
		var summaryText = null;
		for ( var a = 0; a < answers.length && summaryText == null; a++) {
			if (answerId == answers[a].answerId) {
				if (answers[a].summaryText) {
					summaryText = answers[a].summaryText;
				} else {
					summaryText = answers[a].text;
				}
			}
		}

		return summaryText;
	};
	QuestionWidget.prototype.getQuestionResponse = function() {
		var questions = this.attributes.account.get("questions");
		var questionRefId = this.model.get("config").questionRefId;

		var questionResponse = null;

		// Ensure there are actually responses before trying to find the response
		if (questions) {
			for ( var qr = 0; qr < questions.length && null == questionResponse; qr++) {
				if (questions[qr].questionRefId == questionRefId) {
					questionResponse = questions[qr];
				}
			}
		}

		return questionResponse;
	};
	QuestionWidget.prototype.getOrCreateQuestionResponse = function() {
		var questions = this.attributes.account.get("questions");
		var questionRefId = this.model.get("config").questionRefId;

		var questionResponse = this.getQuestionResponse();

		if (null == questionResponse) {
			questionResponse = {
				"questionRefId" : questionRefId,
			};
			questions.push(questionResponse);
		}

		return questionResponse;
	};
	QuestionWidget.prototype.clearQuestionResponse = function() {
		var questions = this.attributes.account.get("questions");
		var questionRefId = this.model.get("config").questionRefId;

		var question = null;
		for ( var q = 0; q < questions.length && question == null; q++) {
			if (questions[q].questionRefId == questionRefId) {
				question = questions[q];
				questions.splice(q, 1);
			}
		}
	};
	QuestionWidget.prototype.getQuestionResponseMember = function(questionResponse, memberRefName) {
		var member;

		for ( var m = 0; m < questionResponse.members.length && null == member; m++) {
			if (questionResponse.members[m].memberRefName == memberRefName) {
				member = questionResponse.members[m];
			}
		}

		return member;
	};
	QuestionWidget.prototype.getOrCreateQuestionResponseMember = function(questionResponse, memberRefName) {
		var member;

		if (!questionResponse.members) {
			questionResponse.members = [];
		} else {
			member = this.getQuestionResponseMember(questionResponse, memberRefName);
		}

		if (null == member) {
			member = {
				"memberRefName" : memberRefName,
				value : {}
			}
			questionResponse.members[questionResponse.members.length] = member;
		}

		return member;
	};

	QuestionWidget.prototype.getQuestionStorage = function() {
		var questionResponse = this.getQuestionResponse();

		if (!questionResponse['data']) {
			questionResponse['data'] = {};
		}

		return questionResponse['data'];

	};
	QuestionWidget.prototype.clearQuestionStorage = function() {
		var questionResponse = this.getQuestionResponse();
		if (questionResponse['data']) {
			questionResponse['data'] = {};
		}
	};
	QuestionWidget.prototype.persistQuestions = function() {
		// Fire an event off to persist account.questions
		this.eventManager.trigger('view:workflow:widget:save', "questions");
	};
	QuestionWidget.prototype.persistMembers = function() {
		// Fire an event off to persist account.members
		this.eventManager.trigger('view:workflow:widget:save', "members");
	};

	return QuestionWidget;
});