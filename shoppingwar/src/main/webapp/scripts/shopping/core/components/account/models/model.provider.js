/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, backbone) {

	var ProviderModel = Backbone.Model.extend({
		initialize : function() {

		}
	});

	return ProviderModel;
});