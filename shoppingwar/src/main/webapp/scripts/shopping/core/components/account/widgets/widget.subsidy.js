/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'components/account/widgets/views/view.widget.subsidy' ], function(require, Backbone, Widget, SubsidyView) {
	/**
	 * Currently this widget has only one view with two internal states.
	 * 
	 * This needs to get converted to have two views.
	 * 
	 */
	var SubsidyWidget = function(options) {
		Widget.call(this, options);
		// default state to render
		this.state = "subsidy";
	};

	SubsidyWidget.prototype = Object.create(Widget.prototype);

	SubsidyWidget.prototype.initialize = function() {
		this.eventManager.on('view:subsidy:close', function(response) {

			this.eventManager.trigger('widget:done');
		}, this);

		this.eventManager.on('view:subsidy:change:state', function(state) {
			this.state = state;
			this.eventManager.trigger('widget:takeover', this.model.get('widgetId'));
		}, this);
	};

	SubsidyWidget.prototype.start = function() {

	};

	SubsidyWidget.prototype.getView = function() {
		// the region this is being rendered to
		// will handle destroying it, if a new one
		// is rendered in its place

		// TODO: This should be broken out into two
		// views instead of one view with different
		// state
		this.view = new SubsidyView({
			model : this.model,
			applicationConfig : this.attributes.applicationConfig,
			account : this.attributes.account,
			actor : this.attributes.actor,
			eventManager : this.eventManager,
			session : this.attributes.session,
			page : this.state,
			componentId : this.attributes.componentId
		});

		return this.view;
	};

	SubsidyWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	SubsidyWidget.prototype.historyKey = function() {
		return this.model.get('widgetId') + '/' + this.state;
	};

	return SubsidyWidget;
});