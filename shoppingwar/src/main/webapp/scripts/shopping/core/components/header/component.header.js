/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the top level component header. This is the main header that will be customized depending on where shoppnig is included.
 * 
 */
define([ 'module', 'log', 'backbone', 'templateEngine', 'common/component/component', 'components/header/views/view.header', 'components/header/views/view.header.welcome',
		'components/header/views/view.header.links', 'components/header/views/view.header.welcome.internal', 'components/header/views/view.header.links.internal',
		'components/header/views/view.header.producer' ], function(module, log, Backbone, templateEngine, connecture, HeaderView, HeaderWelcomeView, HeaderLinksView, InternalWelcomeView,
		InternalLinksView, HeaderProducerView) {

	var HeaderComponent = function(configuration) {
		connecture.component.core.Base.call(this, configuration);

		this.items = module.config().items;

		this.publish = _.extend({}, this.publish, {
			'view:cart:show' : 'component:showCart',
				//<BCBSNE US=RIDERS FOR PROPOSAL MI>
			'view:help:show' : 'component:showHelp',
			'view:finalizeQuote' : 'component:finalizeQuote',
			//</BCBSNE US=RIDERS FOR PROPOSAL MI>
		});

		// component handles the following events:
		this.subscriptions = _.extend({}, this.subscriptions, {
			'component:showHelp' : function() {
				// not really a fan of this but works for now
				this.regionManager.links.currentView.showHelp();
			}
		});

		this.configuration.layout = _.extend({}, this.configuration.layout, {
			View : HeaderView,
			regions : {
				'welcome' : '#headerWelcome',
				'links' : '#headerLinks',
				'producer' : '#producer',
				'producerDropdown' : '#producer-dropdown'
			}
		});
	};

	HeaderComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	HeaderComponent.prototype.getParams = function(requestParams) {
		var retVal = {};
		var url = $.url.parse();
		if (url.params) {
			_.each(requestParams, function(requestedParam) {
				if (url.params.hasOwnProperty(requestedParam)) {
					retVal[requestedParam] = url.params[requestedParam];
				}
			});
		}

		return retVal;
	};

	// register events : events coming from this component's view(s)
	HeaderComponent.prototype.registerEvents = function() {

		this.eventManager.on("start", function() {
			this._showWelcomeView();
			this._showHeaderLinksView();
			this._showProducerView();
		}, this);

		this.eventManager.on('view:logout', function() {
			this.eventManager.trigger('exit', {
				key : 'logout'
			});
		}, this);

		this.eventManager.on('view:login', function() {
			this.eventManager.trigger('exit', {
				key : 'login'
			});
		}, this);
	};

	HeaderComponent.prototype.load = function(data) {
		connecture.component.core.Base.prototype.load.call(this, data);
	};

	HeaderComponent.prototype._getLinkViewData = function() {
		var linkData = {};
		if (this.models.applicationConfig) {
			linkData.messagesURL = this.models.applicationConfig.get("messages-url");
			linkData.myAccountURL = this.models.applicationConfig.get("my-account-url");

			if (this.models.applicationConfig.get("dashboard-url")) {
				linkData.dashboardURL = this.models.applicationConfig.get("dashboard-url");
			}

			if (this.models.applicationConfig.get("user-profile-url")) {
				linkData.userProfileURL = this.models.applicationConfig.get("user-profile-url");
			}

			// show broker back to roster link, append url params
			if (this.models.applicationConfig.get("roster-url")) {
				linkData.rosterURL = this.models.applicationConfig.get("roster-url");
			}
			if (this.models.applicationConfig.get("ffmPriorToApplyURL")) {
				linkData.exit = this.models.applicationConfig.get("ffmPriorToApplyURL");
			}
			
			if (this.models.applicationConfig.get("quotingEnabled")) {
				linkData.quotingEnabled = this.models.applicationConfig.get("quotingEnabled");
			}
			
		} else {
			log.error("error: unable to determine messages url");
		}
		
	

		return linkData;
	};

	HeaderComponent.prototype._showWelcomeView = function() {
		var accountHolder = this.models.accountHolder;

		var accountData = {};
		if (accountHolder && accountHolder.get("firstName")) {
			if (accountHolder.get("lastName")) {
				accountData.name = accountHolder.get("firstName") + " " + accountHolder.get("lastName");
			} else {
				accountData.name = accountHolder.get("firstName");
			}
		}
		var linkData = this._getLinkViewData();
		var actor = this.models.actor.toJSON();

		if (actor.isInternalUser) {
			this.regionManager.welcome.show(new InternalWelcomeView({
				data : accountData,
				actor : this.models.actor,
				configData : linkData,
				eventManager : this.eventManager,
				quote : this.models.quote,
				plans : this.models.plans,
				models : this.models
			}));
		} else {
			this.regionManager.welcome.show(new HeaderWelcomeView({
				data : accountData,
				configData : linkData,
				actor : this.models.actor,
				eventManager : this.eventManager,
				models : this.models
			}));
		}
	};

	HeaderComponent.prototype._showHeaderLinksView = function() {
		// loaded through configuration
		// this can stay here

		var linkData = this._getLinkViewData();
		var actor = this.models.actor.toJSON();

		if (actor.isInternalUser) {
			this.regionManager.links.show(new InternalLinksView({
				data : linkData,
				account : this.models.account,
				eventManager : this.eventManager,
				actor : this.models.actor,
				models : this.models
			}));
		} else {
			this.regionManager.links.show(new HeaderLinksView({
				data : linkData,
				account : this.models.account,
				eventManager : this.eventManager,
				actor : this.models.actor,
				models : this.models
			}));
		}
	};

	HeaderComponent.prototype._showProducerView = function() {
		// if we have a producer, then we can show it
		if (this.models.producer) {
			this.regionManager.producer.show(new HeaderProducerView({
				model : this.models.producer,
				eventManager : this.eventManager
			}));
		} else {
			this.regionManager.producer.hide();
			this.regionManager.producerDropdown.hide();
		}
	};

	HeaderComponent.prototype.hide = function() {
		// do nothing, always show
	};
	HeaderComponent.prototype.show = function() {
		// do nothing, always show
	};
	HeaderComponent.prototype.minimize = function() {
		// do nothing, always show
	};

	return HeaderComponent;
});