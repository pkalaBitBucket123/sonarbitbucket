/**
 * Copyright (C) 2012 Connecture
 *
 * Going to pull in all of the views for now to get it rendering, but we might want to end up breaking this out into multiple components, and have sub components of components
 *
 *
 */
define([ 'module', 'log', 'backbone', 'templateEngine', 'jquery-printElement', 'jquery-formatCurrency', 'dust-extensions', 'common/component/component', 'common/connecture.ui',
		'components/list/views/view.product.list', 'components/list/views/view.product.summary', 'components/list/views/view.product.detail', 'components/list/views/view.product.list.empty',
		'components/list/views/view.product.list.toolbar', 'components/list/views/view.product.detail.toolbar', 'components/list/views/view.product.compare',
		'components/list/views/view.product.compare.toolbar', 'components/list/views/email/view.modal.email', 'shopping/common/views/view.modal.confirmation',
		'shopping/common/collections/collection.product', 'components/list/views/view.product.progress.navigation', 'shopping/common/views/view.member.rates', 'components/list/models/model.email',
		'shopping/common/views/view.product.loading', 'text!html/shopping/components/list/view.product.list.dust', 'text!html/shopping/components/list/view.product.list.navigation.dust',
		'text!html/shopping/components/list/filters/filter.productline.dust', 'text!html/shopping/components/list/view.product.list.filters.dust' ], function(module, log, Backbone, templateEngine,
		printElement, formatCurrency, dustExtensions, connecture, connectureUI, ProductListView, ProductSummaryView, ProductDetailView, ProductListEmptyView, ProductListToolBarView,
		ProductDetailToolBarView, ProductCompareView, ProductCompareToolBarView, EmailModalView, ConfirmationView, ProductList, ProgressNavigation, MemberRatesView, EmailModel, ProductLoadingView,
		productListTemplate, productListNavigationTemplate, productLineFilterTemplate, productListFiltersTemplate) {

	// These two are for the main component view
	templateEngine.load(productListTemplate, 'view.product.list');
	templateEngine.load(productListNavigationTemplate, 'view.product.list.navigation');

	// These are for the filters, not sure if we want to override these or not?
	templateEngine.load(productLineFilterTemplate, 'filter.productline');
	templateEngine.load(productListFiltersTemplate, 'view.product.list.filters');

	// TODO: Look at why I need this one again
	// or if I can break out the main component view to use this
	var ProductListNavigationView = connecture.view.Base.extend({
		template : 'view.product.list.navigation',
		events : {},
		initialize : function() {
		},
		render : function() {
			var self = this;
			// Since this is partial...kind of hack it in there
			$(self.el).html(this.renderTemplate({
				data : self.options.data
			}));

			// apply tooltips
			$('a[title]', self.el).tooltip();

			return this;
		},
		close : function() {

		},
	});

	// This is the core view for the component, acting as the controller
	// for now. We will probably want to break this out eventually
	var ProductListComponentView = connecture.component.core.View.extend({
		template : 'view.product.list',
		frame_navigation : 'view.product.list.navigation',
		frame_localization : {
			//Changes done by Prabhat:US-25 
			title : 'Compare Plans',
			configClass : 'product-section'
		},
		// Right now it will handle navigation events, we will need
		// the global event for the component to handle
		// going right or left
		events : {
			'click .content-header ._compare' : 'showCompare',
			'click ._email' : 'emailPlan',
			'click .print' : 'printPlan'
		},
		initialize : function() {
			var self = this;
			this.events = _.extend({}, connecture.component.core.View.prototype.events, this.events);
			connecture.component.core.View.prototype.initialize.call(self);
		},
		render : function() {
			var self = this;
			connecture.component.core.View.prototype.render.apply(self, arguments);
			this.renderToBody();
			$('a[title]', self.el).tooltip();

			return this;
		},
		close : function() {

		},
		showCompare : function(event) {
			// Need
			// Trigger a parent event that says we want to show the detail
			this.options.eventManager.trigger('view:showProductCompare');
			event.preventDefault();
		},
		emailPlan : function(event) {
			event.preventDefault();

			// get selected plan card html and selected product line
			this.options.eventManager.trigger('view:emailSelectedPlans');
		},
		printPlan : function(event) {
			event.preventDefault();
			// pick out area to print
			// var self = this;
			// this.options.eventManager.trigger('view:print', self.$el.html());
			this.options.eventManager.trigger('view:printSelectedPlans');
		}
	});

	/**
	 * This View requires a Filter object
	 *
	 *
	 * There will be one global event called: FILTER
	 *
	 * We will be maintain a list of active filters that we will package up each time the event is trigger to send along to the event. It will possible go in into the filters object above
	 *
	 */
	var FilterView = connecture.view.Base.extend({
		events : {
			'click ._restoreLink' : 'restoreProduct',
			'click ._showHideFilter' : 'showHideFilter'
		},
		showHideFilter : function(event) {
			event.preventDefault();
			var self = this;

			// 1. Handle changing the html with DOM event
			// (not the same as re-rendering filterView)
			$('.dataGroup', self.el).toggle();
			$('.section', self.el).toggleClass("closed");
			var isVisible = $('.dataGroup', self.el).is(':visible');
			if (isVisible) {
				$(event.currentTarget).attr("title", "Hide this Filter");
			} else {
				$(event.currentTarget).attr("title", "Show this Filter");
			}
			var displayValue = isVisible ? '-' : '+';
			$('._showHideFilter', self.el).html(displayValue);

			// 2. We also need to set something on the filterGroup,
			// in case it IS re-rendered (for example, if a filter is selected)
			this.options.eventManager.trigger('view:toggleHidden', event, $('.dataGroup', self.el).is(':visible'));
		},
		initialize : function() {
			var filterEvents = this.options.filterDefinition.getEvents();
			// Given our filter we need to potentially add new events
			var events = {};
			_.each(filterEvents, function(filter, index) {
				events[filter.type + ' ' + filter.selector] = 'filter';
			});

			this.addEvents(events);
		},
		render : function() {
			var self = this;
			this.options.filterDefinition.render(true, self.el);
			$('a[title], span[title], label[title]', self.el).tooltip();
			return this;
		},
		filter : function(event) {
			// Going to need to pass the filter object and the value we
			// are filtering by to the event to handle the filtering
			this.options.eventManager.trigger('view:processFilter', event);
			event.preventDefault();
		},
		restoreProduct : function(event) {
			event.preventDefault();
			var eventTarget = event.currentTarget;
			var targetValue = $('input', eventTarget).val();
			// whether or not a model is hidden is stored on the product model,
			// which needs to be manipulated by the list component
			this.options.eventManager.trigger('component:toggleHiddenProductFilter', event, targetValue);
		},
		close : function() {

		}
	});

	// This is the core view for the component, acting as the controller
	// for now. We will probably want to break this out eventually
	var FiltersView = connecture.view.Base.extend({
		template : 'view.product.list.filters',
		frame : null,

		// Right now it will handle navigation events, we will need
		// the global event for the component to handle
		// going right or left
		events : {
			'click ._refineProducts' : 'refineProducts'
		},
		initialize : function(options) {
			var self = this;
			connecture.component.core.View.prototype.initialize.call(self);

			// Determine if there are any active filters, ignoring our
			// hard coded product line filter.
			// It would be nice if this could go in the filters code
			// but because we need to ignore a specific filter it is fine here

			// loop through each filter group,
			// process if key is not productLine
			// then loop through filters
			// since we are looping objects, we can't use the
			// every method to break out once ones is found
			this.activeFilters = false;
			if (this.options.filters.filterGroups) {
				_.each(this.options.filters.filterGroups, function(filterGroup, key) {
					if (!this.activeFilters && filterGroup.filters && key !== 'productLine') {
						_.each(filterGroup.filters, function(filter) {
							if (filter.active) {
								self.activeFilters = true;

							}
						});
					}
				});
			}

			this.activeFilters;

		},
		refineProducts : function(event) {
			event.preventDefault();
			// clear all filters
			this.options.eventManager.trigger('view:filters:clear');
		},
		render : function() {
			var self = this;
			$(self.el).append(this.renderTemplate({
				data : {
					activeFilters : this.activeFilters
				}
			}));

			// Loop through a list of our definitions, to render
			// each filter definition, via a view
			_.each(this.options.filters.filterGroups, function(filter, index) {
				filter.getEvents();
				$(self.el).append(new FilterView({
					filterDefinition : filter,
					eventManager : self.options.eventManager
				}).render().el);
			});

			$('a[title]', self.el).tooltip();

			return this;
		},
		close : function() {

		}
	});

	/**
	 * TODO: We should only be maintaining one product list object for the life time of this component, for the main component list view.
	 *
	 * This is because we will also want to go back to what is sorted and/or filtered if we switch views
	 *
	 * We will want a sorting object or sort configuration object. Basically something that will sit at the collection layer to see what sort is active or should be used per collection. These will get
	 * turned on and off independently of the collection, or from the outside, say from an event
	 *
	 */
	var ProductListComponent = function(configuration) {
		var self = this;
		// The id is coming from component configuration
		// client side, we need to make sure to merge this in
		configuration = _.extend(configuration, {
			id : module.config().id
		});

		connecture.component.core.Base.call(this, configuration);
		// //////////////////////////////////////
		// Store items
		// /////////////////////////////////////
		this.items = module.config().items;

		this.selectedProductView = module.config().defaultView;

		this.costViewCode = "monthly";

		// TODO - we will need to figure out a better way to handle this
		// going forward
		this.moduleConfig = module.config();

		this.listDeferreds = [];

		// handle events that may come from other components here:
		this.subscriptions = _.extend({}, this.subscriptions, {
			'component:selectProductLine' : function(productLineCode) {
				this.selectProductLine(productLineCode);
			},
			'component:showProductListDetail' : function(id) {
				this._showProductDetail(id, true);
			},
			'component:members:updated' : function() {
				// just trigger this again for our event manager to pick up
				this.eventManager.trigger('component:members:updated');
			},
			// This gets triggered whenever the account component
			// has completed its run
			'component:account:complete' : function() {
				// If it is already active then don't do any of this,
				// should basically turn to real time then
				if (module.config().show === 'complete' && !this.active) {
					// I think once it is active, it is active
					this.active = true;
					if (module.config().process === 'complete') {
						this.models.plans.reset([], {
							silent : true
						});

						this.show();
						this.models.plans.fetch({
							success : function() {
								// self.show();
							}
						});
					} else {
						this.show();
						this._triggerCurrentView(true);
					}
				} else if (this.active) {
					// otherwise if it is active already (but might be hidden)
					// show and refresh current view
					this.show();
					this._triggerCurrentView(true);
				}
			}
		});

		this.publish = _.extend({}, this.publish, {
			'component:selectProduct' : 'component:selectProduct',
			'component:removeProduct' : 'component:removeProduct',
			'component:mark:progress' : 'component:mark:progress',
			'component:mark:active' : 'component:mark:active',
			'view:showEmailModal' : 'component:showEmailModal'
		});

		this.configuration.layout = _.extend({}, this.configuration.layout, {
			View : ProductListComponentView,
			options : {
				renderNavigation : true
			},
			regions : {
				'navigation' : '.content-navigation',
				'tools' : '.content-tools',
				'content' : '.content-section',
				'contentSidebar' : '.content-sidebar'
			}
		});

		this.routes = [ {
			name : '',
			route : 'view/' + this.id + '/detail/:id',
			callback : function(productId) {
				// the route callback must return the properties
				// around this route, this will come back through
				// the start/update events
				return {
					id : self.id,
					view : 'detail',
					item : {
						id : productId
					}
				};
			}
		}, {
			name : '',
			route : 'view/list/browse',
			callback : function() {
				// the route callback must return the properties
				// around this route, this will come back through
				// the start/update events
				return {
					id : self.id,
					view : 'browse'
				};
			}
		} ];

		this.selectedProducts = [];
	};

	ProductListComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	ProductListComponent.prototype.registerEvents = function() {
		this.eventManager.on("initialize:before", function() {
			if (this.models.plans) {
				this.currentProductList = this.models.plans;
			}
		}, this);

		this.eventManager.on('initialize:after', function() {
			this.navigation.addView(ProgressNavigation, {
				eventManager : this.eventManager,
				componentId : this.id,
				account : this.models.account
			});

			this.regionManager.block(new ProductLoadingView());

			var self = this;

			var myArray = [];
			_.each(this.listDeferreds, function(promise) {
				myArray.push(promise());
			});

			$.when.apply($, myArray).then(function(value) {
				self.regionManager.unblock();
			});

		}, this);

		this.eventManager.on("start", function(options) {
			var self = this;

			// This component can start with no plans, if they have not been through
			// the flow yet

			// IF we do have a route and we have plans
			// then show the list, otherwise only show on startup
			if (!this._handleEmptyPlanList()) {

				if (options.route) {
					this.active = true;
					// TODO, triggering a reset here so it goes through
					// all of the processing, we need to figure out a better way
					// to do this
					this.models.productLines.trigger('reset');
					this.models.plans.trigger('reset');
					if (options.route.view === 'list') {
						this._showProductList(false);
					} else if (options.route.view === 'detail') {
						this._showProductDetail(options.route.item.id, false);
					} else if (options.route.view === 'top') {
						this._showTopProducts(false);
					} else if (options.route.view === 'compare') {
						this._showProductCompare(false, false);
					} else if (options.route.view === 'browse') {
						this.eventManager.trigger('component:view:activated', this.id);
						this._showProductList(false);
					}
					this.eventManager.trigger('component:mark:progress', 'list');
				} else if (module.config().startup === 'show') {
					this.active = true;
					// TODO, triggering a reset here so it goes through
					// all of the processing, we need to figure out a better way
					// to do this
					this.models.plans.trigger('reset');
					// show default view
					this._refreshCurrentView();
					this.eventManager.trigger('component:mark:progress', 'list');
				} else if (module.config().startup === 'hide') {
					this.hide();
				}
			} else {
				// determine if we should show on startup
				// TODO: dup code clean up :)
				if (module.config().startup === 'hide') {
					this.hide();
				} else {
					this.active = true;
				}
			}
		}, this);

		this.eventManager.on("update", function(options) {
			if (options.route) {
				// upon a route that gets trigger while the app
				// is already started up, change the component to
				// being active and then show it.
				if (!this.active) {
					this.active = true;
					this.show();
				}

				// Make sure we have plans as well. Otherwise, if a route
				// is called, show the plan list but keep the default empty
				// plan view.
				if (this.models.plans && !this.models.plans.isEmpty()) {
					if (options.route.view === 'list') {
						this._showProductList(false);
					} else if (options.route.view === 'detail') {
						this._showProductDetail(options.route.item.id, false);
					} else if (options.route.view === 'top') {
						this._showTopProducts(false);
					} else if (options.route.view === 'compare') {
						this._showProductCompare(false, false);
					}

					this.eventManager.trigger('component:view:activated', this.id);
				}

			}
			// if nothing matches leave as is

		}, this);

		// whenever the cart changes in the account, refresh our view
		// because cart is nested, we need to do a wild card change on it
		this.models.account.on('change:cart', function() {
			this._refreshCurrentView();
		}, this);

		this.eventManager.on('view:product:rates:show', function(productId) {
			var product = this.models.plans.get(productId);
			this.modal.show(new MemberRatesView({
				eventManager : this.eventManager,
				account : this.models.account,
				model : product
			}));
		}, this);

		this.eventManager.on('view:showMonthlyCost', function() {

			this.costViewCode = "monthly";

			if (this.selectedProductView === "ProductDetail") {
				this._showProductDetail(null, false);
			} else if (this.selectedProductView === "ProductCompare") {
				this._showTopProducts(false);
			} else if (this.selectedProductView === "ProductList") {
				this._showProductList(false);
			}

		}, this);

		this.eventManager.on('view:showPaycheckCost', function() {

			this.costViewCode = "paycheck";

			if (this.selectedProductView === "ProductDetail") {
				this._showProductDetail(null, false);
			} else if (this.selectedProductView === "ProductCompare") {
				this._showTopProducts(false);
			} else if (this.selectedProductView === "ProductList") {
				this._showProductList(false);
			}
		}, this);

		this.eventManager.on('view:selectProduct', function(selectedProduct) {

			var self = this;
			// find best fit products for other product lines
			var plans = self.models.plans;

			var productLineModels = {};
			var bestFitPlans = {};

			_.each(plans.models, function(plan) {

				// assemble product lines
				var planProductLine = plan.get("productLineCode");
				if (productLineModels[planProductLine]) {
					var productArray = productLineModels[planProductLine];
					productArray[productArray.length] = plan;
				} else {
					var productArray = new Array();
					productArray[productArray.length] = plan;
					productLineModels[planProductLine] = productArray;
				}
			});

			_.each(productLineModels, function(productArray) {

				var productList = new ProductList(productArray);
				var result = self.findBestFitPlan(productList);
				if (result) {
					bestFitPlans[result.get("productLineCode")] = result.toJSON();
				}
			});

			this.eventManager.trigger('component:selectProduct', selectedProduct, bestFitPlans);
		}, this);

		this.eventManager.on('view:showProductDetail', function(id) {
			this._showProductDetail(id, true);

			this.eventManager.trigger('component:view:activated', this.id);

		}, this);

		this.eventManager.on('view:showProductList', function() {
			this._showProductList(true);

			this.eventManager.trigger('component:view:activated', this.id);

		}, this);

		this.eventManager.on('view:showTopProducts', function() {
			this._showTopProducts(true);

			this.eventManager.trigger('component:view:activated', this.id);
		}, this);

		this.eventManager.on('view:showProductCompare', function() {
			this._showProductCompare(false, true);

			this.eventManager.trigger('component:view:activated', this.id);
		}, this);

		// Globally toggle whether a plan is checked or not
		// Putting this here because it can be used for multiple
		// different purposes
		this.eventManager.on('view:planCheckedToggle', function(id) {
			var pos = this.selectedProducts.indexOf(id);
			if (pos === -1) {
				this.selectedProducts.push(id);
			} else {
				this.selectedProducts.splice(pos, 1);
			}

			this._refreshHeaderButtons();

		}, this);

		this.eventManager.on('component:refreshCurrentView', function(event, targetValue) {
			this._refreshCurrentView();
		}, this);

		this.eventManager.on('view:sortChange', function(sortType) {
			// given the id of the sort type, make that one selected
			var activeSort = null;
			_.each(this.sortConfig.sorts, function(sort) {
				if (sort.id === sortType) {
					sort['selected'] = true;
					activeSort = sort;
				} else {
					sort['selected'] = false;
				}
			});
			this.currentProductList.sortProp = activeSort;
			// this.currentProductList.sort();

			this._showProductList(false);

		}, this);

		this.eventManager.on('view:printSelectedPlans', function() {
			var self = this;

			// 'Borrowed' code from above (line 555) -- in _showProductDetail
			// Otherwise no context for Cost Estimation, and module is not rendered for print
			var widgetModel = this.models.widgets.getWidget('CostEstimator', 'widget');
			estimationProductLineCode = null;
			if (widgetModel) {
				estimationProductLineCode = widgetModel.get('config').productLineForEstimation;
			}

			var printProducts = new Array();
			if (self.selectedProducts && self.selectedProducts.length > 0) {
				printProducts = self.selectedProducts;
			} else if (self.selectedProductView === "ProductDetail") {
				// when no products are selected, we show the first product
				// in the currentProducts list
				if (self.currentProductList && self.currentProductList.models && self.currentProductList.models.length > 0) {
					printProducts[0] = self.currentProductList.models[0].id;
				}
			}

			if (printProducts.length > 0) {

				var printHTML = $('<div></div>');

				// get all selected products and render details for each

				_.each(printProducts, function(selectedProductId) {

					var selectedModel = null;
					_.each(self.models.plans.models, function(model) {
						if (model.id === selectedProductId) {
							selectedModel = model;
							return;
						}
					});

					var productDetailView = new ProductDetailView({
						estimationProductLineCode : estimationProductLineCode,
						costViewCode : self.costViewCode,
						eventManager : self.eventManager,
						model : selectedModel
					});

					var productDetailHTML = $(productDetailView.render().el).wrap('<div/>').addClass('data-section').addClass('print-plan');
					$('._currency', productDetailHTML).formatCurrency();
					$('._costEstimateCurrency', productDetailHTML).formatCurrency({
						roundToDecimalPlace : 0
					});
					printHTML.append(productDetailHTML);
				});

				$(printHTML).printElement({
					overrideElementLess : [ '../styles/main.less' ],
					/*overrideElementScript : [ '../scripts/thirdparty/less-1.0.41.min.js' ]*/
					printMode : 'popup'
				});
			}

		}, this);

		
		this.eventManager.on('view:emailSelectedPlans', function() {
			var self = this;
			// call prototype function, easy to override for different contexts
			self.emailPlans();
		}, this);

		this.eventManager.on('view:productLineChanged', function(event) {
			// 1. clear out any selected products
			// uncheck selected products
			_.each(this.models.plans.models, function(product) {
				product.set({
					"checked" : false
				}, {
					silent : true
				});
			});

      this.selectedProducts = [];

      this.lastDetailedViewedProductId = null;

			this.eventManager.trigger('component:filters:reset', {
				refresh : false
			});

			// 2. pass event along to filter..
			this.eventManager.trigger('view:filter:trigger', event);

		}, this);

		this.eventManager.on("view:closeEmailModal", function() {
			this.modal.close();
		}, this);

		// TODO | won't need this once employee modal is refactored
		this.eventManager.on("view:closeConfirmationModal", function() {
			this.modal.close();
		}, this);

		this.eventManager.on("view:closeEmailModalConfirm", function(success) {

			this.modal.close();

			var confirmationData = {};

			if (success) {
				confirmationData.success = 'success';
				confirmationData.message = "Your email was successfully sent";
			} else {
				confirmationData.success = 'failure';
				confirmationData.message = "An error occurred. Your email was not sent";
			}

			var emailConfirmationModalView = new EmailConfirmationModalView({
				eventManager : this.eventManager,
				data : confirmationData
			});

			this.modal.show(emailConfirmationModalView);

		}, this);

		// whenever we make a requst for new plans, block the ui
		this.models.plans.on('request', function() {
			this.regionManager.block(new ProductLoadingView());
		}, this);

		// we need to unblock on the sync because the
		// reset might not always be called depending
		// on if nothing changed
		this.models.plans.on('sync', function() {
			// when the sync is done, we might have to do
			// more processing that requires the UI to be blocked

			// TODO: Each plugin creates its OWN Deferred and then we
			// do what we do with that callback stuff
			var self = this;
			// this.eventManager.trigger('component:product:list:sync:start');

			var myArray = [];
			_.each(this.listDeferreds, function(promise) {
				myArray.push(promise());
			});

			$.when.apply($, myArray).then(function(value) {
				self.regionManager.unblock();
			});
		}, this);

		// This method gets call, any time the models get reset
		// We need to set the new list and then do some processing
		// updates and then refresh the views.
		this.models.plans.on('reset', function() {
      this.currentProductList = this.models.plans;

      this.lastDetailedViewedProductId = null;

			// apply default filters, don't refresh the view
			this.eventManager.trigger('component:filters:reset', {
				refresh : false
			});

			// This is kind of lame but because the nature of our
			// events and how many different paths we are, we need to
			// trigger an event is saying the product list as been reset
			// reapply any plugin changes.
			this.eventManager.trigger('component:product:list:reset');

			// Once all of that is done, resort
			this.currentProductList.sort({
				silent : true
			});

			// TODO | WIP
			this.refreshProductLineFilter();

			if (module.config().show === 'available') {
				this.show();
				this.active = true;
			}

			// Once that is all done, refresh the view
			// only refresh/render if we are active
			if (this.active) {
				this._refreshCurrentView();
			}

		}, this);

		this.models.productLines.on('reset', function() {
			var productLineFilters = [];
      var isFirst = true;
      this.lastDetailedViewedProductId = null;
			//BCBSNE : SHSBCBSNE-228, 230; Change to display plans on basis of coverage type
			//hardcoding label and values for now
			var productLineFilter = {};
			var covType = this.models.account.get("coverageType");
			try{
				if(covType == null || covType == "") {
					setCoverageTypefromURLParam(this.models.account);
					covType = this.models.account.get("coverageType");
				}
			}catch(e){
				console.log("Error:" + e);
			}
			
			if(covType){
				if(covType=='DEN'){
					productLineFilter.label = 'Dental';
					productLineFilter.value = 'PROD_DENTAL';
				}
				else if(covType=='STH'){
					productLineFilter.label = 'Tempcare';
					productLineFilter.value = 'STH';
				}
				else if(covType=='IFP' || covType=='Med'){
					productLineFilter.label = 'Medical';
					productLineFilter.value = 'PROD_MED';
				}
				productLineFilters[0]=productLineFilter;
				if (isFirst) {
					productLineFilter['default'] = true;
					isFirst = false;
				}
			} else {
				this.models.productLines.each(function(productLine) {

				var productLineFilter = {};

				// productLineFilter.label = productLine.get('productLineName' + " Plans");
				productLineFilter.label = productLine.get('productLineName');
				productLineFilter.value = productLine.get('productLineCode');

				if (isFirst) {
					productLineFilter['default'] = true;
					isFirst = false;
				}

				productLineFilters[productLineFilters.length] = productLineFilter;
			});
			}

			var productLine = {
				type : '=',
				property : 'productLineCode',
				resetOnClear : false,
				view : {
					control : {
						getEvent : function() {
							return {};
						},
						processActiveFilters : function(event) {
							// Unselect all to start
							_.each(this.filters, function(filter) {
								filter.active = false;
							});

							var eventFilter = this.getFilter(event);
							eventFilter.active = true;
						},
						getFilterId : function(event) {
							return $(event.currentTarget).attr('data-filter-id');
						},
						bind : function(context) {

						}
					}
				},
				filters : productLineFilters

			};

			var filterModel = this.models.filters;
			for (var i = 0; i < productLine.filters.length; i++) {
			     if(productLine.filters[i].value=="STH"){
			        productLine.property="planType";
			    }
			}
			filterModel.set({
				productLine : productLine
			});
			this.filterConfigurations = filterModel.toJSON();

			this.eventManager.trigger('component:filter:add', 'productLine', productLine);
		}, this);

		// This method gets call anytime
		this.models.plans.on('updated', function() {

			// we don't want to trigger a reset here
			this.currentProductList.sort({
				silent : true
			});
			// When the list gets updated, usually a batch
			// update of the data in the models, just refresh the view
			// only refresh/render if we are active
			if (this.active) {
				this._refreshCurrentView();
			}

		}, this);

		this.eventManager.on('view:email:send', function(data) {
			this.sendEmail(data);
		}, this);

	};

	ProductListComponent.prototype.selectProductLine = function(productLineCode) {
		if (productLineCode) {

			// product line is a filter,
			// and filters are treated as lowercase by the filter plugin
      productLineCode = productLineCode.toLowerCase();

      this.lastDetailedViewedProductId = null;

			$('._productLineDropDown ._option input[value="' + productLineCode + '"]').click();
		}
	};

	ProductListComponent.prototype.findBestFitPlan = function(plans) {
		var bestFitPlan = null;

		// takes in a collection, returns a single model
		_.each(plans.models, function(plan) {
			if (plan.get("isBestMatch")) {
				bestFitPlan = plan;
				return plan;
			}
		});

		if (!bestFitPlan) {
			// if best fit plan not determined, grab first plan
			if (plans.models[0]) {
				bestFitPlan = plans.models[0];
			}
		}

		return bestFitPlan;
	};

	ProductListComponent.prototype._getProductLineProductCount = function() {

		var self = this;
		var total = 0;
		var activeFilter = null;

		var productListFilterGroup = this.filters.filterGroups['productLine'];

		if (productListFilterGroup) {
			_.each(productListFilterGroup.filters, function(filter) {
				if (filter.active) {
					// there should only be one active filter
					// for this filter group!
					activeFilter = filter;
					return;
				}
			});
		}

		if (activeFilter) {
			_.each(self.models.plans.models, function(product) {
				if (activeFilter.includeTotal(product)) {
					total++;
				}
			});
		}
		
		return total;
	};

	ProductListComponent.prototype._showProductList = function(triggerHistory) {

		$('.content-sidebar', self.el).show();

		// We need to pass in a temp product list because the product list view
		// pagination stuff interfers with the global events

		// do a sort here to fix an issue with the sort not being correct
		// first time then build the temp list off the sorted ones
		// so views match up when compare/detail is clicked
// start change by rsingh for user story 374		
		if($(this.models.account.get("questions")).attr("value")=="Less_OOP")
			$(this.sortConfig.sorts).attr("how","desc");
// end change by rsingh for user story 374		
		this.currentProductList.sort();

		var tempProductList = new ProductList(this.currentProductList.models, {
			comparator : false
		});

		var quotingEnabled = false;

		if (this.models.applicationConfig) {
			var quotingEnabledConfig = this.models.applicationConfig.get("quotingEnabled");
			quotingEnabled = (quotingEnabledConfig != null) ? quotingEnabledConfig : false;
            var displayPlansDisclaimerConfig = this.models.applicationConfig.get("displayPlansDisclaimer");
			displayPlansDisclaimer = (displayPlansDisclaimerConfig != null) ? displayPlansDisclaimerConfig : false;        }
		this.regionManager.content.show(new ProductListView({
			costViewCode : this.costViewCode,
			// we are passing in a new collection here because we don't
			// want to cross events
			collection : tempProductList,
			account : this.models.account,
			actor : this.models.actor,
			quotingEnabled : quotingEnabled,
			quote : this.models.quote,
			eventManager : this.eventManager,
            displayPlansDisclaimer : displayPlansDisclaimer,
            session : this.session
		}), {
			renderToParent : true
		});

		var productLineCount = this._getProductLineProductCount();

		this.regionManager.tools.show(new ProductListToolBarView({
			toggleCost : this.models.planConfig.get("hasPaycheckCost"),
			costViewCode : this.costViewCode,
			eventManager : this.eventManager,
			filter : this.filters.filterGroups['productLine'],
			productListSize : productLineCount,
			filteredProductListSize : this.currentProductList.length,
			sortConfig : this.sortConfig
		}));

		this.regionManager.contentSidebar.show(new FiltersView({
			filters : this.filters,
			eventManager : this.eventManager,
		}));

		this.selectedProductView = "ProductList";
		this._refreshHeaderButtons();

		if (triggerHistory) {
			Backbone.history.navigate('view/' + this.id + '/list');
		}

	};

	ProductListComponent.prototype._showTopProducts = function(triggerHistory) {

		// top product list is just the compare list,
		// not based on selection. Supposed to remove any product
		// selections when navigating to this view.

		var self = this;

		// uncheck selected products
		_.each(this.models.plans.models, function(product) {
			product.set("checked", false);
		});

		this.selectedProducts = [];

		self._showProductCompare(true, triggerHistory);
	};

	ProductListComponent.prototype._showProductCompare = function(expandBenefits, triggerHistory) {

		var self = this;
		// Make sure we have at least something checked

		$('.content-sidebar', self.el).hide();

		// get cost estimation product line
		var estimationProductLineCode = null;
		var providerProductLineCode = null;

		// TODO: This is not checking if cost estimator is turned on or not
		var widgetModel = this.models.widgets.getWidget('CostEstimator', 'widget');

		if (widgetModel) {
			estimationProductLineCode = widgetModel.get('config').productLineForEstimation;
		}

		// provider lookup product line
		var providerWidgetModel = this.models.widgets.getWidget('ProviderLookup', 'widget');
		if (providerWidgetModel) {
			providerProductLineCode = providerWidgetModel.get('config').productLineForProviderLookup;
		}

		var collapseBenefits = true;

		if (expandBenefits) {
			collapseBenefits = false;
		}

		var products = [];
		// We are only showing a total of three
		// if they checked more than three
		// show the first three only
		if (this.selectedProducts.length > 0) {
			var total = this.selectedProducts.length;

			if (total > 3) {
				total = 3;
			}

			// Loop through everything we have checked
			for ( var i = 0; i < total; i++) {
				var value = this.selectedProducts[i];
				_.each(self.currentProductList.models, function(model) {
					if (model.id === value) {
						products.push(model);
						return;
					}
				});
			}
		} else {
			if (self.currentProductList) {
				for ( var i = 0; i < 3; i++) {
					if (self.currentProductList.models[i]) {
						products.push(self.currentProductList.models[i]);
					}
				}
			}
		}


		var quotingEnabled = false;

		if (this.models.applicationConfig) {
			var quotingEnabledConfig = this.models.applicationConfig.get("quotingEnabled");
			quotingEnabled = (quotingEnabledConfig != null) ? quotingEnabledConfig : false;
            var displayDisclaimerConfig = this.models.applicationConfig.get("displayPlansDisclaimer");
			displayPlansDisclaimer = (displayDisclaimerConfig != null) ? displayDisclaimerConfig : false;        }
		this.regionManager.content.show(ProductCompareView, {
			estimationProductLineCode : estimationProductLineCode,
			providerProductLineCode : providerProductLineCode,
			costViewCode : self.costViewCode,
			collection : new ProductList(products, {
				comparator : false
			}),
			account : this.models.account,
			actor : this.models.actor,
			quotingEnabled : quotingEnabled,
            displayPlansDisclaimer : displayPlansDisclaimer,
			collapseBenefits : collapseBenefits,
			eventManager : this.eventManager
		}, {
			renderToParent : true
		});

		var productLineCount = this._getProductLineProductCount();

		this.regionManager.tools.show(new ProductCompareToolBarView({
			toggleCost : this.models.planConfig.get("hasPaycheckCost"),
			costViewCode : this.costViewCode,
			eventManager : this.eventManager,
			filter : this.filters.filterGroups['productLine'],
			productListSize : productLineCount,
			filteredProductListSize : products.length
		}));

		this.regionManager.contentSidebar.close();

		this.selectedProductView = "ProductCompare";
		this._refreshHeaderButtons();

		if (triggerHistory) {
			Backbone.history.navigate('view/' + this.id + '/compare');
		}
	};

	ProductListComponent.prototype._showProductDetail = function(id, triggerHistory) {

    var self = this;

		$('.content-sidebar', self.el).hide();

		// get cost estimation product line
		var estimationProductLineCode = null;
		var providerProductLineCode = null;

		// TODO: This is not checking if cost estimator is turned on or not
		var widgetModel = this.models.widgets.getWidget('CostEstimator', 'widget');

		if (widgetModel) {
			estimationProductLineCode = widgetModel.get('config').productLineForEstimation;
		}

		// provider lookup product line
		var providerWidgetModel = this.models.widgets.getWidget('ProviderLookup', 'widget');
		if (providerWidgetModel) {
			providerProductLineCode = providerWidgetModel.get('config').productLineForProviderLookup;
		}

	
		//<by rsingh BCBSNE-Shopping; changes for the SBC files> 
		if (typeof id === 'undefined' || id === null) {
			if (this.selectedProducts.length === 1) {
				id = this.selectedProducts[0];
			} else {
				id = self.currentProductList.models[0].id;
			}
		}
		
		var linkData = {};
		// get product links detail
		 
		   var selfModel = this; 
		if (this.models.applicationConfig) {
			//linkData.sbcBaseURL = this.models.applicationConfig.get("sbc-base-url");
			//linkData.brochureBaseURL = this.models.applicationConfig.get("brochure-base-url");
			 var plans = this.models.plans;
			     _.each(plans.models, function(plan) {
			       if(plan.id == id){
			          linkData.sbcBaseURL = selfModel.models.applicationConfig.get("sbc-base-url") + plan.get('sbcUrl');
			          linkData.brochureBaseURL = selfModel.models.applicationConfig.get("brochure-base-url") + plan.get('brochureUrl');
			        }
			     });
			          
		}
		
/*		var linkData = {};
		// get product links detail
		if (this.models.applicationConfig) {
			linkData.sbcBaseURL = this.models.applicationConfig.get("sbc-base-url");
			linkData.brochureBaseURL = this.models.applicationConfig.get("brochure-base-url");
		}*/
		/*if (typeof id === 'undefined' || id === null) {
			if (this.selectedProducts.length === 1) {
				id = this.selectedProducts[0];
			} else if (this.currentProductList.models[0]) {
				id = this.currentProductList.models[0].id;
			}
		}*/
		//</by rsingh BCBSNE-Shopping; changes for the SBC files> 
		
		
		// Only show the Detail View if there is at least one model in the list
		//   NOTE: You are currently allowed to hide all of the models
		if (id) {
			$('.content-sidebar', self.el).hide();

			// get cost estimation product line
			var estimationProductLineCode = null;
			var providerProductLineCode = null;

			// TODO: This is not checking if cost estimator is turned on or not
			var widgetModel = this.models.widgets.getWidget('CostEstimator', 'widget');

			if (widgetModel) {
				estimationProductLineCode = widgetModel.get('config').productLineForEstimation;
			}

			// provider lookup product line
			var providerWidgetModel = this.models.widgets.getWidget('ProviderLookup', 'widget');
			if (providerWidgetModel) {
				providerProductLineCode = providerWidgetModel.get('config').productLineForProviderLookup;
			}

	//		var linkData = {};
			var quotingEnabled = false;
			// get product links detail
			if (this.models.applicationConfig) {
//				linkData.sbcBaseURL = this.models.applicationConfig.get("sbc-base-url");
//				linkData.brochureBaseURL = this.models.applicationConfig.get("brochure-base-url");
				var quotingEnabledConfig = this.models.applicationConfig.get("quotingEnabled");
				quotingEnabled = (quotingEnabledConfig != null) ? quotingEnabledConfig : false;
                
				var displayPlansDisclaimerConfig = this.models.applicationConfig.get("displayPlansDisclaimer");
				displayPlansDisclaimer = (displayPlansDisclaimerConfig != null) ? displayPlansDisclaimerConfig : false;            }
			var selectedModel = null;
			_.each(this.models.plans.models, function(model) {
				if (model.id === id) {
					selectedModel = model;
					return;
				}
			});

			this.regionManager.content.show(new ProductDetailView({
				linkData : linkData,
				estimationProductLineCode : estimationProductLineCode,
				providerProductLineCode : providerProductLineCode,
				costViewCode : this.costViewCode,
				eventManager : this.eventManager,
				model : selectedModel,
				account : this.models.account,
				actor : this.models.actor,
				quotingEnabled : quotingEnabled,
                displayPlansDisclaimer : displayPlansDisclaimer,
				quote : this.models.quote
			}), {
				renderToParent : true
			});

			var productLineCount = this._getProductLineProductCount();

			this.regionManager.tools.show(new ProductDetailToolBarView({
				toggleCost : this.models.planConfig.get("hasPaycheckCost"),
				costViewCode : this.costViewCode,
				eventManager : this.eventManager,
				filter : this.filters.filterGroups['productLine'],
				productListSize : productLineCount,
				filteredProductListSize : this.currentProductList.length
			}));

			// This region is now in the hands of the component
			// I gave control to. I still need to tell it to close or show though
			this.regionManager.contentSidebar.close();

			this.selectedProductView = "ProductDetail";
			this._refreshHeaderButtons();

			if (triggerHistory) {
				Backbone.history.navigate('view/' + this.id + '/detail/' + encodeURIComponent(id));
			}
		}
	};

	ProductListComponent.prototype.refreshProductLineFilter = function() {

		// based on coverage selection, certain product lines may show
		// no products and should be marked as unavailable for selection.
		// in this case, if the selected product line has no products
		// the filter should change to select the next product line with
		// products, based on the product line sort order.

		var self = this;

		// TODO | find the product line filter values
		var productLineFilters = [];
		if (self.filter) {
			productLineFilters = self.filters.filterGroups.productLine.filters;
		}

		// TODO | find the selected product line filter value
		var activeFilterValue = null;

		_.each(productLineFilters, function(filter) {
			if (filter.active) {
				activeFilterValue = filter.value;
			}
		});

		var currentFilterHasProducts = false;

		// check if any products exist for that product line
		if (activeFilterValue) {
			currentFilterHasProducts = self.productsExistForProductLine(activeFilterValue);
		}

		if (!currentFilterHasProducts) {
			// we need to change the selected product line

			var nextProductLineFound = false;
			var nextProductLineValue = null;

			// get other product lines, in order
			var productLines = self.productLines;
			_.each(productLines, function(productLine) {

				if (nextProductLineFound) {
					return;
				}

				var productLineCode = productLine.productLineCode;

				if (productLineCode != activeFilterValue) {
					if (self.productsExistForProductLine(productLineCode)) {
						nextProductLineValue = productLineCode;
						nextProductLineFound = true;
						return;
					}
				}

			});

			if (nextProductLineFound) {
				// select this product line
				self.selectProductLine(nextProductLineValue);
			}
		}
	};

	ProductListComponent.prototype.productsExistForProductLine = function(productLineCode) {

		var self = this;

		var productFound = false;

		// get list of plans
		var plans = self.models.plans;
		_.each(plans.models, function(plan) {

			if (productFound) {
				return;
			}

			var planProductLineCode = plan.get("productLineCode");
			var planType=plan.get("planType");
			
			if(productLineCode != "STH"){
			// need lowercase check in case we are running against a filter
				if (planProductLineCode && (planProductLineCode.toLowerCase() == productLineCode.toLowerCase())) {
					productFound = true;
					return;
				}
			} else{
				if (planType && (planType.toLowerCase() == productLineCode.toLowerCase())) {
					productFound = true;
					return;
				}
			}
		});

		return productFound;
	};

	ProductListComponent.prototype._refreshCurrentView = function(triggerHistory) {
		// what if we have no plans when this gets called?
		if (!this._handleEmptyPlanList()) {
			if (this.selectedProductView === "ProductDetail") {
				this._showProductDetail(null, triggerHistory);
			} else if (this.selectedProductView === "ProductCompare") {
				this._showTopProducts(triggerHistory);
			} else if (this.selectedProductView === "ProductList") {
				this._showProductList(triggerHistory);
			}
	    }
	};

	// TODO | this is kind of crap, but we need to capture certain events for analytics,
	// and we don't want to capture on every refresh; may be temporary hack until we
	// get everything widgetized
	ProductListComponent.prototype._triggerCurrentView = function(triggerHistory) {
		// what if we have no plans when this gets called?
		if (!this._handleEmptyPlanList()) {
			if (this.selectedProductView === "ProductDetail") {
				this.eventManager.trigger('view:showProductDetail');
			} else if (this.selectedProductView === "ProductCompare") {
				this.eventManager.trigger('view:showTopProducts');
			} else if (this.selectedProductView === "ProductList") {
				this.eventManager.trigger('view:showProductList');
			}
		}
	};

	ProductListComponent.prototype._handleEmptyPlanList = function() {
		var planListEmpty = !this.models.plans || this.models.plans.isEmpty();
		if (planListEmpty) {
			// Display the empty plan view. If the list is being feteched
			// while this is called, the component should already be set to
			// active, then on reset of the list, it should render the
			// correct view
			this.regionManager.content.show(new ProductListEmptyView({
				eventManager : this.eventManager
			}), {
				renderToParent : true
			});

			$('.content-sidebar', this.el).hide();
		}
		return planListEmpty;
	}

	ProductListComponent.prototype._refreshHeaderButtons = function() {

		var isDetailView = (this.selectedProductView === "ProductDetail");

		var isInternalUser = this.models.actor.get("isInternalUser");

		var data = {};

		data.compareDisabled = !(this.selectedProducts.length > 1) || this.selectedProducts.length > 3 || isDetailView;
		data.emailEnabled = (this.selectedProducts.length > 0 && this.selectedProducts.length < 4) || isDetailView;
		data.printEnabled = (this.selectedProducts.length > 0) || isDetailView;
		data.isInternalUser = isInternalUser;

		// TODO - adjust all buttons at once if you're going to re-render the button area
		this.regionManager.navigation.show(new ProductListNavigationView({
			data : data
		}));
		// TODO - adjust all buttons at once if you're going to re-render the button area

	};

	/**
	 * Overriding here because list component is configurable on when it is shown
	 */
	ProductListComponent.prototype.show = function() {
		if (this.regionManager && this.active) {
			this.regionManager.show();
			this.eventManager.trigger('component:mark:progress', 'list');
		}
	};

	ProductListComponent.prototype.load = function(data) {

		// This needs to go in some sort of component configuration file
		this.sortConfig = {
			sorts : [ {
                //Changes for SHSBCBSNE-19
				//id : 'recommend',
				//what : 'sortScore',
				//label : 'Recommended',
				//how : 'desc',
				//selected : true
			//}, {
				id : 'premium',
				what : 'cost.monthly.rate.amount',
				label : 'Lowest to Highest Premium',
				how : 'asc',
				selected : true
			} ]
		};

		if (this.models.productLines) {
			// create product line filter
			var productLineFilters = [];
			var isFirst = true;

			this.models.productLines.each(function(productLine) {

				var productLineFilter = {};

				// productLineFilter.label = productLine.get('productLineName' + " Plans");
				productLineFilter.label = productLine.get('productLineName');
				productLineFilter.value = productLine.get('productLineCode');

				if (isFirst) {
					productLineFilter['default'] = true;
					isFirst = false;
				}

				productLineFilters[productLineFilters.length] = productLineFilter;
			});

			var productLine = {
				type : '=',
				property : 'productLineCode',
				resetOnClear : false,
				view : {
					control : {
						getEvent : function() {
							return {};
						},
						processActiveFilters : function(event) {
							// Unselect all to start
							_.each(this.filters, function(filter) {
								filter.active = false;
							});

							var eventFilter = this.getFilter(event);
							eventFilter.active = true;
						},
						getFilterId : function(event) {
							return $(event.currentTarget).attr('data-filter-id');
						},
						bind : function(context) {

						}
					}
				},
				filters : productLineFilters

			};

			var filterModel = this.models.filters;
			filterModel.set({
				productLine : productLine
			});
			this.filterConfigurations = filterModel.toJSON();
		}

		connecture.component.core.Base.prototype.load.call(this, data);
	};

	ProductListComponent.prototype.emailPlans = function(data) {

		var self = this;

		var emailData = {};

		// get cost view code, "monthly"/"paycheck"
		emailData.costViewCode = self.costViewCode;

		var emailProducts = [];

		// check that between 1 and 3 plans (inclusive) are selected,
		// unless we're viewing the detail view
		if (self.selectedProducts && self.selectedProducts.length > 0 && self.selectedProducts.length < 4) {

			_.each(self.selectedProducts, function(value, index) {
				_.each(self.currentProductList.models, function(model) {
					if (model.id === value) {
						emailProducts.push(model);
						return;
					}
				});
			});

		} else if (self.selectedProductView === "ProductDetail") {

			// when no products are selected, we show the first product
			// in the currentProducts list
			if (self.currentProductList && self.currentProductList.models && self.currentProductList.models.length > 0) {
				emailProducts.push(self.currentProductList.models[0]);
			}
		}

		if (emailProducts.length > 0) {
			var emailProductList = new ProductList(emailProducts);

			var emailModel = new EmailModel(emailData);

			var viewData = {
				eventManager : this.eventManager,
				products : emailProductList,
				model : emailModel,
				formConfig : self.models.applicationConfig
			};

			// not always required
			if (this.models.accountHolder) {
				viewData.accountHolder = this.models.accountHolder;
			}

			var emailModalView = new EmailModalView(viewData);

			this.modal.show(emailModalView);
		}
	};

	ProductListComponent.prototype.sendEmail = function(request) {
		var self = this;

		var emailModel = new EmailModel(request);

		emailModel.save({}, {
			success : function(model, response, options) {
				// close email modal
				self.modal.close();

				var confirmationData = {};

				if (response.result) {
					confirmationData.success = 'success';
					confirmationData.message = "Your email was successfully sent";
				} else {
					confirmationData.success = 'failure';
					confirmationData.message = "An error occurred. Your email was not sent";
				}

				self.modal.show(new ConfirmationView({
					eventManager : self.eventManager,
					data : confirmationData
				}));
			},
			error : function() {
				alert('An unexpected error has occurred. The email could not be sent.');
				self.modal.close();
			}
		});
	};

	return ProductListComponent;
});