/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'text!html/shopping/components/header/view.header.welcome.internal.dust' ], function(require, Backbone,
		templateEngine, dustHelpers, connecture, headerTemplate) {

	templateEngine.load(headerTemplate, "view.header.welcome.internal");

	HeaderWelcomeView = connecture.view.Base.extend({
		template : "view.header.welcome.internal",
		events : {
			'click a._myAccount' : 'toMyAccount',
			'click a._accountSettings' : 'toAccountSettings',
			'click a._exit' : 'exit',
			'click a._logout' : 'logout'
		},
		initialize : function(options) {
			this.data = options.data;
			this.actor = options.actor;
			this.eventManager = options.eventManager;
		},
		render : function() {
			var data = this.data;
			data.actor = this.actor.toJSON();
			var renderedHTML = this.renderTemplate({
				data : this.data
			});

			$(this.el).html(renderedHTML);

			return this;
		},
		toMyAccount : function(event) {
			event.preventDefault();
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.configData.dashboardURL
			});
		},
		toAccountSettings : function(event) {
			event.preventDefault();
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.configData.userProfileURL
			});
		},
		exit : function(event) {
			event.preventDefault();
			this.eventManager.trigger('exit', {
				key : 'back',
				location : this.options.configData.exit,
				params : {
					viewCase : true
				}
			});
		},
		logout : function(event) {
			event.preventDefault();
			this.eventManager.trigger('view:logout');
		}
	});

	return HeaderWelcomeView;
});