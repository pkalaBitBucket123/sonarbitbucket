/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'text!html/shopping/components/header/view.app.header.dust' ], function(require, Backbone,
		templateEngine, dustHelpers, connecture, headerTemplate) {

	templateEngine.load(headerTemplate, "view.app.header");

	var AppHeaderView = connecture.component.core.View.extend({
		template : "view.app.header",
		className : '_component third-section',
		events : {},
		initialize : function() {
			connecture.component.core.View.prototype.initialize.call(self);
		},
		render : function() {
			var self = this;

			var renderedHTML = this.renderTemplate({});

			$(self.el).html(renderedHTML);

			return this;
		}
	});

	return AppHeaderView;
});