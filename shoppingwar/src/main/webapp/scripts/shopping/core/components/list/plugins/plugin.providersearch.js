/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/plugin' ], function(require, Backbone, templateEngine, Plugin) {

	function findProviderAnswerData(widgets, account) {
		var retVal = null;
		if (widgets) {
			var questionId = null;
			// At this point it is an array of models
			var providerWidget = widgets.getWidget('ProviderLookup', 'widget');

			if (providerWidget !== null) {
				questionId = providerWidget.get('config').questionRefId;
			}

			// make sure provider search is configured
			if (questionId !== null) {
				// Now we need to figure out if they have any answers already
				if (account.questions) {
					_.each(account.questions, function(answer) {
						if (questionId === answer.questionRefId) {
							retVal = answer;
						}
					});
				}
			}
		}

		return retVal;
	}

	var ProviderSearchPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'component';
		this.id = 'ProviderSearch';
		// this is specific to component plugins, tells us which components
		// to plug into
		this.pluggedIds = [ 'b718c2e2-2119-4053-ad7b-99b1daa45be6' ];
	};

	ProviderSearchPlugin.prototype = Object.create(Plugin.prototype);

	ProviderSearchPlugin.prototype.initialize = function(options) {
		var self = this;
		Plugin.prototype.initialize.call(this, options);

		// This component handles some global events
		this.subscriptions = _.extend({}, this.subscriptions, {
			'component:provider:changed' : function(data) {
				self.processProviders.call(this, data, true);
			}
		});

		this.publish = _.extend({}, this.publish, {
			'view:provider.search' : 'component:provider.search'
		});

	};

	ProviderSearchPlugin.prototype.addEvents = function() {
		// do not trigger a reset here
		// On start up we need to get the data to be able to handle this
		// When provider search is answered it sends us the data,
		// but here we need to figure out what the previous answers are
		// then pass it in
		this.eventManager.on("initialize:after", function() {

			var providerAnswerData = findProviderAnswerData(this.plugged.models.widgets, this.plugged.models.account.toJSON());

			if (providerAnswerData !== null) {
				this.processProviders.call(this.plugged, providerAnswerData, false);
			}
		}, this);
		// This will handle when we have fetched another set of
		// plans from the server
		this.eventManager.on('component:product:list:reset', function() {
			var providerAnswerData = findProviderAnswerData(this.plugged.models.widgets, this.plugged.models.account.toJSON());

			if (providerAnswerData !== null) {
				this.processProviders.call(this.plugged, providerAnswerData, false);
			}
		}, this);
	};

	ProviderSearchPlugin.prototype.processProviders = function(data, triggerReset) {
		// the data will be the provider search response which
		// will contain everything necessary to figure out how to score
		// and stuff

		// store selected providers
		var providers = [];
		// These are going to be the selected providers
		_.each(data.value, function(providerId) {
			// now we need the provider information for each one
			_.each(data.data.providerSearchResults, function(provider) {
				if (providerId === provider.xrefid) {
					providers.push(provider);
				}
			});
		});

		// Now loop through each model
		_.each(this.models.plans.models, function(product) {
			// Grab the networks for each one
			var networks = product.get('networks');
			// This will be the networks object we set on the
			// product. It will indicate whether it is in
			// or out of network
			var planNetworks = [];
			var hasInNetworks = false;
			var hasOutNetworks = false;
			// eh only processing now if it is an array
			if (_.isArray(networks)) {
				// loop through each provider
				_.each(providers, function(provider) {
					if (_.contains(networks, provider.xrefid)) {
						hasInNetworks = true;
						planNetworks.push({
							provider : provider,
							inNetwork : true
						});
					} else {
						hasOutNetworks = true;
						planNetworks.push({
							provider : provider,
							inNetwork : false
						});
					}
				});
			} else {
				hasOutNetworks = true;
				// if empty, well then everything is out of network
				_.each(providers, function(provider) {
					planNetworks.push({
						provider : provider,
						inNetwork : false
					});
				});
			}

			product.set({
				'providers' : planNetworks,
				'hasInNetworks' : hasInNetworks,
				'hasOutNetworks' : hasOutNetworks
			}, {
				silent : true
			});

		});

		// Now that we have the list of providers, we need to
		// check to see if the product list's network is there

		// Update the display
		if (triggerReset) {
			this.models.plans.trigger('updated');
		}
	};

	return ProviderSearchPlugin;
});