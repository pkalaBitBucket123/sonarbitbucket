/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: some day add widgets :)
 */
define([ 'require', 'backbone', 'common/workflows/workflow.item', 'common/widget.factory', 'components/account/views/view.account.summary', 'components/account/views/view.account.summary.bar',
		'shopping/common/collections/collection.widget', 'components/account/workflows/views/view.workflow.item.summary' ], function(require, Backbone, WorkflowItem, WidgetFactory, SummaryView,
		SummaryBarView, WidgetList, SummaryWorkflowTemplate) {

	var SummaryWorkflowItem = function(configuration, widgets, options) {
		// this is the prefiltered list of widgets, mainly used for questions
		this.baseWidgets = widgets.getFilteredWidgets(configuration.get('widgets'));
		WorkflowItem.call(this, configuration, widgets, options);

	};

	SummaryWorkflowItem.prototype = Object.create(WorkflowItem.prototype);

	SummaryWorkflowItem.prototype.start = function(options) {
		if (options && options.widget === 'Recommended') {
			this.summaryDetailsVisible = true;
			this.renderSummary();
			this.renderSummaryBar();
		} else {

			this.summaryDetailsVisible = false;
			this.renderSummaryBar(options);
		}

	};

	SummaryWorkflowItem.prototype.initialize = function() {
		var self = this;

		WorkflowItem.prototype.initialize.call(this);

		this.eventManager.on('view:summary:close', function(response) {
			this.summaryDetailsVisible = false;
			this.regionManager.content.close();
			this.renderSummaryBar();
		}, this);

		this.eventManager.on('view:summary:show:questions', function() {
			this.options.eventManager.trigger('workflow:item:done', 'Questions');
		}, this);

		this.eventManager.on('view:summary:widget:clear', function(widgetId, memberRefId) {

			// we need to clear the account answer

			// building the widget so it can handle its own clearing.

			// TODO: refactor the summary so it builds all of the widgets here
			// it needs to render, then we can pull the active one and don't
			// have to build and destroy each time
			var widgetModel = this.widgets.getWidget(widgetId);

			var widgetFactory = new WidgetFactory();
			// we have to use
			var widget = widgetFactory.getWidget(widgetModel.get('widgetType'), {
				"widgetModel" : widgetModel,
				"attributes" : this.buildWidgetData(),
				"eventManager" : this.eventManager
			});

			widget.start();

			widget.clearQuestionResponse(memberRefId);
			// destroy it because we don't need it anymore
			widget.destroy();

			// trigger an event to indicate that the account has been updated
			this.options.eventManager.trigger('component:accountUpdated', "questions");

			// then render the summary view again
			this.renderSummary();

		}, this);

		this.eventManager.on('view:summary:edit', function() {
			this.summaryDetailsVisible = true;

			// for tracking / analytics
			this.eventManager.trigger('workflow:item:summary:show');
			this.renderSummary();
			this.renderSummaryBar();
		}, this);

		// bubble these up as well - TODO: I don't like this here but need
		// it for some internal events.. REFACTOR LATER
		this.eventManager.on('widget:provider:changed', function(response) {
			self.options.eventManager.trigger('widget:provider:changed', response);
		});
		this.eventManager.on('widget:estimateCosts', function(response) {
			self.options.eventManager.trigger('widget:estimateCosts', response);
		});
	};

	/**
	 * Overriding here because we need a new template for this workflow item. This will work for now but could be better
	 * 
	 * @param options
	 * @returns {SummaryWorkflowTemplate}
	 */
	SummaryWorkflowItem.prototype.render = function(options) {
		return new SummaryWorkflowTemplate();
	};

	SummaryWorkflowItem.prototype.renderSummary = function() {
		var models = this.options.models;

		var questionFilters = [];

		var widgets = this.widgets;

		this.setWorkflowStep('Summary');

		if (this.options.session.get("activeWorkflow")) {
			questionFilters = this.options.session.get("workflowFilters");
		}

		// filter down the widgets first based on the filters from
		// the session which come from the method page technically
		if (questionFilters && !_.isEmpty(questionFilters)) {
			widgets = this.baseWidgets.getFilteredWidgets(questionFilters);
		}

		var summaryView = new SummaryView({
			"allQuestions" : widgets,
			"questionnaires" : models.workflowItems.get('Questions'),
			"costEstimatorConfigModel" : models.costEstimatorConfig,
			"account" : models.account,
			'componentId' : this.options.history.id,
			eventManager : this.eventManager
		});

		this.regionManager.content.show(summaryView);

		// if (options && options.trigger) {
		// set history for the summary
		var componentId = this.options.history.id;
		var workflowId = this.configuration.get("id");
		Backbone.history.navigate('view/' + componentId + '/' + workflowId + '/Recommended');
		// }

	};

	SummaryWorkflowItem.prototype.renderSummaryBar = function(options) {
		// for tracking / analytics
		this.eventManager.trigger('workflow:item:summary:show');
// by rsingh for user stories 228 & 230 
		this.regionManager.summaryBar.show(new SummaryBarView({
			eventManager : this.eventManager,
			summaryDetailsVisible : this.summaryDetailsVisible,
			coverageType : this.options.models.account.get("coverageType"),
			questionsAnswered : this.options.models.account.get('questions') && !_.isEmpty(this.options.models.account.get('questions'))
		}));
	};

	return SummaryWorkflowItem;

});