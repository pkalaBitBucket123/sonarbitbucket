/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/header/view.header.producer.dust' ], function(require, Backbone, templateEngine,
		connecture, template) {

	templateEngine.load(template, 'view.header.producer');

	ProducerView = connecture.view.Base.extend({
		template : 'view.header.producer',
		events : {},
		initialize : function() {

		},
		render : function() {
			var self = this;
			
			var data = this.model.toJSON();
			if (data.contact.phonenumber!=null)
			{
				data.contact.phonenumber = data.contact.phonenumber.replace( /(\d{3})(\d{4})/, '$1-$2' );
			} else {
				data.contact.phonenumber = "";
			}
			var renderedHTML = this.renderTemplate({
				data : this.model.toJSON()
			});

			$(self.el).html(renderedHTML);

			return this;
		}
	});

	return ProducerView;
});