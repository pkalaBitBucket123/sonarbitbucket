/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'components/account/widgets/views/view.widget.enrollmentdate' ], function(require, Backbone, Widget, EnrollmentDateView) {

	var EnrollmentDateWidget = function(options) {
		Widget.call(this, options);
	};

	EnrollmentDateWidget.prototype = Object.create(Widget.prototype);

	EnrollmentDateWidget.prototype.initialize = function() {

		var self = this;
		this.demographicsData = {};

		var account = self.attributes.account;

		// TODO | get from common data model created in the WORKFLOW ITEM

		// special enrollment date data
		if (account && account.get("eventType")) {
			self.demographicsData.eventType = account.get("eventType");
			self.demographicsData.eventDate = account.get("eventDate");
		}

		// non-special enrollment date data
		if (account.get("effectiveMs")) {
			self.demographicsData.effectiveMs = account.get("effectiveMs");
			self.demographicsData.effectiveDate = self.formatDate(new Date(account.get("effectiveMs")), "{Month:2}/{Date:2}/{FullYear}");
		}
	};

	EnrollmentDateWidget.prototype.canRender = function() {
		return true;
	};

	EnrollmentDateWidget.prototype.formatDate = function(dateInstance, formatString) {
		
		return formatString.replace( // Replace all tokens
		/{(.+?)(?::(.*?))?}/g, // {<part>:<padding>}
		function(v, // Matched string (ignored, used as local var)
		c, // Date component name
		p // Padding amount
		) {
			for (v = dateInstance["get" + c]() // Execute date component getter
					+ /h/.test(c) // Increment Mont(h) components by 1
					+ ""; // Cast to String
			v.length < p; // While padding needed,
			v = 0 + v)
				; // pad with zeros
			return v; // Return padded result
		});
	};

	EnrollmentDateWidget.prototype.getView = function(demographicsData) {

		this.view = null;

		var viewData = this.attributes;
		viewData.demographicsConfig = this.model;
		viewData.eventManager = this.eventManager;

		// if something was passed in, use that;
		// otherwise grab whatever was stored on initialization.
		if (demographicsData) {
			viewData.demographicsData = demographicsData;
		} else {
			viewData.demographicsData = this.demographicsData;
		}

		this.view = new EnrollmentDateView(viewData);

		return this.view;
	};

	EnrollmentDateWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return EnrollmentDateWidget;
});