/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: The one thing we will need to decide is the best way to pull in components dynamically, to get things working I am putting them here
 * 
 * We can actually update this to use something like this:
 * 
 * http://requirejs.org/docs/whyamd.html#sugar
 * 
 * Which might help simiply this
 */
define([
// base
'require',
// Application
'common/application/application',
// Application view - TODO this should be brought in via the application.js
'common/views/application', 'components/loader/views/view.component.loader',
// Component - account
'components/account/component.account',
// Component - header
'components/menu/component.menu',
// Component - product compare
'components/list/component.product.list',
// Component - product decide
'components/cart/component.cart',
// Component - header
'components/header/component.app.header',
// Component - appHeader
'components/header/component.header',
// Component - Welcome
'components/loader/component.loader',
// router
'common/application/router',
//
'common/application/plugins/plugin.component',
//
'components/list/plugins/plugin.filters',
//
'components/list/plugins/plugin.overmind',
//
'components/list/plugins/plugin.costestimator',
//
'components/list/plugins/plugin.providersearch',
//
'components/list/plugins/plugin.subsidy',
//
	//<BCBSNE US=RIDERS FOR PROPOSAL MI>
 'components/quote/component.quote',
'components/list/plugins/plugin.product.availability',

// shims
'es5-shim', 'common/backbone/backbone.sync' ], function(require, Application, ApplicationView, ComponentLoaderView, AccountComponent, ProgressComponent, ProductListComponent, ProductDecideComponent,
		AppHeaderComponent, HeaderComponent, WelcomeComponent, Router, ComponentPlugin, FilterPlugin, OvermindPlugin, CostEstimatorPlugin, ProviderSearchPlugin, SubsidyPlugin,FinalizeQuoteComponent,
		ProductAvailabilityPlugin) {
	//</BCBSNE US=RIDERS FOR PROPOSAL MI>
	// Define our application and set configuration
	// We are saying that this application uses the
	// component plugin

	var application = new Application({
		uses : [ ComponentPlugin, FilterPlugin, OvermindPlugin, CostEstimatorPlugin, ProviderSearchPlugin, SubsidyPlugin, ProductAvailabilityPlugin ],
		Layout : ApplicationView,
		loading : {
			display : 'after',
			View : ComponentLoaderView,
			Component : WelcomeComponent
		},
		regions : {
			'pageHeader' : '._componentRegion._pageHeader',
			'appHeader' : '._componentRegion._appHeader',
			'progress' : '._componentRegion._progress',
			'account' : '._componentRegion._acccount',
			'list' : '._componentRegion._list',
			'cart' : '._componentRegion._cart',
			'finalizeQuote' : '._componentRegion._finalizeQuotePlan'
		}
	});

	// After we are done initializing, we will start our history
	// although this might get moved
	application.eventManager.on("initialize:after", function() {
		// Do nothing for now, Backbone History now starts the app for us
	});

	// Since we are using the component plugin, let's add our
	// components to the application

	// Passing in where each component should be rendered to.
	// The application will create the regions for us based
	// on these settings
	application.component(AppHeaderComponent, {
		layout : {
			region : 'appHeader'
		}
	});
	application.component(HeaderComponent, {
		layout : {
			region : 'pageHeader'
		},
		engage : 'initialization'
	});

	application.component(ProgressComponent, {
		layout : {
			region : 'progress'
		}
	});
	application.component(AccountComponent, {
		layout : {
			region : 'account'
		}
	});
	application.component(ProductListComponent, {
		layout : {
			region : 'list'
		}
	});
	application.component(ProductDecideComponent, {
		layout : {
			region : 'cart',
			render : {
				startup : 'hide'
			}
		}
	});
	application.component(FinalizeQuoteComponent, {
		layout : {
			region : 'finalizeQuote',
			render : {
				startup : 'hide'
			}
		}
	});
	
	// Create our router
	new Router({
		application : application
	});

	return application;
});