/**
 * Copyright (C) 2012 Connecture
 * 
 */
requirejs.config({
	paths : {
		'html/individual' : '../html/shopping/individual'
	},
	map : {
		'*' : {

			'html/shopping/components/list/view.product.summary.dust' : 'html/individual/components/list/view.product.summary.dust',
			'html/shopping/components/list/view.product.compare.summary.dust' : 'html/individual/components/list/view.product.compare.summary.dust',
			'html/shopping/components/list/plansDisclaimer.dust' : 'html/individual/components/list/plansDisclaimer.dust',
			'html/shopping/components/list/plansExtraInfoDisclaimer.dust' : 'html/individual/components/list/plansExtraInfoDisclaimer.dust',
			'html/shopping/components/list/view.product.detail.dust' : 'html/individual/components/list/view.product.detail.dust',
			'html/shopping/components/header/view.header.links.internal.dust' : 'html/individual/components/header/view.header.links.internal.dust',
			'html/shopping/components/view.component.loader.dust' : 'html/individual/components/view.component.loader.dust',
			'html/shopping/components/header/view.header.welcome.internal.dust' : 'html/individual/components/header/view.header.welcome.internal.dust',
			'html/shopping/components/loader/view.loader.summary.dust' : 'html/individual/components/loader/view.loader.summary.dust',
			'html/shopping/components/cart/view.cart.summary.dust' : 'html/individual/components/cart/view.cart.summary.dust',
			'html/shopping/components/header/view.header.quotereceipt.dust' : 'html/individual/components/header/view.header.quotereceipt.dust',
			// email
			'html/shopping/components/list/email/view.email.dust' : 'html/individual/components/list/email/view.email.dust',
			'components/list/views/email/view.modal.email' : 'shopping/individual/components/list/views/email/view.modal.email',
			// anonymous is using the new format, the rest of the shopping is using the old for now
			'components/list/models/model.email' : 'shopping/common/models/model.email',
			'components/account/widgets/views/view.widget.demographics' : 'shopping/individual/components/account/widgets/views/view.widget.demographics'
		},
        'app/app.shopping' : {
			'components/header/component.header' : 'shopping/individual/components/header/component.header',
			'components/account/component.account' : 'shopping/individual/components/account/component.account',
			'components/cart/component.cart' : 'shopping/individual/components/cart/component.cart',
				//<BCBSNE US=RIDERS FOR PROPOSAL MI>
			'components/list/component.product.list' : 'shopping/individual/components/list/component.product.list',
			'components/quote/component.quote' : 'shopping/individual/components/quote/component.quote'
			//</BCBSNE US=RIDERS FOR PROPOSAL MI>
		},
		'workflow/definition' : {
			'components/account/workflows/workflow.item.whoscovered' : 'shopping/individual/components/account/workflows/workflow.item.whoscovered'
		},
		'widget/definition' : {
			'components/account/widgets/widget.demographics' : 'shopping/individual/components/account/widgets/widget.demographics',
			'components/account/widgets/widget.costestimator' : 'shopping/individual/components/account/widgets/widget.costestimator'
		},
		'common/application/proxy.filter' : {
			'common/application/datamodel' : 'shopping/individual/app/datamodel'
		},
		'components/account/views/view.account.summary' : {
			'components/account/views/view.account.questionnaire.members.summary' : 'shopping/individual/components/account/views/view.account.questionnaire.members.summary'
		},
		'components/loader/component.loader' : {
			// We need to process additional data for the loader
			'components/loader/views/view.loader.summary' : 'shopping/individual/components/loader/views/view.loader.summary'
		},
		'components/header/component.app.header' : {
			'components/header/views/view.app.header.logo' : 'shopping/individual/components/header/views/view.app.header.logo'
		},
		'components/header/component.header' : {
			'components/header/views/view.header.links.internal' : 'shopping/individual/components/header/views/view.header.links.internal'
		},
		'components/loader/component.loader' : {
			'components/loader/views/view.loader.summary' : 'shopping/individual/components/loader/views/view.loader.summary'
		}
	},
	config : {
		'common/application/proxy.filter' : {
			context : 'individual',
			stores : [ {
				type : 'consumerDemographics',
				items : [ 'accountHolder', 'producer', 'specialEnrollmentEventTypes' ],
				params : [ 'producerToken' ]
			}, {
				type : 'account',
				items : [ 'account' ],
				queue : [ 'post' ]
			}, {
				type : 'quote',
				items : [ 'quote' ],
				queue : [ 'quote' ]
			}, {
				type : 'rulesEngine',
				items : [ 'rulesEngine' ]
			}, {
				type : 'costEstimatorConfig',
				items : [ 'costEstimatorConfig' ]
			}, {
				type : 'costEstimatorCosts',
				items : [ 'costEstimatorCosts' ]
			}, {
				type : 'products',
				items : [ 'plans', 'planConfig', 'productLines' ]
			}, {
				type : 'providerSearch',
				items : [ 'providers' ]
			}, {
				type : 'povertyIncomeLevel',
				items : [ 'povertyIncomeLevel' ]
			}, {
				type : 'enrollmentEffectiveDate',
				items : [ 'enrollmentEffectiveDate' ]
			}, {
				type : 'specialEnrollmentEventDateValidation',
				items : [ 'specialEnrollmentEventDateValidation' ]
			}, {
				type : 'validZipCodeCountyData',
				items : [ 'validZipCodeCountyData' ]
			}, {
				type : 'applicationConfig',
				items : [ 'applicationConfig', 'filters', 'questionEvaluations', 'widgets', 'workflowItems', 'workflow' ]
			}, {
				type : 'actor',
				items : [ 'actor' ]
			}, {
				type : 'demographicsValidation',
				items : [ 'demographicsValidation' ]
			}, {
				type : 'subsidyUsage',
				items : [ 'subsidyUsage' ]
			}, {
				type : 'availability',
				items : [ 'availability' ]
			} ]
		},
		'common/application/proxy' : {
			queue : true,
			smartQueue : false,
			requests : {
				'component:get' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					type : 'get',
					filter : 'hub',
					params : [ 'transactionId', 'groupId' ]
				},
				'component:persist' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					type : 'post',
					filter : 'hub',
					params : [ 'transactionId', 'groupId' ]
				},
				'logout' : {
					type : 'redirect',
					location : 'Logout.action',
				},
				'login' : {
					type : 'redirect',
					location : 'LoginEdit.action'
				},
				'unauthorized' : {
					type : 'redirect',
					location : 'UnauthorizedHandlerAction.action'
				},
				'checkout' : {
					type : 'redirect',
					location : 'checkoutIFP.action',
					namespace : 'shopping',
					params : [ 'transactionId', 'groupId', 'producerToken' ]
				},
				'checkoutEmployee' : {
					type : 'redirect',
					location : 'enroll.action',
					namespace : 'shopping',
					params : [ 'transactionId', 'groupId', 'context' ]
				},
				'back' : {
					type : 'redirect',
					location : '',
					params : [ 'transactionId', 'groupId', 'context' ]
				},
				'email' : {
					location : 'generateLead.action',
					namespace : 'shopping',
					type : 'post',
					params : [ 'context', 'transactionId', 'producerToken' ]
				},
				'ffm' : {
					type : 'redirect',
					location : '',
					namespace : 'shopping',
					params : [ 'transactionId', 'context' ]
				}
			}
		},
		'components/loader/component.loader' : {
			// TODO: This is requiring widgets because the enrollment data is put
			// into the demographics config, we need to parse that out
			items : [ {
				alias : 'widgets',
				dependency : true,
			}, {
				alias : 'account',
				dependency : true,
			}, {
				alias : 'quote',
				dependency : true
			} ]
		},
		'components/header/component.app.header' : {
			items : []
		},
		'components/header/component.header' : {
			items : [ {
				alias : 'applicationConfig',
				dependency : true
			}, {
				alias : 'accountHolder',
				dependency : true
			}, {
				alias : 'account',
				dependency : true
			}, {
				alias : 'actor',
				dependency : true
			}, {
				alias : 'plans',
				dependency : true
			}, {
				alias : 'quote',
				dependency : true
			}, {
				alias : 'producer',
				dependency : true
			} ]
		},
		'components/account/component.account' : {
			id : 'cbb55234-1fac-4ab0-920c-605d9ac0c032',
			items : [ {
				alias : 'applicationConfig',
				dependency : true
			}, {
				alias : 'accountHolder',
				dependency : true,
			}, {
				alias : 'widgets',
				dependency : true,
			}, {
				alias : 'costEstimatorConfig',
				dependency : true,
			}, {
				alias : 'specialEnrollmentEventTypes',
				dependency : true,
			}, {
				alias : 'productLines',
				dependency : true,
			}, {
				alias : 'account',
				dependency : true
			}, {
				alias : 'actor',
				dependency : true
			}, {
				alias : 'workflowItems',
				dependency : true
			}, {
				alias : 'workflow',
				dependency : true
			}, {
				alias : 'quote',
				dependency : true,
			} ],
			triggerComplete : 'summary', // whoscovered, method, summary
			component : {
				showHeader : false,
			},
			views : {
				// TODO: will this work for long term?
				'widgets' : {
					showNavigation : true
				}
			}
		},
		'components/list/component.product.list' : {
			id : 'b718c2e2-2119-4053-ad7b-99b1daa45be6',
			items : [ {
				alias : 'plans',
				dependency : true,
			}, {
				alias : 'planConfig',
				dependency : true,
			}, {
				alias : 'filters',
				dependency : true
			}, {
				alias : 'productLines',
				dependency : true,
			}, {
				alias : 'widgets',
				dependency : true,
			}, {
				alias : 'account',
				dependency : true,
				share : true
			}, {
				alias : 'quote',
				dependency : true,
			}, {
				alias : 'applicationConfig',
				dependency : true
			}, {
				alias : 'accountHolder',
				dependency : true
			}, {
				alias : 'actor',
				dependency : true
			}, {
				alias : 'questionEvaluations',
				dependency : true,
			} ],
			// set process to realtime ...go out fetch plans and update product list
			// set displayPlans to complete...
			startup : 'hide', // show
			process : 'realtime', // realtime (whenever an event comes in, process the data), complete (only process the data for a complete event)
			show : 'complete', // startup, available (once they get fetched), complete (event)
			defaultView : 'ProductList'
		}
	}
});