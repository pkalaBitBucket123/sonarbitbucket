/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */
define([ 'module', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/component.account', ], function(module, Backbone, templateEngine, dustHelpers,
		connecture, BaseAccountComponent) {

	var AccountComponent = function(configuration) {
		BaseAccountComponent.call(this, configuration);
	};

	AccountComponent.prototype = Object.create(BaseAccountComponent.prototype);

	/**
	 * TODO: Now that we keep adding more "hardcoded" views we need a way to set these up before handle and then dynamically render these views.
	 */
	AccountComponent.prototype.registerEvents = function() {
		// call our base register
		BaseAccountComponent.prototype.registerEvents.call(this);
	};

	AccountComponent.prototype.updateMemberProviders = function(providerWidgetData) {
		var self = this;

		var memberProviders = [];

		// provider gives member data with providers (networkIds) already stored
		// just need to format them and store them into the account members
		_.each(providerWidgetData.members, function(member) {

			var memberProvider = {};
			memberProvider.memberId = member.memberRefName;
			var providers = new Array();

			_.each(member.value, function(memberNetwork) {
				_.each(providerWidgetData.data.providerSearchResults, function(searchResult) {
					if (searchResult.xrefid == memberNetwork) {
						providers[providers.length] = searchResult;
					}
				});
			});

			memberProvider.providers = providers;

			memberProviders[memberProviders.length] = memberProvider;
		});

		_.each(memberProviders, function(memberProviderData) {
			// find the corresponding member record in the account
			var accountMembers = self.models.account.get("members");
			_.each(accountMembers, function(accountMember) {
				if (accountMember.memberRefName == memberProviderData.memberId) {
					accountMember.providers = memberProviderData.providers;
				}
			});
		});

		// save the account member providers
		this.models.account.save();
	};

	return AccountComponent;
});