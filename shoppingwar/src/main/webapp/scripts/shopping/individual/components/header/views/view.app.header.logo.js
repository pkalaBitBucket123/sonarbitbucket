/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'components/header/views/view.app.header.logo' ], function(require, Backbone, BaseView) {

	AppHeaderLogoView = BaseView.extend({
		initialize : function(options) {
			this.logoData = {};
			this.logoData.name = '';
			// TODO: Should probably come from configuration
			//Image name changed
			this.logoData.logoPath = '../images/carrierlogoNE.png';
		}
	});

	return AppHeaderLogoView;
});