/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'backbone.validation', 'templateEngine', 'shopping/common/models/model.email', 'common/views/base', 'text!html/shopping/components/list/email/view.email.dust',
		'common/validation/validation-extensions', 'jquery.maskedInput' ], function(require, Backbone, BackboneValidation, templateEngine, EmailFormModel, BaseView, template, commonValidation,
		maskedInput) {

	templateEngine.load(template, "view.email");

	EmailModalView = BaseView.extend({
		template : "view.email",
		events : {
			'click ._btnEmail' : 'emailPlan',
			'mousedown ._btnEmail' : 'emailPlanDown',
			'mousedown ._btnCancel' : 'closeModal',
			'change input' : 'updateField'
		},
		initialize : function(options) {
			var self = this;

			this.accountHolder = options.accountHolder;
			this.formConfig = options.formConfig;
			this.products = options.products;
			// set a variable for tracking whether the Email Plan(s)
			// button has been clicked (used to track submit when
			// multiple conflicting events are triggered)
			self.submitClicked = false;

			// create the form validation model
			var emailFormModel = new EmailFormModel(self.model.toJSON());

			// add validation pulled in from configuration
			self.setConfiguredValidation(emailFormModel);

			self.model = emailFormModel;

			var toEmail = '';
			if (this.accountHolder) {
				toEmail = this.accountHolder.get("email");
			}
			self.model.set({
				"toEmail" : toEmail
			}, {
				validate : false
			});

			self.formErrorSetId = "ANONYMOUS_EMAIL_FORM";
			self.formErrors = {};

			// bind validation
			Backbone.Validation.bind(this, {
				forceUpdate : true,
				valid : function(view, attr) {

					var errorObj = {};
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.removeErrorMessages(errorObj);
					view.$('[name~="' + attr + '"]').parent().removeClass('hasErrors').removeAttr('data-error');
				},
				invalid : function(view, attr, error) {

					self.submitClicked = false;
					
					var errorObj = {};
					errorObj.errorMsg = error;
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.addErrorMessage(errorObj, attr);
					view.$('[name~="' + attr + '"]').parent().addClass('hasErrors').attr('data-error', error);
				}
			});

		},
		emailPlanDown : function(event) {
			var self = this;
			self.submitClicked = true;
		},
		render : function() {

			var self = this;

			// build model, including errors

			var dustJSON = {};
			dustJSON.data = self.model.toJSON();
			dustJSON.data.formConfig = this.formConfig.toJSON();
			dustJSON.data.products = this.products.toJSON();

			var renderedHTML = this.renderTemplate({
				data : dustJSON
			});

			$(self.el).html(renderedHTML);

			self.postRender();

			return this;
		},
		postRender : function() {

			var self = this;

			if (self.model.get("toEmail")) {
				// if the model already has a toEmail value,
				// set this as the non-default
				$('#_toEmail', self.el).val(self.model.get("toEmail"));
			} else {
				// bind default text
				self.bindDefaultText($(self.el).find('#_toEmail'));
			}

			self.bindDefaultText($(self.el).find('#_toPhone'));

			// add phone masking
			var phoneMaskField = $('#_toPhone', self.el);

			// apply mask
			phoneMaskField.mask('?(999) 999-9999', {
				placeholder : " "
			});
		},
		emailPlan : function(event) {

			var self = this;

			// 1. reset form-level validation (rules engine validation results)
			self.removeErrorMessagesForErrorSet(self.formErrorSetId);

			// 2. check form validation
			var formValid = self.model.isValid(true);
			if (formValid) {

				// get form fields
				var emailData = {};

				// move attributes from the view into the model
				emailData.email = $(self.el).find('#_toEmail').hasClass('default-text') ? '' : $(self.el).find('#_toEmail').val();
				emailData.phone = $(self.el).find('#_toPhone').hasClass('default-text') ? '' : $(self.el).find('#_toPhone').val();
				emailData.firstName = $(self.el).find('#_firstName').hasClass('default-text') ? '' : $(self.el).find('#_firstName').val();
				emailData.lastName = $(self.el).find('#_lastName').hasClass('default-text') ? '' : $(self.el).find('#_lastName').val();

				emailData.callMe = $(self.el).find('#_callMe').is(":checked");
				emailData.futureEmails = $(self.el).find('#_futureEmails').is(":checked");

				self.emailPlans(emailData);
			}
		},
		closeModal : function(event) {
			var self = this;
			self.options.eventManager.trigger('view:closeEmailModal');
		},
		removeErrorMessages : function(errorObject) {

			var self = this;

			var modelId = errorObject.modelId;

			if (self.formErrors[modelId]) {

				var modelErrors = self.formErrors[modelId];

				var newModelErrors = new Array();

				if (modelErrors) {
					// see if the current error is already there

					for ( var i = 0; i < modelErrors.length; i++) {
						if (modelErrors[i].attr != errorObject.attr) {
							newModelErrors[newModelErrors.length] = modelErrors[i];
						}
					}
				}

				self.formErrors[modelId] = newModelErrors;
			}

			self.displayErrors();
		},
		addErrorMessage : function(errorObject) {

			var self = this;

			var modelId = errorObject.modelId;

			// try to find existing errors for the modelId
			if (self.formErrors[modelId]) {
				// get array of errors for the model
				var modelErrors = self.formErrors[modelId];
				if (modelErrors) {
					// see if the current error is already there
					var found = false;
					_.each(modelErrors, function(modelError) {
						if (modelError.errorMsg == errorObject.errorMsg) {
							found = true;
						}
					});
					if (!found) {

						var modelErrorsObject = {};
						modelErrorsObject.attr = errorObject.attr;
						modelErrorsObject.errorMsg = errorObject.errorMsg;

						modelErrors[modelErrors.length] = errorObject;
					}

				}
			} else {
				modelErrors = new Array();

				var modelErrorsObject = {};
				modelErrorsObject.attr = errorObject.attr;
				modelErrorsObject.errorMsg = errorObject.errorMsg;

				modelErrors[0] = modelErrorsObject;
				self.formErrors[modelId] = modelErrors;
			}

			self.displayErrors();

		},
		displayErrors : function() {

			var self = this;

			var errorHTML = ''; // if nothing added this will clear out errors.

			var errorSet = new Array();

			// build error messages for display
			_.each(self.formErrors, function(rowErrors) {
				_.each(rowErrors, function(modelError) {

					var errorFound = false;
					// is this error already in the errorSet? if not, add it here
					_.each(errorSet, function(errorMsg) {
						if (errorMsg == modelError.errorMsg) {
							errorFound = true;
						}
					});

					if (!errorFound) {
						errorSet[errorSet.length] = modelError.errorMsg;
					}
				});
			});

			_.each(errorSet, function(errorMsg) {
				errorHTML += '<div>' + errorMsg + '</div>';
			});

			$("._validationErrors", self.el).html(errorHTML);

			if (errorHTML && errorHTML != '') {
				$("._validationErrors", self.el).show();
			} else {
				// just to be safe here..
				$("._validationErrors", self.el).hide();
			}

		},
		removeErrorMessagesForErrorSet : function(errorSetId) {

			var self = this;

			// would remove all messages associated to a particular
			// logic area based on the id passed in.
			// could be used to remove all errors for a form row model,
			// or the more practical purpose of resetting all
			// errors assigned to the form in general.

			if (self.formErrors && self.formErrors[errorSetId]) {
				delete self.formErrors[errorSetId];
			}
		},
		updateField : function(event) {
			var self = this;

			var fieldValue = $(event.currentTarget).val();
			var fieldName = $(event.currentTarget).attr('name');
			// since this is a dynamic field, we need a set object
			// to set the field name, just set it each time
			var setObject = {};
			setObject[fieldName] = fieldValue;
			self.model.set(setObject, {
				validate : true
			});

			// TODO | hack to ensure that click event is now
			// blown away by the change event; still not sure
			// why that is happening.
			if (self.submitClicked) {
				self.submitClicked = false;
//				self.submitEmailForm();
			}
		}
	});

	EmailModalView.prototype.setConfiguredValidation = function(emailModel) {

		if (emailModel.validation) {

			var emailFormConfig = this.formConfig.toJSON();

			// first name
			if (emailFormConfig.emailFormFirstNameVisible) {
				if (emailFormConfig.emailFormFirstNameRequired) {
					_.extend(emailModel.validation.firstName, {
						required : true
					});
				}
			}

			// last name
			if (emailFormConfig.emailFormLastNameVisible) {
				if (emailFormConfig.emailFormLastNameRequired) {
					_.extend(emailModel.validation.lastName, {
						required : true
					});
				}
			}

			// phone
			if (emailFormConfig.emailFormPhoneVisible) {
				if (emailFormConfig.emailFormPhoneRequired) {
					_.extend(emailModel.validation.phone, {
						required : true
					});
				}
			}
		}
	};

	EmailModalView.prototype.emailPlans = function(emailData) {

		var self = this;

		// build the request data to be used to build the email
		var request = {};

		// form data - lead personal data
		request.email = emailData.email;
		request.phone = emailData.phone;
		request.firstName = emailData.firstName;
		request.lastName = emailData.lastName;

		// form data - preferences
		request.callMe = emailData.callMe;
		request.futureEmails = emailData.futureEmails;

		// TODO : other shopping data (for email formatting, may not be needed for anonymous?)
		request.productData = this.products.toJSON();
		request.costViewCode = self.model.get("costViewCode");

		this.options.eventManager.trigger('view:email:send', request);
	};

	EmailModalView.prototype.submitEmailForm = function() {
		var self = this;

		// 1. reset form-level validation (rules engine validation results)
		self.removeErrorMessagesForErrorSet(self.formErrorSetId);

		// 2. check form validation
		var formValid = self.model.isValid(true);
		if (formValid) {

			// get form fields
			var emailData = {};

			// move attributes from the view into the model
			emailData.email = $(self.el).find('#_toEmail').hasClass('default-text') ? '' : $(self.el).find('#_toEmail').val();

			// configurable fields
			emailData.phone = $(self.el).find('#_toPhone').hasClass('default-text') ? '' : $(self.el).find('#_toPhone').val();
			emailData.firstName = $(self.el).find('#_firstName').hasClass('default-text') ? '' : $(self.el).find('#_firstName').val();
			emailData.lastName = $(self.el).find('#_lastName').hasClass('default-text') ? '' : $(self.el).find('#_lastName').val();
			emailData.callMe = $(self.el).find('#_callMe').hasClass('default-text') ? '' : $(self.el).find('#_callMe').val();
			emailData.futureEmails = $(self.el).find('#_futureEmails').hasClass('default-text') ? '' : $(self.el).find('#_futureEmails').val();

			self.emailPlans(emailData);
		}
	};

	EmailModalView.prototype.bindDefaultText = function(element) {
		var defaultMessageText = $(element).val();
		$(element).addClass('default-text');

		// remove default text on focus
		$(element).focus(function() {
			if ($(element).val() == defaultMessageText) {
				$(element).val('');
				$(element).removeClass('default-text');
			}
		});

		// restore default text on blur, if empty
		$(element).blur(function() {
			if ($(element).val() == '') {
				$(element).addClass('default-text');
				$(element).val(defaultMessageText);
			}
		});
	};

	return EmailModalView;
});