/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */
define([ 'module', 'backbone', 'templateEngine', 'common/component/component', 'components/cart/component.cart' ], function(module, Backbone, templateEngine, connecture, BaseCartComponent) {

	var CartComponent = function(configuration) {
		BaseCartComponent.call(this, configuration);
	};

	CartComponent.prototype = Object.create(BaseCartComponent.prototype);

	/*
	 * Don't build covered members based on product line selections for individual flows, because product lines are not selected through the demographics page as they are in group.
	 */
	CartComponent.prototype.buildCoveredMembers = function(membersJSON) {

		var coveredMembers = new Array();

		_.each(membersJSON, function(member) {
			coveredMembers[coveredMembers.length] = member;
		});

		return coveredMembers;
	};

	CartComponent.prototype.enroll = function() {

		this.eventManager.trigger('exit', {
			key : 'checkout'
		});
	};

	return CartComponent;
});