/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'dust', 'dust-extensions' ], function(require, dust, dustExtensions) {

	dust.helpers['lookupValue'] = function(chk, ctx, bodies, params) {

		var currentContext = ctx.current();

		var propertyFound = true;

		var key = currentContext['key'];
		var defaultValue = currentContext['defaultValue'];

		//var dataType = currentContext['dataType'];

		var mapping = ctx.get("configFieldMapping");
		var modelData = ctx.get("modelData");

		var propertyKey = mapping[key];

		var propertyLayers = propertyKey.split(".");
		var property = modelData[propertyLayers[0]];
		if (property) {
			for ( var i = 1; i < propertyLayers.length; i++) {
				property = property[propertyLayers[i]];
			}
		} else {
			propertyFound = false;
		}

		if (!propertyFound && defaultValue) {
			property = defaultValue;
		}
		if (!property) {
			property = "";
		}

		return chk.write(property);
	};
	
	dust.helpers['hasValue'] = function(chk, ctx, bodies, params) {
		
		var missingValue = false;
		
		var currentContext = ctx.current();

		var modelData = ctx.get("modelData");
		
		var fieldName = currentContext.name;
		var missingValues = ctx.get("missingValues");
		var memberRefName = modelData.memberRefName;
		
		missingValue = (missingValues && missingValues[memberRefName] && missingValues[memberRefName][fieldName]);

		if (missingValue && (missingValue == 'missing')) {
			chk = chk.render(bodies['else'], ctx);			
		} else {
			chk = chk.render(bodies.block, ctx);
		}
	
		return chk;
	};
	
	dust.helpers['selectedOptionLabel'] = function(chk, ctx, bodies, params) {

		var selectedLabel = '';
		
		var currentContext = ctx.current();

		var propertyFound = true;

		var key = currentContext['key'];
		
		var defaultValue = currentContext['defaultValue'];

		var mapping = ctx.get("configFieldMapping");
		var modelData = ctx.get("modelData");

		var propertyKey = mapping[key];

		var propertyLayers = propertyKey.split(".");
		var property = modelData[propertyLayers[0]];
		if (property) {
			for ( var i = 1; i < propertyLayers.length; i++) {
				property = property[propertyLayers[i]];
			}
		} else {
			propertyFound = false;
		}

		if (!propertyFound && defaultValue) {
			property = defaultValue;
		}
		if (!property) {
			property = "";
		}
		
		var options = ctx.current()["options"];
		_.each(options, function(option){			
			if (option['value'] == property) {
				// the property was found in the model with a value
				selectedLabel = option['label'];
			}
		});

		return chk.write(selectedLabel);
	};
	
	dust.helpers['optionSelected'] = function(chk, ctx, bodies, params) {

		var selected = false;

		// need to do 2 things to determine what the selected value should be
		// #1 see if the modelData value is available at all
		// #2 ...if not, see if a defaultSelected option exists

		// then, if a selectedValue is found, and it is the current option,
		// return true.

		// so.. start be looking at the config field to see which property
		// we need to check in the model

		var optionContext = ctx.current();
		var optionValue = optionContext["value"];
		var optionDefault = optionContext["defaultSelection"];

		var selectField = ctx.get("selectField");
		var selectKey = selectField['key'];
		var mapping = ctx.get("configFieldMapping");
		var modelData = ctx.get("modelData");

		var propertyKey = mapping[selectKey];

		var propertyLayers = propertyKey.split(".");
		var property = modelData[propertyLayers[0]];
		for ( var i = 1; i < propertyLayers.length; i++) {
			property = property[propertyLayers[i]];
		}

		if (property) {
			// the property was found in the model with a value
			selected = (optionValue == property);
		} else {
			// the property was NOT found in the model with a value
			// check for a default value in the option
			selected = optionDefault;
		}

		if (selected) {
			return chk.write("selected");
		} else {
			return chk.write("");
		}
	};

});