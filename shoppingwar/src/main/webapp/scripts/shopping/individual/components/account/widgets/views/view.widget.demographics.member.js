/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'shopping/individual/components/account/dust/helpers.demographics',
		'text!html/individual/components/account/view.whoscovered.dust', 'text!html/individual/components/account/view.whoscovered.member.row.dust',
		'text!html/individual/components/account/view.whoscovered.member.row.text.dust', 'text!html/individual/components/account/view.whoscovered.member.row.select.dust', 'jquery.maskedInput' ],
		function(require, Backbone, templateEngine, connecture, demographicsHelpers, template, memberRowTemplate, memberRowTextTemplate, memberRowSelectTemplate, maskedInput) {

			templateEngine.load(memberRowTemplate, "view.whoscovered.member.row");
			templateEngine.load(memberRowTextTemplate, "view.whoscovered.member.row.text");
			templateEngine.load(memberRowSelectTemplate, "view.whoscovered.member.row.select");

			var WhosCoveredMemberView = connecture.view.Base.extend({
				template : "view.whoscovered.member.row",
				tagName : 'tr',
				events : {
					'change input' : 'updateMemberData',
					'change select' : 'updateMemberData'
				},
				initialize : function() {
					var self = this;
					self.invalidated = false;

					self.demographicsConfig = self.options.demographicsConfig;

					self.setDefaults();
					
					var afterDate = Date.today().addYears(-121);
					var afterDateInUTCTime = Date.today().addYears(-121);
					self.options.model.set({
						afterDate : afterDate.toString("dddd, MMMM d, yyyy"),
						afterDateInUTCTime : afterDateInUTCTime.getTime()
					}, {
						validate : false
					});

					// self.model.on('change', this.render, this);

					connecture.view.Base.prototype.initialize.call(self);
				},
				render : function() {

					var self = this;

					var displayModel = self.options.model.toJSON();

					$(self.el).html(this.renderTemplate({
						data : {
							demographicsConfig : self.demographicsConfig,
							model : displayModel
						}
					}));

					Backbone.Validation.bind(this, {
						forceUpdate : true,
						valid : function(view, attr) {
							self.removeErrorMessages(attr);
							view.$('[name~="' + attr + '"]').parent('td').removeClass('hasErrors').removeAttr('data-error');
						},
						invalid : function(view, attr, error) {
							self.addErrorMessage(error, attr);
							view.$('[name~="' + attr + '"]').parent('td').addClass('hasErrors').attr('data-error', error);
						}
					});

					// TODO | this is a HACK
					// need to trigger something to update the model and trigger
					// the view validation; can't seem to trigger the view validation
					// manually..
					self.options.model.set("memberRefName", self.options.model.get("memberRefName"));

					self.postRender();

					if (self.model.get("invalidated")) {
						// show the errors now until they are cleared
						self.model.validate();
					}

					self.applyHCRRules();

					return this;
				},
				setDefaults : function() {
					var self = this;

					_.each(self.demographicsConfig.demographicFields, function(field) {
						var fieldName = field.name;

						if (self.getModelValue(fieldName) == null) {

							// nothing assigned, then try to set a default

							if (field.inputType == "select") {
								// find a default selected option, else assign the first option
								if (field.options) {

									// don't assign a blank value by default
									if (field.options[0].value && field.options[0].value != "") {

										self.setPropertyValue(field.options[0].value, fieldName, field.dataType);

										_.each(field.options, function(optionConfig) {
											if (optionConfig.defaultSelection == true) {

												if (optionConfig.value && optionConfig.value != "") {

													self.setPropertyValue(optionConfig.value, fieldName, field.dataType);
												}
											}
										});
									}
								}
							}
						}
					});
				},
				addErrorMessage : function(error, attr) {

					var self = this;

					var errorObj = {};
					errorObj.errorMsg = error;
					errorObj.attr = attr;
					errorObj.modelId = self.options.model.get("memberRefName");

					self.options.eventManager.trigger('view:addErrorMessage', errorObj);

				},
				removeErrorMessages : function(attr) {
					var self = this;

					var errorObj = {};
					errorObj.attr = attr;
					errorObj.modelId = self.options.model.get("memberRefName");

					self.options.eventManager.trigger('view:removeErrorMessages', errorObj);
				},
				postRender : function() {

					// change rendered HTML / bind scripts here

					var self = this;

					// apply masks
					// look through demographicsConfigJSON for masks and IDs
					var demographicFields = self.demographicsConfig.demographicFields;
					for ( var i = 0; i < demographicFields.length; i++) {
						if (demographicFields[i].mask) {

							if (demographicFields[i].mask.maskClass) {

								var maskField = $('.' + demographicFields[i].mask.maskClass, self.el);

								// apply mask
								maskField.mask(demographicFields[i].mask.mask, {
									placeholder : " "
								});
							}
						}
					}

					// if member age valid and < 18 then don't collect
				},
				applyHCRRules : function() {

					var self = this;
					var ct = $('input[name="coverageType"]').val();
					
					// after model data has been set...

					var effectiveDate = self.demographicsConfig.effectiveDate;

					var displayModel = self.options.model.toJSON();

					// look for birthDate, compare to effectiveDate
					if (displayModel["birthDate"]) {

						var birthdayDateObj = new Date(displayModel["birthDate"]);
						var effectiveDateObj = new Date(effectiveDate);

						var isUnder18 = true;

						var yearDiff = effectiveDateObj.getFullYear() - birthdayDateObj.getFullYear();

						// check year
						if (yearDiff > 18) {
							isUnder18 = false;
						} else if (yearDiff == 18) {

							// check month; if effective Date month is after birthday month, over 18
							if (effectiveDateObj.getMonth() > birthdayDateObj.getMonth()) {
								isUnder18 = false;
							} else if (effectiveDateObj.getMonth() == birthdayDateObj.getMonth()) {
								// check month; if effective Date day is after or ON birthday day, over 18
								if (effectiveDateObj.getDate() >= birthdayDateObj.getDate()) {
									isUnder18 = false;
								}
							}
						}

						if (isUnder18 || ct=='DEN') {
							// disable smoker field;
							if (self.model.get("isSmoker")) {
								self.model.unset("isSmoker");
							}

							// AND change the field to disabled on the UI
							$('[name~="isSmoker"]', self.el).attr('disabled', 'disabled');
						} else {
							$('[name~="isSmoker"]', self.el).attr('disabled', null);
						}
					}
				},
				updateMemberData : function(event) {

					var self = this;

					var $target = $(event.currentTarget);
					var memberRefId = $target.attr("data-id");
					var elementName = event.currentTarget.name;
					var elementValue = event.currentTarget.value;

					// TODO
					// NOTE: this is synchronous as long as what's happening on the other end is
					// also synchronous. This is not a good practice.
					self.options.eventManager.trigger('view:removeErrorMessagesForErrorSet', memberRefId);

					var fieldKey = null;
					var fieldDataType = null;
					var propertyValue = null;

					// look into demographicsFields and find the Key for the name
					var demographicFields = self.demographicsConfig.demographicFields;

					_.each(demographicFields, function(demographicField) {
						if (demographicField.name == elementName) {
							fieldKey = demographicField.key;
							fieldDataType = demographicField.dataType;
						}
					});

					// look into configuration mappings and find the property for the key
					if (fieldKey) {
						propertyValue = self.demographicsConfig.demographicsConfigMapping[fieldKey];
					}

					if (propertyValue) {

						var member = self.model;
						if (member.get("memberRefName") == memberRefId) {
							self.setPropertyValue(elementValue, propertyValue, fieldDataType);
						}
					}

					self.applyHCRRules();
					self.options.eventManager.trigger('view:checkSubmit');
				},
				getModelValue : function(propertyName) {

					var modelValue = null;

					var self = this;
					var member = self.model;

					var propertyStructure = propertyName.split(".");
					if (propertyStructure.length > 1) {

						var baseObjectName = propertyStructure[0];

						// try to find the object as it is assigned on the model
						var resultObject = member.get(baseObjectName);
						if (!resultObject) {
							return;
						} else {
							// keep digging!
							var leafNodeIndex = propertyStructure.length - 1;

							for ( var i = 1; i < propertyStructure.length; i++) {
								if (i < leafNodeIndex) {
									// try to find existing object on the model
									var currentObject = resultObject[propertyStrucure[i]];
									if (!currentObject) {
										// if it doesn't exist, create it as a new object
										return;
									}
								} else {
									modelValue = resultObject[propertyStructure[i]];
								}
							}
						}
					} else {
						modelValue = member.get(propertyName);
					}

					return modelValue;
				},
				setPropertyValue : function(value, propertyName, dataType) {

					var self = this;
					var member = self.model;

					var propertyStructure = propertyName.split(".");

					var setObject = {};

					if (propertyStructure.length > 1) {

						var baseObjectName = propertyStructure[0];

						// try to find the object as it is assigned on the model
						var resultObject = member.get(baseObjectName);
						if (!resultObject) {
							resultObject = {};
						}

						// this MUST be an object, as we do NOT handle arrays

						var leafNodeIndex = propertyStructure.length - 1;

						for ( var i = 1; i < propertyStructure.length; i++) {
							if (i < leafNodeIndex) {
								// try to find existing object on the model
								var currentObject = resultObject[propertyStrucure[i]];
								if (!currentObject) {
									// if it doesn't exist, create it as a new object
									resultObject[propertyStructure[i]] = {};
								}
							} else {
								resultObject[propertyStructure[i]] = self.getPropertyValueForType(value, dataType);
							}
						}

						setObject[baseObjectName] = resultObject;
					} else {
						// just go ahead and set the property directly on the model
						setObject[propertyStructure[0]] = self.getPropertyValueForType(value, dataType);
					}

					member.set(setObject, {
						validate : false
					});
				},
				getPropertyValueForType : function(value, dataType) {
					var returnValue = null;

					if (dataType == "Date") {
						if (value) {
							// no longer converting this
							return value;
						} else {
							returnValue = "";
						}
					} else if (dataType == "String") {
						returnValue = value;
					}

					return returnValue;
				}
			});

			return WhosCoveredMemberView;
		});
