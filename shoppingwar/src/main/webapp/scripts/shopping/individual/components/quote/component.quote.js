/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */
	//<BCBSNE US=RIDERS FOR PROPOSAL MI>
define([ 'module', 'backbone', 'templateEngine', 'common/component/component', 'components/quote/component.quote' ], function(module, Backbone, templateEngine, connecture, BaseQuoteComponent) {

	var FinalizeQuoteComponent = function(configuration) {
		BaseQuoteComponent.call(this, configuration);
	};

	FinalizeQuoteComponent.prototype = Object.create(BaseQuoteComponent.prototype);

	FinalizeQuoteComponent.prototype.registerEvents = function() {
		BaseQuoteComponent.prototype.registerEvents.call(this);
		
		
		
	};

	return FinalizeQuoteComponent;
});