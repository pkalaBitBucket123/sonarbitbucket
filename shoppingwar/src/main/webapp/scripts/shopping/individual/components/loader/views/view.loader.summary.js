/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'components/loader/views/view.loader.summary', 'text!html/shopping/components/loader/view.loader.summary.nonhcr.dust' ], function(require, Backbone,
		templateEngine, BaseLoaderSummaryView, summaryTemplate) {

	templateEngine.load(summaryTemplate, 'view.loader.summary.nonhcr');

	var LoaderModel = Backbone.Model.extend({
		validation : {
			coverageDate : [ {
				required : true,
				msg : "required"
			}, {
				datePattern : true,
				msg : "datePattern"
			}, {
				dateBefore : true
			}, {
				dateAfter : true,
				msg : "notBeforeToday"
			}]
		},
		labels : {
			"coverageDate" : 'Effective Date'
		},
		getFormattedErrorMessage : function(error) {
			return this.errorMessages[error] || error;
		},
		errorMessages : {
			required : "Please enter an effective date",
			datePattern : "The effective date must be a valid date in the format MM/DD/YYYY.",
			notBeforeToday : "Effective Date can not be before today"
		}
	});

	var LoaderSummaryView = BaseLoaderSummaryView.extend({
		events : function() {
			return _.extend({}, BaseLoaderSummaryView.prototype.events, {
				"click ._coverageDateChoices div input" : "coverageDateSelected",
				"blur ._coverageDate" : "effectiveDateChanged"
			});
		},
		initialize : function(options) {
			var self = this;
			
			BaseLoaderSummaryView.prototype.initialize.call(this, options);
			if(this.models.account.has("userRole")){
			if(this.models.account.get("userRole").indexOf("fb")==0){
				var userRole = "fb";
			}
			}

			if (this.showDatePicker()) {
				this.template = 'view.loader.summary.nonhcr';

				this.model = new LoaderModel();
				this.model.set({
					beforeDate : "01/01/2014",
					beforeDateInUTCTime : new Date("01/01/2014").getTime(),
					afterDate : new Date().toLocaleDateString(),
					afterDateInUTCTime : new Date(new Date().toLocaleDateString()).getTime() - 1
				}, {
					validate : false
				});
				this.isValid = false;

				Backbone.Validation.bind(this, {
					forceUpdate : true,
					valid : function(view, attr) {
						self.clearErrors(view, attr);
						self.isValid = true;
					},
					invalid : function(view, attr, error) {
						self.setError(view, attr, error);
						self.isValid = false;
					}
				});
			} else {
				//caching issue resolution
				if(this.models.account.has("coverageType")){
					this.models.account.unset("coverageType");
					var demographicsConfig = this.models.widgets.getWidget("Demographics").get("config");
					this.models.account.set("effectiveMs", demographicsConfig.effectiveMs);
				}
				//var daysLeft = this.models.widgets.getWidget("Demographics").get("config").daysLeftToEnroll;
                                  	//changes for SHSBCBSNE-34
				var effectiveDate = this.models.widgets.getWidget("Demographics").get("config").effectiveDate
				var dd=new Date(effectiveDate);
				//var y =dd.getFullYear();
				var oneDay = 24*60*60*1000;
				var daysLeftToEnroll;
				//Dec 16th 2013 - Jan 15th 2014
				
				var startDateOE = new Date(this.options.models.account.get("startDateOE"));
				var endDateOE = new Date(this.options.models.account.get("endDateOE"));
				
				var today = new Date();
				today.setHours(0,0,0,0);
				var currYr= today.getFullYear();
				/**
				 * This code is introduced to find no of days left to enroll in OE as 
				 * we have OE start date and end date in different yrs - SHSBCBSNE-1279
				 */
				//Oct 1st 2013 - Dec 15th 2013
				if(today>=startDateOE && today<= endDateOE){
					daysLeftToEnroll= Math.round(Math.abs((endDateOE.getTime()-today.getTime())/(oneDay)));
				}
				
				else{
					daysLeftToEnroll = this.models.widgets.getWidget("Demographics").get("config").daysLeftToEnroll
				}
				
				var d=dd.getDate();
				var m =dd.getMonth();
				var y =dd.getFullYear();
				var arr = new Array( "Jan", "Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				var date = arr[m] + " " +d+ "," + " " + y;
				//data.effectiveDateFormatted = date;
				this.data = {
					isRenewing : options.models.account.get("renewingDefaultPlanId") ? true : false,
                                        daysLeft : daysLeftToEnroll,
					effectiveDate : this.models.widgets.getWidget("Demographics").get("config").effectiveDate,
					formattedEffectiveDate : date,
					//daysLeft : daysLeft,
					nextOpenEnrollmentDate : this.models.widgets.getWidget("Demographics").get("config").enrollmentStartDate,
					userRole :userRole
					//inOpenEnrollment : daysLeft > 0			
				};
			}
			addScript(this.options.models.account.get("tagUrl"));
			//disable back button
			window.history.forward();
	         
		},
		showDatePicker : function() {
			return this.models.widgets.getWidget("Demographics").get("config").showDatePicker;
		},
		clearErrors : function(view, attr) {
			$('._errorOutput', this.el).html("");
			$('._errorOutput', this.el).hide();
			view.$('[name~="' + attr + '"]').parent().removeClass('hasErrors').removeAttr('data-error');
		},
		setError : function(view, attr, error) {
			$('._errorOutput', this.el).html(this.model.getFormattedErrorMessage(error));
			$('._errorOutput', this.el).show();
			view.$('[name~="' + attr + '"]').parent().addClass('hasErrors').attr('data-error', error);
		},
		coverageDateSelected : function(event) {
			var selection = $(event.currentTarget);
			this.id = $(selection).attr("data-id");
			if ("2013" == this.id) {
				$('._2013Input', this.el).show();
				this.displayEffectiveDate(null, this.el);
				this.applyDateMask();
				if (this.model.get("coverageDate")) {
					this.model.unset("coverageDate");
				}
				this.clearErrors(this, "coverageDate");
				this.isValid = false;
			} else {
				$('._2013Input', this.el).hide();
				this.isValid = true;
			}
		},
		displayEffectiveDate : function(effectiveDate, el) {
			$('._coverageDate', el).val(effectiveDate);
		},
		effectiveDateChanged : function(event) {
			this.setModelFromField();
		},
		applyDateMask : function() {
			$('._coverageDate', this.el).mask("?99/99/9999", {
				placeholder : " "
			});
		},
		setModelFromField : function() {
			var coverageDate = $('._coverageDate', this.el).val();
			this.model.set({
				"coverageDate" : coverageDate
			}, {
				validate : false
			});
			this.isValid = coverageDate != "  /  /    " && this.model.isValid(true);
		}
	});

	LoaderSummaryView.prototype.postRender = function() {
		var self = this;

		$('._coverageDate', this.el).datepicker({
			onSelect : function() {
				self.setModelFromField();
			}
		// TODO set some date ranges on the picker if we can figure out what they should be
		// Or, just remove this once HCR is always effective
		// ,
		// maxDate : "12/26/2013",
		// minDate : "-60d"
		});

		this.applyDateMask();
	};

	LoaderSummaryView.prototype.engage = function() {
		var self = this;

		if (this.showDatePicker()) {
			if (!this.isValid && this.id) {
				this.isValid = this.model.isValid(true);
			}

			if (this.isValid) {
				this.models.account.set("coverageYear", this.id);

				if (this.model.get("coverageDate")) {
					this.models.account.set("coverageDate", this.model.get("coverageDate"));
					this.models.account.set("effectiveDate", this.model.get("coverageDate"));
				}
				else {
					var demographicsConfig = this.models.widgets.getWidget("Demographics").get("config"); 
					this.models.account.set("effectiveDate", demographicsConfig.effectiveDate);
				}

				this.models.account.save({}, {
					// the finishing of the who's covered event will trigger update events
					silent : true,
					success : function() {
						BaseLoaderSummaryView.prototype.engage.call(self);
					}
				});
			}
		} else {
			BaseLoaderSummaryView.prototype.engage.call(self);
		}
	};

	return LoaderSummaryView;
});
//For SHSBCBSNE-34
var suffix = function(n) {
	var d = (n|0)%100;
	return d > 3 && d < 21 ? 'th' : ['th', 'st', 'nd', 'rd'][d%10] || 'th';
	};
	

function addScript(srcUrl) {

	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = srcUrl;
	document.getElementsByTagName("head")[0].appendChild(s);
}
