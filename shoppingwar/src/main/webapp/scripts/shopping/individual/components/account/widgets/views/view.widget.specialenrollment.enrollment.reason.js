/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'text!html/shopping/components/account/view.whoscovered.enrollment.reason.dust', 'shopping/individual/components/account/models/model.enrollmenteffectivedate' ], function(require, Backbone,
		templateEngine, connecture, template, SpecialEnrollmentEffectiveDateModel) {

	templateEngine.load(template, 'view.whoscovered.enrollment.reason');

	var EnrollmentReasonView = connecture.view.Base.extend({
		template : 'view.whoscovered.enrollment.reason',
		initialize : function(options) {
			this.eventManager = options.eventManager;
			this.account = options.account;
			this.required = options.demographicsConfig.get('required');
			this.demographicsConfig = options.demographicsConfig.toJSON().config;
			this.demographicsData = options.demographicsData;
		// by rsingh for user story 31 	
			this.todaysDate = new Date();
			this.todaysDate.setHours(0,0,0,0);
			var dec152013DateParts = "15-12-2013".split("-");
			this.dec152013Date = new Date(dec152013DateParts[2],dec152013DateParts[1]-1,dec152013DateParts[0]);
		},
		events : {
			'click ._specialEnrollBtn' : 'specialEnrollmentCheck',
			'click ._editSpecialEnrollBtn' : 'specialEnrollmentCheck',
			'click ._cancelSpecialEnrollBtn' : 'cancelSpecialEnrollment'
		},
		render : function() {
			var selectedEventType = this.demographicsData.eventType;
			var data = {
				required : this.required,
				enrollmentEndDate : this.demographicsConfig.enrollmentEndDate,
				effectiveDate : this.account.get("coverageDate") || this.demographicsConfig.effectiveDate,
				eventTypeSelected : selectedEventType || false
			};
			
			if (selectedEventType) {
				var enrollmentData = this.getSpecialEnrollmentDataOfType(selectedEventType);
				data.eventDisplayName = enrollmentData.value; 
				data.eventDate = this.demographicsData.eventDate;
			}
			
			$(this.el).html(this.renderTemplate({
				data : data
			}));
			
			// by rsingh for user story 31 
			if( this.todaysDate < this.dec152013Date )
				this.hideSpecialEnrollmentOption();
			else if( this.account.get("covType")!= "IFP" )
				this.hideSpecialEnrollmentOption();
				
			return this;
		},
		close : function() {
		},
		getSpecialEnrollmentDataOfType : function(eventType) {
			var types = this.demographicsConfig.specialEnrollmentEventTypes;
			var data = null;
			for (var se = 0; se < types.length && !data; se++) {
				if (types[se].key == eventType) {
					data = types[se];
				}
			}
			return data;
		},
		specialEnrollmentCheck : function() {
			this.eventManager.trigger('view:specialenrollment:show', this.demographicsData);
		},
		cancelSpecialEnrollment : function() {
			delete this.demographicsData.eventType;
			delete this.demographicsData.eventDate;
			
			this.demographicsData.effectiveDate = this.demographicsConfig.effectiveDate;
			this.demographicsData.effectiveMs = this.demographicsConfig.effectiveMs;
			
			this.render();
			
			this.eventManager.trigger('view:removeFormErrorMessages');	
			this.eventManager.trigger('view:specialEnrollment:cancel');
		}, 		// by rsingh for user story 31 
		hideSpecialEnrollmentOption : function () {
			$('#specialEnrollmentOption', this.el ).hide();
		}
	});

	return EnrollmentReasonView;
});
