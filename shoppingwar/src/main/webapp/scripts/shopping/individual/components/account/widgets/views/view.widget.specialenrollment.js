/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/validation/validation-extensions', 'templateEngine', 'common/component/component', 'shopping/individual/components/account/models/model.enrollmenteffectivedate', 'shopping/individual/components/account/models/model.specialenrollmenteventdatevalidation', 'text!html/shopping/components/account/view.enrollment.special.dust' ],
		function(require, Backbone, commonValidation, templateEngine, connecture, SpecialEnrollmentEffectiveDateModel, SpecialEnrollmentEventDateValidationModel, template) {

			templateEngine.load(template, 'view.enrollment.special');

			var EventDateModel = Backbone.Model.extend({
				validation : {
					eventDate : [ {
						required : true,
						msg : "required"
					}, {
						datePattern : true,
						msg : "datePattern"
					}
					// TODO Use this once we no longer need to call HCR rule
					//   to determine if we're within the enrollment window.
//					, {
//						dateNotInFuture : true,
//						msg : "dateNotInFuture"
//					}, {
//						dateNotMoreThanDaysInPast : true,
//						msg : "dateNotMoreThanDaysInPast"
//					} 
					]
				},
				labels : {
					"eventDate" : "Event date"
				},
				errorMessages : {
					required : "Please enter a date to continue.",
					datePattern : "The event date must be a valid date in the format MM/DD/YYYY."
					// TODO Use this once we no longer need to call HCR rule
					//   to determine if we're within the enrollment window.
//						,
//					dateNotInFuture : "The event date cannot be a future date. Please verify the event date and try again."
//					dateNotMoreThanDaysInPast : "The event date cannot be more than {0} days in the past. Please verify the event date and try again."
				},
				getFormattedErrorMessage : function(error) {
					return this.format(this.errorMessages[error] || error, this.get("moreThanDays"));
				},
				// Replaces nummeric placeholders like {0} in a string with arguments
				// passed to the function
				format : function() {
					var args = Array.prototype.slice.call(arguments);
					var text = args.shift();
					return text.replace(/\{(\d+)\}/g, function(match, number) {
						return typeof args[number] !== 'undefined' ? args[number] : match;
					});
				}
			});

			var SpecialEnrollmentView = connecture.view.Base.extend({
				template : 'view.enrollment.special',
				events : {
					'change ._eventChoice' : 'specialEnrollChange',
					'click ._specialEnrollCheck ._saveEvent' : 'specialEnrollResult',
					'click ._specialEnrollCheck ._previousEvent' : 'specialEnrollCancel',
					'click ._specialEnrollCheck ._clearEvent' : 'specialEnrollClear',
					'blur ._eventDate' : 'manualEventDateUpdate'
				},
				initialize : function(options) {
					var self = this;
					var requiredFlag = options.demographicsConfig.get('required');

					this.account = options.account;
					this.required = requiredFlag ? requiredFlag : false;
					this.demographicsConfig = options.demographicsConfig;
					this.eventManager = options.eventManager;
					this.demographicsData = options.demographicsData;

					this.model = new EventDateModel();

					var eventDate = this.demographicsData.eventDate;
					if (eventDate) {
						this.model.set("eventDate", eventDate);
					}

					this.eventType = this.demographicsData.eventType;

					Backbone.Validation.bind(this, {
						forceUpdate : true,
						valid : function(view, attr) {
							self.clearErrors(view, attr);
							
							self.validateEventDate();	
						},
						invalid : function(view, attr, error) {
							$('._errors').html(self.model.getFormattedErrorMessage(error));
							$('._errors').show();
							view.$('[name~="' + attr + '"]').parent().addClass('hasErrors').attr('data-error', error);
							self.setValid(false);
						}
					});
				},
				validateEventDate : function() {
					var self = this;
					
					var data = {
						eventDate : this.tempEventDate,
						eventReason : this.eventType
					};
					var specialEnrollmentEventDateValidationModel = new SpecialEnrollmentEventDateValidationModel(data);
					specialEnrollmentEventDateValidationModel.fetch({
						silent : true,
						success : function() {
							var valid = false;
							var withinEnrollmentWindowResult = specialEnrollmentEventDateValidationModel.get("withinEnrollmentWindow");
							if ("Not Applicable" == withinEnrollmentWindowResult) {
								errorMessage = "The event date cannot be a future date. Please verify the event date and try again.";
							// start, changes cherry picked from product for SHSBCBSNE-1196		
							} else {
								if(self.eventType == "LossOfCovg"){
									errorMessage = "The event date cannot be more than {0} days in the past/future. Please verify the event date and try again.";
								}else{
									errorMessage = "The event date cannot be more than {0} days in the past. Please verify the event date and try again.";
								} 
							// end, changes cherry picked from product for SHSBCBSNE-1196	
								valid = "Yes" == withinEnrollmentWindowResult;
							}
							self.setValid(valid);
							if (!valid) {
								$('._errors', self.el).html(self.model.getFormattedErrorMessage(errorMessage));
								$('._errors', self.el).show();
							}
						}
					});
				},
				render : function() {
					$(this.el).html(this.renderTemplate({
						data : {
							required : this.required,
							specialEnrollmentEventTypes : this.demographicsConfig.get("config").specialEnrollmentEventTypes
						}
					}));

					if (this.eventType) {
						$('#' + this.eventType, this.el).attr("checked", true);

						this.updateEnrollmentForEventType(true);
					}

					var eventDate = this.model.get("eventDate");
					if (eventDate) {
						this.setValid(true);
						this.displayEventDate(eventDate, this.el);
					} else {
						this.setValid(false);
					}

					return this;
				},
				clearErrors : function(view, attr) {
					$('._errors').hide();
					view.$('[name~="' + attr + '"]').parent().removeClass('hasErrors').removeAttr('data-error');
				},
				applyDateMask : function() {
					$('._eventDate', this.el).mask("?99/99/9999", {
						placeholder : " "
					});
				},
				manualEventDateUpdate : function() {
					var eventDate = $('._eventDate', this.el).val();
                    this.tempEventDate = eventDate;
					this.eventDateUpdate();
				},
				pickerEventDateUpdate : function() {
                    var eventDate = $('._eventDate', this.el).datepicker("getDate");
                    var eventDateString = $.datepicker.formatDate('mm/dd/yy', eventDate);
                    this.tempEventDate = eventDateString;
					this.eventDateUpdate();
				},
                eventDateUpdate : function() {
                	var self = this;
					this.setValid(false);
					this.model.set({
						"eventDate" : self.tempEventDate
					}, {
						validate : self.tempEventDate != "  /  /    "
					});
				},
				displayEventDate : function(eventDate, el) {
					$('._eventDate', el).val(eventDate);
				},
				setValid : function(isValid) {
					var self = this;

					this.isValidData = isValid;

					if (isValid) {
						$('.btn_primary', this.el).removeClass("disabled-button");
					} else {
						$('.btn_primary', this.el).addClass("disabled-button");
					}
				},
				close : function() {

				},
				specialEnrollChange : function() {
					this.eventType = $('input[name="seReason"]:checked').val();
					this.updateEnrollmentForEventType();
				},
				updateEnrollmentForEventType : function(initialLoad) {
					
					var self = this;
					
					$('._captureEventDate', this.el).show();

					if (!initialLoad) {
						this.model.unset("eventDate");
					}

					this.displayEventDate(this.model.get("eventDate"));
					this.applyDateMask();

					this.setValid(initialLoad);

					this.clearErrors(this, "eventDate");

					this.applyDateMask();
									
					var enrollmentData = this.getSpecialEnrollmentDataOfType(this.eventType);


					// start datepicker script
					// start, changes cherry picked from product for SHSBCBSNE-1196
					if(this.eventType=='LossOfCovg'){
						var maxDaysInPast = enrollmentData.enrollmentDays;
						var maxDaysInFuture = enrollmentData.enrollmentDays-1;
					}else{
						var maxDaysInPast = enrollmentData.enrollmentDays;
						var maxDaysInFuture = 0;
					}
					// end, changes cherry picked from product for SHSBCBSNE-1196
					
					// attach a datepicker restricted to the correct date
					$('._eventDate', this.el).datepicker("destroy");
					$('._eventDate', this.el).datepicker({
						onSelect : function() {
							self.pickerEventDateUpdate();
						},
						maxDate : "+" + maxDaysInFuture + "d",
						minDate : "-" + maxDaysInPast + "d"
					});
					
					// end datepicker script
					
					
					this.model.set({
						moreThanDays : enrollmentData.enrollmentDays
					}, {
						validate : false
					});

					// Update the days to enroll
					$('._enrollmentDays', this.el).html(enrollmentData.enrollmentDays);

					// Update the event type
					$('._displayName', this.el).html(enrollmentData.value);
					// start, changes cherry picked from product for SHSBCBSNE-1196
					//show future date information.
					var futureMessage = "";
					if(this.eventType=='LossOfCovg'){
						futureMessage = " and <strong>"+enrollmentData.enrollmentDays+" days</strong> after ";
					}
					$('._future', this.el).html(futureMessage);
					// end, changes cherry picked from product for SHSBCBSNE-1196
				},
				getSpecialEnrollmentDataOfType : function(eventType) {
					var types = this.demographicsConfig.get("config").specialEnrollmentEventTypes;
					var data;
					for ( var se = 0; se < types.length && !data; se++) {
						if (types[se].key == eventType) {
							data = types[se];
						}
					}
					return data;
				},
				specialEnrollResult : function() {
					var self = this;

					if (this.isValidData) {
						if (this.eventType !== undefined) {
							self.demographicsData.eventType = this.eventType;
							self.demographicsData.eventDate = this.model.get("eventDate");
							
							var effectiveDateModel = new SpecialEnrollmentEffectiveDateModel({
								eventDate : self.demographicsData.eventDate,
								eventReason : self.demographicsData.eventType
							});
							
							effectiveDateModel.fetch({
								silent : true,
								success : function() {
			
									self.demographicsData.effectiveMs = effectiveDateModel.get("effectiveMs");
									self.demographicsData.effectiveDate = effectiveDateModel.get("effectiveDate");
									
									self.eventManager.trigger('view:whoscovered:show', self.demographicsData);	
								}
							});		
							
						} else {
							delete self.demographicsData.eventType;
							delete self.demographicsData.eventDate;
							delete self.demographicsData.effectiveDate;

							self.eventManager.trigger('view:whoscovered:show', self.demographicsData);							
						}	
					} else {
						this.model.validate();
					}
				},
				specialEnrollClear : function() {
									
					delete this.demographicsData.eventType;
					delete this.demographicsData.eventDate;
					
					if (this.model.get("eventDate")) {
						this.model.unset("eventDate");
						// if date isn't required, the model is valid what nothing exists
						// otherwise, the date is required and we have no invalidated the model.
						this.setValid(!this.required);
					}
					
					var config = this.demographicsConfig.get("config");
					
					this.demographicsData.effectiveDate = config.effectiveDate;
					this.demographicsData.effectiveMs = config.effectiveMs;
												
					this.render();
					
					$('input[name="seReason"]:checked').prop("checked", false);
					$('._captureEventDate', this.el).hide();
				},
				specialEnrollCancel : function() {
					this.eventManager.trigger('view:whoscovered:show', this.demographicsData);
				}
			});

			return SpecialEnrollmentView;
		});
