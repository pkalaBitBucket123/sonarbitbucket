/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/views/base', 'common/component/component', 'components/header/views/view.help.faq',
		'components/header/views/view.header.links.internal', 'text!html/shopping/components/header/view.header.links.internal.dust', 'text!html/individual/components/header/view.header.quotereceipt.dust' ], function(require, Backbone, templateEngine, dustHelpers, BaseView, connecture, HelpFAQView, HeaderLinksView, headerTemplate, headerQuoteReceiptTemplate) {

	templateEngine.load(headerQuoteReceiptTemplate, "view.header.quotereceipt");

	IndividualHeaderLinksView = HeaderLinksView.extend({
		
	});

	return IndividualHeaderLinksView;
});