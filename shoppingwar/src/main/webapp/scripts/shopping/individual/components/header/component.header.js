/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the top level component header. This is the main header that will be customized depending on where shoppnig is included.
 * 
 */
define([ 'module', 'log', 'backbone', 'templateEngine', 'common/views/base', 'common/component/component', 'components/header/component.header',
		'text!html/shopping/components/header/view.header.quotereceipt.dust' ], function(module, log, Backbone, templateEngine, BaseView, connecture, BaseHeaderComponent, QuoteReceiptTemplate) {

	templateEngine.load(QuoteReceiptTemplate, "view.header.quotereceipt");

	var QuoteReceiptView = BaseView.extend({
		template : "view.header.quotereceipt",
		renderHTML : function(data) {
			var html = this.renderTemplate({
				data : data
			});

			return html;
		}
	});

	var HeaderComponent = function(configuration) {
		BaseHeaderComponent.call(this, configuration);
	};

	HeaderComponent.prototype = Object.create(BaseHeaderComponent.prototype);

	// register events : events coming from this component's view(s)
	HeaderComponent.prototype.registerEvents = function() {
		var self = this;

		BaseHeaderComponent.prototype.registerEvents.call(this);

		this.eventManager.on('view:quoteReceipt:show', function() {
			// Re-render and Show Quote Receipt
			this.showQuote();
		}, this);

		this.eventManager.on('view:quoteReceipt:hide', function() {
			this.hideQuote();
		}, this);

		this.eventManager.on('view:quoteReceipt:save', function() {
			this.saveQuote();
		}, this);

		this.eventManager.on('view:quoteReceipt:removePlan', function(data) {
			this.removeQuotePlan(data);
		}, this);

		this.models.quote.on('change:plans', function(quote, planId, options) {
			this.changeQuotePlans(options);
		}, this);
	};

	HeaderComponent.prototype.hideQuote = function() {
		$('._quoteDropdown', self.el).css({
			'height' : '0px',
			'opacity' : '0'
		});
	};

	HeaderComponent.prototype.changeQuotePlans = function(options) {

		var self = this;

		if (!options.remove) {
			this.models.quote.save({}, {
				silent : true,
				failure : function() {
					// TODO Display an error to the user?
					// Roll back the toggle?
				},
				success : function() {
					// Re-render and Show Quote Receipt
					self.showQuote(true);
				}
			});
		}
	};

	HeaderComponent.prototype.removeQuotePlan = function(data) {

		var self = this;

		var plans = this.models.quote.get("plans");
		for ( var p = 0; p < plans.length; p++) {
			if (plans[p].planId == data.planId) {
				plans.splice(p, 1);
			}
		}

		this.models.quote.unset('plans', {
			silent : true
		});
		this.models.quote.set({
			'plans' : plans
		}, {
			remove : true
		});

		this.models.quote.save({}, {
			silent : true,
			failure : function() {
				// TODO Display an error to the user?
				// Roll back the toggle?
			},
			success : function() {
				// Re-render and Show Quote Receipt
				self.showQuote();
			}
		});
	};

	HeaderComponent.prototype.showQuote = function(temp) {
		var self = this;

		var view = new QuoteReceiptView();

		var data = this.assembleData();

		$('._quoteDropdown', self.el).html(view.renderHTML(data));

		$('._quoteDropdown ._monthlyRate', self.el).formatCurrency({
			roundToDecimalPlace : 2
		});

		$('._quoteDropdown', self.el).css({
			'height' : '310px',
			'opacity' : '1',
			'filter': 'alpha(opacity=100)' // NE Branding
		});

		if (this.timeoutHandle) {
			clearTimeout(this.timeoutHandle);
			delete this.timeoutHandle;
		}

		// If [ we are showing the Quote Receipt temporarily ] Then
		if (temp) {
			// Hide the quote after 3 seconds
			this.timeoutHandle = setTimeout(this.hideQuote, 3000);
		}
	};

	HeaderComponent.prototype.assembleData = function() {
		var data = {};
		var productPlans = this.models.plans;
		var quotePlans = this.models.quote.get("plans");
		
		if(quotePlans.length>1){
		   for (var i = 0; i < quotePlans.length; i++) {
		      for(var j =i+1;j<quotePlans.length;){
			     if (quotePlans[j].planId == quotePlans[i].planId) {
				    quotePlans.splice(quotePlans, j); 
			     }
			     else {
			        j++;
			     }
			  }
		   }
		}
		if (productPlans && productPlans.length > 0 && quotePlans && quotePlans.length > 0) {

			// Build plan data for quote
			data.quoteplans = [];
			_.each(quotePlans, function(quoteplan) {
				_.any(productPlans.models, function(plan) {
					if (plan.get("planId") == quoteplan.planId) {
						data.quoteplans.push({
							planId : quoteplan.planId,
							name : plan.get("name"),
							// TODO: We should probably have one location on the plan model that has the current rate so we don't need to do this check here
							rate : plan.get("cost").monthly.subsidyData ? plan.get("cost").monthly.subsidyData.netRate : plan.get("cost").monthly.rate.amount
						});
					}
				});
			});
			data.quotePlanCount = data.quoteplans.length;
		}
		return data;
	};

	HeaderComponent.prototype.saveQuote = function() {
		var self = this;

		this.models.quote.save({}, {
			failure : function() {
				// TODO Display an error to the user?
				// Roll back the toggle?
			},
			success : function() {
				self.saveAndExit();
			}
		});

	};

	HeaderComponent.prototype.saveAndExit = function() {
		var priorToApplyURL = this.models.applicationConfig.get("ffmPriorToApplyURL");
		if (priorToApplyURL) {
			var params = this.assembleParams();
			this.eventManager.trigger('exit', {
				key : 'back',
				location : priorToApplyURL,
				params : params
			});
		}
	};

	HeaderComponent.prototype.assembleParams = function() {
		var params = {};
		var plans = this.models.quote.get("plans");
		if (plans.length > 0) {
			var quotePlanIdList = "";
			for ( var i = 0; i < plans.length; i++) {
				quotePlanIdList = quotePlanIdList + plans[i].planId;
				if (i < plans.length - 1) {
					quotePlanIdList = quotePlanIdList + "%2C";
				}
			}
			params.quotePlanIds = quotePlanIdList;
		}
		return params;
	};

	return HeaderComponent;
});