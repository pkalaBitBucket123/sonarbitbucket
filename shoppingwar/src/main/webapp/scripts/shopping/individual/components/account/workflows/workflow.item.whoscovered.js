/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'module', 'backbone', 'common/widget.factory', 'common/workflows/workflow.item', 'components/account/workflows/views/view.workflow.item.whoscovered', 'shopping/individual/components/account/models/model.demographics.member','components/account/workflows/workflow.item.whoscovered' ], function(module, Backbone,
		WidgetFactory, WorkflowItem, WhosCoveredWorkfowTemplate, DemoMemberModel, BaseWhosCoveredWorkflowItem) {
	/**
	 * I removed the config checks determining when we trigger members updated put back as we figure out configuration.
	 */
	
	var WhosCoveredWorkflowItem = function(configuration, widgets, options) {
		BaseWhosCoveredWorkflowItem.call(this, configuration, widgets, options);
	};

	WhosCoveredWorkflowItem.prototype = Object.create(BaseWhosCoveredWorkflowItem.prototype);

	WhosCoveredWorkflowItem.prototype.buildInitData = function() {
		
		var self = this;
		
		// 1. member demographics
		var account = self.options.models.account;
		this.demographicsConfig = self.widgets.getWidget('Demographics').get('config');	
		this.demographicsData = this.buildDemographicsData(account, this.demographicsConfig);
			
		// 2. set defaults using member data and config
		this.setDefaults(this.demographicsConfig, this.demographicsData);
		
		// 3. missing fields (enabled on otherwise disabled views)
		var session = self.options.session;
		if (!session.get("missingValues")){
			session.set("missingValues", this.missingValues);
		} else {
			var sessionMissingValues = session.get("missingValues");
			session.set("missingValues", $.extend(sessionMissingValues, this.missingValues));
		}
	};

	WhosCoveredWorkflowItem.prototype.buildWidgetData = function() {
		return {
			session : this.options.session,
			account : this.options.models.account,
			actor : this.options.models.actor,
			quote : this.options.models.quote,
			applicationConfig : this.options.models.applicationConfig,
			demographicsData : this.demographicsData,
			demographicsConfig : this.demographicsConfig,
			eligibility : this.options.models.eligibility,
			genders : this.options.models.genders,
			productLines : this.options.models.productLines,
			relationships : this.options.models.relationships,
			componentId : this.options.history.id
		};
	};

	/**
	 * Overriding here because we need a new template for this workflow item. This will work for now but could be better
	 * 
	 * @param options
	 * @returns {WhosCoveredWorkfowTemplate}
	 */
	WhosCoveredWorkflowItem.prototype.render = function(options) {

		return new WhosCoveredWorkfowTemplate();
	};

	WhosCoveredWorkflowItem.prototype.buildDemographicsData = function(account, demographicsConfig) {

		var self = this;

		// build the model data for the demographics view
		var demographicsData = {};

		// get member data
		var currentMembers = [];
		if (account.get("members")) {
			_.each(account.get("members"), function(member) {
				currentMembers[currentMembers.length] = self.buildExistingDemographicsMemberModel(member);
			});
		}

		demographicsData.currentMembers = new Backbone.Collection(currentMembers);

		if (account.get("zipCode")) {
			demographicsData.zipCode = account.get("zipCode");
		}

		if (account.get("countyId")) {
			demographicsData.countyId = account.get("countyId");
		}

		if (account.get("eventType")) {
			demographicsData.eventType = account.get("eventType");
			demographicsData.eventDate = account.get("eventDate");
		}

		if (account.get("effectiveMs")) {
			demographicsData.effectiveMs = account.get("effectiveMs");
		} else {
			demographicsData.effectiveMs = demographicsConfig.effectiveMs;
		}

		if (account.get("effectiveDate")) {
			demographicsData.effectiveDate = account.get("effectiveDate");
		} else if (account.get("coverageDate")) {
			demographicsData.effectiveDate = account.get("coverageDate");
		} else {
			demographicsData.effectiveDate = demographicsConfig.effectiveDate;
		}

		return demographicsData;
	};
	
	WhosCoveredWorkflowItem.prototype.buildExistingDemographicsMemberModel = function(memberData) {
		var isDefault = (memberData.relationshipKey == "PRIMARY");
		var memberModel = new DemoMemberModel(memberData);
		if (isDefault && (isDefault == true)) {
			memberModel.set("default", true);
		}
		memberModel.set("new", false);	
		return memberModel;
		
	},
	
	WhosCoveredWorkflowItem.prototype.setDefaults = function(demographicsConfig, demographicsData) {

		var self = this;

		var demographicFields = demographicsConfig.demographicFields;

		_.each(demographicFields, function(field) {
			var fieldName = field.name;

			_.each(demographicsData.currentMembers.models, function(memberModel) {

				if (self.getModelValue(memberModel, fieldName) == null) {

					var memberRefName = memberModel.get("memberRefName");

					// nothing assigned; take note of that, but then try to set a default
					var missingValues = self.missingValues;
					if (!missingValues) {
						missingValues = {};
					}
					if (!missingValues[memberRefName]) {
						missingValues[memberRefName] = {};
					}
					var memberValues = missingValues[memberRefName];
					if (!memberValues[fieldName]) {
						memberValues[fieldName] = "missing";
					}
					// done setting missing values
					self.missingValues = missingValues;

					if (field.inputType == "select") {
						// find a default selected option, else assign the first option
						if (field.options) {

							// don't assign a blank value by default
							if (field.options[0].value && field.options[0].value != "") {

								self.setPropertyValue(field.options[0].value, fieldName, field.dataType, memberModel);

								_.each(field.options, function(optionConfig) {
									if (optionConfig.defaultSelection == true) {

										if (optionConfig.value && optionConfig.value != "") {

											self.setPropertyValue(optionConfig.value, fieldName, field.dataType, memberModel);
										}
									}
								});
							}
						}
					}
				}
			});
		});
	};
	
	WhosCoveredWorkflowItem.prototype.getModelValue = function(memberModel, propertyName) {

		var modelValue = null;

		var member = memberModel;

		var propertyStructure = propertyName.split(".");
		if (propertyStructure.length > 1) {

			var baseObjectName = propertyStructure[0];

			// try to find the object as it is assigned on the model
			var resultObject = member.get(baseObjectName);
			if (!resultObject) {
				return;
			} else {
				// keep digging!
				var leafNodeIndex = propertyStructure.length - 1;

				for ( var i = 1; i < propertyStructure.length; i++) {
					if (i < leafNodeIndex) {
						// try to find existing object on the model
						var currentObject = resultObject[propertyStrucure[i]];
						if (!currentObject) {
							// if it doesn't exist, create it as a new object
							return;
						}
					} else {
						modelValue = resultObject[propertyStructure[i]];
					}
				}
			}
		} else {
			modelValue = member.get(propertyName);
		}

		return modelValue;
	};
	
	WhosCoveredWorkflowItem.prototype.setPropertyValue = function(value, propertyName, dataType, memberModel) {

		var self = this;

		var propertyStructure = propertyName.split(".");

		var setObject = {};

		if (propertyStructure.length > 1) {

			var baseObjectName = propertyStructure[0];

			// try to find the object as it is assigned on the model
			var resultObject = memberModel.get(baseObjectName);
			if (!resultObject) {
				resultObject = {};
			}

			// this MUST be an object, as we do NOT handle arrays

			var leafNodeIndex = propertyStructure.length - 1;

			for ( var i = 1; i < propertyStructure.length; i++) {
				if (i < leafNodeIndex) {
					// try to find existing object on the model
					var currentObject = resultObject[propertyStrucure[i]];
					if (!currentObject) {
						// if it doesn't exist, create it as a new object
						resultObject[propertyStructure[i]] = {};
					}
				} else {
					resultObject[propertyStructure[i]] = self.getPropertyValueForType(value, dataType);
				}
			}

			setObject[baseObjectName] = resultObject;
		} else {
			// just go ahead and set the property directly on the model
			setObject[propertyStructure[0]] = self.getPropertyValueForType(value, dataType);
		}

		memberModel.set(setObject, {
			validate : true
		});
	};
	
	WhosCoveredWorkflowItem.prototype.getPropertyValueForType = function(value, dataType) {
		var returnValue = null;

		if (dataType == "Date") {
			if (value) {
				// no longer converting this
				return value;
			} else {
				returnValue = "";
			}
		} else if (dataType == "String") {
			returnValue = value;
		}

		return returnValue;
	};
	
	return WhosCoveredWorkflowItem;
});