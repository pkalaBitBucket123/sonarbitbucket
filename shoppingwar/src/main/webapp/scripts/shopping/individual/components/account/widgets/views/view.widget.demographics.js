/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'backbone.validation', 'templateEngine', 'common/component/component', 'shopping/individual/components/account/widgets/views/view.widget.demographics.member',
		'shopping/individual/components/account/models/model.demographics.member', 'shopping/individual/components/account/models/model.demographics.form',
		'shopping/individual/components/account/models/model.demographics.validation', 'shopping/individual/components/account/dust/helpers.demographics',
		'shopping/core/components/account/models/model.validzipcodecountydata', 'text!html/individual/components/account/view.whoscovered.dust',
		'text!html/individual/components/account/view.whoscovered.counties.dust', 'jquery.maskedInput', 'common/validation/validation-extensions', 'shopping/individual/components/account/widgets/views/view.widget.demographics.base' ], function(require, Backbone, BackboneValidation,
		templateEngine, connecture, MemberView, DemoMemberModel, DemoFormModel, DemographicsValidationModel, demographicsHelpers, ValidZipCodeCountyDataModel, template, countiesTemplate, maskedInput,
		commonValidation, BaseWhosCoveredView) {

	templateEngine.load(template, "view.whoscovered");
	templateEngine.load(countiesTemplate, "view.whoscovered.counties");

	var WhosCoveredView = BaseWhosCoveredView.extend({
		template : "view.whoscovered",
		events : {
			'click a._addIndividual' : 'addNewMember',
			'click a.remove-link' : 'removeMember',
			'click ._whosCoveredEngage ' : 'saveAndContinue',
			'mousedown ._whosCoveredEngage ' : 'submitDown',
			'keyup ._zipCode' : 'getCountiesForZip',
			'change ._zipCode' : 'updateZipCode',
			'change ._zipCounty' : 'updateZipCounty',
			//<Changes for SHSBCBSNE-100 by Priyanka M>
			'change #childOnlyField' : 'updateChildOnly',
			//</Changes for SHSBCBSNE-100 by Priyanka M>
			 'click #effectiveDate' :  'effectiveDate',
			 'change #effectiveDate' :  'changeInsuranceDate',
		},
		initialize : function(options) {
			var self = this;

			// set a variable for tracking whether the Continue
			// button has been clicked (used to track submit when
			// multiple conflicting events are triggered)
			self.submitClicked = false;

			self.formErrorSetId = "DEMOGRAPHICS_FORM";
			self.account = options.account;

			self.demographicsErrors = {};

			// we don't operate on demographicsConfig so safe to turn to JSON now.
			self.demographicsConfig = options.demographicsConfig.toJSON().config;

			self.demographicsData = options.demographicsData;

			// TODO | lots of extra stuff in here; take it apart
			self.model = new DemoFormModel(self.demographicsData);

			self.actor = options.actor;

			self.quote = options.quote;

			self.quotingEnabled = options.applicationConfig.get("quotingEnabled");
			
			self.collection = self.demographicsData.currentMembers;

			if (self.collection.length == 0) {
					// add an empty default member
					self.addDefaultMember();
				}
			
			/*NE:Change SHSBCBSNE-368:*/
			if(self.account.get("userRole")){
					self.model.set("userRole",self.account.get("userRole"))				
			}
			if(self.actor.get("isInternalUser")){
				self.model.set("isInternalUser", self.actor.get("isInternalUser"))
			}
			//Changes for SHSBCBSNE-6
			if (self.account.get("coverageType")) {
				self.model.set("coverageType", self.account.get("coverageType"));
			}
			// set zipCode and CountyId if they exist in the account
			if (self.model.get("zipCode")) {
				this.updateZipCodeData(self.model.get("zipCode"), this.updateCountySelectionDisplay);
			}
			
			if (self.account.get("effectiveDate1")) {
				self.model.set("effectiveDate", self.account.get("effectiveDate1"));
			}
			
			if (self.account.get("effectiveDate")) {
				self.demographicsData.effectiveDate = self.account.get("effectiveDate");
			} else if (self.account.get("coverageDate")) {
				self.demographicsData.effectiveDate = self.account.get("coverageDate");
			} else {
				self.demographicsData.effectiveDate = self.demographicsConfig.effectiveDate;
			}

			//<Changes for SHSBCBSNE-100 by Priyanka M>
			if (self.account.get("childOnly")) {
				self.model.set("childOnly", self.account.get("childOnly"));					
			}
			//</Changes for SHSBCBSNE-100 by Priyanka M>
			self.collection.on('add', this.render, this);
			self.collection.on('remove', this.render, this);

			this.listenTo(this.options.eventManager, 'view:removeFormErrorMessages', this.removeFormErrorMessages);
			this.listenTo(this.options.eventManager, 'view:removeErrorMessagesForErrorSet', this.removeErrorMessagesForErrorSet);
			this.listenTo(this.options.eventManager, 'view:addErrorMessage', this.addErrorMessage);
			this.listenTo(this.options.eventManager, 'view:removeErrorMessages', this.removeErrorMessages);
			this.listenTo(this.options.eventManager, 'view:checkSubmit', this.checkSubmit);

			Backbone.Validation.bind(this, {
				forceUpdate : true,
				valid : function(view, attr) {

					var errorObj = {};
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.removeErrorMessages(errorObj);
					view.$('[name~="' + attr + '"]').parent().removeClass('hasErrors').removeAttr('data-error');
				},
				invalid : function(view, attr, error) {

					var errorObj = {};
					errorObj.errorMsg = error;
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.addErrorMessage(errorObj, attr);
					view.$('[name~="' + attr + '"]').parent().addClass('hasErrors').attr('data-error', error);
				}
			});
		},
		
		changeInsuranceDate: function(event) {
			var self = this;
			var effDate = $(".effectiveDate").val();
			this.model.set("effectiveDate1", effDate);
			self.account.set({
				"effectiveDate1" : effDate
			});
			
			try {
				var effectiveMs = new Date(effDate).getTime();
				self.account.set("effectiveMs", effectiveMs);
			}catch(e){}
		},
		
		render : function() {
			// Since this view can re-render itself, clean up
			// any sub views we might have
			this.disposeSubViews();
			// render the parent view
			var effStartDate = $('#effectiveDate').val();
			this.model.set("effectiveDate1", this.prepoPulatedEffDate());
			var whosCoveredHTML = this.renderTemplate({
				data : {
					demographicsConfig : this.demographicsConfig,
					demographicsForm : this.model.toJSON(),
					zipCounties : this.zipCounties
				}
			});

			$(this.el).html(whosCoveredHTML);
			//<Changes for SHSBCBSNE-100 by Priyanka M>
			if(this.model.get('childOnly') == 'Yes')
			$(this.el).find('#childOnlyField').attr('checked', 'checked');
			//</Changes for SHSBCBSNE-100 by Priyanka M>
			if(this.model.get('effectiveDate')){
				$('#effectiveDate').val(effStartDate);
			}

			this.displayErrors();

			this.subRender();

			// this.updateZipCodeData(this.model.get("zipCode"), this.updateCountySelectionDisplay);

			if (this.model.get("invalidated")) {
				// show the errors now until they are cleared
				this.model.validate();
			}
                           	
			/*if($('#childOnlyField').is(':checked')){
				
				$("select[name='memberRelationship'] option[value='CHILD']").hide();
				$("select[name='memberRelationship'] option[value='DOMESTIC PARTNER']").hide();
				$("select[name='memberRelationship'] option[value='SPOUSE']").hide();
				$("select[name='memberRelationship'] option[value='BROTHERSISTER']").show();
				$("select[name='memberRelationship'] option[value='STEPBROTHERSISTER']").show();
				
			}
			else{
				
				$("select[name='memberRelationship'] option[value='CHILD']").show();
				$("select[name='memberRelationship'] option[value='DOMESTIC PARTNER']").show();
				$("select[name='memberRelationship'] option[value='SPOUSE']").show();
				$("select[name='memberRelationship'] option[value='BROTHERSISTER']").hide();
				$("select[name='memberRelationship'] option[value='STEPBROTHERSISTER']").hide();	
			}
			*/
			//US:670
			if (this.account.get("coverageType") && ((this.account.get("coverageType")=="DEN" )||(this.account.get("coverageType")=="STH")))
			{
				$("select[name='memberRelationship'] option[value='WARD']").remove();
			}
			//changes for SHSBCBSNE-6
			$('.effectiveDate', this.el).datepicker({				
				constrainInput: true,
				//minDate: this.calendarCurrentDate,

			});
			$('.effectiveDate', this.el).mask("99/99/9999", {placeholder : " "});
			//Changes by Priyanka M
			//$(this.el).find('#effectiveDateWL').val(this.demographicsConfig.effectiveDate);
			//SHSBCBSNE-988;Changing effective date to be picked up from updateEffetiveDate method, this.demographicsConfig has wrong effective date:
			if(this.model.get("effectiveDate1")!="NaN/NaN/NaN/NaN")
			   $(this.el).find('#effectiveDateWL').val(this.model.get("effectiveDate1"));
			else
			   $(this.el).find('#effectiveDateWL').val(this.demographicsConfig.effectiveDate);	
			return this;
		},
		subRender : function() {

			var self = this;

			self.collection.each(function(item) {

				var view = new MemberView({
					model : item,
					demographicsConfig : self.demographicsConfig,
					eventManager : self.options.eventManager
				});

				$('.coverage-table tbody', self.el).append(view.render().el);
				
				//Changes for SHSBCBSNE-5 and 6
				if((self.account.get("coverageType"))=='DEN'){
					if (self.model.get("isSmoker")) {
						self.model.unset("isSmoker");
					}

				$('[name~="isSmoker"]', self.el).attr('disabled', 'disabled');
				}

				// store a reference for disposing later
				self.views.push(view);
			});

		},
		updateEffectiveDate : function(){
			var self = this;
			
			if (self.demographicsData == null || self.demographicsData.eventType == null){
				return;
			}
			var hcrDate = new Date("01/01/2014");
			var currentDate = new Date();
			currentDate.setHours(0,0,0,0);
			var selectedDate = new Date(self.demographicsData.eventDate);
			var eventType = self.demographicsData.eventType;
			if (eventType == "OpenEnrollment") {
				self.setEffDate(selectedDate);				
			}else if (eventType == "Birth"|| eventType == "Adoption" || eventType == "PlacementForAdoption" || eventType == "PlacementInFosterCare") {
				try {
					if(selectedDate < hcrDate){
						self.setEffDate(hcrDate);						
					}else {
						self.setEffDate(selectedDate);						
					}
				}catch(e){}
			}else if(eventType == "Marriage"){
				if(currentDate < hcrDate){
					self.setEffDate(hcrDate)
				}else if(currentDate.getDate() > 1){
					self.setEffDate(self.getFirstDateOfAddedMonths(currentDate, 1));				
				}/*else {
					self.setEffDate(currentDate);						
				}*/
			}else if(eventType == "Move" || eventType.indexOf("LossOfCovg") == 0){
				if (currentDate < hcrDate){
					self.setEffDate(self.getFirstDateOfAddedMonths(hcrDate, 1));						
				}else {
				// start code changes for SHSBCBSNE 1419	
					if(eventType.indexOf("LossOfCovg") == 0){
						if( selectedDate > currentDate) {
							self.setEffDate(self.getCalculatedEffectiveDateBasedonEnrollmentDateLossOfCovg(selectedDate, currentDate));
						}else{
							self.setEffDate(self.getFirstDateOfAddedMonths(currentDate, 1));
						}
					} else {
						self.setEffDate(self.getCalculatedEffectiveDateBasedonEnrollmentDate());
					}
				// end code changes for SHSBCBSNE 1419	
				}
			}
			return self.demographicsData.effectiveDate;
		},
		// start code changes for SHSBCBSNE 1419
		getCalculatedEffectiveDateBasedonEnrollmentDateLossOfCovg : function (selectedDate, currentDate){
			var effDateBasedOnEnrollDate = new Date();
			effDateBasedOnEnrollDate.setDate(1);
				var month = selectedDate.getMonth();
				var year = selectedDate.getFullYear();
			    /*If selected month is december(11) then setting it to (-1) so that
			     * it can be changed to January(0) after doing month+1
			     */
				if (month == 11)
			    {
			    	month = -1
			    	year = year + 1
			    }
			    effDateBasedOnEnrollDate.setMonth(month + 1);
			    effDateBasedOnEnrollDate.setYear(year);
			return effDateBasedOnEnrollDate;
		},
		// end code changes for SHSBCBSNE 1419
		
		getCalculatedEffectiveDateBasedonEnrollmentDate : function (){
			var effDateBasedOnEnrollDate = new Date();
			var date = effDateBasedOnEnrollDate.getDate();
			effDateBasedOnEnrollDate.setDate(1);
			if (date >= 1 && date <= 15) {
				effDateBasedOnEnrollDate.setMonth(effDateBasedOnEnrollDate.getMonth() + 1);				
			}else {
				effDateBasedOnEnrollDate.setMonth(effDateBasedOnEnrollDate.getMonth() + 2);
			}
			return effDateBasedOnEnrollDate;
		},
		
		getFirstDateOfAddedMonths : function (inputDate, monthsToAdd){
			var resultDate = inputDate;
			resultDate.setDate(1);
			resultDate.setMonth(resultDate.getMonth() + monthsToAdd);
			return resultDate;
		},
		setEffDate : function (selectedDate){
			var self = this;
			self.demographicsData.effectiveMs = selectedDate.getTime();
			self.demographicsData.effectiveDate = $.datepicker.formatDate("mm/dd", selectedDate) + "/"  + selectedDate.getFullYear();
		},
		createDefaultDemographicsMemberModel : function(memberData) {

			var self = this;

			var memberModel = this.createDemographicsMemberModel(memberData);
			memberModel.set("default", true);

			// find the initial value for memberRelationship.
			_.each(self.demographicsConfig.demographicFields, function(field) {
				if (field.options) {
					_.each(field.options, function(option) {
						if (option.initialSelection == true) {
							memberModel.set(field.name, option.value);
						}
					});
				}
			});

			return memberModel;
		},
		createDemographicsMemberModel : function(memberData) {
			var memberModel = new DemoMemberModel(memberData);
			memberModel.set("new", true);
			return memberModel;
		},
		updateZipCodeData : function(zipCode, success) {
			var self = this;

			var validZipCodeCountyDataModel = new ValidZipCodeCountyDataModel({
				zipCode : zipCode
			});
			validZipCodeCountyDataModel.fetch({
				silent : true,
				success : function() {
					self.isValidZipCode = validZipCodeCountyDataModel.get("isValidZipCode");
					var counties = new Array();
					 counties = validZipCodeCountyDataModel.get("counties");
					
					self.model.unset("stateKey");

					var validState = false;

					if(counties){
						var statekeys=new Array(counties.length);
						for(i=0;i<counties.length;i++){
							statekeys[i]=counties[i].stateKey;
							
						}
						for(i=0;i<counties.length;i++){
						if(counties[i].stateKey=='NE'){
							validState = true;
							self.model.set("stateKey", 'NE');
						break;
						}	
						else{
							self.model.set("stateKey", counties[0].stateKey);
						}
						}
						

				/*	$.each(counties, function(i,v){

					self.model.set("stateKey", statekeys);

					if(v.stateKey == 'NE')

					validState = true;

					});*/

					}
					
					
					self.zipCounties = null;
					if (self.isValidZipCode  && validState) {
						//Changes by Priyanka M for SHSBCBSNE-7
						//County should display, even if there is only one county
						if (counties.length >= 1) {
							self.zipCounties = counties;
							self.model.set("countiesExist", true);
							self.model.set("noOfCounty",counties.length);
						}
						else {
							self.model.set("countiesExist", false);
							self.model.set("countyId", counties[0].countyId);
						}
					} else {
						self.model.set("countiesExist", false);
						self.model.unset("countyId");
					}

					if (success != undefined && typeof success === 'function') {
						success(self);
					}
				}
			});
		},
		updateZipCounty : function(event) {
			var self = this;
			var zipCountyValue = $(event.currentTarget).val();
			if (zipCountyValue && zipCountyValue != '') {
				var counties = new Array();
				 counties = self.zipCounties;
				 for( i=0; i<counties.length;i++){
					 if(counties[i].countyId==zipCountyValue){
					 if(counties[i].stateKey!='NE'){
						 self.model.set("validState",false);
					 }
					 else{
						 self.model.set("validState",true);
					 }
					 }
				 }
				self.model.set({
					"countyId" : zipCountyValue
				}, {
					validate : true
					
				});
				self.demographicsData.countyId = zipCountyValue;
			} else {
				self.model.unset("countyId");
				delete self.demographicsData.countyId;
			}
		},
		updateZipCode : function(event) {
			var self = this;
			var zipValue = $(event.currentTarget).val();
			self.model.set({
				"zipCode" : zipValue
			}, {
				validate : true
			});
			self.demographicsData.zipCode = zipValue;
		},
		//<Changes for SHSBCBSNE-100 by Priyanka M>
		updateChildOnly :function(event){
			var self = this;
			var childOnly = $(event.currentTarget).val();
			if($('#childOnlyField').is(':checked')){
				$('#disclaimerText').show();
				var removeModel = null;
				// find the model at with the memberRefName
				if(self.collection.length>1){
					
					while(self.collection.length!=1){
						var len=self.collection.length;
				for ( var m = 1; m <= len; m++) {
					
						removeModel = self.collection.at(m);
						self.removeErrorMessagesForErrorSet(m);
						self.collection.remove(removeModel);
				
				
				}
					}
					//fix for Jira defect - 491
					$('#childOnlyField').attr("checked",true);  
					$('#disclaimerText').show();
				}
				}
				else{
				$('#disclaimerText').hide();
				}
			self.model.set("membersLength", self.collection.length);
			   

		if($(event.currentTarget).is(':checked')){
				self.model.set("childOnly", childOnly);
				
				$("select[name='memberRelationship'] option[value='CHILD']").hide();
				$("select[name='memberRelationship'] option[value='DOMESTIC PARTNER']").hide();
				$("select[name='memberRelationship'] option[value='SPOUSE']").hide();
				$("select[name='memberRelationship'] option[value='BROTHERSISTER']").show();
				$("select[name='memberRelationship'] option[value='STEPBROTHERSISTER']").show();
				
			}
			else{
				self.model.set("childOnly", "No");
				
				$("select[name='memberRelationship'] option[value='CHILD']").show();
				$("select[name='memberRelationship'] option[value='DOMESTIC PARTNER']").show();
				$("select[name='memberRelationship'] option[value='SPOUSE']").show();
				$("select[name='memberRelationship'] option[value='BROTHERSISTER']").hide();
				$("select[name='memberRelationship'] option[value='STEPBROTHERSISTER']").hide();	
				
			}
			if($(event.currentTarget).is(':checked'))
				self.model.set("childOnly", childOnly);	
			else
				self.model.set("childOnly", "No");	
		},
		//</Changes for SHSBCBSNE-100 by Priyanka M>
		effectiveDate : function()
		{

        //var self= this;
		var effStartDate = new Date();			
		$.datepicker.formatDate("mm/dd/yyyy", effStartDate);
		//var effStartDate = $(event.currentTarget).val();
		//self.model.set("effectiveDate", effStartDate);

	},
		getCountiesForZip : function(event) {
			var zipValue = $(event.currentTarget).val();

			this.updateZipCodeData(zipValue, this.updateCountySelectionDisplay);
		},
		updateCountySelectionDisplay : function(self) {
			// re-render counties
			var countiesHTML = templateEngine.render('view.whoscovered.counties', {
				demographicsConfig : self.demographicsConfig,
				demographicsForm : self.model.toJSON(),
				zipCounties : self.zipCounties
			});
			$('#_demographicsCounties', self.el).html(countiesHTML);
		},
		addDefaultMember : function() {
			var self = this;

			var newMemberRefId = self.getNextMemberRefId();

			// create the new member data
			// field defaults will be set from configuration
			var newMemberData = {
				memberRefName : newMemberRefId
			};

			var newMember = self.createDefaultDemographicsMemberModel(newMemberData);

			self.collection.add(newMember);

			// set for validation purposes
			self.model.set("membersLength", self.collection.length);
		},
		addNewMember : function() {
			if(!($('#childOnlyField').is(':checked'))){
				
			var self = this;

			var newMemberRefId = self.getNextMemberRefId();

			// create the new member data
			// field defaults will be set from configuration
			var newMemberData = {
				memberRefName : newMemberRefId
			};

			var newMember = self.createDemographicsMemberModel(newMemberData);

			self.collection.add(newMember);

			// set for validation purposes
			self.model.set("membersLength", self.collection.length);
			}
		},
		removeMember : function(event) {

			// create a copy of the current members array, minus the
			// currently removed member. Re-assign the existing members
			// array to the new copy. refresh the view.

			var self = this;

			var memberRefName = $(event.currentTarget).attr("data-id");
			var removeModel = null;
			// find the model at with the memberRefName
			for ( var m = 0; m < self.collection.length; m++) {

				var memberId = self.collection.at(m).get("memberRefName");

				if (memberId == memberRefName) {
					self.removeErrorMessagesForErrorSet(memberId);
					removeModel = self.collection.at(m);
				}
			}

			self.collection.remove(removeModel);

			// set for validation purposes
			self.model.set("membersLength", self.collection.length);
		},
		getNextMemberRefId : function() {

			var self = this;

			var nextMemberRefId = self.collection.length;

			for ( var m = 0; m < self.collection.length; m++) {
				var currentMemberIndex = null;
				if (typeof self.collection.at(m).get("memberRefName") == "string") {
					currentMemberIndex = parseInt(self.collection.at(m).get("memberRefName"), 10);
				} else {
					// TODO Remove this else at some point, as we shouldn't have integer values
					currentMemberIndex = self.collection.at(m).get("memberRefName");
					self.collection.at(m).set("memberRefName", currentMemberIndex.toString());
				}
				if (currentMemberIndex >= nextMemberRefId) {
					nextMemberRefId = currentMemberIndex + 1;
				}
			}

			return nextMemberRefId.toString();
		},
		saveAndContinue : function(event) {
			var self = this;
			var $button = $(event.currentTarget);
			if (!$button.hasClass('_disabled')) {
				$button.addClass('_disabled');
				$button.text('Saving...');

				// reset submitClicked
				self.submitClicked = false;

				self.submitDemographics($button);
			}
		},
		removeFormErrorMessages : function() {
			this.removeErrorMessagesForErrorSet(this.formErrorSetId);
			// actually don't need to re-render here;
			// will cause issues with the continue button
			// this.render();
		},
		
		prepoPulatedEffDate : function(){	
			var effStartDate = null;
			if (this.demographicsData!=null && 
					this.demographicsData.eventType != null){
				effStartDate = new Date(this.updateEffectiveDate());	
				this.calendarCurrentDate = effStartDate;				
			}else if(this.account.get("effectiveDate1")!=null){	
				effStartDate = new Date(this.account.get("effectiveDate1"));	
				this.calendarCurrentDate = effStartDate;
			}/*else {
				effStartDate = new Date();	
				var self = this;//for allowing current date as default date in case of STH
				if (self.account.get("coverageType")!="STH"){
					effStartDate.setMonth(effStartDate.getMonth() + 1, 1);	} 
				else
				effStartDate.setDate(effStartDate.getDate() + 1);
				this.calendarCurrentDate = effStartDate;	
			}*/			
			if(effStartDate!=null)
				return $.datepicker.formatDate("mm/dd", effStartDate) + "/"  + effStartDate.getFullYear();
			else {
				var effStartDate = new Date($("#effectiveDate").val());									
				return $.datepicker.formatDate("mm/dd/yy", effStartDate)+ "/"  + effStartDate.getFullYear();
			}
				
		},	
		
		submitDemographics : function($button) {
			var self = this;

			$('input').blur();

			// 1. reset form-level validation (rules engine validation results)
			self.removeFormErrorMessages();

			// only allow submission if member data is valid.
			var basicValidationPassed = false;

			// do basic member validation, at the field level
			var allMembersValid = true;

			// self.model.set("members", self.collection.models);
			self.model.set("membersLength", self.collection.length);

			var formValid = self.model.isValid(true);
			if (!formValid) {
				self.model.set("invalidated", true);
			} else {
				self.model.unset("invalidated");
			}

			_.each(self.collection.models, function(member) {
				// allMembersValid = member.isValid(true) && allMembersValid;
				var memberValid = member.isValid(true);
				if (!memberValid) {
					member.set("invalidated", true);
				} else {
					member.unset("invalidated");
				}
				allMembersValid = memberValid && allMembersValid;
			});

			// take allMembersValid into account
			basicValidationPassed = (formValid && allMembersValid);

			if (basicValidationPassed) {

				// get memberRelationship dataGroupType
				_.each(self.collection.models, function(member) {

					var selectedMemberRelationship = member.get("memberRelationship");

					// given each selected memberRelationship, set the dataGroupType
					_.each(self.demographicsConfig.demographicFields, function(field) {
						if (field.name == "memberRelationship")
							if (field.options) {
								_.each(field.options, function(option) {
									if (option.value == selectedMemberRelationship) {
										member.set("relationshipKey", option.dataGroupType);
									}
								});
							}
					});
				});

				// handle rules engine validation

				var params = {
					zipCode : self.model.get("zipCode")
				};

				params.effectiveDate = self.demographicsData.effectiveDate;

				if (self.demographicsData.eventType) {
					params.eventType = self.demographicsData.eventType;
					params.eventDate = self.demographicsData.eventDate;
				}

				var validationModel = new DemographicsValidationModel(params);

				var demographicsValidationData = {
					members : self.collection.toJSON()
				};

				// Set the data we want to validate to the model
				validationModel.set({
					members : demographicsValidationData.members
				});

				// now call our validate method, which calls the rules engine
				// for rule validation
				validationModel.validateDemographics({
					success : function() {
						if (validationModel.get("hasErrors") && validationModel.get("hasErrors") == true) {

							var errors = validationModel.get("errors");

							_.each(errors, function(error) {
								var errorObj = {
									errorMsg : error.label,
									modelId : self.formErrorSetId
								};
								self.addErrorMessage(errorObj);
							});
							// this should reset the buttons
							self.render(); // re-applies errors to row data

						} else {
							// Update the account for any eventType
							if (self.demographicsData.eventType !== undefined) {
								self.account.set("eventType", self.demographicsData.eventType);
								self.account.set("eventDate", self.demographicsData.eventDate);

								if (self.demographicsData.effectiveDate) {
									self.account.set("effectiveMs", self.demographicsData.effectiveMs);
									self.account.set("effectiveDate", self.demographicsData.effectiveDate);
								}
							}
							// Update the account for the lack of any eventType
							else {
								self.account.unset("eventType");
								self.account.unset("eventDate");

								// Reset to the default effectiveDate
								if (self.account.get("coverageType") && ((self.account.get("coverageType")=="DEN" )||(self.account.get("coverageType")=="STH")))
								{


										var effDate = new Date($("#effectiveDate").val());									
										var date1 = $.datepicker.formatDate("mm/dd/yy", effDate);
										self.account.set("coverageDate", date1);	
										//Start changes for SHSBCBSNE-1306
										self.account.set("effectiveDate", date1);	
										//End changes for SHSBCBSNE-1306
										var effectiveMs = new Date(date1).getTime();
										self.account.set("effectiveMs", effectiveMs);
										self.model.set("effectiveDate1",date1);
								}								
								else
								{
								//if (self.account.get("coverageDate")) {
								//	self.demographicsData.effectiveDate = self.account.get("coverageDate");
							//	} else {
									var covDate = self.demographicsConfig.effectiveDate;
									self.demographicsData.effectiveDate = self.demographicsConfig.effectiveDate;
									self.account.set("coverageDate", covDate);
							//	}

								var effectiveMs = new Date(self.demographicsData.effectiveDate).getTime();
								self.account.set("effectiveMs", effectiveMs);
								self.account.set("effectiveDate", self.demographicsData.effectiveDate);
							}}

							// valid, then set demographics data into the account
							self.account.set({
								"zipCode" : self.model.get("zipCode")
							}, {
								silent : true
							});

							//<Changes for SHSBCBSNE-100 by Priyanka M>
							self.account.set({
								"childOnly" : self.model.get("childOnly")
							}, {
								silent : true
							});
							//</Changes for SHSBCBSNE-100 by Priyanka M>
							if (self.model.get("countyId")) {
								self.account.set({
									"countyId" : self.model.get("countyId")
								}, {
									silent : true
								});
							} else {
								self.model.unset("countyId");
							}
							self.account.set({
								"members" : demographicsValidationData.members
							}, {
								silent : true
							});
							// TODO: verify this is what I want to do here
							// self.options.eventManager.trigger('component:accountUpdated', {}, 'members');
							// self.options.eventManager.trigger('component:accountUpdated', {}, 'zipCode');

							// proceed
							self.account.save({}, {
								// the finishing of the who's covered event will trigger update events
								silent : true,
								success : function() {
									var url = $.url.parse();
									var hasTransactionId = url.params && url.params.hasOwnProperty("transactionId");
									if (hasTransactionId) {
										self.quote.save({}, {
											failure : function() {
												// TODO Display an error to the user?
												// Roll back the toggle?
											}
										});
									}

									// changes by rsingh for user story 228 & 230 
									//self.options.eventManager.trigger('view:whoscovered:finished');
									if ( self.account.get("coverageType") && ( self.account.get("coverageType")=="STH" || self.account.get("coverageType")=="DEN")  ) {
										//self.session.set("coverageType", self.account.get("coverageType"));
										self.options.eventManager.trigger('view:whoscovered:finished', "WF2");
									}else{ //Medical case
										//self.session.set("coverageType", "Med");
										self.options.eventManager.trigger('view:whoscovered:finished');
									}
								}
							});
						}
					},
					failure : function() {
						// alert('unable to validate demographics!');
						if ($button) {
							$button.removeClass('_disabled');
							$button.text('Save & Continue');
						}
					}
				});
				// }

			} else {
				if ($button) {
					$button.removeClass('_disabled');
					$button.text('Save & Continue');
				}
			}
		}
	});

	return WhosCoveredView;
});
