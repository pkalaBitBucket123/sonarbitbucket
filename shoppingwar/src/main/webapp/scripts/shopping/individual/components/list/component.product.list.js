/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main component for the account section to capture user data
 */
define([ 'module', 'backbone', 'templateEngine', 'common/component/component', 'components/list/component.product.list' ], function(module, Backbone, templateEngine, connecture, BaseListComponent) {

	var ProductListComponent = function(configuration) {
		BaseListComponent.call(this, configuration);
	};

	ProductListComponent.prototype = Object.create(BaseListComponent.prototype);

	ProductListComponent.prototype.registerEvents = function() {
		BaseListComponent.prototype.registerEvents.call(this);
		
		// handle individual-specific events here:
		
		this.eventManager.on('view:selectForQuote', function(planId) {
			this.models.quote.togglePlanInclusion(planId);

			this._refreshCurrentView();
		}, this);

		// whenever the plans in the quote change, refresh our view
		// because the Add to Quote button needs to be updated.
		this.models.quote.on('change:plans', function() {
			this._refreshCurrentView();
		}, this);
		
	};

	return ProductListComponent;
});