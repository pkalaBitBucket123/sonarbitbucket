/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, backbone, ProxyModel) {
	var DemographicsValidationModel = ProxyModel.extend({
		initialize : function(options) {
			var effectiveDate = options.effectiveDate || '';
			var zipCode = options.zipCode || '';
			var eventType = options.eventType || '';
			var eventDate = options.eventDate || '';

			ProxyModel.prototype.initialize.call(this, {
				storeItem : {
					item : 'demographicsValidation',
					params : [ 
					    { key : "effectiveDate", value : effectiveDate }, 
					    { key : "zipCode", value : zipCode },
					    { key : "eventType", value : eventType },
					    { key : "eventDate", value : eventDate }
					]
				}
			});
		},
		validateDemographics : function(options) {
			this.save({}, options);
		}
	});

	return DemographicsValidationModel;
});