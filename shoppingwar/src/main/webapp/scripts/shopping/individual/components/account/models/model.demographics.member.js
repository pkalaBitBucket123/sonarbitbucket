/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'backbone.validation' ], function(require, Backbone, backboneValidation) {
	
	_.extend(backboneValidation.validators, {
		namePattern : function(value, attr, customValue, model) {

			if (value && value != '') {
				// check that the name matches a special value
				// allow alpha, hyphen, apostrophe and space
				var regExp = new RegExp("^[a-zA-Z-' ][a-zA-Z-' ]*$");

				if (!regExp.test(value)) {

					return this.format(backboneValidation.messages.namePattern, this.formatLabel(attr, model));
				}
			}
		},
		
		// <Changes for SHSBCBSNE-100 by Priyanka M>
		smokerAndChilValidation : function(value, attr, customValue, model) {
			
			
			var effectiveDate = $('#effectiveDateWL').val();
			var mem = $('select[data-id="'+model.get('memberRefName')+'"][name="memberRelationship"]').val();
			var childOnly = $('#childOnlyField').val();
			if(!effectiveDate)
				effectiveDate = new Date();
			else
				effectiveDate = new Date(effectiveDate);
			var age = getAge(value, effectiveDate);
			if(age >= 21 && $('#childOnlyField').is(':checked') )
			{
				return this.format(backboneValidation.messages.childOnly, this.formatLabel(attr, model));
			}
			if(age < 18){
				$('select[data-id="'+model.get('memberRefName')+'"][name="isSmoker"] option[value=""]').show();
				$('select[data-id="'+model.get('memberRefName')+'"][name="isSmoker"] option[value=""]').attr('selected','selected');
			}
			if(age >= 18){
				$('select[data-id="'+model.get('memberRefName')+'"][name="isSmoker"] option[value=""]').hide();
			}
		},
		// </Changes for SHSBCBSNE-100 by Priyanka M>
		ageValidation : function(value, attr, customValue, model) {
			if($('#childOnlyField').is(':checked'))
				return;
			
			var effectiveDate = $('#effectiveDateWL').val();
		
			if(!effectiveDate)
				effectiveDate = new Date();
			else
				effectiveDate = new Date(effectiveDate);
			var age = getAge(value, effectiveDate);
			
			var u =64;
			var l = 19;
			// if(ct == 'DEN')
				// l = 0;
			if(age < l || age>u){
				return this.format(backboneValidation.messages.ageValidation, this.formatLabel(attr, model),l,u);
			}
		},
		DOBcheck : function(value, attr, customValue, model) {
			if(!($('#childOnlyField').is(':checked'))){
			var effectiveDate = $('#effectiveDateWL').val();
			var insuranceEffectiveDate = $('#effectiveDate').val();
			if(insuranceEffectiveDate){
				effectiveDate=insuranceEffectiveDate;
			}
			var mem = $('select[data-id="'+model.get('memberRefName')+'"][name="memberRelationship"]').val();
			if(!effectiveDate)
				effectiveDate = new Date();
			else
				effectiveDate = new Date(effectiveDate);
			var age = getAge(value, effectiveDate);
			var ct = $('input[name="coverageType"]').val();
			if(ct=='DEN'){
				if( (age <19 || age>115) && (mem=='SPOUSE'||mem=='MEMBER'||mem=='DOMESTIC PARTNER') ){
					
					
					
					if(mem=='MEMBER'){
						return 'Primary applicant must be between the ages of 19 and 115 as of effective date. ';
					
					}
					
					if(mem=='SPOUSE'){
						return 'Spouse must be between the ages of 19 and 115 as of effective date. ';
					
					}
					if(mem=='DOMESTIC PARTNER'){
						return 'Ward must be between the ages of 19 and 115 as of effective date. ';
					
					}
				
			}
			}
			else{
				
				if( (age <19 || age>64) && (mem=='SPOUSE'||mem=='MEMBER'||mem=='DOMESTIC PARTNER') ){
				
				
				
						if(mem=='MEMBER'){
							return 'Primary applicant must be between the ages of 19 and 64 as of effective date. ';
						
						}
						
						if(mem=='SPOUSE'){
							return 'Spouse must be between the ages of 19 and 64 as of effective date. ';
						
						}
						if(mem=='DOMESTIC PARTNER'){
							return 'Ward must be between the ages of 19 and 64 as of effective date. ';
						
						}
				
					
			}
			}
				if(mem=='CHILD'){
					if(age>=26){
						return 'Child must be between the ages of 0 and 25 as of effective date.';
					}
				}
				if(mem=='WARD'){
					if(age<26){
						return 'You have selected the relationship of \'Adult Dependent\'. Therefore applicant\'s age must be 26 or over.';
					}
				}
			}
		},
		// </Changes for SHSBCBSNE-170 by Perwaiz Ali>
	tobaccoValidations : function(value, attr, customValue, model) {
			var ct = $('input[name="coverageType"]').val();
			if(ct!='DEN'){
		
			var mem = $('select[data-id="'+model.get('memberRefName')+'"][name="memberRelationship"]').val();
			var memName ;
			
			
			var effectiveDate = $('#effectiveDateWL').val();
			if(!effectiveDate)
				effectiveDate = new Date();
			else
				effectiveDate = new Date(effectiveDate);
			// tobacco
			// var value =
			// $('select[data-id="'+model.get('memberRefName')+'"][name="birthDate"]').val();
			// var value = attr[4].birthDate;
			var value = arguments[4].birthDate; 
			var age = getAge(value, effectiveDate);
			// alert("shopping test");
			/* var name = $('select[data-id="1"').val(); */
			var isSmoke = $('select[data-id="'+model.get('memberRefName')+'"][name="isSmoker"]').val();
			if(age>=18 && isSmoke == ' '){
				if(mem=='MEMBER'){
					memName='Primary'
				}
				else if(mem=='SPOUSE'){
					memName='Spouse'
				}
				else if(mem=='DOMESTIC PARTNER'){
					memName='Ward'
				}
				else if(mem=='CHILD'){
					memName='Child'
						
				}
				else if(mem=='BROTHERSISTER'){
					memName='Brother/Sister'
						
				}
				else if(mem=='STEPBROTHERSISTER'){
					memName='Stepbrother/Stepsister'
						
				}
				else if(mem=='WARD'){
					memName='Adult Dependent'
						
				}
				
				else{
						return this.format(backboneValidation.messages.smokerValidationGen, this.formatLabel(attr, model),memName);
				}
				if(mem=='CHILD'){
					
					var mem1 = $("input[data-id='"+model.get('memberRefName')+"']").val();
					
					// return 'Tobacco information for Child' +'-'+
					// model.get('memberRefName')+' '+ mem1+' ' +'is required
					// for requested coverage.';
					return 'Tobacco information for Child' +'  '+ mem1+' ' +'is required for requested coverage.';
					
				}else{
					 if(mem=='MEMBER'){
						
						return 'Tobacco information for Primary applicant is required for requested coverage.';

					}
					// return 'Tobacco information for Primary applicant is
					// required for requested coverage.';
					return this.format(backboneValidation.messages.smokerValidation, this.formatLabel(attr, model),memName);
				}}
			}
		},
		/*
		 * relationshipCheck : function(value, attr, customValue, model) { var
		 * mem =
		 * $('select[data-id="'+model.get('memberRefName')+'"][name="memberRelationship"]').val();
		 *  } var memName; var childNum=10; var effectiveDate =
		 * $('#effectiveDateWL').val(); if(!effectiveDate)
		 * 
		 * effectiveDate = new Date(); else effectiveDate = new
		 * Date(effectiveDate); // tobacco //var value =
		 * $('select[data-id="'+model.get('memberRefName')+'"][name="birthDate"]').val();
		 * //var value = attr[4].birthDate; var value = arguments[4].birthDate;
		 * var age = getAge(value, effectiveDate); var isSmoke =
		 * $('select[data-id="'+model.get('memberRefName')+'"][name="isSmoker"]').val();
		 * if(age>=18 && isSmoke == ' '){ if(mem=='MEMBER'){ memName='Primary' }
		 * else if(mem=='SPOUSE'){ memName='Spouse' } else if(mem=='DOMESTIC
		 * PARTNER'){ memName='Ward' } else if(mem=='CHILD'){ memName='Child'
		 *  } return this.format(backboneValidation.messages.smokerValidation,
		 * this.formatLabel(attr, model),memName); } }
		 */
		
	});

	_.extend(backboneValidation.messages, {
		namePattern : '{0} may only contain alpha characters (A-Z,-,\' or spaces)'
	});

	Backbone.Validation.configure({
		labelFormatter : 'label'
	});

	var DemographicsMemberModel = Backbone.Model.extend({
		initialize : function() {

		},
		validation : {
			"name.first" : [ {
				namePattern : true,
				required : true,
				maxLength : 25
			} ],
			"birthDate" : {
				required : true,
				dateNotInFuture : true,
				// Changes for SHSBCBSNE-100 by Priyanka M
				// ageValidation : true,
				datePattern : true,
				smokerAndChilValidation : true,
				
				DOBcheck : true
			},
			"memberRelationship" : {
				required : true
			},
			"gender" : {
				required : true
				// for SHSBCBSNE-248
                                //changes for SHSBCBSNE-666,667
				//sameGenderAsPrimary : true
			},
			"isSmoker" :{
				
				tobaccoValidations : true
			}
		},

		labels : {
			"name.first" : 'First Name',
			"dateOfBirth" : 'Birth Date'
		}
	});

	// <Changes for SHSBCBSNE-100 by Priyanka M>
	_.extend(backboneValidation.messages, {
		childOnly : 'You have indicated that this is a child only application. Therefore, all applicants must be under age 21.',
		ageValidation : 'Applicants must be between the ages of {1} and {2} to qualify for this type of coverage.'	,
		smokerValidation : 'Tobacco information for {1} is required for requested coverage.',
		smokerValidationGen : 'Tobacco information is required for requested coverage.',
		smokerchild : 'Tobacco information for child {1} is required for requested coverage. '
	});
	// </Changes for SHSBCBSNE-100 by Priyanka M>
	return DemographicsMemberModel;
});

// <Age calculation>
getAge = function(dob, effdt){
	 var days = daysBetween(dob,effdt);
	 days = days - totalLeapYearDays(new Date(dob), effdt);
	 var age = days/365;
	 return parseInt(age);
}
daysBetween = function( d1, d2 ) {
	  var date1 = new Date(d1);
	  var date2 = null;
	  if(d2 != null && d2 != ''){
		  date2 =  new Date(d2);
	  }else{
		  date2 =  new Date();
	  }
	  // Get 1 day in milliseconds
	  var one_day=1000*60*60*24;

	  // Convert both dates to milliseconds
	  var date1_ms = date1.getTime();
	  var date2_ms = date2.getTime();

	  // Calculate the difference in milliseconds
	  var difference_ms = date2_ms - date1_ms;
	    
	  // Convert back to days and return
	  return Math.round(difference_ms/one_day); 
}
totalLeapYearDays = function(startDate , endDate){  
	 var curYear = endDate.getFullYear();
	 var dobYear = startDate.getFullYear();
	 if(startDate.getMonth()>1)
		 dobYear = dobYear + 1;
	 if(endDate.getMonth() == 0 ||  (endDate.getMonth() == 1 && endDate.getDate() <= 28)){
		 curYear = curYear -1;
	 }
	 var totalDays = 0;
	 for(i=dobYear;i<=curYear;i++){
		 isLeap = new Date(i, 1, 29).getMonth() == 1;
		 if(isLeap)
			 totalDays++;
	 }	 
	 
	 return totalDays;
}
// </Age calculation>
