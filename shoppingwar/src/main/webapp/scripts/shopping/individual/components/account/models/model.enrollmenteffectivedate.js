/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy.model' ], function(require, Backbone, ProxyModel) {
	var SpecialEnrollmentEffectiveDateModel = ProxyModel.extend({
		initialize : function(options) {
			var eventDate = options.eventDate ? options.eventDate : '';
			var eventReason = options.eventReason ? options.eventReason : '';
			
			ProxyModel.prototype.initialize.call(this, {
				storeItem : {
					item : 'enrollmentEffectiveDate',
					params : [ 
					   { key : "eventDate", value : eventDate }, 
					   { key : "eventReason", value : eventReason } 
					]
				}
			});
		}
	});

	return SpecialEnrollmentEffectiveDateModel;
});