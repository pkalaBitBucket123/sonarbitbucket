/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'dust-helpers', 'common/component/component', 'components/account/widgets/widget.costestimator' ], function(require, Backbone, templateEngine, dustHelpers, connecture, BaseCostEstimatorWidget) {

	var IndividualCostEstimatorWidget = function(options) {
		BaseCostEstimatorWidget.call(this, options);
	};

	IndividualCostEstimatorWidget.prototype = Object.create(BaseCostEstimatorWidget.prototype);

	IndividualCostEstimatorWidget.prototype.getMembersForEstimation = function(members) {
		return members;
	};

	IndividualCostEstimatorWidget.prototype.isCoverageSelected = function(member) {
		return true;
	};

	return IndividualCostEstimatorWidget;
});