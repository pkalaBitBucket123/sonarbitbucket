/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'backbone.validation', 'templateEngine', 'common/component/component', 'shopping/individual/components/account/widgets/views/view.widget.demographics.member',
		'shopping/individual/components/account/models/model.demographics.member', 'shopping/individual/components/account/models/model.demographics.form',
		'shopping/individual/components/account/models/model.demographics.validation', 'shopping/individual/components/account/dust/helpers.demographics',
		'shopping/core/components/account/models/model.validzipcodecountydata', 'text!html/individual/components/account/view.whoscovered.dust',
		'text!html/individual/components/account/view.whoscovered.counties.dust', 'jquery.maskedInput', 'common/validation/validation-extensions' ], function(require, Backbone, BackboneValidation,
		templateEngine, connecture, MemberView, DemoMemberModel, DemoFormModel, DemographicsValidationModel, demographicsHelpers, ValidZipCodeCountyDataModel, template, countiesTemplate, maskedInput,
		commonValidation) {

	templateEngine.load(template, "view.whoscovered");
	templateEngine.load(countiesTemplate, "view.whoscovered.counties");

	var WhosCoveredView = connecture.view.Base.extend({
		template : "view.whoscovered",
		events : {
			'click ._whosCoveredEngage ' : 'saveAndContinue',
			'mousedown ._whosCoveredEngage ' : 'submitDown',
			'keyup ._zipCode' : 'getCountiesForZip',
			'change ._zipCode' : 'updateZipCode',
			'change ._zipCounty' : 'updateZipCounty',
		},
		initialize : function(options) {
			var self = this;

			// set a variable for tracking whether the Continue
			// button has been clicked (used to track submit when
			// multiple conflicting events are triggered)
			self.submitClicked = false;

			self.formErrorSetId = "DEMOGRAPHICS_FORM";
			self.account = options.account;

			self.demographicsErrors = {};

			// we don't operate on demographicsConfig so safe to turn to JSON now.
			self.demographicsConfig = options.demographicsConfig.toJSON().config;

			self.demographicsData = options.demographicsData;

			self.model = new DemoFormModel(self.demographicsData);

			self.actor = options.actor;
			
			self.quotingEnabled = options.applicationConfig.get("quotingEnabled");
			
			self.quote = options.quote;

			self.collection = self.demographicsData.currentMembers;

			// set zipCode and CountyId if they exist in the account
			if (self.model.get("zipCode")) {
				this.updateZipCodeData(self.model.get("zipCode"), this.updateCountySelectionDisplay);
			}

			self.collection.on('add', this.render, this);
			self.collection.on('remove', this.render, this);

			this.listenTo(this.options.eventManager, 'view:removeFormErrorMessages', this.removeFormErrorMessages);
			this.listenTo(this.options.eventManager, 'view:removeErrorMessagesForErrorSet', this.removeErrorMessagesForErrorSet);
			this.listenTo(this.options.eventManager, 'view:addErrorMessage', this.addErrorMessage);
			this.listenTo(this.options.eventManager, 'view:removeErrorMessages', this.removeErrorMessages);
			this.listenTo(this.options.eventManager, 'view:checkSubmit', this.checkSubmit);

			Backbone.Validation.bind(this, {
				forceUpdate : true,
				valid : function(view, attr) {

					var errorObj = {};
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.removeErrorMessages(errorObj);
					view.$('[name~="' + attr + '"]').parent().removeClass('hasErrors').removeAttr('data-error');
				},
				invalid : function(view, attr, error) {

					var errorObj = {};
					errorObj.errorMsg = error;
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.addErrorMessage(errorObj, attr);
					view.$('[name~="' + attr + '"]').parent().addClass('hasErrors').attr('data-error', error);
				}
			});
		},
		submitDown : function(event) {
			var self = this;
			self.submitClicked = true;
		},
		render : function() {
			// Since this view can re-render itself, clean up
			// any sub views we might have
			this.disposeSubViews();
			// render the parent view
			var whosCoveredHTML = this.renderTemplate({
				data : {
					demographicsConfig : this.demographicsConfig,
					demographicsForm : this.model.toJSON(),
					zipCounties : this.zipCounties
				}
			});

			$(this.el).html(whosCoveredHTML);

			this.displayErrors();

			this.subRender();

			// this.updateZipCodeData(this.model.get("zipCode"), this.updateCountySelectionDisplay);

			if (this.model.get("invalidated")) {
				// show the errors now until they are cleared
				this.model.validate();
			}

			return this;
		},
		checkSubmit : function() {
			var self = this;
			if (self.submitClicked) {
				self.submitClicked = false;
				$('a.btn_primary', self.el).click();
			}
		},
		subRender : function() {

			var self = this;

			self.collection.each(function(item) {

				var view = new MemberView({
					model : item,
					demographicsConfig : self.demographicsConfig,
					eventManager : self.options.eventManager
				});

				$('.coverage-table tbody', self.el).append(view.render().el);

				// store a reference for disposing later
				self.views.push(view);
			});

		},
		removeErrorMessagesForErrorSet : function(errorSetId) {

			var self = this;

			// would remove all messages associated to a particular
			// logic area based on the id passed in.
			// could be used to remove all errors for a member,
			// or the more practical purpose of resetting all
			// errors assigned to the form in general.

			if (self.demographicsErrors && self.demographicsErrors[errorSetId]) {
				delete self.demographicsErrors[errorSetId];
			}
		},
		removeErrorMessages : function(errorObject) {

			var self = this;

			var modelId = errorObject.modelId;

			if (self.demographicsErrors[modelId]) {

				var modelErrors = self.demographicsErrors[modelId];

				var newModelErrors = new Array();

				if (modelErrors) {
					// see if the current error is already there

					for ( var i = 0; i < modelErrors.length; i++) {
						if (modelErrors[i].attr != errorObject.attr) {
							newModelErrors[newModelErrors.length] = modelErrors[i];
						}
					}
				}

				self.demographicsErrors[modelId] = newModelErrors;
			}

			self.displayErrors();
		},
		addErrorMessage : function(errorObject) {

			var self = this;

			var modelId = errorObject.modelId;

			// try to find existing errors for the modelId
			if (self.demographicsErrors[modelId]) {
				// get array of errors for the model
				var modelErrors = self.demographicsErrors[modelId];
				if (modelErrors) {
					// see if the current error is already there
					var found = false;
					_.each(modelErrors, function(modelError) {
						if (modelError.errorMsg == errorObject.errorMsg) {
							found = true;
						}
					});
					if (!found) {

						var modelErrorsObject = {};
						modelErrorsObject.attr = errorObject.attr;
						modelErrorsObject.errorMsg = errorObject.errorMsg;

						modelErrors[modelErrors.length] = errorObject;
					}

				}
			} else {
				modelErrors = new Array();

				var modelErrorsObject = {};
				modelErrorsObject.attr = errorObject.attr;
				modelErrorsObject.errorMsg = errorObject.errorMsg;

				modelErrors[0] = modelErrorsObject;
				self.demographicsErrors[modelId] = modelErrors;
			}

			self.displayErrors();

		},
		displayErrors : function() {

			var self = this;

			var errorHTML = ''; // if nothing added this will clear out errors.

			var errorSet = new Array();

			// build error messages for display
			_.each(self.demographicsErrors, function(rowErrors) {
				_.each(rowErrors, function(modelError) {

					var errorFound = false;
					// is this error already in the errorSet? if not, add it here
					_.each(errorSet, function(errorMsg) {
						if (errorMsg == modelError.errorMsg) {
							errorFound = true;
						}
					});

					if (!errorFound) {
						errorSet[errorSet.length] = modelError.errorMsg;
					}
				});
			});

			_.each(errorSet, function(errorMsg) {
				errorHTML += '<div>' + errorMsg + '</div>';
			});

			$("._validationErrors", self.el).html(errorHTML);

			if (errorHTML && errorHTML != '') {
				$("._validationErrors", self.el).show();
			} else {
				// just to be safe here..
				$("._validationErrors", self.el).hide();
			}

		},
		createDemographicsMemberModel : function(memberData) {
			var memberModel = new DemoMemberModel(memberData);
			memberModel.set("new", true);
			return memberModel;
		},
		updateZipCodeData : function(zipCode, success) {
			var self = this; 

			var validZipCodeCountyDataModel = new ValidZipCodeCountyDataModel({
				zipCode : zipCode
			});
			validZipCodeCountyDataModel.fetch({
				silent : true,
				success : function() {
					self.isValidZipCode = validZipCodeCountyDataModel.get("isValidZipCode");
					var counties = validZipCodeCountyDataModel.get("counties");
					self.zipCounties = null;
					//Changed by Priyanka M for on-exchange flow
					if (self.isValidZipCode) {
						if (counties.length >= 1) {
							self.zipCounties = counties;
							self.model.set("countiesExist", true);
							self.model.set("noOfCounty",counties.length);
						} else {
							self.model.set("countiesExist", false);
							self.model.set("countyId", counties[0].countyId);
						}
					} else {
						self.model.set("countiesExist", false);
						self.model.unset("countyId");
					}

					if (success != undefined && typeof success === 'function') {
						success(self);
					}
				}
			});
		},
		updateZipCounty : function(event) {
			var self = this;
			var zipCountyValue = $(event.currentTarget).val();
			if (zipCountyValue && zipCountyValue != '') {
				self.model.set({
					"countyId" : zipCountyValue
				}, {
					validate : true
				});
				self.demographicsData.countyId = zipCountyValue;
			} else {
				self.model.unset("countyId");
				delete self.demographicsData.countyId;
			}
		},
		updateZipCode : function(event) {
			var self = this;
			var zipValue = $(event.currentTarget).val();
			self.model.set({
				"zipCode" : zipValue
			}, {
				validate : true
			});
			self.demographicsData.zipCode = zipValue;
		},
		getCountiesForZip : function(event) {
			var zipValue = $(event.currentTarget).val();

			this.updateZipCodeData(zipValue, this.updateCountySelectionDisplay);
		},
		updateCountySelectionDisplay : function(self) {
			// re-render counties
			var countiesHTML = templateEngine.render('view.whoscovered.counties', {
				demographicsConfig : self.demographicsConfig,
				demographicsForm : self.model.toJSON(),
				zipCounties : self.zipCounties
			});
			$('#_demographicsCounties', self.el).html(countiesHTML);
		},
		getNextMemberRefId : function() {

			var self = this;

			var nextMemberRefId = self.collection.length;

			for ( var m = 0; m < self.collection.length; m++) {
				var currentMemberIndex = null;
				if (typeof self.collection.at(m).get("memberRefName") == "string") {
					currentMemberIndex = parseInt(self.collection.at(m).get("memberRefName"), 10);
				} else {
					// TODO Remove this else at some point, as we shouldn't have integer values
					currentMemberIndex = self.collection.at(m).get("memberRefName");
					self.collection.at(m).set("memberRefName", currentMemberIndex.toString());
				}
				if (currentMemberIndex >= nextMemberRefId) {
					nextMemberRefId = currentMemberIndex + 1;
				}
			}

			return nextMemberRefId.toString();
		},
		saveAndContinue : function(event) {
			var self = this;

			var $button = $(event.currentTarget);
			if (!$button.hasClass('_disabled')) {
				$button.addClass('_disabled');
				$button.text('Saving...');

				// reset submitClicked
				self.submitClicked = false;

				self.submitDemographics($button);
			}
		},
		removeFormErrorMessages : function() {
			this.removeErrorMessagesForErrorSet(this.formErrorSetId);
			// actually don't need to re-render here;
			// will cause issues with the continue button
			// this.render();
		},
		submitDemographics : function($button) {
			var self = this;

			$('input').blur();

			// 1. reset form-level validation (rules engine validation results)
			self.removeFormErrorMessages();

			// only allow submission if member data is valid.
			var basicValidationPassed = false;

			// do basic member validation, at the field level
			var allMembersValid = true;

			// self.model.set("members", self.collection.models);
			self.model.set("membersLength", self.collection.length);

			var formValid = self.model.isValid(true);
			if (!formValid) {
				self.model.set("invalidated", true);
			} else {
				self.model.unset("invalidated");
			}

			_.each(self.collection.models, function(member) {
				// allMembersValid = member.isValid(true) && allMembersValid;
				var memberValid = member.isValid(true);
				if (!memberValid) {
					member.set("invalidated", true);
				} else {
					member.unset("invalidated");
				}
				allMembersValid = memberValid && allMembersValid;
			});

			// take allMembersValid into account
			basicValidationPassed = (formValid && allMembersValid);

			if (basicValidationPassed) {

				// get memberRelationship dataGroupType
				_.each(self.collection.models, function(member) {

					var selectedMemberRelationship = member.get("memberRelationship");

					// given each selected memberRelationship, set the dataGroupType
					_.each(self.demographicsConfig.demographicFields, function(field) {
						if (field.name == "memberRelationship")
							if (field.options) {
								_.each(field.options, function(option) {
									if (option.value == selectedMemberRelationship) {
										member.set("relationshipKey", option.dataGroupType);
									}
								});
							}
					});
				});

				// handle rules engine validation

				var params = {
					zipCode : self.model.get("zipCode")
				};

				params.effectiveDate = self.demographicsData.effectiveDate;

				if (self.demographicsData.eventType) {
					params.eventType = self.demographicsData.eventType;
					params.eventDate = self.demographicsData.eventDate;
				}

				var validationModel = new DemographicsValidationModel(params);

				var demographicsValidationData = {
					members : self.collection.toJSON()
				};

				// Set the data we want to validate to the model
				validationModel.set({
					members : demographicsValidationData.members
				});

				// now call our validate method, which calls the rules engine
				// for rule validation
				validationModel.validateDemographics({
					success : function() {
						if (validationModel.get("hasErrors") && validationModel.get("hasErrors") == true) {

							var errors = validationModel.get("errors");

							_.each(errors, function(error) {
								var errorObj = {
									errorMsg : error.label,
									modelId : self.formErrorSetId
								};
								self.addErrorMessage(errorObj);
							});
							// this should reset the buttons
							self.render(); // re-applies errors to row data

						} else {
							// Update the account for any eventType
							if (self.demographicsData.eventType !== undefined) {
								self.account.set("eventType", self.demographicsData.eventType);
								self.account.set("eventDate", self.demographicsData.eventDate);

								if (self.demographicsData.effectiveDate) {
									self.account.set("effectiveMs", self.demographicsData.effectiveMs);
									self.account.set("effectiveDate", self.demographicsData.effectiveDate);
								}
							}
							// Update the account for the lack of any eventType
							else {
								self.account.unset("eventType");
								self.account.unset("eventDate");

								// Reset to the default effectiveDate
								if (self.account.get("coverageDate")) {
									self.demographicsData.effectiveDate = self.account.get("coverageDate");
								} else {
									self.demographicsData.effectiveDate = self.demographicsConfig.effectiveDate;
								}

								var effectiveMs = new Date(self.demographicsData.effectiveDate).getTime();
								self.account.set("effectiveMs", effectiveMs);
							}

							// valid, then set demographics data into the account
							self.account.set({
								"zipCode" : self.model.get("zipCode")
							}, {
								silent : true
							});
							if (self.model.get("countyId")) {
								self.account.set({
									"countyId" : self.model.get("countyId")
								}, {
									silent : true
								});
							} else {
								self.model.unset("countyId");
							}
							self.account.set({
								"members" : demographicsValidationData.members
							}, {
								silent : true
							});
							// TODO: verify this is what I want to do here
							// self.options.eventManager.trigger('component:accountUpdated', {}, 'members');
							// self.options.eventManager.trigger('component:accountUpdated', {}, 'zipCode');

							// proceed
							self.account.save({}, {
								// the finishing of the who's covered event will trigger update events
								silent : true,
								success : function() {
									var url = $.url.parse();
									var hasTransactionId = url.params && url.params.hasOwnProperty("transactionId");
									if (hasTransactionId && self.quotingEnabled) {
										self.quote.save({}, {
											failure : function() {
												// TODO Display an error to the user?
												// Roll back the toggle?
											}
										});
									}
									
									self.options.eventManager.trigger('view:whoscovered:finished');
								}
							});
						}
					},
					failure : function() {
						// alert('unable to validate demographics!');
						if ($button) {
							$button.removeClass('_disabled');
							$button.text('Save & Continue');
						}
					}
				});
				// }

			} else {
				if ($button) {
					$button.removeClass('_disabled');
					$button.text('Save & Continue');
				}
			}
		}
	});

	return WhosCoveredView;
});