/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/widget', 'components/account/widgets/views/view.widget.demographics',
		'shopping/individual/components/account/widgets/views/view.widget.demographics.onexchange', 'components/account/widgets/widget.demographics' ], function(require, Backbone, Widget,
		DemographicsView, DemographicsOnExchangeView, BaseDemographicsWidget) {
	/**
	 * This widget will have two views and thus two states really.
	 * 
	 * We need a way to push out that this widget has to re-render based on state changes
	 * 
	 * Maybe the workflow item , listens to the widget and and reacts to a state change
	 */
	var DemographicsWidget = function(configuration) {
		BaseDemographicsWidget.call(this, configuration);
	};

	DemographicsWidget.prototype = Object.create(BaseDemographicsWidget.prototype);

	DemographicsWidget.prototype.getView = function(demographicsData) {
		
		var session = this.attributes.session;

		var onExchange = this.model.get("onExchange");

		if (this.isPrimary) {
			var viewData = this.attributes;
			viewData.demographicsConfig = this.model;
			viewData.eventManager = this.eventManager;
			viewData.demographicsInitialized = this.demographicsInitialized;

			viewData.missingValues = session.get("missingValues");
			
			viewData.applicationConfig = this.attributes.applicationConfig;
	
			if (demographicsData) {
				viewData.demographicsData = demographicsData;
			}
			if (onExchange) {
				// on exchange, disabled view
				this.view = new DemographicsOnExchangeView(viewData);
			} else {
				// normal view
				this.view = new DemographicsView(viewData);
			}
		} else {
			// empty view
			this.view = null;
		}

		return this.view;
	};

	DemographicsWidget.prototype.destroy = function() {
		if (this.view) {
			this.view.remove();
		}
	};

	return DemographicsWidget;
});