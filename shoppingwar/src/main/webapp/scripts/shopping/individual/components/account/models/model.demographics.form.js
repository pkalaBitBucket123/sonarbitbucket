/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'backbone.validation' ], function(require, Backbone, backboneValidation) {

	_.extend(backboneValidation.validators, {
		countySelected : function(value, attr, customValue, model) {
			
			var countiesExist = model.get("countiesExist");
                        // Changes by Priyanka M for SHSBCBSNE-7
			var noOfCounty=model.get("noOfCounty");
			 var validState = model.get("validState");
			// check for existing but empty attribute
			if (countiesExist == true) {
				if (!value || value == "") {
					// countyRequired and if condition added by Priyanka M for SHSBCBSNE-7
						if(noOfCounty!=1){
					return this.format(backboneValidation.messages.countyRequired, this.formatLabel(attr, model));
				}
				}
				else if(validState==false){
					return this.format(backboneValidation.messages.OOAZipCode, this.formatLabel(attr, model)); 
				}
				
			}
		},
		dateNotMore90Days : function(value, attr, customValue, model) {
			var ct = $('input[name="coverageType"]').val();
			var daysAllowed;
			if(ct=='DEN'|| ct=='STH'){
			// handles both date formatting and future date check
			var effDate = new Date($("#effectiveDate").val());									
			var date1 = $.datepicker.formatDate("mm/dd/yy", effDate);
			if (date1 && date1 != '') {

				// check that the name matches a special value
				var regExp = new RegExp("^(0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[01])/(19|20)\\d\\d$");

				var now = new Date();
				// start by rsingh fixes Jira 727
				now.setHours(0,0,0,0);
				// end by rsingh fixes Jira 727
                var nextDate = new Date();
               /*NE:Change SHSBCBSNE-368: Begin*/
                if(model.has("userRole")){
                	//Removed condition on 2014 date for SHSBCBSNE-1485
    				if(model.get("userRole").indexOf("fb")==0){
    					return this.format("No plans available", this.formatLabel(attr, model));
    				}
                }
                /*NE:Change SHSBCBSNE-1300,1330: Begin*/
    				if(ct == 'STH'){
    					nextDate.setDate(now.getDate()+60);
    					daysAllowed = 60;
    				}
    				else{
    					nextDate.setDate(now.getDate()+90);
    					daysAllowed = 90;
    				}
                /*NE:Change SHSBCBSNE-1300,1330: End*/
                /*NE:Change SHSBCBSNE-368: End*/
				// if (!regExp.test(dateValue)) {
				if (!regExp.test(date1)) {
					/*return this.format(Backbone.Validation.messages.datePattern, this.formatLabel(attr, model));*/

				} else {

					var selectedDate = new Date(date1);
					
					if (selectedDate > nextDate || selectedDate < now ) {

						// check if the date is in the future
						return this.format(Backbone.Validation.messages.dateNotMore90Or60Days, this.formatLabel(attr, model), daysAllowed);

					}
				}
			}
			}
		},				
		OOAZipCode : function(value, attr, customValue, model) {

			var validState = model.get("stateKey"); 
			
			/*for(i=0;i<validState.length;i++){
				if(validState[i]=='NE'){
					validState='NE';
					break;
				}
						
			}*/
			// check for existing but empty attribute

			if (validState && validState != 'NE') {

				return this.format(backboneValidation.messages.OOAZipCode, this.formatLabel(attr, model)); 

			}

		}	,
		
dateReqd : function(value, attr, customValue, model) {
			
	var ct = $('input[name="coverageType"]').val();
	if(ct=='DEN'|| ct=='STH'){
			var date1=$('#effectiveDate').val();
			// check for existing but empty attribute
			
				if (!date1 || date1 == "") {
					
						
					return this.format(backboneValidation.messages.startDateReqd, this.formatLabel(attr, model));
				
				}
	}
		},
	});

	var DemographicsFormModel = Backbone.Model.extend({
		initialize : function() {

		},
		validation : {
			"zipCode" : {
				required : true,
				minLength : 5,
				maxLength : 5,
				integer : true,
				OOAZipCode:true
			},
			"countyId" : {
				countySelected : true
			},
			"membersLength" : {
				atLeastOne : true
			},
			"effectiveDate" :{
				dateReqd : true,
				dateNotMore90Days :true
			}			
		},

		labels : {
			"zipCode" : 'Zip code',
			"countyId" : 'County',
			"membersLength" : 'member',
			"effectiveDate" : 'Insurance Start Date'
		}
	});
_.extend(backboneValidation.messages, {
		
		
		OOAZipCode : 'Sorry, the zip code you have entered is not within our service area.'

		});
	return DemographicsFormModel;
});