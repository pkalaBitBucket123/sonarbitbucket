/**
 * Copyright (C) 2012 Connecture
 */
define([ 'require', 'backbone', 'components/account/views/view.account.questionnaire.members.summary' ], function(require, Backbone, BaseQuestionnaireSummaryView) {

	var IndividualQuestionnaireSummaryView = BaseQuestionnaireSummaryView.extend({
		initialize : function(options) {
			BaseQuestionnaireSummaryView.prototype.initialize.call(this, options);
		}
	});
	
	IndividualQuestionnaireSummaryView.prototype = Object.create(BaseQuestionnaireSummaryView.prototype);

	IndividualQuestionnaireSummaryView.prototype.isMemberCovered = function(member) {
		return true;
	}

	return IndividualQuestionnaireSummaryView;
});