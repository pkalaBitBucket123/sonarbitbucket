/**
 * Copyright (C) 2012 Connecture
 * 
 */
define(['require', 'backbone', 'templateEngine', 'common/component/component',
		'shopping/individual/components/account/models/model.demographics.member', 'shopping/individual/components/account/models/model.demographics.form',
		'shopping/individual/components/account/models/model.demographics.validation', 'shopping/individual/components/account/dust/helpers.demographics', 
		'text!html/shopping/components/account/view.whoscovered.onexchange.dust', 'shopping/individual/components/account/widgets/views/view.widget.demographics.onexchange.member', 
		'jquery.maskedInput', 'shopping/individual/components/account/widgets/views/view.widget.demographics.base' ], 
		function(require, Backbone, templateEngine, connecture, DemoMemberModel, DemoFormModel, DemographicsValidationModel, demographicsHelpers, template,
			MemberOnExchangeView, maskedInput, BaseWhosCoveredView) {

	templateEngine.load(template, "view.whoscovered.onexchange");
	
	var WhosCoveredView = BaseWhosCoveredView.extend({	
		template : "view.whoscovered.onexchange",
		events : {
			'click ._whosCoveredEngage ' : 'saveAndContinue',
			'mousedown ._whosCoveredEngage ' : 'submitDown',
			'click ._backToFFM' : 'exitToFFM',
			'change ._zipCounty' : 'updateZipCounty'
		},
		initialize : function(options) {
			
			var self = this;

			// set a variable for tracking whether the Continue
			// button has been clicked (used to track submit when
			// multiple conflicting events are triggered)
			self.submitClicked = false;

			self.formErrorSetId = "DEMOGRAPHICS_FORM";
			
			self.account = options.account;

			self.actor = options.actor;

			self.quote = options.quote;
			
			self.quotingEnabled = options.applicationConfig.get("quotingEnabled");
			
			self.demographicsErrors = {};

			// we don't operate on demographicsConfig so safe to turn to JSON now.
			self.demographicsConfig = options.demographicsConfig.toJSON().config;

			self.demographicsData = options.demographicsData;

			self.model = new DemoFormModel(self.demographicsData);

			self.missingValues = self.options.missingValues;
			
			self.backToFFMURL = this.options.applicationConfig.get("ffmPriorToApplyURL");		
		
			self.collection = self.demographicsData.currentMembers;

			// set zipCode and CountyId if they exist in the account
			if (self.model.get("zipCode")) {
				this.updateZipCodeData(self.model.get("zipCode"), this.updateCountySelectionDisplay);
			}

			self.collection.on('add', this.render, this);
			self.collection.on('remove', this.render, this);

			this.listenTo(this.options.eventManager, 'view:removeFormErrorMessages', this.removeFormErrorMessages);
			this.listenTo(this.options.eventManager, 'view:removeErrorMessagesForErrorSet', this.removeErrorMessagesForErrorSet);
			this.listenTo(this.options.eventManager, 'view:addErrorMessage', this.addErrorMessage);
			this.listenTo(this.options.eventManager, 'view:removeErrorMessages', this.removeErrorMessages);
			this.listenTo(this.options.eventManager, 'view:checkSubmit', this.checkSubmit);

			Backbone.Validation.bind(this, {
				forceUpdate : true,
				valid : function(view, attr) {

					var errorObj = {};
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.removeErrorMessages(errorObj);
					view.$('[name~="' + attr + '"]').parent().removeClass('hasErrors').removeAttr('data-error');
				},
				invalid : function(view, attr, error) {

					var errorObj = {};
					errorObj.errorMsg = error;
					errorObj.attr = attr;
					errorObj.modelId = self.formErrorSetId;

					self.addErrorMessage(errorObj, attr);
					view.$('[name~="' + attr + '"]').parent().addClass('hasErrors').attr('data-error', error);
				}
			});
					
			// check if primary missing
			var primaryFound = false;
			var relationshipDemoField = null;
			
			// 1. find a PRIMARY value from the demographicsConfig
			var primaryValue = null;
			_.every(self.demographicsConfig.demographicFields, function(demoField){
				if (demoField.key === "RELATIONSHIP"){
					_.every(demoField.options, function(demoOption){
						if (demoOption.dataGroupType === "PRIMARY"){
							relationshipDemoField = demoField;
							primaryValue = demoOption.value;
						}
						return !primaryValue;
					});
				}
				return !primaryValue;
			});
			
			// 2. if the value was found, then we have a primary configured.
			// try to find a member with primary set
			if (primaryValue){
				_.every(self.collection.models, function(member){
					if (member.get("memberRelationship") === primaryValue){
						primaryFound = true;
					}
					return !primaryFound;
				});
			}
			
			if (!primaryFound){
				relationshipDemoField.enabled = true;
			}			
		},
		render : function() {
			// render the parent view
			var whosCoveredHTML = this.renderTemplate({
				data : {
					demographicsConfig : this.demographicsConfig,
					demographicsForm : this.model.toJSON(),
					zipCounties : this.zipCounties,
					members : this.collection.toJSON(),
					backToFFMURL : this.backToFFMURL
				}
			});

			$(this.el).html(whosCoveredHTML);

			this.displayErrors();

			this.subRender();

			if (this.model.get("invalidated")) {
				// show the errors now until they are cleared
				this.model.validate();
			}
			
			return this;
		}
	});

	WhosCoveredView.prototype.subRender = function(){
		
		var self = this;

		self.collection.each(function(item) {

			var view = new MemberOnExchangeView({
				model : item,
				demographicsConfig : self.demographicsConfig,
				missingValues : self.missingValues,
				eventManager : self.options.eventManager
			});

			$('.coverage-table tbody', self.el).append(view.render().el);

			// store a reference for disposing later
			self.views.push(view);
		});
	};
	
	WhosCoveredView.prototype.exitToFFM = function(){
		
		var self = this;			
		self.options.eventManager.trigger('exit', {
			key : 'ffm',
			location : self.backToFFMURL,
			params : {
				toFFM : true
			}
		});
		
	};

	return WhosCoveredView;
});