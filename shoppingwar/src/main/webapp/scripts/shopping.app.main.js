/**
 * Copyright (C) 2012 Connecture
 * 
 * This is the core configuration file for shopping
 */
requirejs.config({
	waitSeconds : 120,
	// because the accessing HTML pages all now must go
	// under root/shopping/ we need to move the base url as well so
	// everything falls into place
	baseUrl : '../scripts',
	paths : {
		'html' : '../html/',
		'html/shopping' : '../html/shopping/core',
		'common' : 'common',
		'app' : 'shopping/core/app',
		'components' : 'shopping/core/components',
		'shopping/common' : 'shopping/core/common',
		'shopping/dust' : 'shopping/core/dust',
		'jquery' : 'thirdparty/jquery-1.10.1',
		'jquery-ui' : 'thirdparty/jquery-ui-1.10.1.custom',
		'jquery-formatCurrency' : 'thirdparty/jquery.formatCurrency-1.4.0.min',
		'jquery-printElement' : 'thirdparty/jquery.printElement.custom',
		'dust' : 'thirdparty/dust-full-1.1.1',
		'dust-helpers' : 'thirdparty/dust-helpers-1.1.0',
		'json2' : 'thirdparty/json2',
		'jquery.strutsurl' : 'common/jquery/plugins/jquery.strutsurl',
		'backbone' : 'thirdparty/backbone',
		'underscore' : 'thirdparty/underscore',
		'connecture' : 'common/connecture',
		'es5-shim' : 'thirdparty/es5-shim',
		'dust-extensions' : 'common/dust/dust-extensions',
		'jquery.charts' : 'common/jquery/plugins/jquery.charts',
		'templateEngine' : 'common/template',
		'jquery.url' : 'thirdparty/jquery.urldecoder',
		'jquery.equalHeight' : 'common/jquery/plugins/jquery.equalHeight',
		'amplify' : 'thirdparty/amplify',
		'backbone.deep-model' : 'thirdparty/backbone.deep-model',
		'backbone.validation' : 'thirdparty/backbone-validation-amd',
		'backbone.paginator' : 'thirdparty/backbone.paginator-0.8.1',
		'backbone.mixin' : 'common/backbone/backbone.mixin',
		'underscore.deep-extend' : 'thirdparty/underscore.mixin.deepExtend',
		'log4javascript' : 'thirdparty/log4javascript',
		'log' : 'common/logger',
		'jquery.selectToUISlider' : 'thirdparty/selectToUISlider.jQuery.custom',
		'jquery.maskedInput' : 'thirdparty/jquery.maskedinput',
		'jquery.blockUI' : 'thirdparty/jquery.blockUI-2.59.0',
		'date' : 'thirdparty/date',
		// define the base widget definition here, might make sense to move to context apps instead
		'widget/definition' : 'shopping/core/common/widgets/widget.definition',
		'workflow/definition' : 'shopping/core/common/workflows/workflow.definition'
	},
	shim : {
		'jquery-ui' : {
			deps : [ 'jquery' ]
		},
		'jquery-formatCurrency' : {
			deps : [ 'jquery' ]
		},
		'jquery-printElement' : {
			deps : [ 'jquery' ]
		},
		'jquery.charts' : {
			deps : [ 'jquery', 'connecture' ]
		},
		'dust' : {
			exports : 'dust'
		},
		'dust-helpers' : {
			deps : [ 'dust' ]
		},
		// This requires dust-helpers because we are adding
		// to that
		'dust-extensions' : {
			deps : [ 'dust', 'dust-helpers' ]
		},
		'json2' : {
			exports : 'JSON'
		},
		'jquery.strutsurl' : {
			deps : [ 'jquery' ]
		},
		'jquery.url' : {
			deps : [ 'jquery' ]
		},
		'jquery.equalHeight' : {
			deps : [ 'jquery' ]
		},
		'jquery.maskedInput' : {
			deps : [ 'jquery' ]
		},
		'underscore' : {
			exports : '_'
		},
		'backbone' : {
			deps : [ 'underscore', 'jquery' ],
			exports : 'Backbone'
		},
		'jquery.selectToUISlider' : {
			deps : [ 'jquery', 'jquery-ui' ]
		},
		'common/connecture.ui' : {
			deps : [ 'connecture' ]
		},
		'amplify' : {
			deps : [ 'jquery' ],
			exports : 'amplify'
		},
		'underscore.deep-extend' : {
			deps : [ 'underscore' ]
		},
		'log4javascript' : {
			exports : 'log4javascript'
		},
		'backbone.paginator' : {
			deps : [ 'backbone' ],
			exports : 'Backbone'
		}
	},
	packages : [ 'shopping/employee/app', 'shopping/individual/app', 'shopping/anonymous/app' ],
	config : {
		'components/list/component.product.list' : {
			id : 'b718c2e2-2119-4053-ad7b-99b1daa45be6',
			items : [ {
				alias : 'plans',
				dependency : true,
			}, {
				alias : 'planConfig',
				dependency : true,
			}, {
				alias : 'filters',
				dependency : true
			}, {
				alias : 'productLines',
				dependency : true,
			}, {
				alias : 'widgets',
				dependency : true,
			}, {
				alias : 'account',
				dependency : true,
				share : true
			}, {
				alias : 'applicationConfig',
				dependency : true
			}, {
				alias : 'accountHolder',
				dependency : true
			}, {
				alias : 'actor',
				dependency : true
			}, {
				alias : 'questionEvaluations',
				dependency : true,
			} ],
			// set process to realtime ...go out fetch plans and update product list
			// set displayPlans to complete...
			startup : 'hide', // show
			process : 'realtime', // realtime (whenever an event comes in, process the data), complete (only process the data for a complete event)
			show : 'complete', // startup, available (once they get fetched), complete (event)
			defaultView : 'ProductList'
		},
		'components/cart/component.cart' : {
			id : '211ce84d-9dec-4d8e-8cc5-c77a44f6a9be',
			items : [ {
				alias : 'account',
				dependency : true,
			}, {
				alias : 'plans',
				dependency : true,
			}, {
				alias : 'productLines',
				dependency : true
			}, {
				alias : 'applicationConfig',
				dependency : true,
			}, {
				alias : 'widgets',
				dependency : true,
			} ]
		},
		'components/menu/component.menu' : {
			id : '513211f5-ca6c-47c9-9094-1b3a208f4b38',
			items : [ {
				alias : 'account',
				dependency : true,
			} ]
		},
		'components/account/component.account' : {
			id : 'cbb55234-1fac-4ab0-920c-605d9ac0c032',
			items : [ {
				alias : 'applicationConfig',
				dependency : true,
			}, {
				alias : 'widgets',
				dependency : true,
			}, {
				alias : 'costEstimatorConfig',
				dependency : true,
			}, {
				alias : 'productLines',
				dependency : true,
			}, {
				alias : 'eligibility',
				dependency : true
			}, {
				alias : 'relationships',
				dependency : true,
			}, {
				alias : 'genders',
				dependency : true,
			}, {
				alias : 'account',
				dependency : true
			}, {
				alias : 'actor',
				dependency : true
			}, {
				alias : 'workflowItems',
				dependency : true
			}, {
				alias : 'workflow',
				dependency : true
			} ],
			// complete means that the account is done doing stuff
			// and the plan list can fetch plans or do something
			triggerComplete : 'summary', // whoscovered, method, summary
			component : {
				showHeader : true,
			}
		},
		'components/header/component.header' : {
			items : [ {
				alias : 'applicationConfig',
				dependency : true
			}, {
				alias : 'accountHolder',
				dependency : true
			}, {
				alias : 'account',
				dependency : true
			}, {
				alias : 'actor',
				dependency : true
			} ]
		},
		'components/header/component.app.header' : {
			items : [ {
				alias : 'employer',
				dependency : true,
			} ]
		},
		'components/modal/component.modal.email' : {
			items : [ {
				alias : 'accountHolder',
				dependency : true,
			} ]
		},
		'components/loader/component.loader' : {
			items : [ {
				alias : 'productLines',
				dependency : true,
			}, {
				alias : 'account',
				dependency : true
			}, {
				alias : 'employer',
				dependency : true
			}, {
				alias : 'accountHolder',
				dependency : true
			} ]
		},
		'common/application/proxy' : {
			queue : true,
			smartQueue : true,
			requests : {
				'component:get' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					type : 'get',
					filter : 'hub',
					params : [ 'transactionId', 'groupId' ]
				},
				'component:persist' : {
					location : 'handleRequests.action',
					namespace : 'shopping',
					type : 'post',
					filter : 'hub',
					params : [ 'transactionId', 'groupId' ]
				},
				'logout' : {
					type : 'redirect',
					location : 'Logout.action',
				},
				'login' : {
					type : 'redirect',
					location : 'LoginEdit.action'
				},
				'unauthorized' : {
					type : 'redirect',
					location : 'UnauthorizedHandlerAction.action'
				},
				'checkoutAnonymous' : {
					type : 'redirect',
					location : 'enrollAnonymous.action',
					namespace : 'shopping',
					params : [ 'transactionId', 'groupId' ]
				},
				'checkoutEmployee' : {
					type : 'redirect',
					location : 'enroll.action',
					namespace : 'shopping',
					params : [ 'transactionId', 'groupId', 'context' ]
				},
				'back' : {
					type : 'redirect',
					location : '',
					params : [ 'transactionId', 'groupId', 'context' ]
				},
				'email' : {
					location : 'emailPlan.action',
					namespace : 'shopping',
					type : 'post',
					params : [ 'transactionId', 'groupId', 'context' ]
				},
				'ffm' : {
					type : 'redirect',
					location : '',
					namespace : 'shopping',
					params : [ 'transactionId', 'context' ]
				}
			}
		},
		'common/application/application' : {

		},
		'log' : {
			enabled : false
		},
		'common/application/eventmanager' : {
			analyticsEnabled : true,
			mappings : {
				'view:showProductDetail' : 'viewDetail'
			}
		},
		'components/quote/component.quote' : {
			id:'FinalizeQuote',
			items : [ {
				alias : 'account',
				dependency : true,
			}, {
				alias : 'plans',
				dependency : true,
			}, {
				alias : 'productLines',
				dependency : true
			}, {
				alias : 'applicationConfig',
				dependency : true,
			}, {
				alias : 'widgets',
				dependency : true,
			} ]
		},
	}
});