/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/views/component.frame.dust' ], function(require, backbone, templateEngine, BaseView, template) {

	templateEngine.load(template, 'component.frame');

	ComponentView = BaseView.extend({
		tagName : "div",
		className : "_component",
		// Not sure we will want to define the location of the view here
		frame : "component.frame",
		// TODO: this really won't be here in the long wrong, gonna come from an outside source
		frame_localization : {
			title : ''
		},
		active : true,
		events : {
			'click .data-section .content-header' : 'toggleComponent'
		},

		initialize : function() {

		},
		render : function(options) {
			// TODO: This should be refactored to handle automatically rendering
			// of everything for 80% of the cases...mainly automatically rendering the template
			var self = this;
			options = _.extend({
				renderHeader : true,
				show : true
			}, options);

			if (this.frame) {
				// if frame data is set, render with frame
				$(self.el).append(this.renderTemplate({
					template : 'frame',
					data : _.extend(self.frame_localization, options)
				}));
				// set the attribute
				// Active determines if this component can be made visible
				// if it is visible then indicate
				$(self.el).attr('data-visible', self.active);

				$(self.el).attr('data-component-id', self.options.id);

				if (!self.active) {
					$('._componentContent', self.el).hide();
				}

			} else {
				// else render without frame
				$(self.el).append(this.renderTemplate({}));
			}

			if (this.frame_navigation && options.renderNavigation) {
				$('.content-header .content-navigation', self.el).append(this.renderTemplate({
					template : 'frame_navigation'
				}));
			}
		},
		/**
		 * This method will render a given template to the body of the component.
		 * 
		 * If the template is a string, it will use that as the template name.
		 * 
		 * If it is a instance of a view, it will render that to the body
		 * 
		 * Might want to think about adding an optional clean up for the body
		 * 
		 * @param template
		 */
		renderToBody : function(template) {
			$(this.el).find('.content-wrapper').append(this.renderTemplate({
				template : template
			}));
		},
		toggleComponent : function(event) {
			var self = this;
			// this is here because we want to toggle components and close others
			if (this.active) {
				// if it is currently visible, then hide...can't use toggle cause we want to do other things
				if ($(event.currentTarget).closest('._component').attr('data-visible') === 'true') {
					// Not sure if we need this behavior
					// $('._componentContent', this.el).hide();
					// $(this.el).attr('data-visible', false);
				} else {
					$('._componentContent', this.el).show();
					$(this.el).attr('data-visible', true);
					self.options.eventManager.trigger('component:view:activated', $(this.el).attr('data-component-id'));
				}
			}
		},
		renderNavigation : function() {

		}
	});

	return ComponentView;
});