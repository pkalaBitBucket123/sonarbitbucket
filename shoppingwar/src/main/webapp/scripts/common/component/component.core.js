/**
 * Copyright (C) 2012 Connecture
 * 
 * This defines the component structure off of the connecture namespace
 * 
 */
define([ 'require', 'connecture', 'common/views/base' ], function(require, connecture, BaseView) {

	connecture.component = {};
	connecture.component.core = {};
	connecture.view = {};
	connecture.view.Base = BaseView;

	return connecture;

});