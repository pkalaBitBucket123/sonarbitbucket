/**
 * Copyright (C) 2012 Connecture
 * 
 * Component Template
 * 
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component' ], function(require, Backbone, templateEngine, connecture) {

	var TemplateComponent = function(configuration) {
		connecture.component.core.Base.call(this, configuration);

		this.items = [];
		this.events = _.extend({}, this.events, {});
		this.triggers = {};
	};

	TemplateComponent.prototype = Object.create(connecture.component.core.Base.prototype);

	TemplateComponent.prototype.load = function(data) {

	};

	TemplateComponent.prototype.update = function(data) {

	};

	TemplateComponent.prototype.registerEvents = function() {
		this.eventManager.on("start", function() {

		}, this);
	};

	return TemplateComponent;
});