/**
 * Copyright (C) 2012 Connecture
 * 
 * Add extensions for: - making this persistable - how we handle returning data, JSON or parameters-strutsy
 * 
 * This needs to be updated so it gets based a component base context and then this expands upon it
 * 
 */
define([ 'require', 'log', 'underscore', 'backbone', 'common/application/session.model', 'common/application/eventmanager' ], function(require, log, _, Backbone, SessionModel, EventManager) {

	// TODO: the master is in application.js we need to update this so it uses
	// the same code
	var Callbacks = function() {
		this._deferred = $.Deferred();
		this._callbacks = [];
	};

	Callbacks.prototype = {
		add : function(callback, contextOverride) {
			this._callbacks.push({
				cb : callback,
				ctx : contextOverride
			});

			this._deferred.done(function(context, options) {
				if (contextOverride) {
					context = contextOverride;
				}
				callback.call(context, options);
			});
		},
		run : function(options, context) {
			this._deferred.resolve(context, options);
		},
		reset : function() {
			var that = this;
			this._deferred = $.Deferred();
			_.each(this._callbacks, function(cb) {
				that.add(cb.cb, cb.ctx);
			});
		}
	};

	/**
	 * Each component will contain an EventManager that is
	 * 
	 * @param configuration
	 * @returns {Component.Base}
	 */
	Component = function(configuration) {
		// TODO: The way we are handling configuration needs to be updated
		// so it is more concise
		// Component configuration, merge defaults
		// using $ deep extend here instead of
		// _ because we need nulls
		this.configuration = $.extend(true, {}, {
			layout : {
				render : {
					startup : 'show'
				},
				region : null,
			},
			id : null,
			engage : 'startup'
		}, configuration);

		// Currently a component only gets its own internal
		// event manager.

		// I do not want to mix internal component events with global
		// events. I am also not sure I want to expose the global
		// event system to a component because I do not want it to
		// be abused.

		// If you need to expose global events, you will use the event object
		// to do that, which must be defined in the constructor to work
		// This way we will know exactly which events this component exposes
		this.subscriptions = {
			'component:hide' : function(id) {
				// TODO move this to a method on the component
				// call minimize.
				// Add a show helper as wellWO
				if (this.id !== id) {
					this.hide();
					this.eventManager.trigger('hide');
				} else {
					this.show();
				}
			},
			// This is for external events telling a component to minimze
			'component:minimize' : function(id) {
				if (this.id !== id) {
					this.minimize();
				} else {
					this.show();
				}
			},
			'component:show' : function(id) {
				if (this.id !== id) {
					this.show();
				}
			}
		};

		// These are going to be the events that this component
		// exposes. The key to a trigger will be the internal event
		// that kicks off the external event. We need to set it up
		// this way because we might have some internal event to the component
		// kick off an external one.
		this.publish = {

		};

		this.models = {};
		this.collections = {};

		this.engaged = false;

		// Check to see if we have an id being passed in
		// This is supported for the cases where we need the
		// same id for a component each time, most likely
		// this comes from some configuration
		if (this.configuration.id === null) {
			this.id = getUUID();
		} else {
			this.id = this.configuration.id;
		}
		// This is a callback object that is used to
		// load initialize code for components.
		this.initialize = new Callbacks();
		// Component Specific event manager
		this.eventManager = new EventManager();
		// Holds all plugins for this component
		this.plugins = [];
		this.items = [];
		this.routes = [];
		// A session for each component, use the component id
		// as the session id
		this.session = new SessionModel({
			id : this.id
		});

		// fetch the data right away, we are doing this
		// here to keep the SessionModel pure, in case
		// someone else doesn't want to do it on init
		this.session.fetch();

		this.initialize.add(function() {
			var self = this;
			// Right now this is assuming that we won't need any data to create
			// this initial one...if we do, we might have to specify the configuration
			// and then load it after the load

			// If we pass in a Region object, we can take that Regions object to use as the EL
			if (self.configuration.layout) {
				var el = self.configuration.layout.el;
				this.regionManager = new connecture.component.layout.RegionManager({
					el : el,
					regions : self.configuration.layout.regions
				});

				// Need to check to see if any plugins are using regions
				// This is currently being used
				_.each(this.plugins, function(plugin) {
					if (plugin.hasOwnProperty('region')) {
						plugin.region = self.regionManager[plugin.region];
					}
				});
			}

		}, this);
	};

	Component.prototype = {
		/**
		 * This method is called after the component is instantiated and before anything else is called on the component. It is called automatically when you register a component with the page.
		 */
		load : function(data) {
			_.each(this.plugins, function(plugin) {
				plugin.load(data);
			});
		},
		registerEvents : function() {

		},
		uses : function(Plugin, options) {
			var plugin = new Plugin();
			// should always be this type but this method should
			// be going away
			if (plugin.type === 'component') {
				options = options || {};
				options = _.extend(options, {
					plugged : this,
					eventManager : this.eventManager
				});

				plugin.plugify(options);
			}
			this.plugins.push(plugin);
		},

		/**
		 * Maybe we make this a private method that we don't necessarily override
		 * 
		 * This handles updating the component itself, which would use the proxy.
		 * 
		 * Once that is complete it would call some event called onUpdate or something that the component then would bind to to update itself
		 * 
		 * We could even configure update points for the component so that we could have separate update events depending on what we are doing
		 * 
		 * We should handle taking models and updating them and also taking
		 * 
		 * 
		 * Notes from 11/16/12 - Support a couple different ways of updating - If the event reaction is 'update:xxx', we will do some more automated simple updating of the component - We can specify a
		 * standard callback, in which case we will manually trigger an update to the component. This would be for the cases that we need to gather some sort of information to do the update. We would
		 * just need to trigger the event to update.
		 * 
		 * How do we handle cases where we need to do something, once the update is successful from a event? I guess internally, that update would trigger an event when it was completed.
		 * 
		 * Example: Answer Question - triggers update:account Update:Account - does a proxy update, calls component update method Proxy Complete - triggers update:account:done, Update:Account:Done -
		 * triggers global
		 */
		update : function(options) {
			// default do nothing I think
			// if there is a route
			if (options && options.route) {
				// if there is a route and this component
				// is the route, then call update
				// We have cases where we might need to call update
				// regardless. If there is no route for that component
				// however, it needs to handle that itself
				if (options.route.id === this.id) {
					this.eventManager.trigger("update", options);
				} else {
					// Update it but do not pass in the route options
					this.eventManager.trigger("update", {
						route : false
					});
				}
			} else {
				this.eventManager.trigger("start", options);
			}

		},
		persist : function() {

		},
		/**
		 * This is the main method that will start the component. This is called external to a component. Override at your own risk
		 * 
		 */
		engage : function(options) {
			// on startup we might now have any options
			var routeOptions = options ? options : {
				route : false
			};

			this.registerEvents();
			// Before we initialize this component
			this.eventManager.trigger("initialize:before");
			// Initialize the component
			this.initialize.run({}, this);
			// After we initialize the component
			this.eventManager.trigger("initialize:after");

			// Now startup up our main component
			// While engaging, if there is a route specified for
			// this component, pass it through
			if (routeOptions.route && routeOptions.route.id === this.id) {
				this.eventManager.trigger("start", routeOptions);

			} else {
				this.eventManager.trigger("start", {
					route : false
				});
			}

			this.engaged = true;
		},
		/**
		 * Internal method use to put the guts of the component start logic
		 * 
		 * 4/10/13 - Updated to make this method more simplistic. It does not need to handle the old logic it did (prior to enforcing everything to be models or collections). Just store a reference to
		 * the model or collection at the given key. Do not do any extra logic, that should not be done here. Components reference data but don't really own it anymore.
		 */
		addModel : function(model, key) {
			// if we have a key, us that
			if (key) {
				this.models[key] = model;
			}

			return model;
		},
		clear : function() {
			this.models = {};
			this.collections = {};
		},
		hide : function() {
			if (this.regionManager) {
				this.regionManager.hide();
			}
		},
		show : function() {
			if (this.regionManager) {
				this.regionManager.show();
			}
		},
		minimize : function() {
			if (this.regionManager) {
				this.regionManager.minimize();
			}
		},
		/**
		 * Remove the component from view
		 */
		remove : function() {
			if (this.regionManager) {
				this.regionManager.close();
			}
		},
		/**
		 * Going to clean for now
		 */
		destroy : function() {
			this.session.clear();
			this.remove();
		},
		getLayoutView : function() {
			return new this.configuration.layout.View({
				eventManager : this.eventManager,
				id : this.id
			});
		}
	};

	return Component;
});
