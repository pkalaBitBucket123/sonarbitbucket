/**
 * Copyright (C) 2012 Connecture
 * 
 * Add extensions for: - making this persistable - how we handle returning data, JSON or parameters-strutsy
 * 
 * This needs to be updated so it gets based a component base context and then this expands upon it
 * 
 * 
 * 
 * 
 */
define([ 'require', 'underscore', 'common/views/base', 'common/component/component.core', 'common/component/component.base', 'common/component/component.view', 'common/application/application.region',
		'common/application/application.manager.region' ], function(require, _, BaseView, connecture, Base, View, Region, RegionManager) {
	// This might be over kill but I want to use this to build a namespace
	// of helper objects from a core component framework to use in the rest of the pages
	connecture.component.core.View = View;
	connecture.component.core.Base = Base;
	connecture.component.layout = {};
	connecture.component.layout.Region = Region;
	connecture.component.layout.RegionManager = RegionManager;

	return connecture;

});