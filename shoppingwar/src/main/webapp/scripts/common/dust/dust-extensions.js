/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to be automatically added to the dust helpers conext, I am not make this it's own define because of this. This will have to be shimmed to be added to your project
 */
(function() {
	// If using require, pull in that one,
	// otherwise the global will be referenced
	if (typeof exports !== "undefined") {
		dust = require("dust");
	}

	if (typeof dust.helpers === 'undefined') {
		dust.helpers = {};
	}

	// Add your helpers here
	// Example of how to add helpers

	// dust.helpers['test'] = function(chunk, context, bodies, params) {
	//	
	// }

	dust.helpers['iter'] = function(chk, ctx, bodies) {
		var obj = ctx.current();

		var entityCount = 1;

		for ( var k in obj) {
			if (obj.hasOwnProperty(k)) {
				chk = chk.render(bodies.block, ctx.push({
					count : entityCount,
					key : k,
					value : obj[k]
				}));
				entityCount++;
			}

		}
		return chk;
	};

	dust.helpers['contains'] = function(chk, ctx, bodies, params) {
		if (typeof params['setKey'] === "string" && (typeof params['value'] === "string" || typeof params['value'] === "function")) {
			var key = params['setKey'];

			var value = params['value'];
			if (typeof params['value'] === "function") {
				value = dust.helpers.tap(params['value'], chk, ctx);
			}

			var obj = ctx.get(key);

			var contains = false;

			for ( var k in obj) {
				if (obj[k] == value) {
					contains = true;
					break;
				}
			}

			if (contains) {
				chk = chk.render(bodies.block, ctx);
			} else {
				chk = chk.render(bodies['else'], ctx);
			}
		}

		return chk;
	};

	if (typeof exports !== "undefined") {
		module.exports = dust;
	}

})();