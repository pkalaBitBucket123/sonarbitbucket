/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to be automatically added to the dust helpers conext, I am not make this it's own define because of this. This will have to be shimmed to be added to your project
 */
define([ 'require', 'backbone.validation', 'date' ], function(require, backboneValidation) {
	
	backboneValidation.configure({
		labelFormatter : 'label'
	});
	
	_.extend(backboneValidation.validators, {
		atLeastOne : function(value, attr, customValue, model) {			
			// check for existing but empty attribute
			if($('#childOnlyField').is(':checked')){
				if ((value > 1)){
					return this.format(Backbone.Validation.messages.onlyOne, this.formatLabel(attr, model));
				}
			}else{
			if (!value || !(value > 0)){
				return this.format(Backbone.Validation.messages.atLeastOne, this.formatLabel(attr, model));
			}
			}
		},
		datePattern : function(value, attr, customValue, model) {

			// handles date formatting

			if (value && value != '') {

				// check that the name matches a special value
				var regExp = new RegExp("^(0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[01])/(18|19|20)\\d\\d$");

				// if (!regExp.test(dateValue)) {
				if (!regExp.test(value)) {
					return this.format(Backbone.Validation.messages.datePattern, this.formatLabel(attr, model));
				} 
			}
		},
		dateBefore : function(value, attr, customValue, model) {

			// handles both date formatting and days in past date check

			if (value && value != '') {
				if(!this.validDate(value)){
					return this.format(Backbone.Validation.messages.datePattern, this.formatLabel(attr, model));
				} else {
					var selectedDate = new Date(value);
					
					var beforeDateInUTCTime = model.get("beforeDateInUTCTime");
					
					// check if the date is after or on the beforeDateInUTCTime
					if (selectedDate.getTime() >= beforeDateInUTCTime ) {
						return this.format(Backbone.Validation.messages.dateBeforeDate, this.formatLabel(attr, model), model.get("beforeDate"));
					}
				}
			}
		},
		dateAfter : function(value, attr, customValue, model) {

			// handles both date formatting and days in past date check

			if (value && value != '') {
				if(!this.validDate(value)){
					return this.format(Backbone.Validation.messages.datePattern, this.formatLabel(attr, model));
				} else {
					var selectedDate = new Date(value);
					
					var afterDateInUTCTime = model.get("afterDateInUTCTime");
					
					// check if the date is before the afterDateInUTCTime
					if (selectedDate.getTime() <= afterDateInUTCTime ) {
						return this.format(Backbone.Validation.messages.dateAfterDate, this.formatLabel(attr, model), model.get("afterDate"));
					}
				}
			}
		},
		// for SHSBCBSNE-248
		//commenting for SHSBCBSNE-667
		/*sameGenderAsPrimary : function(value, attr, customValue, model) {
			if(!customValue){return;}
				if(model.get('memberRelationship') == 'SPOUSE')
				{	
					var primaryGender = $('select[data-id="0"][name="gender"]').val();
					if(value == primaryGender)
					{
						return this.format(Backbone.Validation.messages.sameGenderAsPrimary, this.formatLabel(attr, model));	
					}	
				}
		},*/
		dateNotMoreThanDaysInPast : function(value, attr, customValue, model) {

			// handles both date formatting and days in past date check

			if (value && value != '') {
				if(!this.validDate(value)){
					return this.format(Backbone.Validation.messages.datePattern, this.formatLabel(attr, model));
				} else {
					var selectedDate = new Date(value);
					
					var ONE_DAY = 1000 * 60 * 60 * 24;
					
					var moreThanDays = model.get("moreThanDays");
					var maxDaysTime = moreThanDays * ONE_DAY;

					var now = new Date();
					if (selectedDate.getTime() <= now.getTime() - maxDaysTime ) {
						// check if the date is in the future
						return this.format(Backbone.Validation.messages.dateNotMoreThanDaysInPast, this.formatLabel(attr, model), moreThanDays);
					}
				}
			}
		},
		dateNotInFuture : function(value, attr, customValue, model) {

			// handles both date formatting and future date check

			if (value && value != '') {
				if(!this.validDate(value)){
					return this.format(Backbone.Validation.messages.datePattern, this.formatLabel(attr, model));
				}
				
				var now = new Date();
				var selectedDate = new Date(value);
				
				if (selectedDate >= now) {

					// check if the date is in the future
					return this.format(Backbone.Validation.messages.dateNotInFuture, this.formatLabel(attr, model));

				}
			}
		},
		validDate : function(value) {
			// true enforces format
			return Date.parse(value) !== null;
		},
		integer : function(value, attr, customValue, model) {
			if (value && value != '') {

				// check that the name matches a special value
				var regExp = new RegExp("^[\\d]+$");
				
				if (!regExp.test(value)) {
					return this.format(Backbone.Validation.messages.integer, this.formatLabel(attr, model));
				}			
			}
		},
		namePattern : function(value, attr, customValue, model) {

			if (value && value != '') {
				// check that the name matches a special value
				// allow alpha, hyphen, apostrophe and space
				var regExp = new RegExp("^[a-zA-Z-' ][a-zA-Z-' ]*$");

				if (!regExp.test(value)) {

					return this.format(backboneValidation.messages.namePattern, this.formatLabel(attr, model));
				}
			}
		}
	});
	
	_.extend(backboneValidation.messages, {
		required : '{0} is a required field.',
		// countyRequired added by Priyanka M for SHSBCBSNE-7/101
		countyRequired: 'The ZIP Code crosses multiple counties. Please select a County.',
		startDateReqd : 'Insurance Start Date is a required field.',
		dateNotMore90Or60Days : 'Insurance Start Date must be between today and {1} days in the future.',
		atLeastOne : 'At least one {0} is required.',
		datePattern : '{0} must be a valid date in the format MM/DD/YYYY.',
		dateNotInFuture : '{0} must not be in the future.',
		dateNotMoreThanDaysInPast : '{0} must not be more than {1} days in the past.',
		dateBeforeDate : '{0} must be before {1}.',
		dateAfterDate : '{0} must be after {1}.',
		maxLength : '{0} must be no longer than {1} characters.',
		minLength : '{0} must contain at least {1} characters.',
		//integer : '{0} must be numeric test 1234',
		integer : ' Please enter only digits.',
        namePattern : '{0} may only contain alpha characters (A-Z,-,\' or spaces).',
		// for SHSBCBSNE-248
		//changes for SHSBCBSNE-666,667
		//sameGenderAsPrimary : 'Spouse cannot be the same gender as the primary applicant.',
		onlyOne : 'Only one child can be added to a child only application.',
	});
});