/**
 * Copyright (C) 2012 Connecture
 * 
 * How to handle roles and anon. We will need a global spot where the role of the logged in user is accessible. Then based on role, here we will check to see if there is another template for that
 * role...if there is load, that one instead of the default. We might have to have some code special in here, during the build, to load all possible templates given roles so that they are in the file
 * 
 */
define([ 'dust', 'log' ], function(dust, log) {

	return {
		/**
		 * Load a template into dust. Template can either be precompiled or a raw dust string template.
		 * 
		 * This method can also be called to render inline dust from view files.
		 * @param template
		 * @param name
		 * @returns
		 */
		load : function(template, name) {
			// There is a problem using dust.js and AMD with plugins. There is a ticket out there
			// to get dust upgraded to be more AMD compatible (it seems to be a problem when optimizing).
			// Until then we are using this object to handle passing in a template and loading it into
			// dust.

			// We need to check here to see if what we are getting has been compiled or not. Because we
			// are using the text plugin, it is always going to return that data as a string. We need to
			// parse the string to see what it is to determine what we can do with it.

			if (typeof template === 'string') {
				// This should be unique enough that it won't be in standard template that
				// we can check against it
				if (template.indexOf('(function(){dust.register') === 0) {
					dust.loadSource(template);
				} else {
					dust.loadSource(dust.compile(template, name));
				}
			} else {

			}
		},
		render : function(name, context) {
			var retVal = '';
			if (typeof name !== 'undefined' && name !== null && name !== '') {
				// See notes above on why this is being done
				dust.render(name, context, function(err, out) {
					retVal = out;
					if (err) {
						log.error(err);
					}
				});
			}
			return retVal;
		}
	};
});