/**
 * Copyright (C) 2012 Connecture
 * 
 * This is being used for both component and application plugins, should I define separate objects for them?
 */
define(function(require, exports, module) {

	var Plugin = function(options) {
		// this needs to be set by the implementor
		this.type = null;
	};

	Plugin.prototype = {
		/**
		 * This method is used to put any initialization code that the plugin needs to attach to the object is plugging into. This means it cannot go into the constructor
		 */
		initialize : function(options) {
			if (!options.plugged) {
				var err = new Error("The plugged object that we are plugging into must be specificed.");
				err.name = "NoPluggedError";
				throw err;
			}

			if (!options.eventManager) {
				var err = new Error("An event manager must be specified");
				err.name = "NoEventManagerError";
				throw err;
			}

			_.extend(this, options);
		},
		addEvents : function() {

		},
		addMethods : function() {

		},
		plugify : function(options) {
			this.initialize(options);
			this.load();
			this.addEvents();
			this.addMethods();
		},
		load : function(data) {

		}
	};

	return Plugin;

});