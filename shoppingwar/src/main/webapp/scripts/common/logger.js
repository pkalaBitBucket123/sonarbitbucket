/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'module', 'log4javascript' ], function(module, log4javascript) {

	var enabled = module.config().enabled;

	enabled = enabled || false;

	log4javascript.setEnabled(enabled);

	var log = log4javascript.getLogger();
	var popUpAppender = new log4javascript.PopUpAppender();
	var popUpLayout = new log4javascript.PatternLayout("%d{HH:mm:ss} %-5p - %m%n");
	// var popUpLayout = new log4javascript.JsonLayout(true, false);

	popUpAppender.setLayout(popUpLayout);
	log.addAppender(popUpAppender);

	return log;
});