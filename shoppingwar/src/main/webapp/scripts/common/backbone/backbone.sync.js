/**
 * Copyright (C) 2012 Connecture
 * 
 * This file allows us to configure and override Backbone.sync for different purposes.
 * 
 * Each model or collection that needs to use a specific type, should specify with a given string.
 * 
 * This file will then include all possible sync overrides and determine which one to use based on how the model or collection is configured.
 * 
 * Originally we had the model or collection directly referencing the override files but that was causing circular references because one includes Proxy, which is used elsewhere.
 * 
 * This should be more robust and not cause circular dependencies
 * 
 */
define([ 'require', 'backbone', 'common/backbone/backbone.proxy', 'common/backbone/backbone.amplify' ], function(require, Backbone, proxyStorage, sessionStorage) {

	// Easy add here
	var storageEngines = [ proxyStorage, sessionStorage ];

	// This idea might still work, I would reference a key for what type of storage it is, rather
	// then creating an object, since in most cases we don't need that object to really hold
	// anything
	Backbone.ajaxSync = Backbone.sync;

	Backbone.getSyncMethod = function(model) {
		// by default use Backbone sync
		var sync = Backbone.ajaxSync;
	
		_.each(storageEngines, function(engine) {
			if (engine.type === model.storageEngine) {
				sync = engine.sync;
			}
		});

		return sync;
	};

	Backbone.sync = function(method, model, options) {
		return Backbone.getSyncMethod(model).apply(this, [ method, model, options ]);
	};

	// Not sure this really needs to return anything special so
	// just return Backbone
	return Backbone;
});