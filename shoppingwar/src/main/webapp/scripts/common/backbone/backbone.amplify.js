/**
 * Copyright (C) 2012 Connecture
 * 
 * Backbone sync override for working with the Amplify.
 * 
 */
define([ 'require', 'backbone', 'amplify' ], function(require, Backbone, amplify) {

	var sessionStorage = {
		create : function(model, options) {
			amplify.store(model.id, JSON.stringify(model.toJSON()));
			return model.toJSON();
		},
		read : function(model, options) {
			var data = amplify.store.sessionStorage(model.id);
			if (data) {
				return JSON.parse(data);
			} else {
				return null;
			}

		},
		update : function(model) {
			amplify.store.sessionStorage(model.id, null);
			amplify.store.sessionStorage(model.id, JSON.stringify(model.toJSON()));
			return model.toJSON();
		}
	};

	// Standard interface we want to override or add an additional sync
	return {
		type : 'Session',
		sync : function(method, model, options, error) {
			if (typeof options == 'function') {
				options = {
					success : options,
					error : error
				};
			}

			var resp = {};

			switch (method) {
			case "read":
				resp = model.id != undefined ? sessionStorage.read(model) : findAll();
				break;
			case "create":
				resp = sessionStorage.create(model);
				break;
			case "update":
				resp = sessionStorage.update(model);
				break;
			case "delete":
				resp = sessionStorage.destroy(model);
				break;
			}

			if (resp) {
				if (options && options.success) {
					options.success(resp);
				}
			} else {
				if (options && options.error) {
					options.error("Record not found");
				}
			}

		}

	};
});