/**
 * Copyright (C) 2012 Connecture
 * 
 * Backbone sync override for working with the Proxy API.
 * 
 */
define([ 'require', 'backbone', 'common/application/proxy' ], function(require, Backbone, Proxy) {

	// Not sure I want to give this directly to the collection
	var proxy = new Proxy();

	var proxyStorage = {
		read : function(model, options) {
			// If an override requestKey is set,
			// use that one

			// TODO We should pass this through proxy.filter
			// first to determine how to build this data
			var requestKey = 'component:get';
			var proxyOptions = {
				defer : true,
				raw : true
			};

			if (model.requestKey) {
				requestKey = model.requestKey;
			} else { // assuming store for now
				proxyOptions['requests'] = [ model.storeItem ];
			}

			// since we are overriding sync, we need to account for
			// some of their behavior.  Please note, right now this is
			// being triggered right away, even though the request might get
			// queued
			model.trigger('request');

			// we have to pass the callback here in as a function parameter
			// because queue is not returning a deferred
			proxy.queue(proxy.process(requestKey, proxyOptions, function(requestResponses) {
				// We need to parse out this data
				options.success(requestResponses[model.item]);
			}), 'get');
		},
		update : function(model, options) {
			// If an override requestKey is set,
			// use that one

			// TODO We should pass this through proxy.filter
			// first to determine how to build this data
			var requestKey = 'component:persist';
			var proxyOptions = {
				raw : true,
				defer : true
			};

			if (model.requestKey) {
				requestKey = model.requestKey;
				proxyOptions['data'] = model.toJSON();
			} else {// assuming store for now
				var data = {};
				data[model.item] = model.toJSON();
				proxyOptions['requests'] = [ {
					store : model.storeItem,
					data : data
				} ];
			}

			// since we are overriding sync, we need to account for
			// some of their behavior.  Please note, right now this is
			// being triggered right away, even though the request might get
			// queued
			model.trigger('request');

			// we have to pass the callback here in as a function parameter
			// because queue is not returning a deferred
			proxy.queue(proxy.process(requestKey, proxyOptions, function(requestResponses) {
				// We need to parse out this data
				// TODO: figure out a better way to handle this
				// basically I think the proxy should figure it out
				// and we just pass through here
				if (model.item) {
					options.success(requestResponses[model.item]);
				} else {
					options.success(requestResponses);
				}

			}), 'post');
		}
	};

	// Standard interface we want to override or add an additional sync
	return {
		type :  'Proxy',
		sync : function(method, model, options, error) {
			if (typeof options == 'function') {
				options = {
					success : options,
					error : error
				};
			}

			switch (method) {
			case "read":
				proxyStorage.read(model, options);
				break;
			case "create":
				proxyStorage.update(model, options);
				break;
			case "update":
				proxyStorage.update(model, options);
				break;
			case "delete":
				// Add when necessary
				break;
			}

		}
	};
});