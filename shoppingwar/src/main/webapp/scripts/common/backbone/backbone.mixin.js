/**
 * Copyright (C) 2012 Connecture
 * 
 * This is based off the following gist
 * 
 * https://gist.github.com/hxgdzyuyi/3652964
 * 
 * Handles model/collection/view mixins.  This should be expanded
 * though to handle any number of methods, not just inititalize. 
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {

	Backbone.mixin = function(view, mixin, custom) {
		if (custom) {
			if (custom.events && mixin.events) {
				mixin = _.clone(mixin);
				_.defaults(custom.events, mixin.events);
			}
			_.extend(mixin, custom);
		}
		var source = view.prototype || view;
		_.defaults(source, mixin);
		if (mixin.events) {
			if (source.events) {
				_.defaults(source.events, mixin.events);
			} else {
				source.events = mixin.events;
			}
		}
		//TODO: Update to make this automatic
		if (mixin.initialize !== undefined) {
			var oldInitialize = source.initialize;
			source.initialize = function() {
				mixin.initialize.apply(this, arguments);
				oldInitialize.apply(this, arguments);
			};
		}
	};

	return Backbone;
});