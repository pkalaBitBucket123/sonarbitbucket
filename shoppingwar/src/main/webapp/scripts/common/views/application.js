/**
 * Copyright (C) 2012 Connecture
 * 
 * This will be the main application view. It will define the template and high level stuff for the application as a whole.
 * 
 * TODO: This might be better labeled as a page instead of app. Unless we use this for bootstrapping stuff ,
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/views/application.dust' ], function(require, Backbone, templateEngine, BaseView, template) {

	templateEngine.load(template, 'application');

	var AppView = BaseView.extend({
		el : $("body"),
		appTemplate : "application",
		events : {

		},
		initialize : function(options) {
			if (options.eventManager) {
				this.eventManager = options.eventManager;
			}
		},
		render : function() {
			var self = this;

			$(self.el).html(templateEngine.render(this.appTemplate, {}));

			$('a[title]', self.el).tooltip({
				style : {
					classes : 'ui-tooltip-standard'
				},
				position : {
					my : "bottom center", // Use the corner...
					at : "top center" // ...and opposite corner
				}
			});

			return this;
		}
	});

	return AppView;

});