/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'jquery.equalHeight' ], function(require, Backbone, templateEngine) {
	var BaseView = function(options) {
		// Used to hold any child views for disposing of later
		this.views = [];
		Backbone.View.apply(this, [ options ]);
	};

	_.extend(BaseView.prototype, Backbone.View.prototype, {

		dispose : function(options) {
			options = _.extend({
				empty : false
			}, options);

			// Check to see if this view has any child views
			// registered. If so, loop through each one and dispose
			// them as well
			this.disposeSubViews();
			// I feel like this is overkill right now, all of this should be handled by backbone but do this for now
			this.unbind();
			this.stopListening();
			if (!options.empty) {
				this.remove(); // uses the default Backbone.View.remove() method which removes this.el from the DOM and removes DOM events.
			} else {
				$(this.el).empty();
			}
		},
		/**
		 * Use this method to dispose of just sub views. This might be helpful in cases where your view is calling render on itself with subviews and we want to clean up any subviews to rerender them.
		 */
		disposeSubViews : function() {
			var self = this;
			if (this.views) {
				_.each(this.views, function(view) {
					// If this view was listening to any sub views, stop it
					self.stopListening(view);
					view.dispose();
				});
			}
		},
		// Should update this to see if there is a property for template
		// data to automatically append
		renderTemplate : function(options) {
			var retVal = '';
			// You can either pass in a template name, or we can
			// pull from this.template
			// TODO: we should really force it to pull from a template name that is
			// attached to this object, keep a standard

			var template;
			if (options && options.template) {
				// enforcing the template to be on the object, shouldn't be rendering stuff outside
				// your view...TODO: I am not sure I like this restriction anymore
				template = this[options.template];
			} else {
				// TODO: check this one
				template = this.template;
			}

			var data;
			if (options && options.data) {
				data = options.data;
			} else {
				// TODO: check this one
				data = this.data;
			}

			var context = data;
			if (options && options.functions) {
				context = dust.makeBase(options.functions);
				context = context.push(data);
			}

			retVal = templateEngine.render(template, context);

			return retVal;
		},
		/**
		 * This is used for dynamically adding events to views
		 * 
		 * @param events
		 */
		addEvents : function(events) {
			this.events = _.extend(_.clone(this.events), events);
			this.delegateEvents();
		}

	});

	BaseView.extend = Backbone.View.extend;

	return BaseView;
});
