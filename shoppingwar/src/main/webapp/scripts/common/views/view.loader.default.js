/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/views/base' ], function(require, backbone, BaseView) {
	var DefaultLoader = BaseView.extend({
		initialize : function(options) {

		},
		render : function() {
			var self = this;
			$(self.el).html('<div class="action-indicator"><div class="indicator"></div><div class="message">Loading</div></div>');
			return this;
		}
	});

	return DefaultLoader;
});
