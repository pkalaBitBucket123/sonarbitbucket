/**
 * Copyright (C) 2012 Connecture
 * 
 * AMD Backbone View Template to be used within Shopping.
 */
define([ 'require', 'backbone', 'templateEngine', 'common/component/component', 'REPLACE/WITH/TEMPLATE/LOCATION' ], function(require, Backbone, templateEngine, connecture, template) {

	templateEngine.load(template, 'replace.with.template.name');

	var TemplateView = connecture.view.Base.extend({
		template : 'replace.with.template.name',
		initialize : function(options) {
			this.eventManager = options.eventManager;
		},
		events : {

		},
		render : function() {
			var data = {};
			
			$(this.el).html(this.renderTemplate({
				data : data,
			}));
			
			return this;
		},
		close : function() {

		}
	});

	return TemplateView;
});
