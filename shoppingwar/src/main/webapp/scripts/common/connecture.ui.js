/*
 * Copyright (C) 2012 Connecture
 * Version: 0.1
 */

connecture.ui = (function() {

	var ui = {};

	ui.dialogalert = function(content) {
		var alertDialog = '<div id="alertDialog"></div>';
		$("body").append(alertDialog);

    	$("#alertDialog").dialog({
			autoOpen: false,
			draggable: true,
			title: "Alert!",
			width: 350,
			height: 150,
			modal: true});
    	var modalText = '<h3>' + content + '</h3>';
		modalText += '<div class="buttonGroup"><input type="button" id="" class="buttonPrimary" value="Ok" onclick="$(\"#alertDialog\").dialog(\"open\")"/></div>';
	    $("#alertDialog").append(modalText);
	    $('#alertDialog').dialog('open');
	};

	return ui;
	
}) ();