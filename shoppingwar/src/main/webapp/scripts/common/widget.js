/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to become the base Widget file. We will slowly move stuff into here from the existinw widget.question.js
 * 
 */
define([ 'require', 'backbone', ], function(require, Backbone) {

	var Widget = function(options) {
		this.model = options.widgetModel;
		// TODO: I would like this to go away
		this.attributes = options.attributes;
		this.eventManager = options.eventManager;

		this.isPrimary = false;

		this.initialize();
	};

	Widget.prototype = {
		initialize : function() {

		},
		start : function() {

		},
		render : function() {

		},
		getSummaryView : function() {
			return null;
		},
		canMovePrev : function() {
			return true;
		},
		canMoveNext : function() {
			return true;
		},
		canSkip : function() {
			return true;
		},
		destroy : function() {

		},
		canRender : function() {
			return true;
		},
		configModel : function() {
			return this.model;
		},
		setPrimary : function(isPrimary) {
			this.isPrimary = isPrimary;
		},
		historyKey : function() {
			return this.model.get('widgetId');
		},
		isComplete : function() {
			return true;
		},
		/**
		 * Ask/set the of the widget.  Right now
		 * only setup to ask if the widget is in a specific
		 * state.  By default always return true for now
		 * @param state
		 * @returns {Boolean}
		 */
		state : function(state) {
			return true;
		}
	};

	return Widget;
});
