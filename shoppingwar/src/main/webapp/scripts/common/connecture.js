/**
 * Copyright (C) 2012 Connecture
 */
(function(window) {

	var connecture = {};

	connecture.core = {};

	connecture.ui = {};

	connecture.getinternetexplorerversion = function() {
		var rv = -1;
		if (navigator.appName == 'Microsoft Internet Explorer') {
			var ua = navigator.userAgent;
			var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			if (re.exec(ua) != null)
				rv = parseFloat(RegExp.$1);
		}
		return rv;
	}

	if (typeof define === "function" && define.amd) {
		define("connecture", [], function() {
			return connecture;
		});
	}

	window.connecture = connecture;

})(window);