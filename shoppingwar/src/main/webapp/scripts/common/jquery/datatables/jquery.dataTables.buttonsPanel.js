/**  Copyright (C) 2012 Connecture */
(function($) {
	/*
	 * Register a new feature with DataTables
	 */
	if (typeof $.fn.dataTable === 'function') {
		$.fn.dataTableExt.aoFeatures.push({
			'fnInit' : function(oDTSettings) {
				var $dataTablesButtonsNode = $('<div></div>').addClass('dataTables_buttons');
				if (oDTSettings.oInit.oButtonsPanel.hasOwnProperty('aButtons') && $.isArray(oDTSettings.oInit.oButtonsPanel.aButtons)) {
					var buttons = oDTSettings.oInit.oButtonsPanel.aButtons;
					$.each(buttons, function(index, button) {
						// <div class="dataTables_buttons">
						var $buttonWrapper = $('<div></div>').addClass('dataTables_button');
						$buttonWrapper.append(button.sButton);
						$dataTablesButtonsNode.append($buttonWrapper);
					});
				}

				return $dataTablesButtonsNode[0];
			},
			'cFeature' : 'B',
			'sFeature' : 'oButtonsPanel'
		});
	}

}(jQuery));