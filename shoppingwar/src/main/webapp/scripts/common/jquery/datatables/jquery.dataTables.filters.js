/** Copyright (C) 2012 Connecture
 *
 * v0.2
 * 
 * This file contains a set of plugins for the DataTables jQuery plugin. Instead of being a direct plugin to jQuery these are feature plugins to DataTables directly. This file contains to plugins
 * because they are losely related to each other.
 * 
 * 
 * oCustomFilters is a feature for DataTables that allows more robust filtering. This borrows on the idea of some existing filter plugins but adds some new features and fixes some bugs. I didn't think
 * it made sense to modify an existing plugin because of all the custom code and issues I had to fix. Might as well build and own our own filtering plugin.
 * 
 * To use this feature, you will need to put the "F" character in the "sDom" property of your DataTable setup. You can then configure this feature with the following property example:
 * 
 * <pre>
 * 'oCustomFilters' : { 
 * 		bClearLink : true, //add a clear filter link or not 
 * 		sLabel : 'Filters', //add a label to all filters
 * 		afilters : [ { //This is used for an array of filter objects, you will need one for each filter you want 
 * 			sLabel : 'Status', 
 * 			sType : 'multi-select', //this is the type of filter it is, we only support multi-select for now 
 * 			aValues : [], //What are the values we are filtering, if null or left off, it will auto calculate the values for you
 * 			iTarget : 2 //what column are we targeting for the filter 
 * 		}] 
 * }
 * </pre>
 * 
 * Notes: - Right now we have some of the "connecture" standards in this plugin (mainly around multi-select), does it make more sense to pull those out of here and keep this plugin to core
 * functionality and then pass in some of those standards in as properties?
 * 
 * PS: I really dislike Hungarian notation but I am using it here to follow the standards created with DataTables.
 * 
 */
(function($) {

	// We are overriding the multiselect options globally
	// because we do not want it to set any minimum width,
	// we are going to be setting the width ourself for maximum flexibility
	$.ech.multiselect.prototype.options.minWidth = "auto";

	/**
	 * Add backslashes to regular expression symbols in a string.
	 * 
	 * Allows a regular expression to be constructed to search for variable text.
	 * 
	 * @param string
	 *            sText The text to escape.
	 * @return string The escaped string.
	 */
	var fnRegExpEscape = function(sText) {
		return sText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
	};

	var Filters = function(oDataTableSettings) {
		var self = this;
		// This is the container that will contain all of the controls
		self.$container = $('<div></div>').addClass('filters');
		// Just in case we need direct access to the data table
		self.dataTable = oDataTableSettings.oInstance;
		// This will hold all of the filters for this datatable
		self.filters = [];
		// Not saving them because I am not sure I need to reference them again
		var filterSettings = oDataTableSettings.oInit.oCustomFilters;

		if (filterSettings.hasOwnProperty('sLabel') && typeof filterSettings.sLabel === 'string') {
			var $spanLabel = $('<span class="filter-label"></span>').text(oDataTableSettings.oInit.oCustomFilters.sLabel);
			self.$container.append($spanLabel);
		}

		// Let's loop for every filter we are setting up
		$.each(filterSettings.afilters, function(index, oFilter) {
			// Make sure that column exists before we try adding a filter to it
			if (oDataTableSettings.aoColumns.length < oFilter.iTarget || oDataTableSettings.aoColumns[oFilter.iTarget] == null) {
				return;
			}
			// not needed but a snippet of how you get the column
			// var $columnTh =
			// $(dataTableSettings.aoColumns[oFilter.target].nTh);
			var filter = new Filter(self.$container, oDataTableSettings, oFilter);
			self.filters.push(filter);
		});

		if (filterSettings.hasOwnProperty('bClearLink') && filterSettings.bClearLink) {
			var $divClearWrapper = $('<div></div>').addClass('reset-filters');
			var $clearLink = $('<a href="#" class="btn-utility"></a>').text('Reset Filters');
			$divClearWrapper.append($clearLink);
			self.$container.append($divClearWrapper);
			// Loop through each of the filters and then call a reset method on
			// it, BAM
			// TODO: This should be a callback so that it is configurable,
			// the default can be this but there should be some sort of override
			// option
			$clearLink.click(function() {
				self.dataTable.fnResetAllFilters();
				$.each(self.filters, function(index, filter) {
					filter.reset();
				});
			});
		}

		return self.$container[0];
	};

	/*
	 * I think I like this concept better. This way I can create a basis for all filters in a object literal. It won't hold state but it will act on a filter object, which will be passed in as the
	 * "this" object that will be applied to each method as it is called.
	 * 
	 * This way I can have a bunch of predefined filters OR a calling DataTable can create their own filter on the fly and then they can also override pre-existing filters if they need too.
	 * 
	 * And then I don't have to create individual objects for each filter type or have one giant type that will handle all
	 * 
	 * When you are accessing methods in here, you need to call the method on this.filter.<method> This is because the predefined filters will not know if they have been overridden or not at runtime.
	 */
	var predefinedFilters = {
		multiSelect : {
			// This is a required function
			init : function() {
				var self = this;

				// These are settings that are specific to this filter type
				// Make sure we have some.
				if (self.filter.defaults) {
					// pass in an empty target to preserve the default
					self.filterSettings.oTypeSettings = $.extend({}, self.filter.defaults, self.filterSettings.oTypeSettings);
				}

				// This is not a common thing to do because not every filter
				// will have values
				if (self.filterSettings.oTypeSettings.aValues === null) {
					self.filterSettings.oTypeSettings.aValues = self.dataTable.fnGetColumnData(self.filterSettings.iTarget);
				}

				// create a select box, then call the multi select plugin on it
				// to
				// convert it to the logic we want and add all of the
				// appropriate callbacks
				var $select = self.createSelectTag({
					sClass : self.filterSettings.oTypeSettings.sClass,
					sWidth : self.filterSettings.oTypeSettings.sWidth
				});

				this.$container.append($select);

				// Then lets make it a multiselect
				// Right now the default is to not have a header but I am
				// keeping
				// the logic for those around
				$select.multiselect({
					height : 'auto',
					// Default to have no headers
					header : false,
					// The selected text is also going to be the label
					selectedText : self.filterSettings.oTypeSettings.sLabel,
					noneSelectedText : self.filterSettings.oTypeSettings.sLabel,
					click : function(event, ui) {
						if (ui.checked) {
							self.filter._checkFilter.call(self, ui.value);
						} else {
							self.filter._uncheckFilter.call(self, ui.value);
						}
						self.filter.filter.call(self);
					},
					checkAll : function() {
						self.filter._checkAllFilter.call(self);
						self.filter.filter.call(self);
					},
					uncheckAll : function() {
						self.filter._uncheckAllFilter.call(self);
						self.filter.filter.call(self);
					}
				});
				// It is thinking the first one is checked, I assume because
				// there
				// is no default "option" in the select, so the first one is the
				// "selected" one. We do not want this. We want all of them to
				// be
				// unchecked. However, I feel like this option should be
				// configurable
				// either via a callback OR an property to say default
				// checked or unchecked.
				$select.multiselect('widget').find('input:checked').each(function() {
					$(this).attr('checked', false);
				});
				// The default is to also have them be checked, so reset
				// above then
				// check them all
				// Supposedly you can have the attribute "selected" added to
				// each option
				// you want
				// checked but that was not working in all browsers.
				// $select.multiselect('widget').find('input:unchecked').each(function()
				// {
				// this.click();
				// });

				// self.currentFilters =
				// self.filterSettings.oTypeSettings.aValues;
				self.currentFilters = [];

				return $select;
			},
			defaults : {
				bDefaultOption : false,
				aValues : null,
				// Adding here but this could go in a more generic location
				// later
				sClass : '',
				// We are explicitly setting the width here for 2 reasons
				// First, this will allow us to have multiple of these on the
				// page at once with different sizes.
				// Secondly, we have to manually put the width on the HTML
				// element we are building because of how the multiSelect plugin works.
				// When the multiSelect plugin runs, it automatically hides the select
				// element. jQuery will return a width of zero for any elements
				// that are display: none, UNLESS there is a physical width
				// property on that element
				sWidth : '180px'
			},
			// This is a required function
			filter : function() {
				var asEscapedFilters = [];
				var sFilterStart = '^(';
				var sFilterEnd = ')$';
				if (this.currentFilters.length > 0) {
					// Filters must have RegExp symbols escaped
					$.each(this.currentFilters, function(i, sFilter) {
						asEscapedFilters.push(fnRegExpEscape(sFilter));
					});

					this.dataTable.fnFilter(sFilterStart + asEscapedFilters.join('|') + sFilterEnd, this.filterSettings.iTarget, true, false);
				} else {
					// Clear any filters for this column
					// The default is: if none are checked, then it shows all.
					// So do that here. We cannot do a full reset because there
					// might be other
					// filters on this table.
					this.dataTable.fnFilter(sFilterStart + this.filterSettings.oTypeSettings.aValues.join('|') + sFilterEnd, this.filterSettings.iTarget, true, false);
				}
			},
			// This is a required function
			reset : function() {
				this.currentFilters = [];
				// TODO: This should be configured, either through callback
				// or property
				// Find any checkbox that is unchecked and check it to
				// restart
				this.$filter.multiselect('widget').find('input:checkbox').each(function() {
					$(this).attr('checked', false);
				});
				// Call the update method on the exist widget
				this.$filter.multiselect('update');
			},
			// This is a function specific to multiSelect
			// This is a callback that gets called any time a filter is checked.
			_checkFilter : function(value) {

				this.currentFilters.push(value);
			},
			// This is a function specific to multiSelect
			// This is a callback that gets called any time a filter is
			// unchecked.
			_uncheckFilter : function(value) {
				this.currentFilters = $.grep(this.currentFilters, function(currentValue) {
					return currentValue != value;
				});
			},
			// This is a function specific to multiSelect
			_checkAllFilter : function() {
				this.currentFilters = this.filterSettings.oTypeSettings.aValues;
			},
			// This is a function specific to multiSelect
			_uncheckAllFilter : function() {
				this.currentFilters = [];
			}
		}
	};

	/**
	 * This is a filter object. This wraps all filters,both predefined and custom, given some common functionality as well as calling the internal filter settings. The constructor will create all of
	 * its public variables and settings as well as set the type of filter it is.
	 * 
	 * $filtersContainer - This is the jQuery container we are adding the filters dataTableSettings - This is all of the properties of the DataTable we are acting on filterSettings - This is the
	 * specific filter properties
	 */
	var Filter = function($filtersContainer, dataTableSettings, filterSettings) {

		// Need to set a private variable = to this object reference so our
		// private methods can access it.
		var self = this;
		self.dataTable = dataTableSettings.oInstance;

		// We need to check to make sure this filter has a type
		if (!filterSettings.hasOwnProperty('sType') && !filterSettings.sType) {
			return; // fail for now
		}
		// Set the type of filter this is
		self.type = filterSettings.sType;

		// Now see if this is a predefined filter, if it is predefined
		// then we are going to see if there are any overrides, if so
		// then do a merge with the predefined and then set this filter
		// those overrides. We do not want to replace the predefined.
		if (predefinedFilters.hasOwnProperty(self.type)) {
			if (filterSettings.hasOwnProperty(self.type) && filterSettings[self.type]) {
				// pass in an empty target to preserve the default
				self.filter = $.extend({}, predefinedFilters[self.type], filterSettings[self.type]);
			} else {
				// Otherwise use the default settings
				self.filter = predefinedFilters[self.type];
			}
		} else {
			// If it doesn't exist in the predefined filters then it is custom,
			// so then
			// just add it
			self.filter = filterSettings[self.type];
		}

		// Anything in filterSettings.oTypeSettings are settings
		// specific to the filter, anything added here is common and
		// filter itself should handle any of its own defaults
		filterSettings.oTypeSettings = $.extend({
			sLabel : ''
		}, filterSettings.oTypeSettings);

		// Setup defaults
		// This stores all current filters applied to column
		self.currentFilters = [];
		// This stores the main container this filter will be wrapped in
		self.$container = $filtersContainer;
		self.filterSettings = filterSettings;

		// Create the column filter for this filter
		var $filter = self.filter.init.call(self);
		// This sets the filter container that was created, might want to rename
		// this
		self.$filter = $filter;
	};

	/**
	 * This is a helper method to build a select tag based on filter properties.
	 */
	Filter.prototype.createSelectTag = function(options) {
		var settings = $.extend({
			sClass : '',
			sWidth : ''
		}, options);

		// Default classes that will be added but also allowing for addition
		// custom ones
		var select = '<select class="search_init select_filter ';
		if (typeof settings.sClass === 'string' && settings.sClass !== '') {
			select += settings.sClass;
		}
		select += '"';
		if (typeof settings.sWidth === 'string' && settings.sWidth !== '') {
			select += 'style="width: ' + settings.sWidth + '"';
		}

		select += '>';

		if (this.filterSettings.oTypeSettings.bDefaultOption) {
			select += '<option value="" class="search_init">' + this.filterSettings.oTypeSettings.sLabel + '</option>';
		}

		var j;
		var iLen = 0;
		// Stupid check to make sure we have some values.
		if (this.filterSettings.oTypeSettings.aValues) {
			iLen = this.filterSettings.oTypeSettings.aValues.length;
		}

		for (j = 0; j < iLen; j++) {
			select += '<option value="' + this.filterSettings.oTypeSettings.aValues[j] + '">' + this.filterSettings.oTypeSettings.aValues[j] + '</option>';
		}
		var $select = $(select + '</select>');

		return $select;
	};

	// Filter this table, with the current state of this column
	Filter.prototype.filter = function() {
		this.filter.filter.call(this);
	};

	Filter.prototype.reset = function() {
		this.filter.reset.call(this);
	};

	/**
	 * This is a faster way to reset all filters, it will reset the search data for each column, then redraw the table once.
	 */
	$.fn.dataTableExt.oApi.fnResetAllFilters = function(oSettings, bDraw) {
		for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
			oSettings.aoPreSearchCols[iCol].sSearch = '';
		}
		oSettings.oPreviousSearch.sSearch = '';

		if (typeof bDraw === 'undefined') {
			bDraw = true;
		}

		if (bDraw) {
			this.fnDraw();
		}

	};

	/**
	 * This will return, as an array, the values of a given column.
	 */
	$.fn.dataTableExt.oApi.fnGetColumnData = function(oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty) {
		// check that we have a column id
		if (typeof iColumn === "undefined") {
			return [];
		}

		// by default we only want unique data
		if (typeof bUnique === "undefined") {
			bUnique = true;
		}

		// by default we do want to only look at filtered data
		if (typeof bFiltered === "undefined") {
			bFiltered = true;
		}

		// by default we do not want to include empty values
		if (typeof bIgnoreEmpty === "undefined") {
			bIgnoreEmpty = true;
		}

		// list of rows which we're going to loop through
		var aiRows;

		// use only filtered rows
		if (bFiltered === true)
			aiRows = oSettings.aiDisplay;
		// use all rows
		else
			aiRows = oSettings.aiDisplayMaster; // all row numbers

		// set up data array
		var asResultData = [];

		for ( var i = 0, c = aiRows.length; i < c; i++) {
			iRow = aiRows[i];
			var sValue = this.fnGetData(iRow, iColumn);
			// var sValue = aData[iColumn];

			// ignore empty values?
			if (bIgnoreEmpty == true && sValue.length == 0) {
				continue;
			}
			// ignore unique values?
			else if (bUnique == true && $.inArray(sValue, asResultData) > -1) {
				continue;
			}
			// else push the value onto the result data array
			else {
				asResultData.push(sValue);
			}
		}

		return asResultData;
	};

	/*
	 * 
	 * Register a new feature with DataTables
	 */
	if (typeof $.fn.dataTable === 'function') {
		$.fn.dataTableExt.aoFeatures.push({
			'fnInit' : function(oDTSettings) {
				// This expects an actual HTML node to be returned.
				// Basically what happens is that we build all of our HTML and
				// controlls and then return them here
				return new Filters(oDTSettings);
			},
			'cFeature' : 'F',
			'sFeature' : 'oCustomFilters'
		});
	}

}(jQuery));

(function($) {

	/*
	 * Register a new feature with DataTables
	 */
	if (typeof $.fn.dataTable === 'function') {
		$.fn.dataTableExt.aoFeatures.push({
			'fnInit' : function(oDTSettings) {
				var filterSelector = oDTSettings.oInit.oFilterControl.sFilterSelector;

				var $dataTablesFilerNode = $('<div>').addClass('dataTables_filter');
				var $filterDivNode = $('<div>').addClass('filter-div');
				var $filterImageNode = $('<img/>').attr('src', '../../images/navigate_open.png');
				var $filterAnchorNode = $('<a>').addClass('dataTables_filterToggle').text('Filter Options');
				$filterAnchorNode.prepend($filterImageNode);
				$filterDivNode.append($filterAnchorNode);
				$dataTablesFilerNode.append($filterDivNode);
				// Add the toggle, inside of the our data table main
				// wrapper
				// Use the object we just created and attach the
				// appropriate
				// event.
				$filterAnchorNode.toggle(function() {
					$(this).children('img').attr('src', '../../images/navigate_close.png');
					$(filterSelector, oDTSettings.oInstance.fnSettings().nTableWrapper).show();
				}, function() {
					$(this).children('img').attr('src', '../../images/navigate_open.png');
					$(filterSelector, oDTSettings.oInstance.fnSettings().nTableWrapper).hide();
				});

				// This expects an actual HTML node to be returned.
				// Basically what happens is that we build all of our
				// HTML and
				// controlls and then return them here
				return $dataTablesFilerNode[0];
			},
			'cFeature' : 'C',
			'sFeature' : 'oFilterControl'
		});
	}

}(jQuery));
