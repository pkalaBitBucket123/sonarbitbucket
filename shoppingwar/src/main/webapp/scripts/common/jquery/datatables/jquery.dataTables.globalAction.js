/**  Copyright (C) 2012 Connecture */
(function($) {
	/*
	 * Register a new feature with DataTables
	 */
	if (typeof $.fn.dataTable === 'function') {
		$.fn.dataTableExt.aoFeatures.push({
			'fnInit' : function(oDTSettings) {
				var $dataTablesActionsWrapper = $('<div></div>').addClass('dataTables_actions');
				var $baseActionsWrapper = $('<div></div>');
				$dataTablesActionsWrapper.append($baseActionsWrapper);

				// We need the menu map, actionSelected and actionSubmitted callbacks. This is a little redundate
				// but it wraps up everything nicely with the datatables.
				// oDTSettings.oInit.oGobalTableAction.menu;
				// oDTSettings.oInit.oGobalTableAction.actionSelected;
				// oDTSettings.oInit.oGobalTableAction.actionSubmitted;

				if ((typeof oDTSettings.oInit.oGobalTableAction === 'undefined') || !oDTSettings.oInit.oGobalTableAction.hasOwnProperty('mActions')
						|| !oDTSettings.oInit.oGobalTableAction.hasOwnProperty('fActionSelected') || !oDTSettings.oInit.oGobalTableAction.hasOwnProperty('fActionSubmitted')) {
					// fail gracefully and quickly
					return $dataTablesActionsWrapper[0];
				}

				$baseActionsWrapper.splitButtonMenu({
					menu : oDTSettings.oInit.oGobalTableAction.mActions,
					actionSelected : function(action) {
						// Expose our own function and pass the data table and the action
						// So the calling script can do what it pleases.
						oDTSettings.oInit.oGobalTableAction.fActionSelected(oDTSettings.oInstance, action);
					},
					actionSubmitted : oDTSettings.oInit.oGobalTableAction.fActionSubmitted
				});

				return $dataTablesActionsWrapper[0];
			},
			'cFeature' : 'G',
			'sFeature' : 'oGobalTableAction'
		});
	}

}(jQuery));