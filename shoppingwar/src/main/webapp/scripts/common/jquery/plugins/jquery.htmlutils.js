/** Copyright (C) 2012 Connecture
 *
 * HTML Utils
 *
 */
(function($) {
	$.htmlutils = {};
	$.htmlutils.buildOption = function(displayText, value, selectedValue) {
		var optionHtml = '<option value="' + value + '"';
		if (value == selectedValue) {
			optionHtml += ' selected="selected"';
		}
		optionHtml += '>';
		optionHtml += displayText;
		optionHtml += '</option>';
		return optionHtml;
	}
	$.htmlutils.isBlank = function(text) {
		text = $.trim(text);
		return text == null || text == '';
	}
	$.htmlutils.blockUI = function(options) {
		$.blockUI({
			overlayCSS: {backgroundColor: '#fff', opacity:0.75},
			message: '<div class="actionindicator"><span class="message"><br>' + options.message +'</span></div>',
			css: { backgroundColor: 'none', color: '#333', border:'none'}
		});
	}
	$.htmlutils.addDatepicker = function(parentDiv) {
		var inputField = $('input.date_type', parentDiv);
		// date picker
		inputField.datepicker({
			showOn: 'button',
			buttonImage: '../img/calendar-blue.png',
			buttonText: 'Choose a date',
			buttonImageOnly: true
		});
		// date place holder
		inputField.attr('placeholder', 'mm/dd/yyyy');
		inputField.placeholder();
	}
	$.htmlutils.applyMasking = function(parentDiv) {
		// MM/DD/YYYY mask 
		$('input.date_mask_type1', parentDiv).mask("?99/99/9999",{placeholder:" "});
		// MM/YYYY
	   	$('input.date_mask_type2', parentDiv).mask("?99/9999",{placeholder:" "});
		// ###-##-#### mask
	   	$('input.ssn_mask', parentDiv).mask("?999-99-9999",{placeholder:" "});
		// (###)###-#### mask
	   	$('input.phone_mask_type1', parentDiv).mask("?(999)999-9999",{placeholder:" "});
		// ###-###-#### mask
	   	$('input.phone_mask_type2', parentDiv).mask("?999-999-9999",{placeholder:" "});
		// ####-####-####-#### mask
	   	$('input.creditcard_mask', parentDiv).mask("?9999-9999-9999-9999",{placeholder:" "});
		// ##-####### mask
	   	$('input.fed_task_id_mask', parentDiv).mask("?99-9999999",{placeholder:" "});
	}
})(jQuery);