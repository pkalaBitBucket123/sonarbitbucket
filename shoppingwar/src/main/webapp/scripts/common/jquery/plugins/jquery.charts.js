/*
 * Copyright (C) 2012 Connecture Version: 0.2 Dependency: jQuery
 */

(function($) {

	var methods = {
		init : function(options) {

			return this.each(function() {
				var $element = $(this);
				var opts = $.extend({
					'radius' : 50,
					'color' : '#4391B5',
					'stroke' : '#357390',
					'strokeWidth' : 5,
					'bgColor' : '#CECECE',
					'bgRadius' : 45,
					'bgStroke' : '#B5B5B5',
					'bgStrokeWidth' : 1,
				}, options);

				var percent = opts.percent;

				var diameter = opts.radius * 2;
				var center = opts.radius;
				var bgdiameter = opts.bgRadius * 2;

				var drawX = drawY = opts.radius + opts.strokeWidth;
				var drawRadius = opts.radius - opts.strokeWidth;
				var drawDiameter = (opts.radius + opts.strokeWidth) * 2;

				var html = '';
				var ieversion = connecture.getinternetexplorerversion();

				if (ieversion < 9 && ieversion != -1) {
					// Calculate Pie
					var degreeVML = (percent * 360) * 65535;
					var bgdiff = (opts.radius - opts.bgRadius);
					opts.radius = opts.radius + opts.strokeWidth;

					html += '<v:group coordorigin="0,0" coordsize="' + drawDiameter + ',' + drawDiameter + '" style="position:absolute;top:0px;left:0px;width:' + drawDiameter + 'px;height:'
							+ drawDiameter + 'px;visibility:visible;z-index:1;behavior:url(#default#VML);display:inline-block;" xmlns:v="urn:schemas-microsoft-com:vml">';
					html += '<v:oval class="pie-bg" style="position:absolute;top:' + bgdiff + 'px;left:' + bgdiff + 'px;width:' + bgdiameter + 'px;height:' + bgdiameter
							+ 'px;visibility:visible;z-index:1;behavior:url(#default#VML);display:inline-block;" xmlns:v="urn:schemas-microsoft-com:vml"></v:oval>';
					if (percent >= 1)
						html += '<v:oval class="pie-fill" style="position:absolute;top:0px;left:0px;width:' + diameter + 'px;height:' + diameter
								+ 'px;visibility:visible;z-index:1;behavior:url(#default#VML);display:inline-block;" xmlns:v="urn:schemas-microsoft-com:vml"></v:oval>';
					else
						html += '<v:shape class="pie-fill" style="position:absolute;top:0px;left:0px;width:' + diameter + 'px;height:' + diameter
								+ 'px;visibility:visible;z-index:1;behavior:url(#default#VML);display:inline-block;" path="m ' + drawX + ',' + drawY + ' ae ' + drawX + ',' + drawY + ',' + opts.radius
								+ ',' + opts.radius + ',5898150,' + degreeVML + ' x e" xmlns:v="urn:schemas-microsoft-com:vml" ></v:shape>';
					html += '</v:group>';

					$element.html(html);
					$element.find('.pie-fill').prop("fillcolor", opts.color);
					$element.find('.pie-fill').prop("strokecolor", opts.stroke);
					$element.find('.pie-fill').prop("strokeweight", opts.strokeWidth);
					$element.find('.pie-bg').prop("fillcolor", opts.bgColor);
					$element.find('.pie-bg').prop("strokecolor", opts.bgStroke);
					$element.find('.pie-bg').prop("strokeweight", opts.bgStrokeWidth);
				} else {
					// Calculate Pie
					var angle = ((1 - percent) * 360 * Math.PI / 180);
					var posX = drawX + opts.radius * Math.sin(angle);
					var posY = drawY + opts.radius * -Math.cos(angle);

					html += '<svg xmlns="http://www.w3.org/2000/svg" width="' + drawDiameter + 'px" height="' + drawDiameter + 'px" version="1.1">';
					html += '<circle cx="' + drawX + '" cy="' + drawY + '" r="' + opts.bgRadius + '" fill="' + opts.bgColor + '" stroke="' + opts.bgStroke + '" stroke-width="' + opts.bgStrokeWidth
							+ '"/>';
					if (percent >= 1)
						html += '<circle cx="' + drawX + '" cy="' + drawY + '" r="' + opts.radius + '" fill="' + opts.color + '" stroke="' + opts.stroke + '" stroke-width="' + opts.strokeWidth
								+ '"/>';
					else if (percent > 0.5)
						html += '<path d="M ' + drawX + ',' + drawY + ' V' + (drawY - opts.radius) + ' A' + opts.radius + ',' + opts.radius + ' 0 1,0 ' + posX + ',' + posY + ' Z" fill="' + opts.color
								+ '" stroke="' + opts.stroke + '" stroke-width="' + opts.strokeWidth + '"/>';
					else if (percent > 0)
						html += '<path d="M ' + drawX + ',' + drawY + ' V' + (drawY - opts.radius) + ' A' + opts.radius + ',' + opts.radius + ' 0 0,0 ' + posX + ',' + posY + ' Z" fill="' + opts.color
								+ '" stroke="' + opts.stroke + '" stroke-width="' + opts.strokeWidth + '"/>';
					html += '</svg>';
					$element.html(html);
				}
				$element.css('position', 'relative');
				$element.css('height', drawDiameter);
				$element.css('width', drawDiameter);
				$element.css('float', 'left');
			});
		},
		destroy : function() {

			return this.each(function() {

			});

		}
	};

	$.fn.pieChart = function(method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.pieChart');
		}

	};

})(jQuery);