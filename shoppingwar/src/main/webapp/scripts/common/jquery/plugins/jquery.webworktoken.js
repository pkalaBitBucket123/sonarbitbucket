/** Copyright (C) 2012 Connecture
 *
 * Webwork Token JQuery Plugin
 *
 * Maintains the struts.token.name and value.  Provides convenience methods to 
 * append the token to request json and extract values from a response json.
 */
(function($) {
    $.webworktoken = {
        TOKEN_NAME_FIELD : 'struts.token.name',
        // struts.token.name
        _tokenName : null,
        _tokenValue : null,

        /**
         * Generates a new unique token name for the current client
         */
        generateTokenName : function(prefix) {
            var nameArray = [];
            var hexDigits = "0123456789ABCDEF";
            if (prefix) {
                nameArray.push(prefix);
            }
            for ( var i = 0; i < 6; i++) {

                nameArray.push(hexDigits.substr(Math
                        .floor(Math.random() * 0x10), 1));
            }
            this._tokenName = nameArray.join('');
        },

        /*
         * Reads the token name and value from the jsonRepsonse if present and
         * updates the token value.
         */
        updateToken : function(jsonResponse) {
            if (jsonResponse[this.TOKEN_NAME_FIELD]) {
                this._tokenName = jsonResponse[this.TOKEN_NAME_FIELD];
                if (jsonResponse[this._tokenName]) {
                    this._tokenValue = jsonResponse[this._tokenName];
                }
            }
        },

        /*
         * Appends the token name and value if present to the jsonRequest.
         */
        appendToken : function(jsonRequest) {
            if (this._tokenName) {
                jsonRequest[this.TOKEN_NAME_FIELD] = this._tokenName;
                if (this._tokenValue) {
                    jsonRequest[this._tokenName] = this._tokenValue;
                }
            }
        },        
        /*
         * Appends the token name and value if present to the jsonRequest as an Array.
         */
        appendTokenAsArray : function(jsonRequest) {
            if (this._tokenName) {
            		jsonRequest.push({"name": this.TOKEN_NAME_FIELD, "value": this._tokenName});
                if (this._tokenValue) {
                	jsonRequest.push({"name": this._tokenName, "value": this._tokenValue});
                }
            }
        },
        setTokenNameValue : function(tokenName,tokenValue){
        	this._tokenName=tokenName;
        	this._tokenValue=tokenValue;
        }  
    };
})(jQuery);