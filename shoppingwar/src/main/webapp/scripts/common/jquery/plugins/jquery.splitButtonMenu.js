/**
 * Copyright (C) 2012 Connecture
 * 
 * This plugin has been updated so that if no menu exists the button is then disabled.
 * 
 * Can be configured if it becomes an issue.
 */

(function($) {

	var methods = {
		init : function(options) {

			var settings = $.extend({
				/*
				 * If menu = null : then it assumes one exists If it is an object, then we are assuming those are the values menu : { test : test, test2 : test, test 3 : test }
				 */
				menu : null,
				defaultText : 'Select Action',
				defaultOption : true,
				actionSelected : function(action) {
				},
				actionSubmitted : function(action) {
				}
			}, options);

			return this.each(function() {
				var self = this;
				// Forcing this to be done on a containing block around a select as the
				// default. This way the plugin doesn't have two options when ti comes
				// to which selector to call it on, maybe it isn't a big deal.
				var menu = settings.menu;

				// This means that we are building the menu from scratch
				if (menu === null) {
					menu = {};
					// this means we have the menu already, grabbing the first select to be safe, should only be one
					$selectMenu = $(this).children('select:first');
					$('option', $selectMenu).each(function() {
						// build out the menu
						menu[this.value] = this.text;
					});
					// hide it cause we do not need it anymore
					$selectMenu.hide();
				}

				// Now build our menu
				var $menu = $('<ul></ul>').addClass('ui-button-menu');
				// Add our default option first
				// We will use the default text as the name of the default option
				if (settings.defaultOption) {
					var $defaultAction = $('<a></a>').attr('data-action', '').text(settings.defaultText);
					var $defaultMenuItem = $('<li></li>').addClass('ui-button-menu-item').append($defaultAction);
					$menu.append($defaultMenuItem);
				}

				$.each(menu, function(key, value) {
					var $actionLink = $('<a></a>').attr('data-action', key).text(value);
					var $menuItem = $('<li></li>').addClass('ui-button-menu-item').append($actionLink);
					$menu.append($menuItem);

				});

				var isMenuEmpty = $menu.children().length === 0;

				// Add a theme class
				$(this).addClass('ui-split-button-menu');

				// Now we need to add our buttons to handle the split menu
				var $actionButton = $('<button></button>').text(settings.defaultText);
				var $selectButton = $('<button></button>').text('Select an action');

				// Add the two buttons and then add the menu
				$(this).append($actionButton).append($selectButton).append($menu);

				// Setup all of our buttons and events
				$actionButton.button().click(function() {
					// Grab our action
					var action = $(this).attr('data-action');
					var isDisabled = $(this).hasClass('ui-state-disabled');
					var text = $(this).text();
					// Make sure we have some valid action, but be overkill but eh
					if (typeof action !== 'undefined' && action !== null && action !== '' && !isDisabled) {
						// Take the action and then pass it to a callback method,
						// The context will be the button that was clicked,
						// in case we need to get access to other data
						settings.actionSubmitted.call(this, action, text);
						$(this).children('span').text(settings.defaultText);
					}
				}).next().button({
					text : false,
					icons : {
						primary : "ui-icon-triangle-1-s"
					}
				}).click(function() {
					// Toggle the menu we created for this one
					// Only toggle if we have something to toggle
					// Since this can change dynamically
					// need to check here
					if ($menu.children().length > 0) {
						$menu.toggle();
					}

				}).next().bind('click', function(event) {
					var node = event.target.nodeName;
					if (node === 'A') {
						node = $(event.target);
					} else if (node === 'LI') {
						node = $(event.target).children('a');
					} else {
						node = null;
					}

					if (node) {
						// Grab the text, although since this is a button set to need grab it from the span
						var actionText = node.text();
						var action = node.attr('data-action');
						$actionButton.button('option', 'label', actionText);
						$actionButton.attr('data-action', action);
						$menu.toggle();
						// Now call the callback, currently we are not giving this callbacks any context
						settings.actionSelected(action);
					}

				});
				// Need to do this after the buttons are setup cause it
				// seems jQuery likes to remove all
				if (isMenuEmpty) {
					$actionButton.addClass('ui-state-disabled');
					$selectButton.addClass('ui-state-disabled');
				}

				// This latest one will turn the rest of the stuff into a buttons as well
				// Adds some automatic effects
				// Removed for now to make it easier to style
				// $(this).buttonset();
			});

		},
		// This will take in an updated menu
		// You will remove the current menu items and add the existing ones
		// I would update the above code so that at line 85 instead of
		// doing a click event on the anchor tag, we do a click event on
		// the <ul> tag then use the "event" argument from the click event
		// to figure out what we clicked on. This way we do not need to
		// redo the event binding each time we update the menu
		update : function(content) {
			var $menu = $(this).children('.ui-button-menu');

			$menu.children().remove();

			$.each(content, function(key, value) {
				var $actionLink = $('<a></a>').attr('data-action', key).text(value);
				var $menuItem = $('<li></li>').addClass('ui-button-menu-item').append($actionLink);
				$menu.append($menuItem);
			});

			var isMenuEmpty = $menu.children().length === 0;
			if (isMenuEmpty) {
				$(this).children('button').addClass('ui-state-disabled');
			} else {
				$(this).children('button').removeClass('ui-state-disabled');
			}

		}
	};

	$.fn.splitButtonMenu = function(method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.tooltip');
		}

	};
})(jQuery);