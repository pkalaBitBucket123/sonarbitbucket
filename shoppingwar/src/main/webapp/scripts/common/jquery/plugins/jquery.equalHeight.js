/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 */
(function($) {
	// Save off the initial height and width of the window
	var winWidth = $(window).width(), winHeight = $(window).height();

	// Function reference that will set the equal heights
	var setHeights = function(maxHeight) {
		var maxHeight = 0;
		// Loop through each one to find the max height, then set the height
		// after
		this.each(function() {
			$(this).css('min-height', '').css('height', 'auto');

			if ($(this).height() > maxHeight) {
				/* Edited for Connecture */
				maxHeight = $(this).height() + 3;
				/* End edited for Connecture */
			}
		}).each(function() {
			$(this).css('min-height', maxHeight + 'px');
		});
	};

	var methods = {
		start : function() {
			var self = $(this);
			// Call the initial set height method
			setHeights.call(self);

			return this.each(function() {
				$(window).bind('resize.equal', function() {

					// New height and width
					var winNewWidth = $(window).width(), winNewHeight = $(window).height();

					// compare the new height and width with old one
					if (winWidth != winNewWidth || winHeight != winNewHeight) {
						// This would be nice to use, but I don't think it will 100% work with the
						// correct this reference in IE
						// var resizeTimeout = window.setTimeout(setHeights, 10);
						// window.clearTimeout(resizeTimeout);
						setHeights.call(self);
					}
					// Update the width and height
					winWidth = winNewWidth;
					winHeight = winNewHeight;
				});
			});
		},
		stop : function() {
			return this.each(function() {
				$(window).unbind('resize.equal');
			});
		}
	};

	$.fn.equal_height = function(method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.cart_resize');
		}

	};

})(jQuery);