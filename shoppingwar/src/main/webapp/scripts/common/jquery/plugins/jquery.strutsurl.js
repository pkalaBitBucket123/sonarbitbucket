/**
 * Copyright (C) 2012 Connecture
 * 
 * This is really an override script for jquery.webworkurl because of some defects in that file.
 * 
 * Instead of the override, we should be fixing the original file but we need to update it in all projects first before we can do that. This will override the methods that are having issues for GPA
 * only.
 */
(function($) {

	$.strutsurl = {

		url : function(namespace, action, params) {
			return this.replaceUrl(null, null, namespace, action, params);
		},

		/**
		 * Create a webwork html url based on the current url.
		 */
		htmlUrl : function(namespace, action, params) {
			return this.replaceUrl(null, '.html', namespace, action, params);
		},

		htmUrl : function(namespace, action, params) {
			return this.replaceUrl(null, '.htm', namespace, action, params);
		},

		/**
		 * Create a webwork url based on an existing url.
		 */
		replaceUrl : function(url, extension, namespace, action, params, anchor) {
			var parsedUrl = $.url.parse(url);
			if (namespace !== null) {
				parsedUrl.segments[1] = namespace;
			}
			if (extension != null) {
				action = action + extension;
			} else {
				if (!action.match(/.action$/)) {
					action = action + '.action';
				}
			}
			var mergedParams = null;
			if (params) {
				mergedParams = {};
				$.each(params, function(key, value) {
					if (value) {
						mergedParams[key] = value;
					} else if (typeof parsedUrl.params !== 'undefined' && parsedUrl.params.hasOwnProperty(key)) {
						mergedParams[key] = parsedUrl.params[key];
					}
				});
			}
			var newUrl = {
				protocol : parsedUrl.protocol,
				host : parsedUrl.host,
				port : parsedUrl.port,
				segments : parsedUrl.segments,
				file : action,
				params : mergedParams,
				anchor : anchor
			};
			return $.url.build(newUrl);
		},

		/**
		 * This method will take the current parameters of the url and add them to the
		 */
		getParameters : function(url) {
			var parsedUrl = $.url.parse(url);

			return parsedUrl.params;
		},

		addParameters : function(url, params) {
			var parsedUrl = $.url.parse(url);
			// We need it to rebuild itself
			// so null our source
			parsedUrl.source = null;

			parsedUrl.params = params;

			return $.url.build(parsedUrl);
		}
	};

})(jQuery);
