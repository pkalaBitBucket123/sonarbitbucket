/**
 * Copyright (C) 2012 Connecture
 * 
 * Error Utils
 * 
 */
(function($) {
	$.errorutils = {};
	$.errorutils.checkAccess = function(data) {
		if (typeof data.resultCode !== 'undefined') {
			if (data.resultCode === 'unauthorized') {
				// window.location = "/unauthorized.action";
			}
		}
	};
	/**
	 * This method is for errors coming back from server requests
	 */
	$.errorutils.validationError = function(validationErrorDiv, data) {
		validationErrorDiv.empty().hide();
		var actionErrors = [];
		var fieldErrors = null;
		var isError = false;
		if (typeof data.actionErrors !== 'undefined') {
			actionErrors = data.actionErrors;
			isError = true;
		}
		if (typeof data.fieldErrors !== 'undefined') {
			fieldErrors = data.fieldErrors;
			isError = true;
		}

		if (isError) {
			// Add back later or put somewhere else
			// $.webworktoken.updateToken(data);
			$('<p><em>The following errors were encountered:</em></p><ul>').appendTo(validationErrorDiv);
			for ( var i = 0; i < actionErrors.length; i++) {
				$('<li>' + actionErrors[i] + '</li>').appendTo(validationErrorDiv);
			}
			// Only process if there are fieldErrors, since this could be an
			// object
			// we can't try and loop t
			if (fieldErrors) {
				// If it is an array then we only have
				// one form to process and we can just do the standard searching
				if ($.isArray(fieldErrors)) {
					// Not sure I like how I set this up now, this is an array
					// of objects
					// but each object SHOULD only have one key/value pair
					$.each(fieldErrors, function(index, error) {
						for ( var key in error) {
							if (error.hasOwnProperty(key)) {
								$.errorutils.setInputError(key, $('form'), error[key]);
							}
						}

					});
				} else {
					// Otherwise, if it is an object, then we want to
					// there are multiple forms on the page that might
					// have errors
					for ( var formName in fieldErrors) {
						if (fieldErrors.hasOwnProperty(formName)) {
							// These will be in order
							var $forms = $('form[name="' + formName + '"]');

							// If it is an error that means this form
							// is repeatable and has multiple
							if ($.isArray(fieldErrors[formName])) {
								var forms = fieldErrors[formName];
								// Now we will loop through the forms and then
								// set the errors, for that form
								$forms.each(function(index, value) {
									var $form = $(this);
									var formFields = forms[index];
									// If we have errors, then let's process
									if (!$.isEmptyObject(formFields)) {
										$.each(formFields, function(fieldName, fieldMessage) {
											$.errorutils.setInputError(fieldName, $form, fieldMessage);
										});
									}
								});
							} else {
								// otherwise there is just one
								var formFields = fieldErrors[formName];
								$.each(formFields, function(fieldName, fieldMessage) {
									$.errorutils.setInputError(fieldName, $forms, fieldMessage);
								});
							}

						}

					}

				}
			}

			$('</ul><p>Data has <b>not</b> been saved.</p>').appendTo(validationErrorDiv);
			validationErrorDiv.show();
			$(validationErrorDiv).parent().parent().scrollTo(0, 800);
		}

		return isError;
	};

	/**
	 * 
	 */
	$.errorutils.processFormValidation = function($currentElements, errorList, $form) {
		if (errorList.length > 0) {
			$.each(errorList, function(index, value) {
				$.errorutils.setInputError(value.element.name, $form, value.message);
			});
		} else {
			// Just in case there are multiples, this will be easier
			// to maintain
			$currentElements.each(function() {
				// Hmm, since we have the context and
				// the element, lets just do a find from the
				// $form and remove it. This will handle
				// multiple different scenarios
				var $element = $(this);
				$('._errorDiv.' + $element.attr('name'), $form).remove();
				$element.removeClass('error');
				$element.parent('.dataGroup.hasErrors').removeClass('hasErrors');
			});
		}
	};

	/**
	 * hasErrors
	 * 
	 * This is a common method to attach an error to a field.
	 */
	$.errorutils.setInputError = function(fieldName, $context, errorMessage) {
		var $input = $(':input[name="' + fieldName + '"]', $context);
		var $fieldWrapper = $input.closest('.dataGroup');
		// Handle common error stuff
		$fieldWrapper.addClass('hasErrors');
		$input.addClass('error');

		// Add the fieldname here for the multi field cases
		var $errorDiv = $('<div></div>').addClass('dataGroup').addClass('_errorDiv').addClass(fieldName).append($('<div></div>').addClass('fieldError'));
		$errorDiv.children('div').text(errorMessage);
		// Now we need to check to see if this is apart of a multigroup.
		// Checking direct parent right now.
		if ($fieldWrapper.parent('.multiFields').length > 0) {
			var $multiFields = $fieldWrapper.parent('.multiFields');
			// Grab the parent because we will need to see if
			// an error already exists
			var $multiGroup = $multiFields.closest('.multiGroup');

			// We want to remove it for case where the error message might
			// have changed for a different reason. We don't want to maintain
			// current error messages so remove it and then add the new one.
			var $existingError = $multiGroup.find('._errorDiv.' + fieldName);
			if ($existingError.length > 0) {
				$existingError.remove();
			}
			$multiFields.after($errorDiv);
		} else {
			if ($fieldWrapper.next().hasClass('_errorDiv')) {
				$fieldWrapper.next().remove();
			}
			$fieldWrapper.after($errorDiv);
		}

	};
	$.errorutils.errorMessage = function(validationErrorDiv, message) {
		validationErrorDiv.empty().hide();
		$('<p><em>The following errors were encountered:</em></p><ul>').appendTo(validationErrorDiv);
		$('<li>' + message + '</li>').appendTo(validationErrorDiv);
		$('</ul><p>Data has <b>not</b> been saved.</p>').appendTo(validationErrorDiv);
		validationErrorDiv.show();
	};
	$.errorutils.removeFieldErrors = function($form) {
		$(':input', $form).each(function() {
			var $wrapper = $(this).closest('.dataGroup');
			if ($wrapper.next().hasClass('_errorDiv')) {
				$wrapper.next().remove();
			}
		});
	};
	$.errorutils.serverError = function(serverErrorDialog, data, refreshData) {
		serverErrorDialog.empty().hide();
		var isError = false;
		if (typeof (data.resultCode) !== 'undefined') {
			isError = true;
			$.webworktoken.updateToken(data);
			// general server error
			if (data.resultCode == 'error') {
				var errorMessage = '<p>message: ' + data.message + '</p>';
				errorMessage += '<p>exceptionName: ' + data.exceptionName + '</p>';
				errorMessage += '<p>exceptionClass: ' + data.exceptionClass + '</p>';
				errorMessage += '<p>exceptionStack: ' + data.exceptionStack + '</p>';
				serverErrorDialog.html(errorMessage);
				serverErrorDialog.dialog({
					title : "General Server Error",
					autoOpen : false,
					width : 600,
					height : 400,
					resizable : false,
					modal : true
				});
			}
			// concurrency error
			if (data.resultCode == 'concurrency') {
				var errorMessage = '<p>' + data.message + '</p>';
				serverErrorDialog.html(errorMessage);
				serverErrorDialog.dialog({
					title : "Concurrency Error",
					autoOpen : false,
					width : 300,
					height : 200,
					resizable : false,
					modal : true,
					close : function() {
						if (refreshData) {
							refreshData();
						}
					}
				});
			}
			serverErrorDialog.dialog('open');
		}

		return isError;
	};
})(jQuery);