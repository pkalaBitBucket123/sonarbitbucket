/**
 * Copyright (C) 2012 Connecture
 * 
 * As of now, we will include the core filter templates in here. If a new template needs to get added, it will have to be included in here until we come up with the best standard for dynamically
 * including templates
 * 
 * We might also want to break out some of these filter objects, if we find we need to use them independently
 */
define([ "require", 'underscore', 'templateEngine', 'text!html/shopping/views/filters/filter.checkbox.dust', 'text!html/shopping/views/filters/filter.select.dust',
		'text!html/shopping/views/filters/filter.slider.dust', 'text!html/shopping/views/filters/filter.hidden.dust', 'text!html/shopping/views/filters/filter.radio.dust','jquery.selectToUISlider' ], function(app, _, templateEngine, checkboxTemplate,
		selectTemplate, sliderTemplate, hiddenTemplate, radioTemplate) {

	templateEngine.load(checkboxTemplate, 'filter.checkbox');
	templateEngine.load(selectTemplate, 'filter.select');
	templateEngine.load(sliderTemplate, 'filter.slider');
	templateEngine.load(hiddenTemplate, 'filter.hidden');
	templateEngine.load(radioTemplate, 'filter.radio');

	// Template mapping
	var templates = {
		'checkbox' : 'filter.checkbox',
		'hidden' : 'filter.hidden',
		'slider' : 'filter.slider',
		'radio' : 'filter.radio',
		'select' : 'filter.select'
	};

	// Filter Plugins
	// Using this type of design because I don't
	// want to create specific instance objects of a filter
	// type. I might have other things that will also
	// get plugged in, like how filtering works

	var controllers = {
		'checkbox' : {
			getEvent : function() {
				return {
					type : 'change',
					selector : ':input[type="checkbox"]'
				};
			},
			processActiveFilters : function(event, filters) {
				var eventFilter = this.getFilter(event);
				if ($(event.currentTarget).is(':checked')) {
					eventFilter.active = true;
					// filters.push(eventFilterId);
				} else {
					eventFilter.active = false;
				}
			},
			getFilterId : function(event) {
				return $(event.currentTarget).attr('data-filter-id');
			},
			bind : function(context) {

			}
		},
		'slider' : {
			getEvent : function() {
				return {
					type : 'slidestop',
					selector : '.ui-slider'
				};
			},
			processActiveFilters : function(event) {
				// Unselect all to start
				_.each(this.filters, function(filter) {
					filter.active = false;
				});

				// need to figure out which one is selected
				var selectedVal = $(event.currentTarget).prev('select').children('option:selected').val();
				if (selectedVal !== '') {
					var eventFilter = this.getFilter(event);
					eventFilter.active = true;
				}
			},
			getFilterId : function(event) {
				return $(event.currentTarget).prev('select').children('option:selected').attr('data-filter-id');
			},
			bind : function(context) {
				$('select', context).selectToUISlider().hide();
			}
		},
		'hidden' : {
			getEvent : function() {
				return {
					type : 'click',
					selector : '._restoreLinkDummy'
				};
			},
			processActiveFilters : function(event, filters) {

			},
			getFilterId : function(event) {
				return $(event.currentTarget).attr('data-filter-id');
			},
			bind : function(context) {

			}
		},
		'radio' : {
			getEvent : function() {
				return {
					type : 'change',
					selector : ':input[type="radio"]'
				};
			},
			processActiveFilters : function(event, filters) {
				// Unselect all to start
				_.each(this.filters, function(filter) {
					filter.active = false;
				});

				// need to figure out which one is selected
				var eventFilter = this.getFilter(event);
				if ($(event.currentTarget).is(':checked')) {
					eventFilter.active = true;
				}
			},
			getFilterId : function(event) {
				return $(event.currentTarget).attr('data-filter-id');
			},
			bind : function(context) {

			}
		},
		'select' : {
			getEvent : function() {
				return {
					type : 'change',
					selector : 'select'
				};
			},
			processActiveFilters : function(event) {
				// Unselect all to start
				_.each(this.filters, function(filter) {
					filter.active = false;
				});

				// need to figure out which one is selected
				var selectedVal = $('option:selected', event.currentTarget).val();
				if (selectedVal !== '') {
					var eventFilter = this.getFilter(event);
					eventFilter.active = true;
				}
			},
			getFilterId : function(event) {
				return $('option:selected', event.currentTarget).attr('data-filter-id');
			},
			bind : function(context) {

			}
		}
	};

	var createFilter = function(filterType, options) {
		var filter = null;

		switch (filterType) {
		case "filter":
			filter = new Filter(options);
			break;
		case "hide":
			filter = new HideFilter(options);
			break;
		case "show":
			filter = new ShowFilter(options);
			break;
		}

		return filter;
	};

	/**
	 * We will get a set of filter configuration
	 * 
	 * We will get a set of filter data
	 * 
	 * Give the filter configuration we will need to calculate the current amount of data available for that filter
	 * 
	 */
	// by rsingh for user stories 228 & 230
	var Filters = function(data, options, coverageType) {
		var self = this;
		this.data = data;
		this.unfilteredData = data;
		this.configuration = options;
		var coverageType = coverageType;// This contains all filter definitions
		this.filterGroups = {};
		// This contains all active filters, by definition
		// We are storing this by definition, so that we
		// can properly clean and add filters, differs by type
		// this.activeFilters = {};
		// upon construction, lets load
		// this list, shouldn't really
		// be adding after the fact
		var filterConfigs = this.configuration.filters;
		_.each(filterConfigs, function(filterConfig, id) {
			self.add(id, filterConfig, coverageType);
		});

		// Right now the hide filter is a special built in
		// filter
		// TODO | take out TOGGLE Group.. doesn't really reflect what we're doing here anymore
		// ... just need a group for HideFilter
		var hiddenFilter = new ToggleGroup();
		self.filterGroups[hiddenFilter.id] = hiddenFilter;

		// Then let's do an initial calculation of the totals
		self.calculateTotals();
	};

	Filters.prototype = {
		getFilterGroup : function(event) {
			// First check the element we are on
			var filterGroupId = '';
			if ($(event.currentTarget).is('[data-filter-definition-id]')) {
				filterGroupId = $(event.currentTarget).attr('data-filter-definition-id');
			} else {
				filterGroupId = $(event.currentTarget).closest('._filter').attr('data-filter-definition-id');
			}

			// Get the filter definition, then event was done on
			var filterGroup = this.filterGroups[filterGroupId];

			return filterGroup;
		},
		/**
		 * This method is called when a user has selected a filter
		 * 
		 * @param event
		 */
		processEvent : function(event) {
			// Given an event this will process a filter, deciding
			// if it should be added to removed from the active filters
			var filterGroup = this.getFilterGroup(event);

			filterGroup.processActiveFilters(event);
		},
		getActiveFilters : function() {
			// This needs to take the definitions and collapse them
			// into an object of filters
			// This going to be slow?
			var retVal = [];
			// TODO - I feel like we could refactor this
			_.each(this.filterGroups, function(filterGroup) {
				_.each(filterGroup.filters, function(filter) {
					if (filter.active) {
						retVal.push(filter);
					}
				});
			});

			return retVal;
		},
		getInactiveFilters : function() {
			// This needs to take the definitions and collapse them
			// into an object of filters
			// This going to be slow?
			var retVal = [];
			// TODO - I feel like we could refactor this
			_.each(this.filterGroups, function(filterGroup) {
				_.each(filterGroup.filters, function(filter) {
					if (!filter.active) {
						retVal.push(filter);
					}
				});
			});

			return retVal;
		},
		setActiveGroupValues : function() {

			var filterGroupValueMap = {};

			// 1. construct "groupValue" for each filterGroup
			var filterGroupsObj = this.filterGroups;

			$.each(filterGroupsObj, function(key, filterGroup) {

				$.each(filterGroup.filters, function(key, filter) {

					// "exclusive" marks whether or not the filter combines with
					// other filters in the same group for a cumulative result.
					filter.exclusive = filterGroup.configuration.exclusive;

					// "filterGroupId" is required to determine how many filters
					// of a filterGroup are being applied to a collection of products/models;
					// for the moment, this is only used in the case of determining
					// potential product counts for inactive filters, in which case we want
					// only the inactive filter and no other filters for that same group
					// to be applied to product/model list.
					//
					// N.B.: inactive filters will still have reference to the active groupValue,
					// which we are constructing below; this allows us to evaluate all products
					// against the current groupValue + the inactive filter value (for non-exclusive filters)
					// to determine a product-filter match.
					// - CK (12/12/12) - admittedly kind of a hack
					filter.filterGroupId = filterGroup.id;

					filter.groupValue = null;

					// we want to end up with only the currently selected,
					// or "active", values in the groupValue property.
					if (filter.active) {

						var valuesArray = filterGroupValueMap[filterGroup.id];

						if (valuesArray) {

							// add to the extisting groupValue for the filterGroup

							if (_.isArray(filter.value)) {
								for ( var i = 0; i < filter.value.length; i++) {
									var filterObj = {};
									filterObj.type = filter.type;
									filterObj.value = filter.value[i];

									valuesArray[valuesArray.length] = filterObj;
								}
							} else {
								var filterObj = {};
								filterObj.type = filter.type;
								filterObj.value = filter.value;

								valuesArray[valuesArray.length] = filterObj;
							}
						} else {

							// create the first entry in the groupValue for the filterGroup

							var valueObjArray = new Array();

							if (_.isArray(filter.value)) {
								var filterObjArray = new Array();

								for ( var j = 0; j < filter.value.length; j++) {
									var filterObj = {};
									filterObj.type = filter.type;
									filterObj.value = filter.value[j];

									filterObjArray[j] = filterObj;
								}

								valueObjArray = filterObjArray;
							} else {
								var filterObj = {};
								filterObj.type = filter.type;
								filterObj.value = filter.value;

								valueObjArray[0] = filterObj;
							}

							filterGroupValueMap[filterGroup.id] = valueObjArray;
						}
					}
				});
			});

			// 2. set groupValue (active values for the group) into
			// filters (both active and inactive)
			$.each(filterGroupsObj, function(key, filterGroup) {

				$.each(filterGroup.filters, function(key, filter) {
					// now that map is built, assign each groupValue to its respective filters
					// NOTE: this should add current groupValue to even the disabled filters

					if (filterGroupValueMap[filterGroup.id]) {
						var filterGroupValue = filterGroupValueMap[filterGroup.id];
						filter.groupValue = filterGroupValue;
					}
				});
			});

		},
		/**
				 * This method will calculate the sums of all filters. This means that given what is active, we need to filter to then show the remaining set of data. That means that we almost need to
				 * do the filter here as well, which is kind of cac.
		 */
		calculateTotals : function() {
			var self = this;
			_.each(self.filterGroups, function(filterGroup) {
				filterGroup.calculateTotals(self.data);
			});
		},
		/**
		 * TODO
		 */
		calculateInactiveTotals : function() {

			// N.B. this should be called AFTER new data value has been set,
			// as in this.data = models; see filers.update()

			var self = this;
			var currentModelCount = (this.data) ? this.data.length : 0;

			// determine "what if we selected this" product count for inactive filters
			var activeFilters = this.getActiveFilters();
			var inactiveFilters = this.getInactiveFilters();
			var activeFiltersLength = activeFilters.length;

			_.each(inactiveFilters, function(inactiveFilter) {

				inactiveFilter.hiddenCount = 0;

				// Add the current inactive filter to the current activeFilters;
				// we are trying to determine how many products we would see
				// if this inactive filter was applied.
				activeFilters[activeFiltersLength] = inactiveFilter;

				// find # of products returned when (current filters + single inactive filter) are applied
				var countWithInactive = 0;

				_.each(self.unfilteredData, function(model) {

					var modelInFilters = true;

					_.each(activeFilters, function(filter, key) {

						// For non-exclusive filterGroup filters (i.e. filters that add products as more
						// values in the group are selected), we only want to apply the inactive filter.
						// The inactive filter holds both the groupValue (all active values for the group)
						// and the inactive filter's value. Other filters for the filterGroup will not have
						// the inactive value, so will filter out potential matches for our inactive filter.
						if (inactiveFilter.filterGroupId != filter.filterGroupId || filter.id == inactiveFilter.id) {
							if (filter.exclusive == true) {
								if (!filter.filterExclusive(model)) {
									modelInFilters = false;
									// TODO | can make this more efficient
									return;
								}
							} else {
								if (!filter.filter(model)) {
									modelInFilters = false;
									// TODO | can make this more efficient
									return;
								}
							}
						}
					});
					if (modelInFilters) {
						countWithInactive++;
					}
				});

				if (countWithInactive > 0 && countWithInactive > currentModelCount) {
					inactiveFilter.hiddenCount = (countWithInactive - currentModelCount);
				}

				// restore activeFilter count
				// TODO | this doesn't seem safe...
				activeFilters[activeFiltersLength] = null;

			});

		},
		/**
		 * 
		 * @param models
		 */
		update : function(filteredModels, unfilteredModels) {

			var self = this;

			// Getting new data so we are reseting ourselves
			// TODO: WE could update this to be more efficient if we maintain
			// what adds up to the totals
			this.data = filteredModels;
			this.unfilteredData = unfilteredModels;

			// determine counts e.g. "(+1)" for inactive filters
			this.calculateInactiveTotals();

			// reset and calculate totals
			_.each(this.filterGroups, function(filterGroup) {
				// Now for each filter
				_.each(filterGroup.filters, function(filter) {
					filter.resetTotals();

					// TODO | can I make this better?
					if (filter.id == "hideFilter") {
						filter.setHiddenModels(self.unfilteredData);
					}
				});
			});

			this.calculateTotals();
		},
		/**
		 * This method is used to handle filters from the outside. Right now this was built mainly to support the hide/show filter but can easily get expanded for all types.
		 * 
		 * @param event
		 * @param type
		 */
		trigger : function(event, type) {
			var filterGroup = this.getFilterGroup(event);
			filterGroup.trigger(event, type);
		},
		/**
		 * This method will handle resetting all filters
		 */
		reset : function() {
			_.each(this.filterGroups, function(filterGroup) {
				filterGroup.reset();
			});
		},
		/**
		 * This is used to clear the current filters, this is a soft reset. If you want to force reset, use the reset method
		 */
		clear : function() {
			_.each(this.filterGroups, function(filterGroup) {
				filterGroup.clear();
			});
		},
		/**
		 * This method will allow you to add a filter
		 * 
		 * @param configuration
		 */
		// by rsingh for user stories 228 & 230
		add : function(id, configuration, coverageType) {
			var filterGroup = new FilterGroup(id, configuration, coverageType);
			this.filterGroups[filterGroup.id] = filterGroup;
		}
	};

	/**
	 * This object is used to hold the definition of a given filter.
	 * 
	 * Since we know based on configuration, all of the filters we have we can create them right away.
	 * 
	 * TODO: Might want to break the rendering stuff out as well
	 * 
			 * TODO: We should also break out the event stuff so that these methods don't rely on the event object although that might make it more difficult because we might need to check if it is
			 * selected or not
	 */
	// by rsingh for user stories 228 & 230
	var FilterGroup = function(id, configuration, coverageType) {
		var self = this;
		// Important to create a new one here cause we might
		// modify
		this.configuration = _.extend({
			resetOnClear : true
		}, configuration);
		// Right now this is basically adding as a property some functions to
		// handle getting information about the filter based on the filer control

		// OK right now we are assuming there will be a type, if there is now, we are
		// assuming an override
		if (this.configuration.view.control.type) {
			_.extend(this, controllers[this.configuration.view.control.type]);
		} else {
			// otherwise we are going to say we have an override
			_.extend(this, this.configuration.view.control);
		}

		// internal id
		this.id = id;
		// by rsingh for user stories 228 & 230
		this.coverageType = coverageType;
		// Stores all filters for this definition
		this.filters = {};
		// Fast access to filter option settings... TODO: This might go away, if I pull data from filter instead
		// this.options = {};
		// create unique id for each option
		_.each(this.configuration.filters, function(option, index) {
			var type = self.configuration.type;
			var filterType = 'filter';

			if (option.hasOwnProperty('type')) {
				type = option.type;
			}

			if (option.hasOwnProperty('filterType')) {
				filterType = option.filterType;
			}

					var filter = createFilter(filterType, {
						label : option.label,
						type : type,
						value : option.value,
						property : self.configuration.property,
						'default' : option['default'],
						helpText : option.helpText,
						helpTextType : option.helpTextType
					});

			if (option['default']) {
				filter.active = true;
			}

			// Store the filter and some other
			// data around that filter
			self.filters[filter.id] = filter;
		});
	};

	FilterGroup.prototype = {
		/**
		 * 
		 * @param append -
		 *            if false, template will be returned
		 */
		render : function(append, element) {
			var retVal = '';
			// Loop through all of our filters for this group
			// add them to our options list, also check to see
			// if we should render
			var options = [];
			var render = false;
			_.each(this.filters, function(filter, id) {
				if (filter.hiddenCount > 0 || filter.total > 0) {
					render = true;
				}
				options.push(filter);
			});

	                if (!this.initRenderSet) {
				this.initRender = render;
				this.initRenderSet = true;
			}
                        // by rsingh for user stories 228 & 230
			if (this.initRender) {

				retVal = templateEngine.render(this.getTemplate(), {
					options : options,
					definitionId : this.id,
					attributes : this.configuration.view.attributes,
					coverageType : this.coverageType
				});

				if (append) {
					$(element).append(retVal);
					this.bind($(element));
				}
			}

			return retVal;
		},
		getTemplate : function() {
			return templates[this.configuration.view.control.type];
		},
		filter : function() {

		},
		/**
		 * This will export all necessary events for this definition
		 */
		getEvents : function() {
			var retVal = [];
			retVal.push(this.getEvent());
			return retVal;
		},
		/**
		 * Give an event (toggle or changing a filter) we will build out a filter object that can be used to conduct the filter on a list of models
		 * 
		 * @param event -
		 */
		getFilter : function(event) {
			var self = this;
			var filterId = this.getFilterId(event);
			return self.filters[filterId];
		},
		calculateTotals : function(models) {
			var self = this;
			_.each(models.models, function(data) {
				// Now for each filter
				_.each(self.filters, function(filter) {
					filter.calculateTotals(data);
				});
			});
		},
		trigger : function(event, type) {
			this.processActiveFilters(event);
		},
		reset : function() {
					this.initRenderSet = false;
			_.each(this.filters, function(filter) {
				filter.reset();
			});
		},
		clear : function() {
			if (this.configuration.resetOnClear) {
				this.reset();
			}
		}
	};

	var ToggleGroup = function() {
		FilterGroup.call(this, 'hideFilterGroup', {
			type : 'hide',
			property : 'id',
			view : {
				control : {
					type : 'hidden'
				},
				attributes : {
					label : 'Hidden Plans',
				}
			},
			filters : [ {
				label : 'Hidden',
				value : [],
				filterType : 'hide'
			} ]
		});
	};

	ToggleGroup.prototype = Object.create(FilterGroup.prototype);

	ToggleGroup.prototype.calculateTotals = function(models) {
		var self = this;
		_.each(self.filters, function(filter) {
			filter.calculateTotals(models);
		});
	};

	ToggleGroup.prototype.trigger = function(event, hide) {

		// If hide is true, add one to the total
		var hideFilter = this.filters['hideFilter'];
		var total = hideFilter.total;

		if (hide) {
			hideFilter.total = total + 1;
		} else {
			if (total > 0) {
				hideFilter.total = total - 1;
			}
		}

		if (hideFilter.total === 0 && hideFilter.active) {
			hideFilter.active = false;
		} else if (hideFilter.total > 0) {
			hideFilter.active = true;
		}
	};

			// TODO: This is overwritten because of the initRenderSet logic that
			// was added is breaking this.
			ToggleGroup.prototype.render = function(append, element) {
				var retVal = '';
				// Loop through all of our filters for this group
				// add them to our options list, also check to see
				// if we should render
				var options = [];
				var render = false;
				_.each(this.filters, function(filter, id) {
					if (filter.hiddenCount > 0 || filter.total > 0) {
						render = true;
					}
					options.push(filter);
				});

				retVal = templateEngine.render(this.getTemplate(), {
					options : options,
					definitionId : this.id,
					attributes : this.configuration.view.attributes
				});

				if (append) {
					$(element).append(retVal);
					this.bind($(element));
				}

				return retVal;
			};

	/**
	 * This will contain the logic of actually doing the filter
	 */
	var Filter = function(options) {
		_.extend(this, options);
		if (_.isArray(this.value)) {
			_.each(this.value, function(value) {
				if (_.isString(value)) {
					value = value.toLowerCase();
				}
			});
		} else {
			if (_.isString(this.value)) {
				this.value = this.value.toLowerCase();
			}
		}
		// Create a unique id for this filter
		this.id = getUUID();
		this.total = 0;
		this.hiddenCount = 0;
		this.active = false;
		this.display = true;
	};

	Filter.prototype = {
		/**
		 * Given a collection object, this will filter out the ones not needed
		 * 
		 * @param models
		 * @return boolean - whether to filter or not
		 */
		filter : function(model) {
			var self = this;
			var retVal = false;

			// get property value
			var propertyValue = model.get(this.property);
			if (!propertyValue) {
				// NOTE: in Shopping, filters are cleared when changing
				// product lines, so this shouldn't cause issues across product lines.
				return false;
			}

			if (!this.active) {
				// for getting product counts for INACTIVE filters
				// make a new array!
				var finalArray = new Array();

				if (this.groupValue) {
					for ( var i = 0; i < this.groupValue.length; i++) {
						finalArray[finalArray.length] = this.groupValue[i];
					}
				}

				if (_.isArray(this.value)) {
					for ( var i = 0; i < this.value.length; i++) {
						var newObj = {};
						newObj.type = this.type;
						newObj.value = this.value[i];
						finalArray[finalArray.length] = newObj;
					}
				} else {
					var newObj = {};
					newObj.type = this.type;
					newObj.value = this.value;
					finalArray[finalArray.length] = newObj;
				}

				_.each(finalArray, function(groupValueObj) {
					retVal = retVal || self.evaluateFilter(groupValueObj.type, groupValueObj.value, propertyValue);
				});
			} else {
				// for running ACTIVE filters
				// for groupValues, we need to do an OR check
				_.each(this.groupValue, function(groupValueObj) {
					retVal = retVal || self.evaluateFilter(groupValueObj.type, groupValueObj.value, propertyValue);
				});
			}

			return retVal;
		},
		filterExclusive : function(model) {
			// get property value, if it does not exist, well then return
			// false right away.
			var propertyValue = model.get(this.property);
			if (!propertyValue) {
				return false;
			}

			return this.evaluateFilter(this.type, this.value, propertyValue);
		},
		evaluateFilter : function(filterType, filterValue, modelFilterValue) {

			var retVal = false;

			switch (filterType) {
			case "=":
				var filters = _.isArray(filterValue) ? filterValue : [ filterValue ];
				retVal = (_.include(filters, modelFilterValue.toLowerCase()) || _.include(filters, modelFilterValue));
				break;
			case ">":
				retVal = parseFloat(modelFilterValue) > parseInt(filterValue);
				break;
			case "<":
				retVal = parseFloat(modelFilterValue) <= parseInt(filterValue);
				break;
			case "between":
				var limits = filterValue.split('|');
				retVal = (parseFloat(modelFilterValue) > parseFloat(limits[0])) && (parseFloat(modelFilterValue) <= parseFloat(limits[1]));
				break;
			}

			return retVal;
		},
		includeTotal : function(model) {
			return this.filterExclusive(model);
		},
		calculateTotals : function(data) {
			if (this.includeTotal(data)) {
				this.total = this.total + 1;
			}
		},
		resetTotals : function() {
			this.total = 0;
		},
		reset : function() {
			this.active = false;
			this.total = 0;

			if (this['default']) {
				this.active = true;
			}
		}
	};

	// Maybe hidden uses the values and it stores them there?
	var HideFilter = function(options) {
		Filter.call(this, options);
		this.id = "hideFilter";
		this.active = false;
		this.display = true;
		this.hiddenProducts = new Array();
	};

	HideFilter.prototype = Object.create(Filter.prototype);

	HideFilter.prototype.filter = function(model) {

		// the filter dictates what is SHOWN when the filter is active.
		// so, anything (hide == true) should not be shown... can you dig it?

		if (typeof model.get('hide') === 'undefined') {
			return true;
		} else {
			return !_.include([ true ], model.get('hide'));
		}
	};

	HideFilter.prototype.resetTotals = function() {
		this.hiddenProducts = [];
	};

	HideFilter.prototype.setHiddenModels = function(models) {
		var self = this;
		_.each(models, function(model) {
			if (_.include([ true ], model.get('hide'))) {
				self.hiddenProducts[self.hiddenProducts.length] = model;
			}
		});
	};

	HideFilter.prototype.calculateTotals = function(models) {
		// var length = _.isArray(this.value) ? this.value.length : 1;
		// this.total = length;
	};

	return Filters;
});
