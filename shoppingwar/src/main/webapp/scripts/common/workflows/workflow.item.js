/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to become the base Widget file. We will slowly move stuff into here from the existing widget.question.js
 * 
 * TODO: We need to refactor this code so there is one piece of code that renders the current active widgets and handles the layouts. As of now it is in two places.
 * 
 */
define([ 'require', 'backbone', 'common/application/eventmanager', 'common/workflows/view.workflow.item', 'common/widget.factory', 'common/application/application.manager.region',
		'common/workflows/view.workflow.item.widget.progress', 'common/workflows/view.workflow.item.progress', 'common/workflows/view.workflow.item.actions' ], function(require, Backbone,
		EventManager, WorkflowItemView, WidgetFactory, RegionManager, WorkflowItemWidgetProgressView, WorkflowItemProgressView, WorkflowItemActionsView) {
	/**
	 * Configuration is usually configuration from an external source, widgets is the list of all possible widgets and options are the internal configuration from the code. :)
	 */
	var WorkflowItem = function(configuration, widgets, options) {
		this.configuration = configuration;

		if (!this.configuration.get('regions')) {
			// if we don't have configured, regions, use
			// the defaults
			this.configuration.set({
				regions : {
					'content' : {
						el : '._workflowContent',
						primary : true,
						widgetRegion : true
					},
					'actions' : {
						el : '._workflowActions'
					},
					'widgetProgress' : {
						el : '._workflowWidgetProgress'
					},
					'workflowProgress' : {
						el : '._workflowProgress'
					}
				}
			});
		}

		// should I tie this to regions, in most cases if you are
		// overriding default regions you probably have a layouts
		if (!this.configuration.get('layouts')) {
			this.configuration.set({
				layouts : {}
			});
		}

		// filter the widgets done to the ones that only belong to this work flow item
		this.widgets = widgets.getFilteredWidgets(configuration.get('widgets'));

		// set workflow-level configuration on the individual widgets
		this._configureWidgetsForWorkflow(this.widgets);

		this.allWidgets = widgets;

		this.options = options || {};

		// regions can be internall configured, usually from a extending
		// constructor, otherwise use the defaults. This will only apply
		// if a workflow item has
		this.options = _.extend({
			parameters : {},
			active : {},
			view : {}
		}, options);

		// this is for the workflow event manager
		this.eventManager = new EventManager();

		// we need to convert our configured regions to
		// something the region manager can handle
		var regions = {};
		_.each(this.configuration.get('regions'), function(region, key) {
			regions[key] = region.el;
		});

		this.regionManager = new RegionManager({
			el : this.options.view.el,
			regions : regions
		});
		this.initialize();
	};

	WorkflowItem.prototype = {
		// using this method so that it follows closer to backbone :)
		initialize : function() {
			var self = this;

			// setup events on this badboy, these will get reaped when the overall view gets replaced
			// should these bubble up to the workflow.item object?
			this.eventManager.on('view:workflow:previous:widget', this.workflowPrevWidget, this);
			this.eventManager.on('view:workflow:next:widget', this.workflowNextQuestion, this);
			this.eventManager.on('view:workflow:widget:clear', this.workflowClearQuestion, this);
			this.eventManager.on('exit', function() {
				var args = Array.prototype.slice.call(arguments);
				args.unshift('exit');
				self.options.eventManager.trigger.apply(self.options.eventManager, args);
			});

			// TODO: We need configuration to determine what to do when a widget is done
			this.eventManager.on('widget:done', function(stop) {
				if (stop) {
					this.exit('exit');
				} else {
					this.exit('next');
				}

			}, this);

			/**
			 * This event can be trigger by widgets when they are rendering internally.
			 */
			this.eventManager.on('widget:render:internal', function() {
				this.renderActions();
			}, this);

			/**
			 * I think at this point we know there is a next workflow
			 */
			this.eventManager.on('workflow:item:next:workflow', function(workflowId, widgetId) {
				this.setWorkflowStep(workflowId);
				this.activeWorkflowId = workflowId;
				// Is this safe???? I should probably rename this method
				this.start({
					widget : widgetId,
					fromRoute : false,
					trigger : true
				});

			}, this);

			this.eventManager.on('workflow:item:sub:render:widget', function(widgetId, data) {
				this.eventManager.trigger('widget:takeover', widgetId);
				this.renderWidgetProgress();
			}, this);
			/**
			 * A widget will call this event if it wants to take control of the primary view
			 * 
			 * Lookup the primary region and then take the given widgetId, look that up, then render that to the primary region. We will also want to change the 'activeWidget' property so the active
			 * widget is always up to date.
			 * 
			 * 
			 */
			this.eventManager.on('widget:takeover', function(widgetId, data) {
				var self = this;
				// we first need to check to see if we allow multiple widgets
				// to render at once, we have to handle different scenarios
				// depending on if we do or not
				// for tracking / analytics
				// using external event to let the outside world know
				this.options.eventManager.trigger('workflow:item:started', widgetId);
				
				if (!this.configuration.get('allowRenderMultipleWidgets')) {
					// At this point we do not allow multiple widgets to be
					// rendered at once.

					// Some cases we might have one widget per work flow item but
					// it contains multiple views. To handle that, we need to
					// check and see if the primary widget is equal to
					// the one coming in. If it is not the same, then delete
					// the primary and create this one
					if (this.primaryWidget.model.get('widgetId') !== widgetId) {
						// since we only allow one, destroy the currently active widget
						// clear out the array
						this.primaryWidget.destroy();
						this.activeWidgets = [];
						// now start up the new one
						var widget = this.startWidget(this.widgets.getWidget(widgetId));
						this._renderPrimaryWidget(widget);
					} else {
						// Otherwise, just grab the new view and render it to the primary region
						this.regionManager[this._primaryRegion()].show(this.primaryWidget.getView());
					}
				} else {

					var oldActiveWidget = self.primaryWidget;
					var newActiveWidget = self.getWidgetById(widgetId);
					//
					// there might be cases where the widget we are trying
					// to render now might not have been activated
					if (newActiveWidget === null) {
						newActiveWidget = this.startWidget(this.widgets.getWidget(widgetId));
						this.activeWidgets.push(newActiveWidget);
					}

					self.primaryWidget = newActiveWidget;

					oldActiveWidget.setPrimary(false);
					newActiveWidget.setPrimary(true);

					if (newActiveWidget.canRender()) {
						self.regionManager.content.show(newActiveWidget.getView(data));
					}

					var layout = self.configuration.get('layouts')[widgetId] ? self.configuration.get('layouts')[widgetId] : self.configuration.get('layouts')['default'];

					// we need to loop through all of the active widget and render
					// them appropriately. We don't want to render the active widget here
					// and we also need to make sure we close any regions we are not using
					if (_.size(this.activeWidgets) > 1) {
						// loop through each layout region first
						_.each(layout, function(layoutWidgetId, region) {
							// then we need to find the active widget that
							// belongs to this region
							var widget = null;

							_.each(self.activeWidgets, function(activeWidget) {
								// match em up
								if (layoutWidgetId === activeWidget.model.get('widgetId')) {
									widget = activeWidget;
								}
							});

							// try to find the widget in workflow's widget collection if null
							if (widget === null) {

								_.each(self.widgets.models, function(workflowWidget) {
									if (workflowWidget.get("widgetId") == layoutWidgetId) {
										widget = self.startWidget(self.widgets.getWidget(layoutWidgetId));
										self.activeWidgets.push(widget);
									}
								});

								// if nothing was found then close it
								if (widget === null) {
									self.regionManager[region].close();
									return;
								}
							}
							// if it is primary ignore
							if (!widget.isPrimary) {
								self.regionManager[region].show(widget.getView(data));
							}
						});
					}
				}

				if (typeof triggerHistory == 'undefined') {
					triggerHistory = true;
				}

				if (triggerHistory) {
					var componentId = self.options.history.id;
					var workflowId = self.configuration.get("id");
					var widgetId = this.primaryWidget.historyKey();
					Backbone.history.navigate('view/' + componentId + '/' + workflowId + '/' + widgetId);
				}

				// using external event to let the outside world know
				this.options.eventManager.trigger('workflow:item:finished');

			}, this);

			/**
			 * This would be another generic event that a widget could call to reset the workflow view back to default views.
			 */
			this.eventManager.on('widget:reset', function() {

			});

		},
		/**
		 * options will include workflowId, widgetId
		 * 
		 * TODO: This method needs to be broken out and better organized
		 */
		start : function(options) {
			var self = this;

			options = options || {
				fromRoute : false,
				trigger : true
			};

			var activeWidgetId = (options.widget) ? options.widget : null;
			// this.activeWorkflowId = (options.workflowId) ? options.workflowId : null;

			// if we don't have one, check to see if we do need one
			if (!this.activeWorkflowId) {
				// kind of a hack for now but containing this here
				if (!_.isEmpty(this.configuration.get('workflows'))) {
					// if there is an active widgetId, we need to find the workflow it belongs to
					// otherwise use the first one
					if (activeWidgetId) {
						// find out which workflow this widget belongs to
						var found = false;
						_.each(this.configuration.get('workflows'), function(subworkflow) {
							_.each(subworkflow.widgets, function(workflowWidget) {
								if (!found && workflowWidget === activeWidgetId) {
									self.activeWorkflowId = subworkflow.id;
								}
							});
						});

					} else {
						this.activeWorkflowId = this.configuration.get('workflows')[0].config.questionnaireRefId;
					}
				}
			}

			// do further filtering, specialized workflow items can implement this further
			this.filter();

			// in case we are calling this method again, destroy any currently
			// active widgets
			this._destroyActiveWidgets();
			// this is an array of all widgets that will be built and active during the life time of
			// this work flow item
			this.activeWidgets = [];
			// quick reference to the primary widget, as of now, there always
			// has to be a primary widget
			this.primaryWidget = null;
			// quick reference collections of all widgets we have to build and render
			var widgetsToRender = new Backbone.Collection();

			// if we have an active widget id (i.e. we are being told to render this widget as the primary) and
			// we cannot render multiple widgets for this work flow item then just add one
			if (activeWidgetId && !this.configuration.get('allowRenderMultipleWidgets')) {
				widgetsToRender.add(this.widgets.getWidget(activeWidgetId));
			} else {
				// otherwise, we need to determine if there are multiple widgets to render
				// and if we allow it
				if (_.size(this.widgets) > 1 && this.configuration.get('allowRenderMultipleWidgets')) {
					// push them all
					widgetsToRender.add(this.widgets.models);
				} else {
					// otherwwise just grab the first one
					widgetsToRender.add(this.widgets.models[0]);
				}
			}

			// for tracking / analytics
			// using external event to let the outside world know
			this.options.eventManager.trigger('workflow:item:started', widgetsToRender.first().get("widgetId"));

			// If we are only rendering one widget then
			// make it the primary and render to the primary
			// region
			if (_.size(widgetsToRender) === 1) {
				var widget = this.startWidget(widgetsToRender.first(), options);
				this._renderPrimaryWidget(widget);
			} else {
				// This is where it gets a bit tricky, since we have
				// multiple and we also need to look to see if we are
				// suppose to render a specific one. If we are supposed
				// to render a specific one we need to know what that
				// actually means.

				// we need to figure out if there is a layout associated with
				// this widget route
				if (activeWidgetId && self.configuration.get('layouts')[activeWidgetId]) {
					this._processLayout(widgetsToRender, self.configuration.get('layouts')[activeWidgetId]);
				} else {
					// this means just run the default, default must exist
					this._processLayout(widgetsToRender, self.configuration.get('layouts')['default']);
				}
			}

			// 1. Check to see if configuration allows the workflow item to trigger
			// 2. Make sure we are not coming from a route already
			// 3. We need to also make sure the incoming options say we can trigger (if it is the first
			// workflow item, we don't want to trigger history)
			if (this.configuration.get('history').trigger && !options.fromRoute && options.trigger) {
				var route = 'view/' + this.options.history.id + '/';

				if (_.isString(this.configuration.get('history').root) && this.configuration.get('history').root !== '') {
					route = route + this.configuration.get('history').root + '/';
				}

				route = route + self.configuration.get("id") + '/';

				// use the primary widget as the route
				route = route + this.primaryWidget.historyKey();

				if (this.options.parameters.currentMemberRefName) {
					route = route + '/' + this.options.parameters.currentMemberRefName;
				}

				Backbone.history.navigate(route);
			}
			// render widget progress if we can
			this.renderWidgetProgress();

			//
			this.renderProgress();

			this.renderActions();

			// using external event to let the outside world know
			this.options.eventManager.trigger('workflow:item:finished');
		},
		filter : function() {

		},
		renderProgress : function() {
			if (this.regionManager.workflowProgress) {
				if (this.configuration.get('showNavigation')) {
					this.regionManager.workflowProgress.show(new WorkflowItemProgressView({
						workflows : this.options.models.workflowItems,
						account : this.options.models.account,
						session : this.options.session,
						eventManager : this.eventManager,
						componentId : this.options.history.id
					}));

				} else {
					this.regionManager.workflowProgress.close();
				}
			}
		},
		renderWidgetProgress : function() {
			// This will render the widget progress of the active sub workflow
			if (this.regionManager.widgetProgress) {
				// Render the progress view and the navigation
				// these views will be able to determine if they can be rendered.
				var progressView = new WorkflowItemWidgetProgressView({
					model : this.primaryWidget.configModel(),
					collection : this.widgets,
					eventManager : this.eventManager
				});

				this.regionManager.widgetProgress.show(progressView);
			}
		},
		renderActions : function() {
			if (this.regionManager.actions && this.configuration.get('showActions')) {
				var actionView = new WorkflowItemActionsView({
					eventManager : this.eventManager,
					collection : this.widgets,
					// TODO: figure out how to handle this with the navigation
					model : this.primaryWidget.configModel(),
					isLastWidget : this.isLastWorkflowWidget()
				});

				this.regionManager.actions.show(actionView);
			}
		},
		getWidgetById : function(widgetId) {
			var targetWidget = null;

			_.each(this.activeWidgets, function(checkWidget) {
				if (widgetId === checkWidget.model.get("widgetId")) {
					targetWidget = checkWidget;
				}
			});

			return targetWidget;
		},
		startWidget : function(widgetModel, route) {
			var widgetFactory = new WidgetFactory();
			// we have to use
			var widget = widgetFactory.getWidget(widgetModel.get('widgetType'), {
				"widgetModel" : widgetModel,
				"attributes" : this.buildWidgetData(),
				// pass in private event manager
				"eventManager" : this.eventManager,
				'route' : route
			});

			widget.start();

			return widget;
		},
		/**
		 * This will have to return the view to render, which for now will be the WorkflowItemView which will render the appropriate widget
		 */
		render : function(options) {
			return new WorkflowItemView();
		},
		/**
		 * This method should return the necessary data to build the widget
		 */
		buildWidgetData : function() {
			return {
				applicationConfig : this.options.models.applicationConfig,
				session : this.options.session,
				account : this.options.models.account,
				actor : this.options.models.actor,
				parameters : this.options.parameters,
				componentId : this.options.history.id
			};
		},
		canMovePrev : function() {
			return true;
		},
		canMoveNext : function() {
			return true;
		},
		canSkip : function() {
			return true;
		},
		/**
		 * This function will either return a boolean or an object.
		 * 
		 * In some cases if we cannot render we need to return a direction or action, either previous, next, exit
		 * 
		 * @returns {Boolean}
		 */
		canRender : function() {
			return true;
		},
		destroy : function() {
			this.eventManager.off();
			this._destroyActiveWidgets();
			this.regionManager.close();
		},
		/**
		 * This method gets called when the workflow item is done and ready to exit
		 * 
		 * @param id
		 * @param position
		 */
		exit : function(position) {
			// before we can exit, we need to make sure we can, to do that we need to
			// check all currently active widgets and ask them if we can continue
			var proceed = true;
			// eventually this could be an array but right now we only support one
			// failed widget, bummer man, update as this is needed
			var failedWidget = null;
			if (this.activeWidgets) {
				_.each(this.activeWidgets, function(widget) {
					// ask each widget if it is complete
					if (!widget.isComplete()) {
						proceed = false;
						failedWidget = widget;
					}
				});
			}

			if (proceed) {
				// default, trigger an event
				this.options.eventManager.trigger('workflow:item:done', this.configuration.get('id'), position);
			} else {
				// umm this?
				this.start({
					widget : failedWidget.model.get('widgetId')
				});
			}

		},
		workflowPrevWidget : function(event) {
			var canMovePrev = true;
			// loop through all of our widgets to
			// make sure we can proceed with all of them
			_.each(this.activeWidgets, function(widget) {
				if (!widget.canMovePrev()) {
					canMovePrev = false;
				}
			});

			if (!canMovePrev) {
				return;
			}

			var prevQuestionRefNumber = null;
			// using the primary for now to determine next
			var indexOfCurrentWidget = this.widgets.indexOf(this.primaryWidget.configModel());

			// if we still have some to render inside this sub work flow
			// then render that one
			if (indexOfCurrentWidget > 0) {
				prevQuestionRefNumber = this.widgets.models[indexOfCurrentWidget - 1].get('widgetId');
				this.eventManager.trigger('workflow:item:sub:render:widget', prevQuestionRefNumber);
			} else {
				// here we need to check if we have other workflows
				var workflow = this.configuration.getPreviousWorkflow(this.activeWorkflowId);

				if (workflow) {
					// otherwise we are at the last question in the current workflow, so we need to render the previous workflow
					var widgetId = _.last(workflow.widgets);
					this.eventManager.trigger('workflow:item:next:workflow', workflow.id, widgetId);
				} else {
					// if this is empty, then we are done with this workflow item
					this.exit('previous');
				}
			}

		},
		workflowNextQuestion : function(event) {
			var canMovePrev = true;
			// loop through all of our widgets to
			// make sure we can proceed with all of them
			_.each(this.activeWidgets, function(widget) {
				if (!widget.canMoveNext()) {
					canMovePrev = false;
				}
			});

			if (!canMovePrev) {
				return;
			}

			var nextQuestionRefNumber = null;

			var indexOfCurrentWidget = this.widgets.indexOf(this.primaryWidget.configModel());

			if (indexOfCurrentWidget < (this.widgets.size() - 1)) {
				nextQuestionRefNumber = this.widgets.models[indexOfCurrentWidget + 1].get('widgetId');
				this.eventManager.trigger('workflow:item:sub:render:widget', nextQuestionRefNumber);
			} else {
				// this is means we are the last one in the current sub-workflow for this item
				var workflow = this.configuration.getNextWorkflow(this.activeWorkflowId);
				if (workflow) {
					// otherwise we are at the last question in the current workflow, so we need to render the previous workflow
					// var widgetId = _.first(workflow.widgets);
					this.eventManager.trigger('workflow:item:next:workflow', workflow.id);
				} else {
					// if this is empty, then we are done with this workflow item
					this.exit('next');
				}
			}
		},
		workflowSkipQuestion : function(event) {
			this.workflowNextQuestion(event, true);
		},
		workflowClearQuestion : function(event) {
			_.each(this.activeWidgets, function(widget) {
				widget.clearCurrentQuestion();
			});
		},
		isLastWorkflowWidget : function() {
			// first need to figure out if this is the last workflow
			// then the last widget inside of that

			if (!this.configuration.isLastWorkflow(this.activeWorkflowId)) {
				return false;
			}

			// then check to see if the primary widget is the last one
			if (this.widgets.indexOf(this.primaryWidget.configModel()) !== (this.widgets.size() - 1)) {
				return false;
			}

			// if everything is true so far then we need to ask the widget itself
			// if we are on the last view, some widgets might have multiple views/workflows
			// internally

			return this.primaryWidget.state('end');
		},
		/**
		 * This is copied from the old stuff, probably needs to get cleaned up
		 * 
		 * @param workflowStep
		 */
		setWorkflowStep : function(workflowStep) {
			this.options.session.set("currentWorkflowStep", workflowStep);

			if (!this.options.session.get("enabledWorkflowSteps")) {
				this.options.session.set("enabledWorkflowSteps", []);
			}

			var enabledWorkflowSteps = this.options.session.get("enabledWorkflowSteps");

			var alreadyEnabled = false;
			for ( var wfs = 0; wfs < enabledWorkflowSteps.length && !alreadyEnabled; wfs++) {
				if (enabledWorkflowSteps[wfs] == workflowStep) {
					alreadyEnabled = true;
				}
			}
			if (!alreadyEnabled) {
				enabledWorkflowSteps.push(workflowStep);
			}
		},
		_configureWidgetsForWorkflow : function(widgets) {
			var self = this;
			widgets.each(function(widget) {
				_.each(self.configuration.get('widgets'), function(workflowWidgetConfig) {
					if (workflowWidgetConfig.id === widget.get('widgetId')) {
						// set each property in the configuration on its corresponding widget
						for ( var propName in workflowWidgetConfig) {
							if (propName != "id") {
								if (workflowWidgetConfig.hasOwnProperty(propName)) {
									var attribute = {};
									attribute[propName] = workflowWidgetConfig[propName];
									widget.set(attribute);
								}
							}
						}
					}
				});
			});
		},
		_renderPrimaryWidget : function(widget) {
			this.activeWidgets.push(widget);
			this.primaryWidget = widget;
			this.primaryWidget.setPrimary(true);
			// just render this directly to the content, only one
			this.regionManager[this._primaryRegion()].show(this.primaryWidget.getView());
		},
		/**
		 * This method will find the primary region key
		 * 
		 * @returns
		 */
		_primaryRegion : function() {
			var retVal = null;
			_.each(this.configuration.get('regions'), function(region, key) {
				if (region.widgetRegion && region.primary) {
					retVal = key;
				}
			});

			return retVal;
		},
		/**
		 * This method will make sure that this widget can be found in the current active workflow, if it can't it will switch it to a workflow that will work
		 * 
		 * @param widgetId
		 */
		_verifyWorfklow : function(widgetId) {
			if (this.activeWorkflowId) {
				// trying something new here :)
				var workflows = new Backbone.Collection(this.configuration.get('workflows'));

				// get current
				var activeWorkflow = workflows.get(this.activeWorkflowId);

				var widgets = new Backbone.Collection(activeWorkflow.get('widgets'));

				if (!widgets.contains(widgetId)) {
					this._setWorkflow(widgetId);
				}
			}
		},
		/**
		 * This method will set the workflow given a widget id
		 * 
		 * @param widgetId
		 */
		_setWorkflow : function(widgetId) {
			var self = this;
			var workflows = this.configuration.get('workflows');

			var found = false;
			_.each(workflows, function(subworkflow) {
				_.each(subworkflow.widgets, function(workflowWidget) {
					if (!found && workflowWidget === widgetId) {
						self.activeWorkflowId = subworkflow.id;
					}
				});
			});
		},
		_destroyActiveWidgets : function() {
			if (this.activeWidgets) {
				_.each(this.activeWidgets, function(widget) {
					widget.destroy();
				});

				this.activeWidgets = [];
			}
		},
		/**
		 * Processes a layout on startup.
		 * 
		 * @param widgetsToRender
		 * @param layout
		 */
		_processLayout : function(widgetsToRender, layout) {
			var self = this;
			// if there is no layout, then use the standard, determine
			// what is primary and where to render it to
			if (!layout) {
				// if we have multiple, we need to loop over the configs, start each one
				// and then figure out where to render it.
				widgetsToRender.each(function(widgetConfig) {
					// create and start the widget
					var widget = self.startWidget(widgetConfig);
					self.activeWidgets.push(widget);

					// determine where to render this widget
					var workflowWidgetConfig = null;
					_.each(self.configuration.get('widgets'), function(workflowWidget) {
						if (workflowWidget.id === widgetConfig.get('widgetId')) {
							workflowWidgetConfig = workflowWidget;
						}
					});

					// needs a default region, render it to that region
					if (workflowWidgetConfig && workflowWidgetConfig.defaultRegion) {

						// want to set primary BEFORE we render any views; primary status
						// may dictate how the view renders
						// if we already have a primary widget set, do not set
						if (self.primaryWidget === null && self.configuration.get('regions')[workflowWidgetConfig.defaultRegion].primary) {
							self.primaryWidget = widget;
							self.primaryWidget.setPrimary(true);
						} else {
							widget.setPrimary(false);
						}

						self.regionManager[workflowWidgetConfig.defaultRegion].show(widget.getView());
					}
				});
			} else {
				var primaryRegion = this._primaryRegion();
				// loop through the layouts and then look up the widget we are suppose to render
				// if the region value is false, that means we have to close it
				_.each(layout, function(widgetId, region) {
					if (widgetId) {
						widgetsToRender.each(function(widgetConfig) {
							// this will tell us that this widget is active for this layout
							// just startup what we can
							if (widgetId === widgetConfig.get('widgetId')) {
								var widget = self.startWidget(widgetConfig);
								self.activeWidgets.push(widget);

								// we need to determine if this is the primary region
								if (region === primaryRegion) {
									widget.setPrimary(true);
									self.primaryWidget = widget;
								} else {
									widget.setPrimary(false);
								}
								// render it to this region
								self.regionManager[region].show(widget.getView());
							}
						});
					} else { // if it is false, close er down
						self.regionManager[region].close();
					}
				});
			}
		}
	};

	return WorkflowItem;
});
