/**
 * Copyright (C) 2012 Connecture
 * 
 * This is going to end up being the frame from the workflow item
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/common/workflows/view.workflow.template.dust', 'dust-helpers' ], function(require, Backbone, templateEngine,
		BaseView, template) {

	templateEngine.load(template, "view.workflow.template");

	var WorkflowItemView = BaseView.extend({
		className : 'questionnaire _componentView',
		template : "view.workflow.template",
		events : {

		},
		render : function() {
			var self = this;
			// render frame
			var viewHTML = this.renderTemplate();
			$(self.el).append(viewHTML);

			return this;
		}
	});

	return WorkflowItemView;
});