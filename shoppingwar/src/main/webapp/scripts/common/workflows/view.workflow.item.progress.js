/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/common/workflows/view.workflow.item.progress.dust', 'dust-helpers' ], function(require, Backbone,
		templateEngine, BaseView, template) {
	templateEngine.load(template, "view.workflow.item.progress");

	var WorkflowProgressView = BaseView.extend({
		template : "view.workflow.item.progress",
		events : {
			"click .loadable-nav .nav-action" : "navSelected",
			"click ._skipToPlansLink" : "workflowSkipToPlans"
		},
		initialize : function(options) {
			this.workflows = options.workflows;
			this.account = options.account;
			this.session = options.session;
			this.eventManager = options.eventManager;
			this.componentId = this.options.componentId;
		},
		render : function() {
			var self = this;

			// lol this is hardcoded for just questions, whoops fix when necessary I am lazy right now.
			var workflowMenu = [];
			_.each(this.workflows.get('Questions').toJSON().workflows, function(subworkflow) {
				if (!_.isEmpty(subworkflow.widgets)) {
					workflowMenu.push({
						id : subworkflow.id,
						label : subworkflow.config.title,
						route : '#view/' + self.componentId + '/Questions/' + subworkflow.widgets[0]
					});
				}
			});

			var data = {
				"progress" : workflowMenu,
				"currentWorkflowStep" : this.session.get("currentWorkflowStep"),
				"enabledWorkflowSteps" : this.session.get("enabledWorkflowSteps"),
			};

			$(self.el).html(this.renderTemplate({
				data : data
			}));

			return this;
		},
		navSelected : function(event) {

		},
		workflowSkipToPlans : function() {
			this.options.eventManager.trigger('widget:done', 'exit');
		}
	});

	return WorkflowProgressView;

});