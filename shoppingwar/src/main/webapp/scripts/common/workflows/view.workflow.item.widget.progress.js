/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/common/workflows/view.workflow.item.widget.progress.dust', 'dust-helpers' ], function(require, Backbone,
		templateEngine, BaseView, template) {

	templateEngine.load(template, "view.workflow.item.widget.progress");

	var WorkflowItemView = BaseView.extend({
		template : "view.workflow.item.widget.progress",
		events : {
			"click .progBarList > ._question" : "questionSelected"
		},
		render : function() {
			var wigetList = this.collection.toJSON();
			var progressData = {
				currentWidgetId : this.model.get('widgetId'),
				"wigetList" : wigetList,
				// FU dust
				'renderProgressList' : wigetList.length > 1
			};

			$(this.el).html(this.renderTemplate({
				"data" : progressData
			}));

			return this;
		},
		questionSelected : function(event) {
			var $selectedQuestion = $(event.currentTarget);

			var questionRefId = $selectedQuestion.attr("data-id");
			this.options.eventManager.trigger('workflow:item:sub:render:widget', questionRefId);
		},
		workflowSkipToPlans : function() {
			this.options.eventManager.trigger('widget:done', 'exit');
		}
	});

	return WorkflowItemView;
});