/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'templateEngine', 'common/views/base', 'text!html/shopping/common/workflows/view.workflow.item.actions.dust', 'dust-helpers' ], function(require, Backbone,
		templateEngine, BaseView, template) {

	templateEngine.load(template, "view.workflow.item.actions");

	var WorkflowItemView = BaseView.extend({
		template : "view.workflow.item.actions",
		events : {
			"click .paging-section .paging-buttons ._prevQuestion a" : "workflowPrevQuestion",
			"click .paging-section .paging-buttons ._clearQuestion a" : "workflowClearQuestion",
			"click .paging-section .paging-buttons ._skipQuestion a" : "workflowSkipQuestion",
			"click .paging-section .paging-buttons ._nextQuestion a" : "workflowNextQuestion",
		},
		initialize : function(options) {
			BaseView.prototype.initialize.call(this);
			this.isLastWidget = options.isLastWidget;
		},
		render : function() {
			$(this.el).html(this.renderTemplate({
				"data" : {
					hasPrevQuestion : this.hasPrevWorkflowWidget(),
					isLastWidget : this.isLastWidget
				}
			}));

			return this;
		},
		hasPrevWorkflowWidget : function() {
			return this.isPrevWidget() || this.collection.first().get('widgetId') !== this.model.get('widgetId');
		},
		isPrevWidget : function() {
			var indexOfCurrentWidget = this.collection.indexOf(this.model);

			if (indexOfCurrentWidget <= 0) {
				return false;
			} else {
				return true;
			}
		},
		workflowPrevQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:previous:widget', event);
		},
		workflowNextQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:next:widget', event, true);
		},
		workflowSkipQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:next:widget', event);
		},
		workflowClearQuestion : function(event) {
			this.options.eventManager.trigger('view:workflow:widget:clear');
		},
	});

	return WorkflowItemView;
});