/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 */
define([ 'require', 'backbone', 'workflow/definition' ], function(require, Backbone, workflowDefinition) {

	var WorkflowFactory = function(configuration) {
		this.configuration = configuration;
	};

	WorkflowFactory.prototype = {
		getWorkflow : function(workflowType, configuration, widgets, options) {
			return workflowDefinition.createWidget(workflowType, configuration, widgets, options);
		}
	};

	return WorkflowFactory;
});
