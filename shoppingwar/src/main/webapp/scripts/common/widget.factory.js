/**
 * Copyright (C) 2012 Connecture
 * 
 * Not sure the best place for this now but going to use this as a widget factory.
 * 
 * The implementing application will pass in the definition.
 * 
 * For now, the constructor of this factory will take in the configuration map for those widgets.
 * 
 * Ideally we can figure out a better way to get that configuration in there
 * 
 */
define([ 'require', 'backbone', 'widget/definition' ], function(require, Backbone, widgetDefinition) {

	var WidgetFactory = function(configuration) {
		this.configuration = configuration;
	};

	WidgetFactory.prototype = {
		// TODO: the configuration being passed in this method id temp until we
		// get all requirements sorted out
		getWidget : function(widgetType, configuration) {
			return widgetDefinition.createWidget(widgetType, configuration);
		}
	};

	return WidgetFactory;
});
