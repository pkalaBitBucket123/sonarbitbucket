/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'jquery.url' ], function(require) {
	return {
		/**
		 * This method will process url parameters for use with making a url
		 * 
		 * @param requestParams
		 * @param context
		 * @returns
		 */
		getUrlParams : function(requestParams, context) {
			var retVal = {};
			var url = $.url.parse();
			if (url.params) {
				_.each(requestParams, function(requestedParam) {
					if (url.params.hasOwnProperty(requestedParam) || requestedParam === 'context') {
						// Context is a special case!
						// handle it here
						if (requestedParam !== 'context') {
							retVal[requestedParam] = url.params[requestedParam];
						} else {
							// filter owns that property for now so ask for it
							// I think the better end game solution is to have
							// redirects and this stuff run through filter
							retVal['context'] = context;
						}
					}
				});
			}

			return retVal;
		},
		/**
		 * This method will process url parameters for use with building an array of parameters for use with the request hub API.
		 * 
		 * @param requestedParams
		 * @returns
		 */
		getRequestParams : function(requestParams) {
			var retVal = [];
			var url = $.url.parse();
			_.each(requestParams, function(requestedParam) {
				var param = null;
				if (typeof requestedParam === 'string') {
					if (url.params && url.params.hasOwnProperty(requestedParam)) {
						param = {};
						param.key = requestedParam;
						param.value = url.params[requestedParam];
					}
				} else {
					param = {};
					param = requestedParam;
				}
				if (param) {
					retVal.push(param);
				}
			});

			return retVal;
		}
	};
});