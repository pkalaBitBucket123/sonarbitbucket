/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'underscore', 'common/application/application.region' ], function(require, _, Region) {

	var ModalRegion = function(options) {
		options.el = '#shopping-modal';
		Region.call(this, options);
		this.on("view:show", this.showModal, this);
		this.on("view:closed", this.hideModal, this);
	};

	ModalRegion.prototype = Object.create(Region.prototype);

	ModalRegion.prototype.showModal = function(view) {

		var self = this;
		this.$el.dialog({
			modal : true,
			width : 650,
			minHeight : 200,
			minWidth : 300,
			// title : headerHTML,
			close : function() {
				self.close();
			}
		});
	};

	ModalRegion.prototype.hideModal = function(view) {
		this.$el.dialog('close');
	};

	return ModalRegion;
});