/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {

	// This will pull in Backbone.proxy and attach that object to this model by default

	var ProxyModel = Backbone.Model.extend({
		storageEngine : 'Proxy',
		initialize : function(options) {
			options = _.extend({}, options);
			// TODO: Clean up the storeItem/item
			if (options.storeItem) {
				// We need to see if it have a storeItem
				this.storeItem = options.storeItem;
				// This basically merges it into one format
				this.item = _.isObject(this.storeItem) ? this.storeItem.item : this.storeItem;
			} else if (options.requestKey) {
				// make these null for now
				this.storeItem = null;
				this.item = null;

				this.requestKey = options.requestKey;
			}

		}
	});

	return ProxyModel;
});