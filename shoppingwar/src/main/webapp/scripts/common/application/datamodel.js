/**
 * Copyright (C) 2012 Connecture
 * 
 * 
 * 
 */
define([ "module", 'log', 'backbone', 'common/application/proxy.cache' ], function(module, log, Backbone, proxyCache) {

	var DataModel = function(mappings) {
		this.mappings = mappings;
		// Putting this here for now
		this.cache = proxyCache;
	};

	DataModel.prototype = {
		model : function(requestResponses) {
			var self = this;
			// At this point we are going to get a list of
			// everything back from the server, we need to
			// loop through that and look up our stuff

			// we are only going to return what was requested
			var retVal = {};
			_.each(this.mappings, function(dependency, key) {
				// If this component has a dependency, then process
				if (requestResponses[key]) {
					// Also check if this is a collection as well
					if (_.isArray(requestResponses[key])) {
						// If we do not have a specific collection for
						// this data type, then use the default
						var Collection = Backbone.Collection;
						if (dependency.Collection) {
							Collection = dependency.Collection;
						}

						// If we don't have it already then add it
						if (!self.cache[key]) {
							var collection = new Collection(requestResponses[key]);
							self.cache[key] = collection;
						} else {
							self.cache[key].reset(requestResponses[key], {
								silent : true
							});
						}
					} else {
						var Model = Backbone.Model;
						if (dependency.Model) {
							Model = dependency.Model;
						}

						// If we don't have it already, then add it
						if (!self.cache[key]) {
							var model = new Model(requestResponses[key]);
							self.cache[key] = model;
						} else {
							// Basically call a reset on the model
							// to replace anything that comes in
							self.cache[key].clear({
								silent : true
							});
							self.cache[key].set(requestResponses[key], {
								silent : true
							});
						}
					}

					retVal[key] = self.cache[key];
				}
			});

			log.debug(JSON.stringify(this.cache));

			return retVal;
		}
	};

	return DataModel;

});