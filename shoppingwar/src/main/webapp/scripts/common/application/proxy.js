/**
 * Copyright (C) 2012 Connecture
 * 
 * The proxy should allow for override request mappings otherwise it will use the defaults for get and updating
 * 
 * We might need some way to handle batch requests which would be a mixture of gets and persists
 * 
 * Future enhancements: - Break out the request hub/envelop stuff into our own file so that proxy is way more common then it currently is This should be a data interchange format, that basically
 * defines what the data format is for each type of request
 */
define([ 'module', 'log', 'common/application/proxy.filter', 'common/application/proxy.helper', 'jquery.url' ], function(module, log, filter, proxyHelper) {
	// I think this should be pretty stateless
	// This could be updated to define how we retrieve the data as
	// well
	var requestMappings = module.config().requests;

	var queuingEnabled = module.config().queue;
	var smartQueuingEnabled = module.config().smartQueue;

	var ajaxQueue = $({});

	/**
	 * Right now this method assumes everything going through it is local to the application, not external
	 */
	function buildUrl(options) {
		var requestConfig = requestMappings[options.key];

		// use this to handle overrides

		// if an override is provided for location, use it.
		// otherwise use whatever exists in the requestConfig.
		if (!options.location) {
			options.location = requestConfig.location;
		}

		var parsedUrl = $.url.parse(null);
		if (requestConfig.namespace) {
			parsedUrl.segments[1] = requestConfig.namespace;
		}

		var params = {};
		if (requestConfig.params && requestConfig.params.length > 0) {
			params = proxyHelper.getUrlParams(requestConfig.params, filter.getContext());
		}
		
		if (options.params) {
			_.extend(params, options.params);
		}

		var newUrl = {
			protocol : parsedUrl.protocol,
			host : parsedUrl.host,
			port : parsedUrl.port,
			segments : parsedUrl.segments,
			file : options.location,
			params : params
		};
		return $.url.build(newUrl);
	}

	// We don't want to expose this
	function _get(requestId, options, dones) {
		var self = this;
		var request = requestMappings[requestId];
		var filterKey = request.filter ? request.filter : 'none';

		var data;
		options.data ? data = options.data : data = {};

		// return a function that will return the deferred
		// we do not want to make the
		return {
			queue : options.queue || false,
			deferred : function() {
				// We are returning text for now as the dataType
				// because of an issue with the native JSON
				// parse of chrome and empty strings. If there
				// is an error or unauthorizes exception
				// we are returning that as a success but then
				// process headers, so we might not have a
				// responseText.
				return normalizeResponse.call(self, $.ajax({
					url : buildUrl({
						key : requestId
					}),
					dataType : 'text',
					data : data,
					cache : false
				})).pipe(function(requestResponses) {
					// Give the response, normalize it before we return it to the outside world
					requestResponses = filter.processResponse(filterKey, requestResponses);

					if (!options.raw) {
						requestResponses = filter.parse(requestResponses);
					}

					return requestResponses;
				}).done(dones);
			}
		};

	}

	function _persist(requestId, options, dones) {
		var self = this;
		var request = requestMappings[requestId];
		var filterKey = request.filter ? request.filter : 'none';

		var data;
		options.data ? data = options.data : data = {};

		// return a function that will return the deferred
		// we do not want to make the
		return {
			queue : options.queue || false,
			deferred : function() {
				// We are returning text for now as the dataType
				// because of an issue with the native JSON
				// parse of chrome and empty strings. If there
				// is an error or unauthorizes exception
				// we are returning that as a success but then
				// process headers, so we might not have a
				// responseText.
				return normalizeResponse.call(self, $.ajax({
					url : buildUrl({
						key : requestId
					}),
					dataType : 'text',
					type : 'post',
					data : data,
					cache : false
				})).pipe(function(requestResponses) {
					// Give the response, normalize it before we return it to the outside world
					requestResponses = filter.processResponse(filterKey, requestResponses);

					if (!options.raw) {
						requestResponses = filter.parse(requestResponses);
					}

					return requestResponses;
				}).done(dones);
			}
		};

	}

	function redirect(options) {
		window.location.href = buildUrl(options);
	}

	function normalizeResponse(request) {
		var self = this;
		// This will be uses for our request,
		// we will use this to determine status
		var normalizedRequest = $.Deferred();

		// Given the passed in request handle the
		// success/fail callbacks.
		// If it is successful, call our deferred
		// automatically resolving it
		request.then(function() {
			// OK, because we have some issues with how we can return
			// more RESTful appropriate errors, we are returning a 200 but
			// it might not always be a success, so we are returning
			// a specific error header we need to check for.

			// This should return null if it doesn't exist
			var xhr = arguments[2];
			var error = false;
			// basic error
			var internalError = xhr.getResponseHeader('SH-Standard-Error');
			// unauthorized error
			var unauthorized = xhr.getResponseHeader('X-Auth-Required');

			// check for true, regardless of type
			if (internalError && internalError == 'true') {
				// lol, convert it to a 500
				xhr.status = 500;
				error = true;
			}

			// check this one separately in case we don't have an SH error
			if (unauthorized) {
				error = true;
				if (unauthorized == 'unauthorized') {
					// lol, convert it to a 401
					// overwrite because this status is more specific
					xhr.status = 403;
				} else if (unauthorized == 'login') {
					xhr.status = 401;
				}
			}

			if (error) {
				normalizedRequest.reject($.parseJSON(xhr.responseText), "error", xhr);
			}

			normalizedRequest.resolve($.parseJSON(arguments[0]), arguments[1], arguments[2]);
		}, function(xhr) {

			// Check to see what the status code of the
			// response was. A 500 response will represent
			// an unexpected error. Anything else is simply
			// a non-20x error that needs to be manually
			// parsed.
			var response = xhr.responseText;
			if (response !== '') {
				$.parseJSON(response);
			}

			if (xhr.status == 500 || xhr.status == 400) {
				// Normalize the fail() response.
				normalizedRequest.reject(response, "error", xhr);
			} else {
				// Normalize the non-500 "failures."
				normalizedRequest.reject(response, "success", xhr);
			}
		});

		// Add a fail callback to the normalizedRequest
		// here so that this will be the first one called
		normalizedRequest.fail(function(response, status, xhr) {
			self._handleRequestFail(response, status, xhr);
		});

		// Now return a ready only version of our deferred object
		// so that other methods can attach callbacks and stuff
		return normalizedRequest.promise();
	}

	var Proxy = function() {

	};

	Proxy.prototype = {

		/**
		 * This method handles different method types depending on what you are trying to accomplish. If there are two parameters, the first parameter is either the requestId of the request we are
		 * trying to complete or it is an array of items that we are trying to get from the request hub. The second parameters it the options array.
		 * 
		 * If there is only one parameter then it is the options
		 * 
		 * 
		 * @param requestId ||
		 *            array of items
		 * @param options
		 * @returns
		 */
		get : function(key, options, callback) {
			var request = requestMappings[key];
			// This basically ends up process the data to send
			// We need to determine here, if we should do processing here or not

			// The filter will determine how to handle the data,
			// we need to pass in filter key, pass in filter key and
			// options
			var filterKey = request.filter ? request.filter : 'none';

			options = filter.processGet(filterKey, options);

			// options = filter.processGet(options.requests, options);

			options = _.extend({
				defer : true,
				// whether or not we should return the raw response
				raw : false
			}, options);

			if (options.defer) {
				return _get.call(this, key, options, callback);
			} else {
				// Call now but return so that we can attach other callbacks
				return this.run(_get.call(this, key, options, callback));
			}
		},

		/**
		 * This will handle our update or saving operations
		 * 
		 * We will probably have an internal method to handle this
		 * 
		 * @param options
		 */
		persist : function(key, options, callback) {
			var request = requestMappings[key];

			var filterKey = request.filter ? request.filter : 'none';

			options = filter.processPost(filterKey, options);

			options = _.extend({
				defer : true,
			}, options);

			if (options.defer) {
				return _persist.call(this, key, options, callback);
			} else {
				// Call now but return so that we can attach other callbacks
				return this.run(_persist.call(this, key, options, callback));
			}
		},
		/**
		 * If there are three parameters then key, options, callback
		 * 
		 * If there is only one parameter, then key
		 * 
		 * If there are only two parameters and first on is string and second one is object, then key, options
		 * 
		 * @param options
		 */
		process : function(key, options, callback) {
			options = options || {};
			// Key? what key?
			if (_.isString(key)) {
				var request = requestMappings[key];
				if (request.type === 'get') {
					return this.get(key, options, callback);
				} else if (request.type === 'post') {
					return this.persist(key, options, callback);
				} else if (request.type === 'redirect') {
					// Create a deferred, add the callbacks first, then
					// the last one will be the redirect
					var redirectRequest = $.Deferred();

					redirectRequest.done(callback).done(function() {
						redirect(_.extend(options, {
							key : key
						}));
					});

					redirectRequest.resolve();
				}
			} else {
				return false;
			}
		},
		/**
		 * Helper method to run proxy requests for us
		 * 
		 * @param deferred
		 * @returns
		 */
		run : function(deferred) {
			return $.when(deferred.deferred());
		},
		/**
		 * Not sure I like passing in this second parameter right now but deferred is return as a function, might need to expand it to return other information
		 * 
		 * 
		 * @param deferred
		 * @param type
		 */
		queue : function(deferred) {
			var self = this;

			if (!queuingEnabled) {
				log.debug('running - async');
				// Create a complete callback to fire the next event in the queue.
				self.run(deferred).done(function() {
					log.debug('done- async');
				});
			} else {
				// As requests come in, if one of them
				// is an update, then we need to start the queue process
				// 
				// if there is no update in the queue, then everything can
				// pass through, the second we get an update though, we need
				// to start queue requests.

				// if an update comes in while the queue is running, it should
				// be put on the back of the queue because future requests coming
				// in might rely on that update

				// Enhancements
				// - start, stopping the queue
				// - popping things off the queue to
				// see if they can batched together

				// if the request coming in is an update
				if (deferred.queue || !smartQueuingEnabled) {
					// Queue our ajax request.
					ajaxQueue.queue(function(next) {
						log.debug('running - queue');
						// Create a complete callback to fire the next event in the queue.
						self.run(deferred).done(function() {
							log.debug('done - queue');
							// Run the next query in the queue.
							next();
						});
					});
				} else {
					// if we have anything in the queue, add to the queue
					if (ajaxQueue.queue().length > 0) {
						// Queue our ajax request.
						ajaxQueue.queue(function(next) {
							log.debug('running - queue');
							// Create a complete callback to fire the next event in the queue.
							self.run(deferred).done(function() {
								log.debug('done - queue');
								// Run the next query in the queue.
								next();
							});
						});
					} else { // the queue is empty and this is not an update, just run
						log.debug('running - async');
						// Create a complete callback to fire the next event in the queue.
						self.run(deferred).done(function() {
							log.debug('done- async');
						});
					}
				}
			}

		},

		/**
		 * This will internally handle generic fail response codes. This can be overridden if your application needs to do something differently.
		 * 
		 * @param response
		 * @param status
		 * @param xhr
		 */
		_handleRequestFail : function(response, status, xhr) {
			// Does it make sense for the proxy object to handle "redirects"?
			// TODO: This should actually be configurable based on
			// what type of request it is
			if (status === 'error') {
				if (xhr.status == 500 || xhr.status == 400) {
					serverErrorDialog = $('<div id="errorContent"></div>');
					if (typeof (response.resultCode) !== 'undefined') {
						// general server error
						if (response.resultCode == 'error') {
							var errorMessage = '<p>message: ' + response.message + '</p>';
							errorMessage += '<p>exceptionName: ' + response.exceptionName + '</p>';
							errorMessage += '<p>exceptionClass: ' + response.exceptionClass + '</p>';
							errorMessage += '<p>exceptionStack: ' + response.exceptionStack + '</p>';

							serverErrorDialog.html(errorMessage);
							serverErrorDialog.dialog({
								title : "General Server Error",
								autoOpen : false,
								width : 600,
								height : 400,
								resizable : true,
								modal : true
							});
						}
						// concurrency error
						if (response.resultCode == 'concurrency') {
							var errorMessage = '<p>' + response.message + '</p>';
							serverErrorDialog.html(errorMessage);
							serverErrorDialog.dialog({
								title : "Concurrency Error",
								autoOpen : false,
								width : 300,
								height : 200,
								resizable : true,
								modal : true,
								close : function() {
									// eh
									window.location.reload(true);
								}
							});
						}
						serverErrorDialog.dialog('open');
					}
				} else if (xhr.status === 404) {
					// handle 404
				} else if (xhr.status === 403) {
					this.process('unauthorized');
				} else if (xhr.status === 401) {
					this.process('login');
				}
			} else if (status === 'success') {
				if (xhr.status === 401) {
					this.process('login');
				} else if (xhr.status === 403) {
					this.process('login');
				}
			}

		}
	};

	return Proxy;

});