/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'underscore', 'backbone' ], function(require, _, Backbone) {

	var Region = function(options) {
		this.options = options || {};

		_.extend(this, options);

		if (!this.el) {
			var err = new Error("An 'el' must be specified");
			err.name = "NoElError";
			throw err;
		}

		if (this.initialize) {
			this.initialize.apply(this, arguments);
		}
	};

	_.extend(Region.prototype, Backbone.Events, {
		// TODO: Pretty sure this does not work anymore
		refresh : function(options) {
			this.show(this.currentView, options);
		},
		/**
		 * 
		 * @param View
		 * @param viewOptions
		 * @param options
		 */
		show : function() {
			var args = Array.prototype.slice.call(arguments);

			var view = null;
			var View = null;
			var viewOptions = {};
			var options = {};
			// If there is one parameter, it is the view
			// If there are two parameters, first one is the view, second one is the options
			// If there are three parameters, first one is a View type, second one is view options and the third one are shows options
			if (args.length === 1) {
				view = args[0];
			} else if (args.length === 2) {
				view = args[0];
				options = args[1];
			} else if (args.length === 3) {
				View = args[0];
				viewOptions = args[1];
				options = args[2];
			} else {
				var err = new Error("Not enough arguments or too many arguments to compute.");
				err.name = "InvalidArgumentsError";
				throw err;
			}
			// View, viewOptions, options
			
			// Right now we are using the options parameter for the render
			//parameters as well
			this.options = _.extend({
				renderToParent : false
			}, options);

			this.ensureEl();

			// We can append directly to the region or we can have the view own
			// it el which will most likely create its own div, really depends on
			// what you need to do. If you are appending to the parent though we
			// need to take into account some things though.
			if (this.options.renderToParent) {
				// If this is being bound to the parent we need to make sure we are emptying the parent
				// and not remove
				this.close({
					empty : true
				});
				// Because we are appending directly to the parent, we want to unbind and
				// undelegate on the parent so we don't have any left over events
				this.$el.unbind();
				this.$el.undelegate();
				// store the region element for the view element to append to
				// This will allow the view do render how it needs.
				// We have to pass it in and then render because we want to
				// render to the el and NOT have the view append to a generic
				// div and then append that.

				if (view === null) {
					view = new View(viewOptions);
				}

				view.setElement(this.$el);
				view.render(options);
			} else {
				// Otherwise this is standard
				this.close();

				if (view === null) {
					view = new View(viewOptions);
				}

				// render the view
				view.render(options);
				// append that to the region
				this.open(view);
			}
			// check to see if we have a callback on
			// the view
			if (view.onShow) {
				view.onShow();
			}
			// view.trigger("show");

			if (this.onShow) {
				this.onShow(view);
			}
			this.trigger("view:show", view);
			// Store our current view for reference
			this.currentView = view;
		},
		ensureEl : function() {
			if (!this.$el || this.$el.length === 0) {
				this.$el = this.getEl(this.el);
			}
		},
		getEl : function(selector) {
			return $(selector);
		},
		open : function(view) {
			// make sure it was not previously hidden
			this.$el.show();
			this.$el.html(view.el);
		},
		close : function(options) {
			var view = this.currentView;
			if (!view) {
				return;
			}

			if (view.close) {
				view.close();
			}

			view.dispose(options);
			this.trigger("view:closed", view);

			delete this.currentView;
		},
		attachView : function(view) {
			this.currentView = view;
		},
		reset : function() {
			this.close();
			delete this.$el;
		},
		/**
		 * Use this for cases where we want to physically hide this region and not just close to remove the view.
		 */
		hide : function() {
			this.ensureEl();
			this.$el.hide();
		}
	});

	return Region;
});