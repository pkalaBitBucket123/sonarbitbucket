/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require' ], function(require) {
	// lol return empty object for now :)
	// we could expand on this in the future if necessary
	// but we needed to get access to all of the datas
	// at run-time.  This proxy cache object will work with
	// the proxy datamodel and then we can always use it for
	// getting access to the data for GTM stuff
	return {

	};
});