/**
 * Copyright (C) 2012 Connecture
 * 
 * Going to return an object literal for now. I do not see us needing multiple messenger objects created within the life-time of the application. If anything we should have a single object but
 * multiple channels if necessary.
 * 
 */
define([ 'require', 'underscore', 'log', 'common/application/eventmanager' ], function(require, _, log, EventManager) {

	// internally we will be using EventManager which is backed by Backbone. I don't see a need
	// to code our own event system for this yet.

	// again we should only need one for now. If we were to support channels, then
	// we could create new ones.

	var eventManager = new EventManager();

	return {
		subscribe : function(subscriptions, context) {
			_.each(subscriptions, function(subscription, key) {
				if (subscription && $.isFunction(subscription)) {
					// Passing in the component as a context
					eventManager.on(key, subscription, context);
				} else if (typeof subscription === 'string') {
					// if it is a string, that means we are going to
					// do some helper event stuff. Check to see type
					// right now we only support 'update'
					if (subscription.lastIndexOf('update', 0) === 0) {
						// If the reaction is update then let's do this
						// We could pass on what we want to update as well
						eventManager.on(key, self._proxyUpdate);
					}
				} else {
					log.error('You are using an unsupported event type.');
				}
			});
		},
		/**
		 * TODO: Update this to pass in data per publish, should probably also have a configuration object per publish taht gets passed in, instead of all of these parameters
		 * 
		 * @param publish
		 * @param actor
		 * @param channel
		 * @param data
		 * @returns
		 */
		publish : function(publish, actor, data) {
			_.each(publish, function(publisher, key) {
				// key = component level trigger
				// value = the application level trigger that needs to happen
				// We need to pass in a context for this event so that we
				// can trigger the appropriate event...the context is just
				// the key for what we are triggering, not sure that is
				// the correct way to do it
				actor.eventManager.on(key, function() {
					var args = Array.prototype.slice.call(arguments);

					if (typeof this.data !== 'undefined') {
						args.unshift(this.key, this.data);
					} else {
						args.unshift(this.key);
					}

					eventManager.trigger.apply(eventManager, args);
				}, {
					key : publisher,
					data : data
				});
			});
		}
	};

});