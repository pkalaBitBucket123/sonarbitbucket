/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone' ], function(require, Backbone) {

	var ProxyCollection = Backbone.Collection.extend({
		storageEngine : 'Proxy',
		initialize : function(models, options) {
			options = _.extend({}, options);
			this.storeItem = options.storeItem ? options.storeItem : null;
			this.item = _.isObject(this.storeItem) ? this.storeItem.item : this.storeItem;
		},
		/**
		 * This method allows us to set the data we need for fetching after construction. This is for cases when we want one collection object but to
		 * 
		 * @param data
		 */
		setData : function(data) {
			if (this.storeItem) {
				this.storeItem.data = data;
			}
		}
	});

	return ProxyCollection;
});