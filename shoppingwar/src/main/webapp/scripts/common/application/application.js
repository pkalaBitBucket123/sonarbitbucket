/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: I think we will want some sort of component/layout/region manager. Maybe to define certain areas for components
 * 
 * TODO: We need to add a persistent/mediator layer here to handle server communication and when to do it.
 * 
 */
if (!Object.create) {
	Object.create = function create(prototype, properties) {
		var object;
		if (prototype === null) {
			object = {
				"__proto__" : null
			};
		} else {
			// Needs to be an object, otherwise this won't work!
			if (typeof prototype != "object") {
				throw new TypeError("typeof prototype[" + (typeof prototype) + "] != 'object'");
			}
			// Create a new object
			var Type = function() {
			};
			// Assign the new object's prototype to the prototype we passed in
			Type.prototype = prototype;
			// now create a new "Type", this works because we will than add new
			// methods to
			// the prototype of the object we return from this method, adding
			// new or overriding existing.
			// We are not doing any special work here to be able to
			// automatically call super methods, that will
			// have to be done manually, basically because with this method we
			// don't know what we are overriding
			// yet.
			object = new Type();

			// I didn't write this comment :)
			// IE has no built-in implementation of `Object.getPrototypeOf`
			// neither `__proto__`, but this manually setting `__proto__` will
			// guarantee that `Object.getPrototypeOf` will work as expected with
			// objects created using `Object.create`
			object.__proto__ = prototype;
		}
		if (properties !== void 0) {
			Object.defineProperties(object, properties);
		}
		return object;
	};
}

function getUUID() {
	var UUID = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == "x" ? r : (r & 3 | 8);
		return v.toString(16);
	});
	return UUID;
}

//read coverageType from URL param and set it in account.
function setCoverageTypefromURLParam(account) {
	try {
		var cov = decodeURIComponent((new RegExp('[?|&]coverageType=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
		if(cov!=null && cov!="" && (cov == "Den" || cov == "Med" || cov == "STH")) {
			account.set("coverageType", cov);
		}
	}catch(e){}
}

define([ 'require', 'backbone', 'common/application/proxy', 'common/application/eventmanager', 'common/views/view.loader.default', 'common/application/application.manager.region',
		'common/application/application.region.modal' ], function(require, Backbone, Proxy, EventManager, DefaultLoader, RegionManager, ModalRegion) {
	var Callbacks = function() {
		this._callbacks = [];
	};

	Callbacks.prototype = {
		add : function(callback) {
			this._callbacks.push(callback);
		},
		run : function(options, context) {
			return $.when(all(this._callbacks));
		},
		clear : function() {
			this._callbacks = [];
		}
	};

	var all = function(array) {
		var deferred = $.Deferred();
		var fulfilled = 0, length = array.length;
		var results = [];

		if (length === 0) {
			deferred.resolve(results);
		} else {
			array.forEach(function(promise, i) {
				$.when(promise()).then(function(value) {
					results[i] = value;
					fulfilled++;
					if (fulfilled === length) {
						deferred.resolve(results);
					}
				});
			});
		}

		return deferred.promise();
	};

	var Application = function(options) {
		var self = this;

		// Setup configurable defaults
		this.options = _.extend({
			uses : [],
			View : null,
			// configuration on what to do while loading the application
			loading : {
				// this field doesn't do anything right now
				display : 'after',
				// view to display while loading
				View : DefaultLoader
			},
			regions : {

			}
		}, options);

		// Add our default regions that cannot be
		// changed or overridden right now
		this.options.regions['loading'] = '._loadingView';
		this.options.regions['modal'] = {
			regionType : ModalRegion
		};

		// Setup object defaults
		this.initCallbacks = new Callbacks();
		this.startupCallbacks = new Callbacks();
		this.eventManager = new EventManager();
		this.proxy = new Proxy();
		// this stores application specific plugins, that
		// are active objects
		this.plugins = [];
		// this flag determines whether or not the app
		// as been loaded
		this.started = false;

		// Check to see if we have a layout for this
		// application. Add an event to render this
		if (this.options.Layout) {
			// Add it to our start up event
			this.eventManager.on("initialize:before", function(options) {
				if (!this.started) {
					// We need to render the layout view first
					// This is the starting point that has all of the
					// regions that are configured
					new this.options.Layout({
						eventManager : self.eventManager,
					}).render();

					this.regionManager = new RegionManager({
						el : 'body',
						regions : this.options.regions
					});
				}
			}, this);
		}

		// Add default events

		// Now let's attach our initializing screen
		this.eventManager.on("initialize:before", function(options) {
			var display = false;
			// We need this initial flag because, we need to know
			// if we are coming in fresh and index, then we display welcome view
			// otherwise if we are coming in fresh but not at the index, we don't
			// want to display the welcome page
			if (options.initial) {
				// This will close all regions so we can display the loading screen
				this.regionManager.reInitializeRegions();
				this.loadingView = new this.options.loading.View();
				display = true;
			} else {
				if (!this.started) {
					this.loadingView = new DefaultLoader({
						eventManager : this.eventManager
					});
					display = true;
				}
			}

			if (display) {
				this.regionManager.loading.show(this.loadingView);
			}

		}, this);

		// Not sure about the name but
		this.eventManager.on('exit', function(data) {
			var self = this;
			// manually build options, we can only override
			// certain ones
			// location is NOT required
			this.proxy.process(data.key, {
				location : data.location,
				params : data.params
			}, function() {
				// Trigger an exit complete
				self.eventManager.trigger('exit:complete');
				// once that is done the application needs to do its own cleanup
				self.regionManager.close();

				// replace the regionManager with a simple version
				// with only the loading region
				self.regionManager = new RegionManager({
					el : 'body',
					regions : {
						'loading' : '._loadingView'
					}
				});
				// display the loading region as we exit the application
				self.regionManager.loading.show(new DefaultLoader({
					eventManager : this.eventManager
				}));
			});
		}, this);

		// Process any plugins, these need to go AFTER
		// we do initial app events, to keep a strict ordering
		// of how event handlers are processed. Although really
		// within the same event, there should
		// not be dependencies :) - fix when we have time TODO
		if (this.options.uses) {
			if (_.isArray(this.options.uses)) {
				_.each(this.options.uses, function(Plugin) {
					self.uses(Plugin);
				});
			} else if (_.isFunction(this.options.uses)) {
				self.uses(Plugin);
			}
		}
	};

	Application.prototype = {
		/**
		 * Initializers can be one of two things. They can be a standard callback function that needs to do some piece of work, or they can be deferred objects that need to handle an asyncrhonous
		 * request.
		 */
		addInitializer : function() {
			var args = Array.prototype.slice.call(arguments);
			args.splice(1, 0, this);
			this.initCallbacks.add.apply(this.initCallbacks, args);
		},
		/**
		 * These initializers can be used after the application has started up
		 */
		addStartupInitializer : function() {
			var args = Array.prototype.slice.call(arguments);
			args.splice(1, 0, this);
			this.startupCallbacks.add.apply(this.startupCallbacks, args);
		},
		start : function(options) {
			var self = this;

			options = options || {
				initial : true
			};
			
			// we were previously at initial and we are not going
			// to initial then set these on the options to fix somethings
			if (this.initial && !options.initial) {
				options.index = true;
				options.initial = false;
			}

			// TODO: Hack for now.
			// Now that we got this fixed so requests are working, using
			// the back button or routes is now forcing these call each time
			// This needs to get updated so that we are still running
			// the initCallbacks and startupCallbacks but they
			// are configured based on start or not
			if (self.started) {
				this.initCallbacks.clear();
				this.startupCallbacks.clear();
			}

			this.eventManager.trigger("initialize:before", options);
			// Since we know this type of callback supports async requests
			// that are dependent to continue we need to handle it a little differently
			this.initCallbacks.run().done(function() {
				self.eventManager.trigger("initialize:after", options);
				self.startupCallbacks.run().done(function() {
					self.eventManager.trigger("start", options);
					self.started = true;
				});
			});

			// This is here because of an issue when you use a route to go
			// to the welcome page and then you use the back button
			// to a route other than the index, shit explodes
			this.initial = options.initial;
		},
		uses : function(Plugin, options) {
			var plugin = new Plugin();
			// application only attaches itself to
			// application level plugins
			if (plugin.type === 'application') {
				options = options || {};
				options = _.extend(options, {
					plugged : this,
					eventManager : this.eventManager
				});

				plugin.plugify(options);
			}
			// always add it for future use though
			this.plugins.push(plugin);
		},
		/**
		 * This will return all application specific routes
		 */
		getRoutes : function() {

		}
	};

	return Application;
});
