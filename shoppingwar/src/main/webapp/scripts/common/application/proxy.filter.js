/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: Make this file the base/interface and then extend it for shopping because some of the stuff in here is specific to shopping, ie request hub
 * 
 * TODO: Issue, looks like params are also getting mixed into the data
 * 
 */
define([ "module", 'common/application/datamodel', 'common/application/proxy.helper' ], function(module, dataModel, proxyHelper) {

	var stores = module.config().stores;
	var context = module.config().context;

	function _getStore(alias) {
		var retVal = null;
		_.each(stores, function(store) {
			if (store.items) {
				_.each(store.items, function(item) {
					if (item === alias) {
						retVal = store;
						return;
					}
				});
				if (retVal !== null) {
					return;
				}
			} else {
				if (store.type === alias) {
					retVal = store;
					return;
				}
			}

		});
		return retVal;
	}

	function _buildRequestKey(type, method, params) {
		var retVal;
		params = params ? params : [];
		var sortedParams = _.sortBy(params, function(obj) {
			return obj.key;
		});
		// Now we need to convert this into something
		var paramsString = JSON.stringify(sortedParams);

		retVal = method + ':' + type + ':' + paramsString;

		return retVal;
	}

	/**
	 * Data should be passed as individual objects to build the request envelope
	 * 
	 * The first argument should be the method though
	 */
	function stuffEnvelope() {
		var retVal = {
			envelope : {
				requests : []
			}
		};

		var requests = Array.prototype.slice.call(arguments);
		var tempRequests = [];
		_.each(requests, function(request) {
			// Each request needs these values
			if (!request.store) {
				throw new Error('The item object is required to stuff the envelope');
			}
			if (!request.method) {
				throw new Error('The method type is required to stuff the envelope');
			}

			// Each request can have parameters that are passed in from the code base AND parameters
			// can be configured that can be pulled from the URL. Parameters from the store
			// are going to come in as strings.

			var params = [];
			if (request.store.params) {
				params = params.concat(request.store.params);
			}
			if (request.params) {
				params = params.concat(request.params);
			}

			params = proxyHelper.getRequestParams(params);

			var key = _buildRequestKey(request.store.type, request.method, params);

			tempRequests.push({
				method : request.method,
				type : request.store.type,
				key : key,
				params : params,
				data : request.data,
				context : context
			});
		});

		retVal.envelope.requests = _.unique(tempRequests, false, function(request) {
			return request.key;
		});

		retVal.envelope.context = context;

		return retVal;
	}

	return {
		processGet : function(filter, options) {
			// TODO: At some point make these filter types more
			// dynamic
			var data = {};
			// if any one requires a queue, then queue
			var queue = false;
			if (filter === 'hub') {
				var stores = [];
				_.each(options.requests, function(item) {
					// Check to see if the item is a string or not
					// If it is a string, then just grab the item
					// otherwise if it is an object, then pull it cause
					// we might also be passing in data
					if (_.isString(item)) {
						var store = _getStore(item);

						if (_.include(store.queue || [], 'get')) {
							queue = true;
						}

						stores.push({
							store : store,
							method : 'get'
						});
					} else if (_.isObject(item)) {
						var store = _getStore(item.item);

						if (_.include(store.queue || [], 'get')) {
							queue = true;
						}

						stores.push({
							store : store,
							method : 'get',
							data : item.data,
							params : item.params
						});
					}
				});

				// Now we need to compact our requests, we only want unique
				// requests
				data = stuffEnvelope.apply(null, stores);

				// Moving this here because I want the base _get to be pretty dumb
				// with what it is doing, so it can be used for other purposes
				data = {
					envelope : JSON.stringify(data.envelope)
				};
			} else {
				// if there is no type then just pass through the data
				data = JSON.stringify(options.data);
			}

			options = _.extend(options, {
				data : data,
				queue : queue
			});

			return options;
		},
		processPost : function(filter, options) {
			// TODO: At some point make these filter types more
			// dynamic
			var data = {};
			// if any one requires a queue, then queue
			var queue = false;
			if (filter === 'hub') {
				var stores = [];
				_.each(options.requests, function(item) {
					// Check to see if the item is a string or not
					// If it is a string, then just grab the item
					// otherwise if it is an object, then pull it cause
					// we might also be passing in data
					if (_.isString(item.store)) {
						var store = _getStore(item.store);

						if (_.include(store.queue || [], 'post')) {
							queue = true;
						}

						stores.push({
							store : store,
							method : 'update',
							data : item.data,
							params : item.store.params
						});
					} else if (_.isObject(item.store)) {
						var store = _getStore(item.store.item);

						if (_.include(store.queue || [], 'post')) {
							queue = true;
						}

						stores.push({
							store : store,
							method : 'update',
							data : item.data,
							params : item.store.params
						});
					}
				});

				// Now we need to compact our requests, we only want unique
				// requests
				data = stuffEnvelope.apply(null, stores);

				// Moving this here because I want the base _get to be pretty dumb
				// with what it is doing, so it can be used for other purposes
				data = {
					envelope : JSON.stringify(data.envelope)
				};
			} else {
				// if there is no type then just pass through the data
				data = {
					// TODO: Make this configurable, very very bad
					jsonRequestObject : JSON.stringify(options.data)
				};
			}

			options = _.extend(options, {
				data : data,
				queue : queue
			});

			return options;
		},
		processResponse : function(filter, requestResponses) {
			// Pull the data out of here and pass it on so that
			// we are also normalizing our responses...however we are
			// currently ignoring errors...we will have to fix that later
			var retVal = {};

			if (filter === 'hub') {
				_.each(requestResponses.requests, function(request) {
					_.each(request.data, function(item, key) {
						retVal[key] = item;
					});

				});
			} else {
				retVal = requestResponses;
			}

			return retVal;
		},
		parse : function(requestResponses) {
			var retVal = dataModel.model(requestResponses);

			return retVal;
		},
		/**
		 * Right now this module, owns the context value. We need this for redirects for now. TODO: The better option might be to have the redirect run through the filter
		 * 
		 * @returns
		 */
		getContext : function() {
			return context;
		}
	};

});