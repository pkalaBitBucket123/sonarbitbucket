/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ "module", 'backbone', 'common/application/proxy.cache', 'jquery.url' ], function(module, Backbone, cache, jqueryURL) {

	/**
	 * Trigger both page level events and individual user triggered events
	 * 
	 * Cache should NEVER be added to the EventManager object directly, I do not want any abuse. It is only in here for GTM.
	 */
	var EventManager = function() {

		// for capturing a page view in Google Analytics
		
		// make sure we have a global dataLayer variable defined
		window.dataLayer = window.dataLayer || [];

		var componentHeader = 'header';
		var componentAccount = 'planAdvisor';
		var componentList = 'planList';
		var componentCart = 'cart';

		// TODO | ideally should have:
		/*
		 * dataLayer.push({ 'pageDestination' : destination, // page name 'pageFlow' : flowName, // page category/flow -- IFP or EE? 'programType' : 'planDetail', // page sub-category 'userRole' :
		 * userRole, // role of user 'language' : language, //page being viewed in English or Spanish? Not needed for IA Shopping 'event' : 'newPage' // GTM trigger });
		 */

		// HEADER events
		// login
		
		this.on('view:login', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentHeader, // page name
				'programType' : 'login', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		this.on('view:chat:show', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentHeader, // page name
				'programType' : 'chat', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		// ACCOUNT events

		// help
		this.on('view:help:show', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentAccount, // page name
				'programType' : 'help', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		// switching to a new widget
		this.on('workflow:item:started', function(widgetId) {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentAccount, // page name
				'programType' : widgetId, // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		// summary - hard-coded
		this.on('workflow:item:summary:show', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentAccount, // page name
				'programType' : 'Summary', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		// LIST events

		this.on('view:showProductDetail', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentList, // page name
				'programType' : 'planDetail', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		this.on('view:showProductList', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentList, // page name
				'programType' : 'planList', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		this.on('view:showProductCompare', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentList, // page name
				'programType' : 'planCompare', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		this.on('view:showTopProducts', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentList, // page name
				'programType' : 'planCompare', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		this.on('view:selectProduct', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentList, // page name
				'programType' : 'planAdded', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		// CART events

		this.on('view:cart:show', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentCart, // page name
				'programType' : 'showCart', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		this.on('view:addSuggestedProduct', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : componentCart, // page name
				'programType' : 'suggestedPlanAdded', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});

		// TODO: Leave this one hardcoded in here for now until next release
		this.on('enroll', function(data) {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'account' : data,
				'userRole' : userRole, // role of user
				'event' : 'enroll'
			});
		});

		// COMMON events

		this.on('view:product:rates:show', function() {
			var userRole = cache['actor'] ? cache['actor'].get('userRole') : '';
			dataLayer.push({
				'pageDestination' : 'costBreakdown', // page name
				'programType' : 'costBreakdown', // page sub-category
				'userRole' : userRole, // role of user
				'event' : 'newPage' // GTM trigger
			});
		});
	};

	EventManager.prototype = _.extend(EventManager.prototype, Backbone.Events);

	return EventManager;

});