/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ "require", 'log', 'backbone' ], function(app, log, Backbone) {

	// Defining the application router, and all base app routes
	// We are going to have to check if data is loaded already
	// if it is, then we dont start the app up, we just go somewhere
	// this is for back button stuff

	// the application should have an init flag
	// to determine if it is started or not
	var Router = Backbone.Router.extend({
		routes : {
			'' : 'root',
			'index' : 'index',
			'welcome' : 'welcome'
		},
		initialize : function(options) {
			var self = this;
			this.application = options.application;
			// given the application, all to get application
			// specific routes
			var appRoutes = this.application.getRoutes();

			_.each(appRoutes, function(route) {
				self.route(route.route, route.name, function() {
					var routeOptions = route.callback.apply(this, arguments);
					self.application.start({
						initial : false,
						route : routeOptions
					});
				});
			});
		},
		/**
		 * This will be for initial loads
		 */
		root : function() {
			this.application.start({
				initial : true
			});
		},
		/**
		 * This will be for the start of the app after a potential welcome page
		 */
		index : function() {
			this.application.start({
				initial : false,
				index : true
			});
		},
		welcome : function() {
			this.application.start({
				initial : true
			});
		},
		current : function() {
			var Router = this;
			var fragment = Backbone.history.fragment;
			var routes = _.pairs(Router.routes);
			var route = null;
			var params = null;
			var matched;

			matched = _.find(routes, function(handler) {
				route = _.isRegExp(handler[0]) ? handler[0] : Router._routeToRegExp(handler[0]);
				return route.test(fragment);
			});

			if (matched) {
				params = Router._extractParameters(route, fragment);
				route = matched[1];
			}

			return {
				route : route,
				fragment : fragment,
				params : params
			};
		}
	});

	return Router;

});