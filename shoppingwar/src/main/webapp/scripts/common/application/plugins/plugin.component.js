/**
 * Copyright (C) 2012 Connecture
 * 
 * TODO: I think we need to get to the point where we are 100% model driven, if they are using them when going through components. Or at least better support to utilize Backbone Model fetching and
 * saving.
 */
define([ 'require', 'log', 'common/plugin', 'common/navigation', 'common/application/messenger' ], function(require, log, Plugin, Navigation, messenger) {

	function getComponentDependencies(components, event) {
		var retVal = [];
		_.each(components, function(component, key) {
			if (typeof event === 'undefined' || component.configuration.engage === event) {
				_.each(component.items, function(componentItem) {
					if (componentItem.dependency) {
						retVal.push(componentItem.alias);
					}
				});
			}
		});

		retVal = _.uniq(retVal);

		return retVal;
	}

	function isComponentsEvent(components, event) {
		var retVal = false;
		_.each(components, function(component, key) {
			if (component.configuration.engage === event) {
				retVal = true;
			}
		});

		return retVal;
	}

	/**
	 * Plugin should get access to the component that it is plugging into
	 * 
	 * I will attach stuff to the component itself, or it can if it so desires
	 * 
	 */
	var ComponentPlugin = function() {
		Plugin.apply(this, arguments);
		this.type = 'application';
	};

	ComponentPlugin.prototype = Object.create(Plugin.prototype);

	ComponentPlugin.prototype.initialize = function(options) {
		Plugin.prototype.initialize.call(this, options);
		// For this plugin add a new property to the application
		this.plugged.components = {};

		// create the nav at the application level
		// this could be moved to the application itself
		// if it proves useful
		this.plugged.navigation = new Navigation();

		// check to see if a component as added for the loading screen

		if (this.plugged.options.loading && this.plugged.options.loading.Component) {
			var loadingComponent = new this.plugged.options.loading.Component();

			// TODO Make this not a hack
			loadingComponent.eventManager.on('view:component:welcomeDone', function() {
				if (this.componentsEngaged) {
					Backbone.history.navigate('index', {
						trigger : true
					});
				} else {
					this.eventManager.trigger('view:components:start');
				}
			}, this.plugged);

			this.plugged.components[loadingComponent.id] = loadingComponent;
			this.plugged.loadingComponentId = loadingComponent.id;
			this.plugged.hasLoadingComponent = true;
			this.plugged.autoRun = false;
		} else {
			this.plugged.hasLoadingComponent = false;
			this.plugged.autoRun = true;
		}
	};

	ComponentPlugin.prototype.addEvents = function() {
		// Given an application level event exit:complete,
		// loop through all components and destroy them
		this.eventManager.on('exit:complete', function() {
			_.each(this.components, function(component, key) {
				component.destroy();
			});
		}, this.plugged);

		// Hook into the application start event to load our components
		// This allows us to switch out the component start up process
		// if we would need to but keeps everything else in tact

		// Since the plugin, starts up and adds its events because it
		// needs to bootstrap. Given the initialize event, we need to add
		// a proxy initializer. Not sure I like this yet but it works.
		// I think the other alternative would be to do it in the start
		// and then trigger another event that would say the components are completed
		// do something
		this.eventManager.on("initialize:before", function(options) {
			if (this.hasLoadingComponent && options.initial) {
				var loadingComponent = this.components[this.loadingComponentId];
				// TODO HACK HERE because of this special component
				$('._loaderComponent').html(loadingComponent.getLayoutView().render().el);

				// reInitialize the regions in case we are coming back, but only
				// if we started already
				if (loadingComponent.regionManager) {
					loadingComponent.regionManager.reInitializeRegions();
				}

				loadingComponent.engage();
			} else {
				// If there is a not a route, then destroy everything
				// otherwise leave everything
				if (!options.route) {
					// Right now, if it is not an initial load, we are destroying
					// everything and then rendering it again
					_.each(this.components, function(component, key) {
						// just remove from view, we do not want to destroy
						// the session
						component.remove();
					});
				}
			}

		}, this.plugged);

		this.eventManager.on("initialize:before", function(options) {
			var self = this;
			if (!this.started) {
				// Check init event
				// First make sure we have some components that start at this
				// event
				if (isComponentsEvent(self.components, 'initialization')) {

					var initDependencies = getComponentDependencies(self.components, 'initialization');

					if (!_.isEmpty(initDependencies)) {
						this.addInitializer(this.proxy.process('component:get', {
							requests : initDependencies,
							defer : true
						}, function(requestResponses) {
							// once response is done, loop through each component
							// loading data and then started
							_.each(self.components, function(component, key) {
								if (component.configuration.engage === 'initialization') {
									// Now we need to loop through our dependencies
									// to build the data object to load the component
									_.each(component.items, function(dependency) {
										// If this component has a dependency, then process
										if (dependency.dependency && requestResponses[dependency.alias]) {
											component.addModel(requestResponses[dependency.alias], dependency.alias);
										}
									});
									// Given the data that this component is dependent on, pass it in
									// I am using _ methods for most things but this time I am
									// using $ extend because of its deep copy abilities

									// This basically make sure that the data the component is
									// getting is not the same as others. This way we won't
									// have to worry about data changing in one component
									// breaking something else
									// component.load($.extend(true, {}, componentData));
									component.load({});
									// This will start the component

									// TODO: Needs a better location
									if (component.configuration.layout.region) {
										self.regionManager[component.configuration.layout.region].show(component.getLayoutView());
									}

									// We are going to engage these later
									component.engage();

								}
							});
						}).deferred);
					}
				}

				// check startup event

				// Actually we should check to see if there are any components that are set
				// to initialization

				// then check to see if they have any dependencies

				// We need to startup any components that are part of the init
				// but if there are no dependencies, just load them right away

				// grab our component dependencies
				var componentDependencies = getComponentDependencies(self.components, 'startup');

				// Make the Ajax call
				// Might be able to make run handle the get as well
				this.addStartupInitializer(this.proxy.process('component:get', {
					requests : componentDependencies,
					defer : true
				}, function(requestResponses) {
					// This is kind of a hack but for now we need to capture the
					// applicatConfig model. We need this to determine what plugins to run
					self.activePlugins = requestResponses['plugins'];

					// once response is done, loop through each component
					// loading data and then started
					_.each(self.components, function(component, key) {
						if (component.configuration.engage === 'startup') {
							// //////////////////////////////////////////////
							// Temporary
							// Because we have two startup calls, and we want to reprocess
							// some components, we are going to clear each component first
							// then add the data back in for the second call. Right now
							// the second call will make all the calls again, until we have a spot
							// where we can specify what data is called on each so there are no
							// redundant calls
							// ////////////////////////////////////////////////
							component.clear();

							_.each(component.items, function(dependency) {
								// If this component has a dependency, then process
								if (dependency.dependency && requestResponses[dependency.alias]) {
									component.addModel(requestResponses[dependency.alias], dependency.alias);
								}
							});

							// Given the data that this component is dependent on, pass it in
							// I am using _ methods for most things but this time I am
							// using $ extend because of its deep copy abilities

							// This basically make sure that the data the component is
							// getting is not the same as others. This way we won't
							// have to worry about data changing in one component
							// breaking something else
							component.load({});
						}
					});
				}).deferred);
			} else if (options && options.initial) {
				this.addInitializer(function() {
					_.each(self.components, function(component, key) {
						if (component.configuration.engage === 'initialization') {
							component.load({});
							// This will start the component

							// TODO: Needs a better location
							if (component.configuration.layout.region) {
								self.regionManager[component.configuration.layout.region].show(component.getLayoutView());
								component.regionManager.reInitializeRegions();
							}

							component.engage();
						}
					});
				});

			}

		}, this.plugged);

		this.eventManager.on("initialize:after", function(options) {
			if (this.hasLoadingComponent && options.initial) {
				Backbone.history.navigate('welcome');
			}
		}, this.plugged);

		/**
		 * Event to set the modal region for each component
		 */
		this.eventManager.on("initialize:after", function(options) {
			var self = this;
			// if the app as already started, then don't do this
			if (!this.started) {
				_.each(this.components, function(component, key) {
					// set a modal reference from the application region to the
					// component
					component.modal = self.regionManager.modal;
				});
			}
		}, this.plugged);

		// Event used to start up the components
		// This is basically the app/component startup
		this.eventManager.on("view:components:start", function(options) {
			var self = this;
			// Close the loading region, which will dispose of the current view there
			// 
			this.regionManager.loading.close();

			// Loop through each component, call the engage method of
			// the component. This will basically run all of the startup
			// events for the component
			_.each(this.components, function(component, key) {
				if (component.configuration.engage === 'startup') {
					// If the component is using a region manager, then we need to initialize that components regions
					if (component.configuration.layout && component.configuration.layout.region) {
						var renderOptions = component.configuration.layout.options ? component.configuration.layout.options : {};
						// find the region that this component is configured for and show the components layout, that will
						// define the sub regions for that component
						self.regionManager[component.configuration.layout.region].show(component.getLayoutView(), renderOptions);
						// some components might be hidden on startup
						if (component.configuration.layout.render.startup === 'hide') {
							self.regionManager[component.configuration.layout.region].hide();
						}
					}

					component.engage(options);
				}
			});

			// Once all of the components have started up, loop through
			// all of them again (:) and trigger one final event
			// that will run after everything has started up.
			_.each(this.components, function(component, key) {
				if (component.configuration.engage === 'startup') {
					component.eventManager.trigger('start:after');
				}
			});

			// if we are starting up and we don't have a route
			// then set this as the index
			// options might also not be set, if we
			// came from an internal event to start up
			if (!options || !options.route) {
				Backbone.history.navigate('index');
			}

			// Now add to the application a flag, indicating that
			// we have loaded all of the components
			this.componentsEngaged = true;

		}, this.plugged);

		this.eventManager.on("view:components:update", function(options) {
			var self = this;
			// Close the loading region, which will dispose of the current view there
			// TODO: This is happening every time, even though it doesn't need to
			this.regionManager.loading.close();

			// There is an issue here, if we come from the welcome page, where all of the components
			// regions are destroyed, then we use the back button to a route page, we basically need
			// to start everything back over
			_.each(this.components, function(component, key) {
				// if there is no route, for this component, re-initialize the regions
				if (!options || !options.route || (options.index && options.route)) {
					if (component.configuration.layout && component.configuration.layout.region) {
						var renderOptions = component.configuration.layout.options ? component.configuration.layout.options : {};

						self.regionManager[component.configuration.layout.region].show(component.getLayoutView(), renderOptions);
						if (component.configuration.layout.render.startup === 'hide') {
							self.regionManager[component.configuration.layout.region].hide();
						}
						component.regionManager.reInitializeRegions();
					}
				}

				// this will only happen if we went to the welcome page, then used
				// the back button to go to a page with a route, other than the index page
				if (options.index && options.route && options.route.id !== component.id) {
					// call update on each component
					component.update({
						route : false
					});
				} else {
					// call update on each component
					component.update(options);
				}

			});
			// do the same thing as start but call an update
			// after event, also make sure
			// we are passing in the appropriate route information
			// this happens AFTER all components have updated.
			_.each(this.components, function(component, key) {
				component.eventManager.trigger('update:after', options);
			});

		}, this.plugged);

		this.eventManager.on("start", function(options) {
			var self = this;
			// set the plugins right here
			// When we move this. Since they are created already we will just need to plugify after
			// the components are loaded
			if (!this.started) {
				_.each(this.components, function(component, key) {
					// loop through
					_.each(self.plugins, function(plugin) {
						if (plugin.type === 'component' && _.contains(plugin.pluggedIds, component.id) && self.activePlugins.findWhere({
							id : plugin.id
						})) {
							var options = {
								config : component.moduleConfig
							};
							options = _.extend(options, {
								plugged : component,
								eventManager : component.eventManager
							});

							plugin.plugify(options);
							component.plugins.push(plugin);

							// if the plugin has an subscriptions, add them
							if (plugin.subscriptions) {
								messenger.subscribe(plugin.subscriptions, component);
							}

							// if the plugin has to publish any events, add them
							if (plugin.publish) {
								messenger.publish(plugin.publish, component);
							}

						}
					});

				});

			}

		}, this.plugged);

		// At this point we know the initializing of the application
		// should be done...which is the initial fetch of all data
		// Now do something with it
		this.eventManager.on("start", function(options) {
			// At this point we need to know if we have a starting
			// point, if we do then we are going to ignore the loading component
			if (this.hasLoadingComponent && options.initial) {
				var loadingComponent = this.components[this.loadingComponentId];
				loadingComponent.eventManager.trigger('test');
			} else {
				// If the app has started already
				if (this.started) {
					this.eventManager.trigger('view:components:update', options);
				} else {
					this.eventManager.trigger('view:components:start', options);
				}

			}

		}, this.plugged);
	};
	/**
	 * Add a method to the application to add components
	 */
	ComponentPlugin.prototype.addMethods = function() {

		this.plugged['component'] = function(Component, options) {
			var self = this;
			// If the component is for a specific region
			// then grab the application region el and assign it to the layout el
			// this way the component can use that el for it's region manager
			if (options.layout && options.layout.region) {
				options.layout.el = this.options.regions[options.layout.region];
			}

			var component = new Component(options);

			component.navigation = this.navigation;

			this.components[component.id] = component;

			// automatically adding this as an event, better way to handle this?
			// this.eventManager.on("initialize:after", component.start);

			// Now we need to process the global events
			// If it is a standard callback, we will just automatically
			// attach it to the event manager, otherwise we will wrap it in a call
			// to handle updating
			if (component.subscriptions) {
				messenger.subscribe(component.subscriptions, component);
			}

			// Now we need to loop through the triggers and process them
			if (component.publish) {
				messenger.publish(component.publish, component);
			}

			// Common events that are component to component
			messenger.publish({
				'component:view:activated' : 'component:minimize',
				'component:seize' : 'component:hide',
				'component:release' : 'component:show'
			}, component, component.id);

			// internal event, component to itself
			component.eventManager.on('show', function() {
				this.show();
			}, component);

			// internal event, component to itself
			component.eventManager.on('minimize', function() {
				this.minimize();
			}, component);

			// adding this one here because is a component to application
			component.eventManager.on('exit', function() {
				var args = Array.prototype.slice.call(arguments);
				args.unshift('exit');
				self.eventManager.trigger.apply(self.eventManager, args);
			}, component);

			/**
			 * this one will reference the application. This is an event that gets triggered at the component level but really gets handled at the application level.
			 */
			component.eventManager.on('get:batch', function() {
				var args = Array.prototype.slice.call(arguments);

				var items = [];
				_.each(args, function(model) {
					model.trigger('request');
					items.push(model.item);
				});
				// process does not support passing in models yet
				this.proxy.process('component:get', {
					requests : items,
					defer : false
				}).done(function(requestResponses) {
					// at this point loop through
					// each of the collections/models
					// we returned from here
					_.each(requestResponses, function(response) {
						// Trigger the events here, instead of in the proxy
						// might make more sense to move this out of here if
						// it is used else where

						// following close to backbone here, sync comes after reset/change
						if (response instanceof Backbone.Collection) {
							response.trigger('reset');
							response.trigger('sync');
						} else if (response instanceof Backbone.Model) {
							response.trigger('change');
							response.trigger('sync');
						}
					});
				});

			}, this);

			return component;
		};

		// EHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		this.plugged.getRoutes = function() {
			var retVal = [];
			// Add our default generic component route

			// this one is just for workflow item
			retVal.push({
				route : 'view/:component/:page',
				callback : function(component, view, widget) {
					return {
						id : component,
						view : view
					};
				}
			});

			// this one is for workflow item + widget + random shit
			retVal.push({
				route : 'view/:component/:page/:widget(/*stuff)',
				callback : function(component, view, widget) {
					return {
						id : component,
						view : view,
						widget : widget,
						parameters : Array.prototype.slice.call(arguments, 3)
					};
				}
			});

			// Gather all component specific routes and add those
			_.each(this.components, function(component) {
				retVal = retVal.concat(component.routes);
			});

			return retVal;
		};
	};

	return ComponentPlugin;

});