/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'log', 'underscore', 'common/application/application.region', 'jquery.blockUI' ], function(require, log, _, Region) {
	/**
	 * This will be the main region manager for a component.
	 * 
	 * It will define a core template for the component to then attach regions to. The template should contain all of the necessary regions
	 * 
	 */
	var RegionManager = function(options) {
		this.options = options || {};
		// Set the base region type we will be creating
		this.RegionType = Region;
		_.extend(this, options);

		this.initializeRegions();
	};

	RegionManager.prototype = {
		initializeRegions : function() {
			var self = this;

			if (!this.regionManagers) {
				this.regionManagers = {};
			}

			// Process all regions for this region manager
			_.each(this.regions, function(region, name) {
				if (typeof region != 'string' && typeof region.selector != 'string') {
					log.error('Region must be specified as a selector string or an object with selector property ' + name);
				}

				selector = typeof region === 'string' ? region : region.el;
				var regionType = typeof region.regionType === 'undefined' ? self.RegionType : region.regionType;

				var regionManager = new regionType({
					el : selector,
					getEl : function(selector) {
						return $(selector, self.el);
					}
				});

				self.regionManagers[name] = regionManager;
				self[name] = regionManager;
			});

		},
		reInitializeRegions : function() {
			if (this.regionManagers && _.size(this.regionManagers) === 0) {
				this.initializeRegions();
			} else {
				_.each(this.regionManagers, function(region) {
					region.reset();
				});
			}
		},
		close : function() {
			this.closeRegions();
			this.destroyRegions();
			// Not sure this is needed anymore
			// this.closeLayout();
		},
		closeRegions : function() {
			_.each(this.regionManagers, function(manager, name) {
				manager.close();
			});
		},
		destroyRegions : function() {
			var self = this;
			_.each(this.regionManagers, function(manager, name) {
				delete self[name];
			});
			this.regionManagers = {};
		},
		hide : function() {
			$(this.el).hide();
		},
		show : function() {
			$(this.el).show();
			this.maximize();
		},
		// TODO: This is very bad putting this here,
		// either make component specific extending region manager
		// or figure out a better solution
		minimize : function() {
			$('._componentContent', this.el).hide();
			$('.content-header', this.el).hide();
		},
		maximize : function() {
			$('._componentContent', this.el).show();
			$('.content-header', this.el).show();
		},
		block : function(view) {
			var message = null;
			if (_.isObject(view)) {
				message = view.render().el;
			} else if (_.isString(view)) {
				message = view;
			}

			$('._componentContent', this.el).block({
				message : message,
				css : {
					border : '3px solid #333'
				}
			});
		},
		unblock : function() {
			$('._componentContent', this.el).unblock();
		}
	};

	return RegionManager;
});