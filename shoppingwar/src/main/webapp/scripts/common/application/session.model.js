/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'require', 'backbone', 'common/backbone/backbone.amplify', ], function(require, Backbone, SessionStorage) {

	// This will pull in Backbone.proxy and attach taht object to this model by default

	var SessionModel = Backbone.Model.extend({
		storageEngine : 'Session',
		initialize : function(options) {
			// for the session model, upon change, force it
			// to save
			this.bind('change', function() {
				this.save();
			});
		},
		clear : function(options) {
			// clear also removes the ID, which in this case we do not want to do
			var id = this.id;
			// forcing silent true here, because we want to call save in here
			// because of id issue
			if (typeof options === 'undefined') {
				options = {};
			}

			options = _.extend(options, {
				silent : true
			});
			var retVal = Backbone.Model.prototype.clear.call(this, options);
			this.id = id;
			// now save should do an update, instead of a create :)
			this.save();
			return retVal;
		}
	});

	return SessionModel;
});