/**
 * Copyright (C) 2012 Connecture
 * 
 */
define([ 'module' ], function(module) {

	var Navigation = function() {
		// array for now
		this.views = [];
	};
	/**
	 * This will most likely never extend anything else...famous last words :)
	 */
	Navigation.prototype = {
		addView : function(View, options) {
			this.views.push({
				View : View,
				options : options
			});
		}
	};

	return Navigation;

});