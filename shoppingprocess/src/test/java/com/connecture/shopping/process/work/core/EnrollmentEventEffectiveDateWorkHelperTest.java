package com.connecture.shopping.process.work.core;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.work.core.EnrollmentEventEffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class EnrollmentEventEffectiveDateWorkHelperTest
{
  @Test
  public void testGetEffectiveDate() throws Exception
  {
    JSONObject accountObject = new JSONObject();
    EnrollmentEventRulesEngineBean mockBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    EnrollmentEventDomainModel model = new EnrollmentEventDomainModel();
    Mockito.when(mockBean.executeRule()).thenReturn(model);

    // No renewingEffectiveDate, no HCR effective date
    String result = EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(accountObject, mockBean);
    Assert.assertNull(result);

    final String HCR_EFFECTIVE_DATE = "9/9/9999";
    model.getApplication().setEffectiveDate(HCR_EFFECTIVE_DATE);
    
    // No renewingEffectiveDate, so we expect to get the HCR effective date
    result = EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(accountObject, mockBean);
    Assert.assertNotNull(result);
    Assert.assertEquals(HCR_EFFECTIVE_DATE, result);

    final String RENEWING_EFFECTIVE_DATE = "1/1/1970";
    accountObject.accumulate("renewingEffectiveDate", RENEWING_EFFECTIVE_DATE);
    
    // There is renewingEffectiveDate, so we should get that
    result = EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(accountObject, mockBean);
    Assert.assertNotNull(result);
    Assert.assertEquals(RENEWING_EFFECTIVE_DATE, result);    
  }
}
