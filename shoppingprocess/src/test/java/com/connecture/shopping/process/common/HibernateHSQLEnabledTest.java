package com.connecture.shopping.process.common;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.HSQLDialect;
import org.junit.After;
import org.junit.Before;

public abstract class HibernateHSQLEnabledTest
{
  protected SessionFactory sessionFactory;
  protected Session session = null;
  
  @Before
  public void before()
  {
    Configuration config = buildConfig();

    sessionFactory = config.buildSessionFactory();
    session = sessionFactory.openSession();
  }
  
  @After
  public void after()
  {
    if(session != null)
    {
      session.close();
    }
    if(sessionFactory != null)
    {
      sessionFactory.close();
    }
  }
  
  /**
   * Override this to customize the hibernate config for your test.  In most
   * cases, you'll want to override this, call super.buildConfig(), and then
   * modify the returned config via setProperty
   * 
   * @return The hibernate configuration
   */
  protected Configuration buildConfig()
  {
    AnnotationConfiguration config = new AnnotationConfiguration();
    for(Class<?> c : getTestedEntities())
    {
      config.addAnnotatedClass(c);
    }
    config.setProperty(Environment.DIALECT, HSQLDialect.class.getName());
    config.setProperty(Environment.DRIVER, "org.hsqldb.jdbcDriver");
    config.setProperty(Environment.URL, "jdbc:hsqldb:file:../shoppingwar/shoppingDB;shutdown=true;hsqldb.lock_file=false");
    config.setProperty(Environment.USER, "sa");
    
    config.setProperty(Environment.POOL_SIZE, "10");
    config.setProperty(Environment.SHOW_SQL, "true");
    config.setProperty(Environment.FORMAT_SQL, "true");
    config.setProperty(Environment.ORDER_UPDATES, "false");
    config.setProperty(Environment.STATEMENT_BATCH_SIZE, "0");
    config.setProperty(Environment.DEFAULT_BATCH_FETCH_SIZE, "40");
    config.setProperty(Environment.HBM2DDL_AUTO, "update");
    
    config.setProperty(Environment.CACHE_PROVIDER, "org.hibernate.cache.NoCacheProvider");
    
    return config;
  }
  
  protected abstract Class<?>[] getTestedEntities();
}
