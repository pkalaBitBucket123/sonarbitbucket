package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathAction;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathAddAction;

public class JSONPathAddActionTest
{
  @Test
  public void isValidFor()
  {
    Assert.assertTrue(new JSONPathAddAction().isValidFor(JSONPathAction.PathState.MISSING));
    Assert.assertTrue(new JSONPathAddAction().isValidFor(JSONPathAction.PathState.MISSING_SUBPATH));
    Assert.assertFalse(new JSONPathAddAction().isValidFor(JSONPathAction.PathState.FOUND));
  }

  @Test
  public void perform() throws JSONException
  {
    JSONObject jsonObject = new JSONObject();
    JSONPathAction action = new JSONPathAddAction();

    Assert.assertFalse(action.perform("name", jsonObject, JSONPathAction.PathState.FOUND));
    Assert.assertFalse(jsonObject.has("name"));

    Assert.assertTrue(action.perform("name", jsonObject, JSONPathAction.PathState.MISSING));
    Assert.assertTrue(jsonObject.has("name"));

    Assert.assertTrue(action.perform("name", jsonObject, JSONPathAction.PathState.MISSING_SUBPATH));
    Assert.assertTrue(jsonObject.has("name"));

    Assert.assertTrue(action.perform("first", jsonObject.getJSONObject("name"),
      JSONPathAction.PathState.MISSING));
    Assert.assertTrue(jsonObject.getJSONObject("name").has("first"));

    action = new JSONPathAddAction("tester");
    Assert.assertTrue(action.perform("first", jsonObject.getJSONObject("name"),
      JSONPathAction.PathState.MISSING_SUBPATH));
    Assert.assertTrue(jsonObject.getJSONObject("name").has("first"));
    jsonObject.getJSONObject("name").getJSONObject("first");

    Assert.assertTrue(action.perform("first", jsonObject.getJSONObject("name"),
      JSONPathAction.PathState.MISSING));
    Assert.assertEquals("tester", jsonObject.getJSONObject("name").getString("first"));
  }
}
