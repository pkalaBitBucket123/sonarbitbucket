package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPAddConverterTest
{
  private static Logger LOG = Logger.getLogger(AVPAddConverterTest.class);

  @Test
  public void validateBadAddConverter() throws JSONException
  {
    String json = "{ type : \"add\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals("Errors expected", 2, errors.size());
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Expected errors detected:\n" + errors);
    }
  }

  @Test
  public void validateAddConverter() throws JSONException
  {
    String json = "{ type : \"add\", path : \"questions.value.type\", defaultValue : \"value\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }

  @Test
  public void createAddConverter() throws JSONException
  {
    String json = "{ type : \"add\", path : \"questions.value.type\", defaultValue : \"value\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    AVPAddConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    Assert.assertEquals(AVPConverterType.ADD, converter.getType());
    Assert.assertEquals("questions.value.type", converter.getPath());
    Assert.assertEquals("value", converter.getDefaultValue());
    Assert.assertEquals("reason", converter.getReason());
  }

  @Test
  public void addConversion() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPAddConverterTest.class, "testAddConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPAddConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    jsonObject = new JSONObject(JSONFileUtils.readJSONData(AVPAddConverterTest.class,
      "addConversion.json"));
    converter.apply(jsonObject);

    Assert.assertEquals(2, JSONPathUtil.count(jsonObject, converter.getPath()));
  }
}
