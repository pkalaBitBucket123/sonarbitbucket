package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.individual.QuoteWork;

public class QuoteWorkProviderTest
{

  @Test
  public void testGetWork() throws JSONException
  {
    final QuoteWork work = new QuoteWork();
    QuoteWorkProvider provider = new QuoteWorkProvider() {
      @Override
      public QuoteWork createWork()
      {
        return work;
      }
    };
    QuoteWork resultWork = provider.getWork("A", "B", Method.GET, null, null);
    Assert.assertEquals(work, resultWork);
    Assert.assertEquals("A", resultWork.getTransactionId());
    
    work.setTransactionId(null);
    resultWork = provider.getWork("A", "B", Method.UPDATE, null, new JSONObject("{ quote : {} }"));
    Assert.assertEquals("{}", resultWork.getValue().toString());
    Assert.assertEquals("A", resultWork.getTransactionId());
    
    work.setTransactionId(null);
    resultWork = provider.getWork("A", "B", Method.CREATE, null, null);
    Assert.assertNull(resultWork.getTransactionId());
  }
}
