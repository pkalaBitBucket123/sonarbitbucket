package com.connecture.shopping.process.work.context;

import org.junit.Test;

import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.context.ContextWorkFactoryImplTest.FactoryWorkProviderSetter;
import com.connecture.shopping.process.work.core.AccountWork;
import com.connecture.shopping.process.work.core.ActorWork;
import com.connecture.shopping.process.work.core.ApplicationConfigWork;
import com.connecture.shopping.process.work.core.ConsumerDemographicsWork;
import com.connecture.shopping.process.work.core.CostEstimatorCostsWork;
import com.connecture.shopping.process.work.core.CostEstimatorWork;
import com.connecture.shopping.process.work.core.ProviderSearchWork;
import com.connecture.shopping.process.work.core.RulesEngineWork;
import com.connecture.shopping.process.work.core.ValidZipCodeCountyWork;
import com.connecture.shopping.process.work.core.provider.AccountWorkProvider;
import com.connecture.shopping.process.work.core.provider.ActorWorkProvider;
import com.connecture.shopping.process.work.core.provider.ApplicationConfigWorkProvider;
import com.connecture.shopping.process.work.core.provider.ConsumerDemographicsWorkProvider;
import com.connecture.shopping.process.work.core.provider.CostEstimatorCostsWorkProvider;
import com.connecture.shopping.process.work.core.provider.CostEstimatorWorkProvider;
import com.connecture.shopping.process.work.core.provider.ProviderSearchWorkProvider;
import com.connecture.shopping.process.work.core.provider.RulesEngineWorkProvider;
import com.connecture.shopping.process.work.core.provider.ValidZipCodeCountyWorkProvider;
import com.connecture.shopping.process.work.individual.DemographicsValidationWork;
import com.connecture.shopping.process.work.individual.IFPPovertyIncomeLevelWork;
import com.connecture.shopping.process.work.individual.IFPSpecialEnrollmentEffectiveDateWork;
import com.connecture.shopping.process.work.individual.IFPSpecialEnrollmentEventDateValidationWork;
import com.connecture.shopping.process.work.individual.QuoteWork;
import com.connecture.shopping.process.work.individual.RatingPlanWork;
import com.connecture.shopping.process.work.individual.provider.DemographicsValidationWorkProvider;
import com.connecture.shopping.process.work.individual.provider.IFPPovertyIncomeLevelWorkProvider;
import com.connecture.shopping.process.work.individual.provider.IFPSpecialEnrollmentEffectiveDateWorkProvider;
import com.connecture.shopping.process.work.individual.provider.IFPSpecialEnrollmentEventDateValidationWorkProvider;
import com.connecture.shopping.process.work.individual.provider.QuoteWorkProvider;
import com.connecture.shopping.process.work.individual.provider.RatingPlanWorkProvider;

public class IndividualWorkFactoryTest
{
  @Test
  public void testGetWork() throws Exception
  {
    final IndividualWorkFactory factory = new IndividualWorkFactory();

    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.ACCOUNT, AccountWorkProvider.class, AccountWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setAccountWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.QUOTE, QuoteWorkProvider.class, QuoteWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setQuoteWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.PRODUCTS, RatingPlanWorkProvider.class, RatingPlanWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setRatingPlanWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.COST_ESTIMATOR_CONFIG, CostEstimatorWorkProvider.class, CostEstimatorWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setCostEstimatorWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.COST_ESTIMATOR_COSTS, CostEstimatorCostsWorkProvider.class, CostEstimatorCostsWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setCostEstimatorCostsWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.RULES_ENGINE, RulesEngineWorkProvider.class, RulesEngineWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setRulesEngineWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.POVERTY_INCOME_LEVEL, IFPPovertyIncomeLevelWorkProvider.class, IFPPovertyIncomeLevelWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setPovertyIncomeLevelWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.PROVIDER_SEARCH, ProviderSearchWorkProvider.class, ProviderSearchWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setProviderSearchWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.APPLICATION_CONFIG, ApplicationConfigWorkProvider.class, ApplicationConfigWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setApplicationConfigWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.ACTOR, ActorWorkProvider.class, ActorWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setActorWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.DEMOGRAPHICS_VALIDATION, DemographicsValidationWorkProvider.class, DemographicsValidationWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setDemographicsValidationWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.CONSUMER_DEMOGRAPHICS, ConsumerDemographicsWorkProvider.class, ConsumerDemographicsWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setConsumerDemographicsWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.VALID_ZIP_CODE_COUNTY_DATA, ValidZipCodeCountyWorkProvider.class, ValidZipCodeCountyWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setValidZipCodeCountyWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.SPECIAL_ENROLLMENT_EFFECTIVE_DATE, IFPSpecialEnrollmentEffectiveDateWorkProvider.class, IFPSpecialEnrollmentEffectiveDateWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setIfpSpecialEnrollmentEffectiveDateWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.SPECIAL_ENROLLMENT_EVENT_DATE_VALIDATION, IFPSpecialEnrollmentEventDateValidationWorkProvider.class, IFPSpecialEnrollmentEventDateValidationWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setIfpSpecialEnrollmentEventDateValidationWorkProvider(workProvider);
      }
    });
  }
}
