package com.connecture.shopping.process.test;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;

public class AssertJSON
{
  public static void assertNullProperties(JSONObject jsonObject, String... properties)
    throws JSONException
  {
    for (String property : properties)
    {
      Assert.assertEquals(JSONObject.NULL, jsonObject.get(property));
    }
  }

  public static void assertNotNullProperties(JSONObject jsonObject, String... properties)
    throws JSONException
  {
    for (String property : properties)
    {
      Assert.assertFalse(JSONObject.NULL.equals(jsonObject.get(property)));
    }
  }

  public static void assertHasProperties(JSONObject jsonObject, String... properties)
    throws JSONException
  {
    for (String property : properties)
    {
      Assert.assertTrue(jsonObject.has(property));
    }
  }

  public static void assertDoesNotHaveProperties(JSONObject jsonObject, String... properties)
    throws JSONException
  {
    for (String property : properties)
    {
      Assert.assertFalse(jsonObject.has(property));
    }
  }

  public static void assertTrueBooleanProperties(JSONObject jsonObject, String... properties)
    throws JSONException
  {
    for (String property : properties)
    {
      Assert.assertTrue(jsonObject.getBoolean(property));
    }
  }

  public static void assertFalseBooleanProperties(JSONObject jsonObject, String... properties)
    throws JSONException
  {
    for (String property : properties)
    {
      Assert.assertFalse(jsonObject.getBoolean(property));
    }
  }
}
