package com.connecture.shopping.process.work.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.camel.request.PlanAdvisorConfigRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.AnswerData;
import com.connecture.shopping.process.service.data.FilterData;
import com.connecture.shopping.process.service.data.PlanAdvisorData;
import com.connecture.shopping.process.service.data.PlanAdvisorDataConstants.Operators;
import com.connecture.shopping.process.service.data.PlanAdvisorDataConstants.PlanEvaulationType;
import com.connecture.shopping.process.service.data.PlanEvaluationAnswerData;
import com.connecture.shopping.process.service.data.PlanEvaluationData;
import com.connecture.shopping.process.service.data.PlanEvaluationProductData;
import com.connecture.shopping.process.service.data.QuestionData;
import com.connecture.shopping.process.service.data.QuestionnaireData;
import com.connecture.shopping.process.service.data.WorkflowData;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.test.AssertJSON;
import com.connecture.shopping.process.work.core.ApplicationConfigWork;

public class ApplicationConfigWorkTest
{
  @Test
  public void testGetWithAccount() throws Exception
  {
    ApplicationConfigWork work = newApplicationConfigWorkForTesting();
    DataCache dataCache = Mockito.mock(DataCache.class);
    work.setDataCache(dataCache);
    PlanAdvisorConfigRequest planAdvisorConfigRequest = Mockito
      .mock(PlanAdvisorConfigRequest.class);
    work.setPlanAdvisorConfigRequest(planAdvisorConfigRequest);
    Account account = Mockito.mock(Account.class);
    JSONObject accountJSONObject = new JSONObject();
    accountJSONObject.put("effectiveDate", "11/15/2015");
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSONObject);
    work.setAccount(account);
    work.get();
  }

  private ApplicationConfigWork newApplicationConfigWorkForTesting()
  {
    final PlanAdvisorData planAdvisorData = new PlanAdvisorData();
    ApplicationConfigWork work = new ApplicationConfigWork()
      {
        @Override
        public PlanAdvisorData getPlanAdvisorData()
        {
          return planAdvisorData;
        }
      };

    JSONObject auxShoppingConfigJSON = new JSONObject();
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());

    List<WorkflowData> workflows = new ArrayList<WorkflowData>();
    planAdvisorData.setWorkflows(workflows);

    List<QuestionData> questions = new ArrayList<QuestionData>();
    planAdvisorData.setQuestions(questions);

    List<QuestionnaireData> questionnaires = new ArrayList<QuestionnaireData>();
    planAdvisorData.setQuestionnaires(questionnaires);

    List<FilterData> filters = new ArrayList<FilterData>();
    planAdvisorData.setFilters(filters);

    List<PlanEvaluationData> planEvaluations = new ArrayList<PlanEvaluationData>();
    planAdvisorData.setPlanEvaluations(planEvaluations);

    Account account = Mockito.mock(Account.class);
    JSONObject accountJSONObject = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSONObject);
    work.setAccount(account);

    return work;
  }

  @Test
  public void testProcessData() throws Exception
  {
    ApplicationConfigWork work = newApplicationConfigWorkForTesting();
    PlanAdvisorData planAdvisorData = work.getPlanAdvisorData();

    assertBadAuxShoppingConfiguration(work);

    assertBasicApplicationConfig(work);

    WorkflowData workflowData = new WorkflowData();
    planAdvisorData.getWorkflows().add(workflowData);
    List<String> questionRefIds = new ArrayList<String>();
    questionRefIds.add("Q1");
    workflowData.setQuestionRefIds(questionRefIds);

    QuestionnaireData questionnaire = new QuestionnaireData();
    questionnaire.setQuestionnaireRefId("QN1");
    planAdvisorData.getQuestionnaires().add(questionnaire);

    JSONArray widgetsJSON = new JSONArray();
    JSONObject auxShoppingConfigJSON = new JSONObject(work.getAuxShoppingConfig());
    auxShoppingConfigJSON.put("widgets", widgetsJSON);
    JSONObject methodWidgetJSON = new JSONObject();
    methodWidgetJSON.put("widgetId", "Method");
    JSONObject methodWidgetConfigJSON = new JSONObject();
    methodWidgetJSON.put("config", methodWidgetConfigJSON);
    widgetsJSON.put(methodWidgetJSON);
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());

    QuestionData question = new QuestionData();
    question.setQuestionRefId("Q1");
    question.setQuestionnaireRefId("QN1");
    question.setEvaluationType(PlanEvaulationType.SCORING);
    planAdvisorData.getQuestions().add(question);

    JSONObject results = work.processData();

    Assert.assertTrue(results.has("widgets"));
    JSONObject resultMethodWidget = results.getJSONArray("widgets").getJSONObject(0);
    Assert.assertEquals("Method", resultMethodWidget.getString("widgetId"));
    Assert.assertEquals("Q1", resultMethodWidget.getJSONObject("config").getJSONArray("workflows")
      .getJSONObject(0).getJSONArray("questionRefIds").getString(0));
    JSONObject resultQuestionWidget = results.getJSONArray("widgets").getJSONObject(1);
    Assert.assertEquals("Q1", resultQuestionWidget.getString("widgetId"));
    JSONObject resultQuestionWidgetConfig = resultQuestionWidget.getJSONObject("config");
    Assert.assertEquals("QN1", resultQuestionWidgetConfig.getString("questionnaireRefId"));
    Assert.assertEquals("Q1", resultQuestionWidgetConfig.getString("questionRefId"));
    Assert.assertEquals(PlanEvaulationType.SCORING,
      resultQuestionWidgetConfig.getString("evaluationType"));
    AssertJSON
      .assertNullProperties(resultQuestionWidgetConfig, "dataTagKey", "weight", "text",
        "filterRefId", "title", "classOverride", "displayType", "educationalContent", "helpText",
        "helpType", "widgetId", "required", "memberLevelQuestion", "minimumAnswers",
        "maximumAnswers");
    Assert.assertEquals(0, resultQuestionWidgetConfig.getJSONArray("answers").length());
    Assert.assertEquals(ApplicationConfigWork.PLAN_ADVISOR_CONFIGURED_QUESTION_WIDGET_KEY,
      resultQuestionWidget.getString("widgetType"));

    question.setEvaluationType(PlanEvaulationType.DATA_TAG);
    question.setDataTagKey("T1");
    question.setWeight("100");
    List<AnswerData> answers = new ArrayList<AnswerData>();
    AnswerData answer = new AnswerData();
    answer.setAnswerId("A1");
    answer.setDataTagValue("Value");
    answer.setDataTagOperator(Operators.EQUALS);
    answer.setText("This is jeopardy");
    answer.setSummaryText("Isto es Jeopardy");
    answers.add(answer);
    question.setAnswers(answers);

    results = work.processData();
    resultQuestionWidget = results.getJSONArray("widgets").getJSONObject(1);
    resultQuestionWidgetConfig = resultQuestionWidget.getJSONObject("config");
    Assert.assertEquals(PlanEvaulationType.DATA_TAG,
      resultQuestionWidgetConfig.getString("evaluationType"));
    Assert.assertEquals("T1", resultQuestionWidgetConfig.getString("dataTagKey"));
    Assert.assertEquals("100", resultQuestionWidgetConfig.getString("weight"));
    JSONArray resultQuestionAnswers = resultQuestionWidgetConfig.getJSONArray("answers");
    Assert.assertEquals(1, resultQuestionAnswers.length());
    JSONObject resultQuestionAnswer = resultQuestionAnswers.getJSONObject(0);
    Assert.assertEquals("A1", resultQuestionAnswer.getString("answerId"));
    Assert.assertEquals("Value", resultQuestionAnswer.getString("dataTagValue"));
    Assert.assertEquals(Operators.EQUALS, resultQuestionAnswer.getString("dataTagOperator"));
    Assert.assertEquals("This is jeopardy", resultQuestionAnswer.getString("text"));
    Assert.assertEquals("Isto es Jeopardy", resultQuestionAnswer.getString("summaryText"));
    AssertJSON.assertNullProperties(resultQuestionAnswer, "classOverride", "helpText", "helpType",
      "filterOptionRefId");

    JSONArray resultQuestionEvaluations = results.getJSONArray("questionEvaluations");
    JSONObject resultQuestionEvaluation = resultQuestionEvaluations.getJSONObject(0);
    Assert.assertEquals("T1", resultQuestionEvaluation.getString("key"));
    Assert.assertEquals(PlanEvaulationType.DATA_TAG, resultQuestionEvaluation.getString("type"));
    Assert.assertEquals("Q1", resultQuestionEvaluation.getString("questionRefId"));
    JSONArray resultQuestionEvaluationAnswers = resultQuestionEvaluation.getJSONArray("answers");
    Assert.assertEquals(1, resultQuestionAnswers.length());
    JSONObject resultQuestionEvaluationAnswer = resultQuestionEvaluationAnswers.getJSONObject(0);
    Assert.assertEquals("A1", resultQuestionEvaluationAnswer.getString("answerRefId"));
    Assert.assertEquals("Value", resultQuestionEvaluationAnswer.getString("value"));
    Assert.assertEquals(Operators.EQUALS, resultQuestionEvaluationAnswer.getString("operator"));
    Assert.assertEquals("This is jeopardy", resultQuestionEvaluationAnswer.getString("text"));
    Assert
      .assertEquals("Isto es Jeopardy", resultQuestionEvaluationAnswer.getString("summaryText"));
    Assert.assertEquals("100", resultQuestionEvaluationAnswer.getString("score"));

    List<PlanEvaluationData> planEvaluations = planAdvisorData.getPlanEvaluations();
    PlanEvaluationData planEvaluation = new PlanEvaluationData();
    planEvaluation.setQuestionType(PlanEvaulationType.CONFIGURATION);
    planEvaluation.setQuestionRefId("Q1");
    planEvaluation.setQuestionWeight(100l);
    planEvaluations.add(planEvaluation);

    List<PlanEvaluationAnswerData> planEvaluationAnswers = new ArrayList<PlanEvaluationAnswerData>();
    PlanEvaluationAnswerData planEvaluationAnswer = new PlanEvaluationAnswerData();
    planEvaluationAnswer.setAnswerRefId("A1");
    List<PlanEvaluationProductData> planEvaluationProducts = new ArrayList<PlanEvaluationProductData>();
    PlanEvaluationProductData planEvaluationProductData = new PlanEvaluationProductData();
    planEvaluationProductData.setProductId("P 1");
    planEvaluationProducts.add(planEvaluationProductData);
    planEvaluationAnswer.setProducts(planEvaluationProducts);
    planEvaluationAnswers.add(planEvaluationAnswer);
    planEvaluation.setAnswers(planEvaluationAnswers);

    results = work.processData();
    resultQuestionEvaluations = results.getJSONArray("questionEvaluations");
    resultQuestionEvaluation = resultQuestionEvaluations.getJSONObject(1);
    Assert.assertEquals("id", resultQuestionEvaluation.getString("key"));
    Assert.assertEquals(PlanEvaulationType.CONFIGURATION,
      resultQuestionEvaluation.getString("type"));
    Assert.assertEquals("Q1", resultQuestionEvaluation.getString("questionRefId"));
    resultQuestionEvaluationAnswers = resultQuestionEvaluation.getJSONArray("answers");
    resultQuestionEvaluationAnswer = resultQuestionEvaluationAnswers.getJSONObject(0);
    Assert.assertEquals("A1", resultQuestionEvaluationAnswer.getString("answerRefId"));
    JSONArray resultQuestionEvaluationAnswerValues = resultQuestionEvaluationAnswer
      .getJSONArray("value");
    JSONObject resultQuestionEvaluationAnswerValue = resultQuestionEvaluationAnswerValues
      .getJSONObject(0);
    Assert.assertEquals("P 1", resultQuestionEvaluationAnswerValue.getString("value"));
    Assert.assertEquals("0", resultQuestionEvaluationAnswerValue.getString("score"));
    Assert.assertEquals(Operators.EQUALS, resultQuestionEvaluationAnswer.getString("operator"));
    Assert.assertEquals("This is jeopardy", resultQuestionEvaluationAnswer.getString("text"));
    Assert
      .assertEquals("Isto es Jeopardy", resultQuestionEvaluationAnswer.getString("summaryText"));
    Assert.assertEquals("100", resultQuestionEvaluationAnswer.getString("score"));
  }

  private void assertBasicApplicationConfig(ApplicationConfigWork work)
    throws Exception, JSONException
  {
    JSONObject applicationJSON;
    work.setMessagesURL("True");
    work.setMyAccountURL("True");
    work.setRosterURL("True");
    work.setSbcBaseURL("True");
    work.setBrochureBaseURL("True");
    work.setEmailFormPhoneVisible("True");
    work.setEmailFormPhoneRequired("True");
    work.setEmailFormFirstNameVisible("True");
    work.setEmailFormFirstNameRequired("True");
    work.setEmailFormLastNameVisible("True");
    work.setEmailFormLastNameRequired("True");

    JSONObject results = work.processData();

    applicationJSON = results.getJSONObject("applicationConfig");
    AssertJSON.assertNotNullProperties(applicationJSON, "messages-url", "my-account-url",
      "roster-url", "sbc-base-url", "brochure-base-url");

    AssertJSON.assertTrueBooleanProperties(applicationJSON, "emailFormPhoneVisible",
      "emailFormPhoneRequired", "emailFormFirstNameVisible", "emailFormFirstNameRequired",
      "emailFormLastNameVisible", "emailFormLastNameRequired");
  }

  private void assertBadAuxShoppingConfiguration(ApplicationConfigWork work)
    throws Exception, JSONException
  {
    assertProcessDataForJSONException(work, "JSONObject[\"workflows\"] not found.");

    JSONObject workflowsJSON = new JSONObject();
    JSONObject auxShoppingConfigJSON = new JSONObject(work.getAuxShoppingConfig());
    auxShoppingConfigJSON.put("workflows", workflowsJSON);
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());
    assertProcessDataForJSONException(work, "workflow definitions missing");

    JSONObject workflowTestJSON = new JSONObject("{}");
    workflowsJSON.put("Test", workflowTestJSON);
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());
    assertProcessDataForJSONException(work, "JSONObject[\"criteria\"] not found.");

    JSONArray criteriaTestJSON = new JSONArray();
    workflowTestJSON.put("criteria", criteriaTestJSON);
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());
    assertProcessDataForJSONException(work, "JSONObject[\"workflowItems\"] not found.");

    JSONArray workflowItemsJSON = new JSONArray();
    auxShoppingConfigJSON.put("workflowItems", workflowItemsJSON);
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());
    
    auxShoppingConfigJSON.put("plugins", new JSONArray());
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());

    JSONObject results = work.processData();

    JSONObject applicationJSON = results.getJSONObject("applicationConfig");
    AssertJSON.assertDoesNotHaveProperties(applicationJSON, "messages-url", "my-account-url",
      "roster-url", "sbc-base-url", "brochure-base-url");

    AssertJSON.assertFalseBooleanProperties(applicationJSON, "emailFormPhoneVisible",
      "emailFormPhoneRequired", "emailFormFirstNameVisible", "emailFormFirstNameRequired",
      "emailFormLastNameVisible", "emailFormLastNameRequired");

    Assert.assertTrue(0 == results.getJSONArray("workflowItems").length());
    Assert.assertTrue(null == JSONObject.getNames(results.getJSONObject("filters")));
    Assert.assertTrue(0 == results.getJSONArray("questionEvaluations").length());
  }

  public static void assertProcessDataForJSONException(ApplicationConfigWork work, String message)
    throws Exception
  {
    try
    {
      work.processData();
      Assert.fail("Expected JSONException due to " + message);
    }
    catch (JSONException e)
    {
      Assert.assertTrue(message.equals(e.getMessage()));
    }
  }

  @Test
  public void testGetWidget() throws JSONException
  {
    JSONObject jsonObject = new JSONObject();
    String widgetId = "1";

    ApplicationConfigWork work = new ApplicationConfigWork();
    JSONObject widgetObject = work.getWidget(jsonObject, widgetId);
    Assert.assertNull(widgetObject);

    widgetObject = new JSONObject("{ widgetId : " + widgetId + ", value : 0 }");
    work.addWidget(jsonObject, widgetObject);

    widgetObject = work.getWidget(jsonObject, widgetId);

    Assert.assertNotNull(widgetObject);
    Assert.assertEquals(0, widgetObject.getInt("value"));
  }

  @Test
  public void testGetWidgets() throws JSONException
  {
    JSONObject jsonObject = new JSONObject();
    ApplicationConfigWork work = new ApplicationConfigWork();
    JSONArray widgets = work.getOrCreateWidgets(jsonObject);
    Assert.assertNotNull(widgets);
    Assert.assertEquals(0, widgets.length());
    AssertJSON.assertHasProperties(jsonObject, ApplicationConfigWork.WIDGETS_KEY);

    widgets.put(new JSONObject("{ widgetId : 1, value : 0 }"));

    widgets = work.getOrCreateWidgets(jsonObject);
    Assert.assertEquals(1, widgets.length());
    AssertJSON.assertHasProperties(jsonObject, ApplicationConfigWork.WIDGETS_KEY);
  }

  @Test
  public void testAddWidget() throws JSONException
  {
    JSONObject jsonObject = new JSONObject();
    ApplicationConfigWork work = new ApplicationConfigWork();
    work.addWidget(jsonObject, new JSONObject("{ widgetId : 1, value : 0 }"));
    JSONArray widgets = work.getOrCreateWidgets(jsonObject);
    Assert.assertEquals(1, widgets.length());
    AssertJSON.assertHasProperties(jsonObject, ApplicationConfigWork.WIDGETS_KEY);
  }

  @Test
  public void testMisc() throws Exception
  {
    // Placeholder for 100% coverage
    ApplicationConfigWork work = new ApplicationConfigWork();
    work.create();
    work.update();
    work.delete();
  }

  @Test
  public void testWorkflowPicker() throws Exception
  {
    ApplicationConfigWork work = newApplicationConfigWorkForWorkflowPickerTest();
    
    Map<String, String> configCriteria = new HashMap<String, String>();
    work.setConfigCriteria(configCriteria);
    
    configCriteria.put("context", "anonymous");

    JSONObject results = work.processData();
    
    Assert.assertNotNull(results.get("workflow"));
    Assert.assertNotNull(results.getJSONObject("workflow").getJSONArray("criteria"));
    
    // TODO: Add more tests to return different flows based on criteria
   
  }

  // how long can I make this method name but used to generate base test date for
  // workflow picker tests
  private ApplicationConfigWork newApplicationConfigWorkForWorkflowPickerTest()
    throws Exception
  {
    ApplicationConfigWork work = newApplicationConfigWorkForTesting();
    // run these here because they also setup some dummy data for us
    assertBadAuxShoppingConfiguration(work);
    assertBasicApplicationConfig(work);
    
    JSONObject auxShoppingConfigJSON = new JSONObject(work.getAuxShoppingConfig());

    // setup our workflows
    JSONObject workflows = new JSONObject();
   
    auxShoppingConfigJSON.put("workflows", workflows);
    
    JSONObject defaultWorkflow = new JSONObject();
    defaultWorkflow.put("criteria", new JSONArray());
    defaultWorkflow.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCovered","Method", "Questions", "Summary"))));
    workflows.put("default", defaultWorkflow);
    
    JSONObject anonymousWorkflow = new JSONObject();
    JSONArray anonymousCriteria = new JSONArray();
    anonymousCriteria.put(new JSONObject("{label : \"context\",value : \"anonymous\"}"));
    anonymousWorkflow.put("criteria", anonymousCriteria);
    anonymousWorkflow.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCovered","Method","Questions", "Summary"))));
    workflows.put("anonymous", anonymousWorkflow);
    
    JSONObject anonymousFFM = new JSONObject();
    JSONArray anonymousFFMCriteria = new JSONArray();
    anonymousFFMCriteria.put(new JSONObject("{label : \"context\",value : \"anonymous\"}, {label : \"ffmEnabled\",value : \"true\"}"));
    anonymousFFM.put("criteria", anonymousFFMCriteria);
    anonymousFFM.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("Subsidy","WhosCovered","Method","Questions", "Summary"))));
    workflows.put("anonymousFFM", anonymousFFM);   
    
    JSONObject individual = new JSONObject();
    JSONArray individualCriteria = new JSONArray();
    individualCriteria.put(new JSONObject("{label : \"context\",value : \"individual\"}"));
    individual.put("criteria", individualCriteria);
    individual.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCoveredSEOpt","Method","Questions", "Summary"))));
    workflows.put("individual", individual);
    
    JSONObject employee = new JSONObject();
    JSONArray employeeCriteria = new JSONArray();
    employeeCriteria.put(new JSONObject("{label : \"context\",value : \"employee\"}"));
    employee.put("criteria", employeeCriteria);
    employee.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCoveredEE","Method","Questions", "Summary"))));
    workflows.put("employee", employee);
    
    JSONObject specialEnrollment = new JSONObject();
    JSONArray specialEnrollmentCriteria = new JSONArray();
    specialEnrollmentCriteria.put(new JSONObject("{label : \"specialEnrollment\",value : \"true\"}"));
    specialEnrollment.put("criteria", specialEnrollmentCriteria);
    specialEnrollment.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCoveredSE","Method","Questions", "Summary"))));
    workflows.put("specialEnrollment", specialEnrollment);
 
    JSONObject individualFFM = new JSONObject();
    JSONArray individualFFMCriteria = new JSONArray();
    individualFFMCriteria.put(new JSONObject("{label : \"context\",value : \"individual\"}, {label : \"ffmEnabled\",value : \"true\"}"));
    individualFFM.put("criteria", individualFFMCriteria);
    individualFFM.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("Subsidy","WhosCoveredSEOpt","Method","Questions", "Summary"))));
    workflows.put("individualFFM", individualFFM);    
    
    JSONObject specialEnrollmentAnonymous = new JSONObject();
    JSONArray specialEnrollmentAnonymousCriteria = new JSONArray();
    specialEnrollmentAnonymousCriteria.put(new JSONObject("{label : \"context\",value : \"anonymous\"},{label : \"specialEnrollment\",value : \"true\"}"));
    specialEnrollmentAnonymous.put("criteria", specialEnrollmentAnonymousCriteria);
    specialEnrollmentAnonymous.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCoveredSEOpt","Method","Questions", "Summary"))));
    workflows.put("specialEnrollmentAnonymous", specialEnrollmentAnonymous);
    
    JSONObject specialEnrollmentAnonymousFFM = new JSONObject();
    JSONArray specialEnrollmentAnonymousFFMCriteria = new JSONArray();
    specialEnrollmentAnonymousCriteria.put(new JSONObject("{label : \"context\",value : \"anonymous\"},{label : \"specialEnrollment\",value : \"true\"}, {label : \"ffmEnabled\",value : \"true\"}"));
    specialEnrollmentAnonymousFFM.put("criteria", specialEnrollmentAnonymousFFMCriteria);
    specialEnrollmentAnonymousFFM.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("Subsidy","WhosCoveredSEOpt","Method","Questions", "Summary"))));
    workflows.put("specialEnrollmentAnonymousFFM", specialEnrollmentAnonymousFFM);
    
    JSONObject renewal = new JSONObject();
    JSONArray renewalCriteria = new JSONArray();
    renewalCriteria.put(new JSONObject("{label : \"context\",value : \"individual\"},{label : \"renewal\",value : \"true\"}"));
    renewal.put("criteria", renewalCriteria);
    renewal.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("WhosCovered","Method","Questions", "Summary"))));
    workflows.put("renewal", renewal);
    
    JSONObject renewalFFM = new JSONObject();
    JSONArray renewalFFMCriteria = new JSONArray();
    renewalFFMCriteria.put(new JSONObject("{label : \"context\",value : \"individual\"},{label : \"renewal\",value : \"true\"}, {label : \"ffmEnabled\",value : \"true\"}"));
    renewalFFM.put("criteria", renewalFFMCriteria);
    renewalFFM.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("Subsidy","WhosCovered","Method","Questions", "Summary"))));
    workflows.put("rerenewalFFMnewal", renewalFFM);
    
    JSONObject onExchange = new JSONObject();
    JSONArray onExchangeCriteria = new JSONArray();
    onExchangeCriteria.put(new JSONObject("{label : \"context\",value : \"individual\"},{label : \"onExchange\",value : \"true\"}"));
    onExchange.put("criteria", onExchangeCriteria);
    onExchange.put("items", new JSONArray(new ArrayList<String>(Arrays.asList("Subsidy", "WhosCoveredOnExchange", "Method", "Questions", "Summary"))));
    workflows.put("onExchange", onExchange);
    
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());

    return work;

  }
}
