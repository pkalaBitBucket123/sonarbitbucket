package com.connecture.shopping.process.message;

import junit.framework.Assert;

import org.junit.Test;

import com.connecture.model.data.ShoppingLead;

public class MessageUtilsTest
{
  @Test
  public void testConstructShoppingLead()
  {
    ShoppingLead lead = MessageUtils.constructShoppingLead("1", false, false, "A", "B", "1111111");
    Assert.assertEquals("1", lead.getEmail());
    Assert.assertFalse(lead.isCallMe());
    Assert.assertFalse(lead.isFutureEmails());
    Assert.assertEquals("A", lead.getFirstName());
    Assert.assertEquals("B", lead.getLastName());
    Assert.assertEquals("1111111", lead.getPhoneNumber());
  }
}
