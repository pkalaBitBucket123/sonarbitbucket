package com.connecture.shopping.process.camel.request;

import org.junit.Assert;
import org.junit.Test;

public class IACreateAccountRequestTest
{
  @Test
  public void testMisc()
  {
    IACreateAccountRequest request = new IACreateAccountRequest();
    Assert.assertNull(request.getProducer());
    request.setTransactionId("TEST");
    Assert.assertEquals("TEST", request.getRequestHeaders().get(BaseCamelRequest.TRANSACTION_ID_KEY));
  }
}
