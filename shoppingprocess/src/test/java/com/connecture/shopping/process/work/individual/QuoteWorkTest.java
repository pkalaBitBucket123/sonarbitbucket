package com.connecture.shopping.process.work.individual;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingQuotePlanData;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.camel.request.IAToShoppingQuotePlanDataRequest;
import com.connecture.shopping.process.service.data.cache.DataCache;

public class QuoteWorkTest
{
  @Test
  public void testGet() throws Exception
  {
    QuoteWork work = new QuoteWork();
    DataCache dataCache = new DataCache();
    work.setDataCache(dataCache);
    IAToShoppingIndividualDataRequest request = Mockito.mock(IAToShoppingIndividualDataRequest.class);
    work.setIndividualDataRequest(request);
    IAToShoppingData mockData = Mockito.mock(IAToShoppingData.class);
    Mockito.when(request.submitRequest()).thenReturn(mockData);
    work.get();
    Mockito.verify(request, Mockito.times(0)).submitRequest();
    
    work.setTransactionId("TEST");
    work.get();
    Mockito.verify(request, Mockito.times(1)).submitRequest();
  }
  
  @Test
  public void testUpdate() throws Exception
  {
    QuoteWork work = new QuoteWork();
    IAToShoppingQuotePlanDataRequest request = Mockito.mock(IAToShoppingQuotePlanDataRequest.class);
    work.setQuotePlanDataRequest(request);
    IAToShoppingData data = new IAToShoppingData();
    Mockito.when(request.submitRequest()).thenReturn(data);
    work.update();
    Mockito.verify(request, Mockito.times(0)).submitRequest();
    
    JSONObject inputQuote = new JSONObject();
    work.setValue(inputQuote);
    work.update();
    Mockito.verify(request, Mockito.times(1)).submitRequest();
    
    JSONArray planArray = new JSONArray();
    inputQuote.put("plans", planArray);
    Mockito.reset(request);
    work.update();
    Mockito.verify(request, Mockito.times(1)).submitRequest();
    
    JSONObject planObject = new JSONObject();
    planObject.put("planId", 1000l);
    planArray.put(planObject);
    Mockito.reset(request);
    work.update();
    Mockito.verify(request, Mockito.times(1)).submitRequest();
  }
  
  @Test
  public void testProcessData() throws Exception
  {
    QuoteWork work = new QuoteWork();
    JSONObject result = work.processData();
    Assert.assertNotNull(result);
    Assert.assertEquals(0, result.getJSONObject("quote").getJSONArray("plans").length());

    work.setTransactionId("TEST");
    DataCache dataCache = new DataCache();
    work.setDataCache(dataCache);
    IAToShoppingIndividualDataRequest request = Mockito.mock(IAToShoppingIndividualDataRequest.class);
    work.setIndividualDataRequest(request);
    IAToShoppingData data = new IAToShoppingData();
    Mockito.when(request.submitRequest()).thenReturn(data);
    work.get();

    result = work.processData();
    Assert.assertEquals(0, result.getJSONObject("quote").getJSONArray("plans").length());
    
    List<IAToShoppingQuotePlanData> quotePlanData = new ArrayList<IAToShoppingQuotePlanData>();
    data.setQuotePlanData(quotePlanData);
    result = work.processData();
    Assert.assertEquals(0, result.getJSONObject("quote").getJSONArray("plans").length());   
    
    IAToShoppingQuotePlanData quoteData = new IAToShoppingQuotePlanData();
    quoteData.setPlanId(1000l);
    quotePlanData.add(quoteData);
    result = work.processData();
    Assert.assertEquals(1, result.getJSONObject("quote").getJSONArray("plans").length());
    Assert.assertEquals(1000, result.getJSONObject("quote").getJSONArray("plans").getJSONObject(0).getLong("planId"));
  }
}
