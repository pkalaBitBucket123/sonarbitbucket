package com.connecture.shopping.process.service.stub;

import org.junit.Assert;
import org.junit.Test;

public class ServiceStubTest
{
  @Test
  public void testAllYourStubsAreBelongToUs() throws Exception
  {
    Assert.assertNotNull(new ActorServiceStub().getActorForContext(null));
    Assert.assertFalse("{}".equals(new ConsumerDemographicsServiceStub().getConsumerDemographics(null)));
    Assert.assertNotNull(new ConsumerDemographicsClientStub().getInitialMemberData(null, null));
    Assert.assertFalse("{}".equals(new ConsumerProductsServiceStub().getConsumerProducts(null)));
    Assert.assertFalse("{}".equals(new PlanAdvisorServiceStub().getPlanAdvisorData(null)));
    Assert.assertFalse("{}".equals(new ProductServiceStub().getPlanData(null)));
    Assert.assertFalse("{}".equals(new ProviderSearchServiceStub().getProviders(null)));
    Assert.assertFalse("{}".equals(new RatingPlanDataServiceStub().getPlans(null)));
    Assert.assertNotNull(new RatingPlanDataServiceStub().getValidZipCodeCountyData(null).names());
    Assert.assertTrue(new RatingPlanDataServiceStub().validateZipCode("90210"));
    Assert.assertFalse(new RatingPlanDataServiceStub().validateZipCode("40000"));
    Assert.assertEquals("CA", new RatingPlanDataServiceStub().getStateKeyForCountyAndZipCode(null, "90210"));
    Assert.assertEquals("DC", new RatingPlanDataServiceStub().getStateKeyForCountyAndZipCode(null, "20001"));
    Assert.assertEquals("HI", new RatingPlanDataServiceStub().getStateKeyForCountyAndZipCode(null, "96000"));
    Assert.assertEquals("AL", new RatingPlanDataServiceStub().getStateKeyForCountyAndZipCode(null, "99000"));
    Assert.assertEquals("WI", new RatingPlanDataServiceStub().getStateKeyForCountyAndZipCode(null, "53186"));
    Assert.assertFalse("{}".equals(new RulesEngineServiceStub().executeRules(null)));
    Assert.assertNull(new IndividualDataServiceStub().buildTransactionIDDataFromTransactionId(null));
    Assert.assertNotNull(new IndividualDataServiceStub().getDataFromIA(null));
    Assert.assertNull(new IndividualDataServiceStub().getShoppingToIADataForAnonymousEnrollment());
    Assert.assertNotNull(new ProducerDemographicsDataServiceStub().getDemographicsData(null));
  }
}
