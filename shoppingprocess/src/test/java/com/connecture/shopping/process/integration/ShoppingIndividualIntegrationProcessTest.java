package com.connecture.shopping.process.integration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.connecture.model.data.ShoppingLead;
import com.connecture.model.integration.data.ShoppingToIAData;
import com.connecture.shopping.process.camel.request.LeadGenerationRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.common.account.SessionAccountImpl;




@RunWith(MockitoJUnitRunner.class)
public class ShoppingIndividualIntegrationProcessTest
{
 
  @Spy
  @InjectMocks
  ShoppingIndividualIntegrationProcess shoppingIndividualIntegrationProcess; 

  Account sessionAccountImpl;
  JSONObject accountJSON;
  LeadGenerationRequest leadGenReq;

  SimpleDateFormat sdf;
  

  @Before
  public void setUp()
  {
    accountJSON = new JSONObject();
    sessionAccountImpl = Mockito.spy(new SessionAccountImpl());
    shoppingIndividualIntegrationProcess.setSessionAccountImpl(sessionAccountImpl);
    sdf = new SimpleDateFormat("MM/dd/yyyy");
  }
  
  /**
     * @throws Exception
     * Method under test: GetAnonymousEnrollmentData()
     * Scenario: CovType present in accountJSON   
     * Expectation: shoppingData has coverageType set in 
     * as an attribute with value same as accountJSON's covType
     */
  @Test
  public void testGetAnonymousEnrollmentData_WhenCovTypePresent() throws Exception {
    setUpAccountJSON();
    accountJSON.put("covType", "DEN");
    Mockito.doReturn(accountJSON).when(sessionAccountImpl).getAccount(null);
    ShoppingToIAData shoppingData = shoppingIndividualIntegrationProcess.getAnonymousEnrollmentData();
    Assert.assertTrue(  ((ShoppingToIADataExtn)shoppingData).getValues().containsKey("coverageType") );
    Assert.assertTrue(  ((ShoppingToIADataExtn)shoppingData).getValues().containsValue("DEN") );
    
  }
  
  /**
     * @throws Exception
     * Method under test: GetAnonymousEnrollmentData()
     * Scenario: CoverageType present in accountJSON   
     * Expectation: shoppingData has coverageType set in 
     * as an attribute with value same as accountJSON's coverageType
     */
  @Test
  public void testGetAnonymousEnrollmentData_WhenCoverageTypePresent() throws Exception {
    setUpAccountJSON();
    accountJSON.put("coverageType", "DEN");
    Mockito.doReturn(accountJSON).when(sessionAccountImpl).getAccount(null);
    ShoppingToIAData shoppingData = shoppingIndividualIntegrationProcess.getAnonymousEnrollmentData();
    Assert.assertTrue(  ((ShoppingToIADataExtn)shoppingData).getValues().containsKey("coverageType") );
    Assert.assertTrue(  ((ShoppingToIADataExtn)shoppingData).getValues().containsValue("DEN") );
    
  }
  
  /**
     * @throws Exception
     * Method under test: GetAnonymousEnrollmentData()
     * Scenario: Neither CovType nor CoverageType present in accountJSON   
     * Expectation: shoppingData doesn't have coverageType
     * set in as an attribute 
     */
  @Test
  public void testGetAnonymousEnrollmentData_WhenBothCovAndCoverageTypeMissing() throws Exception {
    setUpAccountJSON();
    Mockito.doReturn(accountJSON).when(sessionAccountImpl).getAccount(null);
    ShoppingToIAData shoppingData = shoppingIndividualIntegrationProcess.getAnonymousEnrollmentData();
    Assert.assertTrue( ! ((ShoppingToIADataExtn)shoppingData).getValues().containsKey("coverageType") );
    
  }

  /**
     * @throws Exception
     * Method under test: GetAnonymousEnrollmentData()
     * Scenario: Dental CoverageType, when leadGenReq is null
     * Expectation: shoppingData has different date than the date in shopping lead
     */
  @Test
  public void testGetAnonymousEnrollmentData_DEN_Email_Null() throws Exception {
    setUpAccountJSON();
    
    ShoppingLead shoppingLeadData = new ShoppingLead();
    Date tempDate = new Date();
    shoppingLeadData.setEffectiveDate(tempDate);
    leadGenReq = null;
    shoppingIndividualIntegrationProcess.setLeadGenRequest(leadGenReq);
    Map<String,Object> leadMap = new HashMap<String,Object>();
    leadMap.put("shoppingLeadData", shoppingLeadData);
    accountJSON.put("coverageType", "DEN");
    Mockito.doReturn(accountJSON).when(sessionAccountImpl).getAccount(null);
    ShoppingToIAData shoppingData = shoppingIndividualIntegrationProcess.getAnonymousEnrollmentData();
    Assert.assertNotNull(shoppingData.getEffectiveDate());
    Assert.assertFalse(shoppingData.getEffectiveDate().compareTo(tempDate) == 0);
  }
  /**
     * @throws Exception
     * Method under test: GetAnonymousEnrollmentData()
     * Scenario: CoverageType present in accountJSON   
     * Expectation: shoppingData has coverageType & effectiveDate1 set in 
     * as an attribute with value same as accountJSON's coverageType
     */
  @Test
  public void testGetAnonymousEnrollmentData_WhenCoverageTypeEffectiveDate1Present() throws Exception {
    setUpAccountJSON();
    Date now = new Date();
    accountJSON.put("coverageType", "DEN");
    accountJSON.put("effectiveDate1", sdf.format(now));
    accountJSON.put("effectiveMs", now.getTime());
    Mockito.doReturn(accountJSON).when(sessionAccountImpl).getAccount(null);
    ShoppingToIAData shoppingData = shoppingIndividualIntegrationProcess.getAnonymousEnrollmentData();
    Assert.assertTrue(  ((ShoppingToIADataExtn)shoppingData).getValues().containsKey("coverageType") );
    Assert.assertTrue(  ((ShoppingToIADataExtn)shoppingData).getValues().containsValue("DEN") );
    Assert.assertEquals( sdf.format(now) , sdf.format(shoppingData.getEffectiveDate()));
  }
  
  public void setUpAccountJSON() throws Exception{
    accountJSON.put("effectiveDate", "07/01/2014");
    accountJSON.put("effectiveMs", 1404153000000L);
    accountJSON.put("zipCode", "68501");
    accountJSON.put("countyId", "0");
    
    Map<String, Object> members = new HashMap<String, Object>();
    members.put("isNew", true);
    members.put("memberRefName", 249);
    
    JSONObject nameJSON = new JSONObject();
    nameJSON.put("first", "gfrwe");
    members.put("name", nameJSON);
    members.put("birthDate", "01/02/1987");
    members.put("gender", "M");
    members.put("memberRelationship", "MEMBER");
    members.put("relationshipKey", "PRIMARY");
    members.put("isSmoker", "n");
    JSONArray membersJSONArray = new JSONArray();
    membersJSONArray.put(members);
    accountJSON.put("members", membersJSONArray);
  }
  
  @After
  public void tearDown()
  {
    accountJSON = null;
    sessionAccountImpl = null;
    shoppingIndividualIntegrationProcess = null;
    sdf = null;
  }
}