package com.connecture.shopping.process.camel.request;

import org.junit.Assert;
import org.junit.Test;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class ConsumerDemographicsDataRequestTest
{
  @Test
  public void testMisc()
  {
    ConsumerDemographicsDataRequest request = new ConsumerDemographicsDataRequest();
    Assert.assertNull(request.getProducer());
    request.setContext(ShoppingContext.ANONYMOUS);
    Assert.assertEquals(ShoppingContext.ANONYMOUS.getContextName(), request.getRequestHeaders().get(BaseCamelRequest.CONTEXT_KEY));
    request.setTransactionId("TEST");
    Assert.assertEquals("TEST", request.getRequestHeaders().get(BaseCamelRequest.TRANSACTION_ID_KEY));
  }
}
