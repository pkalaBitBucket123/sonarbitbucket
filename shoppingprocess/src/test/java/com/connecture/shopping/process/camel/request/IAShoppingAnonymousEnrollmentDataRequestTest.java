package com.connecture.shopping.process.camel.request;

import org.junit.Assert;
import org.junit.Test;

public class IAShoppingAnonymousEnrollmentDataRequestTest
{
  @Test
  public void testMisc()
  {
    IAShoppingAnonymousEnrollmentDataRequest request = new IAShoppingAnonymousEnrollmentDataRequest();
    Assert.assertNull(request.getProducer());
  }
}
