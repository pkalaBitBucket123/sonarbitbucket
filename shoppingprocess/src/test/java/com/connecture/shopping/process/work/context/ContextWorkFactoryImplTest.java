package com.connecture.shopping.process.work.context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.Work;
import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.WorkProviderImpl;

public class ContextWorkFactoryImplTest
{
  interface FactoryWorkProviderSetter
  {
    void setWorkProvider(WorkProviderImpl<?> workProvider);
  }
  
  public static <WP extends WorkProviderImpl<W>, W extends Work> void testGetWork(
    ContextWorkFactoryImpl factory,
    RequestType requestType,
    Class<WP> workProviderClass,
    Class<W> workClass,
    FactoryWorkProviderSetter setter) throws JSONException, Exception
  {
    WorkProviderImpl<W> workProvider = (WorkProviderImpl<W>)Mockito.mock(workProviderClass);
    W work = (W)Mockito.mock(workClass);
    setter.setWorkProvider(workProvider);
    Mockito.when(workProvider.getWork(Mockito.anyString(), Mockito.anyString(), Mockito.any(WorkConstants.Method.class), Mockito.any(JSONObject.class), Mockito.any(JSONObject.class))).thenReturn(work);
    Work resultWork = factory.getWork(null, requestType, null, Method.GET, null, null, null);
    Assert.assertEquals(work, resultWork);
  }

  @Test
  public void testGetWork() throws Exception
  {
    final ContextWorkFactoryImpl factory = new ContextWorkFactoryImpl();
    try
    {
      factory.getWork(null, null, null, null, null, null, null);
      Assert.fail("Excepion expected");
    }
    catch (Exception e)
    {
      // This is expected
    }
    
    Account account = Mockito.mock(Account.class);
    factory.setAccount(account);
    
    @SuppressWarnings("unchecked")
    WorkProviderImpl<MockWork> mockProvider = Mockito.mock(WorkProviderImpl.class);
    factory.registerProvider(RequestType.APPLICATION_CONFIG, mockProvider);
    Mockito.when(mockProvider.getWork(Mockito.anyString(), Mockito.anyString(), Mockito.any(WorkConstants.Method.class),
      Mockito.any(JSONObject.class), Mockito.any(JSONObject.class))).thenReturn(null);
    Work work = factory.getWork("", RequestType.APPLICATION_CONFIG, "", WorkConstants.Method.GET, null, null, null);
    Assert.assertNull(work);

    MockWork mockWork = Mockito.mock(MockWork.class);
    testGetWork(factory);

    factory.registerProvider(RequestType.ACCOUNT, mockProvider);
    Mockito.when(mockProvider.getWork(Mockito.anyString(), Mockito.anyString(), Mockito.any(WorkConstants.Method.class),
      Mockito.any(JSONObject.class), Mockito.any(JSONObject.class))).thenReturn(mockWork);
    work = factory.getAccountWork("");
    Assert.assertEquals(mockWork, work);
  }

  @SuppressWarnings("unchecked")
  void testGetWork(final ContextWorkFactoryImpl factory) throws JSONException, Exception
  {
    testGetWork(factory, RequestType.APPLICATION_CONFIG, WorkProviderImpl.class, MockWork.class,
      new FactoryWorkProviderSetter() {
        @Override
        public void setWorkProvider(WorkProviderImpl<?> workProvider)
        {
          factory.registerProvider(RequestType.APPLICATION_CONFIG, workProvider);
        }
      });
  }
  
  abstract class MockWork implements Work { }
  
  @Test
  public void testToJSONObject() throws JSONException
  {
    ContextWorkFactoryImpl factory = new ContextWorkFactoryImpl();
    Assert.assertNotNull(factory.toJSONObject(null));
    JSONObject result = factory.toJSONObject(new JSONArray());
    Assert.assertNotNull(result);
    Assert.assertNull(result.names());
    result = factory.toJSONObject(new JSONArray("[ { }, { } ]"));
    Assert.assertNull(result.names());
    result = factory.toJSONObject(new JSONArray("[ { key : \"key\" }, { } ]"));
    Assert.assertNull(result.names());
    result = factory.toJSONObject(new JSONArray("[ { key : \"1\", value : \"1\" }, { } ]"));
    Assert.assertEquals(1, result.names().length());
    result = factory.toJSONObject(new JSONArray("[ { key : \"1\", value : \"1\" }, { key : \"2\", value : \"2\" } ]"));
    Assert.assertEquals(2, result.names().length());
  }
}
