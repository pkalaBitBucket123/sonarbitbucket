package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathAction;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathBulkRemoveAction;

public class JSONPathBulkRemoveActionTest
{
  @Test
  public void isValidFor()
  {
    Assert.assertFalse(new JSONPathBulkRemoveAction().isValidFor(JSONPathAction.PathState.MISSING));
    Assert.assertFalse(new JSONPathBulkRemoveAction()
      .isValidFor(JSONPathAction.PathState.MISSING_SUBPATH));
    Assert.assertTrue(new JSONPathBulkRemoveAction().isValidFor(JSONPathAction.PathState.FOUND));
  }

  @Test
  public void perform() throws JSONException
  {
    JSONObject jsonObject = new JSONObject("{ testObjects : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] }");
    JSONPathBulkRemoveAction action = new JSONPathBulkRemoveAction();

    Assert.assertFalse(action.perform("name", jsonObject.getJSONArray("testObjects").getJSONObject(0), JSONPathAction.PathState.MISSING));

    Assert.assertTrue(action.perform("name", jsonObject.getJSONArray("testObjects").getJSONObject(0), JSONPathAction.PathState.FOUND));
    Assert.assertFalse(jsonObject.getJSONArray("testObjects").getJSONObject(0).has("name"));
    Assert.assertTrue(action.perform("name", jsonObject.getJSONArray("testObjects").getJSONObject(1), JSONPathAction.PathState.FOUND));
    Assert.assertFalse(jsonObject.getJSONArray("testObjects").getJSONObject(1).has("name"));
    Assert.assertEquals(2, action.getAllRemoved().size());
  }
}
