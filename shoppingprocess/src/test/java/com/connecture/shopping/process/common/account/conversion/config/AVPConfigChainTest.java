package com.connecture.shopping.process.common.account.conversion.config;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class AVPConfigChainTest
{
  @Test
  public void add()
  {
    AVPConfigChain chain = new AVPConfigChain();
    try
    {
      chain.add(null);
      Assert.fail("Expected illegalArgumentException");
    }
    catch (IllegalArgumentException e)
    {
      // This is expected
    }
    
    try
    {
      chain.add(new AVPConfig());
      Assert.fail("Expected IllegalStateException");
    }
    catch (IllegalStateException e)
    {
      // This is expected
    }
    
    AVPConfig config = new AVPConfig();
    config.setNewVersion("2.0");
    config.setPreviousVersion("1.0");
    chain.add(config);
    
    Assert.assertEquals(1, chain.getConfigurations().size());
    
    config = new AVPConfig();
    config.setNewVersion("3.0");
    config.setPreviousVersion("2.0");
    chain.add(config);
    
    Assert.assertEquals(2, chain.getConfigurations().size());
    Assert.assertEquals(config, chain.getConfigurations().get(1));
    
    config = new AVPConfig();
    config.setNewVersion("1.0");
    config.setPreviousVersion("0.5");
    chain.add(config);
    
    Assert.assertEquals(3, chain.getConfigurations().size());
    Assert.assertEquals(config, chain.getConfigurations().get(0));
  }
  
  @Test
  public void getConfigurationsFor()
  {
    AVPConfigChain chain = new AVPConfigChain();
    
    Assert.assertEquals(0, chain.getConfigurationsFor("bogus").size());
    
    AVPConfig config1 = new AVPConfig();
    config1.setNewVersion("2.0");
    config1.setPreviousVersion("1.0");
    chain.add(config1);
    
    AVPConfig config2 = new AVPConfig();
    config2.setNewVersion("3.0");
    config2.setPreviousVersion("2.0");
    chain.add(config2);
    
    AVPConfig config3 = new AVPConfig();
    config3.setNewVersion("1.0");
    config3.setPreviousVersion("0.5");
    chain.add(config3);
    
    List<AVPConfig> configurations = chain.getConfigurationsFor("2.0");
    Assert.assertEquals(2, configurations.size());
    
    Assert.assertEquals(config3, configurations.get(0));
    Assert.assertEquals(config1, configurations.get(1));
  }
  
  @Test
  public void getConfigurationsForFrom()
  {
    AVPConfigChain chain = new AVPConfigChain();
    
    AVPConfig config1 = new AVPConfig();
    config1.setNewVersion("2.0");
    config1.setPreviousVersion("1.0");
    chain.add(config1);
    
    AVPConfig config2 = new AVPConfig();
    config2.setNewVersion("3.0");
    config2.setPreviousVersion("2.0");
    chain.add(config2);
    
    AVPConfig config3 = new AVPConfig();
    config3.setNewVersion("1.0");
    config3.setPreviousVersion("0.5");
    chain.add(config3);
    
    List<AVPConfig> configurations = chain.getConfigurationsFor("2.0", "3.0");
    Assert.assertEquals(1, configurations.size());
    
    Assert.assertEquals(config2, configurations.get(0));
  }
}
