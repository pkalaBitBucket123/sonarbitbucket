package com.connecture.shopping.process.service.data.cache;

import org.junit.Assert;
import org.junit.Test;

public class ShoppingDataProviderKeyTest
{
  @Test
  public void testFindByKey()
  {
    Assert.assertNull(ShoppingDataProviderKey.findByKey("monkeys"));
    Assert.assertEquals(ShoppingDataProviderKey.PRODUCTS, ShoppingDataProviderKey.findByKey("products"));
  }
}
