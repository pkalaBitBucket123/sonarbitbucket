package com.connecture.shopping.process.work.individual;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.work.WorkConstants;

public class IndividualAccountWorkTest
{

  @Test
  public void testGet() throws Exception
  {
    IndividualAccountWork work = new IndividualAccountWork();
    Account account = Mockito.mock(Account.class);
    JSONObject accountJSON = new JSONObject();
    Mockito.when(account.getOrCreateAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.setAccount(account);
    work.get();
    Assert.assertEquals(accountJSON, work.getAccountJSON());
  }
  
  @Test
  public void testProcessData() throws Exception
  {
    IndividualAccountWork work = new IndividualAccountWork();
    Account account = Mockito.mock(Account.class);
    work.setAccount(account);
    Mockito.when(account.getOrCreateAccount(Mockito.anyString())).thenReturn(null);
    work.get();
    
    JSONObject result = work.processData();
    Assert.assertNull(result.optJSONObject("account"));
    Assert.assertNull(result.optJSONArray("members"));
    
    JSONObject accountJSON = new JSONObject();
    Mockito.when(account.getOrCreateAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.get();
    result = work.processData();
    Assert.assertNotNull(result.getJSONObject("account"));
    Assert.assertNull(result.optJSONArray("members"));
    
    accountJSON.put("members", new JSONArray());
    result = work.processData();
    Assert.assertNotNull(result.getJSONArray("members"));
    
    work.setMethod(WorkConstants.Method.UPDATE);
    result = work.processData();
    Assert.assertNull(result.optJSONObject("account"));
    Assert.assertNull(result.optJSONArray("members"));
  }
}
