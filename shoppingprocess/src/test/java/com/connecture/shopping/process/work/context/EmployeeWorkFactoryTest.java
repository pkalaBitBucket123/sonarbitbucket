package com.connecture.shopping.process.work.context;

import org.junit.Test;

import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.context.ContextWorkFactoryImplTest.FactoryWorkProviderSetter;
import com.connecture.shopping.process.work.core.AccountWork;
import com.connecture.shopping.process.work.core.ActorWork;
import com.connecture.shopping.process.work.core.ApplicationConfigWork;
import com.connecture.shopping.process.work.core.ConsumerDemographicsWork;
import com.connecture.shopping.process.work.core.CostEstimatorCostsWork;
import com.connecture.shopping.process.work.core.CostEstimatorWork;
import com.connecture.shopping.process.work.core.ProductLinesWork;
import com.connecture.shopping.process.work.core.ProductPlanWork;
import com.connecture.shopping.process.work.core.ProviderSearchWork;
import com.connecture.shopping.process.work.core.RulesEngineWork;
import com.connecture.shopping.process.work.core.provider.AccountWorkProvider;
import com.connecture.shopping.process.work.core.provider.ActorWorkProvider;
import com.connecture.shopping.process.work.core.provider.ApplicationConfigWorkProvider;
import com.connecture.shopping.process.work.core.provider.ConsumerDemographicsWorkProvider;
import com.connecture.shopping.process.work.core.provider.CostEstimatorCostsWorkProvider;
import com.connecture.shopping.process.work.core.provider.CostEstimatorWorkProvider;
import com.connecture.shopping.process.work.core.provider.ProductLinesWorkProvider;
import com.connecture.shopping.process.work.core.provider.ProductPlanWorkProvider;
import com.connecture.shopping.process.work.core.provider.ProviderSearchWorkProvider;
import com.connecture.shopping.process.work.core.provider.RulesEngineWorkProvider;
import com.connecture.shopping.process.work.employee.ConsumerEligibilityWork;
import com.connecture.shopping.process.work.employee.provider.ConsumerEligibilityWorkProvider;

public class EmployeeWorkFactoryTest
{
  @Test
  public void testGetWork() throws Exception
  {
    final EmployeeWorkFactory factory = new EmployeeWorkFactory();
    
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.ACCOUNT, AccountWorkProvider.class, AccountWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setAccountWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.CONSUMER_DEMOGRAPHICS, ConsumerDemographicsWorkProvider.class, ConsumerDemographicsWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setConsumerDemographicsWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.MEMBER_ELIGIBILITY, ConsumerEligibilityWorkProvider.class, ConsumerEligibilityWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setConsumerEligibilityWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.PRODUCT_LINES, ProductLinesWorkProvider.class, ProductLinesWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setProductLinesWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.PRODUCTS, ProductPlanWorkProvider.class, ProductPlanWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setProductPlanWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.COST_ESTIMATOR_CONFIG, CostEstimatorWorkProvider.class, CostEstimatorWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setCostEstimatorWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.COST_ESTIMATOR_COSTS, CostEstimatorCostsWorkProvider.class, CostEstimatorCostsWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setCostEstimatorCostsWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.RULES_ENGINE, RulesEngineWorkProvider.class, RulesEngineWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setRulesEngineWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.PROVIDER_SEARCH, ProviderSearchWorkProvider.class, ProviderSearchWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setProviderSearchWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.APPLICATION_CONFIG, ApplicationConfigWorkProvider.class, ApplicationConfigWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setApplicationConfigWorkProvider(workProvider);
      }
    });
    ContextWorkFactoryImplTest.testGetWork(factory, RequestType.ACTOR, ActorWorkProvider.class, ActorWork.class,
      new FactoryWorkProviderSetter() {
      @Override
      public void setWorkProvider(WorkProviderImpl<?> workProvider)
      {
        factory.setActorWorkProvider(workProvider);
      }
    });
  }
}
