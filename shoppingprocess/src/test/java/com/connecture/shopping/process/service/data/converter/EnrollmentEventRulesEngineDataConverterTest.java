package com.connecture.shopping.process.service.data.converter;

import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentEvent;

public class EnrollmentEventRulesEngineDataConverterTest
{

  @Test
  public void testConvertData() throws Exception
  {
    EnrollmentEventRulesEngineDataConverter converter = new EnrollmentEventRulesEngineDataConverter();
    
    String data = "";
    try
    {
      converter.convertData(data);
      Assert.fail("Excepted exception for bad JSON");
    }
    catch (Exception e)
    {
      // Expected
    }
    
    data = "{ request : { }}";
    try
    {
      converter.convertData(data);
      Assert.fail("Excepted exception for missing Application");
    }
    catch (Exception e)
    {
      // Expected
    }
    
    data = "{ request : { error : \"Error\" } }";
    try
    {
      converter.convertData(data);
      Assert.fail("Excepted exception for Error");
    }
    catch (Exception e)
    {
      // Expected
    }
    
    data = "{}";
    try
    {
      converter.convertData(data);
      Assert.fail("Excepted exception for missing Application");
    }
    catch (Exception e)
    {
      // Expected
    }
    
    data = "{ Application : { } }";
    EnrollmentEventDomainModel result = converter.convertData(data);
    Assert.assertNotNull(result);
    
    String effectiveDate = "12/12/2012";
    data = "{ Application : { Effective_Date : \"" + effectiveDate + "\" } }";
    result = converter.convertData(data);
    Assert.assertEquals(effectiveDate, result.getApplication().getEffectiveDate());

    String openEnrollmentStartDate = "12/12/2012";
    data = "{ Application : { Special_Enrollment : { Open_Enrollment_Start_Date : \"" + openEnrollmentStartDate + "\"} } }";
    result = converter.convertData(data);
    Assert.assertEquals(openEnrollmentStartDate, result.getApplication().getSpecialEnrollment().getOpenEnrollmentStartDate());

    String openEnrollmentEndDate = "12/12/2012";
    data = "{ Application : { Special_Enrollment : { Open_Enrollment_End_Date : \"" + openEnrollmentEndDate + "\"} } }";
    result = converter.convertData(data);
    Assert.assertEquals(openEnrollmentEndDate, result.getApplication().getSpecialEnrollment().getOpenEnrollmentEndDate());

    int daysLeftInOpenEnrollment = 1;
    data = "{ Application : { Special_Enrollment : { Days_Left_In_Open_Enrollment : " + daysLeftInOpenEnrollment + " } } }";
    result = converter.convertData(data);
    Assert.assertEquals(daysLeftInOpenEnrollment, result.getApplication().getSpecialEnrollment().getDaysLeftToEnroll().intValue());
    
    data = "{ Application : { Special_Enrollment : { Special_Enrollment_Reasons : [ { Reason_Key : \"reason\", Reason_Value : \"value\", EnrollmentWindowValue : 30 } ] } } }";
    result = converter.convertData(data);
    Assert.assertEquals(1, result.getApplication().getSpecialEnrollment().getEvents().size());
    SpecialEnrollmentEvent event = result.getApplication().getSpecialEnrollment().getEvents().get(0);
    Assert.assertEquals("reason", event.getEventType());
    Assert.assertEquals("value", event.getDisplayName());
    Assert.assertEquals(30, event.getEnrollmentDays().intValue());
    
    data = "{ Application : { Special_Enrollment : { Within_Enrollment_Window : \"Yes\" } } }";
    result = converter.convertData(data);
    Assert.assertEquals("Yes", result.getApplication().getSpecialEnrollment().getWithinEnrollmentWindow());
  }
}
