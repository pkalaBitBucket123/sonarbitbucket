package com.connecture.shopping.process.common;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class JSONObjectUserTypeTest
{
  @Test
  public void testReturnedClass()
  {
    Assert.assertEquals(JSONObject.class, new JSONObjectUserType().returnedClass());
  }

  @Test
  public void testNullSafeSet() throws Exception
  {
    JSONObjectUserType type = new JSONObjectUserType();
    String stringValue = "{\"name\":\"Name\"}";
    PreparedStatement statement = Mockito.mock(PreparedStatement.class);
    type.nullSafeSet(statement, null, 0);
    Mockito.verify(statement).setNull(0, Types.LONGVARCHAR);
    
    JSONObject value = new JSONObject(stringValue);
    type.nullSafeSet(statement, value, 0);
    Mockito.verify(statement).setString(0, "<?xml version=\"1.0\" encoding=\"UTF-16\"?>\r\n" +
    		"<o><name type=\"string\">Name</name></o>\r\n");
  }
  
  @Test
  public void testNullSafeGet() throws SQLException
  {
    JSONObjectUserType type = new JSONObjectUserType();
    ResultSet rs = Mockito.mock(ResultSet.class);
    Mockito.when(rs.wasNull()).thenReturn(Boolean.TRUE);
    Assert.assertNull(type.nullSafeGet(rs, new String[]{"A"}, null));
    
    Mockito.when(rs.wasNull()).thenReturn(Boolean.FALSE);
    Mockito.when(rs.getString(Mockito.anyString())).thenReturn(null);
    String result = String.valueOf(type.nullSafeGet(rs, new String[]{"A"}, null));
    Assert.assertEquals("{}", result);
    
    Mockito.when(rs.getString(Mockito.anyString())).thenReturn("<?xml version=\"1.0\" encoding=\"UTF-16\"?>\r\n" +
      "<o><name type=\"string\">Name</name></o>\r\n");
    result = String.valueOf(type.nullSafeGet(rs, new String[]{"A"}, null));
    Assert.assertEquals("{\"name\":\"Name\"}", result);
  }
  
  @Test
  public void testMisc() throws JSONException
  {
    JSONObjectUserType type = new JSONObjectUserType();
    Assert.assertTrue(type.isMutable());
    
    JSONObject jsonObject = new JSONObject();
    Assert.assertEquals("{}".hashCode(), type.hashCode(jsonObject));

    Assert.assertTrue(type.equals(jsonObject, jsonObject));
    Assert.assertTrue(type.equals(jsonObject, "{}"));
    Assert.assertFalse(type.equals(jsonObject, ""));
    Assert.assertFalse(type.equals(jsonObject, null));
    Assert.assertFalse(type.equals(null, "{}"));
    Assert.assertTrue(type.equals(null, null));
    
    Assert.assertEquals("{}", type.disassemble(jsonObject));
    
    Assert.assertNull(type.deepCopy(null));
    Assert.assertNull(type.deepCopy(null));
    String json = "{ things : [ { type : \"X\" }, { type : \"Y\", value : 2 } ] }";
    jsonObject = new JSONObject(json);
    Assert.assertEquals(jsonObject.toString(), type.deepCopy(jsonObject).toString());
    
    Assert.assertNull(type.assemble(null, null));
    Assert.assertEquals(jsonObject.toString(), type.assemble(json, null).toString());
    
    Assert.assertEquals(jsonObject.toString(), type.replace(jsonObject, null, null).toString());
    
    Assert.assertEquals(1, type.sqlTypes().length);

    
  }
}
