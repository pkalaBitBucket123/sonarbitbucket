package com.connecture.shopping.process.work.context;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class ContextFactoryTest
{
  @Test
  public void testGetFactory() throws Exception
  {
    final ContextWorkFactory employeeWorkFactory = Mockito.mock(ContextWorkFactory.class);
    final ContextWorkFactory individualWorkFactory = Mockito.mock(ContextWorkFactory.class);
    final ContextWorkFactory anonymousWorkFactory = Mockito.mock(ContextWorkFactory.class);
    ContextFactory factory = new ContextFactory() {
      @Override
      protected ContextWorkFactory createEmployeeWorkFactory()
      {
        return employeeWorkFactory;
      }

      @Override
      protected ContextWorkFactory createIndividualWorkFactory()
      {
        return individualWorkFactory;
      }

      @Override
      protected ContextWorkFactory createAnonymousWorkFactory()
      {
        return anonymousWorkFactory;
      }
    };
    ContextWorkFactory workFactory = factory.getFactory(ShoppingContext.EMPLOYEE);
    Assert.assertEquals(employeeWorkFactory, workFactory);
    workFactory = factory.getFactory(ShoppingContext.INDIVIDUAL);
    Assert.assertEquals(individualWorkFactory, workFactory);
    workFactory = factory.getFactory(ShoppingContext.ANONYMOUS);
    Assert.assertEquals(anonymousWorkFactory, workFactory);
  }
}
