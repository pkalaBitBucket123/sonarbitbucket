package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathAction;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathRemoveAction;

public class JSONPathRemoveActionTest
{
  @Test
  public void isValidFor()
  {
    Assert.assertFalse(new JSONPathRemoveAction().isValidFor(JSONPathAction.PathState.MISSING));
    Assert.assertFalse(new JSONPathRemoveAction()
      .isValidFor(JSONPathAction.PathState.MISSING_SUBPATH));
    Assert.assertTrue(new JSONPathRemoveAction().isValidFor(JSONPathAction.PathState.FOUND));
  }

  @Test
  public void perform() throws JSONException
  {
    JSONObject jsonObject = new JSONObject("{ testObject_1 : { name : \"tester\" } }");
    JSONPathRemoveAction action = new JSONPathRemoveAction();

    Assert.assertFalse(action.perform("name", jsonObject.getJSONObject("testObject_1"),
      JSONPathAction.PathState.MISSING));
    Assert.assertTrue(jsonObject.getJSONObject("testObject_1").has("name"));

    Assert.assertTrue(action.perform("name", jsonObject.getJSONObject("testObject_1"),
      JSONPathAction.PathState.FOUND));
    Assert.assertFalse(jsonObject.getJSONObject("testObject_1").has("name"));
    Assert.assertEquals("tester", action.getRemoved());
  }
}
