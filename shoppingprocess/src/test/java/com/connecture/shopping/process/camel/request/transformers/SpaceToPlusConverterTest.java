package com.connecture.shopping.process.camel.request.transformers;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

public class SpaceToPlusConverterTest
{

  @Test
  public void testConvert()
  {
    Map<String, Object> headers = new HashMap<String, Object>();
    String headersToConvertStr = null;
    new SpaceToPlusConverter().convert(headers, headersToConvertStr);
    Assert.assertTrue(headers.isEmpty());
    
    headersToConvertStr = "";
    new SpaceToPlusConverter().convert(headers, headersToConvertStr);
    Assert.assertTrue(headers.isEmpty());
    
    headersToConvertStr = "A; B";
    new SpaceToPlusConverter().convert(headers, headersToConvertStr);
    Assert.assertTrue(headers.isEmpty());
    
    headers.put("A", "1");
    new SpaceToPlusConverter().convert(headers, headersToConvertStr);
    Assert.assertEquals("1", headers.get("A"));
    
    headers.put("B", "1 2 3");
    new SpaceToPlusConverter().convert(headers, headersToConvertStr);
    Assert.assertEquals("1+2+3", headers.get("B"));
  }

}
