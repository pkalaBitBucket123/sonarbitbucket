package com.connecture.shopping.process.service.data.cache;

public enum DataCacheTestProviderKey implements DataProviderKey
{
  TEST, TEST2
}
