package com.connecture.shopping.process.service.data.converter;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.service.data.Attribute;
import com.connecture.shopping.process.service.data.AttributeConsumer;
import com.connecture.shopping.process.service.data.Benefit;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ConsumerProfile;
import com.connecture.shopping.process.service.data.ConsumerUsage;
import com.connecture.shopping.process.service.data.Product;
import com.connecture.shopping.process.service.data.ProductConsumer;
import com.connecture.shopping.process.service.data.Profile;
import com.connecture.shopping.process.service.data.Property;
import com.connecture.shopping.process.service.data.Request;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.UsageCategory;
import com.connecture.shopping.process.service.data.UsageData;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsForm;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldMask;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldOption;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormValidationError;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormValidationErrorConfig;

public class ShoppingRulesEngineDataConverterTest
{

  @Test
  public void testCreateProperty() throws JSONException
  {
    JSONArray propertyArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<Property> propertyList = converter.createProperty(propertyArray);
    Assert.assertNotNull(propertyList);
    Assert.assertEquals(0, propertyList.size());
    
    propertyArray.put(new JSONObject("{ key : \"key\", code : \"code\" }"));
    propertyList = converter.createProperty(propertyArray);
    Assert.assertEquals(1, propertyList.size());
    Assert.assertEquals("key", propertyList.get(0).getKey());
    Assert.assertEquals("code", propertyList.get(0).getCode());
  }
  
  @Test
  public void testCreateBenefit() throws JSONException
  {
    JSONArray benefitArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<Benefit> benefitList = converter.createBenefit(benefitArray);
    Assert.assertNotNull(benefitList);
    Assert.assertEquals(0, benefitList.size());
    
    benefitArray.put(new JSONObject("{ key : \"key\", code : \"code\" }"));
    benefitList = converter.createBenefit(benefitArray);
    Assert.assertEquals(1, benefitList.size());
    Assert.assertEquals("key", benefitList.get(0).getKey());
    Assert.assertEquals("code", benefitList.get(0).getCode());
  }
  
  @Test
  public void testCreateConsumerUsage() throws JSONException
  {
    JSONArray consumerUsageArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<ConsumerUsage> consumerUsageList = converter.createConsumerUsage(consumerUsageArray);
    Assert.assertNotNull(consumerUsageList);
    Assert.assertEquals(0, consumerUsageList.size());
    
    consumerUsageArray.put(new JSONObject("{ xid : \"0\" }"));
    consumerUsageList = converter.createConsumerUsage(consumerUsageArray);
    Assert.assertEquals(0, consumerUsageList.size());
    
    consumerUsageArray = new JSONArray();
    consumerUsageArray.put(new JSONObject("{ xid : \"0\", value : null }"));
    consumerUsageList = converter.createConsumerUsage(consumerUsageArray);
    Assert.assertEquals(0, consumerUsageList.size());
    
    consumerUsageArray = new JSONArray();
    consumerUsageArray.put(new JSONObject("{ xid : \"0\", value : 0 }"));
    consumerUsageList = converter.createConsumerUsage(consumerUsageArray);
    Assert.assertEquals(1, consumerUsageList.size());
    Assert.assertEquals("0", consumerUsageList.get(0).getXid());
    Assert.assertEquals(0d, consumerUsageList.get(0).getValue().doubleValue(), 0.000001d);
  }

  @Test
  public void testCreateUsageData() throws JSONException
  {
    JSONArray usageDataArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<UsageData> usageDataList = converter.createUsageData(usageDataArray);
    Assert.assertNotNull(usageDataList);
    Assert.assertEquals(0, usageDataList.size());
    
    usageDataArray.put(new JSONObject("{ key : \"key\", display : \"display\" }"));
    usageDataList = converter.createUsageData(usageDataArray);
    Assert.assertNotNull(usageDataList);
    Assert.assertEquals(1, usageDataList.size());
    Assert.assertEquals("key", usageDataList.get(0).getKey());
    Assert.assertEquals("display", usageDataList.get(0).getDisplay());
    Assert.assertEquals(0, usageDataList.get(0).getConsumer().size());
    
    usageDataArray = new JSONArray();
    usageDataArray.put(new JSONObject("{ key : \"key\", display : \"display\", consumer : [ { xid : \"0\", value : 0 } ]}"));
    usageDataList = converter.createUsageData(usageDataArray);
    Assert.assertNotNull(usageDataList);
    Assert.assertEquals(1, usageDataList.get(0).getConsumer().size());
  }

  @Test
  public void testCreateUsageCategory() throws JSONException
  {
    JSONArray usageCategoryArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<UsageCategory> usageCategoryList = converter.createUsageCategory(usageCategoryArray);
    Assert.assertNotNull(usageCategoryList);
    Assert.assertEquals(0, usageCategoryList.size());
    
    usageCategoryArray.put(new JSONObject("{ categoryId : \"categoryId\", display : \"display\" }"));
    usageCategoryList = converter.createUsageCategory(usageCategoryArray);
    Assert.assertNotNull(usageCategoryList);
    Assert.assertEquals(1, usageCategoryList.size());
    Assert.assertEquals("categoryId", usageCategoryList.get(0).getCategoryId());
    Assert.assertEquals("display", usageCategoryList.get(0).getDisplay());
    Assert.assertEquals(0, usageCategoryList.get(0).getUsageData().size());
    
    usageCategoryArray = new JSONArray();
    usageCategoryArray.put(new JSONObject("{ categoryId : \"categoryId\", display : \"display\", usageData : [ { key : \"key\", display : \"display\" } ]}"));
    usageCategoryList = converter.createUsageCategory(usageCategoryArray);
    Assert.assertNotNull(usageCategoryList);
    Assert.assertEquals(1, usageCategoryList.get(0).getUsageData().size());
  }

  @Test
  public void testCreateProfile() throws JSONException
  {
    JSONArray profileArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<Profile> profileList = converter.createProfile(profileArray);
    Assert.assertNotNull(profileList);
    Assert.assertEquals(0, profileList.size());
    
    profileArray.put(new JSONObject("{ id : \"id\", name : \"name\", description : \"description\" }"));
    profileList = converter.createProfile(profileArray);
    Assert.assertNotNull(profileList);
    Assert.assertEquals(1, profileList.size());
    Assert.assertEquals("id", profileList.get(0).getId());
    Assert.assertEquals("name", profileList.get(0).getName());
    Assert.assertEquals("description", profileList.get(0).getDescription());
    Assert.assertEquals(0, profileList.get(0).getUsageCategory().size());
    
    profileArray = new JSONArray();
    profileArray.put(new JSONObject("{ id : \"id\", name : \"name\", description : \"description\", usageCategory : [ { categoryId : \"categoryId\", display : \"display\" } ]}"));
    profileList = converter.createProfile(profileArray);
    Assert.assertNotNull(profileList);
    Assert.assertEquals(1, profileList.get(0).getUsageCategory().size());
  }

  @Test
  public void testCreateProductConsumer() throws JSONException
  {
    JSONArray productConsumerArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<ProductConsumer> productConsumerList = converter.createProductConsumer(productConsumerArray);
    Assert.assertNotNull(productConsumerList);
    Assert.assertEquals(0, productConsumerList.size());
    
    productConsumerArray.put(new JSONObject("{ xid : \"xid\", cost : 0 }"));
    productConsumerList = converter.createProductConsumer(productConsumerArray);
    Assert.assertNotNull(productConsumerList);
    Assert.assertEquals(1, productConsumerList.size());
    Assert.assertEquals("xid", productConsumerList.get(0).getXid());
    Assert.assertEquals(0d, productConsumerList.get(0).getCost(), 0.000001d);
  }

  @Test
  public void testCreateAttributeConsumer() throws JSONException
  {
    JSONArray attributeConsumerArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<AttributeConsumer> attributeConsumerList = converter.createAttributeConsumer(attributeConsumerArray);
    Assert.assertNotNull(attributeConsumerList);
    Assert.assertEquals(0, attributeConsumerList.size());
    
    attributeConsumerArray.put(new JSONObject("{ xid : \"xid\", cost : 0 }"));
    attributeConsumerList = converter.createAttributeConsumer(attributeConsumerArray);
    Assert.assertNotNull(attributeConsumerList);
    Assert.assertEquals(1, attributeConsumerList.size());
    Assert.assertEquals("xid", attributeConsumerList.get(0).getXid());
    Assert.assertEquals(0d, attributeConsumerList.get(0).getCost(), 0.000001d);
  }

  @Test
  public void testCreateAttribute() throws JSONException
  {
    JSONArray attributeArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<Attribute> attributeList = converter.createAttribute(attributeArray);
    Assert.assertNotNull(attributeList);
    Assert.assertEquals(0, attributeList.size());
    
    attributeArray.put(new JSONObject("{ key : \"key\", value : 0 }"));
    attributeList = converter.createAttribute(attributeArray);
    Assert.assertNotNull(attributeList);
    Assert.assertEquals(1, attributeList.size());
    Assert.assertEquals("key", attributeList.get(0).getKey());
    Assert.assertEquals(0d, attributeList.get(0).getValue().doubleValue(), 0.000001d);
    Assert.assertEquals(0, attributeList.get(0).getConsumer().size());
    
    attributeArray = new JSONArray();
    attributeArray.put(new JSONObject("{ key : \"key\", value : 0, consumer : [ { xid : \"0\", cost : 0 } ]}"));
    attributeList = converter.createAttribute(attributeArray);
    Assert.assertNotNull(attributeList);
    Assert.assertEquals(1, attributeList.get(0).getConsumer().size());
  }
  
  @Test
  public void testCreateProduct() throws JSONException
  {
    JSONArray productArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<Product> productList = converter.createProduct(productArray);
    Assert.assertNotNull(productList);
    Assert.assertEquals(0, productList.size());

    productArray.put(new JSONObject("{ xid : \"xid\" }"));
    productList = converter.createProduct(productArray);
    Assert.assertNotNull(productList);
    Assert.assertEquals(1, productList.size());
    Assert.assertEquals(0, productList.get(0).getAttribute().size());
    Assert.assertEquals(0, productList.get(0).getConsumer().size());

    productArray = new JSONArray();
    productArray.put(new JSONObject("{ xid : \"xid\", tierCode : \"tierCode\", cost : 0, maxOOPCost : 0, deductible : 0 }"));
    productList = converter.createProduct(productArray);
    Assert.assertNotNull(productList);
    Assert.assertEquals(1, productList.size());
    Assert.assertEquals(0, productList.get(0).getAttribute().size());
    Assert.assertEquals(0, productList.get(0).getConsumer().size());

    productArray = new JSONArray();
    productArray.put(new JSONObject("{ xid : \"xid\", tierCode : \"tierCode\", cost : 0, maxOOPCost : 0, deductible : 0, attribute : [ { key : \"key\", value : 0 } ], consumer : [ { xid : \"0\", cost : 0 } ] }"));
    productList = converter.createProduct(productArray);
    Assert.assertNotNull(productList);
    Assert.assertEquals(1, productList.size());
    Assert.assertEquals(1, productList.get(0).getAttribute().size());
    Assert.assertEquals(1, productList.get(0).getConsumer().size());
  }
  
  @Test
  public void testCreateConsumerProfile() throws JSONException
  {
    JSONArray consumerProfileArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<ConsumerProfile> consumerProfileList = converter.createConsumerProfile(consumerProfileArray);
    Assert.assertNotNull(consumerProfileList);
    Assert.assertEquals(0, consumerProfileList.size());
    
    consumerProfileArray.put(new JSONObject("{ key : \"key\", value : 0 }"));
    consumerProfileList = converter.createConsumerProfile(consumerProfileArray);
    Assert.assertNotNull(consumerProfileList);
    Assert.assertEquals(1, consumerProfileList.size());
    Assert.assertEquals("key", consumerProfileList.get(0).getKey());
    Assert.assertEquals(0, consumerProfileList.get(0).getValue().intValue());
  }
  
  @Test
  public void testCreateConsumer() throws JSONException
  {
    JSONArray consumerArray = new JSONArray();
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<Consumer> consumerList = converter.createConsumer(consumerArray);
    Assert.assertNotNull(consumerList);
    Assert.assertTrue(consumerList.isEmpty());
    
    consumerArray.put(new JSONObject("{}"));
    consumerList = converter.createConsumer(consumerArray);
    Assert.assertNotNull(consumerList);
    Assert.assertEquals(1, consumerList.size());
    
    consumerArray = new JSONArray();
    consumerArray.put(new JSONObject("{ xid : \"xid\", dateOfBirth : \"05/05/1980\", age : 33, gender : \"M\", zip : \"90210\", state : \"CA\", smoker : false, profile : [{ key : \"key\", value : 0 }]}"));
    consumerList = converter.createConsumer(consumerArray);
    Assert.assertNotNull(consumerList);
    Assert.assertEquals(1, consumerList.size());
    Consumer consumer = consumerList.get(0);
    Assert.assertEquals("xid", consumer.getXid());
    Assert.assertEquals("05/05/1980", consumer.getDateOfBirth());
    Assert.assertEquals(33, consumer.getAge().intValue());
    Assert.assertEquals("M", consumer.getGender());
    Assert.assertEquals("90210", consumer.getZip());
    Assert.assertEquals("CA", consumer.getState());
    Assert.assertFalse(consumer.getSmoker());
    Assert.assertEquals(1, consumer.getProfile().size());
  }
  
  @Test
  public void testCreateValidationError() throws JSONException 
  {
    JSONObject validationErrorObject = new JSONObject("{ key : \"key\", label : \"label\" }");
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    DemographicsFormValidationError validationError = converter.createValidationError(validationErrorObject);
    Assert.assertEquals("key", validationError.getKey());
    Assert.assertEquals("label", validationError.getLabel());
  }
  
  @Test
  public void testCreateValidationErrorConfiguration() throws JSONException 
  {
    JSONObject validationErrorConfigObject = new JSONObject("{ key : \"key\", label : \"label\" }");
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    DemographicsFormValidationErrorConfig validationErrorConfig = converter.createValidationErrorConfiguration(validationErrorConfigObject);
    Assert.assertEquals("key", validationErrorConfig.getKey());
    Assert.assertEquals("label", validationErrorConfig.getLabel());
  }
  
  @Test
  public void testCreateFormFieldMask() throws JSONException 
  {
    JSONObject formFieldMaskObject = new JSONObject("{ mask : \"mask\", maskClass : \"maskClass\" }");
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    DemographicsFormFieldMask fieldMask = converter.createFormFieldMask(formFieldMaskObject);
    Assert.assertEquals("mask", fieldMask.getMask());
    Assert.assertEquals("maskClass", fieldMask.getMaskClass());
  }
  
  @Test
  public void testCreateFormFieldOption() throws JSONException 
  {
    JSONObject formFieldOptionObject = new JSONObject("{ dataGroupType : \"dataGroupType\", defaultSelection : \"Y\", initialSelection : \"N\", label : \"label\", order : 1, value : \"value\" }");
    
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    DemographicsFormFieldOption formFieldOption = converter.createFormFieldOption(formFieldOptionObject);
    Assert.assertEquals("dataGroupType", formFieldOption.getDataGroupType());
    Assert.assertTrue(formFieldOption.getDefaultSelection());
    Assert.assertFalse(formFieldOption.getInitialSelection());
    Assert.assertEquals("label", formFieldOption.getLabel());
    Assert.assertEquals(1, formFieldOption.getOrder().intValue());
    Assert.assertEquals("value", formFieldOption.getValue());
  }
  
  @Test
  public void testCreateFormFieldOptions() throws JSONException
  {
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    List<DemographicsFormFieldOption> formFieldOptions = converter.createFormFieldOptions(null);
    Assert.assertTrue(formFieldOptions.isEmpty());

    formFieldOptions = converter.createFormFieldOptions(new JSONArray());
    Assert.assertTrue(formFieldOptions.isEmpty());

    JSONArray formFieldOptionsArray = new JSONArray("[{ dataGroupType : \"dataGroupType\", defaultSelection : \"Y\", initialSelection : \"N\", label : \"label\", order : 1, value : \"value\" }]");
    formFieldOptions = converter.createFormFieldOptions(formFieldOptionsArray);
    Assert.assertEquals(1, formFieldOptions.size());
  }
  
  @Test
  public void testCreateDemographicsFormField() throws JSONException
  {
    JSONObject fieldJSON = new JSONObject("{ key : \"key\", name : \"name\", label : \"label\", dataType : \"dataType\", styleClass : \"styleClass\", headerStyleClass : \"headerStyleClass\", inputType : \"inputType\", size : 1, order : 1, option : [] }");
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    DemographicsFormField formField = converter.createDemographicsFormField(fieldJSON);
    Assert.assertEquals("key", formField.getKey());
    Assert.assertEquals("name", formField.getName());
    Assert.assertEquals("label", formField.getLabel());
    Assert.assertEquals("dataType", formField.getDataType());
    Assert.assertEquals("styleClass", formField.getStyleClass());
    Assert.assertEquals("headerStyleClass", formField.getHeaderStyleClass());
    Assert.assertEquals("inputType", formField.getInputType());
    Assert.assertEquals(1, formField.getSize().intValue());
    Assert.assertEquals(1, formField.getOrder().intValue());
    Assert.assertTrue(formField.getOption().isEmpty());
    Assert.assertNotNull(formField.getMask());
    
    fieldJSON.put("mask", new JSONObject("{ mask : \"mask\", maskClass : \"maskClass\" }"));
    formField = converter.createDemographicsFormField(fieldJSON);
    Assert.assertNotNull(formField.getMask());
    
    fieldJSON.put("option", new JSONArray("[{ dataGroupType : \"dataGroupType\", defaultSelection : \"Y\", initialSelection : \"N\", label : \"label\", order : 1, value : \"value\" }]"));
    formField = converter.createDemographicsFormField(fieldJSON);
    Assert.assertEquals(1, formField.getOption().size());
  }
  
  @Test
  public void testCreateDemographicsForm() throws JSONException
  {
    JSONObject demographicsFormObject = new JSONObject();
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    DemographicsForm form = converter.createDemographicsForm(demographicsFormObject);
    Assert.assertFalse(form.getHasErrors());
    Assert.assertTrue(form.getField().isEmpty());
    Assert.assertTrue(form.getValidationErrorConfig().isEmpty());
    Assert.assertTrue(form.getValidationError().isEmpty());

    demographicsFormObject = new JSONObject("{ hasErrors : null, field : [], validationErrorConfig : [], validationError : []}");
    form = converter.createDemographicsForm(demographicsFormObject);
    Assert.assertFalse(form.getHasErrors());
    Assert.assertTrue(form.getField().isEmpty());
    Assert.assertTrue(form.getValidationErrorConfig().isEmpty());
    Assert.assertTrue(form.getValidationError().isEmpty());

    demographicsFormObject = new JSONObject("{ hasErrors : true, field : [{ key : \"key\", name : \"name\", label : \"label\", dataType : \"dataType\", styleClass : \"styleClass\", headerStyleClass : \"headerStyleClass\", inputType : \"inputType\", size : 1, order : 1, option : [] }], validationErrorConfig : [{ key : \"key\", label : \"label\" }], validationError : [{ key : \"key\", label : \"label\" }]}");
    form = converter.createDemographicsForm(demographicsFormObject);
    Assert.assertTrue(form.getHasErrors());
    Assert.assertEquals(1, form.getField().size());
    Assert.assertEquals(1, form.getValidationErrorConfig().size());
    Assert.assertEquals(1, form.getValidationError().size());
  }
  
  @Test
  public void testConvertData() throws Exception
  {
    ShoppingRulesEngineDataConverter converter = new ShoppingRulesEngineDataConverter();
    try
    {
      converter.convertData("");
      Assert.fail("Exception expected: failed parsing");
    }
    catch (Exception e)
    {
      // This is expected
    }
    
    try
    {
      converter.convertData("{}");
      Assert.fail("Exception expected: failed parsing");
    }
    catch (Exception e)
    {
      // This is expected
    }
    
    String dataStr = "{ request : {} }";
    ShoppingDomainModel domainModel = converter.convertData(dataStr);
    Request request = domainModel.getRequest();
    Assert.assertNotNull(request);
    Assert.assertEquals("", request.getId());
    Assert.assertEquals(0, request.getTimeInMs().longValue());
    Assert.assertEquals(0, request.getTimeOutMs().longValue());
    Assert.assertNull(request.getError());
    Assert.assertTrue(request.getConsumer().isEmpty());
    Assert.assertTrue(request.getProduct().isEmpty());
    Assert.assertTrue(request.getProfile().isEmpty());
    Assert.assertTrue(request.getBenefit().isEmpty());
    Assert.assertTrue(request.getProperty().isEmpty());
    Assert.assertNotNull(request.getDemographicsForm());
    
    dataStr = "{ request : { id : \"0\", timeInMs : 1, timeOutMs : 1, error : \"error\", consumer : [], product : [], profile : [], benefit : [], property : [], demographicsForm : {} } }";
    domainModel = converter.convertData(dataStr);
    request = domainModel.getRequest();
    Assert.assertEquals("0", request.getId());
    Assert.assertEquals(1, request.getTimeInMs().longValue());
    Assert.assertEquals(1, request.getTimeOutMs().longValue());
    Assert.assertEquals("error", request.getError());
    Assert.assertTrue(request.getConsumer().isEmpty());
    Assert.assertTrue(request.getProduct().isEmpty());
    Assert.assertTrue(request.getProfile().isEmpty());
    Assert.assertTrue(request.getBenefit().isEmpty());
    Assert.assertTrue(request.getProperty().isEmpty());
    Assert.assertNotNull(request.getDemographicsForm());
  }
}
