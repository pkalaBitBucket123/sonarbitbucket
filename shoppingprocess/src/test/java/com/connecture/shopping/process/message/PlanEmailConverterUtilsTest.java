package com.connecture.shopping.process.message;

import java.text.NumberFormat;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class PlanEmailConverterUtilsTest
{
  @Test
  public void testConvertToBenefitCategory() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.convertToBenefitCategory(null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    try 
    {
      PlanEmailConverterUtils.convertToBenefitCategory(new JSONObject());
      Assert.fail("JSONException expected");
    }
    catch (JSONException e)
    {
      // This is expected
    }
    
    JSONObject benefitCategoryJSON = new JSONObject();
    benefitCategoryJSON.put("categoryName", "C1");
    benefitCategoryJSON.put("benefits", new JSONArray());
    PlanEmailBenefitCategoryBean result = PlanEmailConverterUtils.convertToBenefitCategory(benefitCategoryJSON);
    Assert.assertEquals("C1", result.getCategoryName());
    Assert.assertTrue(result.getBenefits().isEmpty());
    
    JSONObject benefitObject = new JSONObject("{ displayName : \"Benefit\", values : [] }");
    JSONArray benefits = benefitCategoryJSON.getJSONArray("benefits");
    benefits.put(benefitObject);
    result = PlanEmailConverterUtils.convertToBenefitCategory(benefitCategoryJSON);
    Assert.assertEquals(1, result.getBenefits().size());
    Assert.assertEquals("Benefit", result.getBenefits().get(0).getBenefitName());
    Assert.assertEquals("", result.getBenefits().get(0).getBenefitValue());
    
    JSONArray valuesArray = benefitObject.getJSONArray("values");
    valuesArray.put(new JSONObject("{ value : \"10\" }"));
    valuesArray.put(new JSONObject("{ value : \"25\" }"));
    valuesArray.put(new JSONObject("{ value : \"100\" }"));
    result = PlanEmailConverterUtils.convertToBenefitCategory(benefitCategoryJSON);
    Assert.assertEquals("10 / 25 / 100", result.getBenefits().get(0).getBenefitValue());
  }
  
  @Test
  public void testConvertToBenefitCategories() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.convertToBenefitCategories(null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    JSONObject planJSON = new JSONObject();
    
    try 
    {
      PlanEmailConverterUtils.convertToBenefitCategories(planJSON);
      Assert.fail("JSONException expected");
    }
    catch (JSONException e)
    {
      // This is expected
    }
    
    planJSON.put("benefitCategories", new JSONArray());
    List<PlanEmailBenefitCategoryBean> benefitCategories = PlanEmailConverterUtils.convertToBenefitCategories(planJSON);
    Assert.assertTrue(benefitCategories.isEmpty());
    
    populatePlanJSONForConvertToBenefitCategoriesTest(planJSON);
    benefitCategories = PlanEmailConverterUtils.convertToBenefitCategories(planJSON);
    Assert.assertEquals(1, benefitCategories.size());
  }
  
  static void populatePlanJSONForConvertToBenefitCategoriesTest(JSONObject planJSON) throws JSONException
  {
    JSONObject benefitCategoryJSON = new JSONObject("{ categoryName : \"C1\", benefits : [{ displayName : \"Benefit\", values : [] }] }");
    JSONArray benefitCategoryArray = new JSONArray();
    benefitCategoryArray.put(benefitCategoryJSON);
    planJSON.put("benefitCategories", benefitCategoryArray);
  }

  @Test
  public void testConvertToProviders() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.convertToProviders(null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    JSONObject planJSON = new JSONObject();
    List<PlanEmailProviderBean> result = PlanEmailConverterUtils.convertToProviders(planJSON);
    Assert.assertTrue(result.isEmpty());

    JSONArray providersArray = new JSONArray();
    planJSON.put("providers", providersArray);
    result = PlanEmailConverterUtils.convertToProviders(planJSON);
    Assert.assertTrue(result.isEmpty());
    
    populatePlanJSONForConvertToProvidersTest(planJSON);
    result = PlanEmailConverterUtils.convertToProviders(planJSON);
    Assert.assertEquals(1, result.size());
    PlanEmailProviderBean provider = result.get(0);
    Assert.assertEquals("monkey", provider.getType());
    Assert.assertTrue(provider.isInNetwork());
    Assert.assertFalse(provider.isPersonalName());
    Assert.assertEquals("-", provider.getLocationName());
    
    planJSON = new JSONObject();
    providersArray = new JSONArray();
    JSONObject providerObject = new JSONObject("{ provider : { type : \"physician\", name : { prefix : \"Dr.\", first : \"Joe\", middle : \"Matthew\", last : \"Smith\" } }, inNetwork : true }");
    providersArray.put(providerObject);
    planJSON.put("providers", providersArray);
    result = PlanEmailConverterUtils.convertToProviders(planJSON);
    Assert.assertEquals(1, result.size());
    provider = result.get(0);
    Assert.assertEquals("physician", provider.getType());
    Assert.assertTrue(provider.isPersonalName());
    Assert.assertEquals("Joe", provider.getFirstName());
    Assert.assertEquals("Matthew", provider.getMiddleName());
    Assert.assertEquals("Smith", provider.getLastName());
    Assert.assertEquals("Dr.", provider.getPrefix());
    Assert.assertNull(provider.getLocationName());
  }

  static void populatePlanJSONForConvertToProvidersTest(JSONObject planJSON)
    throws JSONException
  {
    JSONArray providersArray = new JSONArray();
    JSONObject providerObject = new JSONObject("{ provider : { type : \"monkey\" }, inNetwork : true }");
    providersArray.put(providerObject);
    planJSON.put("providers", providersArray);
  }
  
  @Test
  public void testConvertToPreferenceMatches() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.convertToPreferenceMatches(null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    JSONObject planJSON = new JSONObject();
    List<PlanEmailPreferenceMatch> result = PlanEmailConverterUtils.convertToPreferenceMatches(planJSON);
    Assert.assertTrue(result.isEmpty());

    planJSON.put("preferenceMatches", new JSONArray());
    result = PlanEmailConverterUtils.convertToPreferenceMatches(planJSON);
    Assert.assertTrue(result.isEmpty());
    
    populatePlanJSONForConvertToPreferenceMatchesTest(planJSON);
    result = PlanEmailConverterUtils.convertToPreferenceMatches(planJSON);
    Assert.assertEquals(1, result.size());
    PlanEmailPreferenceMatch preferenceMatch = result.get(0);
    Assert.assertEquals("Label", preferenceMatch.getDescription());
    Assert.assertTrue(preferenceMatch.isMatch());
  }

  static void populatePlanJSONForConvertToPreferenceMatchesTest(JSONObject planJSON)
    throws JSONException
  {
    JSONArray preferenceMatchesArray = new JSONArray();
    JSONObject preferenceMatchesObject = new JSONObject("{ label : \"Label\", value : \"True\" }");
    preferenceMatchesArray.put(preferenceMatchesObject);
    planJSON.put("preferenceMatches", preferenceMatchesArray);
  }
  
  @Test
  public void testConvertToEstimatedCostBean() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.convertToEstimatedCostBean(100.0d, null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    JSONObject planJSON = new JSONObject();
    PlanEmailEstimatedCostBean result = PlanEmailConverterUtils.convertToEstimatedCostBean(100.0d, planJSON);
    Assert.assertFalse(result.isEstimated());
    
    planJSON.put("estimatedCost", 500.0d);
    result = PlanEmailConverterUtils.convertToEstimatedCostBean(100.0d, planJSON);
    Assert.assertFalse(result.isEstimated());

    planJSON.put("maximumCost", 1000.0d);
    result = PlanEmailConverterUtils.convertToEstimatedCostBean(100.0d, planJSON);
    Assert.assertTrue(result.isEstimated());
    Assert.assertEquals(result.getMinimumCost(), NumberFormat.getCurrencyInstance().format(1200.0d));
    Assert.assertEquals(result.getMaximumCost(), NumberFormat.getCurrencyInstance().format(2200.0d));
    Assert.assertEquals(result.getExpectedCost(), NumberFormat.getCurrencyInstance().format(1700.0d));
    Assert.assertEquals(40 + (60 * -700d) / 1000, result.getEstimatedCostPercentage(), 0.01d);
  }
  
  @Test
  public void testCalculateCost()
  {
    double result = PlanEmailConverterUtils.calculateCost(100.0d, 50.0d);
    Assert.assertEquals(50.0d, result, 0.01d);
    result = PlanEmailConverterUtils.calculateCost(100.0d, 200.0d);
    Assert.assertEquals(0.0d, result, 0.01d);
  }
  
  @Test
  public void testPerPaycheckAmounts() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.setPerPaycheckAmounts(null, null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    PlanEmailBean resultBean = new PlanEmailBean();
    JSONObject costJSON = new JSONObject();
    
    PlanEmailConverterUtils.setPerPaycheckAmounts(costJSON, resultBean);
    Assert.assertNull(resultBean.getPaycheckRate());
    
    populateCostJSONForPerPaycheckAmounts(costJSON);
    PlanEmailConverterUtils.setPerPaycheckAmounts(costJSON, resultBean);
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(50.0d), resultBean.getPaycheckRate());
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(25.0d), resultBean.getPaycheckContribution());
    double paycheckCost = PlanEmailConverterUtils.calculateCost(50.0d, 25.0d);
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(paycheckCost), resultBean.getPaycheckCost());
  }
  
  @Test
  public void testApplyPlanCostData() throws JSONException
  {
    try 
    {
      PlanEmailConverterUtils.applyPlanCostData(null, null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    PlanEmailBean resultBean = new PlanEmailBean();
    JSONObject planJSON = new JSONObject();
    
    try 
    {
      PlanEmailConverterUtils.applyPlanCostData(planJSON, resultBean);
      Assert.fail("JSONException expected");
    }
    catch (JSONException e)
    {
      // This is expected
    }
    
    populatePlanJSONForTestApplyPlanCostData(planJSON);
    PlanEmailConverterUtils.applyPlanCostData(planJSON, resultBean);
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(100.0d), resultBean.getMonthlyRate());
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(50.0d), resultBean.getMonthlyContribution());
    double monthlyCost = PlanEmailConverterUtils.calculateCost(100.0d, 50.0d);
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(monthlyCost), resultBean.getMonthlyCost());
  }

  static void populatePlanJSONForTestApplyPlanCostData(JSONObject planJSON) throws JSONException
  {
    JSONObject costJSON = new JSONObject();
    JSONObject monthlyCostJSON = new JSONObject("{ rate : { amount : 100.0 }, contribution : { amount : 50.0 }}");
    costJSON.put("monthly", monthlyCostJSON);
    planJSON.put("cost", costJSON);
  }

  static void populateCostJSONForPerPaycheckAmounts(JSONObject costJSON) throws JSONException
  {
    costJSON.put("perPaycheck", new JSONObject("{ rate : { amount : 50.0 }, contribution : { amount : 25.0 } }"));
  }
  
  @Test
  public void testConvertToPlanEmailBean() throws JSONException
  {
    try
    {
      PlanEmailConverterUtils.convertToPlanEmailBean(null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    JSONObject planJSON = new JSONObject();
    try
    {
      PlanEmailConverterUtils.convertToPlanEmailBean(planJSON);
      Assert.fail("JSONException expected");
    }
    catch (JSONException e)
    {
      // This is expected
    }
    
    populatePlanJSONForConvertToPlanEmailBeanTest(planJSON);

    PlanEmailBean resultBean = PlanEmailConverterUtils.convertToPlanEmailBean(planJSON);
    Assert.assertEquals("Name", resultBean.getPlanName());
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(100.0d), resultBean.getMonthlyRate());
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(50.0d), resultBean.getMonthlyContribution());
    double monthlyCost = PlanEmailConverterUtils.calculateCost(100.0d, 50.0d);
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(monthlyCost), resultBean.getMonthlyCost());

    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(50.0d), resultBean.getPaycheckRate());
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(25.0d), resultBean.getPaycheckContribution());
    double paycheckCost = PlanEmailConverterUtils.calculateCost(50.0d, 25.0d);
    Assert.assertEquals(NumberFormat.getCurrencyInstance().format(paycheckCost), resultBean.getPaycheckCost());
    
    PlanEmailEstimatedCostBean estimatedCostBean = resultBean.getEstimatedCost();
    Assert.assertTrue(estimatedCostBean.isEstimated());
    Assert.assertEquals(estimatedCostBean.getMinimumCost(), NumberFormat.getCurrencyInstance().format(1200.0d));
    Assert.assertEquals(estimatedCostBean.getMaximumCost(), NumberFormat.getCurrencyInstance().format(2200.0d));
    Assert.assertEquals(estimatedCostBean.getExpectedCost(), NumberFormat.getCurrencyInstance().format(1700.0d));
    Assert.assertEquals(40 + (60 * -700d) / 1000, estimatedCostBean.getEstimatedCostPercentage(), 0.01d);
    
    Assert.assertEquals(1, resultBean.getBenefitCategories().size());
    Assert.assertEquals(1, resultBean.getPreferenceMatches().size());
    Assert.assertEquals(1, resultBean.getInNetworkProviders().size());
    Assert.assertEquals(1, resultBean.getOutOfNetworkProviders().size());
  }

  static void populatePlanJSONForConvertToPlanEmailBeanTest(JSONObject planJSON)
    throws JSONException
  {
    planJSON.put("name", "Name");
    
    populatePlanJSONForTestApplyPlanCostData(planJSON);
    
    planJSON.put("estimatedCost", 500.0d);
    planJSON.put("maximumCost", 1000.0d);

    populateCostJSONForPerPaycheckAmounts(planJSON.getJSONObject("cost"));
    
    populatePlanJSONForConvertToBenefitCategoriesTest(planJSON);
    
    populatePlanJSONForConvertToPreferenceMatchesTest(planJSON);

    populatePlanJSONForConvertToProvidersTest(planJSON);
    JSONArray providersArray = planJSON.getJSONArray("providers");
    JSONObject providerObject = new JSONObject("{ provider : { type : \"physician\", name : { prefix : \"Dr.\", first : \"Joe\", middle : \"Matthew\", last : \"Smith\" } }, inNetwork : false }");
    providersArray.put(providerObject);
  }
}
