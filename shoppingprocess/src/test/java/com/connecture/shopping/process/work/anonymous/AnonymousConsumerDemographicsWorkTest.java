package com.connecture.shopping.process.work.anonymous;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;



import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.common.account.SessionAccountImpl;




@RunWith(MockitoJUnitRunner.class)
public class AnonymousConsumerDemographicsWorkTest
{
 
  @Spy
  @InjectMocks
  AnonymousConsumerDemographicsWork anonymousConsumerDemographicsWork; 

  Account persistedAccountImpl;
  JSONObject accountJSON;
  JSONObject persistedAccountJSON;
  SimpleDateFormat sdf;

  

  @Before
  public void setUp()
  {
    accountJSON = new JSONObject();
    persistedAccountJSON = new JSONObject();
    persistedAccountImpl = Mockito.spy(new SessionAccountImpl());
    anonymousConsumerDemographicsWork.setPersistedAccountImpl(persistedAccountImpl);
    sdf = new SimpleDateFormat("MM/dd/yyyy");
  }
  
  
  /**
     * @throws Exception
     * Method under test: setCoverageTypeAndEffectiveDate1()
     * Scenario: persistedAccountJSON has DEN CoverageType set in   
     * Expectation: accountJSON has coverageType key set in
     * with value DEN
     */
  @Test
  public void testSetCoverageTypeAndEffectiveDate1_CoverageDEN() throws Exception {
    setUpJSONObjects();
    persistedAccountJSON.put("coverageType", "DEN");
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getAccount(Matchers.anyString());
    anonymousConsumerDemographicsWork.setCoverageTypeAndEffectiveDate1(accountJSON);
    Assert.assertTrue(accountJSON.has("coverageType"));
    Assert.assertEquals("DEN" , accountJSON.get("coverageType") );
    
  }
  
  /**
     * @throws Exception
     * Method under test: setCoverageTypeAndEffectiveDate1()
     * Scenario: persistedAccountJSON has STH CoverageType set in   
     * Expectation: accountJSON has coverageType key set in
     * with value as STH
     */
  @Test
  public void testSetCoverageTypeAndEffectiveDate1_CoverageSTH() throws Exception {
    setUpJSONObjects();
    persistedAccountJSON.put("coverageType", "STH");
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getAccount(Matchers.anyString());
    anonymousConsumerDemographicsWork.setCoverageTypeAndEffectiveDate1(accountJSON);
    Assert.assertTrue(accountJSON.has("coverageType"));
    Assert.assertEquals("STH" , accountJSON.get("coverageType") );
    
  }
  
  /**
     * @throws Exception
     * Method under test: setCoverageTypeAndEffectiveDate1()
     * Scenario: persistedAccountJSON hasn't CoverageType set in   
     * Expectation: accountJSON doesn't have coverageType key set in
     */
  @Test
  public void testSetCoverageTypeAndEffectiveDate1_CoverageNone() throws Exception {
    setUpJSONObjects();
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getAccount(Matchers.anyString());
    anonymousConsumerDemographicsWork.setCoverageTypeAndEffectiveDate1(accountJSON);
    Assert.assertTrue(! accountJSON.has("coverageType"));
     
  }
  
  /**
     * @throws Exception
     * Method under test: setCoverageTypeAndEffectiveDate1()
     * Scenario: persistedAccountJSON has STH CoverageType & effectiveMs set in   
     * Expectation: accountJSON has coverageType set in as STH & effectiveDate1 as MM/dd/yyyy format date from effectiveMs
     */
  @Test
  public void testSetCoverageTypeAndEffectiveDate1_EffectiveMsPresent() throws Exception {
    setUpJSONObjects();
    Date now = new Date();
    persistedAccountJSON.put("coverageType", "STH");
    persistedAccountJSON.put("effectiveMs", now.getTime() );
    Mockito.doReturn(persistedAccountJSON).when(persistedAccountImpl).getAccount(Matchers.anyString());
    anonymousConsumerDemographicsWork.setCoverageTypeAndEffectiveDate1(accountJSON);
    Assert.assertTrue(accountJSON.has("effectiveDate1"));
    Assert.assertEquals(sdf.format(now) , accountJSON.get("effectiveDate1") );
    
  }

  public void setUpJSONObjects() throws Exception{
    accountJSON.put("effectiveDate", "07/01/2014");
    accountJSON.put("effectiveMs", 1404153000000L);
    accountJSON.put("zipCode", "68501");
    accountJSON.put("countyId", "0");
    accountJSON.put("userRole", "anonymous");
    accountJSON.put("tagUrl", "//nexus.ensighten.com/nebraskablue/qa/Bootstrap.js");
    
    Map<String, Object> members = new HashMap<String, Object>();
    members.put("isNew", true);
    members.put("memberRefName", 299);
    
    JSONObject nameJSON = new JSONObject();
    nameJSON.put("first", "rewr");
    members.put("name", nameJSON);
    members.put("birthDate", "01/02/1987");
    members.put("gender", "M");
    members.put("memberRelationship", "MEMBER");
    members.put("relationshipKey", "PRIMARY");
    members.put("isSmoker", "n");
    JSONArray membersJSONArray = new JSONArray();
    membersJSONArray.put(members);
    accountJSON.put("members", membersJSONArray);
    
    persistedAccountJSON.put("transactionId", "IF_Lead_269");
    
  }
  
  @After
  public void tearDown()
  {
    accountJSON = null;
    persistedAccountJSON = null;
    persistedAccountImpl = null;
    anonymousConsumerDemographicsWork = null;
    sdf = null;
  }
}