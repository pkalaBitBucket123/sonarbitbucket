package com.connecture.shopping.process.test;

public class ValueHolder<T>
{
  private T value;
  
  public ValueHolder(T value) 
  {
    this.value = value;
  }

  public T getValue()
  {
    return value;
  }

  public void setValue(T value)
  {
    this.value = value;
  }

  @Override
  public String toString()
  {
    return String.valueOf(value);
  }

  @Override
  public int hashCode()
  {
    return value.hashCode();
  }

  @Override
  public boolean equals(Object obj)
  {
    return value == obj || null != value && value.equals(obj);
  }
}
