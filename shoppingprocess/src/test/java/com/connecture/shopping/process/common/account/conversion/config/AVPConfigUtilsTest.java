package com.connecture.shopping.process.common.account.conversion.config;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.common.account.conversion.config.AVPConfig;
import com.connecture.shopping.process.common.account.conversion.config.AVPConfigUtils;
import com.connecture.shopping.process.common.account.conversion.converter.AVPAddConverter;
import com.connecture.shopping.process.common.account.conversion.converter.AVPConverter;
import com.connecture.shopping.process.common.account.conversion.converter.AVPDeleteConverter;

public class AVPConfigUtilsTest
{
  private static Logger LOG = Logger.getLogger(AVPConfigUtilsTest.class);

  @Test
  public void validateEmptyObject() throws JSONException
  {
    JSONObject jsonObject = new JSONObject("{}");

    List<String> errors = AVPConfigUtils.validateJSON(jsonObject);
    Assert.assertEquals("Errors expected", 2, errors.size());
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Expected errors detected:\n" + errors);
    }
  }

  @Test
  /**
   * It is valid to not have any converters defined.  It would represent 
   * nothing having changed between versions.
   */
  public void validateNoConverters() throws JSONException
  {
    String json = "{ newVersion : \"2.0\", previousVersion : \"1.0\", converters : [] }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConfigUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }

  @Test
  public void validateSingleConverter() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPConfigUtilsTest.class, "testSingleConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConfigUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }

  @Test
  public void validateBadConverter() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPConfigUtilsTest.class, "testBadConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConfigUtils.validateJSON(jsonObject);
    Assert.assertEquals(1, errors.size());
  }

  @Test
  public void validateMultipleConverter() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPConfigUtilsTest.class, "testMultipleConverters.json");

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConfigUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }
  
  @Test
  public void valueOfEmptyObject() throws JSONException
  {
    JSONObject jsonObject = new JSONObject("{}");

    try
    {
      AVPConfigUtils.valueOf(jsonObject);
      
      Assert.fail("Expected errors");
    }
    catch (JSONException e) 
    {
      // This is expected, as there should be errors
      // Not going to check specific errors here
    }
  }
  
  @Test
  /**
   * It is valid to not have any converters defined.  It would represent 
   * nothing having changed between versions.
   */
  public void valueOfNoConverters() throws JSONException
  {
    String json = "{ newVersion : \"2.0\", previousVersion : \"1.0\", converters : [] }";

    JSONObject jsonObject = new JSONObject(json);
    
    AVPConfig config = AVPConfigUtils.valueOf(jsonObject);
    
    Assert.assertEquals("2.0", config.getNewVersion());
    Assert.assertEquals("1.0", config.getPreviousVersion());
    Assert.assertEquals(0, config.getConverters().size());
  }
  
  @Test
  public void valueOfSingleConverter() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPConfigUtilsTest.class, "testSingleConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPConfig config = AVPConfigUtils.valueOf(jsonObject);
    Assert.assertEquals(1, config.getConverters().size());
    AVPConverter converter = config.getConverters().get(0);
    Assert.assertTrue(converter instanceof AVPDeleteConverter);
  }
  
  @Test
  public void valueOfMultipleConverter() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPConfigUtilsTest.class, "testMultipleConverters.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPConfig config = AVPConfigUtils.valueOf(jsonObject);
    Assert.assertEquals(2, config.getConverters().size());
    AVPConverter converter = config.getConverters().get(0);
    Assert.assertTrue(converter instanceof AVPDeleteConverter);
    converter = config.getConverters().get(1);
    Assert.assertTrue(converter instanceof AVPAddConverter);
  }
}
