package com.connecture.shopping.process.service.data.converter;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.connecture.shopping.process.IntegrationException;
import com.connecture.shopping.process.service.data.ProductPlanData;

public class ProductDataConverterTest
{

  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void testConvertData() throws Exception
  {
    ProductDataConverter converter = new ProductDataConverter();

    String json = FileUtils.readFileToString(FileUtils.toFile(this.getClass().getResource(
      "mlrPlanTestData.json")));

    testCreatePlanData(converter, json);
  }

  @Test
  public void testCreatePlanDataEmptyJSONArray() throws Exception
  {
    ProductDataConverter converter = new ProductDataConverter();
    // empty JSON Array
    ProductPlanData productPlanData = converter.convertData("[]");

    Assert.assertTrue(productPlanData.getPlans().isEmpty());
    Assert.assertTrue(productPlanData.getPaychecksPerYear() == null);
  }

  @Test
  public void testCreatePlanDataJSONObject() throws Exception
  {
    ProductDataConverter converter = new ProductDataConverter();
    // empty JSON object, but shoudn't matter if there's anything in there or
    // not
    exception.expect(IntegrationException.class);
    converter.convertData("{}");
  }

  @Test
  public void testCreatePlanDataEmptyString() throws Exception
  {
    ProductDataConverter converter = new ProductDataConverter();
    // empty String
    ProductPlanData productPlanData = converter.convertData("");

    Assert.assertTrue(productPlanData.getPlans().isEmpty());
    Assert.assertTrue(productPlanData.getPaychecksPerYear() == null);
  }

  @Test
  public void testCreatePlanDataNull() throws Exception
  {
    ProductDataConverter converter = new ProductDataConverter();

    // null
    ProductPlanData productPlanData = converter.convertData(null);

    Assert.assertTrue(productPlanData.getPlans().isEmpty());
    Assert.assertTrue(productPlanData.getPaychecksPerYear() == null);
  }

  @Test
  public void testCreatePlanDataNullString() throws Exception
  {
    ProductDataConverter converter = new ProductDataConverter();

    // Yeah, this really happens. This is typically what we get back from
    // product server when things
    // don't quite work out.

    // null string
    ProductPlanData productPlanData = converter.convertData("null");

    Assert.assertTrue(productPlanData.getPlans().isEmpty());
    Assert.assertTrue(productPlanData.getPaychecksPerYear() == null);
  }

  private void testCreatePlanData(ProductDataConverter converter, String productData)
    throws Exception
  {
    converter.convertData(productData);

    // TODO | assert
  }

}
