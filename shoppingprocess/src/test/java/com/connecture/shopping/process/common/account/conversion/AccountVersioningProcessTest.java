package com.connecture.shopping.process.common.account.conversion;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class AccountVersioningProcessTest
{
  @Test
  public void loadEmptyConfiguration() throws JSONException
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testEmptyConfiguration.json");
    process.setAVPConverterDataProvider(dataProvider);
 
    process.loadConverterConfigurationChain();
    Assert.assertEquals(0, process.getAVPConfigChain().getConfigurations().size());
  }

  @Test
  public void loadBadConfiguration() throws JSONException
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testBadConfiguration.json");
    process.setAVPConverterDataProvider(dataProvider);

    process.loadConverterConfigurationChain();
    Assert.assertEquals(0, process.getAVPConfigChain().getConfigurations().size());
  }

  @Test
  public void loadMixedGoodAndBadConfiguration() throws JSONException
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testGoodAndBadConfigurations.json");
    process.setAVPConverterDataProvider(dataProvider);

    process.loadConverterConfigurationChain();
    Assert.assertEquals(1, process.getAVPConfigChain().getConfigurations().size());
  }

  @Test
  public void loadSingleConfiguration() throws JSONException
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testSingleConfiguration.json");
    process.setAVPConverterDataProvider(dataProvider);
    
    process.loadConverterConfigurationChain();
    Assert.assertEquals(1, process.getAVPConfigChain().getConfigurations().size());
  }

  @Test
  public void loadMultipleConfigurations() throws JSONException
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testMultipleConfigurations.json");
    process.setAVPConverterDataProvider(dataProvider);
    
    process.loadConverterConfigurationChain();
    Assert.assertEquals(2, process.getAVPConfigChain().getConfigurations().size());
    Assert.assertEquals(2, process.getAVPConfigChain().getConfigurationsFor("3.0").size());
  }

  @Test
  public void loadMissingConfiguration() throws JSONException
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testDoesn'tExist.json");
    process.setAVPConverterDataProvider(dataProvider);
    
    process.loadConverterConfigurationChain();
    Assert.assertEquals(0, process.getAVPConfigChain().getConfigurations().size());
  }
  
  @Test
  public void convertAccountSingleConversion() throws Exception
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testSingleConfiguration.json");
    process.setAVPConverterDataProvider(dataProvider);
    
    AVPAccountInfoTestDataProvider accountInfoDataProvider = new AVPAccountInfoTestDataProvider();
    accountInfoDataProvider.setDataFile("testAccounts.json");
    process.setAccountInfoDataProvider(accountInfoDataProvider);
    
    AVPAccountInfoTestDataStore accountInfoDataStore = new AVPAccountInfoTestDataStore();
    process.setAccountInfoDataStore(accountInfoDataStore);
    
    process.setConvertToVersion("2.0");
    
    process.versionAccounts();
    Assert.assertEquals(1, process.getAVPConfigChain().getConfigurations().size());
    Assert.assertEquals(1, process.getAVPConfigChain().getConfigurationsFor("2.0").size());
    Assert.assertEquals(1, process.getAccountsToConvert().size());
    Assert.assertEquals(0, process.getErroredAccounts().size());
    Assert.assertEquals(1, process.getConvertedAccounts().size());
    
    // Now, did the conversion actually do anything?
    JSONObject accountObject = process.getConvertedAccounts().get(0).getValue();
    
    Assert.assertFalse(accountObject.has("questions"));
    
    JSONArray membersArray = accountObject.getJSONArray("members");
    for (int m = 0; m < membersArray.length(); m++)
    {
      Assert.assertFalse(membersArray.getJSONObject(m).has("nameData"));
    }
    
    // Now verify the convertedAccounts were stored
    Assert.assertEquals(1, accountInfoDataStore.getAccountInfo().size());
  }
  
  @Test
  public void convertAccountMultipleConversions() throws Exception
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testMultipleConfigurations.json");
    process.setAVPConverterDataProvider(dataProvider);
    
    AVPAccountInfoTestDataProvider accountInfoDataProvider = new AVPAccountInfoTestDataProvider();
    accountInfoDataProvider.setDataFile("testAccounts.json");
    process.setAccountInfoDataProvider(accountInfoDataProvider);
    
    AVPAccountInfoTestDataStore accountInfoDataStore = new AVPAccountInfoTestDataStore();
    process.setAccountInfoDataStore(accountInfoDataStore);
    
    process.setConvertToVersion("2.0");
    
    process.versionAccounts();
    Assert.assertEquals(2, process.getAVPConfigChain().getConfigurations().size());
    Assert.assertEquals(1, process.getAVPConfigChain().getConfigurationsFor("2.0").size());
    Assert.assertEquals(1, process.getAccountsToConvert().size());
    Assert.assertEquals(0, process.getErroredAccounts().size());
    Assert.assertEquals(1, process.getConvertedAccounts().size());
    
    // Now, did the conversion actually do anything?
    JSONObject accountObject = process.getConvertedAccounts().get(0).getValue();
    
    Assert.assertFalse(accountObject.has("questions"));
    
    JSONArray membersArray = accountObject.getJSONArray("members");
    for (int m = 0; m < membersArray.length(); m++)
    {
      Assert.assertFalse(membersArray.getJSONObject(m).has("nameData"));
    }
    
    // Now verify the convertedAccounts were stored
    Assert.assertEquals(1, accountInfoDataStore.getAccountInfo().size());
  }
  
  @Test
  public void convertBadAccount() throws Exception
  {
    AccountVersioningProcess process = new AccountVersioningProcess();
    
    AccountVersioningConverterDataProvider dataProvider = new AccountVersioningConverterDataProvider();
    dataProvider.setDataFile("testSingleConfiguration.json");
    process.setAVPConverterDataProvider(dataProvider);
    
    AVPAccountInfoTestDataProvider accountInfoDataProvider = new AVPAccountInfoTestDataProvider();
    accountInfoDataProvider.setDataFile("testGoodAndBadAccounts.json");
    process.setAccountInfoDataProvider(accountInfoDataProvider);
    
    AVPAccountInfoTestDataStore accountInfoDataStore = new AVPAccountInfoTestDataStore();
    process.setAccountInfoDataStore(accountInfoDataStore);
    
    process.setConvertToVersion("2.0");
    
    process.versionAccounts();
    Assert.assertEquals(2, process.getAccountsToConvert().size());
    Assert.assertEquals(1, process.getErroredAccounts().size());
    Assert.assertEquals(1, process.getConvertedAccounts().size());
  }
}
