package com.connecture.shopping.process.work;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.camel.request.BaseCamelRequest;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public class CamelRequestingDataProviderTest
{

  @Test
  public void test() throws Exception
  {
    @SuppressWarnings("unchecked")
    BaseCamelRequest<String> request = Mockito.mock(BaseCamelRequest.class);
    Mockito.when(request.submitRequest()).thenReturn("Test");
    DataProvider<String> provider = new CamelRequestingDataProvider<String>(request);
    Assert.assertEquals("Test", provider.fetchData());
  }
}
