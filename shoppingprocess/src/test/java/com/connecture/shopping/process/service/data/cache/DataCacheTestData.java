package com.connecture.shopping.process.service.data.cache;

public class DataCacheTestData
{
  private String value;

  public String getValue()
  {
    return value;
  }

  public void setValue(String value)
  {
    this.value = value;
  }
}
