package com.connecture.shopping.process.work.individual;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.work.core.ShoppingRulesEngineBean;

public class SubsidyUsageWorkTest
{
  @Test
  public void testGet() throws Exception
  {
    // need to get a little tricky for effectiveDate rule call
    SubsidyUsageWork subsidyUsageWork = Mockito.spy(new SubsidyUsageWork());

//    ShoppingRulesEngineBean rulesEngineBean = 
      new ShoppingRulesEngineBean();
    //subsidyUsageWork.setShoppingSubsidyRulesEngineBean(rulesEngineBean);
    
    // mock out effectiveDate rules call
    //Mockito.doReturn("01/01/2014").when(subsidyUsageWork).getEffectiveDate(Mockito.any(JSONObject.class));
    Mockito.doReturn(this.getMockAccountData()).when(subsidyUsageWork).getAccountJSON();
    
    subsidyUsageWork.setInputJSON(getMockProductData());
    // subsidyUsageWork.get();
  }
  
  private JSONObject getMockProductData() throws JSONException, IOException
  {
    String json = FileUtils.readFileToString(FileUtils.toFile(this.getClass().getResource(
      "shoppingProductDataIFP.json")));
    
    JSONObject returnJSON = new JSONObject(json);
    return returnJSON;
  };
  
  private JSONObject getMockAccountData() throws JSONException, IOException
  {
    String json = FileUtils.readFileToString(FileUtils.toFile(this.getClass().getResource(
      "subsidyAccountData.json")));
    
    JSONObject accountJSON = new JSONObject(json);
    return accountJSON;
  }
}
