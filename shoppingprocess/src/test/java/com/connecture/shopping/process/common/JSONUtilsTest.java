package com.connecture.shopping.process.common;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class JSONUtilsTest
{
  @Test
  public void testIterator()
  {
    JSONArray array = new JSONArray();
    
    // Let's presume all objects in the array are JSONObject
    Iterator<JSONObject> iterator = JSONUtils.iterator(array);
    
    Assert.assertFalse(iterator.hasNext());
    try
    {
      iterator.next();
    }
    catch (NoSuchElementException e)
    {
      // This is expected
    }
    try
    {
      iterator.remove();
    }
    catch (IllegalStateException e)
    {
      // This is expected
    }
    
    array.put(new JSONObject());
    iterator = JSONUtils.iterator(array);
    Assert.assertTrue(iterator.hasNext());
    JSONObject jsonObject = iterator.next();
    Assert.assertNotNull(jsonObject);
    
    array.put(5);
    iterator = JSONUtils.iterator(array);
    Assert.assertTrue(iterator.hasNext());
    jsonObject = iterator.next();
    try 
    {
      jsonObject = iterator.next();
    }
    catch (ClassCastException e)
    {
      // This is expected
    }
    
    // We can also just make an Object iterator, if we are unsure of the
    //   types in the array
    array = new JSONArray();
    Iterator<Object> objectIterator = JSONUtils.iterator(array);
    
    Assert.assertFalse(objectIterator.hasNext());
    try
    {
      objectIterator.next();
    }
    catch (NoSuchElementException e)
    {
      // This is expected
    }
    try
    {
      objectIterator.remove();
    }
    catch (IllegalStateException e)
    {
      // This is expected
    }
    
    array.put(new JSONObject());
    objectIterator = JSONUtils.iterator(array);
    Assert.assertTrue(objectIterator.hasNext());
    Object object = objectIterator.next();
    Assert.assertTrue(object instanceof JSONObject);
    Assert.assertNotNull(object);
    
    array.put(5);
    objectIterator = JSONUtils.iterator(array);
    Assert.assertTrue(objectIterator.hasNext());
    object = objectIterator.next();
    object = objectIterator.next();
    Assert.assertTrue(object instanceof Integer);
    Assert.assertNotNull(object);
  }
  
  @Test
  public void testIterable()
  {
    JSONArray array = new JSONArray();
    array.put(new JSONObject());
    int count = 0;
    for (JSONObject object : JSONUtils.toIterable(JSONObject.class, array))
    {
      Assert.assertNotNull(object);
      count++;
    }
    Assert.assertEquals(1, count);
  }
}
