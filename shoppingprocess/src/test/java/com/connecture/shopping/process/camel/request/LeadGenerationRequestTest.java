package com.connecture.shopping.process.camel.request;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.data.ShoppingLead;
import com.connecture.model.integration.data.ShoppingToIAData;

public class LeadGenerationRequestTest
{
  @Test
  public void testMisc()
  {
    LeadGenerationRequest request = new LeadGenerationRequest();
    Assert.assertNull(request.getProducer());
    request.setTransactionId("TEST");
    Assert.assertEquals("TEST", request.getRequestHeaders().get(BaseCamelRequest.TRANSACTION_ID_KEY));
    ShoppingToIAData toIAData = Mockito.mock(ShoppingToIAData.class);
    request.setShoppingToIAData(toIAData);
    Assert.assertEquals(toIAData, request.getRequestHeaders().get(LeadGenerationRequest.SHOPPING_DATA_KEY));
    ShoppingLead leadData = Mockito.mock(ShoppingLead.class);
    request.setShoppingLead(leadData);
    Assert.assertEquals(leadData, request.getRequestHeaders().get(LeadGenerationRequest.SHOPPING_LEAD_KEY));
  }
}
