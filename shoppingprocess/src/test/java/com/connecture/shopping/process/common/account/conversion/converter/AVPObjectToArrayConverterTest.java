package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPObjectToArrayConverterTest
{
  private static Logger LOG = Logger.getLogger(AVPObjectToArrayConverterTest.class);

  @Test
  public void validateBadObjectToArrayConverter() throws JSONException
  {
    String json = "{ type : \"objectToArray\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals("Errors expected", 2, errors.size());
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Expected errors detected:\n" + errors);
    }
  }

  @Test
  public void validateObjectToArrayConverter() throws JSONException
  {
    String json = "{ type : \"objectToArray\", path : \"cart.*.product\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }

  @Test
  public void createObjectToArrayConverter() throws JSONException
  {
    String json = "{ type : \"objectToArray\", path : \"cart.productLines.*.product\",  reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    AVPObjectToArrayConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    Assert.assertEquals(AVPConverterType.OBJECT_TO_ARRAY, converter.getType());
    Assert.assertEquals("cart.productLines.*.product", converter.getPath());
    Assert.assertEquals("reason", converter.getReason());
  }

  @Test
  public void objectToArrayConversionNoCart() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPObjectToArrayConverter.class, "testObjectToArrayConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPObjectToArrayConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    jsonObject = new JSONObject("{\"test\":{}}");
    
    if (LOG.isDebugEnabled())
    {
      LOG.debug("objectToArrayConversion BEFORE:\n" + jsonObject.toString(4));
    }
    
    converter.apply(jsonObject);

    if (LOG.isDebugEnabled())
    {
      LOG.debug("objectToArrayConversion AFTER:\n" + jsonObject.toString(4));
    }
    
    Assert.assertEquals(0, JSONPathUtil.count(jsonObject, converter.getPath()));
  }
  
  @Test
  public void objectToArrayConversion() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPObjectToArrayConverter.class, "testObjectToArrayConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPObjectToArrayConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    jsonObject = new JSONObject(JSONFileUtils.readJSONData(AVPObjectToArrayConverter.class,
      "objectToArrayConversion.json"));
    
    if (LOG.isDebugEnabled())
    {
      LOG.debug("objectToArrayConversion BEFORE:\n" + jsonObject.toString(4));
    }
    
    converter.apply(jsonObject);

    if (LOG.isDebugEnabled())
    {
      LOG.debug("objectToArrayConversion AFTER:\n" + jsonObject.toString(4));
    }
    
    Assert.assertEquals(4, JSONPathUtil.count(jsonObject, converter.getPath()));
  }
  
  @Test
  public void objectToArrayConversionWithRename() throws JSONException
  {
    //String json = JSONFileUtils.readJSONData(AVPObjectToArrayConverter.class, "testObjectToArrayConverter.json");

    String json = "{ type : \"objectToArray\", path : \"cart.productLines.*.product\",  reason : \"reason\", rename : \"products\"}";
    
    JSONObject jsonObject = new JSONObject(json);
    AVPObjectToArrayConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    jsonObject = new JSONObject(JSONFileUtils.readJSONData(AVPObjectToArrayConverter.class,
      "objectToArrayConversion.json"));
    
    if (LOG.isDebugEnabled())
    {
      LOG.debug("objectToArrayConversion BEFORE:\n" + jsonObject.toString(4));
    }
    
    converter.apply(jsonObject);

    if (LOG.isDebugEnabled())
    {
      LOG.debug("objectToArrayConversion AFTER:\n" + jsonObject.toString(4));
    }
    
    Assert.assertEquals(4, JSONPathUtil.count(jsonObject, "cart.productLines.*.products"));
  }
}
