package com.connecture.shopping.process.common;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class JSONFileUtilsTest
{
  @Test
  public void readJSONDataMissingFile()
  {
    String results = JSONFileUtils.readJSONData(JSONFileUtilsTest.class, "missingfile.json");
    Assert.assertEquals("{}", results);
  }
  
  @Test
  public void readJSONData() throws JSONException
  {
    String results = JSONFileUtils.readJSONData(JSONFileUtilsTest.class, "testData.json");
    Assert.assertFalse("{}".equals(results));
    Assert.assertEquals("test", new JSONObject(results).getString("value"));
  }
}
