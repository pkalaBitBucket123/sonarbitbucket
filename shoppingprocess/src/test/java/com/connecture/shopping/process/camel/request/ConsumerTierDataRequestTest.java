package com.connecture.shopping.process.camel.request;

import java.util.ArrayList;
import java.util.Map;

import junit.framework.Assert;

import org.json.JSONObject;
import org.junit.Test;

import com.connecture.model.data.ShoppingMember;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class ConsumerTierDataRequestTest
{

  @Test
  public void testGetRequestHeaders()
  {
    ConsumerTierDataRequest request = new ConsumerTierDataRequest();
    request.setTransactionId("1");
    request.setContext(ShoppingContext.EMPLOYEE);
    request.setShoppingMembers(new ArrayList<ShoppingMember>());
    request.setMemberJSON(new JSONObject());
    request.setEligibilityGroupId(1l);
    
    Map<String,Object> result = request.getRequestHeaders();
    Assert.assertEquals("1", String.valueOf(result.get(CamelRequestParameters.TRANSACTION_ID_KEY)));
    Assert.assertEquals(ShoppingContext.EMPLOYEE.getContextName(), result.get(ConsumerTierDataRequest.CONTEXT_KEY));
    Assert.assertNotNull(result.get(ConsumerTierDataRequest.SHOPPING_MEMBERS_KEY));
    Assert.assertNotNull(result.get(ConsumerTierDataRequest.MEMBER_JSON_KEY));
    Assert.assertEquals(1l, ((Long)result.get(ConsumerTierDataRequest.ELIGIBILITY_GROUP_ID_KEY)).longValue());
  }
  
  @Test
  public void testMisc()
  {
    ConsumerTierDataRequest request = new ConsumerTierDataRequest();
    Assert.assertNull(request.getProducer());
    Assert.assertNull(request.getRequestBody());
  }
}
