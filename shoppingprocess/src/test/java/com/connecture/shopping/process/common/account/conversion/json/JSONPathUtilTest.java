package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class JSONPathUtilTest
{
  @Test
  public void isValid()
  {
    Assert.assertTrue(JSONPathUtil.isValid("testObject_1.value_0"));

    // Bad Path
    Assert.assertFalse(JSONPathUtil.isValid(".testObject_1.value_0"));
  }

  @Test
  public void count() throws JSONException
  {
    String json = "{ testObject_1 : { value_0 : 0 }, testObject_2 : { values : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] } }";

    String testPath = "testObject_1.value_0";
    Assert.assertEquals(1, JSONPathUtil.count(new JSONObject(json), testPath));

    testPath = "testObject_1.value_0.name";
    Assert.assertEquals(0, JSONPathUtil.count(new JSONObject(json), testPath));

    testPath = "testObject_2.values.name";
    Assert.assertEquals(2, JSONPathUtil.count(new JSONObject(json), testPath));
  }

  @Test
  public void remove() throws JSONException
  {
    String json = "{ testObject_1 : { value_0 : 0 }, testObject_2 : { values : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] } }";

    // Remove value_0 from testObject_1
    String testPath = "testObject_1.value_0";
    JSONObject testObject = new JSONObject(json);
    Assert.assertEquals(1, JSONPathUtil.remove(testObject, testPath));
    Assert.assertEquals(0, JSONPathUtil.count(testObject, testPath));

    // Remove name from value_0 of testObject_1
    testPath = "testObject_1.value_0.name";
    testObject = new JSONObject(json);
    Assert.assertEquals(0, JSONPathUtil.remove(testObject, testPath));

    // Remove name from all testObject_2.values
    testPath = "testObject_2.values.name";
    testObject = new JSONObject(json);
    Assert.assertEquals(2, JSONPathUtil.remove(testObject, testPath));
    Assert.assertEquals(0, JSONPathUtil.count(testObject, testPath));
  }

  @Test
  public void add() throws JSONException
  {
    String json = "{ testObject_1 : { }, testObject_2 : { values : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] } }";

    // Add value_0 to testObject_1
    String testPath = "testObject_1.value_0";
    JSONObject testObject = new JSONObject(json);
    Assert.assertEquals(1, JSONPathUtil.add(testObject, testPath));
    Assert.assertEquals(1, JSONPathUtil.count(testObject, testPath));

    // Now, building on the previous test, add a name to value_0
    testPath = "testObject_1.value_0.name";
    Assert.assertEquals(1, JSONPathUtil.add(testObject, testPath, "tester"));
    Assert.assertEquals("tester", testObject.getJSONObject("testObject_1").getJSONObject("value_0")
      .getString("name"));

    // Now, add type to all values objects of testObject_2 with a default value
    // of 1
    testPath = "testObject_2.values.type";
    testObject = new JSONObject(json);
    Assert.assertEquals(2, JSONPathUtil.add(testObject, testPath, 1));
    Assert.assertEquals(2, JSONPathUtil.count(testObject, testPath));

    // Ensure the default values were actually stored
    boolean matching = true;
    JSONArray values = testObject.getJSONObject("testObject_2").getJSONArray("values");
    for (int v = 0; v < values.length(); v++)
    {
      matching &= values.getJSONObject(v).getInt("type") == 1;
    }
    Assert.assertTrue(matching);

    // Now, add a nested path value directly
    testPath = "testObject_1.value_0.name";
    testObject = new JSONObject(json);
    Assert.assertEquals(1, JSONPathUtil.add(testObject, testPath, "tester"));
    Assert.assertEquals("tester", testObject.getJSONObject("testObject_1").getJSONObject("value_0")
      .getString("name"));
  }

  @Test
  public void rename() throws JSONException
  {
    String json = "{ testObject_1 : { value_0 : { name : \"tester\" } }, testObject_2 : { values : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] } }";

    // Rename value_0 to value_1
    String oldPath = "testObject_1.value_0";
    String newPath = "testObject_1.value_1";
    JSONObject testObject = new JSONObject(json);
    // Check there were two changes
    Assert.assertEquals(2, JSONPathUtil.rename(testObject, oldPath, newPath));

    // Check the new path exists
    Assert.assertEquals(1, JSONPathUtil.count(testObject, newPath));

    // Ensure the value_0 value was retained
    Assert.assertEquals("tester", testObject.getJSONObject("testObject_1").getJSONObject("value_1")
      .getString("name"));

    // Now, building on the previous test, rename name to name.first
    oldPath = "testObject_1.value_1.name";
    newPath = "testObject_1.value_1.name.first";
    Assert.assertEquals(2, JSONPathUtil.rename(testObject, oldPath, newPath));
    Assert.assertEquals("tester", testObject.getJSONObject("testObject_1").getJSONObject("value_1")
      .getJSONObject("name").getString("first"));
    Assert.assertEquals(1, JSONPathUtil.count(testObject, newPath));

    // Now, rename all values.name objects of testObject_2 to values.name.first
    oldPath = "testObject_2.values.name";
    newPath = "testObject_2.values.name.first";
    testObject = new JSONObject(json);
    Assert.assertEquals(4, JSONPathUtil.rename(testObject, oldPath, newPath));
    Assert.assertEquals(2, JSONPathUtil.count(testObject, newPath));

    // Ensure the old values persisted
    boolean matching = true;
    JSONArray values = testObject.getJSONObject("testObject_2").getJSONArray("values");
    for (int v = 0; v < values.length(); v++)
    {
      matching &= ("tester_" + (v + 1)).equals(values.getJSONObject(v).getJSONObject("name")
        .getString("first"));
    }
    Assert.assertTrue(matching);
  }
}
