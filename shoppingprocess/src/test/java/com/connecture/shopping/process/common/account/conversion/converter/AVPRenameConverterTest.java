package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPRenameConverterTest
{
  private static Logger LOG = Logger.getLogger(AVPRenameConverterTest.class);

  @Test
  public void validateBadRenameConverter() throws JSONException
  {
    String json = "{ type : \"rename\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals("Errors expected", 3, errors.size());
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Expected errors detected:\n" + errors);
    }
  }

  @Test
  public void validateRenameConverter() throws JSONException
  {
    String json = "{ type : \"rename\", oldPath : \"questions.value.answer\", newPath : \"questions.value.answerValue\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }

  @Test
  public void createRenameConverter() throws JSONException
  {
    String json = "{ type : \"rename\", oldPath : \"questions.value.answer\", newPath : \"questions.value.answerValue\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    AVPRenameConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    Assert.assertEquals(AVPConverterType.RENAME, converter.getType());
    Assert.assertEquals("questions.value.answer", converter.getOldPath());
    Assert.assertEquals("questions.value.answerValue", converter.getNewPath());
    Assert.assertEquals("reason", converter.getReason());
  }

  @Test
  public void renameConversion() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPRenameConverterTest.class, "testRenameConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPRenameConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    jsonObject = new JSONObject(JSONFileUtils.readJSONData(AVPRenameConverterTest.class,
      "renameConversion.json"));
    converter.apply(jsonObject);

    // Verify the newPath
    Assert.assertEquals(2, JSONPathUtil.count(jsonObject, converter.getNewPath()));
    
    // Verify the values were retained
    Assert.assertEquals(1, jsonObject.getJSONArray("questions").getJSONObject(0).getJSONObject("value").getInt("answerValue"));
    Assert.assertEquals(2, jsonObject.getJSONArray("questions").getJSONObject(1).getJSONObject("value").getInt("answerValue"));
  }
}
