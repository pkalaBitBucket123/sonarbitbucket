package com.connecture.shopping.process.service.data.converter;

import org.junit.Assert;
import org.junit.Test;

public class FrequencyCodeTest
{

  @Test
  public void testFromString()
  {
    Assert.assertNull(FrequencyCode.fromString(null));
    Assert.assertNull(FrequencyCode.fromString("fake"));
    Assert.assertEquals(FrequencyCode.MONTHLY, FrequencyCode.fromString("monthly"));
    Assert.assertEquals(FrequencyCode.MONTHLY, FrequencyCode.fromString("moNTHlY"));
    Assert.assertEquals(FrequencyCode.PAYCHECK, FrequencyCode.fromString("payCheck"));
  }
}
