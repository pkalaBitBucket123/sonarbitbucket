package com.connecture.shopping.process.message;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.test.ValueHolder;

public class MessageProcessTest
{
  @Test
  public void testBuildEmailPlanContent() throws JSONException, IOException
  {
    MessageProcess process = new MessageProcess();
    try 
    {
      process.buildEmailPlanContent(null, null, null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }

    process.setEmailTemplateLocation("");
    File tempTemplateFile = new File("shoppingEmailTemplate.vt");
    FileWriter writer = new FileWriter(tempTemplateFile);
    writer.write(
      "#foreach( $plan in $planDataList)\n" +
    	"1\n" +
    	"#end");
    writer.close();
    tempTemplateFile.setReadable(true);
    tempTemplateFile.deleteOnExit();
    String path = new File(".").getAbsolutePath();
    path = path.substring(0, path.length() - 2);
    process.setFileStoreLocation(path);
    
    JSONArray planDataArray = new JSONArray();
    String result = process.buildEmailPlanContent(planDataArray, null, null);
    Assert.assertEquals("", result);
      
    JSONObject planJSON = new JSONObject();
    PlanEmailConverterUtilsTest.populatePlanJSONForConvertToPlanEmailBeanTest(planJSON);
    planDataArray.put(planJSON);
    
    result = process.buildEmailPlanContent(planDataArray, "01", "Test");
    Assert.assertEquals("1\n", result);
  }
  
  @Test
  public void testConvertData() throws JSONException
  {
    MessageProcess process = new MessageProcess();
    try 
    {
      process.convertData(null);
      Assert.fail("NullPointerException expected");
    }
    catch (NullPointerException e)
    {
      // This is expected
    }
    
    JSONArray planDataArray = new JSONArray();
    Assert.assertTrue(process.convertData(planDataArray).isEmpty());
    
    JSONObject planJSON = new JSONObject();
    PlanEmailConverterUtilsTest.populatePlanJSONForConvertToPlanEmailBeanTest(planJSON);
    planDataArray.put(planJSON);
    Assert.assertEquals(1, process.convertData(planDataArray).size());
  }
  
  @Test
  public void testSendEmail()
  {
    final ValueHolder<Integer> sentCount = new ValueHolder<Integer>(new Integer(0)); 
    MessageProcess process = new MessageProcess() {
      @Override
      protected void sendMessage(Message msg) throws MessagingException
      {
        sentCount.setValue(sentCount.getValue() + 1);
      }
    };
    
    process.sendEmail(null);
    
    MessageBean bean = new MessageBean();
    process.sendEmail(bean);
    
    process.setHostKey("mail.smtp.host");
    process.setSmtpHost("localhost");
    process.setFromAddress("junit@connecture.com");
    bean.setContent("Test");
    process.sendEmail(bean);
    
    List<MessageRecipient> recipients = new ArrayList<MessageRecipient>();
    bean.setRecipients(recipients);
    process.sendEmail(bean);

    MessageRecipient recipient = new MessageRecipient();
    recipient.setEmail(null);
    recipients.add(recipient);
    process.sendEmail(bean);
    
    recipient.setEmail("junit@connecture.com");
    process.sendEmail(bean);
    Assert.assertEquals(1, sentCount.getValue().intValue());
  }
}
