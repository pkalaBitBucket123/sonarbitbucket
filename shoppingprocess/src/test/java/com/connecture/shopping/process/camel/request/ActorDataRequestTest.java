package com.connecture.shopping.process.camel.request;

import org.junit.Assert;
import org.junit.Test;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class ActorDataRequestTest
{
  @Test
  public void testMisc()
  {
    ActorDataRequest request = new ActorDataRequest();
    Assert.assertNull(request.getProducer());
    request.setContext(ShoppingContext.ANONYMOUS);
    Assert.assertEquals(ShoppingContext.ANONYMOUS, request.getRequestHeaders().get(BaseCamelRequest.CONTEXT_KEY));
  }
}
