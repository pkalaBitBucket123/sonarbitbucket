package com.connecture.shopping.process.common.account.conversion.converter;

import org.junit.Assert;
import org.junit.Test;


public class AVPConverterTypeTest
{
  @Test
  public void findForName()
  {
    Assert.assertNull(AVPConverterType.findForName("bogus"));
    Assert.assertNotNull(AVPConverterType.findForName("delete"));
    Assert.assertEquals(AVPConverterType.DELETE, AVPConverterType.findForName("delete"));
  }
}
