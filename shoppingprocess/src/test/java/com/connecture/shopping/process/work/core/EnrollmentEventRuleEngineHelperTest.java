package com.connecture.shopping.process.work.core;

import junit.framework.Assert;

import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.service.data.EnrollmentEventApplication;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentData;
import com.connecture.shopping.process.work.core.EnrollmentEventRuleEngineHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class EnrollmentEventRuleEngineHelperTest
{
  @Test
  public void testGetDomainModel() throws Exception
  {
    EnrollmentEventRulesEngineBean hcrRulesEngineBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    Mockito.when(hcrRulesEngineBean.executeRule()).thenThrow(new Exception("Ka-boom!"));
    Assert.assertNull(EnrollmentEventRuleEngineHelper.getDomainModel(hcrRulesEngineBean));
    
    hcrRulesEngineBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    EnrollmentEventDomainModel model = new EnrollmentEventDomainModel();
    Mockito.when(hcrRulesEngineBean.executeRule()).thenReturn(model);
    Assert.assertNotNull(EnrollmentEventRuleEngineHelper.getDomainModel(hcrRulesEngineBean));
  }
  
  @Test
  public void testGetSpecialEnrollmentEvents() throws Exception
  {
    Assert.assertNotNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEvents(null));
    EnrollmentEventDomainModel model = new EnrollmentEventDomainModel();
    Assert.assertNotNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEvents(model));
    EnrollmentEventApplication application = model.getApplication();
    Assert.assertNotNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEvents(model));
    SpecialEnrollmentData specialEnrollment = new SpecialEnrollmentData();
    application.setSpecialEnrollment(specialEnrollment);
    Assert.assertNotNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEvents(model));
  }
  
  @Test
  public void testGetSpecialEnrollmentEffectiveDate() throws Exception
  {
    Assert.assertNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEffectiveDate(null));
    EnrollmentEventDomainModel model = new EnrollmentEventDomainModel();
    Assert.assertNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEffectiveDate(model));
    EnrollmentEventApplication application = model.getApplication();
    Assert.assertNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEffectiveDate(model));
    application.setEffectiveDate("TEST");
    Assert.assertNotNull(EnrollmentEventRuleEngineHelper.getSpecialEnrollmentEffectiveDate(model));
  }
}
