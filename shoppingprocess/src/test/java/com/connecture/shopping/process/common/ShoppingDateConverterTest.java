package com.connecture.shopping.process.common;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Assert;
import org.junit.Test;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class ShoppingDateConverterTest
{
  @Test
  public void testConvertToString()
  {
    ShoppingDateConverter converter = new ShoppingDateConverter();
    Calendar calendar = new GregorianCalendar(2013, 0, 1);
    Date date = calendar.getTime();
    String result = converter.convertToString(null, date);
    Assert.assertEquals(ShoppingDateUtils.dateToString(date), result);
    
    result = converter.convertToString(null, calendar);
    Assert.assertEquals(ShoppingDateUtils.dateToString(date), result);
  }
  
  @Test
  public void convertFromString()
  {
    ShoppingDateConverter converter = new ShoppingDateConverter();
    Calendar calendar = new GregorianCalendar(2013, 0, 1);
    Date date = calendar.getTime();
    
    try
    {
      converter.convertFromString(null, new String[] { "A" }, Date.class);
      Assert.fail("TypeConversionException expected");
    }
    catch (TypeConversionException e)
    {
      // Expected
    }
    
    try
    {
      converter.convertFromString(null, new String[] { "01/01/013" }, Date.class);
      Assert.fail("TypeConversionException expected");
    }
    catch (TypeConversionException e)
    {
      // Expected
    }
    
    try
    {
      converter.convertFromString(null, new String[] { "0101201" }, Date.class);
      Assert.fail("TypeConversionException expected");
    }
    catch (TypeConversionException e)
    {
      // Expected
    }
    
    try
    {
      converter.convertFromString(null, new String[] { "01/01//013" }, Date.class);
      Assert.fail("TypeConversionException expected");
    }
    catch (TypeConversionException e)
    {
      // Expected
    }
    
    Object result = converter.convertFromString(null, new String[] { null }, Date.class);
    Assert.assertNull(result);
    
    result = converter.convertFromString(null, new String[] { "01/01/2013" }, Date.class);
    Assert.assertEquals(ShoppingDateUtils.dateToString(date), ShoppingDateUtils.dateToString((Date)result));
    
    result = converter.convertFromString(null, new String[] { "01012013" }, Date.class);
    Assert.assertEquals(ShoppingDateUtils.dateToString(date), ShoppingDateUtils.dateToString((Date)result));
    
    result = converter.convertFromString(null, new String[] { "01/01/2013" }, Calendar.class);
    Assert.assertEquals(ShoppingDateUtils.dateToString(date), ShoppingDateUtils.dateToString(((Calendar)result).getTime()));
  }
}
