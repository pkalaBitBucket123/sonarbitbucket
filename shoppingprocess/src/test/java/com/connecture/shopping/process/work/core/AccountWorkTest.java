package com.connecture.shopping.process.work.core;

import java.lang.reflect.Field;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.work.WorkConstants;

public class AccountWorkTest
{
  @Test
  public void testProcessData() throws Exception
  {
    AccountWork work = new AccountWork();
    JSONObject results = work.processData();
    Assert.assertNull(results.names());

    JSONObject accountJSON = new JSONObject("{ value : 0 }");
    Field field = AccountWork.class.getDeclaredField("accountJSON");
    field.setAccessible(true);
    field.set(work, accountJSON);
    work.setMethod(WorkConstants.Method.UPDATE);
    results = work.processData();
    Assert.assertNull(results.names());

    work.setMethod(WorkConstants.Method.GET);
    results = work.processData();
    Assert.assertEquals(1, results.names().length());
    Assert.assertTrue(results.has("account"));
  }

  @Test
  public void testGet() throws Exception
  {
    Account account = Mockito.mock(Account.class);
    String transactionId = "TEST";
    JSONObject value = new JSONObject("{ value : 0 }");
    Mockito.when(account.getOrCreateAccount(Mockito.eq(transactionId))).thenReturn(value);

    AccountWork work = new AccountWork();
    work.setAccount(account);
    work.setTransactionId(transactionId);
    
    work.get();
    
    Mockito.verify(account).getOrCreateAccount(Mockito.eq(transactionId));

    JSONObject accountJSON = work.accountJSON;
    Assert.assertNotNull(accountJSON);
    Assert.assertTrue(accountJSON.has("value"));
  }

  @Test
  public void testUpdate() throws Exception
  {
    Account account = Mockito.mock(Account.class);
    AccountWork work = new AccountWork();
    work.setAccount(account);
    String transactionId = "TEST";
    work.setTransactionId(transactionId);
    JSONObject value = new JSONObject("{ value : 0 }");
    work.setValue(value);
    work.update();

    Mockito.verify(account).getOrCreateAccount(Mockito.eq(transactionId));
    Mockito.verify(account).updateAccountInfo(Mockito.eq(transactionId), Mockito.eq(value));
  }

  @Test
  public void testForCompleteness() throws Exception
  {
    new AccountWork().create();
    new AccountWork().delete();
  }
}
