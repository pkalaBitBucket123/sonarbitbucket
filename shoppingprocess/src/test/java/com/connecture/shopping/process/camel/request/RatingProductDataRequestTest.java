package com.connecture.shopping.process.camel.request;

import static com.connecture.shopping.process.camel.request.RatingProductDataRequest.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class RatingProductDataRequestTest
{
  @Test
  public void testGetRequestHeaders() throws ParseException
  {
    RatingProductDataRequest request = new RatingProductDataRequest();
    Map<String, Object> result = request.getRequestHeaders();
    
    Assert.assertEquals(CHILD_ONLY_NO, String.valueOf(result.get(CHILD_ONLY)));
    
    request.setPrimary("0", "05/05/2013", "M", "Yes");
    result = request.getRequestHeaders();
    Assert.assertEquals(CHILD_ONLY_NO, String.valueOf(result.get(CHILD_ONLY)));
    Assert.assertEquals("0", String.valueOf(result.get(PRIMARY_PREFIX + "." + ID)));
    Assert.assertEquals("05/05/2013", String.valueOf(result.get(PRIMARY_PREFIX + "." + AGE)));
    Assert.assertEquals("M", String.valueOf(result.get(PRIMARY_PREFIX + "." + GENDER)));
    Assert.assertEquals("Yes", String.valueOf(result.get(PRIMARY_PREFIX + "." + TOBACCO)));
    
    request = new RatingProductDataRequest();
    request.setSpouse("0", "05/05/2013", "M", "Yes");
    result = request.getRequestHeaders();
    Assert.assertEquals(CHILD_ONLY_NO, String.valueOf(result.get(CHILD_ONLY)));
    Assert.assertEquals("0", String.valueOf(result.get(SPOUSE_PREFIX + "." + ID)));
    Assert.assertEquals("05/05/2013", String.valueOf(result.get(SPOUSE_PREFIX + "." + AGE)));
    Assert.assertEquals("M", String.valueOf(result.get(SPOUSE_PREFIX + "." + GENDER)));
    Assert.assertEquals("Yes", String.valueOf(result.get(SPOUSE_PREFIX + "." + TOBACCO)));
    
    request = new RatingProductDataRequest();
    request.addChild("0", "01/01/2003", "M", "No");
    result = request.getRequestHeaders();
    Assert.assertEquals(CHILD_ONLY_YES, String.valueOf(result.get(CHILD_ONLY)));

    request = new RatingProductDataRequest();
    request.setEffectiveDate(new SimpleDateFormat(DATE_PATTERN).parse("05/05/2003"));
    request.setCounty("1010");
    request.setCsrCode("01");
    request.setISNewBusiness(Boolean.FALSE);
    request.setIsOnExchangePlans(Boolean.TRUE);
    request.setZipCode("90210");
    result = request.getRequestHeaders();
    Assert.assertEquals("05/05/2003", result.get(EFF_DATE_KEY));
    Assert.assertEquals("1010", result.get(COUNTY));
    Assert.assertEquals("01", result.get(CSR_CODE));
    Assert.assertFalse(Boolean.valueOf(String.valueOf(result.get(IS_NEW_BUSINESS))));
    Assert.assertTrue(Boolean.valueOf(String.valueOf(result.get(IS_ON_EXCHANGE_PLANS))));
    Assert.assertEquals("90210", result.get(ZIP_CODE));
  }
  
  @Test
  public void testGetRequestBody()
  {
    RatingProductDataRequest request = new RatingProductDataRequest();
    Assert.assertNull(request.getRequestBody());
  }
  
  @Test
  public void testGetProducterTemplate()
  {
    RatingProductDataRequest request = new RatingProductDataRequest();
    Assert.assertNull(request.getProducer());
  }
}
