package com.connecture.shopping.process.service.data.converter;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductPlanData;

public class RatingPlanDataConverterTest
{

  private String ratedPlanJSONStr;

  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Before
  public void setUp() throws Exception
  {
    ratedPlanJSONStr = FileUtils.readFileToString(FileUtils.toFile(this.getClass().getResource(
      "mlrPlanTestData.json")));
  }

  @Test
  public void testConvertData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    converter.convertData(ratedPlanJSONStr);
  }
  
  @Test
  public void testMonthlyRateData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    ProductPlanData productPlanData = converter.convertData(ratedPlanJSONStr);
    PlanData planData = productPlanData.getPlans().get(0);
//    JSONArray monthlyRatesJSON = 
      RatingPlanDataConverter.buildMemberLevelRateJSON(planData, FrequencyCode.MONTHLY, null);
    // TODO | assert
  }
  
  @Test
  public void testPaycheckRateData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    ProductPlanData productPlanData = converter.convertData(ratedPlanJSONStr);
    PlanData planData = productPlanData.getPlans().get(0);
//    JSONArray paycheckRatesJSON = 
      RatingPlanDataConverter.buildMemberLevelRateJSON(planData, FrequencyCode.PAYCHECK, 12);
    // TODO | assert
  }
  
  @Test
  public void testMonthlyContributionData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    ProductPlanData productPlanData = converter.convertData(ratedPlanJSONStr);
    PlanData planData = productPlanData.getPlans().get(0);
//    JSONObject monthlyContributionsJSON = 
      RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeContributions(), FrequencyCode.MONTHLY, null);
    // TODO | assert
  }
  
  @Test
  public void testPaycheckContributionData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    ProductPlanData productPlanData = converter.convertData(ratedPlanJSONStr);
    PlanData planData = productPlanData.getPlans().get(0);
//    JSONObject paycheckContributionsJSON = 
      RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeContributions(), FrequencyCode.PAYCHECK, 12);
    // TODO | assert
  }
  
  @Test
  public void testMonthlyCostData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    ProductPlanData productPlanData = converter.convertData(ratedPlanJSONStr);
    PlanData planData = productPlanData.getPlans().get(0);
//    JSONObject monthlyCostJSON = 
      RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeCosts(), FrequencyCode.MONTHLY, null);
    // TODO | assert
  }
  
  @Test
  public void testPaycheckCostData() throws Exception
  {
    RatingPlanDataConverter converter = new RatingPlanDataConverter();
    ProductPlanData productPlanData = converter.convertData(ratedPlanJSONStr);
    PlanData planData = productPlanData.getPlans().get(0);
//    JSONObject paycheckCostJSON = 
      RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeCosts(), FrequencyCode.PAYCHECK, 12);
    // TODO | assert
  }
}
