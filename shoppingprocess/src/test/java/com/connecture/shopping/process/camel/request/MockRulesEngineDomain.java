package com.connecture.shopping.process.camel.request;

public class MockRulesEngineDomain
{
  private String name;

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }
}
