package com.connecture.shopping.process.work.core;

import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;

public class EffectiveDateWorkHelperTest
{
  @Test
  public void testGetEffectiveDate() throws ParseException
  {
    Date date = EffectiveDateWorkHelper.getEffectiveDate("1/1/1970");
    // Doesn't blow up and we get a date
    Assert.assertNotNull(date);
    
    // Shouldn't matter where we run it, it'll be at most 23 HOURS back.
    Assert.assertEquals(new GregorianCalendar(1970, 0, 1).getTime(), date);
    
    try
    {
      EffectiveDateWorkHelper.getEffectiveDate("A");
      Assert.fail("Excepted ParseException");
    }
    catch (ParseException e)
    {      
      // Should totally explode
    }
  }
  
  @Test
  public void testGetEffectiveDateMs() throws ParseException
  {
    Long value = EffectiveDateWorkHelper.getEffectiveDateMs("1/1/1970");
    // Doesn't blow up and we get a value
    Assert.assertNotNull(value);
    
    // Shouldn't matter where we run it, it'll be at most 23 HOURS back.
    Long ONE_DAY = 24 * 60 * 60 * 1000l;
    Assert.assertTrue(value >= -ONE_DAY);
    
    try
    {
      EffectiveDateWorkHelper.getEffectiveDateMs("A");
      Assert.fail("Excepted ParseException");
    }
    catch (ParseException e)
    {      
      // Should totally explode
    }
  }
}
