package com.connecture.shopping.process.common.account.conversion.json;

import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathAction;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathBulkAddAction;
import com.connecture.shopping.process.common.account.conversion.json.JSONPathAction.PathState;

public class JSONPathBulkAddActionTest
{
  @Test
  public void isValidFor()
  {
    Assert.assertTrue(new JSONPathBulkAddAction().isValidFor(JSONPathAction.PathState.MISSING));
    Assert.assertTrue(new JSONPathBulkAddAction()
      .isValidFor(JSONPathAction.PathState.MISSING_SUBPATH));
    Assert.assertFalse(new JSONPathBulkAddAction().isValidFor(JSONPathAction.PathState.FOUND));
  }

  @Test
  public void perform() throws JSONException
  {
    JSONObject jsonObject = new JSONObject("{ testObjects : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] }");
    JSONPathBulkAddAction action = new JSONPathBulkAddAction();
    
    Assert.assertFalse(action.perform("type", jsonObject.getJSONArray("testObjects").getJSONObject(0), PathState.FOUND));
    
    Assert.assertTrue(action.perform("type", jsonObject.getJSONArray("testObjects").getJSONObject(0), PathState.MISSING));
    jsonObject.getJSONArray("testObjects").getJSONObject(0).getJSONObject("type");
    
    List<Object> types = Arrays.asList(new Object[] { "type_1", "type_2" });
    action = new JSONPathBulkAddAction(types);
    jsonObject = new JSONObject("{ testObjects : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] }");
    Assert.assertTrue(action.perform("type", jsonObject.getJSONArray("testObjects").getJSONObject(0), PathState.MISSING));
    Assert.assertTrue(action.perform("type", jsonObject.getJSONArray("testObjects").getJSONObject(1), PathState.MISSING));
    
    Assert.assertEquals("type_1", jsonObject.getJSONArray("testObjects").getJSONObject(0).getString("type"));
    Assert.assertEquals("type_2", jsonObject.getJSONArray("testObjects").getJSONObject(1).getString("type"));
    
    jsonObject = new JSONObject("{ testObjects : [ { name : \"tester_1\" }, { name : \"tester_2\" } ] }");
    action = new JSONPathBulkAddAction();
    Assert.assertTrue(action.perform("type", jsonObject.getJSONArray("testObjects").getJSONObject(0), PathState.MISSING_SUBPATH));
    jsonObject.getJSONArray("testObjects").getJSONObject(0).getJSONObject("type");
    
    types = Arrays.asList(new Object[] { "type_2" });
    action = new JSONPathBulkAddAction(types);
    Assert.assertTrue(action.perform("type", jsonObject.getJSONArray("testObjects").getJSONObject(1), PathState.MISSING_SUBPATH));
    jsonObject.getJSONArray("testObjects").getJSONObject(1).getJSONObject("type");
  }
}
