package com.connecture.shopping.process.work.core;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.data.ProductLineData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.work.core.ProductLinesWork;
import com.connecture.shopping.process.work.employee.ProductPlanDataWork;

public class ProductLinesWorkTest
{

  @Test
  public void testGetProductLineData()
  {
    Assert.assertNull(ProductLinesWork.getProductLineData(null));

    ProductPlanData productPlanData = createTestProductPlanData();

    ProductLineData productLineData = ProductLinesWork.getProductLineData(productPlanData);

    Assert.assertEquals(2, productLineData.getProducts().size());

    Assert.assertEquals(2, productLineData.getProducts().get(0).getPlanCount().intValue());
  }

  private ProductPlanData createTestProductPlanData()
  {
    ProductPlanData productPlanData = new ProductPlanData();

    List<PlanData> plans = new ArrayList<PlanData>();

    PlanData plan = new PlanData();
    ProductData productData = new ProductData();
    productData.setSortOrder(0);
    productData.setProductLineName("Medical");
    productData.setProductLineCode("PROD_MED");
    plan.setProductData(productData);
    plans.add(plan);

    plan = new PlanData();
    plan.setProductData(productData);
    plans.add(plan);

    plan = new PlanData();
    productData = new ProductData();
    productData.setSortOrder(1);
    productData.setProductLineName("Dental");
    productData.setProductLineCode("PROD_DENT");
    plan.setProductData(productData);
    plans.add(plan);

    productPlanData.setPlans(plans);
    return productPlanData;
  }

  @Test
  public void testProcessData() throws Exception
  {
    ProductLinesWork work = new ProductLinesWork();

    JSONObject results = work.processData();
    Assert.assertTrue(results.has("productLines"));
    Assert.assertEquals(0, results.getJSONArray("productLines").length());

    ProductPlanData productPlanData = createTestProductPlanData();

    ProductLineData productLineData = ProductLinesWork.getProductLineData(productPlanData);

    Field field = ProductLinesWork.class.getDeclaredField("productLineData");
    field.setAccessible(true);
    field.set(work, productLineData);

    results = work.processData();
    Assert.assertTrue(results.has("productLines"));
    Assert.assertEquals(2, results.getJSONArray("productLines").length());
    Assert.assertEquals("PROD_MED", results.getJSONArray("productLines").getJSONObject(0)
      .getString("productLineCode"));
  }

  @Test
  public void testGet() throws Exception
  {
    ProductPlanDataWork productPlanDataWork = Mockito.mock(ProductPlanDataWork.class);
    
    ProductPlanData testProductPlanData = createTestProductPlanData();
    Mockito.when(productPlanDataWork.getProductPlanData()).thenReturn(testProductPlanData);
    
    ProductLinesWork work = new ProductLinesWork();
    work.setProductPlanDataWork(productPlanDataWork);
    
    work.get();
    
    JSONObject results = work.processData();

    Assert.assertTrue(results.has("productLines"));
    Assert.assertEquals(2, results.getJSONArray("productLines").length());
    Assert.assertEquals("PROD_MED", results.getJSONArray("productLines").getJSONObject(0)
      .getString("productLineCode"));
  }
  
  @Test
  public void testTheRest() throws Exception {
    ProductLinesWork work = new ProductLinesWork();
    
    work.create();
    work.delete();
    work.update();
    work.setTransactionId("Test");
  }
}
