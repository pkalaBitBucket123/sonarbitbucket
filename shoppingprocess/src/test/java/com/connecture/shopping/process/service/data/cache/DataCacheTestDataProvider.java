package com.connecture.shopping.process.service.data.cache;

public class DataCacheTestDataProvider implements DataProvider<DataCacheTestData>
{
  @Override
  public DataCacheTestData fetchData()
  {
    DataCacheTestData data = new DataCacheTestData();
    data.setValue("TEST");
    return data;
  }
}
