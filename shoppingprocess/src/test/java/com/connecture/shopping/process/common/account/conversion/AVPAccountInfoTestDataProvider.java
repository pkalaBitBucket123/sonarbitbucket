package com.connecture.shopping.process.common.account.conversion;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.domain.AccountInfo;

public class AVPAccountInfoTestDataProvider extends AccountInfoDataProvider
{
  private String dataFile;
  
  @Override
  public List<AccountInfo> fetchData() throws Exception
  {
    List<AccountInfo> infoList = new ArrayList<AccountInfo>();
    
    JSONArray accountsObject = new JSONArray(JSONFileUtils.readJSONData(getClass(), dataFile));
    
    for (int a = 0; a < accountsObject.length(); a++)
    {
      JSONObject accountObject = accountsObject.getJSONObject(a);
      String accountVersion = "1.0";
      if (accountObject.has("version"))
      {
        accountVersion = accountObject.getString("version");
      }
      if (!getVersion().equals(accountVersion))
      {
        AccountInfo info = new AccountInfo();
        info.setVersion(accountVersion);
        info.setValue(accountsObject.getJSONObject(a));
        infoList.add(info);
      }
    }
    
    return infoList;
  }

  public void setDataFile(String dataFile)
  {
    this.dataFile = dataFile;
  }
}
