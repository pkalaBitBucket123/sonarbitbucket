package com.connecture.shopping.process.camel.request;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.service.data.EnrollmentEventApplication;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentData;

public class EnrollmentEventRulesEngineDataRequestTest
{
  @Test
  public void testGetRequestBody() throws JSONException
  {
    EnrollmentEventRulesEngineDataRequest request = new EnrollmentEventRulesEngineDataRequest();
    String results = (String)request.getRequestBody();
    Assert.assertNotNull(request);
    
    EnrollmentEventDomainModel domain = new EnrollmentEventDomainModel();
    request.setDomain(domain);

    results = (String)request.getRequestBody();
    Assert.assertNotNull(request);
    JSONObject resultJSON = new JSONObject(results);
    JSONObject applicationJSON = resultJSON.getJSONObject("Application");
    Assert.assertTrue(applicationJSON.has("Special_Enrollment"));
    
    EnrollmentEventApplication application = domain.getApplication();
    String effectiveDate = "12/12/2012";
    application.setEffectiveDate(effectiveDate);
    results = (String)request.getRequestBody();
    resultJSON = new JSONObject(results);
    applicationJSON = resultJSON.getJSONObject("Application");
    Assert.assertEquals(effectiveDate, applicationJSON.getString("Effective_Date"));
    
    SpecialEnrollmentData specialEnrollment = application.getSpecialEnrollment();
    String eventDate = "1/1/2014";
    specialEnrollment.setEventDate(eventDate);
    results = (String)request.getRequestBody();
    resultJSON = new JSONObject(results);
    applicationJSON = resultJSON.getJSONObject("Application");
    JSONObject specialEnrollmentJSON = applicationJSON.getJSONObject("Special_Enrollment");
    Assert.assertEquals(eventDate, specialEnrollmentJSON.getString("Event_Date"));
    
    String eventReason = "Birth";
    specialEnrollment.setSelectedSpecialEnrollmentReason(eventReason);
    results = (String)request.getRequestBody();
    resultJSON = new JSONObject(results);
    applicationJSON = resultJSON.getJSONObject("Application");
    specialEnrollmentJSON = applicationJSON.getJSONObject("Special_Enrollment");
    Assert.assertEquals(eventReason, specialEnrollmentJSON.getString("Selected_Special_Enrollment_Reason"));    
  }
  
  @Test
  public void testConvertData() throws Exception
  {
    EnrollmentEventRulesEngineDataRequest request = new EnrollmentEventRulesEngineDataRequest();
    EnrollmentEventDomainModel result = request.convertData("{ Application : { } }");
    Assert.assertNotNull(result);
  }
}
