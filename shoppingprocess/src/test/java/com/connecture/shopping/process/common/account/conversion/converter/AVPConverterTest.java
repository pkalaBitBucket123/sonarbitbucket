package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;


public class AVPConverterTest
{
  private static Logger LOG = Logger.getLogger(AVPConverterTest.class);

  @Test
  public void validateJSONEmptyObject() throws JSONException
  {
    JSONObject jsonObject = new JSONObject("{}");

    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);

    // "type" is required
    Assert.assertEquals("Errors expected", 1, errors.size());
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Expected errors detected:\n" + errors);
    }
  }
}
