package com.connecture.shopping.process.work.core;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.work.core.EnrollmentEventDaysLeftToEnrollWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class EnrollmentEventDaysLeftToEnrollWorkHelperTestTest
{
  @Test
  public void testGetDaysLeftToEnroll() throws Exception
  {
    JSONObject accountObject = new JSONObject();
    EnrollmentEventRulesEngineBean mockBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    EnrollmentEventDomainModel model = new EnrollmentEventDomainModel();
    Mockito.when(mockBean.executeRule()).thenReturn(model);

    // No HCR days left to enroll
    Integer result = EnrollmentEventDaysLeftToEnrollWorkHelper.getDaysLeftToEnroll(accountObject, mockBean);
    Assert.assertNull(result);

    final Integer HCR_DAYS_LEFT_TO_ENROLL = 42;
    model.getApplication().getSpecialEnrollment().setDaysLeftToEnroll(HCR_DAYS_LEFT_TO_ENROLL);
    
    // We expect to get the HCR days left to enroll
    result = EnrollmentEventDaysLeftToEnrollWorkHelper.getDaysLeftToEnroll(accountObject, mockBean);
    Assert.assertNotNull(result);
    Assert.assertEquals(HCR_DAYS_LEFT_TO_ENROLL, result);
  }
}
