package com.connecture.shopping.process;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.domain.AccountInfo;

public class ShoppingIntegrationProcessTest
{
  @Test
  public void testGetAccountIdSuccess()
  {
    String testTransactionid = "transaction1234";
    Long testAccountId = 12345L;

    AccountInfo accountInfo = Mockito.mock(AccountInfo.class);
    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);

    Mockito.when(criteria.uniqueResult()).thenReturn(accountInfo);
    Mockito.when(accountInfo.getAccountId()).thenReturn(testAccountId);
    ShoppingIntegrationProcess shoppingIntegrationProcess = new ShoppingIntegrationProcess();
    shoppingIntegrationProcess.setSessionFactory(sessionFactory);
    Long result = shoppingIntegrationProcess.getAccountId(testTransactionid);
    Assert.assertEquals(testAccountId, result);
  }

  @Test
  public void testGetAccountIdFailure()
  {
    String testTransactionid = "transaction1234";

    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);

    Mockito.when(criteria.uniqueResult()).thenReturn(null);

    ShoppingIntegrationProcess shoppingIntegrationProcess = new ShoppingIntegrationProcess();
    shoppingIntegrationProcess.setSessionFactory(sessionFactory);
    Long result = shoppingIntegrationProcess.getAccountId(testTransactionid);
    Assert.assertEquals(null, result);
  }

  @Test
  public void testGetEnrollmentDataMLR() throws Exception
  {
    String accountDataJSON = FileUtils.readFileToString(FileUtils.toFile(this.getClass()
      .getResource("eeEnrollmentTestDataMLR.json")));

    testGetEnrollmentData(accountDataJSON);
  }

  @Test
  public void testGetEnrollmentDataNonMLR() throws Exception
  {
    String accountDataJSON = FileUtils.readFileToString(FileUtils.toFile(this.getClass()
      .getResource("eeEnrollmentTestDataNonMLR.json")));

    testGetEnrollmentData(accountDataJSON);
  }

  private void testGetEnrollmentData(String accountDataJSON) throws Exception
  {
    Long testAccountId = 1234L;

    AccountInfo accountInfo = Mockito.mock(AccountInfo.class);
    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
    Mockito.when(criteria.uniqueResult()).thenReturn(accountInfo);
    Mockito.when(accountInfo.getValue()).thenReturn(new JSONObject(accountDataJSON));

    ShoppingIntegrationProcess shoppingIntegrationProcess = new ShoppingIntegrationProcess();
    shoppingIntegrationProcess.setSessionFactory(sessionFactory);
    shoppingIntegrationProcess.getEnrollmentData(testAccountId);
  }
}
