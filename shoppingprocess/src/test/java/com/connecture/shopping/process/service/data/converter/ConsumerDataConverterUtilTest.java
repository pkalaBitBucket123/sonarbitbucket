package com.connecture.shopping.process.service.data.converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.model.data.ShoppingMember;
import com.connecture.shopping.process.service.plan.data.PlanContributionData;

public class ConsumerDataConverterUtilTest
{

  @Test
  public void testBuildShoppingMembers() throws Exception
  {
    JSONObject membersObject = new JSONObject(memberJSON);
    JSONArray memberArray = membersObject.getJSONArray("members");
    List<ShoppingMember> members = ConsumerDataConverterUtil.buildShoppingMembers(memberArray);

    Assert.assertEquals(3, members.size());

    DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    Assert.assertEquals(formatter.parse("Nov 5, 1965 00:00:00 AM"), members.get(0).getDateOfBirth());

    Assert.assertEquals("Frank", members.get(2).getFirstName());

    Assert.assertEquals(3, members.get(0).getSelectedProductLineKeys().size());
    Assert.assertEquals(3, members.get(1).getSelectedProductLineKeys().size());
    Assert.assertEquals(3, members.get(2).getSelectedProductLineKeys().size());
  }

  @Test
  public void testDetermineSelectedProductLines() throws Exception
  {
    JSONObject membersObject = new JSONObject(memberJSON);
    JSONArray memberArray = membersObject.getJSONArray("members");
    Set<String> productLines = ConsumerDataConverterUtil.determineSelectedProductLines(memberArray);

    Assert.assertEquals(3, productLines.size());
  }

  @Test
  public void testCalculateAgeAsOfDate() throws Exception
  {
    Date birthDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/06/1973");
    Date asOfDate = new SimpleDateFormat("mm/dd/yyyy").parse("03/15/2013");

    int age = ConsumerDataConverterUtil.calculateAgeAsOfDate(birthDate, asOfDate);
    Assert.assertEquals(40, age);

    asOfDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/05/2013");

    age = ConsumerDataConverterUtil.calculateAgeAsOfDate(birthDate, asOfDate);
    Assert.assertEquals(39, age);

    asOfDate = new SimpleDateFormat("mm/dd/yyyy").parse("01/06/2013");

    age = ConsumerDataConverterUtil.calculateAgeAsOfDate(birthDate, asOfDate);
    Assert.assertEquals(40, age);
  }

  @Test
  public void testGetPolicyEffectiveTime() throws JSONException, Exception
  {
    ConsumerDataConverterUtil.getPolicyEffectiveTime(new JSONObject(
      "{ event : { Coverage_Effective_Date : \"05/05/2013\" } }"));
  }

  @Test
  public void testGetPlanContributionData() throws JSONException
  {
    PlanContributionData data = ConsumerDataConverterUtil.getPlanContributionData(new JSONObject(
      "{ Employer_Contribution : 100 }"));

    Assert.assertEquals(100d, data.getAmount(), 0.01d);
  }

  @Test
  public void testGetCostData() throws JSONException, Exception
  {
    try
    {
      ConsumerDataConverterUtil.getCostData(new JSONObject("{}"));
      Assert.fail("expected excetion");
    }
    catch (Exception e)
    {
      // This is expected
    }

    Double value = ConsumerDataConverterUtil.getCostData(new JSONObject("{ EE_Cost : 100 }"));
    Assert.assertEquals(100d, value, 0.01d);
  }

  private static final String memberJSON = "{ "
    + "\"members\":["
    + "{\"birthDate\":\"11/05/1965\",\"isNew\":false,\"coverageSelected\":true,\"dateOfBirth\":\"-131133600000\",\"gender\":\"Male\",\"isPrimary\":true,\"isSmoker\":\"No\",\"memberAge\":null,\"memberExtRefId\":\"EMP_EXT_1\",\"memberRefName\":\"0\",\"memberRelationship\":\"Primary\",\"name\":{\"first\":\"Bruce\",\"last\":null,\"middle\":null,\"prefix\":null},\"nameData\":{\"first\":\"Bruce\",\"last\":null,\"middle\":null,\"prefix\":null},\"new\":false,\"products\":[{\"productLineCode\":\"PROD_MED\"},{\"productLineCode\":\"PROD_DENTAL\"},{\"productLineCode\":\"PROD_VISION\"}],\"workEmail\":\"ckroll@connecture.com\",\"zipCode\":\"53186\"},"
    + "{\"birthDate\":\"10/04/1970\",\"isNew\":true,\"coverageSelected\":true,\"dateOfBirth\":\"23864400000\",\"gender\":\"Female\",\"isPrimary\":false,\"isSmoker\":\"No\",\"memberAge\":null,\"memberExtRefId\":\"DEP_EXT_1\",\"memberRefName\":\"1\",\"memberRelationship\":\"Spouse (opposite sex)\",\"name\":{\"first\":\"Mary\",\"last\":null,\"middle\":null,\"prefix\":null},\"nameData\":{\"first\":\"Mary\",\"last\":null,\"middle\":null,\"prefix\":null},\"new\":false,\"products\":[{\"productLineCode\":\"PROD_MED\"},{\"productLineCode\":\"PROD_DENTAL\"},{\"productLineCode\":\"PROD_VISION\"}],\"workEmail\":null,\"zipCode\":\"53186\"},"
    + "{\"birthDate\":\"04/15/2007\",\"isNew\":true,\"coverageSelected\":true,\"dateOfBirth\":\"1176613200000\",\"gender\":\"Male\",\"isPrimary\":false,\"isSmoker\":\"No\",\"memberAge\":null,\"memberExtRefId\":\"DEP_EXT_2\",\"memberRefName\":\"2\",\"memberRelationship\":\"Child - dependent on subscriber for support and is legal guardian\",\"name\":{\"first\":\"Frank\",\"last\":null,\"middle\":null,\"prefix\":null},\"nameData\":{\"first\":\"Frank\",\"last\":null,\"middle\":null,\"prefix\":null},\"new\":false,\"products\":[{\"productLineCode\":\"PROD_MED\"},{\"productLineCode\":\"PROD_DENTAL\"},{\"productLineCode\":\"PROD_VISION\"}],\"workEmail\":null,\"zipCode\":\"53186\"}"
    + "] " + "}";
}
