package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.common.JSONFileUtils;

public class AVPDeleteConverterTest
{
  private static Logger LOG = Logger.getLogger(AVPDeleteConverterTest.class);

  @Test
  public void validateBadDeleteConverter() throws JSONException
  {
    String json = "{ type : \"delete\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals("Errors expected", 2, errors.size());
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Expected errors detected:\n" + errors);
    }
  }

  @Test
  public void validateDeleteConverter() throws JSONException
  {
    String json = "{ type : \"delete\", path : \"testObject.value_1\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    List<String> errors = AVPConverterUtils.validateJSON(jsonObject);
    Assert.assertEquals(0, errors.size());
  }

  @Test
  public void createDeleteConverter() throws JSONException
  {
    String json = "{ type : \"delete\", path : \"testObject.value_1\", reason : \"reason\" }";

    JSONObject jsonObject = new JSONObject(json);
    AVPDeleteConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    Assert.assertEquals(AVPConverterType.DELETE, converter.getType());
    Assert.assertEquals("testObject.value_1", converter.getPath());
    Assert.assertEquals("reason", converter.getReason());
  }

  @Test
  public void deleteConversion() throws JSONException
  {
    String json = JSONFileUtils.readJSONData(AVPDeleteConverterTest.class, "testDeleteConverter.json");

    JSONObject jsonObject = new JSONObject(json);
    AVPDeleteConverter converter = AVPConverterUtils.createAVPConverter(jsonObject);

    jsonObject = new JSONObject(JSONFileUtils.readJSONData(AVPDeleteConverterTest.class,
      "deleteConversion.json"));
    converter.apply(jsonObject);

    Assert.assertFalse(jsonObject.has(converter.getPath()));
  }

}
