package com.connecture.shopping.process.work.anonymous;

import org.json.JSONObject;
import org.junit.Test;

import com.connecture.shopping.process.work.anonymous.AnonymousApplicationConfigWork;
import com.connecture.shopping.process.work.core.ApplicationConfigWorkTest;

public class AnonymousApplicationConfigWorkTest
{
  @Test
  public void testProcessData() throws Exception
  {
    AnonymousApplicationConfigWork work = new AnonymousApplicationConfigWork();
    JSONObject auxShoppingConfigJSON = new JSONObject();
    work.setAuxShoppingConfig(auxShoppingConfigJSON.toString());
    ApplicationConfigWorkTest.assertProcessDataForJSONException(work, "JSONObject[\"workflows\"] not found.");
  }
}
