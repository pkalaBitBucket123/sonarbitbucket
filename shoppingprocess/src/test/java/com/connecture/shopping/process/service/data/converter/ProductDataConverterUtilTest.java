package com.connecture.shopping.process.service.data.converter;

import java.util.List;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueData;

public class ProductDataConverterUtilTest
{
  @Test
  public void testGetBenefitValues() throws JSONException
  {
    JSONArray benefitJSONArray = new JSONArray();
    
    List<PlanBenefitValueData> result = ProductDataConverterUtil.getBenefitValues(benefitJSONArray);
    Assert.assertEquals(0, result.size());
    
    JSONObject benefitObject = new JSONObject("{ \"code\" : \"type\", \"value\" : \"A1\", \"description\" : \"Test\"}");
    benefitJSONArray.put(benefitObject);
    result = ProductDataConverterUtil.getBenefitValues(benefitJSONArray);
    Assert.assertEquals(1, result.size());
    Assert.assertEquals("type", result.get(0).getBenefitValueTypeCode());
    Assert.assertEquals("A1", result.get(0).getDisplayName());
    Assert.assertEquals("Test", result.get(0).getDescription());
    Assert.assertNull(result.get(0).getNumericValue());
    
    benefitObject.put("numericValue", 1d);
    result = ProductDataConverterUtil.getBenefitValues(benefitJSONArray);
    Assert.assertEquals(1d, result.get(0).getNumericValue().doubleValue());
  }
  
  @Test
  public void testPopulateBenefitCategoriesFromShoppingPlanJSON() throws JSONException
  {
    PlanData planData = new PlanData();
    JSONObject planJSON = new JSONObject();
    ProductDataConverterUtil.populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(0, planData.getBenefitCategories().size());
    
    JSONArray benefitCategoryArray = new JSONArray();
    planJSON.put("benefitCategories", benefitCategoryArray);
    ProductDataConverterUtil.populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(0, planData.getBenefitCategories().size());
    
    JSONObject benefitCategoryObject = new JSONObject("{ \"categoryName\" : \"NAME\", \"sortOrder\" : 0 }");
    benefitCategoryArray.put(benefitCategoryObject);
    ProductDataConverterUtil.populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(1, planData.getBenefitCategories().size());
    PlanBenefitCategoryData categoryData = planData.getBenefitCategories().get(0);
    Assert.assertEquals(0, categoryData.getBenefits().size());
    
    JSONArray benefitArray = new JSONArray();
    benefitCategoryObject.put("benefits", benefitArray);
    
    ProductDataConverterUtil.populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);
    categoryData = planData.getBenefitCategories().get(0);
    Assert.assertEquals(0, categoryData.getBenefits().size());

    JSONObject benefitObject = new JSONObject("{ \"key\" : \"A\", \"displayName\" : \"A\", \"sortOrder\" : 0 }");
    benefitArray.put(benefitObject);

    ProductDataConverterUtil.populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);
    categoryData = planData.getBenefitCategories().get(0);
    List<PlanBenefitData> benefitDataList = categoryData.getBenefits(); 
    Assert.assertEquals(1, planData.getBenefitCategories().size());
    PlanBenefitData benefitData = benefitDataList.get(0);
    Assert.assertEquals("A", benefitData.getInternalCode());
    Assert.assertEquals("A", benefitData.getDisplayName());
    Assert.assertEquals(0, benefitData.getSortOrder());
    Assert.assertEquals(0, benefitData.getValues().size());
    
    JSONArray benefitValuesJSONArray = new JSONArray();
    JSONObject benefitValueObject = new JSONObject("{ \"code\" : \"type\", \"value\" : \"A1\", \"description\" : \"Test\"}");
    benefitValuesJSONArray.put(benefitValueObject);
    benefitObject.put("values", benefitValuesJSONArray);
    
    ProductDataConverterUtil.populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);
    categoryData = planData.getBenefitCategories().get(0);
    benefitDataList = categoryData.getBenefits(); 
    Assert.assertEquals(1, planData.getBenefitCategories().size());
    benefitData = benefitDataList.get(0);
    Assert.assertEquals(1, benefitData.getValues().size());
  }
  
  @Test
  public void testPopulateMLRFromShoppingPlanMonthlyCostJSON() throws JSONException
  {
    PlanData planData = new PlanData();
    JSONObject monthlyCostObject = new JSONObject();
    ProductDataConverterUtil.populateMLRFromShoppingPlanMonthlyCostJSON(planData, monthlyCostObject);
    Assert.assertEquals(0, planData.getMemberLevelRates().size());
    Assert.assertEquals(0, planData.getMemberLevelTobaccoSurcharges().size());
    
    JSONObject memberLevelRatingObject = new JSONObject();
    monthlyCostObject.put("memberLevelRating", memberLevelRatingObject);
    ProductDataConverterUtil.populateMLRFromShoppingPlanMonthlyCostJSON(planData, monthlyCostObject);
    Assert.assertEquals(0, planData.getMemberLevelRates().size());
    
    JSONArray memberRatesArray = new JSONArray();
    memberLevelRatingObject.put("memberRates", memberRatesArray);
    ProductDataConverterUtil.populateMLRFromShoppingPlanMonthlyCostJSON(planData, monthlyCostObject);
    Assert.assertEquals(0, planData.getMemberLevelRates().size());
    
    JSONObject mlrRatesObject = new JSONObject("{ \"memberId\" : \"M1\", \"amount\" : \"100.00\" }");
    memberRatesArray.put(mlrRatesObject);
    ProductDataConverterUtil.populateMLRFromShoppingPlanMonthlyCostJSON(planData, monthlyCostObject);
    Assert.assertEquals(1, planData.getMemberLevelRates().size());
    Assert.assertEquals("100.00", planData.getMemberLevelRates().get("M1"));
    Assert.assertEquals("0", planData.getMemberLevelTobaccoSurcharges().get("M1"));
    
    mlrRatesObject.put("tobaccoSurcharge", "5.00");
    ProductDataConverterUtil.populateMLRFromShoppingPlanMonthlyCostJSON(planData, monthlyCostObject);
    Assert.assertEquals("5.00", planData.getMemberLevelTobaccoSurcharges().get("M1"));
  }
  
  @Test
  public void testPopulateCostFromShoppingPlanJSON() throws JSONException
  {
    PlanData planData = new PlanData();
    JSONObject planJSON = new JSONObject();
    ProductDataConverterUtil.populateCostFromShoppingPlanJSON(planData, planJSON);
    Assert.assertNull(planData.getMemberLevelRates());
    Assert.assertNull(planData.getMemberLevelTobaccoSurcharges());
    Assert.assertNull(planData.getRate());
    Assert.assertNull(planData.getTobaccoSurcharge());
    Assert.assertFalse(planData.isMemberLevelRated());
    
    JSONObject costObject = new JSONObject();
    planJSON.put("cost", costObject);
    ProductDataConverterUtil.populateCostFromShoppingPlanJSON(planData, planJSON);
    Assert.assertNull(planData.getRate());
    
    JSONObject monthlyCostObject = new JSONObject("{ \"tobaccoSurcharge\" : 5.00 }");
    costObject.put("monthly", monthlyCostObject);
    ProductDataConverterUtil.populateCostFromShoppingPlanJSON(planData, planJSON);
    Assert.assertNull(planData.getRate());
    Assert.assertEquals(5d, planData.getTobaccoSurcharge().doubleValue());
    Assert.assertFalse(planData.isMemberLevelRated());

    JSONObject monthlyRateObject = new JSONObject("{ \"amount\" : 125.00 }");
    monthlyCostObject.put("rate", monthlyRateObject);
    ProductDataConverterUtil.populateCostFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(125d, planData.getRate().doubleValue());
    
    monthlyCostObject.put("memberLevelRated", Boolean.TRUE);
    ProductDataConverterUtil.populateCostFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(0, planData.getMemberLevelRates().size());
    Assert.assertEquals(0, planData.getMemberLevelTobaccoSurcharges().size());
    Assert.assertTrue(planData.isMemberLevelRated());
  }
  
  @Test
  public void testPopulateNetworksFromShoppingPlanJSON() throws JSONException
  {
    PlanData planData = new PlanData();
    JSONObject planJSON = new JSONObject();
    ProductDataConverterUtil.populateNetworksFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(0, planData.getNetworks().size());
    
    JSONArray networksArray = new JSONArray();
    planJSON.put("networks", networksArray);
    ProductDataConverterUtil.populateNetworksFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(0, planData.getNetworks().size());
    
    networksArray.put("N1");
    ProductDataConverterUtil.populateNetworksFromShoppingPlanJSON(planData, planJSON);
    Assert.assertEquals(1, planData.getNetworks().size());
    Assert.assertEquals("N1", planData.getNetworks().get(0));
  }
  
  @Test
  public void testConvertFromShoppingPlanJSON() throws JSONException
  {
    JSONObject planJSON = new JSONObject();
    PlanData planData = ProductDataConverterUtil.convertFromShoppingPlanJSON(planJSON);
    Assert.assertNull(planData.getIdentifier());
    Assert.assertNull(planData.getInternalCode());
    Assert.assertNull(planData.getPlanType());
    Assert.assertNull(planData.getPlanName());
    Assert.assertNull(planData.getMetalTier());
    ProductData productData = planData.getProductData();
    Assert.assertNotNull(productData);
    Assert.assertNull(productData.getProductLineName());
    Assert.assertNull(productData.getProductLineCode());
    Assert.assertNull(planData.getMemberTypeCosts());
    Assert.assertEquals(0, planData.getBenefitCategories().size());
    Assert.assertNull(planData.getMemberLevelRates());
    Assert.assertNull(planData.getMemberLevelTobaccoSurcharges());
    Assert.assertNull(planData.getRate());
    Assert.assertNull(planData.getTobaccoSurcharge());
    Assert.assertFalse(planData.isMemberLevelRated());
  }
  
  @Test
  public void testConvertFromShoppingPlansJSON() throws JSONException
  {
    JSONArray planArray = new JSONArray();
    List<PlanData> planDataList = ProductDataConverterUtil.convertFromShoppingPlansJSON(planArray);
    Assert.assertEquals(0, planDataList.size());
    JSONObject planObject = new JSONObject();
    planArray.put(planObject);
    planDataList = ProductDataConverterUtil.convertFromShoppingPlansJSON(planArray);
    Assert.assertEquals(1, planDataList.size());
  }
}
