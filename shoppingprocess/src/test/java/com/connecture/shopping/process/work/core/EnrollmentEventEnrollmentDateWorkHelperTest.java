package com.connecture.shopping.process.work.core;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.work.core.EnrollmentEventEnrollmentDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class EnrollmentEventEnrollmentDateWorkHelperTest
{
  @Test
  public void testGetEnrollmentEndDate() throws Exception
  {
    JSONObject accountObject = new JSONObject();
    EnrollmentEventRulesEngineBean mockBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    EnrollmentEventDomainModel model = new EnrollmentEventDomainModel();
    Mockito.when(mockBean.executeRule()).thenReturn(model);

    // No HCR enrollment end date
    String result = EnrollmentEventEnrollmentDateWorkHelper.getEnrollmentEndDate(accountObject, mockBean);
    Assert.assertNull(result);

    final String HCR_ENROLLMENT_END_DATE = "9/9/9999";
    model.getApplication().getSpecialEnrollment().setOpenEnrollmentEndDate(HCR_ENROLLMENT_END_DATE);
    
    // We expect to get the HCR enrollment end date
    result = EnrollmentEventEnrollmentDateWorkHelper.getEnrollmentEndDate(accountObject, mockBean);
    Assert.assertNotNull(result);
    Assert.assertEquals(HCR_ENROLLMENT_END_DATE, result);
  }
}
