package com.connecture.shopping.process.service.data.converter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.service.data.ProviderData;
import com.connecture.shopping.process.service.data.ProvidersData;

public class ProviderSearchDataConverterTest
{
  @Test
  public void testConvertData() throws Exception
  {
    ProviderSearchDataConverter converter = new ProviderSearchDataConverter();
    ProvidersData result = converter.convertData(null);
    Assert.assertTrue(result.getProviders().isEmpty());

    JSONArray providersArray = new JSONArray();
    result = converter.convertData(providersArray.toString());
    Assert.assertTrue(result.getProviders().isEmpty());
    
    JSONObject providerJSON = new JSONObject();
    providersArray.put(providerJSON);
    result = converter.convertData(providersArray.toString());
    Assert.assertEquals(0, result.getProviders().size());
    
    providerJSON.put("type", "Unsupported Type");
    result = converter.convertData(providersArray.toString());
    Assert.assertEquals(0, result.getProviders().size());

    providerJSON.put("type", ProviderSearchDataConverter.PHYSICIAN_TYPE);
    result = converter.convertData(providersArray.toString());
    Assert.assertEquals(0, result.getProviders().size());

    providerJSON.put("xrefId", "1");
    result = converter.convertData(providersArray.toString());
    Assert.assertEquals(0, result.getProviders().size());
    
    JSONObject nameJSON = new JSONObject();
    providerJSON.put("name", nameJSON);
    result = converter.convertData(providersArray.toString());
    Assert.assertEquals(1, result.getProviders().size());
    ProviderData providerData = result.getProviders().get(0);
    Assert.assertEquals("1", providerData.getId());
    Assert.assertEquals("", providerData.getName().getPrefix());
    Assert.assertEquals("", providerData.getName().getFirst());
    Assert.assertEquals("", providerData.getName().getLast());
    Assert.assertEquals("", providerData.getName().getMiddle());
    
    nameJSON.put("prefix", "Cpt.");
    nameJSON.put("first", "James");
    nameJSON.put("last", "Kirk");
    nameJSON.put("middle", "T");
    result = converter.convertData(providersArray.toString());
    Assert.assertEquals(1, result.getProviders().size());
    providerData = result.getProviders().get(0);
    Assert.assertEquals("Cpt.", providerData.getName().getPrefix());
    Assert.assertEquals("James", providerData.getName().getFirst());
    Assert.assertEquals("Kirk", providerData.getName().getLast());
    Assert.assertEquals("T", providerData.getName().getMiddle());
  }
}
