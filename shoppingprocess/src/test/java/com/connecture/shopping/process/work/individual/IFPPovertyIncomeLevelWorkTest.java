package com.connecture.shopping.process.work.individual;

import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.camel.request.ShoppingRulesEngineDataRequest;
import com.connecture.shopping.process.camel.request.StateKeyForCountyAndZipCodeDataRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ConsumerProfile;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.service.data.cache.DataProviderKey;
import com.connecture.shopping.process.work.individual.provider.IFPPovertyIncomeLevelDataProvider;

public class IFPPovertyIncomeLevelWorkTest
{

  @Test
  public void testProcessData() throws Exception
  {
    IFPPovertyIncomeLevelWork work = new IFPPovertyIncomeLevelWork();
    
    JSONObject results = work.processData();
    Assert.assertNotNull(results);
    Assert.assertTrue(results.has("povertyIncomeLevel"));

    final ShoppingDomainModel domainModel = new ShoppingDomainModel();
    
    work = new IFPPovertyIncomeLevelWork()
    {
      @Override
      public ShoppingDomainModel getResult()
      {
        return domainModel;
      }
    };

    results = work.processData();
    Assert.assertNotNull(results);
    Assert.assertTrue(results.has("povertyIncomeLevel"));
    Assert.assertFalse(results.getJSONObject("povertyIncomeLevel").has("povertyIncomeLevel"));
    
    Consumer consumer = new Consumer();
    ConsumerProfile profile = new ConsumerProfile();
    profile.setValue(100);
    List<ConsumerProfile> profiles = Arrays.asList(profile);
    consumer.setProfile(profiles);
    List<Consumer> consumers = Arrays.asList(consumer);
    domainModel.getRequest().setConsumer(consumers);
    
    results = work.processData();
    Assert.assertNotNull(results);
    Assert.assertTrue(results.has("povertyIncomeLevel"));
    Assert.assertTrue(results.getJSONObject("povertyIncomeLevel").has("povertyIncomeLevel"));
    Assert.assertEquals(100, results.getJSONObject("povertyIncomeLevel").getInt("povertyIncomeLevel"));
    
    domainModel.getRequest().setError("Oh noes!");
    results = work.processData();
    Assert.assertNotNull(results);
    Assert.assertTrue(results.has("povertyIncomeLevel"));
  }

  @Test
  public void testGet() throws Exception
  {
    IFPPovertyIncomeLevelWork work = new IFPPovertyIncomeLevelWork();
    work.get();
    
    ShoppingDomainModel domainModel = work.getResult();
    Assert.assertNull(domainModel);
    
    String transactionId = "0";
    work.setTransactionId(transactionId);
    
    Account account = Mockito.mock(Account.class);
    JSONObject accountObject = new JSONObject("{ }");
    Mockito.when(account.getAccount(Mockito.eq(transactionId))).thenReturn(accountObject);
    work.setAccount(account);
    
    work.get();
    
    Mockito.verify(account).getAccount(Mockito.eq(transactionId));
    
    domainModel = work.getResult();
    Assert.assertNull(domainModel);

    long effectiveMs = ShoppingDateUtils.stringToDate("01/01/2013").getTime();
    accountObject.put("effectiveDate", "01/01/2013");

    ShoppingRulesEngineDataRequest rulesEngineDataRequest = Mockito.mock(ShoppingRulesEngineDataRequest.class);
    work.setRulesEngineDataRequest(rulesEngineDataRequest);
    
    IFPPovertyIncomeLevelDataProvider dataProvider = Mockito.mock(IFPPovertyIncomeLevelDataProvider.class);
    work.setDataProvider(dataProvider);

    StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest = Mockito.mock(StateKeyForCountyAndZipCodeDataRequest.class);
    work.setStateKeyForCountyAndZipCodeDataRequest(stateKeyForCountyAndZipCodeDataRequest);
    
    String rulesEngineGroupId = "groupId";
    work.setRulesEngineGroupId(rulesEngineGroupId);
    
    @SuppressWarnings("unchecked")
    List<String> ruleSet = (List<String>)Mockito.mock(List.class);
    work.setRuleSet(ruleSet);
    
    DataCache dataCache = Mockito.mock(DataCache.class);
    work.setDataCache(dataCache);
    
    ShoppingDomainModel testDomainModel = Mockito.mock(ShoppingDomainModel.class);
    Mockito.when(dataCache.getData(Mockito.any(DataProviderKey.class), Mockito.eq(dataProvider))).thenReturn(testDomainModel);
    
    work.get();
    
    domainModel = work.getResult();
    Assert.assertNotNull(domainModel);
    
    Mockito.verify(dataProvider).setRuleSet(Mockito.eq(ruleSet));
    Mockito.verify(dataProvider).setRulesEngineGroupId(Mockito.eq(rulesEngineGroupId));
    Mockito.verify(dataProvider).setRequest(Mockito.eq(rulesEngineDataRequest));
    Mockito.verify(dataProvider).setEffectiveDateMs(Mockito.eq(effectiveMs));
    Mockito.verify(dataProvider).setTransactionId(Mockito.eq(transactionId));
    Mockito.verify(dataProvider).setAccount(Mockito.eq(accountObject));
    Mockito.verify(dataProvider).setStateKeyForCountyAndZipCodeDataRequest(Mockito.eq(stateKeyForCountyAndZipCodeDataRequest));
  }

  @Test
  public void testForCompletion() throws Exception
  {
    IFPPovertyIncomeLevelWork work = new IFPPovertyIncomeLevelWork();
    work.create();
    work.update();
    work.delete();
  }
}
