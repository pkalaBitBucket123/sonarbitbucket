package com.connecture.shopping.process.common;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

public class ShoppingDateUtilsTest
{
  @Test
  public void testDateToString()
  {
    assertNull(ShoppingDateUtils.dateToString(null));
    Calendar calendar = new GregorianCalendar(2013, 7, 1);
    assertEquals("08/01/2013", ShoppingDateUtils.dateToString(calendar.getTime()));
  }
  
  @Test
  public void testStringToDate()
  {
    assertNull(ShoppingDateUtils.stringToDate(null));
    assertNull(ShoppingDateUtils.stringToDate("123ABC"));
    Calendar calendar = new GregorianCalendar(2013, 7, 1);
    assertEquals(calendar.getTime(), ShoppingDateUtils.stringToDate("08/01/2013"));
    assertEquals(calendar.getTime(), ShoppingDateUtils.stringToDate("08012013"));
  }
}
