package com.connecture.shopping.process.work.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.data.Address;
import com.connecture.model.data.Name;
import com.connecture.model.data.ShoppingProducer;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingFfmMemberEligibilityData;
import com.connecture.model.integration.data.IAToShoppingMemberDetails;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.Request;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsForm;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldOption;

public class ConsumerDemographicsWorkTest
{
  @Test
  public void testProcessData() throws Exception
  {
    ConsumerDemographicsWork work = new TestConsumerDemographicsWork();
    Assert.assertEquals("{}", work.processData().toString());
  }
  
  @Test
  public void testProcessProducerDemographics() throws JSONException
  {
    ConsumerDemographicsWork work = new TestConsumerDemographicsWork();
    
    ShoppingProducer shoppingProducer = new ShoppingProducer();
    JSONObject result = work.processProducerDemographics(shoppingProducer);
    Assert.assertEquals("{}", result.getJSONObject("name").toString());
    Assert.assertEquals("{}", result.getJSONObject("address").toString());
    JSONObject contactObject = result.getJSONObject("contact");
    Assert.assertNull(contactObject.optJSONObject("areacode"));
    Assert.assertNull(contactObject.optString("phonenumber", null));
    Assert.assertNull(contactObject.optJSONObject("extension"));
    Assert.assertNull(contactObject.optString("email", null));
    Assert.assertNull(result.optString("agency", null));
    
    Name name = new Name();
    name.setFirst("TestFirst");
    name.setLast("TestLast");
    shoppingProducer.setName(name);
    
    Address address = new Address();
    address.setStreetAddress1("TestStreetAddress1");
    address.setCity("TestCity");
    address.setStateCode("TestStateCode");
    address.setZipCode("TestZipCode");
    shoppingProducer.setAddress(address);
    
    shoppingProducer.setAreaCode("TestAreaCode");
    shoppingProducer.setPhoneNumber("TestPhone");
    shoppingProducer.setExtension("TestExtension");
    shoppingProducer.setEmailAddress("TestEmail");
    shoppingProducer.setAgency("TestAgency");
    
    result = work.processProducerDemographics(shoppingProducer);
    JSONObject producerName = result.getJSONObject("name");
    Assert.assertEquals("TestFirst", producerName.getString("first"));
    Assert.assertEquals("TestLast", producerName.getString("last"));
    
    JSONObject producerAddressObject = result.getJSONObject("address");
    Assert.assertEquals("TestStreetAddress1", producerAddressObject.getString("street"));
    Assert.assertEquals("TestCity", producerAddressObject.getString("city"));
    Assert.assertEquals("TestStateCode", producerAddressObject.getString("state"));
    Assert.assertEquals("TestZipCode", producerAddressObject.getString("zipCode"));
    
    contactObject = result.getJSONObject("contact");
    Assert.assertEquals("TestAreaCode", contactObject.getString("areacode"));
    Assert.assertEquals("TestPhone", contactObject.getString("phonenumber"));
    Assert.assertEquals("TestExtension", contactObject.getString("extension"));
    Assert.assertEquals("TestEmail", contactObject.getString("email"));
    
    Assert.assertEquals("TestAgency", result.getString("agency"));
  }
  
  @Test
  public void testBuildOptionValueTypeMap() throws Exception
  {
    ConsumerDemographicsWork work = new TestConsumerDemographicsWork();
    ShoppingRulesEngineBean mockBean = Mockito.mock(ShoppingRulesEngineBean.class);
    work.setShoppingDemographicsBean(mockBean);
    ShoppingDomainModel domainData = new ShoppingDomainModel();
    Mockito.when(mockBean.executeRule()).thenReturn(domainData);
    Map<String, String> result = work.buildOptionValueTypeMap("01/01/2014");
    Assert.assertTrue(result.isEmpty());
    
    Request request = new Request();
    domainData.setRequest(request);
    DemographicsForm demographicsForm = new DemographicsForm();
    request.setDemographicsForm(demographicsForm);
    List<DemographicsFormField> fields = new ArrayList<DemographicsFormField>();
    demographicsForm.setField(fields);
    result = work.buildOptionValueTypeMap("01/01/2014");
    Assert.assertTrue(result.isEmpty());
    
    DemographicsFormField field = new DemographicsFormField();
    List<DemographicsFormFieldOption> options = new ArrayList<DemographicsFormFieldOption>();
    field.setOption(options);
    fields.add(field);
    result = work.buildOptionValueTypeMap("01/01/2014");
    Assert.assertTrue(result.isEmpty());
    
    DemographicsFormFieldOption option = new DemographicsFormFieldOption();
    option.setValue("A");
    option.setDataGroupType("G");
    options.add(option);

    result = work.buildOptionValueTypeMap("01/01/2014");
    Assert.assertEquals(1, result.size());
    Assert.assertEquals("G", result.get("A"));
  }
  
  @Test
  public void testGetEffectiveDate() throws Exception
  {
    ConsumerDemographicsWork work = new TestConsumerDemographicsWork();
    EnrollmentEventRulesEngineBean mockBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    Account account = Mockito.mock(Account.class);
    JSONObject accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.setAccount(account);
    work.setHcrEffectiveDateBean(mockBean);
    EnrollmentEventDomainModel domainData = new EnrollmentEventDomainModel();
    Mockito.when(mockBean.executeRule()).thenReturn(domainData);
    String result = work.getEffectiveDate();
    Assert.assertNull(result);
  }
  
  @Test
  public void testPopulateAccountDemographicData() throws Exception
  {
    ConsumerDemographicsWork work = new TestConsumerDemographicsWork();
    JSONObject accountJSON = new JSONObject();
    IAToShoppingData individualData = new IAToShoppingData();
    Map<String, String> optionValueTypeMap = new HashMap<String, String>();
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    Assert.assertEquals(-1d, accountJSON.optDouble("householdPremiumTaxCredit", -1d));
    Assert.assertNotNull(accountJSON.optJSONArray("members"));
    Assert.assertNull(accountJSON.optString("zipCode", null));
    Assert.assertNull(accountJSON.optString("countyId", null));
    Assert.assertNull(accountJSON.optString("eventDate", null));
    Assert.assertNull(accountJSON.optString("eventType", null));
    
    List<IAToShoppingMemberDetails> membersData = new ArrayList<IAToShoppingMemberDetails>();
    individualData.setMemberDetails(membersData);
    
    IAToShoppingMemberDetails memberDetails = new IAToShoppingMemberDetails();
    membersData.add(memberDetails);
    memberDetails.setMemberId(1);
    memberDetails.setFirstName("firstName");
    memberDetails.setMemberType("Primary");
    
    optionValueTypeMap.put("Primary", "PRIMARY");

    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    JSONArray memberArray = accountJSON.optJSONArray("members");
    Assert.assertEquals(1, memberArray.length());
    JSONObject memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals(false, memberObject.getBoolean("isNew"));
    Assert.assertEquals(1l, memberObject.getLong("memberRefName"));
    Assert.assertEquals("firstName", memberObject.getJSONObject("name").getString("first"));
    Assert.assertNull(memberObject.optString("gender", null));
    Assert.assertEquals("Primary", memberObject.getString("memberRelationship"));
    Assert.assertEquals("PRIMARY", memberObject.getString("relationshipKey"));
    Assert.assertNull(memberObject.optString("isSmoker", null));
    Assert.assertNull(memberObject.optString("birthDate", null));
    Assert.assertNull(memberObject.optJSONObject("ffmData"));
    
    Date birthDate = new GregorianCalendar(2013, 0, 1).getTime(); 
    memberDetails.setDateOfBirth(birthDate);
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals(ShoppingDateUtils.dateToString(birthDate), memberObject.getString("birthDate"));

    memberDetails.setGender("M");
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals("M", memberObject.getString("gender"));
    
    memberDetails.setSmokerStatus("TRUE");
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals("y", memberObject.getString("isSmoker"));

    memberDetails.setSmokerStatus("FALSE");
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals("n", memberObject.getString("isSmoker"));
    
    IAToShoppingFfmMemberEligibilityData ffmMemberEligibilityData = new IAToShoppingFfmMemberEligibilityData();
    memberDetails.setFfmMemberEligibilityData(ffmMemberEligibilityData);
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertTrue(Double.isNaN(accountJSON.optDouble("householdPremiumTaxCredit")));
    Assert.assertEquals("{}", memberObject.optJSONObject("ffmData").toString());

    ffmMemberEligibilityData.setAptcEligibIndicator(true);
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertTrue(Double.isNaN(accountJSON.optDouble("householdPremiumTaxCredit")));

    ffmMemberEligibilityData.setMonthlyAPTCAmount(new BigDecimal(100.0d));
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    JSONObject ffmDataObject = memberObject.getJSONObject("ffmData");
    Assert.assertEquals(100.0d, ((BigDecimal)ffmDataObject.get("premiumTaxCredit")).doubleValue());

    memberDetails.setZipCode("90210");
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals("90210", accountJSON.getString("zipCode"));
    
    memberDetails.setCounty("1");
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    memberArray = accountJSON.optJSONArray("members");
    memberObject = memberArray.getJSONObject(0);
    Assert.assertEquals("1", accountJSON.getString("countyId"));
    
    Date specialEnrollmentEventDate = new GregorianCalendar(2013, 0, 1).getTime(); 
    individualData.setSpecialEnrollmentEventDate(specialEnrollmentEventDate);
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    Assert.assertNull(accountJSON.optString("eventDate", null));
    Assert.assertNull(accountJSON.optString("eventType", null));
    
    individualData.setSpecialEnrollmentReasonKey("A");
    work.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);
    Assert.assertEquals(ShoppingDateUtils.dateToString(specialEnrollmentEventDate), accountJSON.getString("eventDate"));
    Assert.assertEquals("A", accountJSON.getString("eventType"));
  }
  
  @Test
  public void testMisc()
  {
    ConsumerDemographicsWork work = new TestConsumerDemographicsWork();
    JSONObject paramsObject = new JSONObject();
    work.setParams(paramsObject);
    Assert.assertEquals("{}", work.getParams().toString());
    
    work.setTransactionId("A");
    Assert.assertEquals("A", work.getTransactionId());
  }
}

class TestConsumerDemographicsWork extends ConsumerDemographicsWork
{
}
