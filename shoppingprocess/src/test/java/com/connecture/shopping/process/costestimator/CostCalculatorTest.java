package com.connecture.shopping.process.costestimator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

import org.junit.Test;

public class CostCalculatorTest {
//	private static final Logger LOG = Logger.getLogger(CostCalculatorTest.class);
	
	// RegExp and ReplacementExp for converting CVS to TestCases
	// Step 1
	//   RegExp:            , B
	//   ReplacementExp:    |B
	// Step 2
	//   RegExp:            ^\(B.*\),
	//   ReplacementExp:    "\1",
	// Step 3
	//   RegExp:            ^"\(B.*\)",[ ]*[0-9]*,[ ]*\([0-9]*\),[ ]*\([0-9]*\),[ ]*\([0-9]*\),[ ]*\([0-9]*\),[ ]*\([0-9]*\),[ ]*\([0-9]*\)
	//   ReplacementExp:    new TestCase("\i", P1, Arrays.asList(new Benefit[]{ \1 }), M1, M1, new EstimatedCosts(\2d, \3d, \4d, \5d, \6d, \7d)),
	// Step 4
	//   RegExp:			  \(|B\)
	//   ReplacementExp:    , B
	
	@Test
	public void testDeductibleAndOOPMax() {
		Member M1 = new Member("M1");
    Member M2 = new Member("M2");
    Member M3 = new Member("M3");
		
		PlanBenefit P1 = new PlanBenefit("P1", 1000d, 3000d, 3000d, 9000d);
		PlanBenefit P2 = new PlanBenefit("P2",    0d, 3000d, 0d, 9000d);
		PlanBenefit P3 = new PlanBenefit("P3", 1000d,    0d, 3000d, 0d); 
		PlanBenefit P4 = new PlanBenefit("P4",    0d,    0d, 0d, 0d);
    PlanBenefit P5 = new PlanBenefit("P5",    0d,    0d, 3000d, 9000d);
    PlanBenefit P6 = new PlanBenefit("P6",    0d,    0d, 0d, 9000d);
    PlanBenefit P7 = new PlanBenefit("P7",    0d,    0d, 3000d, 0d);
    PlanBenefit P8 = new PlanBenefit("P8",    0d,    0d, 0d, 0d);
		
		Benefit B1 = new Benefit("B1",   50d,  true, false, false, 0);
		Benefit B2 = new Benefit("B2", 1000d,  true, false, false, 0);
		Benefit B3 = new Benefit("B3",  100d, false,  true, false, 0);
		Benefit B4 = new Benefit("B4", 3000d, false,  true, false, 0);
		Benefit B5 = new Benefit("B5",  200d, false, false, false, 0); 
		Benefit B6 = new Benefit("B6",  400d,  true,  true, false, 0);
		
		List<TestCase> testCases = new ArrayList<TestCase>(Arrays.asList(new TestCase[] {
				// P1 - DEDUCTIBLE NOT MET, OOPMAX NOT MET
				new TestCase("1", P1, Arrays.asList(new Benefit[]{ B1 }), M1, new EstimatedCosts(50d, 0d, 50d, 0d, 0d, 50d)),
				new TestCase("2", P1, Arrays.asList(new Benefit[]{ B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 0d, 100d)),
				new TestCase("3", P1, Arrays.asList(new Benefit[]{ B1, B3 }), M1, new EstimatedCosts(50d, 100d, 50d, 100d, 0d, 150d)),
				new TestCase("4", P1, Arrays.asList(new Benefit[]{ B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 200d, 200d)),
				new TestCase("5", P1, Arrays.asList(new Benefit[]{ B1, B5 }), M1, new EstimatedCosts(50d, 0d, 50d, 0d, 200d, 250d)),
				new TestCase("6", P1, Arrays.asList(new Benefit[]{ B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 200d, 300d)),
				new TestCase("7", P1, Arrays.asList(new Benefit[]{ B1, B3, B5 }), M1, new EstimatedCosts(50d, 100d, 50d, 100d, 200d, 350d)),
				new TestCase("8", P1, Arrays.asList(new Benefit[]{ B6 }), M1, new EstimatedCosts(400d, 0d, 400d, 0d, 0d, 400d)),
				new TestCase("9", P1, Arrays.asList(new Benefit[]{ B1, B6 }), M1, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d)),
				new TestCase("10", P1, Arrays.asList(new Benefit[]{ B3, B6 }), M1, new EstimatedCosts(400d, 100d, 400d, 100d, 0d, 500d)),
				new TestCase("11", P1, Arrays.asList(new Benefit[]{ B1, B3, B6 }), M1, new EstimatedCosts(450d, 100d, 450d, 100d, 0d, 550d)),
				new TestCase("12", P1, Arrays.asList(new Benefit[]{ B5, B6 }), M1, new EstimatedCosts(400d, 0d, 400d, 0d, 200d, 600d)),
				new TestCase("13", P1, Arrays.asList(new Benefit[]{ B1, B5, B6 }), M1, new EstimatedCosts(450d, 0d, 450d, 0d, 200d, 650d)),
				new TestCase("14", P1, Arrays.asList(new Benefit[]{ B3, B5, B6 }), M1, new EstimatedCosts(400d, 100d, 400d, 100d, 200d, 700d)),
				new TestCase("15", P1, Arrays.asList(new Benefit[]{ B1, B3, B5, B6 }), M1, new EstimatedCosts(450d, 100d, 450d, 100d, 200d, 750d)),

				// P1 - DEDUCTIBLE NOT MET, OOPMAX MET
				new TestCase("16", P1, Arrays.asList(new Benefit[]{ B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 0d, 3000d)),
				new TestCase("17", P1, Arrays.asList(new Benefit[]{ B1, B4 }), M1, new EstimatedCosts(50d, 3000d, 50d, 3000d, 0d, 3050d)),
				new TestCase("18", P1, Arrays.asList(new Benefit[]{ B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 0d, 3000d)),
				new TestCase("19", P1, Arrays.asList(new Benefit[]{ B1, B3, B4 }), M1, new EstimatedCosts(50d, 3100d, 50d, 3000d, 0d, 3050d)),
				new TestCase("20", P1, Arrays.asList(new Benefit[]{ B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 200d, 3200d)),
				new TestCase("21", P1, Arrays.asList(new Benefit[]{ B1, B4, B5 }), M1, new EstimatedCosts(50d, 3000d, 50d, 3000d, 200d, 3250d)),
				new TestCase("22", P1, Arrays.asList(new Benefit[]{ B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 200d, 3200d)),
				new TestCase("23", P1, Arrays.asList(new Benefit[]{ B1, B3, B4, B5 }), M1, new EstimatedCosts(50d, 3100d, 50d, 3000d, 200d, 3250d)),
				new TestCase("24", P1, Arrays.asList(new Benefit[]{ B4, B6 }), M1, new EstimatedCosts(400d, 3000d, 400d, 3000d, 0d, 3400d)),
				new TestCase("25", P1, Arrays.asList(new Benefit[]{ B1, B4, B6 }), M1, new EstimatedCosts(450d, 3000d, 450d, 3000d, 0d, 3450d)),
				new TestCase("26", P1, Arrays.asList(new Benefit[]{ B3, B4, B6 }), M1, new EstimatedCosts(400d, 3100d, 400d, 3000d, 0d, 3400d)),
				new TestCase("27", P1, Arrays.asList(new Benefit[]{ B1, B3, B4, B6 }), M1, new EstimatedCosts(450d, 3100d, 450d, 3000d, 0d, 3450d)),
				new TestCase("28", P1, Arrays.asList(new Benefit[]{ B4, B5, B6 }), M1, new EstimatedCosts(400d, 3000d, 400d, 3000d, 200d, 3600d)),
				new TestCase("29", P1, Arrays.asList(new Benefit[]{ B1, B4, B5, B6 }), M1, new EstimatedCosts(450d, 3000d, 450d, 3000d, 200d, 3650d)),
				new TestCase("30", P1, Arrays.asList(new Benefit[]{ B3, B4, B5, B6 }), M1, new EstimatedCosts(400d, 3100d, 400d, 3000d, 200d, 3600d)),
				new TestCase("31", P1, Arrays.asList(new Benefit[]{ B1, B3, B4, B5, B6 }), M1, new EstimatedCosts(450d, 3100d, 450d, 3000d, 200d, 3650d)),

				// P1 - DEDUCTIBLE MET, OOPMAX NOT MET
				new TestCase("32", P1, Arrays.asList(new Benefit[]{ B2 }), M1, new EstimatedCosts(1000d, 0d, 1000d, 0d, 0d, 1000d)),
				new TestCase("33", P1, Arrays.asList(new Benefit[]{ B1, B2 }), M1, new EstimatedCosts(1050d, 0d, 1000d, 0d, 50d, 1050d)),
				new TestCase("34", P1, Arrays.asList(new Benefit[]{ B2, B3 }), M1, new EstimatedCosts(1000d, 100d, 1000d, 100d, 0d, 1100d)),
				new TestCase("35", P1, Arrays.asList(new Benefit[]{ B1, B2, B3 }), M1, new EstimatedCosts(1050d, 100d, 1000d, 100d, 50d, 1150d)),
				new TestCase("36", P1, Arrays.asList(new Benefit[]{ B2, B5 }), M1, new EstimatedCosts(1000d, 0d, 1000d, 0d, 200d, 1200d)),
				new TestCase("37", P1, Arrays.asList(new Benefit[]{ B1, B2, B5 }), M1, new EstimatedCosts(1050d, 0d, 1000d, 0d, 250d, 1250d)),
				new TestCase("38", P1, Arrays.asList(new Benefit[]{ B2, B3, B5 }), M1, new EstimatedCosts(1000d, 100d, 1000d, 100d, 200d, 1300d)),
				new TestCase("39", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B5 }), M1, new EstimatedCosts(1050d, 100d, 1000d, 100d, 250d, 1350d)),
				new TestCase("40", P1, Arrays.asList(new Benefit[]{ B2, B6 }), M1, new EstimatedCosts(1400d, 400d, 1000d, 400d, 0d, 1400d)),
				new TestCase("41", P1, Arrays.asList(new Benefit[]{ B1, B2, B6 }), M1, new EstimatedCosts(1450d, 400d, 1000d, 400d, 50d, 1450d)),
				new TestCase("42", P1, Arrays.asList(new Benefit[]{ B2, B3, B6 }), M1, new EstimatedCosts(1400d, 500d, 1000d, 500d, 0d, 1500d)),
				new TestCase("43", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B6 }), M1, new EstimatedCosts(1450d, 500d, 1000d, 500d, 50d, 1550d)),
				new TestCase("44", P1, Arrays.asList(new Benefit[]{ B2, B5, B6 }), M1, new EstimatedCosts(1400d, 400d, 1000d, 400d, 200d, 1600d)),
				new TestCase("45", P1, Arrays.asList(new Benefit[]{ B1, B2, B5, B6 }), M1, new EstimatedCosts(1450d, 400d, 1000d, 400d, 250d, 1650d)),
				new TestCase("46", P1, Arrays.asList(new Benefit[]{ B2, B3, B5, B6 }), M1, new EstimatedCosts(1400d, 500d, 1000d, 500d, 200d, 1700d)),
				new TestCase("47", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B5, B6 }), M1, new EstimatedCosts(1450d, 500d, 1000d, 500d, 250d, 1750d)),

				// P1 - DEDUCTIBLE MET, OOPMAX MET
				new TestCase("48", P1, Arrays.asList(new Benefit[]{ B2, B4 }), M1, new EstimatedCosts(1000d, 3000d, 1000d, 3000d, 0d, 4000d)),
				new TestCase("49", P1, Arrays.asList(new Benefit[]{ B1, B2, B4 }), M1, new EstimatedCosts(1050d, 3000d, 1000d, 3000d, 50d, 4050d)),
				new TestCase("50", P1, Arrays.asList(new Benefit[]{ B2, B3, B4 }), M1, new EstimatedCosts(1000d, 3100d, 1000d, 3000d, 0d, 4000d)),
				new TestCase("51", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), M1, new EstimatedCosts(1050d, 3100d, 1000d, 3000d, 50d, 4050d)),
				new TestCase("52", P1, Arrays.asList(new Benefit[]{ B2, B4, B5 }), M1, new EstimatedCosts(1000d, 3000d, 1000d, 3000d, 200d, 4200d)),
				new TestCase("53", P1, Arrays.asList(new Benefit[]{ B1, B2, B4, B5 }), M1, new EstimatedCosts(1050d, 3000d, 1000d, 3000d, 250d, 4250d)),
				new TestCase("54", P1, Arrays.asList(new Benefit[]{ B2, B3, B4, B5 }), M1, new EstimatedCosts(1000d, 3100d, 1000d, 3000d, 200d, 4200d)),
				new TestCase("55", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5 }), M1, new EstimatedCosts(1050d, 3100d, 1000d, 3000d, 250d, 4250d)),
				new TestCase("56", P1, Arrays.asList(new Benefit[]{ B2, B4, B6 }), M1, new EstimatedCosts(1400d, 3400d, 1000d, 3000d, 0d, 4000d)),
				new TestCase("57", P1, Arrays.asList(new Benefit[]{ B1, B2, B4, B6 }), M1, new EstimatedCosts(1450d, 3400d, 1000d, 3000d, 50d, 4050d)),
				new TestCase("58", P1, Arrays.asList(new Benefit[]{ B2, B3, B4, B6 }), M1, new EstimatedCosts(1400d, 3500d, 1000d, 3000d, 0d, 4000d)),
				new TestCase("59", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B6 }), M1, new EstimatedCosts(1450d, 3500d, 1000d, 3000d, 50d, 4050d)),
				new TestCase("60", P1, Arrays.asList(new Benefit[]{ B2, B4, B5, B6 }), M1, new EstimatedCosts(1400d, 3400d, 1000d, 3000d, 200d, 4200d)),
				new TestCase("61", P1, Arrays.asList(new Benefit[]{ B1, B2, B4, B5, B6 }), M1, new EstimatedCosts(1450d, 3400d, 1000d, 3000d, 250d, 4250d)),
				new TestCase("62", P1, Arrays.asList(new Benefit[]{ B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(1400d, 3500d, 1000d, 3000d, 200d, 4200d)),
				new TestCase("63", P1, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(1450d, 3500d, 1000d, 3000d, 250d, 4250d)),

				// P2 - NO DEDUCTIBLE, OOPMAX NOT MET
				new TestCase("64", P2, Arrays.asList(new Benefit[]{ B1 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 50d, 50d)),
				new TestCase("65", P2, Arrays.asList(new Benefit[]{ B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 0d, 100d)),
				new TestCase("66", P2, Arrays.asList(new Benefit[]{ B1, B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 50d, 150d)),
				new TestCase("67", P2, Arrays.asList(new Benefit[]{ B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 200d, 200d)),
				new TestCase("68", P2, Arrays.asList(new Benefit[]{ B1, B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 250d, 250d)),
				new TestCase("69", P2, Arrays.asList(new Benefit[]{ B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 200d, 300d)),
				new TestCase("70", P2, Arrays.asList(new Benefit[]{ B1, B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 250d, 350d)),
				new TestCase("71", P2, Arrays.asList(new Benefit[]{ B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 0d, 400d)),
				new TestCase("72", P2, Arrays.asList(new Benefit[]{ B1, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 50d, 450d)),
				new TestCase("73", P2, Arrays.asList(new Benefit[]{ B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 0d, 500d)),
				new TestCase("74", P2, Arrays.asList(new Benefit[]{ B1, B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 50d, 550d)),
				new TestCase("75", P2, Arrays.asList(new Benefit[]{ B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 200d, 600d)),
				new TestCase("76", P2, Arrays.asList(new Benefit[]{ B1, B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 250d, 650d)),
				new TestCase("77", P2, Arrays.asList(new Benefit[]{ B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 200d, 700d)),
				new TestCase("78", P2, Arrays.asList(new Benefit[]{ B1, B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 250d, 750d)),
				new TestCase("79", P2, Arrays.asList(new Benefit[]{ B2 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1000d, 1000d)),
				new TestCase("80", P2, Arrays.asList(new Benefit[]{ B1, B2 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1050d, 1050d)),
				new TestCase("81", P2, Arrays.asList(new Benefit[]{ B2, B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1000d, 1100d)),
				new TestCase("82", P2, Arrays.asList(new Benefit[]{ B1, B2, B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1050d, 1150d)),
				new TestCase("83", P2, Arrays.asList(new Benefit[]{ B2, B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1200d, 1200d)),
				new TestCase("84", P2, Arrays.asList(new Benefit[]{ B1, B2, B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1250d, 1250d)),
				new TestCase("85", P2, Arrays.asList(new Benefit[]{ B2, B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1200d, 1300d)),
				new TestCase("86", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1250d, 1350d)),
				new TestCase("87", P2, Arrays.asList(new Benefit[]{ B2, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1000d, 1400d)),
				new TestCase("88", P2, Arrays.asList(new Benefit[]{ B1, B2, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1050d, 1450d)),
				new TestCase("89", P2, Arrays.asList(new Benefit[]{ B2, B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1000d, 1500d)),
				new TestCase("90", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1050d, 1550d)),
				new TestCase("91", P2, Arrays.asList(new Benefit[]{ B2, B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1200d, 1600d)),
				new TestCase("92", P2, Arrays.asList(new Benefit[]{ B1, B2, B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1250d, 1650d)),
				new TestCase("93", P2, Arrays.asList(new Benefit[]{ B2, B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1200d, 1700d)),
				new TestCase("94", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1250d, 1750d)),

				// P2 - NO DEDUCTIBLE, OOPMAX MET
				new TestCase("95", P2, Arrays.asList(new Benefit[]{ B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 0d, 3000d)),
				new TestCase("96", P2, Arrays.asList(new Benefit[]{ B1, B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 50d, 3050d)),
				new TestCase("97", P2, Arrays.asList(new Benefit[]{ B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 0d, 3000d)),
				new TestCase("98", P2, Arrays.asList(new Benefit[]{ B1, B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 50d, 3050d)),
				new TestCase("99", P2, Arrays.asList(new Benefit[]{ B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 200d, 3200d)),
				new TestCase("100", P2, Arrays.asList(new Benefit[]{ B1, B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 250d, 3250d)),
				new TestCase("101", P2, Arrays.asList(new Benefit[]{ B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 200d, 3200d)),
				new TestCase("102", P2, Arrays.asList(new Benefit[]{ B1, B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 250d, 3250d)),
				new TestCase("103", P2, Arrays.asList(new Benefit[]{ B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 0d, 3000d)),
				new TestCase("104", P2, Arrays.asList(new Benefit[]{ B1, B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 50d, 3050d)),
				new TestCase("105", P2, Arrays.asList(new Benefit[]{ B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 0d, 3000d)),
				new TestCase("106", P2, Arrays.asList(new Benefit[]{ B1, B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 50d, 3050d)),
				new TestCase("107", P2, Arrays.asList(new Benefit[]{ B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 200d, 3200d)),
				new TestCase("108", P2, Arrays.asList(new Benefit[]{ B1, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 250d, 3250d)),
				new TestCase("109", P2, Arrays.asList(new Benefit[]{ B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 200d, 3200d)),
				new TestCase("110", P2, Arrays.asList(new Benefit[]{ B1, B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 250d, 3250d)),
				new TestCase("111", P2, Arrays.asList(new Benefit[]{ B2, B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1000d, 4000d)),
				new TestCase("112", P2, Arrays.asList(new Benefit[]{ B1, B2, B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1050d, 4050d)),
				new TestCase("113", P2, Arrays.asList(new Benefit[]{ B2, B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1000d, 4000d)),
				new TestCase("114", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1050d, 4050d)),
				new TestCase("115", P2, Arrays.asList(new Benefit[]{ B2, B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1200d, 4200d)),
				new TestCase("116", P2, Arrays.asList(new Benefit[]{ B1, B2, B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1250d, 4250d)),
				new TestCase("117", P2, Arrays.asList(new Benefit[]{ B2, B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1200d, 4200d)),
				new TestCase("118", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1250d, 4250d)),
				new TestCase("119", P2, Arrays.asList(new Benefit[]{ B2, B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 1000d, 4000d)),
				new TestCase("120", P2, Arrays.asList(new Benefit[]{ B1, B2, B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 1050d, 4050d)),
				new TestCase("121", P2, Arrays.asList(new Benefit[]{ B2, B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 1000d, 4000d)),
				new TestCase("122", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 1050d, 4050d)),
				new TestCase("123", P2, Arrays.asList(new Benefit[]{ B2, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 1200d, 4200d)),
				new TestCase("124", P2, Arrays.asList(new Benefit[]{ B1, B2, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3000d, 1250d, 4250d)),
				new TestCase("125", P2, Arrays.asList(new Benefit[]{ B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 1200d, 4200d)),
				new TestCase("126", P2, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3000d, 1250d, 4250d)),

				// P3 - DEDUCTIBLE NOT MET, NO OOPMAX
				new TestCase("127", P3, Arrays.asList(new Benefit[]{ B1 }), M1, new EstimatedCosts(50d, 0d, 50d, 0d, 0d, 50d)),
				new TestCase("128", P3, Arrays.asList(new Benefit[]{ B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 0d, 100d)),
				new TestCase("129", P3, Arrays.asList(new Benefit[]{ B1, B3 }), M1, new EstimatedCosts(50d, 100d, 50d, 100d, 0d, 150d)),
				new TestCase("130", P3, Arrays.asList(new Benefit[]{ B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 200d, 200d)),
				new TestCase("131", P3, Arrays.asList(new Benefit[]{ B1, B5 }), M1, new EstimatedCosts(50d, 0d, 50d, 0d, 200d, 250d)),
				new TestCase("132", P3, Arrays.asList(new Benefit[]{ B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 200d, 300d)),
				new TestCase("133", P3, Arrays.asList(new Benefit[]{ B1, B3, B5 }), M1, new EstimatedCosts(50d, 100d, 50d, 100d, 200d, 350d)),
				new TestCase("134", P3, Arrays.asList(new Benefit[]{ B6 }), M1, new EstimatedCosts(400d, 0d, 400d, 0d, 0d, 400d)),
				new TestCase("135", P3, Arrays.asList(new Benefit[]{ B1, B6 }), M1, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d)),
				new TestCase("136", P3, Arrays.asList(new Benefit[]{ B3, B6 }), M1, new EstimatedCosts(400d, 100d, 400d, 100d, 0d, 500d)),
				new TestCase("137", P3, Arrays.asList(new Benefit[]{ B1, B3, B6 }), M1, new EstimatedCosts(450d, 100d, 450d, 100d, 0d, 550d)),
				new TestCase("138", P3, Arrays.asList(new Benefit[]{ B5, B6 }), M1, new EstimatedCosts(400d, 0d, 400d, 0d, 200d, 600d)),
				new TestCase("139", P3, Arrays.asList(new Benefit[]{ B1, B5, B6 }), M1, new EstimatedCosts(450d, 0d, 450d, 0d, 200d, 650d)),
				new TestCase("140", P3, Arrays.asList(new Benefit[]{ B3, B5, B6 }), M1, new EstimatedCosts(400d, 100d, 400d, 100d, 200d, 700d)),
				new TestCase("141", P3, Arrays.asList(new Benefit[]{ B1, B3, B5, B6 }), M1, new EstimatedCosts(450d, 100d, 450d, 100d, 200d, 750d)),
				new TestCase("142", P3, Arrays.asList(new Benefit[]{ B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 0d, 3000d)),
				new TestCase("143", P3, Arrays.asList(new Benefit[]{ B1, B4 }), M1, new EstimatedCosts(50d, 3000d, 50d, 3000d, 0d, 3050d)),
				new TestCase("144", P3, Arrays.asList(new Benefit[]{ B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 0d, 3100d)),
				new TestCase("145", P3, Arrays.asList(new Benefit[]{ B1, B3, B4 }), M1, new EstimatedCosts(50d, 3100d, 50d, 3100d, 0d, 3150d)),
				new TestCase("146", P3, Arrays.asList(new Benefit[]{ B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 200d, 3200d)),
				new TestCase("147", P3, Arrays.asList(new Benefit[]{ B1, B4, B5 }), M1, new EstimatedCosts(50d, 3000d, 50d, 3000d, 200d, 3250d)),
				new TestCase("148", P3, Arrays.asList(new Benefit[]{ B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 200d, 3300d)),
				new TestCase("149", P3, Arrays.asList(new Benefit[]{ B1, B3, B4, B5 }), M1, new EstimatedCosts(50d, 3100d, 50d, 3100d, 200d, 3350d)),
				new TestCase("150", P3, Arrays.asList(new Benefit[]{ B4, B6 }), M1, new EstimatedCosts(400d, 3000d, 400d, 3000d, 0d, 3400d)),
				new TestCase("151", P3, Arrays.asList(new Benefit[]{ B1, B4, B6 }), M1, new EstimatedCosts(450d, 3000d, 450d, 3000d, 0d, 3450d)),
				new TestCase("152", P3, Arrays.asList(new Benefit[]{ B3, B4, B6 }), M1, new EstimatedCosts(400d, 3100d, 400d, 3100d, 0d, 3500d)),
				new TestCase("153", P3, Arrays.asList(new Benefit[]{ B1, B3, B4, B6 }), M1, new EstimatedCosts(450d, 3100d, 450d, 3100d, 0d, 3550d)),
				new TestCase("154", P3, Arrays.asList(new Benefit[]{ B4, B5, B6 }), M1, new EstimatedCosts(400d, 3000d, 400d, 3000d, 200d, 3600d)),
				new TestCase("155", P3, Arrays.asList(new Benefit[]{ B1, B4, B5, B6 }), M1, new EstimatedCosts(450d, 3000d, 450d, 3000d, 200d, 3650d)),
				new TestCase("156", P3, Arrays.asList(new Benefit[]{ B3, B4, B5, B6 }), M1, new EstimatedCosts(400d, 3100d, 400d, 3100d, 200d, 3700d)),
				new TestCase("157", P3, Arrays.asList(new Benefit[]{ B1, B3, B4, B5, B6 }), M1, new EstimatedCosts(450d, 3100d, 450d, 3100d, 200d, 3750d)),

				// P3 - DEDUCTIBLE MET, NO OOPMAX
				new TestCase("158", P3, Arrays.asList(new Benefit[]{ B2 }), M1, new EstimatedCosts(1000d, 0d, 1000d, 0d, 0d, 1000d)),
				new TestCase("159", P3, Arrays.asList(new Benefit[]{ B1, B2 }), M1, new EstimatedCosts(1050d, 0d, 1000d, 0d, 50d, 1050d)),
				new TestCase("160", P3, Arrays.asList(new Benefit[]{ B2, B3 }), M1, new EstimatedCosts(1000d, 100d, 1000d, 100d, 0d, 1100d)),
				new TestCase("161", P3, Arrays.asList(new Benefit[]{ B1, B2, B3 }), M1, new EstimatedCosts(1050d, 100d, 1000d, 100d, 50d, 1150d)),
				new TestCase("162", P3, Arrays.asList(new Benefit[]{ B2, B5 }), M1, new EstimatedCosts(1000d, 0d, 1000d, 0d, 200d, 1200d)),
				new TestCase("163", P3, Arrays.asList(new Benefit[]{ B1, B2, B5 }), M1, new EstimatedCosts(1050d, 0d, 1000d, 0d, 250d, 1250d)),
				new TestCase("164", P3, Arrays.asList(new Benefit[]{ B2, B3, B5 }), M1, new EstimatedCosts(1000d, 100d, 1000d, 100d, 200d, 1300d)),
				new TestCase("165", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B5 }), M1, new EstimatedCosts(1050d, 100d, 1000d, 100d, 250d, 1350d)),
				new TestCase("166", P3, Arrays.asList(new Benefit[]{ B2, B6 }), M1, new EstimatedCosts(1400d, 400d, 1000d, 400d, 0d, 1400d)),
				new TestCase("167", P3, Arrays.asList(new Benefit[]{ B1, B2, B6 }), M1, new EstimatedCosts(1450d, 400d, 1000d, 400d, 50d, 1450d)),
				new TestCase("168", P3, Arrays.asList(new Benefit[]{ B2, B3, B6 }), M1, new EstimatedCosts(1400d, 500d, 1000d, 500d, 0d, 1500d)),
				new TestCase("169", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B6 }), M1, new EstimatedCosts(1450d, 500d, 1000d, 500d, 50d, 1550d)),
				new TestCase("170", P3, Arrays.asList(new Benefit[]{ B2, B5, B6 }), M1, new EstimatedCosts(1400d, 400d, 1000d, 400d, 200d, 1600d)),
				new TestCase("171", P3, Arrays.asList(new Benefit[]{ B1, B2, B5, B6 }), M1, new EstimatedCosts(1450d, 400d, 1000d, 400d, 250d, 1650d)),
				new TestCase("172", P3, Arrays.asList(new Benefit[]{ B2, B3, B5, B6 }), M1, new EstimatedCosts(1400d, 500d, 1000d, 500d, 200d, 1700d)),
				new TestCase("173", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B5, B6 }), M1, new EstimatedCosts(1450d, 500d, 1000d, 500d, 250d, 1750d)),
				new TestCase("174", P3, Arrays.asList(new Benefit[]{ B2, B4 }), M1, new EstimatedCosts(1000d, 3000d, 1000d, 3000d, 0d, 4000d)),
				new TestCase("175", P3, Arrays.asList(new Benefit[]{ B1, B2, B4 }), M1, new EstimatedCosts(1050d, 3000d, 1000d, 3000d, 50d, 4050d)),
				new TestCase("176", P3, Arrays.asList(new Benefit[]{ B2, B3, B4 }), M1, new EstimatedCosts(1000d, 3100d, 1000d, 3100d, 0d, 4100d)),
				new TestCase("177", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), M1, new EstimatedCosts(1050d, 3100d, 1000d, 3100d, 50d, 4150d)),
				new TestCase("178", P3, Arrays.asList(new Benefit[]{ B2, B4, B5 }), M1, new EstimatedCosts(1000d, 3000d, 1000d, 3000d, 200d, 4200d)),
				new TestCase("179", P3, Arrays.asList(new Benefit[]{ B1, B2, B4, B5 }), M1, new EstimatedCosts(1050d, 3000d, 1000d, 3000d, 250d, 4250d)),
				new TestCase("180", P3, Arrays.asList(new Benefit[]{ B2, B3, B4, B5 }), M1, new EstimatedCosts(1000d, 3100d, 1000d, 3100d, 200d, 4300d)),
				new TestCase("181", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5 }), M1, new EstimatedCosts(1050d, 3100d, 1000d, 3100d, 250d, 4350d)),
				new TestCase("182", P3, Arrays.asList(new Benefit[]{ B2, B4, B6 }), M1, new EstimatedCosts(1400d, 3400d, 1000d, 3400d, 0d, 4400d)),
				new TestCase("183", P3, Arrays.asList(new Benefit[]{ B1, B2, B4, B6 }), M1, new EstimatedCosts(1450d, 3400d, 1000d, 3400d, 50d, 4450d)),
				new TestCase("184", P3, Arrays.asList(new Benefit[]{ B2, B3, B4, B6 }), M1, new EstimatedCosts(1400d, 3500d, 1000d, 3500d, 0d, 4500d)),
				new TestCase("185", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B6 }), M1, new EstimatedCosts(1450d, 3500d, 1000d, 3500d, 50d, 4550d)),
				new TestCase("186", P3, Arrays.asList(new Benefit[]{ B2, B4, B5, B6 }), M1, new EstimatedCosts(1400d, 3400d, 1000d, 3400d, 200d, 4600d)),
				new TestCase("187", P3, Arrays.asList(new Benefit[]{ B1, B2, B4, B5, B6 }), M1, new EstimatedCosts(1450d, 3400d, 1000d, 3400d, 250d, 4650d)),
				new TestCase("188", P3, Arrays.asList(new Benefit[]{ B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(1400d, 3500d, 1000d, 3500d, 200d, 4700d)),
				new TestCase("189", P3, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(1450d, 3500d, 1000d, 3500d, 250d, 4750d)),

				// P4 - NO DEDUCTIBLE, NO OOPMAX
				new TestCase("190", P4, Arrays.asList(new Benefit[]{ B1 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 50d, 50d)),
				new TestCase("191", P4, Arrays.asList(new Benefit[]{ B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 0d, 100d)),
				new TestCase("192", P4, Arrays.asList(new Benefit[]{ B1, B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 50d, 150d)),
				new TestCase("193", P4, Arrays.asList(new Benefit[]{ B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 200d, 200d)),
				new TestCase("194", P4, Arrays.asList(new Benefit[]{ B1, B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 250d, 250d)),
				new TestCase("195", P4, Arrays.asList(new Benefit[]{ B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 200d, 300d)),
				new TestCase("196", P4, Arrays.asList(new Benefit[]{ B1, B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 250d, 350d)),
				new TestCase("197", P4, Arrays.asList(new Benefit[]{ B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 0d, 400d)),
				new TestCase("198", P4, Arrays.asList(new Benefit[]{ B1, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 50d, 450d)),
				new TestCase("199", P4, Arrays.asList(new Benefit[]{ B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 0d, 500d)),
				new TestCase("200", P4, Arrays.asList(new Benefit[]{ B1, B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 50d, 550d)),
				new TestCase("201", P4, Arrays.asList(new Benefit[]{ B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 200d, 600d)),
				new TestCase("202", P4, Arrays.asList(new Benefit[]{ B1, B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 250d, 650d)),
				new TestCase("203", P4, Arrays.asList(new Benefit[]{ B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 200d, 700d)),
				new TestCase("204", P4, Arrays.asList(new Benefit[]{ B1, B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 250d, 750d)),
				new TestCase("205", P4, Arrays.asList(new Benefit[]{ B2 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1000d, 1000d)),
				new TestCase("206", P4, Arrays.asList(new Benefit[]{ B1, B2 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1050d, 1050d)),
				new TestCase("207", P4, Arrays.asList(new Benefit[]{ B2, B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1000d, 1100d)),
				new TestCase("208", P4, Arrays.asList(new Benefit[]{ B1, B2, B3 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1050d, 1150d)),
				new TestCase("209", P4, Arrays.asList(new Benefit[]{ B2, B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1200d, 1200d)),
				new TestCase("210", P4, Arrays.asList(new Benefit[]{ B1, B2, B5 }), M1, new EstimatedCosts(0d, 0d, 0d, 0d, 1250d, 1250d)),
				new TestCase("211", P4, Arrays.asList(new Benefit[]{ B2, B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1200d, 1300d)),
				new TestCase("212", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B5 }), M1, new EstimatedCosts(0d, 100d, 0d, 100d, 1250d, 1350d)),
				new TestCase("213", P4, Arrays.asList(new Benefit[]{ B2, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1000d, 1400d)),
				new TestCase("214", P4, Arrays.asList(new Benefit[]{ B1, B2, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1050d, 1450d)),
				new TestCase("215", P4, Arrays.asList(new Benefit[]{ B2, B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1000d, 1500d)),
				new TestCase("216", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1050d, 1550d)),
				new TestCase("217", P4, Arrays.asList(new Benefit[]{ B2, B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1200d, 1600d)),
				new TestCase("218", P4, Arrays.asList(new Benefit[]{ B1, B2, B5, B6 }), M1, new EstimatedCosts(0d, 400d, 0d, 400d, 1250d, 1650d)),
				new TestCase("219", P4, Arrays.asList(new Benefit[]{ B2, B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1200d, 1700d)),
				new TestCase("220", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B5, B6 }), M1, new EstimatedCosts(0d, 500d, 0d, 500d, 1250d, 1750d)),
				new TestCase("221", P4, Arrays.asList(new Benefit[]{ B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 0d, 3000d)),
				new TestCase("222", P4, Arrays.asList(new Benefit[]{ B1, B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 50d, 3050d)),
				new TestCase("223", P4, Arrays.asList(new Benefit[]{ B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 0d, 3100d)),
				new TestCase("224", P4, Arrays.asList(new Benefit[]{ B1, B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 50d, 3150d)),
				new TestCase("225", P4, Arrays.asList(new Benefit[]{ B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 200d, 3200d)),
				new TestCase("226", P4, Arrays.asList(new Benefit[]{ B1, B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 250d, 3250d)),
				new TestCase("227", P4, Arrays.asList(new Benefit[]{ B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 200d, 3300d)),
				new TestCase("228", P4, Arrays.asList(new Benefit[]{ B1, B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 250d, 3350d)),
				new TestCase("229", P4, Arrays.asList(new Benefit[]{ B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 0d, 3400d)),
				new TestCase("230", P4, Arrays.asList(new Benefit[]{ B1, B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 50d, 3450d)),
				new TestCase("231", P4, Arrays.asList(new Benefit[]{ B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 0d, 3500d)),
				new TestCase("232", P4, Arrays.asList(new Benefit[]{ B1, B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 50d, 3550d)),
				new TestCase("233", P4, Arrays.asList(new Benefit[]{ B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 200d, 3600d)),
				new TestCase("234", P4, Arrays.asList(new Benefit[]{ B1, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 250d, 3650d)),
				new TestCase("235", P4, Arrays.asList(new Benefit[]{ B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 200d, 3700d)),
				new TestCase("236", P4, Arrays.asList(new Benefit[]{ B1, B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 250d, 3750d)),
				new TestCase("237", P4, Arrays.asList(new Benefit[]{ B2, B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1000d, 4000d)),
				new TestCase("238", P4, Arrays.asList(new Benefit[]{ B1, B2, B4 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1050d, 4050d)),
				new TestCase("239", P4, Arrays.asList(new Benefit[]{ B2, B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 1000d, 4100d)),
				new TestCase("240", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 1050d, 4150d)),
				new TestCase("241", P4, Arrays.asList(new Benefit[]{ B2, B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1200d, 4200d)),
				new TestCase("242", P4, Arrays.asList(new Benefit[]{ B1, B2, B4, B5 }), M1, new EstimatedCosts(0d, 3000d, 0d, 3000d, 1250d, 4250d)),
				new TestCase("243", P4, Arrays.asList(new Benefit[]{ B2, B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 1200d, 4300d)),
				new TestCase("244", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5 }), M1, new EstimatedCosts(0d, 3100d, 0d, 3100d, 1250d, 4350d)),
				new TestCase("245", P4, Arrays.asList(new Benefit[]{ B2, B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 1000d, 4400d)),
				new TestCase("246", P4, Arrays.asList(new Benefit[]{ B1, B2, B4, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 1050d, 4450d)),
				new TestCase("247", P4, Arrays.asList(new Benefit[]{ B2, B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1000d, 4500d)),
				new TestCase("248", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1050d, 4550d)),
				new TestCase("249", P4, Arrays.asList(new Benefit[]{ B2, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 1200d, 4600d)),
				new TestCase("250", P4, Arrays.asList(new Benefit[]{ B1, B2, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3400d, 0d, 3400d, 1250d, 4650d)),
				new TestCase("251", P4, Arrays.asList(new Benefit[]{ B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1200d, 4700d)),
				new TestCase("252", P4, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5, B6 }), M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1250d, 4750d))
		}));
    
    // P1 - MULTIPLE MEMBERS
		Map<Member, EstimatedCosts> expectedCosts = new HashMap<Member, EstimatedCosts>();
		expectedCosts.put(M1, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d));
    expectedCosts.put(M2, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d));
    testCases.add(new TestCase("253", P1, Arrays.asList(new Benefit[]{ B1, B6 }), Arrays.asList(new Member[] { M1, M2 }), expectedCosts)); 

    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d));
    expectedCosts.put(M2, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d));
    expectedCosts.put(M3, new EstimatedCosts(450d, 0d, 450d, 0d, 0d, 450d));
    testCases.add(new TestCase("254", P1, Arrays.asList(new Benefit[]{ B1, B6 }), Arrays.asList(new Member[] { M1, M2 }), expectedCosts)); 

    // P5 - FAMILY DEDUCTIBLE NOT MET, FAMILY OOP MAX NOT MET
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(1050d, 0d, 1050d, 0d, 0d, 1050d));
    expectedCosts.put(M2, new EstimatedCosts(1050d, 0d, 1050d, 0d, 0d, 1050d));
    EstimatedCosts totalEstimatedCosts = new EstimatedCosts(2100d, 0d, 2100d, 0d, 0d, 2100d);
    testCases.add(new TestCase("255", P5, Arrays.asList(new Benefit[]{ B1, B2 }), Arrays.asList(new Member[] { M1, M2 }), expectedCosts, totalEstimatedCosts)); 

    // P5 - FAMILY DEDUCTIBLE MET, FAMILY OOP MAX NOT MET
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(1050d, 0d, 1000d, 0d, 50d, 1050d));
    expectedCosts.put(M2, new EstimatedCosts(1050d, 0d, 1000d, 0d, 50d, 1050d));
    expectedCosts.put(M3, new EstimatedCosts(1050d, 0d, 1000d, 0d, 50d, 1050d));
    totalEstimatedCosts = new EstimatedCosts(3150d, 0d, 3000d, 0d, 150d, 3150d);
    testCases.add(new TestCase("256", P5, Arrays.asList(new Benefit[]{ B1, B2 }), Arrays.asList(new Member[] { M1, M2, M3 }), expectedCosts, totalEstimatedCosts)); 

    // P5 - FAMILY DEDUCTIBLE NOT MET, FAMILY OOP MAX MET
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 0d, 3000d));
    expectedCosts.put(M2, new EstimatedCosts(0d, 3100d, 0d, 3000d, 0d, 3000d));
    expectedCosts.put(M3, new EstimatedCosts(0d, 3100d, 0d, 3000d, 0d, 3000d));
    totalEstimatedCosts = new EstimatedCosts(0d, 9300d, 0d, 9000d, 0d, 9000d);
    testCases.add(new TestCase("257", P5, Arrays.asList(new Benefit[]{ B3, B4 }), Arrays.asList(new Member[] { M1, M2, M3 }), expectedCosts, totalEstimatedCosts)); 

    // P5 - FAMILY DEDUCTIBLE MET, FAMILY OOP MAX MET
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(1050d, 3100d, 1000d, 3000d, 50d, 4050d));
    expectedCosts.put(M2, new EstimatedCosts(1050d, 3100d, 1000d, 3000d, 50d, 4050d));
    expectedCosts.put(M3, new EstimatedCosts(1050d, 3100d, 1000d, 3000d, 50d, 4050d));
    totalEstimatedCosts = new EstimatedCosts(3150d, 9300d, 3000d, 9000d, 150d, 12150d);
    testCases.add(new TestCase("258", P5, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), Arrays.asList(new Member[] { M1, M2, M3 }), expectedCosts, totalEstimatedCosts)); 

    // P6 - NO FAMILY DEDUCTIBLE, FAMILY OOP MAX MET
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1050d, 4050d));
    expectedCosts.put(M2, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1050d, 4050d));
    expectedCosts.put(M3, new EstimatedCosts(0d, 3100d, 0d, 3000d, 1050d, 4050d));
    totalEstimatedCosts = new EstimatedCosts(0d, 9300d, 0d, 9000d, 3150d, 12150d);
    testCases.add(new TestCase("259", P6, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), Arrays.asList(new Member[] { M1, M2, M3 }), expectedCosts, totalEstimatedCosts)); 

    // P7 - FAMILY DEDUCTIBLE MET, NO FAMILY OOP MAX
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(1050d, 3100d, 1000d, 3100d, 50d, 4150d));
    expectedCosts.put(M2, new EstimatedCosts(1050d, 3100d, 1000d, 3100d, 50d, 4150d));
    expectedCosts.put(M3, new EstimatedCosts(1050d, 3100d, 1000d, 3100d, 50d, 4150d));
    totalEstimatedCosts = new EstimatedCosts(3150d, 9300d, 3000d, 9300d, 150d, 12450d);
    testCases.add(new TestCase("260", P7, Arrays.asList(new Benefit[]{ B1, B2, B3, B4 }), Arrays.asList(new Member[] { M1, M2, M3 }), expectedCosts, totalEstimatedCosts)); 

    // P8 - NO FAMILY DEDUCTIBLE, NO FAMILY OOP MAX
    expectedCosts = new HashMap<Member, EstimatedCosts>();
    expectedCosts.put(M1, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1250d, 4750d));
    expectedCosts.put(M2, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1250d, 4750d));
    expectedCosts.put(M3, new EstimatedCosts(0d, 3500d, 0d, 3500d, 1250d, 4750d));
    totalEstimatedCosts = new EstimatedCosts(0d, 10500d, 0d, 10500d, 3750d, 14250d);
    testCases.add(new TestCase("261", P8, Arrays.asList(new Benefit[]{ B1, B2, B3, B4, B5, B6 }), Arrays.asList(new Member[] { M1, M2, M3 }), expectedCosts, totalEstimatedCosts)); 
    
		for (TestCase testCase : testCases) {
			if (Integer.parseInt(testCase.id) > 0) {
				runTestCase(testCase);
				run.add(testCase);
			}
		}
		
		NumberFormat format = DecimalFormat.getInstance();
		format.setMaximumFractionDigits(2);
		System.out.println("*** Test Results - " + format.format(passed.size()*100/(double)run.size()) + "% Successful ***");
		System.out.println("\tTest Cases PASSED " + passed.size());
		String failedMessage = "\tTest Cases FAILED " + failed.size();
		if (failed.isEmpty()) {
			System.out.println(failedMessage);
		}
		else {
			System.err.println(failedMessage + ", details:\n");
		}
		for (TestCase testCase : failed) {
			StringBuffer output = new StringBuffer();
			output.append(String.valueOf(testCase) + "... ");
			output.append("\nMember Costs:\n\t" + EstimatedCosts.generateHeader(testCase.members.size()) + "\n\texpected " + String.valueOf(testCase.expectedCosts) + "\n\t  actual " + String.valueOf(testCase.results.memberCosts));
			if (null != testCase.totalExpectedCosts) {
			  output.append("\nFamily Costs:\n\t" + EstimatedCosts.generateHeader(0) + "\n\texpected " + String.valueOf(testCase.totalExpectedCosts) + "\n\t  actual " + String.valueOf(testCase.results.familyCosts));
			}
			System.err.println(output + "\n");
		}
		System.out.println("*** Test Finished ***");
	}
	
	private static List<TestCase> run = new ArrayList<TestCase>();
	private static List<TestCase> passed = new ArrayList<TestCase>();
	private static List<TestCase> failed = new ArrayList<TestCase>();
	
	private void runTestCase(TestCase testCase) {
		try {
		  testCase.results = new CostCalculator().calculateEstimatedCostsInRulesTerms(testCase.members, testCase.benefits, testCase.planBenefit);
			Assert.assertEquals(testCase.members.size(), testCase.results.memberCosts.size());
			for (Member member : testCase.members) {
  			EstimatedCosts actualCosts = testCase.results.memberCosts.get(member);
  			EstimatedCosts expectedCosts = testCase.expectedCosts.get(member);
  			assertTestCaseEquals(actualCosts, expectedCosts);
			}
			if (null != testCase.totalExpectedCosts) {
			  assertTestCaseEquals(testCase.results.familyCosts, testCase.totalExpectedCosts);
			}
			passed.add(testCase);
		}
		catch (AssertionFailedError e) {
			failed.add(testCase);
		}
	}

  public void assertTestCaseEquals(EstimatedCosts actualCosts, EstimatedCosts expectedCosts)
  {
    Assert.assertEquals(expectedCosts.deductibleCosts, actualCosts.deductibleCosts);
    Assert.assertEquals(expectedCosts.oopCosts, actualCosts.oopCosts);
    Assert.assertEquals(expectedCosts.deductibleAdjustedCosts, actualCosts.deductibleAdjustedCosts);
    Assert.assertEquals(expectedCosts.oopAdjustedCosts, actualCosts.oopAdjustedCosts);
    Assert.assertEquals(expectedCosts.extraCosts, actualCosts.extraCosts);
    Assert.assertEquals(expectedCosts.totalCosts, actualCosts.totalCosts);
  }
	
	class TestCase {
		String id;
		List<Benefit> benefits;
		PlanBenefit planBenefit;
		List<Member> members;
		Map<Member, EstimatedCosts> expectedCosts;
		EstimatedCosts totalExpectedCosts;
		CostCalculatorResult results;
		
		TestCase(String id, PlanBenefit planBenefit, List<Benefit> benefits, Member member, EstimatedCosts expectedCosts) {
			this.id = id;
			this.planBenefit = planBenefit;
			this.benefits = benefits;
			this.members = new ArrayList<Member>();
			members.add(member);
			this.expectedCosts = new TreeMap<Member, EstimatedCosts>();
			this.expectedCosts.put(member, expectedCosts);
		}

		TestCase(String id, PlanBenefit planBenefit, List<Benefit> benefits, List<Member> members, Map<Member, EstimatedCosts> expectedCosts) {
		  this(id, planBenefit, benefits, members, expectedCosts, null);
		}
		
    TestCase(String id, PlanBenefit planBenefit, List<Benefit> benefits, List<Member> members, Map<Member, EstimatedCosts> expectedCosts, EstimatedCosts totalExpectedCosts) {
      this.id = id;
      this.planBenefit = planBenefit;
      this.benefits = benefits;
      this.members = members;
      this.expectedCosts = new TreeMap<Member, EstimatedCosts>(expectedCosts);
      this.totalExpectedCosts = totalExpectedCosts;
    }
		
		@Override
		public String toString() {
			return "Test Case " + id + " - <Plan " + planBenefit.id + ", Benefits " + benefits + ", Members " + members + ">";
		}
	}
}
