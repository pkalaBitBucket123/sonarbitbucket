package com.connecture.shopping.process.service.data.converter;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.connecture.shopping.process.service.data.AnswerData;
import com.connecture.shopping.process.service.data.FilterData;
import com.connecture.shopping.process.service.data.FilterOptionData;
import com.connecture.shopping.process.service.data.PlanAdvisorData;
import com.connecture.shopping.process.service.data.PlanAdvisorDataConstants;
import com.connecture.shopping.process.service.data.QuestionData;
import com.connecture.shopping.process.service.data.QuestionnaireData;
import com.connecture.shopping.process.service.data.WorkflowData;

public class PlanAdvisorDataConverterTest
{

  @Test
  public void testConvertData() throws Exception
  {
    PlanAdvisorDataConverter converter = new PlanAdvisorDataConverter();
    JSONObject object = new JSONObject(
        "{ " +
    		"  questionSetHash : \"#!@$\", " +
    		"  workflows : [], " +
    		"  questionCategories : [], " +
    		"  questions : [], " +
    		"  filters : [], " +
    		"  filterEvaluations : [], " +
    		"  planEvaluations : []" +
    		"}");
    testVersionId(converter, object);
    
    testWorkFlow(converter, object);
    testQuestionnaire(converter, object);
    testQuestion(converter, object);
    testFilters(converter, object);
    
    JSONArray filterEvaluationsArray = object.getJSONArray("filterEvaluations");
    JSONObject filterEvaluation = new JSONObject(
      "{" +
      "  filterRefId = \"F1\"," +
      "  options : []" +
      "}");
    filterEvaluationsArray.put(filterEvaluation);
    PlanAdvisorData resultData = converter.convertData(object.toString());
    Assert.assertEquals(1, resultData.getFilterEvaluationMap().size());
  }

  private void testVersionId(PlanAdvisorDataConverter converter, JSONObject object)
    throws Exception
  {
    PlanAdvisorData resultData = converter.convertData(object.toString());
    
    Assert.assertEquals("#!@$", resultData.getVersionId());
    Assert.assertTrue(resultData.getFilterEvaluationMap().isEmpty());
    Assert.assertTrue(resultData.getQuestionnaires().isEmpty());
    Assert.assertTrue(resultData.getQuestions().isEmpty());
    Assert.assertTrue(resultData.getFilters().isEmpty());
    Assert.assertTrue(resultData.getPlanEvaluations().isEmpty());
    Assert.assertTrue(resultData.getWorkflows().isEmpty());
  }

  private void testWorkFlow(PlanAdvisorDataConverter converter, JSONObject object)
    throws JSONException, Exception
  {
    JSONArray workflowArray = object.getJSONArray("workflows");
    JSONObject workflowObject = new JSONObject(
      "{" +
      "  workflowId : \"W1\", " +
      "  title : \"Workflow\", " +
      "  description : \"It's a workflow\"," +
      "  default : false, " +
      "  questionRefIds : [ \"Q1\" ]" +
      "}");
    workflowArray.put(workflowObject);
    PlanAdvisorData resultData = converter.convertData(object.toString());
    
    Assert.assertEquals(1, resultData.getWorkflows().size());
    WorkflowData workflowData = resultData.getWorkflows().get(0);
    Assert.assertEquals("W1", workflowData.getWorkflowRefId());
    Assert.assertEquals("Workflow", workflowData.getTitle());
    Assert.assertEquals("It's a workflow", workflowData.getInfoBlock());
    Assert.assertNull(workflowData.getClassOverride());
    Assert.assertFalse(workflowData.getDefaultWorkflow());
    List<String> questionRefIds = workflowData.getQuestionRefIds();
    Assert.assertEquals(1, questionRefIds.size());
    Assert.assertEquals("Q1", questionRefIds.get(0));
    
    workflowObject.put("classOverride", "null");
    resultData = converter.convertData(object.toString());
    workflowData = resultData.getWorkflows().get(0);
    Assert.assertNull(workflowData.getClassOverride());

    workflowObject.put("classOverride", "thing");
    resultData = converter.convertData(object.toString());
    workflowData = resultData.getWorkflows().get(0);
    Assert.assertEquals("thing", workflowData.getClassOverride());
  }

  private void testQuestionnaire(PlanAdvisorDataConverter converter, JSONObject object)
    throws JSONException, Exception
  {
    JSONArray questionnairesArray = object.getJSONArray("questionCategories");
    JSONObject questionnaireObject = new JSONObject(
      "{" +
      "  categoryId : \"QN1\"," +
      "  title : \"Category\"," +
      "  description : \"It's a category\"" +
      "}");
    questionnairesArray.put(questionnaireObject);
    PlanAdvisorData resultData = converter.convertData(object.toString());
    Assert.assertEquals(1, resultData.getQuestionnaires().size());
    QuestionnaireData questionnaireData = resultData.getQuestionnaires().get(0);
    Assert.assertEquals("QN1", questionnaireData.getQuestionnaireRefId());
    Assert.assertEquals("Category", questionnaireData.getTitle());
    Assert.assertEquals("It's a category", questionnaireData.getHeader());
    
    questionnaireObject.put("classOverride", "null");
    resultData = converter.convertData(object.toString());
    questionnaireData = resultData.getQuestionnaires().get(0);
    Assert.assertNull(questionnaireData.getClassOverride());

    questionnaireObject.put("classOverride", "thing");
    resultData = converter.convertData(object.toString());
    questionnaireData = resultData.getQuestionnaires().get(0);
    Assert.assertEquals("thing", questionnaireData.getClassOverride());
  }

  private void testQuestion(PlanAdvisorDataConverter converter, JSONObject object)
    throws JSONException, Exception
  {
    JSONArray questionsArray = object.getJSONArray("questions");
    JSONObject questionObject = new JSONObject(
      "{" +
      "  questionId : \"Q1\"," +
      "  categoryRefId : \"QN1\"" +
      "}");
    questionsArray.put(questionObject);
    PlanAdvisorData resultData = converter.convertData(object.toString());
    Assert.assertEquals(1, resultData.getQuestions().size());
    QuestionData questionData = resultData.getQuestions().get(0);
    Assert.assertEquals("Q1", questionData.getQuestionRefId());
    Assert.assertEquals("QN1", questionData.getQuestionnaireRefId());
    Assert.assertNull(questionData.getRequired());
    Assert.assertNull(questionData.getText());
    Assert.assertNull(questionData.getEducationalContent());
    Assert.assertNull(questionData.getTitle());
    Assert.assertNull(questionData.getHelpText());
    Assert.assertNull(questionData.getHelpType());
    Assert.assertNull(questionData.getDisplayType());
    Assert.assertNull(questionData.getWidgetId());
    Assert.assertNull(questionData.getMemberLevelQuestion());
    Assert.assertNull(questionData.getFilterRefId());
    Assert.assertNull(questionData.getDataTagKey());
    Assert.assertNull(questionData.getEvaluationType());
    Assert.assertNull(questionData.getMinimumAnswers());
    Assert.assertNull(questionData.getMaximumAnswers());
    Assert.assertNull(questionData.getClassOverride());
    Assert.assertNull(questionData.getWeight());
    Assert.assertTrue(questionData.getAnswers().isEmpty());
    
    questionObject = new JSONObject(
      "{" +
      "  questionId : \"Q1\"," +
      "  categoryRefId : \"QN1\"," +
      "  required : true," +
      "  text : \"text\"," +
      "  educationalBlock : \"educationalBlock\"," +
      "  navText : \"navText\"," +
      "  helpText : \"helpText\"," +
      "  helpType : \"helpType\"," +
      "  displayType : \"displayType\"," +
      "  widgetId : \"W1\"," +
      "  memberLevelQuestion : true," +
      "  filterRefId : \"F1\"," +
      "  dataTagKey : \"D1\"," +
      "  evaluationType : \"evaluationType\"," +
      "  minimumAnswers : 1," +
      "  maximumAnswers : 3," +
      "  classOverride : \"null\"," +
      "  weight : \"100\"" +
      "}");
    questionsArray.put(0, questionObject);
    resultData = converter.convertData(object.toString());
    Assert.assertEquals(1, resultData.getQuestions().size());
    questionData = resultData.getQuestions().get(0);
    Assert.assertTrue(questionData.getRequired());
    Assert.assertEquals("text", questionData.getText());
    Assert.assertEquals("educationalBlock", questionData.getEducationalContent());
    Assert.assertEquals("navText", questionData.getTitle());
    Assert.assertEquals("helpText", questionData.getHelpText());
    Assert.assertEquals("helpType", questionData.getHelpType());
    Assert.assertEquals("displayType", questionData.getDisplayType());
    Assert.assertEquals("W1", questionData.getWidgetId());
    Assert.assertTrue(questionData.getMemberLevelQuestion());
    Assert.assertEquals("F1", questionData.getFilterRefId());
    Assert.assertEquals("D1", questionData.getDataTagKey());
    Assert.assertEquals("evaluationType", questionData.getEvaluationType());
    Assert.assertEquals(1, questionData.getMinimumAnswers().intValue());
    Assert.assertEquals(3, questionData.getMaximumAnswers().intValue());
    Assert.assertNull(questionData.getClassOverride());
    Assert.assertEquals("100", questionData.getWeight());
    Assert.assertTrue(questionData.getAnswers().isEmpty());

    questionObject.put("classOverride", "thing");
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    Assert.assertEquals("thing", questionData.getClassOverride());
    
    testQuestionAnswer(converter, object, questionObject);
  }

  private void testQuestionAnswer(
    PlanAdvisorDataConverter converter,
    JSONObject object,
    JSONObject questionObject) throws JSONException, Exception
  {
    PlanAdvisorData resultData;
    QuestionData questionData;
    JSONArray answersArray = new JSONArray();
    JSONObject answerObject = new JSONObject(
      "{" +
      "  answerId : \"A1\"," +
      "}");
    answersArray.put(answerObject);
    questionObject.put("answers", answersArray);
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    Assert.assertEquals(1, questionData.getAnswers().size());
    AnswerData answerData = questionData.getAnswers().get(0);
    Assert.assertEquals("A1", answerData.getAnswerId());
    Assert.assertNull(answerData.getText());
    Assert.assertNull(answerData.getSummaryText());
    Assert.assertNull(answerData.getHelpText());
    Assert.assertNull(answerData.getHelpType());
    Assert.assertNull(answerData.getDataTagValue());
    Assert.assertNull(answerData.getDataTagOperator());
    Assert.assertNull(answerData.getFilterOptionRefId());
    Assert.assertNull(answerData.getClassOverride());
    
    answerObject = new JSONObject(
      "{" +
      "  answerId : \"A1\"," +
      "  text : \"text\"," +
      "  summary : \"summary\"," +
      "  helpText : \"helpText\"," +
      "  helpType : \"helpType\"," +
      "  dataTagValue : \"D1\"," +
      "  dataTagOperator : \"" + PlanAdvisorDataConstants.Operators.EQUALS + "\"," +
      "  filterOptionRefId : \"F1\"," +
      "  classOverride : \"null\"" +
      "}");
    answersArray.put(0, answerObject);
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    answerData = questionData.getAnswers().get(0);
    Assert.assertEquals("text", answerData.getText());
    Assert.assertEquals("summary", answerData.getSummaryText());
    Assert.assertEquals("helpText", answerData.getHelpText());
    Assert.assertEquals("helpType", answerData.getHelpType());
    Assert.assertEquals("D1", answerData.getDataTagValue());
    Assert.assertEquals("=", answerData.getDataTagOperator());
    Assert.assertEquals("F1", answerData.getFilterOptionRefId());
    Assert.assertNull(answerData.getClassOverride());

    answerObject.put("classOverride", "thing");
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    answerData = questionData.getAnswers().get(0);
    Assert.assertEquals("thing", answerData.getClassOverride());

    answerObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.LESS_THAN);
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    answerData = questionData.getAnswers().get(0);
    Assert.assertEquals("<", answerData.getDataTagOperator());

    answerObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.GREATER_THAN);
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    answerData = questionData.getAnswers().get(0);
    Assert.assertEquals(">", answerData.getDataTagOperator());

    answerObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.IN_BETWEEN);
    resultData = converter.convertData(object.toString());
    questionData = resultData.getQuestions().get(0);
    answerData = questionData.getAnswers().get(0);
    Assert.assertEquals("between", answerData.getDataTagOperator());
  }

  private void testFilters(PlanAdvisorDataConverter converter, JSONObject object)
    throws JSONException, Exception
  {
    JSONArray filtersArray = object.getJSONArray("filters");
    JSONObject filterObject = new JSONObject(
      "{" +
      "  filterId : \"F1\"," +
      "  options : []" +
      "}");
    filtersArray.put(filterObject);
    PlanAdvisorData resultData = converter.convertData(object.toString());
    Assert.assertEquals(1, resultData.getFilters().size());
    FilterData filterData = resultData.getFilters().get(0);
    Assert.assertEquals("F1", filterData.getFilterRefId());
    Assert.assertEquals("", filterData.getTitle());
    Assert.assertEquals("", filterData.getEvaluationType());
    Assert.assertEquals("", filterData.getDisplayType());
    Assert.assertNull(filterData.getHelpText());
    Assert.assertNull(filterData.getHelpType());
    Assert.assertNull(filterData.getClassOverride());
    Assert.assertEquals("", filterData.getDataTagKey());
    Assert.assertEquals(0, filterData.getOptions().size());
    
    filterObject = new JSONObject(
      "{" +
      "  filterId : \"F1\"," +
      "  title : \"title\"," +
      "  evaluationType : \"evaluationType\"," +
      "  displayType : \"displayType\"," +
      "  helpText : \"helpText\"," +
      "  helpType : \"helpType\"," +
      "  classOverride : \"null\"," +
      "  dataTagKey : \"D1\"," +
      "  options : []" +
      "}");
    filtersArray.put(0, filterObject);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    Assert.assertEquals("title", filterData.getTitle());
    Assert.assertEquals("evaluationType", filterData.getEvaluationType());
    Assert.assertEquals("", filterData.getDisplayType());
    Assert.assertEquals("helpText", filterData.getHelpText());
    Assert.assertEquals("helpType", filterData.getHelpType());
    Assert.assertNull(filterData.getClassOverride());
    Assert.assertEquals("D1", filterData.getDataTagKey());
    
    filterObject.put("classOverride", "Test");
    filterObject.put("displayType", PlanAdvisorDataConstants.DisplayType.SLIDER);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    Assert.assertEquals("Test", filterData.getClassOverride());
    Assert.assertEquals("slider", filterData.getDisplayType());
    
    filterObject.put("displayType", PlanAdvisorDataConstants.DisplayType.CHECKBOX);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    Assert.assertEquals("checkbox", filterData.getDisplayType());
    
    filterObject.put("displayType", PlanAdvisorDataConstants.DisplayType.RADIO_BUTTON);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    Assert.assertEquals("radio", filterData.getDisplayType());
    
    filterObject.put("displayType", PlanAdvisorDataConstants.DisplayType.SELECT);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    Assert.assertEquals("select", filterData.getDisplayType());
    
    testFilterOptions(converter, object, filterObject);
  }

  private void testFilterOptions(
    PlanAdvisorDataConverter converter,
    JSONObject object,
    JSONObject filterObject) throws JSONException, Exception
  {
    JSONArray filterOptionArray = filterObject.getJSONArray("options");
    JSONObject filterOptionObject = new JSONObject(
      "{" +
      "  optionId : \"O1\"" +
      "}");
    filterOptionArray.put(filterOptionObject);
    PlanAdvisorData resultData = converter.convertData(object.toString());
    FilterData filterData = resultData.getFilters().get(0);
    Assert.assertEquals(1, filterData.getOptions().size());
    FilterOptionData filterOption = filterData.getOptions().get(0);
    Assert.assertEquals("O1", filterOption.getOptionRefId());
    Assert.assertEquals("", filterOption.getText());
    Assert.assertNull(filterOption.getHelpText());
    Assert.assertNull(filterOption.getHelpType());
    Assert.assertNull(filterOption.getClassOverride());
    Assert.assertEquals("", filterOption.getDataTagValue());
    Assert.assertEquals("", filterOption.getDataTagOperator());
    
    filterOptionObject.put("classOverride", "null");
    filterOptionObject.put("dataTagValue", "_");
    filterOptionObject.put("dataTagOperator", "dataTagOperator");
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    filterOption = filterData.getOptions().get(0);
    Assert.assertNull(filterOption.getClassOverride());
    Assert.assertEquals("", filterOption.getDataTagValue());
    Assert.assertEquals("", filterOption.getDataTagOperator());

    filterOptionObject.put("text", "text");
    filterOptionObject.put("helpText", "helpText");
    filterOptionObject.put("helpType", "helpType");
    filterOptionObject.put("classOverride", "classOverride");
    filterOptionObject.put("dataTagValue", "V1");
    filterOptionObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.LESS_THAN);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    filterOption = filterData.getOptions().get(0);
    Assert.assertEquals("text", filterOption.getText());
    Assert.assertEquals("helpText", filterOption.getHelpText());
    Assert.assertEquals("helpType", filterOption.getHelpType());
    Assert.assertEquals("classOverride", filterOption.getClassOverride());
    Assert.assertEquals("V1", filterOption.getDataTagValue());
    Assert.assertEquals("<", filterOption.getDataTagOperator());
    
    filterOptionObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.GREATER_THAN);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    filterOption = filterData.getOptions().get(0);
    Assert.assertEquals(">", filterOption.getDataTagOperator());
    
    filterOptionObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.IN_BETWEEN);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    filterOption = filterData.getOptions().get(0);
    Assert.assertEquals("between", filterOption.getDataTagOperator());
    
    filterOptionObject.put("dataTagOperator", PlanAdvisorDataConstants.Operators.EQUALS);
    resultData = converter.convertData(object.toString());
    filterData = resultData.getFilters().get(0);
    filterOption = filterData.getOptions().get(0);
    Assert.assertEquals("=", filterOption.getDataTagOperator());
  }
}
