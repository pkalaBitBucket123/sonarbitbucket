package com.connecture.shopping.process.camel.request;

import org.apache.camel.CamelExecutionException;
import org.apache.camel.ProducerTemplate;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.IntegrationException;
import com.connecture.shopping.process.camel.request.BaseCamelRequest;

public class BaseCamelRequestTest
{
  @Test
  public void testGetRequestHeaders()
  {
    MockBaseCamelRequest request = new MockBaseCamelRequest();
    Assert.assertTrue(request.getRequestHeaders().isEmpty());
  }
  
  @Test
  public void testConvertData() throws Exception
  {
    MockBaseCamelRequest request = new MockBaseCamelRequest();
    request.convertData("Monkey");
  }
  
  @Test
  public void testSubmitRequest() throws Exception
  {
    final ProducerTemplate mockProducerTemplate = Mockito.mock(ProducerTemplate.class);
    Object expectedResult = "Test";
    Mockito.when(mockProducerTemplate.requestBodyAndHeaders(Mockito.anyObject(), Mockito.anyMapOf(String.class, Object.class))).thenReturn(expectedResult);
    
    BaseCamelRequest<String> request = new BaseCamelRequest<String>()
    {
      @Override
      public ProducerTemplate getProducer()
      {
        return mockProducerTemplate;
      }

      @Override
      public Object getRequestBody()
      {
        return null;
      }
    };
    
    String result = request.submitRequest();
    Assert.assertEquals(expectedResult, result);

    final ProducerTemplate mockProducerTemplate2 = Mockito.mock(ProducerTemplate.class);
    request = new BaseCamelRequest<String>()
    {
      @Override
      public ProducerTemplate getProducer()
      {
        return mockProducerTemplate2;
      }

      @Override
      public Object getRequestBody()
      {
        return null;
      }
    };
    Exception cause = new Exception("Test");
    CamelExecutionException ce = new CamelExecutionException(null, null, cause);
    Mockito.when(mockProducerTemplate2.requestBodyAndHeaders(Mockito.anyObject(), Mockito.anyMapOf(String.class, Object.class))).thenThrow(ce);

    try
    {
      request.submitRequest();
    }
    catch (IntegrationException e)
    {
      // This is expected
    }
  }
  
  @Test
  public void testHandleException()
  {
    MockBaseCamelRequest request = new MockBaseCamelRequest();
    Exception cause = new Exception("Test");
    CamelExecutionException ce = new CamelExecutionException(null, null, cause);
    try
    {
      request.handleCamelException(ce);
    }
    catch (IntegrationException e)
    {
      Assert.assertEquals(cause, e.getCause());
    }
  }
  
  class MockBaseCamelRequest extends BaseCamelRequest<String>
  {
    @Override
    public ProducerTemplate getProducer()
    {
      return null;
    }

    @Override
    public Object getRequestBody()
    {
      return null;
    }
  }
}
