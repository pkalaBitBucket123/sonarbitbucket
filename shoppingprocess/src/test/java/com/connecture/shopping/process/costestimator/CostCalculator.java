package com.connecture.shopping.process.costestimator;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

public class CostCalculator {
	public CostCalculatorResult calculateEstimatedCostsInRulesTerms(
    List<Member> members, List<Benefit> benefits, PlanBenefit planBenefit) {
    CostCalculatorResult result = new CostCalculatorResult();
    
    // Holds the costs for the entire family
    EstimatedCosts familyCosts = result.familyCosts;
    // This is used when a plan has a Family Deductible or Family OOP Max
    
    double individualDeductible = planBenefit.individualDeductible;
    double familyDeductible = planBenefit.familyDeductible;
    double individualOopMax = planBenefit.individualOOPMax;
    double familyOopMax = planBenefit.familyOOPMax;
    
    double familyDeductibleCosts = 0;
    double familyDeductibleOnlyCosts = 0;
    double familyDeductibleAdjustedCosts = 0;
    double familyOOPCosts = 0;
    double familyOOPAdjustedCosts = 0;
    double familyExtraCosts = 0;
    
    int memberCount = members.size();

    /******************************\
    | START OF RULE IMPLEMENTATION |
    \******************************/

    for (Member member : members) {
      EstimatedCosts memberCosts = new EstimatedCosts();
      result.memberCosts.put(member, memberCosts);
      
      double deductibleCosts = 0;
      double deductibleOnlyCosts = 0;
      double deductibleAdjustedCosts = 0;
      double oOPCosts = 0;
      double oOPAdjustedCosts = 0;
      double extraCosts = 0;
      double totalCosts = 0;
      
      for(Benefit benefit : benefits) {
        double benefitCost = benefit.value;
        boolean applyToDeductible = benefit.applyToDeductible;
        boolean applyToMaxOutOfPocket = benefit.applyToOOPMax;
        
        // The remainder of the benefit to be applied
        double benefitRemainder = benefitCost;
        // This value may be adjusted based on how it can be applied to the deductible
        //   or OOP costs
        
        // Only applies to Deductible if there is a deductible AND the benefit says it does
        boolean appliesToDeductible = individualDeductible > 0 && applyToDeductible;
        // This is used later when applying remaining benefit value to the OOP max or extra
        //   costs (costs passed on directly to the Member)
        
        // Only applies to the Family Deductible if the plan has a Family Deductible AND
        //   the benefit says it does
        boolean appliesToFamilyDeductible = familyDeductible > 0 && applyToDeductible && memberCount > 1;
        
        if (appliesToDeductible || appliesToFamilyDeductible) {
          // Keep track of all deductible applicable benefit values
          deductibleCosts += benefitCost;
  
          // If [ the benefit doesn't apply to the OOP Max ] Then
          if (!applyToMaxOutOfPocket) {
            // Keep track of costs that do not go to the OOP Costs
            deductibleOnlyCosts += benefitCost;
            // This may be used later if we need to adjust deductible costs
            //   down because of a Family Deductible
          }
          
          // If [ the total deductibleCosts exceeds the plan deductible
          //   OR the benefit applies to the Family Deductible ] Then
          if (deductibleCosts > individualDeductible || appliesToFamilyDeductible) {
            // If [ the adjusted deductible Costs (costs not to exceed the deductible) are below the plan deductible
            //   OR the benefit applies to the Family Deductible ] Then
            if (deductibleAdjustedCosts < individualDeductible || appliesToFamilyDeductible) {
              // If [ the benefit doesn't apply to the Family Deductible ] Then
              if (!appliesToFamilyDeductible) {
                // Set the remainder of the benefit to be applied to the total deductible costs
                //    less the deductible, this will be used to keep the adjusted deductible
                //    costs within the plan deductible (see below)
                benefitRemainder = deductibleCosts - individualDeductible;
              }
              else {
                // All of the benefit value is applied, though it may be adjusted later
                benefitRemainder = 0d;
              }
            }
          }
          else {
            // All of the benefit value is applied
            benefitRemainder = 0d;
          }
          
          // If [ the adjusted deductible Costs (costs not to exceed the deductible) are below the plan deductible 
          //   OR the benefit applies to the Family Deductible ] Then
          if (deductibleAdjustedCosts < individualDeductible || appliesToFamilyDeductible) {
            // Add the difference between the benefit value and the remainder of the benefit
            //     to be applied.
            deductibleAdjustedCosts += benefitCost - benefitRemainder;
            // e.g. if the benefit value is 500, but the total deductible has already
            //      exceeded the deductible by 100, then only 400 will be applied.
          }
        }
        // End If [ appliesToDeductible || appliesToFamilyDeductible ]
        
        // If [ the benefit applies to the OOP Max ] Then
        if (applyToMaxOutOfPocket) {
          // Holds the value to be applied to OOP Costs, if any
          double amountForOOPCosts = 0;
          
          // If [ the benefit didn't apply to the deductible ] Then
          if (!appliesToDeductible) {
            // Apply the entire benefit value to the total OOP Costs
            amountForOOPCosts = benefitCost;
            oOPCosts += amountForOOPCosts;
            
            // We can also clear the remainder as it is all used.
            benefitRemainder = 0;
          }
          // Else If [ the benefit applied partially to the deductible, it was met ] Then
          else if (appliesToDeductible && benefitRemainder > 0) {
            // Apply the remainder of the benefit value to the total OOP Costs
            amountForOOPCosts = benefitRemainder;
            oOPCosts += amountForOOPCosts;
  
            // We can also clear the remainder as it is all used.
            benefitRemainder = 0;
          }
          
          // If [ the adjusted OOP Costs (costs not to exceed the oopMax) are below the oopMax 
          //   OR there is no OOP Max ] Then
          if (oOPAdjustedCosts < individualOopMax || individualOopMax == 0) {
            // We can apply the same amount to the adjusted OOP Costs that we did to the total
            oOPAdjustedCosts += amountForOOPCosts;
            // However, If [ there is an OOP Max 
            //           AND the adjusted OOP Costs now now exceeds the OOP Max ] Then
            if (individualOopMax > 0 && oOPAdjustedCosts > individualOopMax) {
              // Reduce the adjusted OOP Costs down to the OOP Max
              oOPAdjustedCosts = individualOopMax;
            }
          }
        }
        // End If [ applyToMaxOutOfPocket ]
          
        // If [ there is any remaining benefit that hasn't been addressed ] Then
        if (benefitRemainder > 0) {
          // Save it as extra Costs, which are applied directly to the Member
          extraCosts += benefitRemainder;
        }
      }
      // End Do For [ Benefit benefit : benefits ]
      
      // Sum up the Total Costs for the Member
      totalCosts = deductibleAdjustedCosts + oOPAdjustedCosts + extraCosts;
      memberCosts.totalCosts = totalCosts;
      
      memberCosts.deductibleCosts = deductibleCosts; 
      memberCosts.deductibleOnlyCosts = deductibleOnlyCosts;
      memberCosts.deductibleAdjustedCosts = deductibleAdjustedCosts;
      memberCosts.oopCosts = oOPCosts;
      memberCosts.oopAdjustedCosts = oOPAdjustedCosts;
      memberCosts.extraCosts = extraCosts;
      
      // Keep track of the Family Costs
      familyDeductibleCosts += memberCosts.deductibleCosts;
      familyOOPCosts += memberCosts.oopCosts;
      familyDeductibleOnlyCosts += memberCosts.deductibleOnlyCosts;
      familyDeductibleAdjustedCosts += memberCosts.deductibleAdjustedCosts;
      familyOOPAdjustedCosts += memberCosts.oopAdjustedCosts;
      familyExtraCosts += memberCosts.extraCosts;
    }
    // End Do For [ Member member : members ]
    
    // If [ the plan has a Family Deductible
    //  AND the adjusted Family Deductible exceeds the plan Family Deductible ] Then
    if (familyDeductible > 0 && familyDeductibleAdjustedCosts > familyDeductible) {
      // We need to reduce the Member adjusted deductibles so they fit within the Family Deductible
      double reduction = (familyDeductibleAdjustedCosts - familyDeductible) / memberCount;
      // We may also need to shift some of the Member deductible Only Costs (not applied to OOP Max)
      //   to the Member Extra Costs 
      double shiftedExtraCosts = (familyDeductibleOnlyCosts - familyDeductible) / memberCount;
      
      // Keep track of the adjusted Family Extra Costs
      double adjustedFamilyExtraCosts = 0;
      
      for (Member member : members) {
        // Adjust the Member adjusted Deductible Costs 
        result.memberCosts.get(member).deductibleAdjustedCosts -= reduction;
        // If [ some of the Deductible Only Costs should be shifted ] Then
        if (shiftedExtraCosts > 0) {
          // Shift the Costs
          result.memberCosts.get(member).extraCosts += shiftedExtraCosts;
        }
        
        // Re-summarize the Family Extra Costs 
        adjustedFamilyExtraCosts += result.memberCosts.get(member).extraCosts;
      }
      
      familyDeductibleAdjustedCosts = familyDeductible;
      familyExtraCosts = adjustedFamilyExtraCosts;
    }
    
    // If [ the plan has a Family OOP Max
    //  AND the Family adjusted OOP Costs exceeds the plan Family OOP Max
    //  AND it is a family ] Then
    if (familyOopMax > 0 && familyOOPAdjustedCosts > familyOopMax && memberCount > 1) {
      // We need to reduce the Member adjusted OOP Costs so they will fit within the Family OOP Max
      double reduction = (familyOOPAdjustedCosts - familyOopMax) / memberCount;
      
      for (Member member : members) {
        result.memberCosts.get(member).oopAdjustedCosts -= reduction;
      }
      
      // (Re)adjust the Family adjusted OOP Max to fit within the plan Family OOP Max 
      familyOOPAdjustedCosts = familyOopMax;
    }
    
    // Now, total up the costs for each Member and the entire Family
    for (Member member : members) {
      EstimatedCosts memberCosts = result.memberCosts.get(member);
      memberCosts.totalCosts = memberCosts.deductibleAdjustedCosts + memberCosts.oopAdjustedCosts + memberCosts.extraCosts;
    }

    familyCosts.totalCosts = familyDeductibleAdjustedCosts + familyOOPAdjustedCosts + familyExtraCosts;
    
    /****************************\
    | END OF RULE IMPLEMENTATION |
    \****************************/
    
    // Keep track of the Family Costs
    familyCosts.deductibleCosts = familyDeductibleCosts;
    familyCosts.oopCosts = familyOOPCosts;
    familyCosts.deductibleOnlyCosts = familyDeductibleOnlyCosts;
    familyCosts.deductibleAdjustedCosts = familyDeductibleAdjustedCosts;
    familyCosts.oopAdjustedCosts = familyOOPAdjustedCosts;
    familyCosts.extraCosts = familyExtraCosts;
    
    return result;
  }
}

class Member implements Comparable<Member> {
	String id;
	Member(String id) {
		this.id = id;
	}
	
	@Override
  public int compareTo(Member o)
  {
    return id.compareTo(o.id);
  }

  @Override
	public String toString() {
		return String.valueOf(id);
	}
}

class EstimatedCosts {
	double deductibleCosts;
	double deductibleOnlyCosts;
	double oopCosts;
	double deductibleAdjustedCosts;
	double oopAdjustedCosts;
	double extraCosts;
	double totalCosts;
	
	EstimatedCosts() {
	}
	
	EstimatedCosts(double deductibleCosts, double oopCosts, double deductibleAdjustedCosts, double oopAdjustedCosts, double extraCosts, double totalCosts) {
		this.deductibleCosts = deductibleCosts;
		this.oopCosts = oopCosts;
		this.deductibleAdjustedCosts = deductibleAdjustedCosts;
		this.oopAdjustedCosts = oopAdjustedCosts;
		this.extraCosts = extraCosts;
		this.totalCosts = totalCosts;
	}

	@Override
	public String toString() {
		return "<" + StringUtils.leftPad(String.valueOf(deductibleCosts), 15, ' ') + ", " + 
				StringUtils.leftPad(String.valueOf(oopCosts), 8, ' ') + ", " + 
				StringUtils.leftPad(String.valueOf(deductibleAdjustedCosts), 18, ' ') + ", " +  
				StringUtils.leftPad(String.valueOf(oopAdjustedCosts), 11, ' ') + ", " + 
				StringUtils.leftPad(String.valueOf(extraCosts), 10, ' ') + ", " + 
				StringUtils.leftPad(String.valueOf(totalCosts), 10, ' ') + ">";
	}

	public static String generateHeader(int members) {
	  StringBuffer header = new StringBuffer();
	  if (0 == members) {
      header.append(StringUtils.leftPad("<", 10, ' ') + 
        "deductibleCosts, oopCosts, deductibleAdjCosts, oopAdjCosts, extraCosts, totalCosts>");
	  }
	  else {
	    for (int m = 0; m < members; m++) {
	      header.append(StringUtils.leftPad("<", (m == 0 ? 14 : 6), ' ') + 
	        "deductibleCosts, oopCosts, deductibleAdjCosts, oopAdjCosts, extraCosts, totalCosts>");
	    }
	  }
	  return header.toString();
	}
}

class Benefit {
	String id;
	boolean applyToDeductible;
	boolean applyToOOPMax;
	boolean applyCoinsAfterDeductible;
	int numberOfCopayBeforeCoins;
	double value;
	
	Benefit(String id, double value, boolean applyToDeductible, boolean applyToOOPMax, boolean applyCoinsAfterDeductible, int numberOfCopayBeforeCoins) {
		this.id = id;
		this.value = value;
		this.applyToDeductible = applyToDeductible;
		this.applyToOOPMax = applyToOOPMax;
		this.applyCoinsAfterDeductible = applyCoinsAfterDeductible;
		this.numberOfCopayBeforeCoins = numberOfCopayBeforeCoins;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}
}

class PlanBenefit {
	String id;
	double individualDeductible;
	double individualOOPMax;
  double familyDeductible;
  double familyOOPMax;
	
	PlanBenefit(String id, double individualDeductible, double individualOOPMax, double familyDeductible, double familyOOPMax) {
		this.id = id;
		this.individualDeductible = individualDeductible;
		this.individualOOPMax = individualOOPMax;
    this.familyDeductible = familyDeductible;
    this.familyOOPMax = familyOOPMax;
	}
  
  public boolean hasIndividualDeductible() {
    return individualDeductible > 0d;
  }
  
  public boolean hasIndividualOOPMax() {
    return individualOOPMax > 0d;
  }
	
	public boolean hasFamilyDeductible() {
		return familyDeductible > 0d;
	}
	
	public boolean hasFamilyOOPMax() {
		return familyOOPMax > 0d;
	}
}

class CostCalculatorResult {
  Map<Member, EstimatedCosts> memberCosts = new TreeMap<Member, EstimatedCosts>();
  EstimatedCosts familyCosts = new EstimatedCosts();
}