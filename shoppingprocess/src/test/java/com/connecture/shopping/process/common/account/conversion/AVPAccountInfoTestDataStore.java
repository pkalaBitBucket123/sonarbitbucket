package com.connecture.shopping.process.common.account.conversion;

import java.util.ArrayList;
import java.util.List;

import com.connecture.shopping.process.domain.AccountInfo;

public class AVPAccountInfoTestDataStore implements AccountInfoDataStore
{
  private List<AccountInfo> accountInfo = new ArrayList<AccountInfo>();
  
  @Override
  public void storeData(List<AccountInfo> data) throws Exception
  {
    accountInfo = new ArrayList<AccountInfo>(data);
  }

  public List<AccountInfo> getAccountInfo()
  {
    return accountInfo;
  }
}
