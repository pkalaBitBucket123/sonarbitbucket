package com.connecture.shopping.process.common.account;

import junit.framework.Assert;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Criterion;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.shopping.process.domain.AccountInfo;

public class PersistedAccountImplTest
{
  @Test
  public void testGetAccount()
  {
    PersistedAccountImpl account = new PersistedAccountImpl();
    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
    account.setSessionFactory(sessionFactory);
    JSONObject result = account.getAccount("1");
    Assert.assertNull(result);
    
    AccountInfo accountInfo = new AccountInfo();
    JSONObject accountJSON = new JSONObject();
    accountInfo.setVersion("A1");  // Just throwing this here for coverage
    accountInfo.setValue(accountJSON);
    Mockito.when(criteria.uniqueResult()).thenReturn(accountInfo);

    result = account.getAccount("1");
    Assert.assertEquals(accountJSON, result);
  }
  
  @Test
  public void testUpdateAccountInfoKeyJSON() throws JSONException
  {
    PersistedAccountImpl account = new PersistedAccountImpl();
    AccountInfo accountInfo = new AccountInfo();
    account.updateAccountInfoKeyJSON("1", accountInfo);
    Assert.assertEquals("{\"transactionId\":\"1\"}", accountInfo.getValue().toString());
    account.updateAccountInfoKeyJSON("2", accountInfo);
    Assert.assertEquals("{\"transactionId\":\"2\"}", accountInfo.getValue().toString());
  }
  
  @Test
  public void testUpdateAccountInfo() throws JSONException
  {
    PersistedAccountImpl account = new PersistedAccountImpl();
    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
    account.setSessionFactory(sessionFactory);

    AccountInfo accountInfo = new AccountInfo();
    JSONObject accountJSON = new JSONObject();
    accountInfo.setValue(accountJSON);
    
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    
    JSONObject newAccountInfo = new JSONObject("{ \"name\" : \"Name\" }");
    
    account.updateAccountInfo("1",  newAccountInfo);
    
    JSONObject result = account.getAccount("1");
    Assert.assertNull(result);
    
    Mockito.when(criteria.uniqueResult()).thenReturn(accountInfo);
    accountJSON = new JSONObject();
    accountInfo.setValue(accountJSON);

    account.updateAccountInfo(null,  newAccountInfo);
    result = account.getAccount("1");
    Assert.assertEquals(newAccountInfo.toString(), result.toString());
    
    account.updateAccountInfo("1",  newAccountInfo);
    result = account.getAccount("1");
    Assert.assertEquals(newAccountInfo.toString(), result.toString());
  }
  
  @Test
  public void testGetOrCreateAccount()
  {
    PersistedAccountImpl account = new PersistedAccountImpl();
    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
    account.setSessionFactory(sessionFactory);
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    JSONObject result = account.getOrCreateAccount("1");
    Assert.assertEquals("{\"transactionId\":\"1\"}", result.toString());
    AccountInfo accountInfo = new AccountInfo();
    JSONObject accountJSON = new JSONObject();
    accountInfo.setValue(accountJSON);
    Mockito.when(criteria.uniqueResult()).thenReturn(accountInfo);
    result = account.getOrCreateAccount("1");
    Assert.assertEquals(new JSONObject().toString(), result.toString());
    
    Mockito.when(session.save(Mockito.any(PersistedAccountImpl.class))).thenThrow(new HibernateException("TEST"));
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    try 
    {
      account.getOrCreateAccount("1");
      Assert.fail("Expected Hibernate Exception to propogate up");
    }
    catch (HibernateException e)
    {
      // This is expected
    }
  }
  
  @Test
  public void testUpdateAccountTransactionId()
  {
    PersistedAccountImpl account = new PersistedAccountImpl();
    SessionFactory sessionFactory = Mockito.mock(SessionFactory.class);
    Session session = Mockito.mock(Session.class);
    Criteria criteria = Mockito.mock(Criteria.class);
    Mockito.when(criteria.add(Mockito.any(Criterion.class))).thenReturn(criteria);
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    Mockito.when(session.createCriteria(Mockito.any(Class.class))).thenReturn(criteria);
    Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
    account.setSessionFactory(sessionFactory);
    Mockito.when(criteria.uniqueResult()).thenReturn(null);
    account.updateAccountTransactionId(null, "1");
    JSONObject result = account.getAccount("1");
    Assert.assertNull(result);
    
    AccountInfo accountInfo = new AccountInfo();
    JSONObject accountJSON = new JSONObject();
    accountInfo.setValue(accountJSON);
    Mockito.when(criteria.uniqueResult()).thenReturn(accountInfo);
    account.updateAccountTransactionId("1", "2");
    result = account.getAccount("2");
    Assert.assertEquals("{\"transactionId\":\"2\"}", result.toString());
  }
}
