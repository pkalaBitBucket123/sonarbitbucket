package com.connecture.shopping.process.work.core;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

public class RuleSetDataProviderKeyTest
{

  @Test
  public void testGeneratedMethods()
  {
    RuleSetDataProviderKey key1 = new RuleSetDataProviderKey("Test");
    RuleSetDataProviderKey key2 = new RuleSetDataProviderKey("Test");
    RuleSetDataProviderKey key3 = new RuleSetDataProviderKey(null);
    RuleSetDataProviderKey key4 = new RuleSetDataProviderKey(null);
    
    Assert.assertTrue(key1.equals(key1));
    Assert.assertTrue(key1.equals(key2));
    Assert.assertFalse(key1.equals(null));
    Assert.assertFalse(key1.equals("Test"));
    Assert.assertFalse(key1.equals(key3));
    Assert.assertFalse(key3.equals(key1));
    Assert.assertTrue(key3.equals(key4));
    Map<RuleSetDataProviderKey,String> map = new HashMap<RuleSetDataProviderKey, String>();
    map.put(key1, "String");
    Assert.assertEquals("String", map.get(key2));
    Assert.assertNull(map.get(key3));
  }
}
