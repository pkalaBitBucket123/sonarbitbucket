package com.connecture.shopping.process.camel.request;

import java.util.ArrayList;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

public class RulesEngineDataRequestTest
{
  @Test
  public void testGetRequestHeaders()
  {
    RulesEngineDataRequest<String> request = new RulesEngineDataRequest<String>();
    request.setEffDate(1000l);
    request.setGroupId("1");
    request.setRuleCodes(new ArrayList<String>());
    Map<String,Object> result = request.getRequestHeaders();
    Assert.assertEquals(1000l, result.get("effDateMs"));
    Assert.assertEquals("1", result.get("groupId"));
    Assert.assertNotNull(result.get("ruleCodes"));
  }
  
  @Test
  public void testGetRequestBody()
  {
    RulesEngineDataRequest<String> request = new RulesEngineDataRequest<String>();
    String result = String.valueOf(request.getRequestBody());
    Assert.assertEquals("", result);
    
    String stringDomain = "{ name : \"Test\" }";
    request.setDomain(stringDomain);
    Assert.assertEquals(stringDomain, request.getDomain());
    result = String.valueOf(request.getRequestBody());
    Assert.assertEquals(stringDomain, result);
    
    MockRulesEngineDomain domain = new MockRulesEngineDomain();
    domain.setName("Test");
    RulesEngineDataRequest<MockRulesEngineDomain> request2 = new RulesEngineDataRequest<MockRulesEngineDomain>();
    request2.setDomain(domain);
    result = String.valueOf(request2.getRequestBody());
    Assert.assertEquals("{\"name\":\"Test\"}", result);
  }
  
  @Test
  public void testMisc()
  {
    RulesEngineDataRequest<String> request = new RulesEngineDataRequest<String>();
    Assert.assertNull(request.getProducer());
  }
}
