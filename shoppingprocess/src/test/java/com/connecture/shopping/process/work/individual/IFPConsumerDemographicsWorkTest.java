package com.connecture.shopping.process.work.individual;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.IAToShoppingContactData;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingFfmHouseholdData;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.DataProviderKey;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;
import com.connecture.shopping.process.work.core.ShoppingRulesEngineBean;

public class IFPConsumerDemographicsWorkTest
{
	
  @Test
  public void testGet() throws Exception
  {
	  IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    
    IAToShoppingIndividualDataRequest iaToShoppingRequest = Mockito.mock(IAToShoppingIndividualDataRequest.class);
    work.setIndividualDataRequest(iaToShoppingRequest);
    Account account = Mockito.mock(Account.class);
    work.setAccount(account);
    JSONObject accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    DataCache dataCache = Mockito.mock(DataCache.class);
    @SuppressWarnings("unchecked")
    DataProvider<IAToShoppingData> provider = Mockito.any(DataProvider.class);
    IAToShoppingData individualData = new IAToShoppingData();
    Mockito.when(dataCache.getData(Mockito.any(DataProviderKey.class), provider)).thenReturn(individualData);
    work.setDataCache(dataCache);
    
    EnrollmentEventRulesEngineBean mockBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    work.setHcrEffectiveDateBean(mockBean);
    EnrollmentEventDomainModel domainData = new EnrollmentEventDomainModel();
    domainData.getApplication().setEffectiveDate("01/01/2014");
    Mockito.when(mockBean.executeRule()).thenReturn(domainData);
    
    ShoppingRulesEngineBean mockShoppingRulesEngineBean = Mockito.mock(ShoppingRulesEngineBean.class);
    work.setShoppingDemographicsBean(mockShoppingRulesEngineBean);
    ShoppingDomainModel shoppingDomainData = new ShoppingDomainModel();
    Mockito.when(mockShoppingRulesEngineBean.executeRule()).thenReturn(shoppingDomainData);

    JSONObject paramsObject = new JSONObject();
    work.setParams(paramsObject);
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
    work.setIfpActorService(ifpActorService);
    work.get();
    
    Assert.assertEquals(EffectiveDateWorkHelper.getEffectiveDateMs("01/01/2014").longValue(), accountJSON.getLong("effectiveMs"));
    Assert.assertNull(accountJSON.optString("csrCode", null));
    Assert.assertFalse(accountJSON.getBoolean("isOnExchangePlans"));
    Assert.assertEquals(0, accountJSON.optJSONArray("members").length());
    Assert.assertNull(accountJSON.optJSONObject("cart"));
    Assert.assertNull(accountJSON.optJSONObject("questions"));
    Mockito.verify(account, Mockito.times(1)).updateAccountInfo(Mockito.anyString(), Mockito.any(JSONObject.class));
    
    IAToShoppingFfmHouseholdData houseHoldData = new IAToShoppingFfmHouseholdData();
    individualData.setFfmHouseholdData(houseHoldData);
    
    accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.get();
    
    Assert.assertNull(accountJSON.optString("csrCode", null));

    houseHoldData.setOnExchangePlans(true);
    
    accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.get();
    
    Assert.assertNull(accountJSON.optString("csrCode", null));
    
    houseHoldData.setCsrCode("A");
    
    accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.get();
    
    Assert.assertEquals("A", accountJSON.getString("csrCode"));

    accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    work.get();
    
    Assert.assertEquals(0, accountJSON.optJSONArray("members").length());

//    List<IAToShoppingMemberDetails> membersData = new ArrayList<IAToShoppingMemberDetails>();
//    individualData.setMemberDetails(membersData);
//    IAToShoppingMemberDetails memberDetails = new IAToShoppingMemberDetails();
//    membersData.add(memberDetails);
//    memberDetails.setMemberId(1);
//    memberDetails.setFirstName("firstName");
//    memberDetails.setMemberType("Primary");

//    Request request = shoppingDomainData.getRequest();
//    DemographicsForm demographicsForm = new DemographicsForm();
//    request.setDemographicsForm(demographicsForm);
//    List<DemographicsFormField> fields = new ArrayList<DemographicsFormField>();
//    demographicsForm.setField(fields);
//    
//    DemographicsFormField field = new DemographicsFormField();
//    List<DemographicsFormFieldOption> options = new ArrayList<DemographicsFormFieldOption>();
//    field.setOption(options);
//    fields.add(field);
//    
//    DemographicsFormFieldOption option = new DemographicsFormFieldOption();
//    option.setValue("A");
//    option.setDataGroupType("G");
//    options.add(option);
//    
//    accountJSON = new JSONObject();
//    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
//    work.get();
    
    // TODO Assert many things
    // TODO More testing
  }
  
  @Test
  public void testUpdateShoppingSpecificMemberData() throws JSONException
  {
    IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    Map<String, String> oldNewMemberIdMapping = new HashMap<String, String>();
    work.updateShoppingSpecificMemberData(null, null, oldNewMemberIdMapping);
    JSONArray oldMemberData = new JSONArray();
    work.updateShoppingSpecificMemberData(oldMemberData, null, oldNewMemberIdMapping);
    JSONArray newMemberData = new JSONArray();
    work.updateShoppingSpecificMemberData(null, newMemberData, oldNewMemberIdMapping);
    
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);
    
    JSONObject oldMemberObject = new JSONObject("{ \"memberRefName\" : \"A\" }");
    oldMemberData.put(oldMemberObject);
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);
    
    JSONObject newMemberObject = new JSONObject("{ \"memberRefName\" : \"B\" }");
    newMemberData.put(newMemberObject);
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);

    newMemberObject.put("memberRefName", "A");
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);
    
    oldNewMemberIdMapping.put("A", "C");
    newMemberObject.put("memberRefName", "B");
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);

    oldNewMemberIdMapping.put("A", "B");
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);
    Assert.assertNull(newMemberObject.optJSONArray("providers"));
    
    JSONArray providersArray = new JSONArray();
    oldMemberObject.put("providers", providersArray);
    work.updateShoppingSpecificMemberData(oldMemberData, newMemberData, oldNewMemberIdMapping);
    Assert.assertNotNull(newMemberObject.getJSONArray("providers"));
  }
  
  @Test
  public void testUpdateAccountCartMemberIds() throws JSONException
  {
    IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    Map<String, String> oldNewMemberIdMapping = new HashMap<String, String>();
    JSONObject accountJSON = new JSONObject();
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONObject cartObject = new JSONObject();
    accountJSON.put("cart", cartObject);
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONObject productLinesObject = new JSONObject();
    cartObject.put("productLines", productLinesObject);
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONObject productLineObject = new JSONObject();
    productLinesObject.put("MEDICAL", productLineObject);
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONArray coveredMembersArray = new JSONArray();
    productLineObject.put("coveredMembers", coveredMembersArray);
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONObject coveredMemberObject = new JSONObject("{ \"memberRefName\" : \"A\" }");
    coveredMembersArray.put(coveredMemberObject);
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    oldNewMemberIdMapping.put("A", "B");
    work.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);
    
    Assert.assertEquals("B", coveredMemberObject.getString("memberRefName"));
  }
  
  @Test
  public void testCleanUpQuestionMemberIds() throws JSONException
  {
    IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    Set<String> memberIds = new HashSet<String>();
    JSONObject accountJSON = new JSONObject();
    work.cleanUpQuestionMemberIds(memberIds, accountJSON);

    JSONArray questionsArray = new JSONArray();
    accountJSON.put("questions", questionsArray);
    work.cleanUpQuestionMemberIds(memberIds, accountJSON);
    
    JSONObject questionObject = new JSONObject();
    questionsArray.put(questionObject);
    work.cleanUpQuestionMemberIds(memberIds, accountJSON);

    JSONArray membersArray = new JSONArray();
    questionObject.put("members", membersArray);
    work.cleanUpQuestionMemberIds(memberIds, accountJSON);
    
    JSONObject memberObject = new JSONObject("{ \"memberRefName\" : \"A\" }");
    membersArray.put(memberObject);
    questionObject.put("members", membersArray);
    work.cleanUpQuestionMemberIds(memberIds, accountJSON);
    Assert.assertEquals(0, questionObject.getJSONArray("members").length());
    
    questionObject.put("members", membersArray);
    memberIds.add("A");
    work.cleanUpQuestionMemberIds(memberIds, accountJSON);
    Assert.assertEquals(1, questionObject.getJSONArray("members").length());
  }
  
  @Test
  public void testUpdateQuestionMemberIds() throws JSONException
  {
    IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    Map<String, String> oldNewMemberIdMapping = new HashMap<String, String>();
    JSONObject accountJSON = new JSONObject();
    work.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONArray questionsArray = new JSONArray();
    accountJSON.put("questions", questionsArray);
    work.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);
    
    JSONObject questionObject = new JSONObject();
    questionsArray.put(questionObject);
    work.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);

    JSONArray membersArray = new JSONArray();
    questionObject.put("members", membersArray);
    work.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);

    JSONObject memberObject = new JSONObject("{ \"memberRefName\" : \"A\" }");
    membersArray.put(memberObject);
    work.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);

    oldNewMemberIdMapping.put("A", "B");
    work.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);
    Assert.assertEquals("B", memberObject.getString("memberRefName"));
  }
  
  @Test
  public void testGetUpdatedMemberReferences() throws JSONException
  {
    IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    JSONObject newAccountJSON = new JSONObject();
    JSONObject currentShoppingJSON = new JSONObject(newAccountJSON.toString());
    
    JSONArray membersArray = new JSONArray();
    newAccountJSON.put("members", membersArray);
    
    Map<String, String> result = work.getUpdatedMemberReferences(currentShoppingJSON, newAccountJSON);
    Assert.assertEquals(0, result.size());
    
    JSONObject memberObject = new JSONObject();
    membersArray.put(memberObject);
    memberObject.put("memberRefName", "0");
    memberObject.put("birthDate", "01/01/1984");
    memberObject.put("gender", "M");
    memberObject.put("memberRelationship", "Self");
    JSONObject nameObject = new JSONObject();
    nameObject.put("first", "Test");
    memberObject.put("name", nameObject);
    
    currentShoppingJSON = new JSONObject(newAccountJSON.toString());

    memberObject.put("memberRefName", "1");
    memberObject.put("birthDate", "01/01/1984");
    memberObject.put("gender", "F");
    memberObject.put("memberRelationship", "Self");
    nameObject = new JSONObject();
    nameObject.put("first", "Test");
    memberObject.put("name", nameObject);

    result = work.getUpdatedMemberReferences(currentShoppingJSON, newAccountJSON);
    Assert.assertTrue(result.isEmpty());

    memberObject.put("gender", "M");

    result = work.getUpdatedMemberReferences(currentShoppingJSON, newAccountJSON);
    Assert.assertEquals(1, result.size());
    
    memberObject.put("memberRefName", "0");
    result = work.getUpdatedMemberReferences(currentShoppingJSON, newAccountJSON);
    Assert.assertTrue(result.isEmpty());
  }
  
  @Test
  public void testGetLatestMemberIds() throws JSONException
  {
    JSONObject accountJSON = new JSONObject();
    JSONArray membersArray = new JSONArray();
    accountJSON.put("members", membersArray);
    Set<String> result = new IFPConsumerDemographicsWork().getLatestMemberIds(accountJSON);
    Assert.assertTrue(result.isEmpty());
    
    JSONObject memberObject = new JSONObject("{ \"memberRefName\" : \"A\" }");
    membersArray.put(memberObject);
    
    result = new IFPConsumerDemographicsWork().getLatestMemberIds(accountJSON);
    Assert.assertEquals("A", result.toArray(new String[]{})[0]);
  }
  
  @Test
  public void testMemberDemographicsMatch() throws JSONException
  {
    JSONObject memberOne = new JSONObject();
    memberOne.put("birthDate", "01/01/1984");
    memberOne.put("gender", "M");
    memberOne.put("memberRelationship", "Self");
    JSONObject nameObject = new JSONObject();
    nameObject.put("first", "Test");
    memberOne.put("name", nameObject);

    JSONObject memberTwo = new JSONObject();
    memberTwo.put("birthDate", "01/02/1984");
    memberTwo.put("gender", "F");
    memberTwo.put("memberRelationship", "Child");
    nameObject = new JSONObject();
    nameObject.put("first", "TestBaby");
    memberTwo.put("name", nameObject);
    
    Assert.assertFalse(new IFPConsumerDemographicsWork().memberDemographicsMatch(memberOne, memberTwo));

    memberTwo.put("birthDate", "01/01/1984");
    Assert.assertFalse(new IFPConsumerDemographicsWork().memberDemographicsMatch(memberOne, memberTwo));

    memberTwo.put("gender", "M");
    Assert.assertFalse(new IFPConsumerDemographicsWork().memberDemographicsMatch(memberOne, memberTwo));

    memberTwo.put("memberRelationship", "Self");
    Assert.assertFalse(new IFPConsumerDemographicsWork().memberDemographicsMatch(memberOne, memberTwo));

    nameObject.put("first", "Test");
    Assert.assertTrue(new IFPConsumerDemographicsWork().memberDemographicsMatch(memberOne, memberTwo));
    
    memberTwo.put("isSmoker", "y");
    Assert.assertFalse(new IFPConsumerDemographicsWork().memberDemographicsMatch(memberOne, memberTwo));
  }
  
  @Test
  public void testPropertyMatchOptionalBooleanString() throws JSONException
  {
    JSONObject objectOne = new JSONObject();
    JSONObject objectTwo = new JSONObject();
    Assert.assertTrue(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));

    objectTwo.put("value", "y");
    Assert.assertFalse(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));

    objectTwo.put("value", "n");
    Assert.assertTrue(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));
    
    objectOne.put("value", "y");
    Assert.assertFalse(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));

    objectOne.put("value", "n");
    Assert.assertTrue(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));
    
    objectTwo.remove("value");
    Assert.assertTrue(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));

    objectOne.put("value", "y");
    Assert.assertFalse(IFPConsumerDemographicsWork.propertyMatchOptionalBooleanString(objectOne, objectTwo, "value"));
  }
  
  @Test
  public void testBuildAccountHolderData() throws JSONException
  {
    JSONObject result = IFPConsumerDemographicsWork.buildAccountHolderData(null);
    Assert.assertNull(result.optString("firstName", null));
    Assert.assertNull(result.optString("lastName", null));
    Assert.assertNull(result.optString("email", null));
    Assert.assertNull(result.optString("phone", null));
    
    IAToShoppingContactData data = new IAToShoppingContactData();
    result = IFPConsumerDemographicsWork.buildAccountHolderData(data);
    Assert.assertNull(result.optString("firstName", null));
    Assert.assertNull(result.optString("lastName", null));
    Assert.assertNull(result.optString("email", null));
    Assert.assertNull(result.optString("phone", null));
  }

  @Test
  public void testProcessData() throws Exception
  {
    IFPConsumerDemographicsWork work = new IFPConsumerDemographicsWork();
    IAToShoppingIndividualDataRequest request = Mockito.mock(IAToShoppingIndividualDataRequest.class);
    work.setIndividualDataRequest(request);
    Account account = Mockito.mock(Account.class);
    work.setAccount(account);
    JSONObject accountJSON = new JSONObject();
    Mockito.when(account.getAccount(Mockito.anyString())).thenReturn(accountJSON);
    DataCache dataCache = Mockito.mock(DataCache.class);
    @SuppressWarnings("unchecked")
    DataProvider<IAToShoppingData> provider = Mockito.any(DataProvider.class);
    IAToShoppingData data = new IAToShoppingData();
    Mockito.when(dataCache.getData(Mockito.any(DataProviderKey.class), provider)).thenReturn(data);
    work.setDataCache(dataCache);
    
    EnrollmentEventRulesEngineBean mockBean = Mockito.mock(EnrollmentEventRulesEngineBean.class);
    work.setHcrEffectiveDateBean(mockBean);
    EnrollmentEventDomainModel domainData = new EnrollmentEventDomainModel();
    domainData.getApplication().setEffectiveDate("01/01/2014");
    Mockito.when(mockBean.executeRule()).thenReturn(domainData);
    
    ShoppingRulesEngineBean mockShoppingRulesEngineBean = Mockito.mock(ShoppingRulesEngineBean.class);
    work.setShoppingDemographicsBean(mockShoppingRulesEngineBean);
    ShoppingDomainModel shoppingDomainData = new ShoppingDomainModel();
    Mockito.when(mockShoppingRulesEngineBean.executeRule()).thenReturn(shoppingDomainData);

    JSONObject paramsObject = new JSONObject();
    work.setParams(paramsObject);
    ActorService ifpActorService = Mockito.mock(IFPActorWork.class);
    Actor actor = Mockito.mock(Actor.class);
    Mockito.when(ifpActorService.getActorData()).thenReturn(actor);
    work.setIfpActorService(ifpActorService);
    work.get();
    JSONObject result = work.processData();
    
    // TODO Assert many things
    
    // TODO More testing
  }
 
}
