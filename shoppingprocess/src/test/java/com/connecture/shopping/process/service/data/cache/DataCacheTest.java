package com.connecture.shopping.process.service.data.cache;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;

public class DataCacheTest
{
  @Test
  public void testGetData() throws Exception
  {
    final DataCache cache = new DataCache();
    
    DataCacheTestData data = cache.getData(DataCacheTestProviderKey.TEST, null);
    Assert.assertNull(data);
    
    DataProvider<DataCacheTestData> provider = new DataCacheTestDataProvider();
    data = cache.getData(DataCacheTestProviderKey.TEST, provider);
    Assert.assertNotNull(data);
    Assert.assertEquals("TEST", data.getValue());
    
    data = cache.getData(DataCacheTestProviderKey.TEST, null);
    Assert.assertNotNull(data);
  }
  
  @Test
  public void testSynchronizedFetch() throws InterruptedException
  {
    final DataCache cache = new DataCache();
    final CountDownLatch fetchLatch = new CountDownLatch(1);
    final CountDownLatch getDataLatch = new CountDownLatch(2);
    new Thread(new Runnable() {
      public void run() {
        SynchronizingDataCacheTestDataProvider provider2 = new SynchronizingDataCacheTestDataProvider();
        provider2.setFetchLatch(fetchLatch);
        try
        {
          cache.getData(DataCacheTestProviderKey.TEST2, provider2);
          getDataLatch.countDown();
        }
        catch (Exception e)
        {
          Assert.fail("Unexpected exception");
        }
      }
    }).start();
    new Thread(new Runnable() {
      public void run() {
        SynchronizingDataCacheTestDataProvider provider3 = new SynchronizingDataCacheTestDataProvider();
        provider3.setFetchLatch(fetchLatch);
        try
        {
          cache.getData(DataCacheTestProviderKey.TEST2, provider3);
          getDataLatch.countDown();
        }
        catch (Exception e)
        {
          Assert.fail("Unexpected exception");
        }
      }
    }).start();
    Assert.assertTrue(fetchLatch.await(1000, TimeUnit.MILLISECONDS));
    Assert.assertTrue(getDataLatch.await(1000, TimeUnit.MILLISECONDS));
  }
}

class SynchronizingDataCacheTestDataProvider implements DataProvider<DataCacheTestData>
{
  private CountDownLatch fetchLatch;

  @Override
  public DataCacheTestData fetchData()
  {
    DataCacheTestData data = new DataCacheTestData();
    data.setValue("TEST");

    try
    {
      Thread.sleep(250);
    }
    catch (InterruptedException e)
    {
    }
    fetchLatch.countDown();
    
    return data;
  }
  
  public void setFetchLatch(CountDownLatch fetchLatch)
  {
    this.fetchLatch = fetchLatch;
  }
}