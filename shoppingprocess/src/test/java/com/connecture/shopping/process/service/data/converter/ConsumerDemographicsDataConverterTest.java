package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.connecture.model.data.ShoppingDependent;
import com.connecture.model.data.ShoppingEmployee;
import com.connecture.model.data.ShoppingEmployer;
import com.connecture.model.data.ShoppingEvent;
import com.connecture.model.data.ShoppingGender;
import com.connecture.model.data.ShoppingInitializationData;
import com.connecture.model.data.ShoppingProduct;
import com.connecture.model.data.ShoppingRelationship;
import com.connecture.model.data.ShoppingTierChangeData;
import com.connecture.shopping.process.service.data.ConsumerData;
import com.connecture.shopping.process.service.data.GenderData;
import com.connecture.shopping.process.service.data.MemberData;
import com.connecture.shopping.process.service.data.NameData;
import com.connecture.shopping.process.service.data.RelationshipData;

public class ConsumerDemographicsDataConverterTest
{

  @Test
  public void testCreateRelationshipData()
  {
    ConsumerDemographicsDataConverter converter = new ConsumerDemographicsDataConverter();
    List<ShoppingRelationship> relationships = new ArrayList<ShoppingRelationship>();
    List<RelationshipData> result = converter.createRelationshipData(relationships);
    Assert.assertTrue(result.isEmpty());
    
    ShoppingRelationship relationship = new ShoppingRelationship();
    relationship.setKey("A");
    relationship.setDisplayValue("A1");
    relationships.add(relationship);
    
    result = converter.createRelationshipData(relationships);
    Assert.assertEquals(1, result.size());
    RelationshipData data = result.get(0);
    Assert.assertEquals("A", data.getType());
    Assert.assertEquals("A1", data.getDisplay());
  }
  
  @Test
  public void testCreateMemberDataList()
  {
    ConsumerDemographicsDataConverter converter = new ConsumerDemographicsDataConverter();
    Calendar calendar = new GregorianCalendar(1980, 4, 5);
    Date birthDate = calendar.getTime();
    ShoppingEmployee employee = new ShoppingEmployee.Builder("1", "M", "Primary", birthDate, "90210", "me@me.com", "No", "Me", "Man").build();
    List<ShoppingDependent> dependents = new ArrayList<ShoppingDependent>();
    List<MemberData> result = converter.createMemberDataList(employee, dependents);
    Assert.assertEquals(1, result.size());
    MemberData member = result.get(0);
    Assert.assertEquals("1", member.getMemberExtRefId());
    Assert.assertEquals("M", member.getGender());
    Assert.assertEquals("Primary", member.getMemberRelationship());
    Assert.assertEquals("05/05/1980", member.getBirthDate());
    Assert.assertEquals("90210", member.getZipCode());
    Assert.assertEquals("me@me.com", member.getWorkEmail());
    NameData nameData = member.getNameData();
    Assert.assertEquals("Me", nameData.getFirst());
    Assert.assertNull(nameData.getLast());
    
    ShoppingDependent dependent = new ShoppingDependent();
    dependent.setExtRefId("2");
    dependent.setGenderTypeKey("F");
    dependent.setRelationshipTypeKey("Spouse");
    dependent.setDateOfBirth(birthDate);
    dependent.setSmoker("No");
    dependent.setFirstName("Her");
    dependent.setNew(true);
    dependents.add(dependent);
    
    result = converter.createMemberDataList(employee, dependents);
    Assert.assertEquals(2, result.size());
    
    member = result.get(1);
    Assert.assertEquals("2", member.getMemberExtRefId());
    Assert.assertEquals("F", member.getGender());
    Assert.assertEquals("Spouse", member.getMemberRelationship());
    Assert.assertEquals("05/05/1980", member.getBirthDate());
    Assert.assertEquals("90210", member.getZipCode());
    Assert.assertNull(member.getWorkEmail());
    nameData = member.getNameData();
    Assert.assertEquals("Her", nameData.getFirst());
    Assert.assertNull(nameData.getLast());
  }

  @Test
  public void testCreateGenderData()
  {
    ConsumerDemographicsDataConverter converter = new ConsumerDemographicsDataConverter();
    List<ShoppingGender> genders = new ArrayList<ShoppingGender>();
    List<GenderData> genderDataList = converter.createGenderData(genders);
    Assert.assertTrue(genderDataList.isEmpty());
    
    ShoppingGender gender = new ShoppingGender();
    gender.setKey("M");
    gender.setDisplayValue("Male");
    genders.add(gender);
    genderDataList = converter.createGenderData(genders);
    GenderData genderData = genderDataList.get(0);
    Assert.assertEquals("M", genderData.getKey());
    Assert.assertEquals("Male", genderData.getDisplay());
  }
  
  @Test
  public void testConvertData() throws Exception
  {
    ShoppingInitializationData data = new ShoppingInitializationData();
    
    ShoppingGender gender = new ShoppingGender();
    gender.setKey("M");
    gender.setDisplayValue("Male");
    List<ShoppingGender> genders = new ArrayList<ShoppingGender>();
    genders.add(gender);
    data.setGenderRefData(genders);

    Calendar calendar = new GregorianCalendar(1980, 4, 5);
    Date birthDate = calendar.getTime();
    ShoppingEmployee employee = new ShoppingEmployee.Builder("1", "M", "Primary", birthDate, "90210", "me@me.com", "No", "Me", "Man").build();
    ShoppingDependent dependent = new ShoppingDependent();
    dependent.setExtRefId("2");
    dependent.setGenderTypeKey("F");
    dependent.setRelationshipTypeKey("Spouse");
    dependent.setDateOfBirth(birthDate);
    dependent.setSmoker("No");
    dependent.setFirstName("Her");
    dependent.setNew(true);
    List<ShoppingDependent> dependents = new ArrayList<ShoppingDependent>();
    dependents.add(dependent);
    data.setEmployee(employee);
    data.setDependents(dependents);
    
    ShoppingRelationship relationship = new ShoppingRelationship();
    relationship.setKey("A");
    relationship.setDisplayValue("A1");
    List<ShoppingRelationship> relationships = new ArrayList<ShoppingRelationship>();
    relationships.add(relationship);
    data.setRelationshipRefData(relationships);
    
    ShoppingTierChangeData shoppingData = new ShoppingTierChangeData();
    
    shoppingData.setEligibilityGroupId(1l);
    
    List<ShoppingProduct> products = new ArrayList<ShoppingProduct>();
    shoppingData.setPlans(products);
    
    shoppingData.setPolicyEffectiveDate(birthDate);
    
    data.setShoppingData(shoppingData);
    
    ShoppingEmployer employer = new ShoppingEmployer();
    data.setEmployer(employer);
    
    ConsumerDemographicsDataConverter converter = new ConsumerDemographicsDataConverter();
    ConsumerData result = converter.convertData(data);
    
    ShoppingEvent event = new ShoppingEvent();
    data.setEvent(event);
    result = converter.convertData(data);
    
    event.setEnrollmentPeriodEndDate(birthDate);
    result = converter.convertData(data);

    Assert.assertEquals(1l, result.getDemographicsData().getEligibilityGroupId().longValue());
    Assert.assertEquals(birthDate.getTime(), result.getDemographicsData().getPolicyEffectiveTime().longValue());
    Assert.assertTrue(result.getPlanData().getPlans().isEmpty());
  }
}
