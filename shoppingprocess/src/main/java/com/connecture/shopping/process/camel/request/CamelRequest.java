package com.connecture.shopping.process.camel.request;

import java.util.Map;

import org.apache.camel.ProducerTemplate;

import com.connecture.shopping.process.IntegrationException;

public interface CamelRequest<T>
{
  ProducerTemplate getProducer();
  Map<String, Object> getRequestHeaders();
  Object getRequestBody();
  T submitRequest() throws IntegrationException;
}
