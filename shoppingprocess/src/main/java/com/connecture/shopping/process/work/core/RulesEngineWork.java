package com.connecture.shopping.process.work.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.RulesEngineDataRequest;
import com.connecture.shopping.process.service.data.Attribute;
import com.connecture.shopping.process.service.data.AttributeConsumer;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ConsumerProfile;
import com.connecture.shopping.process.service.data.ConsumerUsage;
import com.connecture.shopping.process.service.data.Product;
import com.connecture.shopping.process.service.data.Profile;
import com.connecture.shopping.process.service.data.Request;
import com.connecture.shopping.process.service.data.UsageCategory;
import com.connecture.shopping.process.service.data.UsageData;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;

public class RulesEngineWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(RulesEngineWork.class);

  private RulesEngineDataRequest<Request> rulesEngineDataRequest;
  private Request rulesEngineResult;

  private String ruleSetId;

  private JSONObject domainObject;

  @Override
  public void get() throws Exception
  {
    rulesEngineDataRequest.setGroupId("shopping");
    List<String> ruleCodes = new ArrayList<String>();
    ruleCodes.add(ruleSetId);
    rulesEngineDataRequest.setRuleCodes(ruleCodes);

    Request request = new Request();
    request.setId("0");
    request.setTimeInMs(new Date().getTime());

    if (domainObject.has("consumer"))
    {
      List<Consumer> consumers = new ArrayList<Consumer>();
      JSONArray consumerArray = domainObject.getJSONArray("consumer");
      for (int c = 0; c < consumerArray.length(); c++)
      {
        JSONObject consumerObject = consumerArray.getJSONObject(c);
        Consumer consumer = new Consumer();
        consumer.setGender(consumerObject.getString("gender"));
        consumer.setDateOfBirth(consumerObject.getString("dateOfBirth"));
        consumer.setZip(consumerObject.getString("zip"));
        consumer.setXid(consumerObject.getString("xid"));

        if (consumerObject.has("profile"))
        {
          List<ConsumerProfile> consumerProfiles = new ArrayList<ConsumerProfile>();
          JSONArray profileArray = consumerObject.getJSONArray("profile");
          for (int p = 0; p < profileArray.length(); p++)
          {
            JSONObject consumerProfileObject = profileArray.getJSONObject(p);
            ConsumerProfile consumerProfile = new ConsumerProfile();
            consumerProfile.setKey(consumerProfileObject.getString("key"));
            consumerProfile.setValue(consumerProfileObject.getInt("value"));
            consumerProfiles.add(consumerProfile);
          }
          consumer.setProfile(consumerProfiles);
        }

        consumers.add(consumer);
      }

      request.setConsumer(consumers);
    }

    if (domainObject.has("product"))
    {
      List<Product> products = new ArrayList<Product>();
      JSONArray productArray = domainObject.getJSONArray("product");
      for (int p = 0; p < productArray.length(); p++)
      {
        JSONObject productObject = productArray.getJSONObject(p);
        Product product = new Product();
        product.setXid(productObject.getString("xid"));

        if (productObject.has("attribute"))
        {
          List<Attribute> attributes = new ArrayList<Attribute>();
          JSONArray attributeArray = productObject.getJSONArray("attribute");
          for (int a = 0; a < attributeArray.length(); a++)
          {
            JSONObject attributeObject = attributeArray.getJSONObject(a);
            Attribute attribute = new Attribute();
            attribute.setKey(attributeObject.getString("key"));
            attribute.setValue(attributeObject.getDouble("value"));

            if (attributeObject.has("consumer"))
            {
              List<AttributeConsumer> attributeConsumers = new ArrayList<AttributeConsumer>();
              JSONArray attributeConsumerArray = attributeObject.getJSONArray("consumer");
              for (int ac = 0; ac < attributeConsumerArray.length(); ac++)
              {
                JSONObject attributeConsumerObject = attributeConsumerArray.getJSONObject(ac);
                AttributeConsumer attributeConsumer = new AttributeConsumer();
                attributeConsumer.setXid(attributeConsumerObject.getString("xid"));
                attributeConsumer.setCost(attributeConsumerObject.getInt("cost"));
                attributeConsumers.add(attributeConsumer);
              }

              attribute.setConsumer(attributeConsumers);
            }

            attributes.add(attribute);
          }

          product.setAttribute(attributes);
        }

        products.add(product);
      }

      request.setProduct(products);
    }

    if (domainObject.has("profile"))
    {
      List<Profile> profiles = new ArrayList<Profile>();
      JSONArray profileArray = domainObject.getJSONArray("profile");
      for (int p = 0; p < profileArray.length(); p++)
      {
        JSONObject profileObject = profileArray.getJSONObject(p);
        Profile profile = new Profile();
        profile.setId(profileObject.getString("id"));
        profile.setName(profileObject.getString("name"));
        profile.setDescription(profileObject.getString("description"));

        if (profileObject.has("usageCategory"))
        {
          List<UsageCategory> usageCategories = new ArrayList<UsageCategory>();
          JSONArray usageCategoryArray = profileObject.getJSONArray("usageCategory");
          for (int uc = 0; uc < usageCategoryArray.length(); uc++)
          {
            JSONObject usageCategoryObject = usageCategoryArray.getJSONObject(uc);
            UsageCategory usageCategory = new UsageCategory();
            usageCategory.setCategoryId(usageCategoryObject.getString("categoryId"));
            usageCategory.setDisplay(usageCategoryObject.getString("display"));

            if (usageCategoryObject.has("usageData"))
            {
              List<UsageData> usageDataList = new ArrayList<UsageData>();
              JSONArray usageDataArray = usageCategoryObject.getJSONArray("usageData");
              for (int ud = 0; ud < usageDataArray.length(); ud++)
              {
                JSONObject usageDataObject = usageDataArray.getJSONObject(uc);
                UsageData usageData = new UsageData();
                usageData.setKey(usageDataObject.getString("key"));
                usageData.setDisplay(usageDataObject.getString("display"));

                if (usageDataObject.has("consumer"))
                {
                  List<ConsumerUsage> consumerUsageList = new ArrayList<ConsumerUsage>();
                  JSONArray consumerUsageArray = usageDataObject.getJSONArray("consumer");
                  for (int cu = 0; cu < consumerUsageArray.length(); cu++)
                  {
                    JSONObject consumerUsageObject = consumerUsageArray.getJSONObject(cu);
                    ConsumerUsage consumerUsage = new ConsumerUsage();
                    consumerUsage.setXid(consumerUsageObject.getString("xid"));
                    consumerUsage.setValue(consumerUsageObject.getDouble("value"));
                    consumerUsageList.add(consumerUsage);
                  }

                  usageData.setConsumer(consumerUsageList);
                }

                usageDataList.add(usageData);
              }

              usageCategory.setUsageData(usageDataList);
            }

            usageCategories.add(usageCategory);
          }

          profile.setUsageCategory(usageCategories);
        }

        profiles.add(profile);
      }

      request.setProfile(profiles);
    }

    rulesEngineDataRequest.setDomain(request);
    
    DataProvider<Request> dataProvider = new CamelRequestingDataProvider<Request>(rulesEngineDataRequest);
    rulesEngineResult = getCachedData(new RuleSetDataProviderKey(ruleSetId), dataProvider);
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    JSONArray costsArray = new JSONArray();

    // If there was no AccountInfo yet there won't be a RulesEngine result
    // either
    if (null != rulesEngineResult)
    {
      if (StringUtils.isBlank(rulesEngineResult.getError()))
      {
        for (Product product : rulesEngineResult.getProduct())
        {
          costsArray.put(new JSONObject(product));
        }
      }
      else
      {
        LOG.error("Unable to obtain profile settings from RulesEngine. Cause: "
          + rulesEngineResult.getError());
      }
    }

    jsonObject.put("costEstimatorCosts", costsArray);

    return jsonObject;
  }

  public void setRuleSetId(String ruleSetId)
  {
    this.ruleSetId = ruleSetId;
  }

  public void setRulesEngineDataRequest(RulesEngineDataRequest<Request> rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }

  public void setDomain(JSONObject domainObject)
  {
    this.domainObject = domainObject;
  }
}
