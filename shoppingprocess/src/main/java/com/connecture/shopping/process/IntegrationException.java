package com.connecture.shopping.process;

public class IntegrationException extends RuntimeException
{
  private static final long serialVersionUID = 8457767218310262928L;

  public IntegrationException()
  {
    super();
  }
  
  public IntegrationException(String message)
  {
    super(message);
  }
  
  public IntegrationException(Throwable cause)
  {
    super(cause);
  }
  
  public IntegrationException(String message, Throwable cause)
  {
    super(message, cause);
  }
}
