package com.connecture.shopping.process.common.account;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.domain.AccountInfo;

/**
 * This name is kind of caca works for now
 * 
 * @author Shopping Development
 */
public class PersistedAccountImpl implements Account
{
  private static Logger LOG = Logger.getLogger(PersistedAccountImpl.class);

  private SessionFactory sessionFactory;

  private String accountVersion;

  /**
   * Retrieves the account JSONObject
   * 
   * @return
   */
  public JSONObject getAccount(String transactionId)
  {
    JSONObject account = null;
    
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Getting Account Data for { transactionId : \"" + transactionId + " }");
    }

    Session session = sessionFactory.getCurrentSession();
    Criteria crit = session.createCriteria(AccountInfo.class).add(
      Restrictions.eq("transactionId", transactionId));

    AccountInfo accountInfo = (AccountInfo) crit.uniqueResult();

    if (null != accountInfo)
    {
      if (LOG.isDebugEnabled())
      {
        String accountInfoStr = accountInfo.getValue().toString();
        LOG.debug("Retrieved AccountInfo of " + accountInfoStr);
      }
      
      account = accountInfo.getValue();
    }

    return account;
  }

  public void updateAccountInfo(String transactionId, JSONObject account)
  {
    Session session = sessionFactory.getCurrentSession();
    AccountInfo accountInfo = getAccountInfo(transactionId);
    
    if (null != accountInfo)
    {
      accountInfo.setValue(account);

      accountInfo.setUpdated(new Date());

      updateAccountInfoKeyJSON(accountInfo.getTransactionId(), accountInfo);

      if (LOG.isDebugEnabled())
      {
        LOG.debug("Updating AccountInfo to " + accountInfo.getValue().toString());
      }

      session.save(accountInfo);
      session.flush();
    }
    else
    {
      LOG.warn("No accountInfo to update for transactionId \"" + transactionId + "\"");
    }
  }

  public void updateAccountTransactionId(String oldTransactionId, String newTransactionId)
  {
    Session session = sessionFactory.getCurrentSession();
    AccountInfo accountInfo = getAccountInfo(oldTransactionId);

    if (null != accountInfo)
    {
      accountInfo.setTransactionId(newTransactionId);
      
      accountInfo.setUpdated(new Date());
      
      updateAccountInfoKeyJSON(accountInfo.getTransactionId(), accountInfo);
      
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Updating AccountInfo to " + accountInfo.getValue().toString());
      }
      
      session.save(accountInfo);
      session.flush();
    }
    else
    {
      LOG.warn("No accountInfo to update for transactionId \"" + oldTransactionId + "\"");
    }
  }

  public JSONObject getOrCreateAccount(String transactionId)
  {
    AccountInfo accountInfo = getOrCreateAccountInfo(transactionId);
    return accountInfo.getValue();
  }

  @Override
  public void clearAccount()
  {
    // STUB : don't do this for persisted account...
  }
  
  void updateAccountInfoKeyJSON(String transactionId, AccountInfo accountInfo)
  {
    JSONObject accountObject = null;

    if (null != accountInfo.getValue())
    {
      accountObject = accountInfo.getValue();
    }
    else
    {
      accountObject = new JSONObject();
    }
    try 
    {
      accountObject.put("transactionId", transactionId);
      accountInfo.setValue(accountObject);
    }
    catch (JSONException e) 
    {
      LOG.error("TransactionId \"" + transactionId + "\" is not valid, not updating accountInfo", e);
      // NOTE not sure we'll ever see this
    }
  }

  private AccountInfo getOrCreateAccountInfo(String transactionId)
  {
    AccountInfo accountInfo = getAccountInfo(transactionId);

    // No accountInfo the very first time we try to get
    if (null == accountInfo)
    {
      accountInfo = createAccountInfo(transactionId);
    }

    return accountInfo;
  }

  private AccountInfo getAccountInfo(String transactionId)
  {
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Getting Account Data for { transactionId : \"" + transactionId + " }");
    }

    Session session = sessionFactory.getCurrentSession();
    Criteria crit = session.createCriteria(AccountInfo.class).add(
      Restrictions.eq("transactionId", transactionId));

    AccountInfo accountInfo = (AccountInfo) crit.uniqueResult();

    String accountInfoStr = null;
    if (null != accountInfo)
    {
      accountInfoStr = accountInfo.getValue().toString();
    }

    if (LOG.isDebugEnabled())
    {
      LOG.debug("Retrieved AccountInfo of " + accountInfoStr);
    }

    return accountInfo;
  }

  private AccountInfo createAccountInfo(String transactionId)
  {
    AccountInfo accountInfo = null;
    
    try
    {
      Session session = sessionFactory.getCurrentSession();
      // Create a new one
      accountInfo = new AccountInfo();
      accountInfo.setTransactionId(transactionId);
      accountInfo.setVersion(accountVersion);
      Date created = new Date();
      accountInfo.setCreated(created);
      accountInfo.setUpdated(created);
  
      updateAccountInfoKeyJSON(transactionId, accountInfo);
  
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Creating AccountInfo as " + accountInfo.getValue().toString());
      }
  
      session.save(accountInfo);
      session.flush();
    }
    catch (HibernateException e)
    {
      LOG.error("Failed creating Account", e);
      e.fillInStackTrace();
      throw e;
    }

    return accountInfo;
  }

  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }

  public void setAccountVersion(String accountVersion)
  {
    this.accountVersion = accountVersion;
  }
}
