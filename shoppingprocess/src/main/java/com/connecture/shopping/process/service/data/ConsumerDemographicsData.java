package com.connecture.shopping.process.service.data;

import java.util.List;

public class ConsumerDemographicsData
{
  private Long eligibilityGroupId;
  
  private List<MemberData> members;
  private List<RelationshipData> relationships;
  private List<GenderData> genders;

  private String policyEffectiveDate;
  private Long policyEffectiveTime;
  private Long expirationTime;
  
  // TODO | change to Employer Data
  private String employerName;
  private String employerLogoPath;
  
  private EmployeeData employeeData;
  
  /**
   * @return the eligibilityGroupId
   */
  public Long getEligibilityGroupId()
  {
    return eligibilityGroupId;
  }
  /**
   * @param eligibilityGroupId the eligibilityGroupId to set
   */
  public void setEligibilityGroupId(Long eligibilityGroupId)
  {
    this.eligibilityGroupId = eligibilityGroupId;
  }
  /**
   * @return the members
   */
  public List<MemberData> getMembers()
  {
    return members;
  }
  /**
   * @param members the members to set
   */
  public void setMembers(List<MemberData> members)
  {
    this.members = members;
  }
  /**
   * @return the relationships
   */
  public List<RelationshipData> getRelationships()
  {
    return relationships;
  }
  /**
   * @param relationships the relationships to set
   */
  public void setRelationships(List<RelationshipData> relationships)
  {
    this.relationships = relationships;
  }
  /**
   * @return the policyEffectiveTime
   */
  public Long getPolicyEffectiveTime()
  {
    return policyEffectiveTime;
  }
  /**
   * @param policyEffectiveTime the policyEffectiveTime to set
   */
  public void setPolicyEffectiveTime(Long policyEffectiveTime)
  {
    this.policyEffectiveTime = policyEffectiveTime;
  }
  /**
   * @return the expirationTime
   */
  public Long getExpirationTime()
  {
    return expirationTime;
  }
  /**
   * @param expirationTime the expirationTime to set
   */
  public void setExpirationTime(Long expirationTime)
  {
    this.expirationTime = expirationTime;
  }
  /**
   * @return the employerName
   */
  public String getEmployerName()
  {
    return employerName;
  }
  /**
   * @param employerName the employerName to set
   */
  public void setEmployerName(String employerName)
  {
    this.employerName = employerName;
  }
  /**
   * @return the employerLogoPath
   */
  public String getEmployerLogoPath()
  {
    return employerLogoPath;
  }
  /**
   * @param employerLogoPath the employerLogoPath to set
   */
  public void setEmployerLogoPath(String employerLogoPath)
  {
    this.employerLogoPath = employerLogoPath;
  }
  /**
   * @return the employeeData
   */
  public EmployeeData getEmployeeData()
  {
    return employeeData;
  }
  /**
   * @param employeeData the employeeData to set
   */
  public void setEmployeeData(EmployeeData employeeData)
  {
    this.employeeData = employeeData;
  }
  /**
   * @return the genders
   */
  public List<GenderData> getGenders()
  {
    return genders;
  }
  /**
   * @param genders the genders to set
   */
  public void setGenders(List<GenderData> genders)
  {
    this.genders = genders;
  }
  
  public String getPolicyEffectiveDate() {
    return policyEffectiveDate;
  }
  
  public void setPolicyEffectiveDate(String policyEffectiveDate) {
    this.policyEffectiveDate = policyEffectiveDate;
  }
}