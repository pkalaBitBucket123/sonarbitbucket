package com.connecture.shopping.process.service.data.availability;

public class AvailabilityProductBenefit
{
  private String internalCode;

  public String getInternalCode()
  {
    return internalCode;
  }
  public void setInternalCode(String internalCode)
  {
    this.internalCode = internalCode;
  }
}
