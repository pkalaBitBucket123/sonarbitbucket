package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONPathRemoveAction implements JSONPathAction
{
  private Object removed;
  
  @Override
  public boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException
  {
    boolean success = false;
    
    if (isValidFor(state))
    {
      removed = jsonObject.remove(key);
    
      success = removed != null;
    }
    
    return success;
  }

  @Override
  public boolean isValidFor(PathState state)
  {
    return JSONPathAction.PathState.FOUND.equals(state);
  }

  public Object getRemoved()
  {
    return removed;
  }
}
