package com.connecture.shopping.process.service.data;

import java.util.List;

public class DemographicsConfigData
{
  private List<DemographicsConfigFieldData> demographicsFields;

  public List<DemographicsConfigFieldData> getDemographicsFields()
  {
    return demographicsFields;
  }

  public void setDemographicsFields(List<DemographicsConfigFieldData> demographicsFields)
  {
    this.demographicsFields = demographicsFields;
  }
}
