package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.model.integration.data.ShoppingToIAData;

public class IAShoppingAccountDataRequest extends BaseCamelRequest<ShoppingToIAData>
{
  @Produce(uri="{{camel.shopping.producer.ifpAccountData}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
  
  public void setContext(ShoppingContext context)
  {
    headers.put(CONTEXT_KEY, context.getContextName());
  }
  
  public void setTransactionId(String transactionId)
  {
    headers.put(TRANSACTION_ID_KEY, transactionId);
  }
}
