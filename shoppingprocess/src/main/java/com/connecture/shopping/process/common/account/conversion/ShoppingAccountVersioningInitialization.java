package com.connecture.shopping.process.common.account.conversion;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

public class ShoppingAccountVersioningInitialization
  implements ApplicationListener<ContextRefreshedEvent>
{
  private static Logger LOG = Logger.getLogger(ShoppingAccountVersioningInitialization.class);

  private AccountVersioningProcess aVp;
  private PlatformTransactionManager transactionManager;

  @Override
  public void onApplicationEvent(ContextRefreshedEvent arg0)
  {
    TransactionStatus status = transactionManager.getTransaction(new DefaultTransactionDefinition(
      TransactionDefinition.PROPAGATION_REQUIRED));

    try
    {
      aVp.versionAccounts();

      transactionManager.commit(status);
    }
    catch (Exception e)
    {
      if (!status.isCompleted())
      {
        transactionManager.rollback(status);
      }
      LOG.error("Issues detected during account conversion", e);
    }
  }

  public void setAccountVersioningProcess(AccountVersioningProcess aVp)
  {
    this.aVp = aVp;
  }

  public void setTransactionManager(PlatformTransactionManager transactionManager)
  {
    this.transactionManager = transactionManager;
  }
}
