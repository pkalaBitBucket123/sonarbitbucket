package com.connecture.shopping.process.camel.request;

import java.util.List;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingMember;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.service.data.ConsumerPlanData;

public class ConsumerTierDataRequest extends BaseCamelRequest<ConsumerPlanData>
{
  public static final String MEMBER_JSON_KEY = "memberJSON";
  public static final String SHOPPING_MEMBERS_KEY = "members";
  public static final String ELIGIBILITY_GROUP_ID_KEY = "eligibilityGroupId";
  
  @Produce(uri="{{camel.shopping.producer.tier}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
  
  public void setTransactionId(String transactionId)
  {
    headers.put(TRANSACTION_ID_KEY, transactionId);
  }
  
  public void setContext(ShoppingContext context)
  {
    headers.put(CONTEXT_KEY, context.getContextName());
  }
  
  /**
   * for integration through client jar objects
   * @param members
   */
  public void setShoppingMembers(List<ShoppingMember> members)
  {
    headers.put(SHOPPING_MEMBERS_KEY, members);
  }

  /**
   * for integration through JSON
   * @param memberJSON
   */
  public void setMemberJSON(JSONObject memberJSON)
  {
    headers.put(MEMBER_JSON_KEY, memberJSON);
  }
  
  public void setEligibilityGroupId(Long eligibilityGroupId)
  {
    headers.put(ELIGIBILITY_GROUP_ID_KEY, eligibilityGroupId);
  }
}