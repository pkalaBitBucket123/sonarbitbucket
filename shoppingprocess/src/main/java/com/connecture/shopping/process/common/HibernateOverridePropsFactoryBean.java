package com.connecture.shopping.process.common;

import java.util.Iterator;
import java.util.Properties;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.io.Resource;

/**
 * Allows hibernate override properties to defined in a override file which can
 * be defined by setting the <code>framework.propertyregistry.override</code>
 * jvm property.
 */
public class HibernateOverridePropsFactoryBean implements FactoryBean<Properties>
{
  private static final String PLANSERVICE_OVERRIDE_SYS_PROP = "framework.propertyregistry.grouppolicyadmin.override";

//  private Logger logger = Logger.getLogger(HibernateOverridePropsFactoryBean.class);

  private String overrideProperty = PLANSERVICE_OVERRIDE_SYS_PROP;

  private Resource defaultProperties;

//  private PropertiesPersister persister = new DefaultPropertiesPersister();

  public Properties getObject() throws Exception
  {
    /*String overrideStr = System.getProperty(overrideProperty);
    ResourceEditor resEditor = new ResourceEditor();
    resEditor.setAsText(overrideStr);
    Resource overrideResource = (Resource) resEditor.getValue();
    Properties overrideProps = new Properties();
    if (overrideResource == null)
    {
      logger.warn("Unable to load system override file: " + overrideStr + " for jvm property "
        + overrideProperty);
    }
    else
    {
      persister.load(overrideProps, overrideResource.getInputStream());
    }

    Properties hibProps = new Properties();
    persister.load(hibProps, defaultProperties.getInputStream());

    Configuration config = null;

    overrideHibernateProperties(overrideProps, hibProps);
    boolean usingJTA = hibProps.containsKey("hibernate.connection.datasource");
    // if the datasource is not defined, assume jdbc, otherwise jta
    if (usingJTA)
    {
      hibProps.remove("hibernate.connection.driver_class");
      hibProps.remove("hibernate.connection.url");
      hibProps.remove("hibernate.connection.username");
      hibProps.remove("hibernate.connection.password");
    }
    else
    {
      hibProps.remove("hibernate.connection.datasource");
    }
    return hibProps;*/
    return null;
  }

  protected void overrideHibernateProperties(Properties overrideProps, Properties hibProps)
  {
    Iterator<Object> iter = overrideProps.keySet().iterator();
    while (iter.hasNext())
    {
      String key = (String) iter.next();
      if (key.startsWith("hibernate."))
      {
        hibProps.setProperty(key, overrideProps.getProperty(key));
      }
    }
  }

  public Class<Properties> getObjectType()
  {
    return Properties.class;
  }

  public boolean isSingleton()
  {
    return true;
  }

  public String getOverrideProperty()
  {
    return overrideProperty;
  }

  public void setOverrideProperty(String overrideProperty)
  {
    this.overrideProperty = overrideProperty;
  }

  public Resource getDefaultProperties()
  {
    return defaultProperties;
  }

  public void setDefaultProperties(Resource defaultProperties)
  {
    this.defaultProperties = defaultProperties;
  }
}