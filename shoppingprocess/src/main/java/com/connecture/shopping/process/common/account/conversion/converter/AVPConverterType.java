package com.connecture.shopping.process.common.account.conversion.converter;

public enum AVPConverterType
{
  DELETE("delete"), ADD("add"), RENAME("rename"), OBJECT_TO_ARRAY("objectToArray");
  
  private String name;

  private AVPConverterType(String name) {
    this.name = name;
  }
  
  public static AVPConverterType findForName(String name) {
    AVPConverterType type = null;
    
    for (AVPConverterType aType : values()) {
      if (aType.name.equals(name)) {
        type = aType;
      }
    }
    
    return type;
  }
}
