package com.connecture.shopping.process.common.account.conversion.json;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public interface JSONSupport<T>
{
  T valueOf(JSONObject jsonObject) throws JSONException;
  List<String> validateJSONObject(JSONObject jsonObject) throws JSONException;
}
