package com.connecture.shopping.process.service.data.converter;

public enum FrequencyCode
{
  MONTHLY("monthly"), PAYCHECK("paycheck");
  
  private String value;

  private FrequencyCode(String value)
  {
    this.value = value;
  }
  
  public static FrequencyCode fromString(String value)
  {
    FrequencyCode code = null;
    if (MONTHLY.value.equalsIgnoreCase(value)) 
    {
      code = MONTHLY;
    }
    else if (PAYCHECK.value.equalsIgnoreCase(value))
    {
      code = PAYCHECK;
    }
    return code;
  }
}
