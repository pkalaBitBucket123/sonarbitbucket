package com.connecture.shopping.process.work.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EffectiveDateWorkHelper
{
  public static final String DATE_FORMAT = "MM/dd/yyyy";
  
  public static Long getEffectiveDateMs(String effectiveDateStr) throws ParseException
  {
    return getEffectiveDate(effectiveDateStr).getTime();
  }

  public static Date getEffectiveDate(String effectiveDateStr) throws ParseException
  {
    return new SimpleDateFormat(DATE_FORMAT).parse(effectiveDateStr);
  }
  
  public static String getEffectiveDateString(Date effectiveDate) throws ParseException
  {
    return new SimpleDateFormat(DATE_FORMAT).format(effectiveDate);
  }
}
