package com.connecture.shopping.process.work.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.data.ProductLineData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.employee.ProductPlanDataWork;

public class ProductLinesWork extends WorkImpl
{
  // Start Required Inputs
  private String transactionId;
  private ProductPlanDataWork productPlanDataWork;
  // End Required Inputs

  // Start Outputs
  private ProductLineData productLineData;
  // End Outputs

  @Override
  public void get() throws Exception
  {
    productPlanDataWork.setTransactionId(transactionId);
    productPlanDataWork.setAccount(getAccount());
    productPlanDataWork.setDataCache(getDataCache());
    productPlanDataWork.get();
    ProductPlanData productPlanData = productPlanDataWork.getProductPlanData();
    
    productLineData = getProductLineData(productPlanData);
  }

  public static ProductLineData getProductLineData(ProductPlanData productPlanData)
  {
    ProductLineData productLineData = null;

    if (null != productPlanData)
    {
      List<ProductData> productLines = new ArrayList<ProductData>();

      // Derive the ProductData.plaCount from available plans
      for (PlanData planData : productPlanData.getPlans())
      {
        ProductData productData = planData.getProductData();
        if (productLines.contains(productData))
        {
          ProductData existingProductData = productLines.get(productLines.indexOf(productData));
          existingProductData.setPlanCount(existingProductData.getPlanCount() + 1);
        }
        else
        {
          productData.setPlanCount(1);
          productLines.add(productData);
        }
      }

      Collections.sort(productLines, new Comparator<ProductData>()
        {
          @Override
          public int compare(ProductData p1, ProductData p2)
          {
            return p1.getSortOrder() - p2.getSortOrder();
          }
        });

      productLineData = new ProductLineData();
      productLineData.setProducts(productLines);
    }

    return productLineData;
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();
    
    // add product lines data
    if (productLineData != null)
    {
      JSONArray productArray = new JSONArray();
      for (ProductData productData : productLineData.getProducts())
      {
        productArray.put(new JSONObject(productData));
      }

      jsonObject.put("productLines", productArray);
    }
    else
    {
      jsonObject.put("productLines", new JSONArray());
    }

    return jsonObject;
  }
  
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setProductPlanDataWork(ProductPlanDataWork productPlanDataWork)
  {
    this.productPlanDataWork = productPlanDataWork;
  }
}
