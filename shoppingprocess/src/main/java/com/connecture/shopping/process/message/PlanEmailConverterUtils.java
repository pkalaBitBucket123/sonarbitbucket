package com.connecture.shopping.process.message;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PlanEmailConverterUtils
{
  static PlanEmailBean convertToPlanEmailBean(JSONObject planJSON) throws JSONException
  {
    PlanEmailBean resultBean = new PlanEmailBean();

    // set name
    resultBean.setPlanName(planJSON.getString("name"));

    applyPlanCostData(planJSON, resultBean);

    // set benefit data
    List<PlanEmailBenefitCategoryBean> benefitCategories = convertToBenefitCategories(planJSON);
    resultBean.setBenefitCategories(benefitCategories);

    List<PlanEmailPreferenceMatch> preferenceMatches = convertToPreferenceMatches(planJSON);
    resultBean.setPreferenceMatches(preferenceMatches);

    List<PlanEmailProviderBean> inNetworkProviders = new ArrayList<PlanEmailProviderBean>();
    List<PlanEmailProviderBean> outOfNetworkProviders = new ArrayList<PlanEmailProviderBean>();

    List<PlanEmailProviderBean> emailProviders = convertToProviders(planJSON);

    // split into in network, out of network
    for (PlanEmailProviderBean provider : emailProviders)
    {
      if (provider.isInNetwork())
      {
        inNetworkProviders.add(provider);
      }
      else
      {
        outOfNetworkProviders.add(provider);
      }
    }

    resultBean.setInNetworkProviders(inNetworkProviders);
    resultBean.setOutOfNetworkProviders(outOfNetworkProviders);

    return resultBean;
  }

  static void applyPlanCostData(JSONObject planJSON, PlanEmailBean resultBean)
    throws JSONException
  {
    // set cost data
    JSONObject costJSON = planJSON.getJSONObject("cost");
    
    JSONObject monthlyCostJSON = costJSON.getJSONObject("monthly");
    JSONObject monthlyRateJSON = monthlyCostJSON.getJSONObject("rate");
    JSONObject monthlyContributionJSON = monthlyCostJSON.getJSONObject("contribution");
    
    Double monthlyRateAmount = monthlyRateJSON.getDouble("amount");
    Double monthlyContributionAmount = monthlyContributionJSON.getDouble("amount");
    Double monthlyCostAmount = calculateCost(monthlyRateAmount, monthlyContributionAmount);
    
    NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    resultBean.setMonthlyRate(currencyFormat.format(monthlyRateAmount));
    resultBean.setMonthlyContribution(currencyFormat.format(monthlyContributionAmount));
    resultBean.setMonthlyCost(currencyFormat.format(monthlyCostAmount));

    setPerPaycheckAmounts(costJSON, resultBean);
    
    // set estimated cost data
    resultBean.setEstimatedCost(convertToEstimatedCostBean(monthlyRateAmount, planJSON));
  }

  static void setPerPaycheckAmounts(
    JSONObject costJSON,
    PlanEmailBean resultBean) throws JSONException
  {
    if (costJSON.optJSONObject("perPaycheck") != null)
    {
    	JSONObject paycheckCostJSON = costJSON.getJSONObject("perPaycheck");
    	JSONObject paycheckRateJSON = paycheckCostJSON.getJSONObject("rate");
    	JSONObject paycheckContributionJSON = paycheckCostJSON.getJSONObject("contribution");
    	
    	Double paycheckRateAmount = paycheckRateJSON.getDouble("amount");
    	Double paycheckContributionAmount = paycheckContributionJSON.getDouble("amount");
    	Double paycheckCostAmount = calculateCost(paycheckRateAmount, paycheckContributionAmount); 

      NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    	resultBean.setPaycheckRate(currencyFormat.format(paycheckRateAmount));
      resultBean.setPaycheckContribution(currencyFormat.format(paycheckContributionAmount));
      resultBean.setPaycheckCost(currencyFormat.format(paycheckCostAmount));
    }
  }

  static double calculateCost(double rate, double contribution)
  {
    double costAmount = rate - contribution;
    return (costAmount > 0 ? costAmount : 0);
  }

  static PlanEmailEstimatedCostBean convertToEstimatedCostBean(Double monthlyRate, JSONObject planJSON)
    throws JSONException
  {
    PlanEmailEstimatedCostBean estimatedCostBean = new PlanEmailEstimatedCostBean();
    
    double maximumCost = planJSON.optDouble("maximumCost");
    double estimatedCost = planJSON.optDouble("estimatedCost");

    boolean isEstimated = false;
    if (!Double.isNaN(estimatedCost) && !Double.isNaN(maximumCost))
    {
      double planYearCost = monthlyRate * 12;
      
      double estimatedCostPercentage = 40 + ( 60 * (estimatedCost - planYearCost) ) / maximumCost;
      estimatedCostBean.setEstimatedCostPercentage(estimatedCostPercentage);
      
      NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
      estimatedCostBean.setMinimumCost(currencyFormat.format(planYearCost));
      estimatedCostBean.setExpectedCost(currencyFormat.format(estimatedCost + planYearCost));
      estimatedCostBean.setMaximumCost(currencyFormat.format(maximumCost + planYearCost));
      isEstimated = true;
    }
    estimatedCostBean.setEstimated(isEstimated);

    return estimatedCostBean;
  }

  static List<PlanEmailPreferenceMatch> convertToPreferenceMatches(
    JSONObject planJSON) throws JSONException
  {
    List<PlanEmailPreferenceMatch> preferenceMatches = new ArrayList<PlanEmailPreferenceMatch>();

    if (!planJSON.isNull("preferenceMatches"))
    {
      // set preference match data
      JSONArray preferenceMatchesArray = planJSON.getJSONArray("preferenceMatches");
      
      for (int i = 0; i < preferenceMatchesArray.length(); i++)
      {
        PlanEmailPreferenceMatch preferenceMatch = new PlanEmailPreferenceMatch();

        JSONObject preferenceMatchJSON = preferenceMatchesArray.getJSONObject(i);
        preferenceMatch.setDescription(preferenceMatchJSON.getString("label"));
        preferenceMatch.setMatch(preferenceMatchJSON.getBoolean("value"));

        preferenceMatches.add(preferenceMatch);
      }
    }

    return preferenceMatches;
  }

  static List<PlanEmailProviderBean> convertToProviders(JSONObject planJSON)
    throws JSONException
  {
    List<PlanEmailProviderBean> providers = new ArrayList<PlanEmailProviderBean>();

    // set provider data
    if (!planJSON.isNull("providers"))
    {
      JSONArray providersJSON = planJSON.getJSONArray("providers");
      
      for (int i = 0; i < providersJSON.length(); i++)
      {
        PlanEmailProviderBean provider = new PlanEmailProviderBean();
  
        JSONObject providerData = providersJSON.getJSONObject(i);
        JSONObject providerObject = providerData.getJSONObject("provider");
  
        provider.setInNetwork(providerData.getBoolean("inNetwork"));
  
        provider.setType(providerObject.getString("type"));
  
        // TODO | provider type enum
        if ("physician".equalsIgnoreCase(provider.getType()))
        {
          provider.setPersonalName(true);
          // get individual name
          JSONObject providerName = providerObject.getJSONObject("name");
          provider.setPrefix(providerName.getString("prefix"));
  
          provider.setFirstName(providerName.getString("first"));
          provider.setMiddleName(providerName.getString("middle"));
          provider.setLastName(providerName.getString("last"));
        }
        else
        {
          provider.setPersonalName(false);
          // TODO
          // get location name
          provider.setLocationName("-");
        }
  
        providers.add(provider);
      }
    }

    return providers;
  }

  static PlanEmailBenefitCategoryBean convertToBenefitCategory(
    JSONObject benefitCategoryJSON) throws JSONException
  {
    PlanEmailBenefitCategoryBean resultBenefitCategory = new PlanEmailBenefitCategoryBean();

    String benefitCategoryName = benefitCategoryJSON.getString("categoryName");

    // set category name
    resultBenefitCategory.setCategoryName(benefitCategoryName);

    List<PlanEmailBenefitBean> benefits = new ArrayList<PlanEmailBenefitBean>();

    JSONArray categoryBenefits = benefitCategoryJSON.getJSONArray("benefits");
    for (int j = 0; j < categoryBenefits.length(); j++)
    {
      PlanEmailBenefitBean benefitBean = new PlanEmailBenefitBean();

      JSONObject benefit = categoryBenefits.getJSONObject(j);
      String benefitName = benefit.getString("displayName");

      // set benefit display name
      benefitBean.setBenefitName(benefitName);

      StringBuffer values = new StringBuffer();

      JSONArray benefitValues = benefit.getJSONArray("values");
      for (int k = 0; k < benefitValues.length(); k++)
      {
        if (values.length() > 0)
        {
          values.append(" / ");
        }

        JSONObject benefitValue = benefitValues.getJSONObject(k);
        String value = benefitValue.getString("value");
        values.append(value);
      }

      // set benefit value(s)
      benefitBean.setBenefitValue(values.toString());

      benefits.add(benefitBean);
    }

    resultBenefitCategory.setBenefits(benefits);

    return resultBenefitCategory;
  }

  static List<PlanEmailBenefitCategoryBean> convertToBenefitCategories(JSONObject planJSON)
    throws JSONException
  {
    List<PlanEmailBenefitCategoryBean> benefitCategories = new ArrayList<PlanEmailBenefitCategoryBean>();

    JSONArray benefitCategoriesJSON = planJSON.getJSONArray("benefitCategories");
    for (int i = 0; i < benefitCategoriesJSON.length(); i++)
    {
      JSONObject benefitCategory = benefitCategoriesJSON.getJSONObject(i);

      PlanEmailBenefitCategoryBean benefitCategoryBean = convertToBenefitCategory(benefitCategory);

      benefitCategories.add(benefitCategoryBean);
    }
    return benefitCategories;
  }
}
