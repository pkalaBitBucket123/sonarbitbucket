package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class QuestionData
{
  // differences:
  // shopping -> | <- planAdvisor
  private String questionRefId;
  private String questionnaireRefId; // (<-categoryRefId)
  private String title; // (<-navText)
  private String text;
  private String educationalContent;
  private String helpText;
  private String helpType; // e.g. Icon
  private String classOverride;
  private String displayType;
  private String widgetId;
  private Boolean required;
  private String evaluationType; // e.g.  Filtering, ?
  private Boolean memberLevelQuestion;
  private Integer minimumAnswers;
  private Integer maximumAnswers;
  private String filterRefId;
  private String dataTagKey;
  private String weight;
  public String getWeight()
  {
    return weight;
  }

  public void setWeight(String weight)
  {
    this.weight = weight;
  }

  private List<AnswerData> answers = new ArrayList<AnswerData>();

  public String getQuestionRefId()
  {
    return questionRefId;
  }

  public void setQuestionRefId(String questionRefId)
  {
    this.questionRefId = questionRefId;
  }

  public String getQuestionnaireRefId()
  {
    return questionnaireRefId;
  }

  public void setQuestionnaireRefId(String questionnaireRefId)
  {
    this.questionnaireRefId = questionnaireRefId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getEducationalContent()
  {
    return educationalContent;
  }

  public void setEducationalContent(String educationalContent)
  {
    this.educationalContent = educationalContent;
  }

  public String getDisplayType()
  {
    return displayType;
  }

  public void setDisplayType(String displayType)
  {
    this.displayType = displayType;
  }

  public List<AnswerData> getAnswers()
  {
    return answers;
  }

  public void setAnswers(List<AnswerData> answers)
  {
    this.answers = answers;
  }

  public String getHelpText()
  {
    return helpText;
  }

  public void setHelpText(String helpText)
  {
    this.helpText = helpText;
  }

  public String getHelpType()
  {
    return helpType;
  }

  public void setHelpType(String helpType)
  {
    this.helpType = helpType;
  }

  public String getFilterRefId()
  {
    return filterRefId;
  }

  public void setFilterRefId(String filterRefId)
  {
    this.filterRefId = filterRefId;
  }

  public String getClassOverride()
  {
    return classOverride;
  }

  public void setClassOverride(String classOverride)
  {
    this.classOverride = classOverride;
  }
  
  public String getEvaluationType()
  {
    return evaluationType;
  }

  public void setEvaluationType(String evaluationType)
  {
    this.evaluationType = evaluationType;
  }

  public String getWidgetId()
  {
    return widgetId;
  }

  public void setWidgetId(String widgetId)
  {
    this.widgetId = widgetId;
  }

  public Boolean getRequired()
  {
    return required;
  }

  public void setRequired(Boolean required)
  {
    this.required = required;
  }

  public Boolean getMemberLevelQuestion()
  {
    return memberLevelQuestion;
  }

  public void setMemberLevelQuestion(Boolean memberLevelQuestion)
  {
    this.memberLevelQuestion = memberLevelQuestion;
  }

  public Integer getMinimumAnswers()
  {
    return minimumAnswers;
  }

  public void setMinimumAnswers(Integer minimumAnswers)
  {
    this.minimumAnswers = minimumAnswers;
  }

  public Integer getMaximumAnswers()
  {
    return maximumAnswers;
  }

  public void setMaximumAnswers(Integer maximumAnswers)
  {
    this.maximumAnswers = maximumAnswers;
  }

  public String getDataTagKey()
  {
    return dataTagKey;
  }

  public void setDataTagKey(String dataTagKey)
  {
    this.dataTagKey = dataTagKey;
  }
}
