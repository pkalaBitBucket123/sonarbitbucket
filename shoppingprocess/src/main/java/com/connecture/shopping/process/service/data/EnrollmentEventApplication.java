package com.connecture.shopping.process.service.data;

public class EnrollmentEventApplication
{
  private String effectiveDate;
  private SpecialEnrollmentData specialEnrollment = new SpecialEnrollmentData();

  public String getEffectiveDate()
  {
    return effectiveDate;
  }

  public void setEffectiveDate(String effectiveDate)
  {
    this.effectiveDate = effectiveDate;
  }

  public SpecialEnrollmentData getSpecialEnrollment()
  {
    return specialEnrollment;
  }

  public void setSpecialEnrollment(SpecialEnrollmentData specialEnrollment)
  {
    this.specialEnrollment = specialEnrollment;
  }
}
