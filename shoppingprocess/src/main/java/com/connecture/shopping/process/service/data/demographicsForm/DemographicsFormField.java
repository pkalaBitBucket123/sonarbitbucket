package com.connecture.shopping.process.service.data.demographicsForm;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DemographicsFormField
{
  private String key;
  private String name;
  private String styleClass;
  private String headerStyleClass;
  private String dataType;
  private String inputType;
  private String label;
  private Integer size;
  private Integer order;

  private List<DemographicsFormFieldOption> option = new ArrayList<DemographicsFormFieldOption>();
  private DemographicsFormFieldMask mask = new DemographicsFormFieldMask();

  public static Comparator<DemographicsFormField> FieldOrderComparator = new Comparator<DemographicsFormField>()
    {

      public int compare(DemographicsFormField field1, DemographicsFormField field2)
      {
        // ascending order
        return Integer.valueOf(field1.getOrder()).compareTo(Integer.valueOf(field2.getOrder()));

      }

    };

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getStyleClass()
  {
    return styleClass;
  }

  public void setStyleClass(String styleClass)
  {
    this.styleClass = styleClass;
  }

  public String getHeaderStyleClass()
  {
    return headerStyleClass;
  }

  public void setHeaderStyleClass(String headerStyleClass)
  {
    this.headerStyleClass = headerStyleClass;
  }

  public String getDataType()
  {
    return dataType;
  }

  public void setDataType(String dataType)
  {
    this.dataType = dataType;
  }

  public String getInputType()
  {
    return inputType;
  }

  public void setInputType(String inputType)
  {
    this.inputType = inputType;
  }

  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public Integer getSize()
  {
    return size;
  }

  public void setSize(Integer size)
  {
    this.size = size;
  }

  public Integer getOrder()
  {
    return order;
  }

  public void setOrder(Integer order)
  {
    this.order = order;
  }

  public List<DemographicsFormFieldOption> getOption()
  {
    return option;
  }

  public void setOption(List<DemographicsFormFieldOption> option)
  {
    this.option = option;
  }

  public DemographicsFormFieldMask getMask()
  {
    return mask;
  }

  public void setMask(DemographicsFormFieldMask mask)
  {
    this.mask = mask;
  }
}