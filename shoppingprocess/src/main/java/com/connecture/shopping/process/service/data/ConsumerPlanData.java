package com.connecture.shopping.process.service.data;

import java.util.List;

import com.connecture.model.data.ShoppingProduct;

public class ConsumerPlanData
{
  private List<ShoppingProduct> plans;
  private Long policyEffectiveTime;
  private Integer paychecksPerYear;
  
  private List<MemberEligibilityData> memberEligibilities;

  public List<ShoppingProduct> getPlans()
  {
    return plans;
  }

  public void setPlans(List<ShoppingProduct> plans)
  {
    this.plans = plans;
  }

  /**
   * @return the policyEffectiveTime
   */
  public Long getPolicyEffectiveTime()
  {
    return policyEffectiveTime;
  }

  /**
   * @param policyEffectiveTime the policyEffectiveTime to set
   */
  public void setPolicyEffectiveTime(Long policyEffectiveTime)
  {
    this.policyEffectiveTime = policyEffectiveTime;
  }

  /**
   * @return the paychecksPerYear
   */
  public Integer getPaychecksPerYear()
  {
    return paychecksPerYear;
  }

  /**
   * @param paychecksPerYear the paychecksPerYear to set
   */
  public void setPaychecksPerYear(Integer paychecksPerYear)
  {
    this.paychecksPerYear = paychecksPerYear;
  }

  /**
   * @return the memberEligibilities
   */
  public List<MemberEligibilityData> getMemberEligibilities()
  {
    return memberEligibilities;
  }

  /**
   * @param memberEligibilities the memberEligibilities to set
   */
  public void setMemberEligibilities(List<MemberEligibilityData> memberEligibilities)
  {
    this.memberEligibilities = memberEligibilities;
  }
}