package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class ActorDataRequest extends BaseCamelRequest<Actor>
{  
  @Produce(uri="{{camel.shopping.producer.actor}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
  
  public void setContext(ShoppingContext context)
  {
    headers.put(CONTEXT_KEY, context);
  }
}