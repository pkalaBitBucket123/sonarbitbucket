package com.connecture.shopping.process.service.data.aptc.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.rules.aptc.APTCDomainPlan;
import com.connecture.model.data.rules.aptc.APTCDomainPlanBenefit;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;

public class APTCDomainPlanConverterUtils
{
  // private static Logger LOG =
  // Logger.getLogger(APTCDomainPlanConverterUtils.class);

  private static final String PLAN_ID = "Plan_Id";
  private static final String PREMIUM = "Premium";
  private static final String NET_PREMIUM = "Net_Premium";
  private static final String APTC_APPLIED_AMT = "APTC_Applied_Amt";
  private static final String TOBACCO_SURCHARGE = "Tobacco_Surcharge";
  private static final String PRODUCT_LINE_CODE = "Product_Line_Code";
  private static final String PRODUCT_TYPE = "Product_Type";
  private static final String BASE_PREMIUM = "Base_Premium";

  // benefits
  private static final String BENEFITS = "Benefits";
  private static final String KEY = "Key";

  // conversion for PlanData to APTCDomainPlan is a one-way street

  // FROM PLANDATA

  public static List<APTCDomainPlan> fromPlanData(List<PlanData> plans)
  {
    List<APTCDomainPlan> aptcDomainPlan = new ArrayList<APTCDomainPlan>();

    for (PlanData plan : plans)
    {
      aptcDomainPlan.add(fromPlanData(plan));
    }

    return aptcDomainPlan;
  }

  private static APTCDomainPlan fromPlanData(PlanData planData)
  {
    APTCDomainPlan aptcDomainPlan = new APTCDomainPlan();
    aptcDomainPlan.setPlanId(planData.getInternalCode());
    aptcDomainPlan.setRatedPremium(planData.getRate());
    aptcDomainPlan.setBasePremium(determineBaseRate(planData));
    aptcDomainPlan.setTobaccoSurcharge(planData.getTobaccoSurcharge());
    aptcDomainPlan.setProductLineKey(planData.getProductData().getProductLineCode());
    aptcDomainPlan.setProductTypeKey(planData.getPlanType());

    List<APTCDomainPlanBenefit> planBenefits = new ArrayList<APTCDomainPlanBenefit>();

    for (PlanBenefitCategoryData benefitCategory : planData.getBenefitCategories())
    {
      for (PlanBenefitData benefit : benefitCategory.getBenefits())
      {
        APTCDomainPlanBenefit planBenefit = new APTCDomainPlanBenefit();
        planBenefit.setKey(benefit.getInternalCode());
        planBenefits.add(planBenefit);
      }
    }

    aptcDomainPlan.setBenefits(planBenefits);

    return aptcDomainPlan;
  }

  private static Double determineBaseRate(PlanData planData)
  {
    return planData.getRate() - planData.getTobaccoSurcharge();
  }

  // TO JSON

  public static JSONArray toJSON(List<APTCDomainPlan> aptcDomainPlans) throws JSONException
  {
    JSONArray aptcDomainPlansJSON = new JSONArray();
    for (APTCDomainPlan aptcDomainPlan : aptcDomainPlans)
    {
      aptcDomainPlansJSON.put(toJSON(aptcDomainPlan));
    }
    return aptcDomainPlansJSON;
  }

  private static JSONObject toJSON(APTCDomainPlan aptcDomainPlan) throws JSONException
  {
    JSONObject aptcDomainPlanJSON = new JSONObject();
    // Inputs: plan properties (from rating)
    aptcDomainPlanJSON.put(PLAN_ID, aptcDomainPlan.getPlanId());
    aptcDomainPlanJSON.put(PREMIUM, aptcDomainPlan.getRatedPremium()); // includes
                                                                       // tobacco
                                                                       // surcharge.
    aptcDomainPlanJSON.put(TOBACCO_SURCHARGE, aptcDomainPlan.getTobaccoSurcharge());
    // Outputs: plan properties set as a result of running the rule
    aptcDomainPlanJSON.put(NET_PREMIUM, aptcDomainPlan.getNetPremium());
    aptcDomainPlanJSON.put(APTC_APPLIED_AMT, aptcDomainPlan.getAppliedAPTC());
    aptcDomainPlanJSON.put(PRODUCT_LINE_CODE, aptcDomainPlan.getProductLineKey());
    aptcDomainPlanJSON.put(PRODUCT_TYPE, aptcDomainPlan.getProductTypeKey());
    aptcDomainPlanJSON.put(BASE_PREMIUM, aptcDomainPlan.getBasePremium());
    aptcDomainPlanJSON.put(BENEFITS, toBeneiftsJSON(aptcDomainPlan.getBenefits()));

    aptcDomainPlanJSON.put(NET_PREMIUM, aptcDomainPlan.getNetPremium());
    aptcDomainPlanJSON.put(APTC_APPLIED_AMT, aptcDomainPlan.getAppliedAPTC());

    return aptcDomainPlanJSON;
  }

  private static JSONArray toBeneiftsJSON(List<APTCDomainPlanBenefit> aptcDomainPlanBenfits)
    throws JSONException
  {
    JSONArray benefitsJSON = new JSONArray();
    for (APTCDomainPlanBenefit benefit : aptcDomainPlanBenfits)
    {
      JSONObject benefitJSON = new JSONObject();
      benefitJSON.put(KEY, benefit.getKey());
      benefitsJSON.put(benefitJSON);
    }
    return benefitsJSON;
  }

  // FROM JSON

  public static List<APTCDomainPlan> fromJSON(JSONArray aptcDomainPlansJSON) throws JSONException
  {
    List<APTCDomainPlan> aptcDomainPlans = new ArrayList<APTCDomainPlan>();
    if (aptcDomainPlansJSON != null)
    {
      for (int i = 0; i < aptcDomainPlansJSON.length(); i++)
      {
        JSONObject aptcDomainPlanJSON = aptcDomainPlansJSON.getJSONObject(i);
        aptcDomainPlans.add(fromJSON(aptcDomainPlanJSON));
      }
    }
    return aptcDomainPlans;
  }

  private static APTCDomainPlan fromJSON(JSONObject aptcDomainPlanJSON) throws JSONException
  {
    APTCDomainPlan aptcDomainPlan = new APTCDomainPlan();
    aptcDomainPlan.setPlanId(aptcDomainPlanJSON.getString(PLAN_ID));
    // this is useless and won't be returned from the rule...
    //aptcDomainPlan.setRatedPremium(aptcDomainPlanJSON.getDouble(PREMIUM));
    aptcDomainPlan.setBasePremium(aptcDomainPlanJSON.getDouble(BASE_PREMIUM));
    // this won't change from the rule, don't need it here
    //aptcDomainPlan.setTobaccoSurcharge(aptcDomainPlanJSON.getDouble(TOBACCO_SURCHARGE));
    aptcDomainPlan.setNetPremium(aptcDomainPlanJSON.getDouble(NET_PREMIUM));
    aptcDomainPlan.setAppliedAPTC(aptcDomainPlanJSON.getDouble(APTC_APPLIED_AMT));
    return aptcDomainPlan;
  }

}
