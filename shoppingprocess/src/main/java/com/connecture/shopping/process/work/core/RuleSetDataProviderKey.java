package com.connecture.shopping.process.work.core;

import com.connecture.shopping.process.service.data.cache.DataProviderKey;

public class RuleSetDataProviderKey implements DataProviderKey
{
  private String ruleSetId;

  public RuleSetDataProviderKey(String ruleSetId)
  {
    this.ruleSetId = ruleSetId;
  }

  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((ruleSetId == null) ? 0 : ruleSetId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    RuleSetDataProviderKey other = (RuleSetDataProviderKey) obj;
    if (ruleSetId == null)
    {
      if (other.ruleSetId != null) return false;
    }
    else if (!ruleSetId.equals(other.ruleSetId)) return false;
    return true;
  }
}
