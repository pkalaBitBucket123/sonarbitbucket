package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.connecture.model.data.ShoppingDependent;
import com.connecture.model.data.ShoppingEmployee;
import com.connecture.model.data.ShoppingGender;
import com.connecture.model.data.ShoppingInitializationData;
import com.connecture.model.data.ShoppingRelationship;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.service.data.ConsumerData;
import com.connecture.shopping.process.service.data.ConsumerDemographicsData;
import com.connecture.shopping.process.service.data.ConsumerPlanData;
import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.EmployeeData;
import com.connecture.shopping.process.service.data.GenderData;
import com.connecture.shopping.process.service.data.MemberData;
import com.connecture.shopping.process.service.data.NameData;
import com.connecture.shopping.process.service.data.RelationshipData;



/**
 * Returns a domain data object representing the converted contents of the json
 * (String) input data
 */
public class ConsumerDemographicsDataConverter implements DataConverter<ShoppingInitializationData, ConsumerData>
{
  @Override
  public ConsumerData convertData(ShoppingInitializationData data) throws Exception
  {
    // return data
    // root
    ConsumerData consumerData = new ConsumerData();
    // demographics
    ConsumerDemographicsData consumerDemographicsData = new ConsumerDemographicsData();
    // plan data
    ConsumerPlanData consumerPlanData = new ConsumerPlanData();

    consumerDemographicsData.setMembers(createMemberDataList(data.getEmployee(), data.getDependents()));

    consumerDemographicsData.setGenders(createGenderData(data.getGenderRefData()));

    consumerDemographicsData.setRelationships(createRelationshipData(data.getRelationshipRefData()));

    String effectiveDate = ShoppingDateUtils.dateToString(data.getShoppingData().getPolicyEffectiveDate());
    consumerDemographicsData.setPolicyEffectiveTime(data.getShoppingData().getPolicyEffectiveDate().getTime());
    consumerDemographicsData.setPolicyEffectiveDate(effectiveDate);

    // There might not be an event
    if (null != data.getEvent())
    {
      Date enrollmentPeriodEndDate = data.getEvent().getEnrollmentPeriodEndDate();
      if (null != enrollmentPeriodEndDate)
      {
        // If there is an event, we should always get here 
        consumerDemographicsData.setExpirationTime(enrollmentPeriodEndDate.getTime());
      }
    }

    consumerPlanData.setPlans(data.getShoppingData().getPlans());

    consumerData.setDemographicsData(consumerDemographicsData);
    consumerData.setPlanData(consumerPlanData);
    consumerDemographicsData.setEligibilityGroupId(data.getShoppingData().getEligibilityGroupId());

    // set employer data
    consumerDemographicsData.setEmployerName(data.getEmployer().getName());
    consumerDemographicsData.setEmployerLogoPath(data.getEmployer().getLogoURL());

    // set employee data
    EmployeeData employeeData = new EmployeeData();
    employeeData.setFirstName(data.getEmployee().getFirstName());
    employeeData.setLastName(data.getEmployee().getLastName());
    employeeData.setEmail(data.getEmployee().getEmail());
    consumerDemographicsData.setEmployeeData(employeeData);

    return consumerData;
  }

  List<GenderData> createGenderData(List<ShoppingGender> shoppingGenders)
  {
    List<GenderData> genders = new ArrayList<GenderData>();

    for (ShoppingGender shoppingGender : shoppingGenders)
    {
      GenderData gender = new GenderData();
      gender.setDisplay(shoppingGender.getDisplayValue());
      gender.setKey(shoppingGender.getKey());
      genders.add(gender);
    }

    return genders;
  }

  List<MemberData> createMemberDataList(ShoppingEmployee employeeData, List<ShoppingDependent> dependents)
  {
    List<MemberData> memberDataList = new ArrayList<MemberData>();

    // build employee first
    MemberData primaryMember = new MemberData();
    primaryMember.setIsPrimary(true);
    primaryMember.setDateOfBirth(employeeData.getDateOfBirth().getTime());
    primaryMember.setBirthDate(ShoppingDateUtils.dateToString(employeeData.getDateOfBirth()));
    primaryMember.setGender(employeeData.getGenderTypeKey());
    primaryMember.setSmokerStatus(employeeData.getSmoker());
    primaryMember.setMemberExtRefId(employeeData.getExtRefId());
    primaryMember.setMemberRelationship(employeeData.getRelationshipTypeKey());
    primaryMember.setWorkEmail(employeeData.getEmail());
    primaryMember.setZipCode(employeeData.getZipCode());
    // set name
    NameData nameData = new NameData();
    nameData.setFirst(employeeData.getFirstName());
    primaryMember.setName(nameData);

    memberDataList.add(primaryMember);

    for (ShoppingDependent dependent : dependents)
    {
      MemberData dependentMember = new MemberData();
      dependentMember.setDateOfBirth(dependent.getDateOfBirth().getTime());
      dependentMember.setBirthDate(ShoppingDateUtils.dateToString(dependent.getDateOfBirth()));
      dependentMember.setGender(dependent.getGenderTypeKey());
      dependentMember.setSmokerStatus(dependent.getSmoker());
      dependentMember.setMemberExtRefId(dependent.getExtRefId());
      dependentMember.setMemberRelationship(dependent.getRelationshipTypeKey());

      dependentMember.setZipCode(employeeData.getZipCode());
      // set name
      NameData dependentNameData = new NameData();
      dependentNameData.setFirst(dependent.getFirstName());
      dependentMember.setName(dependentNameData);
      
      dependentMember.setNew(dependent.isNew());

      memberDataList.add(dependentMember);
    }

    return memberDataList;
  }

  List<RelationshipData> createRelationshipData(List<ShoppingRelationship> relationships)
  {
    List<RelationshipData> relationshipDataList = new ArrayList<RelationshipData>();

    for (ShoppingRelationship relationship : relationships)
    {
      RelationshipData relationshipData = new RelationshipData();
      relationshipData.setType(relationship.getKey());
      relationshipData.setDisplay(relationship.getDisplayValue());
      relationshipDataList.add(relationshipData);
    }

    return relationshipDataList;
  }
}