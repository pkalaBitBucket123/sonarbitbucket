package com.connecture.shopping.process.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

public class JSONUtils
{
  private JSONUtils()
  {
  }
  
  public static <T> Iterator<T> iterator(final JSONArray jsonArray)
  {
    List<T> objects = toList(jsonArray);
    return objects.iterator();
  }

  static <T> List<T> toList(final JSONArray jsonArray)
  {
    List<T> objects = new ArrayList<T>();
    for (int i = 0; i < jsonArray.length(); i++)
    {
      try
      {
        @SuppressWarnings("unchecked")
        T value = (T) jsonArray.get(i);
        objects.add(value);
      }
      catch (JSONException e)
      {
        // Dumbest API ever; it should throw a Runtime (not Checked) Exception
      }
    }
    return objects;
  }

  public static <T> List<T> toIterable(Class<T> objectClass, JSONArray jsonArray)
  {
    return toList(jsonArray);
  }
}
