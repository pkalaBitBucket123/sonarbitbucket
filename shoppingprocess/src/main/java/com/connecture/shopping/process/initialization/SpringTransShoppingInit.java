package com.connecture.shopping.process.initialization;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;


public class SpringTransShoppingInit implements ApplicationListener<ContextRefreshedEvent>
{
  private static final Logger LOG = Logger.getLogger(SpringTransShoppingInit.class);

  private SessionFactory sessionFactory;
  private PlatformTransactionManager transManager;
  private ShoppingInitializationHelper initHelper;
    
  @Override
  public void onApplicationEvent(ContextRefreshedEvent event)
  {
    contextRefreshedEvent();
  }

  protected void contextRefreshedEvent()
  {
    LOG.info("Initializing Shopping");
    // start a new transaction
    transManager.getTransaction(new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW));

    Session session = sessionFactory.getCurrentSession();
    Transaction tx = session.getTransaction();
    
    try
    {      
      initHelper.initComponentConfigurations();
      tx.commit();
    }
    catch (Exception e)
    {
      tx.rollback();
      throw new RuntimeException("Failed to load default component configurations", e);
    }
    
    try
    {
      initHelper.initExternalServices();
    }
    catch(Exception e)
    {
      throw new RuntimeException("Failed to contact required external service", e);
    }
  }

  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }

  public void setTransManager(PlatformTransactionManager transManager)
  {
    this.transManager = transManager;
  }
  
  public void setInitHelper(ShoppingInitializationHelper initHelper)
  {
    this.initHelper = initHelper;
  }
}
