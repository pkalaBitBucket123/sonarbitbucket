package com.connecture.shopping.process.work.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingMember;
import com.connecture.model.data.ShoppingProduct;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.ConsumerTierDataRequest;
import com.connecture.shopping.process.camel.request.ProductDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.ConsumerPlanData;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.service.data.converter.ConsumerDataConverterUtil;
import com.connecture.shopping.process.service.data.converter.FrequencyCode;
import com.connecture.shopping.process.service.data.converter.RatingPlanDataConverter;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueSetData;
import com.connecture.shopping.process.service.plan.data.PlanContributionData;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;

public class ProductPlanWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(ProductPlanWork.class);

  // inputs
  private String transactionId;
  // product server group, configurable
  private String productServerGroup;

  // requests
  private ConsumerTierDataRequest consumerTierDataRequest;
  private ProductDataRequest productDataRequest;

  // data objects (data resulting from requests)
  private ProductPlanData productPlanData;

  @Override
  public void get() throws Exception
  {
    Account account = getAccount();

    JSONObject accountValueJSON = null;
    JSONArray membersJSON = null;

    if (account != null)
    {
      accountValueJSON = account.getAccount(transactionId);

      if (accountValueJSON != null)
      {
        membersJSON = accountValueJSON.optJSONArray("members");
      }
    }

    if (membersJSON != null)
    {
      // check for eligibilityGroupId
      Long eligibilityGroupId = accountValueJSON.optLong("eligibilityGroupId");

      if (eligibilityGroupId != null)
      {
        consumerTierDataRequest.setEligibilityGroupId(eligibilityGroupId);
      }
      else
      {
        consumerTierDataRequest.setEligibilityGroupId(null);
      }

      // If AccountInfo exists and AccountInfo.value has member demographics
      // data, then populate the MemberData of the request using AccountInfo
      // member demographics data
      if (LOG.isDebugEnabled())
      {
        LOG.debug("product plan members: " + membersJSON.toString(4));
      }

      List<ShoppingMember> shoppingMembers = ConsumerDataConverterUtil
        .buildShoppingMembers(membersJSON);
      consumerTierDataRequest.setShoppingMembers(shoppingMembers);
    }
    else
    {
      consumerTierDataRequest.setEligibilityGroupId(null);

      // send no member data; this will result in the existing member data
      // being
      // returned from the consumer system
      consumerTierDataRequest.setMemberJSON(new JSONObject());
      // if it is null, set it to empty
      membersJSON = new JSONArray();
    }

    Set<String> selectedProductLines = ConsumerDataConverterUtil
      .determineSelectedProductLines(membersJSON);

    productPlanData = getProductPlanData(transactionId, productServerGroup, selectedProductLines);
  }

  public ProductPlanData getProductPlanData(
    String transactionId,
    String productServerGroup,
    Set<String> selectedProductLines) throws Exception
  {
    ProductPlanData productPlanData = null;

    consumerTierDataRequest.setTransactionId(transactionId);
    consumerTierDataRequest.setContext(ShoppingContext.EMPLOYEE);

    // Call to GPA to get the plan data
    DataProvider<ConsumerPlanData> consumerPlanDataProvider = new CamelRequestingDataProvider<ConsumerPlanData>(consumerTierDataRequest);
    
    ConsumerPlanData consumerProductsData = getCachedData(ShoppingDataProviderKey.CONSUMER_TIER,
      consumerPlanDataProvider);

    // we only want plans that have data in the consumer system;
    // ideally this shouldn't be necessary
    List<PlanData> resultPlans = new ArrayList<PlanData>();

    if (null != consumerProductsData.getPlans())
    {
      Map<String, ShoppingProduct> consumerProductsByExternalPlanId = new HashMap<String, ShoppingProduct>();

      for (ShoppingProduct planData : consumerProductsData.getPlans())
      {
        consumerProductsByExternalPlanId.put(planData.getExtRefId(), planData);
      }

      // get plans from product server based on consumerProduct external IDs
      List<String> planExtRefIds = new ArrayList<String>();
      planExtRefIds.addAll(consumerProductsByExternalPlanId.keySet());

      productDataRequest.setPlanExtRefIds(planExtRefIds);
      productDataRequest.setEffectiveDate(consumerProductsData.getPolicyEffectiveTime());
      productDataRequest.setGroup(productServerGroup);

      DataProvider<ProductPlanData> productPlanDataProvider = new CamelRequestingDataProvider<ProductPlanData>(productDataRequest);
      productPlanData = getCachedData(ShoppingDataProviderKey.PRODUCTS, productPlanDataProvider);

      if (null != productPlanData && null != productPlanData.getPlans())
      {
        // Merge ProductPlanData and GPAPlanData
        for (PlanData planData : productPlanData.getPlans())
        {
          String planProductLineCode = planData.getProductData().getProductLineCode();

          // check that the product line was selected for a member; otherwise
          // there is no
          // point in showing the product line's products
          if (selectedProductLines.contains(planProductLineCode))
          {
            ShoppingProduct employeePlan = consumerProductsByExternalPlanId.get(planData
              .getExtRefId());
            if (null != employeePlan)
            {
              planData.updateFrom(employeePlan);
              resultPlans.add(planData);
            }
            else
            {
              LOG
                .error("A product server plan (extRefId:"
                  + planData.getExtRefId()
                  + ") was found which was not present in consumer products. This should not happen.");
            }
          }
        }
      }
      else
      {
        LOG.error("No Product Plan Data");
      }
      
      productPlanData.setPlans(resultPlans);
      
      // set paychecks per year to help with cost calculation
      productPlanData.setPaychecksPerYear(consumerProductsData.getPaychecksPerYear());
    }

    return productPlanData;
  }

  public static JSONObject buildMonthlyCostJSON(PlanData planData) throws Exception
  {
    return buildCostJSON(planData, "monthly", null);
  }

  public static JSONObject buildPerPaycheckCostJSON(PlanData planData, Integer paychecksPerYear)
    throws Exception
  {
    return buildCostJSON(planData, "paycheck", paychecksPerYear);
  }

  private static JSONObject buildCostJSON(
    PlanData planData,
    String frequencyCode,
    Integer paychecksPerYear) throws Exception
  {
    // this method assumes plan rates are always MONTHLY,
    JSONObject costData = new JSONObject();

    if (planData.getPlanCost() != null)
    {
      Double costAmount = planData.getPlanCost();
      if ("paycheck".equalsIgnoreCase(frequencyCode))
      {
        costAmount = (costAmount * 12) / paychecksPerYear;
        costData.put("amount", costAmount);
      }
      else if ("monthly".equalsIgnoreCase(frequencyCode))
      {
        costData.put("amount", costAmount);
      }
      else
      {
        throw new Exception("Could not determine frequencyCode for plan cost: " + frequencyCode);
      }

      costData.put("frequencyCode", frequencyCode);
    }
    else
    {
      throw new Exception("Could not determine cost for consumer plan : " + planData.getExtRefId());
    }

    return costData;
  }

  public static JSONObject buildMonthlyRateJSON(PlanData planData) throws Exception
  {
    // TODO | constants
    return buildRateJSON(planData, "monthly", null);
  }

  public static JSONObject buildPerPaycheckRateJSON(PlanData planData, Integer paychecksPerYear)
    throws Exception
  {
    return buildRateJSON(planData, "paycheck", paychecksPerYear);
  }

  private static JSONObject buildRateJSON(
    PlanData planData,
    String frequencyCode,
    Integer paychecksPerYear) throws Exception
  {
    // this method assumes plan rates are always MONTHLY,
    JSONObject rateData = new JSONObject();

    if (planData.getRate() != null)
    {
      Double rateAmount = planData.getRate();
      if ("paycheck".equalsIgnoreCase(frequencyCode))
      {
        rateAmount = (rateAmount * 12) / paychecksPerYear;
        rateData.put("amount", rateAmount);
      }
      else if ("monthly".equalsIgnoreCase(frequencyCode))
      {
        rateData.put("amount", rateAmount);
      }
      else
      {
        throw new Exception("Could not determine frequencyCode for plan rate: " + frequencyCode);
      }

      rateData.put("frequencyCode", frequencyCode);
    }
    else
    {
      throw new Exception("Could not determine rate for consumer plan : " + planData.getExtRefId());
    }

    return rateData;
  }

  public static JSONObject buildMonthlyContributionJSON(PlanData planData) throws Exception
  {
    return buildContributionJSON(planData, "monthly", null);
  }

  public static JSONObject buildPerPaycheckContributionJSON(PlanData planData, int paychecksPerYear)
    throws Exception
  {
    return buildContributionJSON(planData, "paycheck", paychecksPerYear);
  }

  private static JSONObject buildContributionJSON(
    PlanData planData,
    String frequencyCode,
    Integer paychecksPerYear) throws Exception
  {
    JSONObject contributionData = new JSONObject();

    PlanContributionData planContributionData = planData.getContributionData();

    Double contributionAmount = planContributionData.getAmount();

    if ("paycheck".equalsIgnoreCase(frequencyCode))
    {
      contributionAmount = (contributionAmount * 12) / paychecksPerYear;
    }
    else if ("monthly".equalsIgnoreCase(frequencyCode))
    {
      // do nothing
    }
    else
    {
      throw new Exception("Could not determine contribution for consumer plan : "
        + planData.getExtRefId());
    }

    contributionData.put("amount", contributionAmount);
    contributionData.put("frequencyCode", frequencyCode);
    contributionData.put("typeCode", planContributionData.getTypeCode());

    return contributionData;
  }

  public static JSONObject buildCostJSON(PlanData planData, Integer paychecksPerYear)
    throws Exception
  {
    JSONObject costJSON = new JSONObject();

    // our goal here is to provide "monthly" and "per paycheck" values

    JSONObject monthlyCost = new JSONObject();
    monthlyCost.put("rate", buildMonthlyRateJSON(planData));
    monthlyCost.put("contribution", buildMonthlyContributionJSON(planData));
    monthlyCost.put("productCost", buildMonthlyCostJSON(planData));

    monthlyCost.put("memberLevelRated", planData.isMemberLevelRated());

    if (planData.isMemberLevelRated())
    {
      JSONObject memberLevelData = new JSONObject();

      // add perMember Rates
      memberLevelData.put("memberRates",
        RatingPlanDataConverter.buildMemberLevelRateJSON(planData, FrequencyCode.MONTHLY, paychecksPerYear));
      // add perType Contributions
      memberLevelData
        .put("memberTypeContributions", RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeContributions(), FrequencyCode.MONTHLY, paychecksPerYear));
      // add perType Costs
      memberLevelData.put("memberTypeCosts",
        RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeCosts(), FrequencyCode.MONTHLY, paychecksPerYear));
      
      monthlyCost.put("memberLevelRating", memberLevelData);
    }

    costJSON.put("monthly", monthlyCost);

    if (paychecksPerYear != null)
    {
      JSONObject perPaycheckCost = new JSONObject();
      perPaycheckCost.put("rate", buildPerPaycheckRateJSON(planData, paychecksPerYear));
      perPaycheckCost.put("contribution",
        buildPerPaycheckContributionJSON(planData, paychecksPerYear));
      perPaycheckCost.put("productCost", buildPerPaycheckCostJSON(planData, paychecksPerYear));

      perPaycheckCost.put("memberLevelRated", planData.isMemberLevelRated());

      if (planData.isMemberLevelRated())
      {
        JSONObject memberLevelData = new JSONObject();

        // add perMember Rates
        memberLevelData.put("memberRates",
          RatingPlanDataConverter.buildMemberLevelRateJSON(planData, FrequencyCode.PAYCHECK, paychecksPerYear));
        // add perType Contributions
        memberLevelData.put("memberTypeContributions", RatingPlanDataConverter
          .buildMemberTypeValueJSON(planData.getMemberTypeContributions(), FrequencyCode.PAYCHECK, paychecksPerYear));
        // add perType Costs
        memberLevelData.put("memberTypeCosts",
          RatingPlanDataConverter.buildMemberTypeValueJSON(planData.getMemberTypeCosts(), FrequencyCode.PAYCHECK, paychecksPerYear));

        perPaycheckCost.put("memberLevelRating", memberLevelData);
      }

      costJSON.put("perPaycheck", perPaycheckCost);
    }

    return costJSON;
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    // add product plan data
    if (productPlanData != null)
    {
      JSONArray planArray = new JSONArray();

      // This may come out null (valid), depending on how domain data was set up
      Integer paychecksPerYear = productPlanData.getPaychecksPerYear();

      JSONObject planConfig = new JSONObject();
      planConfig.put("hasPaycheckCost", paychecksPerYear != null);

      for (PlanData planData : productPlanData.getPlans())
      {
        // plan (empty)
        JSONObject planDataJSON = new JSONObject();

        // use internal code as ID, as this is how data will be configured
        // in other systems (plan advisor)
        planDataJSON.put("id", planData.getInternalCode()); // use in most cases

        planDataJSON.put("planId", planData.getIdentifier()); // needed for lead currently
        
        planDataJSON.put("name", planData.getPlanName());

        planDataJSON.put("tier", planData.getTierCode());

        planDataJSON.put("planType", planData.getPlanType());

        // create COST JSON

        planDataJSON.put("cost", buildCostJSON(planData, paychecksPerYear));

        // This is a temporary fix for first release
        // we need the premium for the sorting, we are going
        // to use the monthly rate because that will always be there or
        // should be there anyway :)
        if (planData.getRate() != null)
        {
          planDataJSON.put("premium", planData.getPlanCost());
        }

        // get networks as array
        JSONArray networksJSON = new JSONArray();
        for (String networkId : planData.getNetworks())
        {
          networksJSON.put(networkId);
        }
        planDataJSON.put("networks", networksJSON);

        // product line
        // TODO | change "productLine" to "productLineName" here and on UI
        planDataJSON.put("productLine", planData.getProductData().getProductLineName());
        planDataJSON.put("productLineCode", planData.getProductData().getProductLineCode());

        planDataJSON.put("benefitCategories", this.getPlanBenefitCategoryJSONArray(planData));

        // filterValues
        // construct filter values (flattened values), allowing us to filter
        // on anything regardless of JSON object structure

        Map<String, String> filterValuesMap = new HashMap<String, String>();
        filterValuesMap.putAll(this.getFilterValueMap(planData.getProductData()
          .getProductLineCode(), planData.getBenefitCategories()));

        // Now put each one of these directly on the plan. This will
        // allow us to easily access both benefit values and any other
        // property on the product automatically, without having hardcoded
        // properties
        for (Entry<String, String> entry : filterValuesMap.entrySet())
        {
          planDataJSON.put(entry.getKey(), entry.getValue());
        }

        planArray.put(planDataJSON);
      }

      jsonObject.put("plans", planArray);
      jsonObject.put("planConfig", planConfig);
    }

    return jsonObject;
  }

  public Map<String, String> getFilterValueMap(
    String productLineCode,
    List<PlanBenefitCategoryData> benefitCategoryDataList)
  {

    Map<String, String> filterValueMap = new HashMap<String, String>();

    for (PlanBenefitCategoryData planBenefitCategoryData : benefitCategoryDataList)
    {
      for (PlanBenefitData planBenefitData : planBenefitCategoryData.getBenefits())
      {
        String benefitInternalCode = planBenefitData.getInternalCode();

        String selectedValue = planBenefitData.getSelectedValue();
        PlanBenefitValueSetData benefitValueSetData = planBenefitData.getBenefitValueSetMap().get(
          selectedValue);

        Map<String, PlanBenefitValueData> selectedBenefitValueMap = benefitValueSetData
          .getBenefitValueMap();
        for (Map.Entry<String, PlanBenefitValueData> entry : selectedBenefitValueMap.entrySet())
        {
          PlanBenefitValueData benefitValueData = entry.getValue();

          String benefitValue = (benefitValueData.getNumericValue() != null) ? benefitValueData
            .getNumericValue().toString() : benefitValueData.getDisplayName();

          String keyValue = productLineCode + "." + benefitInternalCode + "." + entry.getKey();

          // add value
          filterValueMap.put(keyValue, benefitValue);
        }
      }
    }

    return filterValueMap;
  }

  public JSONArray getPlanBenefitCategoryJSONArray(PlanData planData) throws JSONException
  {
    JSONArray benefitCategories = new JSONArray();

    // build benefitCategoryMap
    for (PlanBenefitCategoryData benefitCategoryData : planData.getBenefitCategories())
    {
      // benefits
      JSONArray sortedBenefitArray = new JSONArray();

      // get benefits
      for (PlanBenefitData benefit : benefitCategoryData.getBenefits())
      {
        // identify benefit and get value
       // String selectedValue = benefit.getSelectedValue();
    	  //Changes for US-459
        Iterator it=benefit.getBenefitValueSetMap().entrySet().iterator();
        List<String> benefitValueKeys=new ArrayList<String>();
        while(it.hasNext()){
        	Map.Entry pairs = (Map.Entry)it.next();
        	benefitValueKeys.add(pairs.getKey().toString());
        }
        List<PlanBenefitValueSetData> benefitValueSetData =new ArrayList<PlanBenefitValueSetData>();
        List<String> benefitValueKeys1 = new ArrayList<String>();
        Map<String, PlanBenefitValueData> benefitValueMap =new HashMap<String, PlanBenefitValueData>();
        List<Map<String, PlanBenefitValueData>> benefitValueMapList= new ArrayList<Map<String,PlanBenefitValueData>>();
        for (String benefitValueKey : benefitValueKeys)
        {
        	benefitValueSetData.add(benefit.getBenefitValueSetMap().get(
        			benefitValueKey));
        	
        }
       
        for(PlanBenefitValueSetData planDataset :benefitValueSetData){
        	benefitValueKeys1.addAll(planDataset.getBenefitValueKeys());
        	benefitValueMap.putAll(planDataset.getBenefitValueMap());
        	benefitValueMapList.add(planDataset.getBenefitValueMap());
        }
      
        JSONArray benefitValues = new JSONArray();
    
        for (String benefitValueKey : benefitValueKeys1)
        {
         
          if(benefitCategoryData.getName().equalsIgnoreCase("Prescription Drug Coverage")){
        		for( int i=0; i<benefitValueMapList.size();i++){
        			JSONObject benefitJSON = new JSONObject();
        			PlanBenefitValueData planBenefit =benefitValueMapList.get(i).get(benefitValueKey);
        			benefitJSON.put("code", planBenefit.getBenefitValueTypeCode());
        	          benefitJSON.put("value", planBenefit.getDisplayName());
        	          benefitJSON.put("description", planBenefit.getDescription());

        	          // putOpt, may be null
        	          if (planBenefit.getNumericValue() != null)
        	          {
        	            benefitJSON.putOpt("numericValue", planBenefit.getNumericValue());
        	          }
        	         

        	          // We ignore the custom attributes (see CostEstimatorCostsWork)
        	          // since the Shopping UI doesn't utilize them yet

        	          benefitValues.put(benefitJSON);
        	         
        	          
        		}
        		break;
          }
          else{ 
          PlanBenefitValueData planBenefit = benefitValueMap.get(benefitValueKey);
          JSONObject benefitJSON = new JSONObject();
          benefitJSON.put("code", planBenefit.getBenefitValueTypeCode());
          benefitJSON.put("value", planBenefit.getDisplayName());
          benefitJSON.put("description", planBenefit.getDescription());

          // putOpt, may be null
          if (planBenefit.getNumericValue() != null)
          {
            benefitJSON.putOpt("numericValue", planBenefit.getNumericValue());
          }

          // We ignore the custom attributes (see CostEstimatorCostsWork)
          // since the Shopping UI doesn't utilize them yet

          benefitValues.put(benefitJSON);
        }
        }
        List<Object> obj=new ArrayList<Object>();
        if(benefitCategoryData.getName().equalsIgnoreCase("Prescription Drug Coverage")){
        if(benefitValues.length()>1){
        for(int j=0;j<benefitValues.length();j++){
      	 
      	  obj.add(benefitValues.get(j));
      	  
        }
        SortedMap map = new TreeMap();
        for(Object o : obj){
      	  JSONObject tmp = (JSONObject) o;
      	  
      	 if (tmp.has("numericValue")){
       
      		 map.put(tmp.get("numericValue"),tmp);
      	 }
           
        }
       
        if(null!=map&&map.size()>0){
        	 benefitValues=new  JSONArray();
        Set<Double> numbers = map.keySet();
       // JSONArray benefitValues1 = new JSONArray();
        for (double number : numbers) {
      	  benefitValues.put((map.get(number)));
        }
        }
        }
        }
        JSONObject benefitJSON = new JSONObject();
        benefitJSON.put("key", benefit.getInternalCode());
        benefitJSON.put("displayName", benefit.getDisplayName());
        benefitJSON.put("sortOrder", benefit.getSortOrder());
        
        benefitJSON.put("values", benefitValues);

        sortedBenefitArray.put(benefitJSON);
      }

      // build category object
      JSONObject benefitCategory = new JSONObject();
      benefitCategory.put("categoryName", benefitCategoryData.getName());
      benefitCategory.put("sortOrder", benefitCategoryData.getSortOrder());
      benefitCategory.put("benefits", sortedBenefitArray);

      // add category object to category array
      benefitCategories.put(benefitCategory);
    }

    return benefitCategories;
  }

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setConsumerTierDataRequest(ConsumerTierDataRequest consumerProductsDataRequest)
  {
    this.consumerTierDataRequest = consumerProductsDataRequest;
  }

  public void setProductDataRequest(ProductDataRequest productPlanDataRequest)
  {
    this.productDataRequest = productPlanDataRequest;
  }

  /**
   * @return the productServerGroup
   */
  public String getProductServerGroup()
  {
    return productServerGroup;
  }

  /**
   * @param productServerGroup the productServerGroup to set
   */
  public void setProductServerGroup(String productServerGroup)
  {
    this.productServerGroup = productServerGroup;
  }
}