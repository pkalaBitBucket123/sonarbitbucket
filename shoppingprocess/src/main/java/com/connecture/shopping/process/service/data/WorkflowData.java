package com.connecture.shopping.process.service.data;

import java.util.List;

public class WorkflowData
{
  private String workflowRefId;
  
  private String title;
  
  private String infoBlock;
  
  private Boolean defaultWorkflow;
  
  private String classOverride;
  
  private List<String> questionRefIds;

  public String getWorkflowRefId()
  {
    return workflowRefId;
  }

  public void setWorkflowRefId(String workflowRefId)
  {
    this.workflowRefId = workflowRefId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getInfoBlock()
  {
    return infoBlock;
  }

  public void setInfoBlock(String infoBlock)
  {
    this.infoBlock = infoBlock;
  }

  public List<String> getQuestionRefIds()
  {
    return questionRefIds;
  }

  public void setQuestionRefIds(List<String> questionRefIds)
  {
    this.questionRefIds = questionRefIds;
  }

  public Boolean getDefaultWorkflow()
  {
    return defaultWorkflow;
  }

  public void setDefaultWorkflow(Boolean defaultWorkflow)
  {
    this.defaultWorkflow = defaultWorkflow;
  }

  public String getClassOverride()
  {
    return classOverride;
  }

  public void setClassOverride(String classOverride)
  {
    this.classOverride = classOverride;
  }
}
