package com.connecture.shopping.process.service.stub;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.JSONFileUtils;

public class RatingPlanDataServiceStub
{
  private static Logger LOG = Logger.getLogger(RatingPlanDataServiceStub.class);

  public String getPlans(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "ratingPlanData.json");
  }

  public JSONObject getValidZipCodeCountyData(String zipCode)
  {
    JSONObject results = new JSONObject();
    
    try {
      results = new JSONObject(JSONFileUtils.readJSONData(getClass(), "isValidZipCodeCountyData.json"));
    }
    catch (JSONException e)
    {
      LOG.error("Failed to parse JSON", e);
    }

    return results;
  }

  public Boolean validateZipCode(String zipCode) throws Exception
  {
    // JSONObject returnObject = new JSONObject();
    // JSONArray errors = new JSONArray();
    //
    boolean isValid = false;
    // Just for testing
    if (!zipCode.startsWith("4"))
    {

      // JSONObject errorObject = new JSONObject();
      // errorObject.put("code", "invalidUSZip");
      // errorObject.put("message", zipCode + " is not a valid US Zip Code (stub).");
      // errors.put(errorObject);

      isValid = true;
    }

    // returnObject.put("hasErrors", hasErrors);
    // returnObject.put("errors", errors);
    return isValid;
  }

  public String getStateKeyForCountyAndZipCode(Integer countyId, String zipCode)
  {
    return zipCode.startsWith("90") ? "CA" : zipCode.startsWith("20") ? "DC" : zipCode
      .startsWith("96") ? "HI" : zipCode.startsWith("99") ? "AL" : "WI";
  }
}
