/**
 * 
 */
package com.connecture.shopping.process.message;

import java.io.File;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 */
public class MessageProcess
{
  private static Logger LOG = Logger.getLogger(MessageProcess.class);

  // configuration through properties
  private String smtpHost;
  private String hostKey;
  private String fromAddress;

  // velocity
  private VelocityContext velocityContext;
  private String fileStoreLocation;
  private String emailTemplateLocation;

  static
  {
    loadProperties();
  }

  protected static void loadProperties()
  {
    try
    {
      Properties props = new Properties();
      props.put("resource.loader", "file");
      props.put("file.resource.loader.class",
        "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
      props.put("file.resource.loader.path", "");
      props.put("file.resource.loader.cache", "false");
      Velocity.init(props);
    }
    catch (Exception e)
    {
      LOG.error("Failed to load properties", e);
    }
  }

  public String buildEmailPlanContent(JSONArray planDataArray, String costViewCode, String note)
    throws JSONException
  {
    // TODO | pass in "context" instead of building separate implementations

    List<PlanEmailBean> planDataList = this.convertData(planDataArray);
    StringWriter writer = new StringWriter();

    // create velocity context
    velocityContext = new VelocityContext();
    // add data to context
    velocityContext.put("planDataList", planDataList);
    velocityContext.put("costViewCode", costViewCode);
    velocityContext.put("note", note);

    String templateFileName = emailTemplateLocation + File.separator + "shoppingEmailTemplate.vt";
    templateFileName = fileStoreLocation.replace("file:/", "") + templateFileName;
    templateFileName = templateFileName.replaceAll("/", "\\\\");
    Template t = Velocity.getTemplate(templateFileName);

    // merge template and context
    t.merge(velocityContext, writer);

    String result = writer.toString();

    return result;
  }

  List<PlanEmailBean> convertData(JSONArray planDataArray) throws JSONException
  {
    List<PlanEmailBean> planDataList = new LinkedList<PlanEmailBean>();

    for (int i = 0; i < planDataArray.length(); i++)
    {
      JSONObject planDataJSON = planDataArray.getJSONObject(i);

      PlanEmailBean planEmailBean = PlanEmailConverterUtils.convertToPlanEmailBean(planDataJSON);
      planDataList.add(planEmailBean);
    }

    return planDataList;
  }

  public boolean sendEmail(MessageBean messageBean)
  {
    boolean sent = false;

    try
    {
      List<MessageRecipient> recipients = messageBean.getRecipients();

      String mailServer = smtpHost;

      Properties mailProperties = new Properties();
      mailProperties.setProperty(hostKey, mailServer);

      javax.mail.Session session = javax.mail.Session.getInstance(mailProperties, null);
      InternetAddress fromAddr = new InternetAddress(fromAddress);

      Message msg = new MimeMessage(session);
      msg.setFrom(fromAddr);
      if (recipients != null)
      {
        // eliminate duplicates
        Set<InternetAddress> toAddresses = new HashSet<InternetAddress>();

        for (MessageRecipient to : recipients)
        {
          if (StringUtils.isNotBlank(to.getEmail()))
          {
            toAddresses.add(new InternetAddress(to.getEmail()));
          }
        }

        if (!toAddresses.isEmpty())
        {
          // put unique set of addresses into a collection
          Address[] addresses = toAddresses.toArray(new Address[]{});
  
          msg.setRecipients(Message.RecipientType.TO, addresses);
          
          msg.setSubject(messageBean.getSubject());
          msg.setContent(messageBean.getContent(), "text/html");
          msg.setSentDate(new Date());

          if (LOG.isDebugEnabled())
          {
            LOG.debug("Sending email with content: " + msg.getContent());
          }
          
          msg.saveChanges();
          
          try
          {
            sendMessage(msg);
            sent = true;
          }
          catch (Exception e)
          {
            LOG.error("Failed sending message", e);
          }
        }
      }
      else {
        LOG.error("No recipients specified for email");
      }
    }
    catch (Exception e)
    {
      LOG.error("An error occurred while sending an email", e);
    }

    return sent;
  }

  protected void sendMessage(Message msg) throws Exception
  {
    Transport.send(msg);
  }

  /**
   * @param smtpHost the new value of smtpHost
   */
  public void setSmtpHost(String smtpHost)
  {
    this.smtpHost = smtpHost;
  }

  /**
   * @param hostKey the new value of hostKey
   */
  public void setHostKey(String hostKey)
  {
    this.hostKey = hostKey;
  }

  /**
   * @param fromAddress the new value of fromAddress
   */
  public void setFromAddress(String fromAddress)
  {
    this.fromAddress = fromAddress;
  }

  /**
   * @param fileStoreLocation the new value of fileStoreLocation
   */
  public void setFileStoreLocation(String fileStoreLocation)
  {
    this.fileStoreLocation = fileStoreLocation;
  }

  /**
   * @param emailTemplateLocation the new value of emailTemplateLocation
   */
  public void setEmailTemplateLocation(String emailTemplateLocation)
  {
    this.emailTemplateLocation = emailTemplateLocation;
  }

}