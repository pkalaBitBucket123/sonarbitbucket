package com.connecture.shopping.process.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class JSONFileUtils
{
  private static Logger LOG = Logger.getLogger(JSONFileUtils.class);
  
  public static String readJSONData(Class<?> owner, String fileName)
  {
    String jsonData = "{}";
    
    BufferedReader reader = null;
    try
    {
      InputStream is = owner.getResourceAsStream(fileName);
      if (null != is) {
        InputStreamReader isr = new InputStreamReader(is);
        reader = new BufferedReader(isr);
        StringBuffer data = new StringBuffer();
        String line;
        while((line = reader.readLine()) != null)
        {
          data.append(line);
        }
        jsonData = data.toString();
      }
      else 
      {
        LOG.error("Could not find file \"" + fileName + "\" for owner \"" + owner.getSimpleName() + "\"");
      }
    }
    catch (IOException e)
    {
      LOG.error("Failed to read " + fileName, e);
    }
    finally
    {
      if (null != reader)
      {
        try
        {
          reader.close();
        }
        catch (IOException e)
        {
          LOG.error("Failed to close " + fileName, e);
        }
      }
    }
    
    return jsonData;
  }
}
