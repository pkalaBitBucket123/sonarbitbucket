package com.connecture.shopping.process.service.data;

import java.util.Map;

public class MemberEligibilityData
{
  private String memberKey;
  private Map<String, Boolean> productLineEligibility;

  /**
   * @return the memberKey
   */
  public String getMemberKey()
  {
    return memberKey;
  }
  /**
   * @param memberKey the memberKey to set
   */
  public void setMemberKey(String memberKey)
  {
    this.memberKey = memberKey;
  }
  /**
   * @return the productLineEligibility
   */
  public Map<String, Boolean> getProductLineEligibility()
  {
    return productLineEligibility;
  }
  /**
   * @param productLineEligibility the productLineEligibility to set
   */
  public void setProductLineEligibility(Map<String, Boolean> productLineEligibility)
  {
    this.productLineEligibility = productLineEligibility;
  }
}
