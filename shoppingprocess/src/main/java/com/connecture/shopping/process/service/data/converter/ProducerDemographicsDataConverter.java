package com.connecture.shopping.process.service.data.converter;

import org.json.JSONObject;

import com.connecture.shopping.process.service.data.DataConverter;

/**
 * This class might be overkill for now
 * 
 * @author shopping dev
 *
 */
public class ProducerDemographicsDataConverter implements DataConverter<String, JSONObject>
{

  @Override
  public JSONObject convertData(String data) throws Exception
  {
    return new JSONObject(data);
  }

}
