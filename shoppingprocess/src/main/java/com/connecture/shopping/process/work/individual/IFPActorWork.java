package com.connecture.shopping.process.work.individual;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.core.ActorWork;

public class IFPActorWork extends ActorWork
{
  @Override
  public void get() throws Exception
  {
    actor = getActorData();
  }
  
  @Override
  public Actor getActorData() throws Exception
  {
    actorDataRequest.setContext(ShoppingContext.INDIVIDUAL);   
    
    DataProvider<Actor> dataProvider = new CamelRequestingDataProvider<Actor>(actorDataRequest);
    
    return getCachedData(ShoppingDataProviderKey.ACTOR, dataProvider);
  }
}
