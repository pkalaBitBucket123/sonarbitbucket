package com.connecture.shopping.process.service.data;

public class QuestionnaireData
{
  private String questionnaireRefId;
  
  private String title;
  
  private String header;
  
  private String classOverride;

  public String getQuestionnaireRefId()
  {
    return questionnaireRefId;
  }

  public void setQuestionnaireRefId(String questionnaireRefId)
  {
    this.questionnaireRefId = questionnaireRefId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getHeader()
  {
    return header;
  }

  public void setHeader(String header)
  {
    this.header = header;
  }

  public String getClassOverride()
  {
    return classOverride;
  }

  public void setClassOverride(String classOverride)
  {
    this.classOverride = classOverride;
  }
}
