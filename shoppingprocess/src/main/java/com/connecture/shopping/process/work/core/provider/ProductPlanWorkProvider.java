package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.ProductPlanWork;

public abstract class ProductPlanWorkProvider implements WorkProviderImpl<ProductPlanWork>
{
  @Override
  public ProductPlanWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject input,
    JSONObject data) throws JSONException
  {
    ProductPlanWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}
