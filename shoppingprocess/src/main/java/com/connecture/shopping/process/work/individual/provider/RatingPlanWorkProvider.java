package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.individual.RatingPlanWork;

public abstract class RatingPlanWorkProvider implements WorkProviderImpl<RatingPlanWork>
{
  @Override
  public RatingPlanWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject input,
    JSONObject data) throws JSONException
  {
    RatingPlanWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}
