package com.connecture.shopping.process.service.stub;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.connecture.model.api.ShoppingTierChangeProvider;
import com.connecture.model.data.MemberLevelRateData;
import com.connecture.model.data.ShoppingMember;
import com.connecture.model.data.ShoppingMemberEligibility;
import com.connecture.model.data.ShoppingProduct;
import com.connecture.model.data.ShoppingTierChangeData;
import com.connecture.model.integration.data.MemberTypeEnum;

public class ConsumerProductsClientStub implements ShoppingTierChangeProvider
{
  public ShoppingTierChangeData getTierChangeResults(
    String transactionId,
    Long groupId,
    Long eligibilityGroupId,
    List<ShoppingMember> members)
  {
    // return the object we want back from IA
    ShoppingTierChangeData resultObj = createConsumerShoppingData();
    return resultObj;
  }

  public static ShoppingTierChangeData createConsumerShoppingData()
  {
    ShoppingTierChangeData shoppingData = new ShoppingTierChangeData();

    shoppingData.setMemberEligibility(createEligibility());

    try
    {
      Date coverageEffectiveDate = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)
        .parse("02/20/2013");

      shoppingData.setPolicyEffectiveDate(coverageEffectiveDate);
    }
    catch (ParseException e)
    {
      // TODO | handle exception for this stub
    }

    shoppingData.setEligibilityGroupId(1111L);

    // test with paychecks per year
    shoppingData.setPaychecksPerYear(24.0);

    // test without paychecks per year
    // shoppingData.setPaychecksPerYear(null);

    shoppingData.setPlans(createPlans());

    return shoppingData;
  }

  private static List<ShoppingProduct> createPlans()
  {
    List<ShoppingProduct> plans = new ArrayList<ShoppingProduct>();

    // plan 1 : a composite rated plan

    ShoppingProduct plan1 = new ShoppingProduct();

    plan1.setExtRefId("PPO EH");
    plan1.setTierKey("FAM");
    plan1.setTierName("Family");

    plan1.setRate(200.50);
    plan1.setContribution(100.50);
    plan1.setCost(100.00);

    plans.add(plan1);

    // plan 2 : a member-level rated plan

    ShoppingProduct plan2 = new ShoppingProduct();

    MemberLevelRateData rateData2 = new MemberLevelRateData();

    Map<String, Double> memberRates = new HashMap<String, Double>();

    memberRates.put("0", 20.00);
    memberRates.put("1", 30.00);
    memberRates.put("2", 40.00);

    rateData2.setMemberRates(memberRates);

    // these don't get displayed individually but will be used to create a
    // contribution total
    Map<MemberTypeEnum, Double> employerContributions = new HashMap<MemberTypeEnum, Double>();
    employerContributions.put(MemberTypeEnum.Employee, 10.00);
    employerContributions.put(MemberTypeEnum.Spouse, 5.00);
    employerContributions.put(MemberTypeEnum.Dependent, 1.00);

    rateData2.setEmployerContributions(employerContributions);

    // these don't get displayed individually but will be used to create a cost
    // total
    Map<MemberTypeEnum, Double> memberCosts = new HashMap<MemberTypeEnum, Double>();
    memberCosts.put(MemberTypeEnum.Employee, 10.00);
    memberCosts.put(MemberTypeEnum.Spouse, 25.00);
    memberCosts.put(MemberTypeEnum.Dependent, 39.00);

    rateData2.setMemberCosts(memberCosts);

    plan2.setMemberLevelRateData(rateData2);
    
    plan2.setExtRefId("PPO Gold");
    plan2.setTierKey("FAM");
    plan2.setTierName("Family");

    plan2.setRate(90.00);
    plan2.setContribution(16.00);
    plan2.setCost(74.00);
    
    plans.add(plan2);

    return plans;
  }

  private static List<ShoppingMemberEligibility> createEligibility()
  {
    List<ShoppingMemberEligibility> eligibility = new ArrayList<ShoppingMemberEligibility>();

    ShoppingMemberEligibility member1Eligibility = new ShoppingMemberEligibility();
    member1Eligibility.setMemberKey("0");

    Set<String> member1EligibileProductLines = new HashSet<String>();
    member1EligibileProductLines.add("Medical");
    member1EligibileProductLines.add("Dental");
    member1EligibileProductLines.add("Vision");
    member1Eligibility.setEligibleProductLines(member1EligibileProductLines);
    member1Eligibility.setIneligibleProductLines(new HashSet<String>());

    eligibility.add(member1Eligibility);

    ShoppingMemberEligibility member2Eligibility = new ShoppingMemberEligibility();
    // member2Eligibility.setEligibile(true);
    member2Eligibility.setMemberKey("1");
    // member2Eligibility.setNotEligibleReason(null);
    Set<String> member2EligibileProductLines = new HashSet<String>();
    member2EligibileProductLines.add("Medical");
    member2EligibileProductLines.add("Dental");
    member2EligibileProductLines.add("Vision");
    member2Eligibility.setEligibleProductLines(member2EligibileProductLines);
    member2Eligibility.setIneligibleProductLines(new HashSet<String>());

    eligibility.add(member2Eligibility);

    ShoppingMemberEligibility member3Eligibility = new ShoppingMemberEligibility();
    // member3Eligibility.setEligibile(true);
    member3Eligibility.setMemberKey("2");

    Set<String> member3EligibileProductLines = new HashSet<String>();
    member3EligibileProductLines.add("Medical");
    member3EligibileProductLines.add("Dental");

    member3Eligibility.setEligibleProductLines(member3EligibileProductLines);

    Set<String> member3IneligibileProductLines = new HashSet<String>();
    member3IneligibileProductLines.add("Vision");
    member3Eligibility.setIneligibleProductLines(member3IneligibileProductLines);

    eligibility.add(member3Eligibility);

    // ShoppingMemberEligibility member4Eligibility = new
    // ShoppingMemberEligibility();
    // //member3Eligibility.setEligibile(true);
    // member4Eligibility.setMemberKey("3");
    //
    // Set<String> member4EligibileProductLines = new HashSet<String>();
    // member4EligibileProductLines.add("Medical");
    // member4EligibileProductLines.add("Dental");
    //
    // member4Eligibility.setEligibleProductLines(member4EligibileProductLines);
    //
    // Set<String> member4IneligibileProductLines = new HashSet<String>();
    // member4IneligibileProductLines.add("Vision");
    // member4Eligibility.setIneligibleProductLines(member4IneligibileProductLines);
    //
    // eligibility.add(member4Eligibility);

    return eligibility;
  }
}
