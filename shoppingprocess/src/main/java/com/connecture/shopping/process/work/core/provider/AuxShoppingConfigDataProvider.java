package com.connecture.shopping.process.work.core.provider;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public class AuxShoppingConfigDataProvider implements DataProvider<String> 
{
  @Override
  public String fetchData()
  {
    return JSONFileUtils.readJSONData(AuxShoppingConfigDataProvider.class, "auxShoppingConfig.json");
  }
}
