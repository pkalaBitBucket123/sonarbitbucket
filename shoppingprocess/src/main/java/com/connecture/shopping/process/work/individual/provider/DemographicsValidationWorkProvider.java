package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.individual.DemographicsValidationWork;

public abstract class DemographicsValidationWorkProvider
  implements WorkProviderImpl<DemographicsValidationWork>
{
  @Override
  public DemographicsValidationWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    DemographicsValidationWork work = createWork();

    try
    {
      String effectiveDate = params.getString("effectiveDate");
      work.setCoverageEffectiveDate(effectiveDate);
    }
    catch (JSONException e)
    {
      throw new JSONException("\"effectiveDate\" param must be specified");
    }

    try
    {
      String zipCode = params.getString("zipCode");
      work.setZipCode(zipCode);
    }
    catch (JSONException e)
    {
      throw new JSONException("\"zipCode\" param must be specified");
    }

    try
    {
      String eventDate = params.getString("eventDate");
      work.setEventDate(eventDate != null ? eventDate : "");
    }
    catch (JSONException e)
    {
      throw new JSONException("\"eventDate\" param must be specified");
    }

    try
    {
      String eventType = params.getString("eventType");
      work.setEventType(eventType != null ? eventType : "");
    }
    catch (JSONException e)
    {
      throw new JSONException("\"eventType\" param must be specified");
    }

    work.setInputJSON(data);

    return work;
  }
}