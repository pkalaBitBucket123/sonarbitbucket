package com.connecture.shopping.process.service.data.cache;

import java.util.Arrays;
import java.util.Iterator;

public enum ShoppingDataProviderKey implements DataProviderKey
{
  ACTOR("actor"),
  AVAILABILITY("availability"),
  COST_ESTIMATOR_KEYS("costEstimatorKeys"),
  COST_ESTIMATOR_SETTINGS("costEstimatorSettings"),
  AUX_SHOPPING_CONFIG("auxShoppingConfig"),
  COST_ESTIMATOR_COSTS("costEstimatorCosts"),
  CONSUMER_DEMOGRAPHICS("comsumerDemographics"),
  CONSUMER_TIER("consumerTier"),
  PRODUCTS("products"),
  RATING_PRODUCTS("ratingProducts"),
  PLAN_ADVISOR("planAdvisor"),
  DEMOGRAPHICS_CONFIG("deomgraphicsConfig"),
  DEMOGRAPHICS_VALIDATION("demographicsValidation"),
  VALID_ZIP_CODE_COUNTY_DATA("validZipCodeCountyData"), 
  EFFECTIVE_DATE_DATA("effectiveDateData"),
  SPECIAL_ENROLLMENT_REASONS("specialEnrollmentReasons"),
  SPECIAL_ENROLLMENT_EFFECTIVE_DATE("specialEnrollmentEffectiveDate"),
  SPECIAL_ENROLLMENT_EVENT_DATE_VALIDATION("specialEnrollmentEventDateValidation"),
  ZIP_CODE_VALIDATION("zipCodeValidation"),
  POVERTY_INCOME_LEVEL("proveryIncomeLevel"),
  INDIVIDUAL_DATA("individualData"), 
  PRODUCER("producer"), 
  PROVIDER_SEARCH("providerSearch"),
  APTC_DATA("aptcData"),
  SUBSIDY_USAGE("subsidyUsage");
  
  private String key;

  private ShoppingDataProviderKey(String key) 
  {
    this.key = key;
  }
  
  public static ShoppingDataProviderKey findByKey(String key)
  {
    ShoppingDataProviderKey result = null;
    for (Iterator<ShoppingDataProviderKey> iter = Arrays.asList(values()).iterator(); null == result && iter.hasNext();)
    {
      ShoppingDataProviderKey dataProviderKey = iter.next();
      if (dataProviderKey.key.equals(key))
      {
        result = dataProviderKey;
      }
    }
    return result;
  }
}
