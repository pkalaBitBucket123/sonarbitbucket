/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

import java.util.List;

/**
 * 
 */
public class PlanBenefitCategoryData
{
  private String name;
  private List<PlanBenefitData> benefits;
 
  private int sortOrder;
  
  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }
  /**
   * @param name the new value of name
   */
  public void setName(String name)
  {
    this.name = name;
  }
  
  /**
   * @return the benefits
   */
  public List<PlanBenefitData> getBenefits()
  {
    return benefits;
  }
  /**
   * @param benefits the new value of benefits
   */
  public void setBenefits(List<PlanBenefitData> benefits)
  {
    this.benefits = benefits;
  }

  /**
   * @return the sortOrder
   */
  public int getSortOrder()
  {
    return sortOrder;
  }
  /**
   * @param sortOrder the new value of sortOrder
   */
  public void setSortOrder(int sortOrder)
  {
    this.sortOrder = sortOrder;
  }
}