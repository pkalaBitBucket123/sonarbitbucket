package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.shopping.process.service.data.PlanAdvisorData;

public class PlanAdvisorConfigRequest extends BaseCamelRequest<PlanAdvisorData>
{
  @Produce(uri = "{{camel.shopping.producer.paConfig}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setGroupId(String groupId)
  {
    headers.put("groupId", groupId);
  }

  public void setEffectiveDate(long effectiveDate)
  {
    headers.put("effDateMs", effectiveDate);
  }
}
