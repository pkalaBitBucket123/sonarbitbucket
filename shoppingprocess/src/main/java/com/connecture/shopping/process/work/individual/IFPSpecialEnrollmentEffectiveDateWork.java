package com.connecture.shopping.process.work.individual;

import java.text.SimpleDateFormat;

import org.json.JSONObject;

import com.connecture.shopping.process.service.data.EnrollmentEventApplication;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentData;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class IFPSpecialEnrollmentEffectiveDateWork extends WorkImpl
{
  // Start Required Inputs
  private EnrollmentEventRulesEngineBean hcrSpecialEnrollmentEffectiveDateRulesEngineBean;
  private String eventReason;
  private String eventDate;
  // End Required Inputs

  private EnrollmentEventDomainModel specialEnrollmentEffectiveDateDomain;
  
  @Override
  public void get() throws Exception
  {
    EnrollmentEventDomainModel domainModel = new EnrollmentEventDomainModel();
    EnrollmentEventApplication application = new EnrollmentEventApplication();
    SpecialEnrollmentData specialEnrollment = new SpecialEnrollmentData();
    specialEnrollment.setSelectedSpecialEnrollmentReason(eventReason);
    specialEnrollment.setEventDate(eventDate);
    application.setSpecialEnrollment(specialEnrollment);
    domainModel.setApplication(application);
    
    hcrSpecialEnrollmentEffectiveDateRulesEngineBean.setDomainModel(domainModel);
    hcrSpecialEnrollmentEffectiveDateRulesEngineBean.setDataCache(getDataCache());
    specialEnrollmentEffectiveDateDomain = hcrSpecialEnrollmentEffectiveDateRulesEngineBean.executeRule();
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject results = new JSONObject();
    
    JSONObject enrollmentEffectiveDateModel = new JSONObject();
    String effectiveDate = specialEnrollmentEffectiveDateDomain.getApplication().getEffectiveDate();
    Long effectiveDateMs = new SimpleDateFormat("MM/dd/yyyy").parse(effectiveDate).getTime();
    enrollmentEffectiveDateModel.put("effectiveDate", effectiveDate);
    enrollmentEffectiveDateModel.put("effectiveMs", effectiveDateMs);
    results.put("enrollmentEffectiveDate", enrollmentEffectiveDateModel);
    
    return results;
  }
  
  public void setEventReason(String eventReason)
  {
    this.eventReason = eventReason;
  }

  public void setEventDate(String eventDate)
  {
    this.eventDate = eventDate;
  }

  public void setHcrSpecialEnrollmentEffectiveDateRulesEngineBean(
    EnrollmentEventRulesEngineBean hcrSpecialEnrollmentEffectiveDateRulesEngineBean)
  {
    this.hcrSpecialEnrollmentEffectiveDateRulesEngineBean = hcrSpecialEnrollmentEffectiveDateRulesEngineBean;
  }
}
