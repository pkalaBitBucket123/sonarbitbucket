package com.connecture.shopping.process.work.core;

import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.ValidZipCodeCountyDataRequest;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;

public class ValidZipCodeCountyWork extends WorkImpl
{
  // Start Required Inputs
  private ValidZipCodeCountyDataRequest validZipCodeCountyDataRequest;
  // End Required Inputs
  
  private JSONObject validZipCodeCountyDataJSONObject;
  private String zipCode;

  @Override
  public void get() throws Exception
  {
    DataProvider<JSONObject> dataProvider = new CamelRequestingDataProvider<JSONObject>(validZipCodeCountyDataRequest);
    validZipCodeCountyDataRequest.setZipCode(zipCode);

    // Get the ZipCode results, includes validation and multiCountyZipCodes, if any.
    validZipCodeCountyDataJSONObject = getCachedData(
      ShoppingDataProviderKey.VALID_ZIP_CODE_COUNTY_DATA, dataProvider);
  }

  @Override
  public JSONObject processData() throws Exception
  {
    // return object
    JSONObject jsonObject = new JSONObject();

    jsonObject.put("validZipCodeCountyData", validZipCodeCountyDataJSONObject);

    return jsonObject;
  }

  public void setValidZipCodeCountyDataRequest(
    ValidZipCodeCountyDataRequest validZipCodeCountyDataRequest)
  {
    this.validZipCodeCountyDataRequest = validZipCodeCountyDataRequest;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
}
