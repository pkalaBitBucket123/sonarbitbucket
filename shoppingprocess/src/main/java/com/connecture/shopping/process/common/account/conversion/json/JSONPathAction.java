package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONException;
import org.json.JSONObject;

public interface JSONPathAction
{
  enum PathState {
    MISSING, FOUND, MISSING_SUBPATH;
  }
  
  boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException;
  
  static class JSONPathCountAction implements JSONPathAction
  {
    @Override
    public boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException
    {
      return isValidFor(state);
    }

    @Override
    public boolean isValidFor(PathState state)
    {
      return PathState.FOUND.equals(state);
    }
  }

  boolean isValidFor(PathState state);
}
