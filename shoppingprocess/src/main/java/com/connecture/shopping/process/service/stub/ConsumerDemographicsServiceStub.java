package com.connecture.shopping.process.service.stub;

import com.connecture.shopping.process.common.JSONFileUtils;

public class ConsumerDemographicsServiceStub
{
  public String getConsumerDemographics(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "current-consumerDemographicsData.json");
  }
}
