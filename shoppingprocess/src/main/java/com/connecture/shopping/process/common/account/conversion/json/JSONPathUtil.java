package com.connecture.shopping.process.common.account.conversion.json;

import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class JSONPathUtil
{
  private static final Pattern pattern;

  private static final String WILDCARD = "*";

  static
  {
    synchronized (JSONPathUtil.class)
    {
      pattern = Pattern.compile("^(([a-zA-Z0-9_*]+)\\.*)*$");
    }
  }

  public static boolean isValid(String path)
  {
    return pattern.matcher(path).matches();
  }

  /**
   * Returns the count of the number of matches of the specified path in the
   * specified jsonObject.
   * 
   * @param jsonObject the JSONObject that is having paths counted
   * @param path the path to count instances of
   * @return the count of path matches in the jsonObject
   * @throws JSONException
   */
  public static int count(JSONObject jsonObject, String path) throws JSONException
  {
    return performPathAction(jsonObject, path, new JSONPathAction.JSONPathCountAction());
  }

  public static int remove(JSONObject jsonObject, String path) throws JSONException
  {
    return performPathAction(jsonObject, path, new JSONPathRemoveAction());
  }

  public static int add(JSONObject jsonObject, String path) throws JSONException
  {
    return performPathAction(jsonObject, path, new JSONPathAddAction());
  }

  public static int objectToArray(JSONObject jsonObject, String path, String rename) throws JSONException
  {
    JSONPathObjectToArrayAction objToArrayAction = new JSONPathObjectToArrayAction(rename);  
    return performPathAction(jsonObject, path, objToArrayAction);
  }

  public static int add(JSONObject jsonObject, String path, Object defaultValue)
    throws JSONException
  {
    return performPathAction(jsonObject, path, new JSONPathAddAction(defaultValue));
  }

  public static int rename(JSONObject jsonObject, String oldPath, String newPath)
    throws JSONException
  {
    JSONPathBulkRemoveAction removeAction = new JSONPathBulkRemoveAction();
    int count = performPathAction(jsonObject, oldPath, removeAction);
    count += performPathAction(jsonObject, newPath,
      new JSONPathBulkAddAction(removeAction.getAllRemoved()));
    return count;
  }

  /**
   * Performs one of the supported JSONPathAction on the specified jsonObject
   * given the specified path.
   * 
   * @param jsonObject target of the action
   * @param path path for the action
   * @param action the JSONPathAction to be performed
   * @return the number of times the action was performed
   * @throws JSONException
   */
  static int performPathAction(JSONObject jsonObject, String path, JSONPathAction action)
    throws JSONException
  {
    int count = jsonObject.has(path) ? 1 : 0;

    if (count == 0)
    {
      int separatorIndex = path.indexOf('.');
      if (separatorIndex > 0)
      {
        String subPath = path.substring(0, separatorIndex);
        if (jsonObject.has(subPath))
        {
          count = performSubPathAction(jsonObject, path, action, separatorIndex, subPath);
        }
        else if (WILDCARD.equals(subPath))
        {
          count = performSubPathAction(jsonObject, path, action, separatorIndex, subPath);
        }
        else
        {
          // Subpath missing, check if we can add it
          // Most likely adding a new JSONObject so we can continue along the
          // path
          if (action.perform(subPath, jsonObject, JSONPathAction.PathState.MISSING_SUBPATH))
          {
            // Continue processing from the rest of the subPath
            count = performSubPathAction(jsonObject, path, action, separatorIndex, subPath);
          }
        }
      }
      else
      {
        if (action.perform(path, jsonObject, JSONPathAction.PathState.MISSING))
        {
          // Count the Missing action
          count++;
        }
      }
    }
    else
    {
      action.perform(path, jsonObject, JSONPathAction.PathState.FOUND);
      // This has already been counted, i.e. count == 1
    }

    return count;
  }

  private static int performSubPathAction(
    JSONObject jsonObject,
    String path,
    JSONPathAction action,
    int separatorIndex,
    String subPath) throws JSONException
  {
    int count = 0;
    String newPath = path.substring(separatorIndex + 1);

    if (WILDCARD.equals(subPath))
    {
      // find properties on the current object, if any exist
      if (jsonObject.names() != null && jsonObject.names().length() > 0)
      {
        for (String objectName : JSONObject.getNames(jsonObject))
        {
          JSONObject newObject = jsonObject.getJSONObject(objectName);
          count += performPathAction(newObject, newPath, action);
        }
      }
    }
    else if (jsonObject.get(subPath) instanceof JSONObject)
    {
      JSONObject newObject = jsonObject.getJSONObject(subPath);
      count = performPathAction(newObject, newPath, action);
    }
    else if (jsonObject.get(subPath) instanceof JSONArray)
    {
      JSONArray values = jsonObject.getJSONArray(subPath);
      for (int i = 0; i < values.length(); i++)
      {
        JSONObject newObject = values.getJSONObject(i);
        count += performPathAction(newObject, newPath, action);
      }
    }
    return count;
  }
}
