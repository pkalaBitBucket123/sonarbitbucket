package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.integration.data.ShoppingToIAData;

public class IAShoppingAnonymousEnrollmentDataRequest extends BaseCamelRequest<ShoppingToIAData>
{
  @Produce(uri="{{camel.shopping.producer.anonymousEnrollmentData}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
}
