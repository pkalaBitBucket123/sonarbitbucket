package com.connecture.shopping.process.service.data;

import java.util.List;

public class ProvidersData
{
  private List<ProviderData> providers;

  public List<ProviderData> getProviders()
  {
    return providers;
  }

  public void setProviders(List<ProviderData> providers)
  {
    this.providers = providers;
  }

  
}
