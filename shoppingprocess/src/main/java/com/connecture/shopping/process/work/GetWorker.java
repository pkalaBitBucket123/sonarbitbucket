/**
 * 
 */
package com.connecture.shopping.process.work;

import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;


/**
 * 
 */
public class GetWorker implements Runnable
{
  private static Logger LOG = Logger.getLogger(GetWorker.class);
  private CountDownLatch sync;
  private Work work;

  public GetWorker(CountDownLatch sync, Work work)
  {
    this.sync = sync;
    this.work = work;
  }

  public void run()
  {
    try
    {
      work.get();
    }
    catch (Exception e)
    {
      LOG.error("Work " + work.getKey() + " failed", e);
    }
    finally
    {
      sync.countDown();
    }
  }
}
