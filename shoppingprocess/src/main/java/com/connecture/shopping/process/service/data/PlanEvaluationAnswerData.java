package com.connecture.shopping.process.service.data;

import java.util.List;

public class PlanEvaluationAnswerData
{
  private String answerRefId;
  private List<PlanEvaluationProductData> products;

  public String getAnswerRefId()
  {
    return answerRefId;
  }

  public void setAnswerRefId(String answerRefId)
  {
    this.answerRefId = answerRefId;
  }

  public List<PlanEvaluationProductData> getProducts()
  {
    return products;
  }

  public void setProducts(List<PlanEvaluationProductData> products)
  {
    this.products = products;
  }

}
