package com.connecture.shopping.process.service.data;

public class EmployerData
{
  private String employerName;
  private String employerLogoPath;
  
  /**
   * @return the employerName
   */
  public String getEmployerName()
  {
    return employerName;
  }
  /**
   * @param employerName the employerName to set
   */
  public void setEmployerName(String employerName)
  {
    this.employerName = employerName;
  }
  /**
   * @return the employerLogoPath
   */
  public String getEmployerLogoPath()
  {
    return employerLogoPath;
  }
  /**
   * @param employerLogoPath the employerLogoPath to set
   */
  public void setEmployerLogoPath(String employerLogoPath)
  {
    this.employerLogoPath = employerLogoPath;
  }
}