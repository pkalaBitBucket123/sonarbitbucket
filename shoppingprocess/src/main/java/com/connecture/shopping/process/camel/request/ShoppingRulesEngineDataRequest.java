package com.connecture.shopping.process.camel.request;

import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.converter.ShoppingRulesEngineDataConverter;

public class ShoppingRulesEngineDataRequest extends RulesEngineDataRequest<ShoppingDomainModel>
{
  @Override
  public ShoppingDomainModel convertData(Object data) throws Exception
  {
    return new ShoppingRulesEngineDataConverter().convertData((String)data);
  }
}
