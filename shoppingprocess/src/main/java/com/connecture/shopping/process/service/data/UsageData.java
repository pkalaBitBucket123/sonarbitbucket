package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class UsageData
{
  private String key;
  private String display;

  List<ConsumerUsage> consumer = new ArrayList<ConsumerUsage>();

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public String getDisplay()
  {
    return display;
  }

  public void setDisplay(String display)
  {
    this.display = display;
  }

  public List<ConsumerUsage> getConsumer()
  {
    return consumer;
  }

  public void setConsumer(List<ConsumerUsage> consumer)
  {
    this.consumer = consumer;
  }

}
