package com.connecture.shopping.process.work.core;

import com.connecture.model.data.rules.aptc.APTCDomain;

public class APTCRulesEngineBean extends RulesEngineBean<APTCDomain>
{
  @Override
  protected APTCDomain createDefaultDomainModel()
  {
    return new APTCDomain();
  }
}
