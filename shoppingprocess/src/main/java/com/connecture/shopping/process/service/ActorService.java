package com.connecture.shopping.process.service;

import com.connecture.model.data.Actor;

public interface ActorService
{
  public Actor getActorData() throws Exception;
}
