package com.connecture.shopping.process.work.core;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.ShoppingRulesEngineDataRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.Profile;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.provider.CostEstimatorSettingsDataProvider;

public class CostEstimatorWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(CostEstimatorWork.class);

  // Start Required Inputs
  private ShoppingRulesEngineDataRequest rulesEngineDataRequest;
  private String rulesEngineGroupId;
  private String costEstimatorSettingsRuleSet;
  private CostEstimatorSettingsDataProvider costEstimatorSettingsDataProvider;
  private String transactionId;
  // End Required Inputs
  
  private ShoppingDomainModel rulesEngineResult;

  @Override
  public void get() throws Exception
  {
    Account account = getAccount();

    if (null != account)
    {
      JSONObject accountObject = account.getAccount(transactionId);

      if (accountObject.has("effectiveDate"))
      {
        Long effectiveDateMs = ShoppingDateUtils.stringToDate(accountObject.getString("effectiveDate")).getTime();
        
        costEstimatorSettingsDataProvider.setRuleSet(costEstimatorSettingsRuleSet);
        costEstimatorSettingsDataProvider.setRulesEngineGroupId(rulesEngineGroupId);
        costEstimatorSettingsDataProvider.setRequest(rulesEngineDataRequest);
        
        ShoppingDomainModel domainModel = new ShoppingDomainModel();
        costEstimatorSettingsDataProvider.setRulesEngineDomain(domainModel);
        
        costEstimatorSettingsDataProvider.setEffectiveDateMs(effectiveDateMs);
        costEstimatorSettingsDataProvider.setTransactionId(transactionId);
        costEstimatorSettingsDataProvider.setAccount(accountObject);
        rulesEngineResult = getCachedData(ShoppingDataProviderKey.COST_ESTIMATOR_SETTINGS,
          costEstimatorSettingsDataProvider);
      }
    }
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();
    
    JSONObject costEstimatorObject = new JSONObject();

    // If there was no AccountInfo yet there won't be a RulesEngine result
    // either
    if (null != rulesEngineResult)
    {
      if (StringUtils.isBlank(rulesEngineResult.getRequest().getError()))
      {
        JSONArray profileArray = new JSONArray();

        for (Profile profile : rulesEngineResult.getRequest().getProfile())
        {
          profileArray.put(new JSONObject(profile));
        }
        costEstimatorObject.put("profile", profileArray);
      }
      else
      {
        LOG.error("Unable to obtain profile settings from RulesEngine. Cause: "
          + rulesEngineResult.getRequest().getError());
      }
    }

    if (LOG.isDebugEnabled())
    {
      LOG.debug("CostEstimatorConfig\n" + costEstimatorObject.toString(4));
    }

    jsonObject.put("costEstimatorConfig", costEstimatorObject);

    return jsonObject;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setRulesEngineDataRequest(ShoppingRulesEngineDataRequest rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }

  public void setCostEstimatorSettingsRuleSet(String costEstimatorSettingsRuleSet)
  {
    this.costEstimatorSettingsRuleSet = costEstimatorSettingsRuleSet;
  }

  public void setCostEstimatorSettingsDataProvider(
    CostEstimatorSettingsDataProvider costEstimatorSettingsDataProvider)
  {
    this.costEstimatorSettingsDataProvider = costEstimatorSettingsDataProvider;
  }

  public void setRulesEngineGroupId(String rulesEngineGroupId)
  {
    this.rulesEngineGroupId = rulesEngineGroupId;
  }
}
