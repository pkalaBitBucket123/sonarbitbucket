package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public interface AVPConverterUtil<T extends AVPConverter>
{
  List<String> validateJSON(JSONObject converterObject) throws JSONException;

  T createConverter(JSONObject converterObject)  throws JSONException;
}
