package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueData;

public class ProductDataConverterUtil
{
  // client-side strings; tedious but safer this way.

  // root-level attributes
  private static final String PLAN_ID = "planId"; // product server plan id
                                                  // (long)
  private static final String INTERNAL_CODE = "id"; // yes, even though we use
                                                    // it as
  // id
  private static final String PLAN_TYPE = "planType"; // PPO, HMO, DENTAL; stuff
                                                      // like
  // that.
  private static final String PLAN_NAME = "name";
  private static final String METAL_TIER = "metalTier";
  private static final String PRODUCT_LINE_NAME = "productLine"; // TODO | fix
                                                                 // this;
  // why can't you be
  // more like code?
  private static final String PRODUCT_LINE_CODE = "productLineCode";

  // cost-level attributes
  private static final String COST = "cost";
  private static final String MONTHLY = "monthly";
  private static final String RATE = "rate";
  private static final String MEMBER_LEVEL_RATED = "memberLevelRated";
  private static final String MEMBER_LEVEL_RATING = "memberLevelRating";
  private static final String MEMBER_RATES = "memberRates";
  private static final String MEMBER_ID = "memberId";
  private static final String AMOUNT = "amount";
  private static final String TOBACCO_SURCHARGE = "tobaccoSurcharge";

  // networks
  private static final String NETWORKS = "networks";

  // benefits
  private static final String SORT_ORDER = "sortOrder";
  // benefits (benefit category)
  private static final String BENEFIT_CATEGORIES = "benefitCategories";
  private static final String CATEGORY_NAME = "categoryName";
  private static final String BENEFITS = "benefits";
  // benefits (benefit)
  private static final String KEY = "key";
  private static final String DISPLAY_NAME = "displayName";
  private static final String VALUES = "values";
  // benefits (benefit value)
   static final String CODE = "code";
  private static final String VALUE = "value";
  private static final String NUMERIC_VALUE = "numericValue";
  private static final String DESCRIPTION = "description";

  public static List<PlanData> convertFromShoppingPlansJSON(JSONArray plansJSON)
    throws JSONException
  {
    List<PlanData> plans = new ArrayList<PlanData>();

    for (int i = 0; i < plansJSON.length(); i++)
    {
      JSONObject planJSON = plansJSON.getJSONObject(i);
      plans.add(convertFromShoppingPlanJSON(planJSON));
    }

    return plans;
  }

  /**
   * Create the Shopping <code>PlanData</code> object from client-side plan
   * data. Potentially used for outgoing plan data (rules calls, enrollment).
   * 
   * @return PlanData
   */
  public static PlanData convertFromShoppingPlanJSON(JSONObject planJSON) throws JSONException
  {
    PlanData planData = new PlanData();

    // root level attributes (required)
    planData.setIdentifier(planJSON.optString(PLAN_ID, null)); 
    // TODO | should be Long, but works for now
    
    planData.setInternalCode(planJSON.optString(INTERNAL_CODE, null)); // "id"
    planData.setPlanType(planJSON.optString(PLAN_TYPE, null));
    planData.setPlanName(planJSON.optString(PLAN_NAME, null));

    // root level attributes (optional)
    planData.setMetalTier(planJSON.optString(METAL_TIER, null));

    // product line data... seems wrong, doesn't it?
    ProductData productData = new ProductData();
    productData.setProductLineName(planJSON.optString(PRODUCT_LINE_NAME, null));
    productData.setProductLineCode(planJSON.optString(PRODUCT_LINE_CODE, null));
    planData.setProductData(productData);

    // populate cost data
    populateCostFromShoppingPlanJSON(planData, planJSON);

    // networks
    populateNetworksFromShoppingPlanJSON(planData, planJSON);

    // benefits (benefitCategories)
    populateBenefitCategoriesFromShoppingPlanJSON(planData, planJSON);

    return planData;
  }

  static void populateNetworksFromShoppingPlanJSON(PlanData planData, JSONObject planJSON)
    throws JSONException
  {
    List<String> networks = new ArrayList<String>();

    if (planJSON.has(NETWORKS))
    {
      JSONArray networksJSON = planJSON.getJSONArray(NETWORKS);
      for (int i = 0; i < networksJSON.length(); i++)
      {
        networks.add(networksJSON.getString(i));
      }
    }
    
    planData.setNetworks(networks);
  }

  // TODO | ultimately it would be nice to have a Cost Object on the PlanData
  // Object; i.e. I think
  // it would be better if the planData object more closely resembled the plan
  // JSON
  static void populateCostFromShoppingPlanJSON(PlanData planData, JSONObject planJSON)
    throws JSONException
  {
    if (planJSON.has(COST))
    {
      JSONObject costJSON = planJSON.getJSONObject(COST);
      
      if (costJSON.has(MONTHLY))
      {
        // monthly cost should exist for all Shopping plans; paycheck cost is
        // derived FROM monthly cost
        JSONObject monthlyCostJSON = costJSON.getJSONObject(MONTHLY);
        
        if (monthlyCostJSON.has(RATE))
        {
          // rate should always exist
          JSONObject monthlyRateJSON = monthlyCostJSON.getJSONObject(RATE);
          // TODO | frequencyCode is on client side, but may not be used? investigate.
          // Don't think we need it here, anyway.
          planData.setRate(monthlyRateJSON.getDouble(AMOUNT));
        }
        
        planData.setTobaccoSurcharge(monthlyCostJSON.getDouble(TOBACCO_SURCHARGE));
    
        planData.setMemberLevelRated(monthlyCostJSON.optBoolean(MEMBER_LEVEL_RATED));
    
        if (planData.isMemberLevelRated())
        {
          // TODO | member level rated data is a little different depending on
          // context; override?
          // no need to right now, we don't convert to object for EE
          populateMLRFromShoppingPlanMonthlyCostJSON(planData, monthlyCostJSON);
        }
      }
    }
  }

  // TODO | EE has additional properties, but GPA doesn't use an object for
  // enrollment integration at the moment,
  // so those additional properties go straight from our accountJSON to the
  // enrollment JSON anyway :/
  static void populateMLRFromShoppingPlanMonthlyCostJSON(
    PlanData planData,
    JSONObject monthlyCostJSON) throws JSONException
  {
    Map<String, String> memberLevelRates = new HashMap<String, String>();
    Map<String, String> memberTobaccoSurcharges = new HashMap<String, String>();
    
    if (monthlyCostJSON.has(MEMBER_LEVEL_RATING))
    {
      JSONObject mlrRatingJSON = monthlyCostJSON.getJSONObject(MEMBER_LEVEL_RATING);
  
      if (mlrRatingJSON.has(MEMBER_RATES))
      {
        // get rates
        JSONArray mlrRatesJSON = mlrRatingJSON.getJSONArray(MEMBER_RATES);
    
        for (int i = 0; i < mlrRatesJSON.length(); i++)
        {
          JSONObject mlrRateJSON = mlrRatesJSON.getJSONObject(i);
          String memberId = mlrRateJSON.getString(MEMBER_ID);
          String memberAmount = mlrRateJSON.getString(AMOUNT);
          // TODO | memberAmount may not be a number, e.g. "included".. how do we
          // handle this out-going?
          // currently, IA re-rates in these cases, but what do we do for EE? Looks
          // like we just throws back
          // whatever was stored in that mapping as its string value..
    
          String tobaccoSurcharge = mlrRateJSON.optString(TOBACCO_SURCHARGE, "0");
          memberTobaccoSurcharges.put(memberId, tobaccoSurcharge);
    
          memberLevelRates.put(memberId, memberAmount);
        }
      }
    }
    
    planData.setMemberLevelRates(memberLevelRates);

    planData.setMemberLevelTobaccoSurcharges(memberTobaccoSurcharges);

  }

  static void populateBenefitCategoriesFromShoppingPlanJSON(
    PlanData planData,
    JSONObject planJSON) throws JSONException
  {
    List<PlanBenefitCategoryData> benefitCategories = new ArrayList<PlanBenefitCategoryData>();

    if (planJSON.has(BENEFIT_CATEGORIES))
    {
      JSONArray benefitCategoriesJSON = planJSON.getJSONArray(BENEFIT_CATEGORIES);
      for (int i = 0; i < benefitCategoriesJSON.length(); i++)
      {
        PlanBenefitCategoryData benefitCategory = new PlanBenefitCategoryData();
  
        JSONObject benefitCategoryJSON = benefitCategoriesJSON.getJSONObject(i);
        benefitCategory.setName(benefitCategoryJSON.getString(CATEGORY_NAME));
        benefitCategory.setSortOrder(benefitCategoryJSON.getInt(SORT_ORDER));
  
        List<PlanBenefitData> benefits = new ArrayList<PlanBenefitData>();
        
        if (benefitCategoryJSON.has(BENEFITS))
        {
          JSONArray benefitsJSON = benefitCategoryJSON.getJSONArray(BENEFITS);
          for (int j = 0; j < benefitsJSON.length(); j++)
          {
    
            PlanBenefitData benefit = new PlanBenefitData();
    
            JSONObject benefitJSON = benefitsJSON.getJSONObject(j);
    
            // "key" is the really internal code. REMEMBER THIS.
            benefit.setInternalCode(benefitJSON.getString(KEY));
            benefit.setDisplayName(benefitJSON.getString(DISPLAY_NAME));
            benefit.setSortOrder(benefitJSON.getInt(SORT_ORDER));
    
            List<PlanBenefitValueData> benefitValues = new ArrayList<PlanBenefitValueData>();
            
            if (benefitJSON.has(VALUES))
            {
              // getBenefitValueSetMap
              benefitValues.addAll(getBenefitValues(benefitJSON.getJSONArray(VALUES)));
            }
            
            benefit.setValues(benefitValues);
    
            benefits.add(benefit);
          }
        }
        benefitCategory.setBenefits(benefits);
        benefitCategories.add(benefitCategory);
      }
    }

    planData.setBenefitCategories(benefitCategories);
  }

  static List<PlanBenefitValueData> getBenefitValues(JSONArray benefitValuesJSON)
    throws JSONException
  {
    List<PlanBenefitValueData> benefitValues = new ArrayList<PlanBenefitValueData>();

    for (int i = 0; i < benefitValuesJSON.length(); i++)
    {
      PlanBenefitValueData benefitValue = new PlanBenefitValueData();

      JSONObject benefitValueJSON = benefitValuesJSON.getJSONObject(i);

      benefitValue.setBenefitValueTypeCode(benefitValueJSON.getString(CODE));
      // TODO | I don't like "displayName" here
      benefitValue.setDisplayName(benefitValueJSON.getString(VALUE));
      if (benefitValueJSON.has(NUMERIC_VALUE))
      {
        benefitValue.setNumericValue(benefitValueJSON.getDouble(NUMERIC_VALUE));
      }
      benefitValue.setDescription(benefitValueJSON.getString(DESCRIPTION));

      // TODO | custom attributes

      benefitValues.add(benefitValue);
    }

    return benefitValues;
  }
}
