package com.connecture.shopping.process.message;

import java.util.List;

/**
 * Encapsulates a message object
 */
public class MessageBean
{
  private String content;
  private String subject;
  private List<MessageRecipient> recipients;
  
  /**
   * @return the subject
   */
  public String getSubject()
  {
    return subject;
  }
  
  /**
   * @param subject the new value of subject
   */
  public void setSubject(String subject)
  {
    this.subject = subject;
  }
  
  /**
   * @return the content
   */
  public String getContent()
  {
    return content;
  }
  
  /**
   * @param content the new value of content
   */
  public void setContent(String content)
  {
    this.content = content;
  }

  /**
   * @return the recipients
   */
  public List<MessageRecipient> getRecipients()
  {
    return recipients;
  }
  /**
   * @param recipients the new value of recipients
   */
  public void setRecipients(List<MessageRecipient> recipients)
  {
    this.recipients = recipients;
  }
}