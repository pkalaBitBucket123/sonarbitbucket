package com.connecture.shopping.process.work;

import com.connecture.shopping.process.camel.request.BaseCamelRequest;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public class CamelRequestingDataProvider<Data> implements DataProvider<Data>
{
  private BaseCamelRequest<Data> request;
  
  public CamelRequestingDataProvider(BaseCamelRequest<Data> request)
  {
    this.request = request;
  }

  @Override
  public Data fetchData() throws Exception
  {
    return request.submitRequest(); 
  }
}
