package com.connecture.shopping.process.message;

public class PlanEmailProviderBean
{
  private String prefix;
  private String firstName;
  private String middleName;
  private String lastName;
  
  private String locationName;
  
  private boolean inNetwork;
  
  private String type;  
  private boolean personalName;
  
  /**
   * @return the prefix
   */
  public String getPrefix()
  {
    return prefix;
  }
  /**
   * @param prefix the prefix to set
   */
  public void setPrefix(String prefix)
  {
    this.prefix = prefix;
  }
  /**
   * @return the firstName
   */
  public String getFirstName()
  {
    return firstName;
  }
  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }
  /**
   * @return the middleName
   */
  public String getMiddleName()
  {
    return middleName;
  }
  /**
   * @param middleName the middleName to set
   */
  public void setMiddleName(String middleName)
  {
    this.middleName = middleName;
  }
  /**
   * @return the lastName
   */
  public String getLastName()
  {
    return lastName;
  }
  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  /**
   * @return the locationName
   */
  public String getLocationName()
  {
    return locationName;
  }
  /**
   * @param locationName the locationName to set
   */
  public void setLocationName(String locationName)
  {
    this.locationName = locationName;
  }
  /**
   * @return the inNetwork
   */
  public boolean isInNetwork()
  {
    return inNetwork;
  }
  /**
   * @param inNetwork the inNetwork to set
   */
  public void setInNetwork(boolean inNetwork)
  {
    this.inNetwork = inNetwork;
  }
  /**
   * @return the type
   */
  public String getType()
  {
    return type;
  }
  /**
   * @param type the type to set
   */
  public void setType(String type)
  {
    this.type = type;
  }
  /**
   * @return the isPersonalName
   */
  public boolean isPersonalName()
  {
    return personalName;
  }
  /**
   * @param isPersonalName the isPersonalName to set
   */
  public void setPersonalName(boolean personalName)
  {
    this.personalName = personalName;
  }
}