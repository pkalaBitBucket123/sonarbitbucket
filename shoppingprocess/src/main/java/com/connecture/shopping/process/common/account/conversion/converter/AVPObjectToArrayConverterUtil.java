package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;

public class AVPObjectToArrayConverterUtil implements AVPConverterUtil<AVPObjectToArrayConverter>
{
  @Override
  public List<String> validateJSON(JSONObject converterObject) throws JSONException
  {
    List<String> errors = new ArrayList<String>();
    JSONValidationUtils.checkForMissingField(errors, converterObject, "path");
    JSONValidationUtils.checkForMissingField(errors, converterObject, "reason");
    return errors;
  }

  @Override
  public AVPObjectToArrayConverter createConverter(JSONObject converterObject) throws JSONException
  {
    AVPObjectToArrayConverter converter = new AVPObjectToArrayConverter();
    converter.setPath(converterObject.getString("path"));
    converter.setReason(converterObject.getString("reason"));
    if (converterObject.has("rename")){
      converter.setRename(converterObject.optString("rename"));
    }
    return converter;
  }
}
