package com.connecture.shopping.process.work.core;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.Actor;
import com.connecture.model.data.Name;
import com.connecture.shopping.process.camel.request.ActorDataRequest;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.work.WorkImpl;

/**
 * This work supplies the information about the person currently logged in doing
 * the shopping.
 * 
 * @author Tcvetan
 */
public abstract class ActorWork extends WorkImpl implements ActorService
{
  // private static Logger LOG = Logger.getLogger(ActorWork.class);

  private String transactionId;

  protected Actor actor;
  protected ActorDataRequest actorDataRequest;

  @Override
  public JSONObject processData()
  {
    JSONObject retVal = new JSONObject();

    try
    {
      JSONObject actorJSON = new JSONObject();
      actorJSON.put("isInternalUser", actor.isInternalUser());
      actorJSON.put("ffmEnabled", actor.isFfmEnabled());
      actorJSON.put("userRole", actor.getUserRole());

      Name name = actor.getName();
      if (name != null)
      {
        JSONObject nameJSON = new JSONObject();
        nameJSON.put("first", name.getFirst());
        nameJSON.put("last", name.getLast());
        actorJSON.put("name", nameJSON);
      }

      retVal.put("actor", actorJSON);
    }
    catch (JSONException e)
    {
      // Auto-generated catch block
      e.printStackTrace();
    }

    return retVal;
  }
  
  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String xrefId)
  {
    this.transactionId = xrefId;
  }

  public Actor getActor()
  {
    return actor;
  }

  public void setActor(Actor actor)
  {
    this.actor = actor;
  }

  public void setActorDataRequest(ActorDataRequest actorDataRequest)
  {
    this.actorDataRequest = actorDataRequest;
  }
}