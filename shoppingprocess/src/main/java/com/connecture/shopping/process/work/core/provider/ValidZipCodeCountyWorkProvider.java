package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.ValidZipCodeCountyWork;

public abstract class ValidZipCodeCountyWorkProvider
  implements WorkProviderImpl<ValidZipCodeCountyWork>
{
  @Override
  public ValidZipCodeCountyWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    ValidZipCodeCountyWork work = createWork();

    try
    {
      String zipCode = params.getString("zipCode");
      work.setZipCode(zipCode);
    }
    catch (JSONException e)
    {
      throw new JSONException("\"zipCode\" param must be specified");
    }

    return work;
  }
}
