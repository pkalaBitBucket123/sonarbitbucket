package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.RulesEngineWork;

public abstract class RulesEngineWorkProvider implements WorkProviderImpl<RulesEngineWork>
{
  @Override
  public RulesEngineWork getWork(
    String userId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    RulesEngineWork work = createWork();
    
    try {
      String ruleSetId = params.getString("ruleSetId");
      work.setRuleSetId(ruleSetId);
      
      work.setDomain(data);
    }
    catch (JSONException e) {
      throw new JSONException("\"ruleSetId\" param must be specified");
    }
    
    return work;
  }
}
