package com.connecture.shopping.process.service.data;

public class GenderData
{
  private String key;
  private String display;

  /**
   * @return the key
   */
  public String getKey()
  {
    return key;
  }

  /**
   * @param key the key to set
   */
  public void setKey(String key)
  {
    this.key = key;
  }

  /**
   * @return the display
   */
  public String getDisplay()
  {
    return display;
  }

  /**
   * @param display the display to set
   */
  public void setDisplay(String display)
  {
    this.display = display;
  }
}
