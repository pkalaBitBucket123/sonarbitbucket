/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

/**
 * 
 */
public class PlanContributionData
{
  private Double amount;
  private String typeCode;
  
  /**
   * @return the amount
   */
  public Double getAmount()
  {
    return amount;
  }
  /**
   * @param amount the new value of amount
   */
  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  /**
   * @return the typeCode
   */
  public String getTypeCode()
  {
    return typeCode;
  }
  /**
   * @param typeCode the new value of typeCode
   */
  public void setTypeCode(String typeCode)
  {
    this.typeCode = typeCode;
  }
}