package com.connecture.shopping.process.camel.request;

import java.util.List;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.log4j.Logger;
import org.json.JSONObject;


public class RulesEngineDataRequest<D> extends BaseCamelRequest<D>
{
  private static Logger LOG = Logger.getLogger(RulesEngineDataRequest.class);
  
  @Produce(uri="{{camel.shopping.producer.executeRule}}")
  private ProducerTemplate producer;
  
  private D domain;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  @Override
  public Object getRequestBody()
  {
    String requestBody = "";
    if (null != domain)
    {
      if (domain instanceof String)
      {
        requestBody = String.valueOf(domain);
      }
      else 
      {
        requestBody = new JSONObject(domain).toString();
      }
    }
    else {
      LOG.warn("No domain object specified, requestBody is empty");
    }
    return requestBody;
  }

  public void setGroupId(String groupId)
  {
    headers.put("groupId", groupId);
  }

  public void setRuleCodes(List<String> ruleCodes)
  {
    headers.put("ruleCodes", ruleCodes);
  }
  
  public void setEffDate(Long effDateMs) {
    headers.put("effDateMs", effDateMs);
  }
  
  public void setDomain(D domain)
  {
    this.domain = domain;
  }

  public D getDomain()
  {
    return domain;
  }
}
