package com.connecture.shopping.process.service.data;


public class DemographicsConfigFieldMaskData
{
  private String mask;
  private String maskClass;
  
  public String getMask()
  {
    return mask;
  }
  public void setMask(String mask)
  {
    this.mask = mask;
  }
  public String getMaskClass()
  {
    return maskClass;
  }
  public void setMaskClass(String maskClass)
  {
    this.maskClass = maskClass;
  } 
}