package com.connecture.shopping.process.camel.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelExecutionException;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import com.connecture.shopping.process.DataConversionException;
import com.connecture.shopping.process.IntegrationException;

/**
 * This will handle the basic generic case for an extending request
 * object such as <code>MyRequest&lt;String&gt;</code>.  If something fancier is needed,
 * like <code>MyFancyRequest&lt;List&lt;String&gt;&gt;</code>, then override {@link #getReturnType()}
 * to return the appropriate class.
 * 
 * @author bengen
 *
 * @param <T> The return type for this request
 */
public abstract class BaseCamelRequest<T> implements CamelRequest<T>, CamelRequestParameters
{
  private static Logger LOG = Logger.getLogger(BaseCamelRequest.class);

  protected Map<String,Object> headers = new HashMap<String,Object>();

  @Override
  public Map<String, Object> getRequestHeaders()
  {
    return headers;
  }
  
  protected T convertData(Object data) throws Exception {
    @SuppressWarnings("unchecked")
    T convertedData = (T) data;
    return convertedData;
  }
  
  @Override
  public Object getRequestBody()
  {
    return null;
  }

  public T submitRequest() throws IntegrationException
  {
    T result = null;
    StopWatch watch = new StopWatch();
    watch.start();
    
    LOG.info("Starting Camel Request for " + getClass().getSimpleName());
    
    Object resultData = null;
    try
    {
      resultData = getProducer().requestBodyAndHeaders(getRequestBody(), getRequestHeaders());
    }
    catch(CamelExecutionException cee)
    {
      handleCamelException(cee);
    }
    
    try
    {
      result = convertData(resultData);
    }
    catch(Exception e)
    {
      handleConversionException(e);
    }

    watch.stop();
    LOG.info("Completed Camel Request for " + getClass().getSimpleName() + " in " + watch.getTime() + "ms");

    return result;
  }
  
  /**
   * Camel wraps any thrown exception during a request in a
   * {@link CamelExecutionException} which is handled here.
   * The default behavior is to extract the wrapped exception
   * and rethrow it.  Struts will handle the exception.  If
   * more specific handling is required, override this.  The
   * {@link CamelExecutionException} contains the exchange,
   * so it is possible to analyze the request/response for
   * faults
   *  
   * @param cause The {@link CamelExecutionException}
   */
  protected void handleCamelException(CamelExecutionException cause)
  {
    Throwable unwrappedEx = cause.getCause();
    if(unwrappedEx != null && unwrappedEx instanceof RuntimeException)
    {
      //throw the root cause of the camel execption
      throw (RuntimeException)unwrappedEx;
    }
    else
    {
      throw new IntegrationException(cause.getMessage(), cause.getCause());
    }
  }
  
  /**
   * If anything causes data conversion to fail, we wrap the cause
   * in our own {@link DataConversionException} and re-throw.
   * This makes it easier to catch a specific exception instead of
   * generically catching and throwing {@link Exception} everywhere.
   *  
   * @param cause The conversion error to be reported
   */
  protected void handleConversionException(Exception cause)
  {
    throw new DataConversionException(cause.getMessage(), cause);
  }
}
