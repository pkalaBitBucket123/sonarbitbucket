package com.connecture.shopping.process.work.individual;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingRenewalData;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentEvent;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldMask;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldOption;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.core.ApplicationConfigWork;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventDaysLeftToEnrollWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventEffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventEnrollmentDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRuleEngineHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;
import com.connecture.shopping.process.work.core.ShoppingRulesEngineBean;

/**
 * @author shopping development Extending <code>ApplicationConfigWork</code> to
 *         add in the demographics configuration calls. This is most likely
 *         temporary because we are using the rules engine to handle that, so
 *         making this IFP specific so we don't muck up the core logic.
 */
public class IFPApplicationConfigWork extends ApplicationConfigWork
{
  private static Logger LOG = Logger.getLogger(IFPApplicationConfigWork.class);

  // Start Required Inputs
  private EnrollmentEventRulesEngineBean hcrEffectiveDateRulesEngineBean;
  private EnrollmentEventRulesEngineBean hcrSpecialEnrollmentReasonsRulesEngineBean;
  private ShoppingRulesEngineBean shoppingDemographicsRulesEngineBean;
  private IAToShoppingIndividualDataRequest individualDataRequest;

  private Boolean ffmEnabled;
  private String hcrEffectiveDate;
  private String ffmPriorToApplyURL;
  // End Required Inputs

  // Start Outputs
  private ShoppingDomainModel demographicsRulesEngineDomainData;
  private EnrollmentEventDomainModel specialEnrollmentReasonsDomain;

  private IAToShoppingData individualData;
  private String effectiveDateStr;
  private ActorService ifpActorService;

  // private String enrollmentEndDateStr;

  // End Outputs

  private boolean isEffectiveDateProvided;

  /*
   * (non-Javadoc)
   * @see com.connecture.shopping.process.work.ApplicationConfigWork#get()
   */
  @Override
  public void get() throws Exception
  {
    // handle all of our parent logic
    super.get();

    configCriteria.put("ffmEnabled", Boolean.toString(ffmEnabled));

    individualData = getIndividualData();

    // get a reference to a new or existing account for the transaction Id
    Account account = getAccount();
    JSONObject accountJSON = account.getAccount(getTransactionId());

    // ***************************************************************************************
    // JWG - 28-JUN-2013 Use the following for testing Renewals
    // TODO Remove this once a value is passed in from IA
    // String HARD_CODED_RENEWING_DEFAULT_PLAN_INTERNAL_CODE = "PPO_Max_500";
    // IAToShoppingRenewalData renewalData = new IAToShoppingRenewalData();
    // renewalData.setRenewingDefaultPlanInternalCode(HARD_CODED_RENEWING_DEFAULT_PLAN_INTERNAL_CODE);
    //
    // Date HARD_CODED_RENEWING_EFFECTIVE_DATE = new GregorianCalendar(2013, 7,
    // 1).getTime();
    // renewalData.setRenewingEffectiveDate(HARD_CODED_RENEWING_EFFECTIVE_DATE);
    //
    // individualData.setRenewalData(renewalData);
    // TODO Remove the above once values are passed in from IA
    // ***************************************************************************************

    isEffectiveDateProvided = EnrollmentEventEffectiveDateWorkHelper
      .isEffectiveDateProvided(accountJSON);

    this.populateRenewalData(accountJSON, individualData);

    // now retrieve the demographics configuration for IFP
    hcrEffectiveDateRulesEngineBean.setDataCache(getDataCache());

    effectiveDateStr = EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(getAccount()
      .getAccount(getTransactionId()), hcrEffectiveDateRulesEngineBean);

    EnrollmentEventDomainModel domainModel = EnrollmentEventRuleEngineHelper
      .getDomainModel(hcrEffectiveDateRulesEngineBean);
    
    // Show Special Enrollment in either of these two cases:
    // 1. Special enrollment is specifically required as marked from the calling module.
    // 2. This is NOT a new business case AND we are outside of the open enrollment window.
    configCriteria.put(
      "specialEnrollment",
      Boolean.toString(individualData.isSpecialEnrollmentRequired()
        || (!EnrollmentEventEnrollmentDateWorkHelper.isWithinHCREnrollmentWindow(domainModel))));

    hcrSpecialEnrollmentReasonsRulesEngineBean.setDataCache(getDataCache());
    specialEnrollmentReasonsDomain = hcrSpecialEnrollmentReasonsRulesEngineBean.executeRule();

    shoppingDemographicsRulesEngineBean.setCoverageEffectiveDate(EffectiveDateWorkHelper
      .getEffectiveDate(effectiveDateStr));
    shoppingDemographicsRulesEngineBean.setDataCache(getDataCache());
    demographicsRulesEngineDomainData = shoppingDemographicsRulesEngineBean.executeRule();
  }

  private IAToShoppingData getIndividualData() throws Exception
  {
    individualDataRequest.setTransactionId(getTransactionId());
    individualDataRequest.setContext(ShoppingContext.INDIVIDUAL);

    DataProvider<IAToShoppingData> dataProvider = new CamelRequestingDataProvider<IAToShoppingData>(
      individualDataRequest);
    return getCachedData(ShoppingDataProviderKey.INDIVIDUAL_DATA, dataProvider);
  }

  private void populateRenewalData(JSONObject accountDataJSON, IAToShoppingData individualData)
    throws Exception
  {
    IAToShoppingRenewalData renewalData = individualData.getRenewalData();
    if (null != renewalData)
    {
      configCriteria.put("renewal", Boolean.TRUE.toString());

      accountDataJSON
        .put("renewingDefaultPlanId", renewalData.getRenewingDefaultPlanInternalCode());

      Date renewingEffectiveDate = renewalData.getRenewingEffectiveDate();
      if (null != renewingEffectiveDate)
      {
        accountDataJSON.put("renewingEffectiveDate",
          EffectiveDateWorkHelper.getEffectiveDateString(renewingEffectiveDate));
      }
    }
    else
    {
      // Clear out the values so the account resets to non-renewal
      accountDataJSON.remove("renewingDefaultPlanId");
      accountDataJSON.remove("renewingEffectiveDate");
    }
  }

  /*
   * (non-Javadoc)
   * @see
   * com.connecture.shopping.process.work.ApplicationConfigWork#processData()
   */
  @Override
  public JSONObject processData() throws Exception
  {
    // return object
    JSONObject jsonObject = super.processData();

    JSONObject demographicsConfig = new JSONObject();
    Actor actor = ifpActorService.getActorData();

    if (null != demographicsRulesEngineDomainData)
    {
      if (StringUtils.isBlank(demographicsRulesEngineDomainData.getRequest().getError()))
      {
        // TODO | get specific items out of the rules engine result
        List<DemographicsFormField> demographicsFields = demographicsRulesEngineDomainData
          .getRequest().getDemographicsForm().getField();

        demographicsConfig.put("demographicFields",
          this.buildDemographicsFormFieldsJSON(demographicsFields));

        demographicsConfig.put("demographicsConfigMapping",
          getDemographicsConfigMemberFieldMappings(demographicsFields));

        // TOOD | get account data by version and validate demographic mappings
      }
      else
      {
        LOG.error("Unable to obtain demographic configuration from RulesEngine. Cause: "
          + demographicsRulesEngineDomainData.getRequest().getError());
      }
    }

    boolean hcrEffective = new Date().getTime() >= EnrollmentEventEffectiveDateWorkHelper
      .getEffectiveDateMs(hcrEffectiveDate);
    // demographicsConfig.put("hcrEffective", hcrEffective);
    demographicsConfig.put("hcrEffective", true);

    // we need to determine here if we need the datepicker to appear for
    // 2013/2014.
    // conditions for showing the datepicker are:
    // 1. an effective date isn't already provided from the calling module
    // (renewal)
    // 2. Health Care Reform (HCR) is not yet effective, in which case the
    // user must choose the date before
    // advancing in shopping because the rules will differ.
  //demographicsConfig.put("showDatePicker", !(isEffectiveDateProvided || hcrEffective));
    demographicsConfig.put("showDatePicker", false);

    if (StringUtils.isNotBlank(effectiveDateStr))
    {
      demographicsConfig.put("effectiveDate", effectiveDateStr);

      demographicsConfig.put("effectiveMs",
        EffectiveDateWorkHelper.getEffectiveDateMs(effectiveDateStr));
      demographicsConfig.put("userRole", actor.getUserRole());
    }

    String enrollmentEndDate = EnrollmentEventEnrollmentDateWorkHelper.getEnrollmentEndDate(
      getAccount().getAccount(getTransactionId()), hcrEffectiveDateRulesEngineBean);
    if (StringUtils.isNotBlank(enrollmentEndDate))
    {
      demographicsConfig.put("enrollmentEndDate", enrollmentEndDate);
    }

    String enrollmentStartDate = EnrollmentEventEnrollmentDateWorkHelper.getEnrollmentStartDate(
      getAccount().getAccount(getTransactionId()), hcrEffectiveDateRulesEngineBean);
    if (StringUtils.isNotBlank(enrollmentStartDate))
    {
      demographicsConfig.put("enrollmentStartDate", enrollmentStartDate);
    }

    Integer daysLeftToEnroll = EnrollmentEventDaysLeftToEnrollWorkHelper.getDaysLeftToEnroll(
      getAccount().getAccount(getTransactionId()), hcrEffectiveDateRulesEngineBean);
    if (null != daysLeftToEnroll)
    {
      demographicsConfig.put("daysLeftToEnroll", daysLeftToEnroll);
    }

    if (null != specialEnrollmentReasonsDomain)
    {
      JSONArray reasonTypesArray = new JSONArray();

      List<SpecialEnrollmentEvent> events = specialEnrollmentReasonsDomain.getApplication()
        .getSpecialEnrollment().getEvents();

      for (SpecialEnrollmentEvent event : events)
      {
        JSONObject reasonType = new JSONObject();
        reasonType.put("key", event.getEventType());
        reasonType.put("value", event.getDisplayName());
        reasonType.put("enrollmentDays", event.getEnrollmentDays());
        reasonTypesArray.put(reasonType);
      }

      demographicsConfig.put("specialEnrollmentEventTypes", reasonTypesArray);
    }

    JSONObject demographicWidgetConfig = getWidget(jsonObject, "Demographics");

    // demographicWidgetConfig.put(WIDGET_ID_KEY, "Demographics");

    demographicWidgetConfig.put(WIDGET_CONFIG_KEY, demographicsConfig);

    // demographicWidgetConfig.put(WIDGET_TYPE_KEY, "Demographics");

    // addWidget(jsonObject, demographicWidgetConfig);

    JSONObject specialEnrollmentConfig = getWidget(jsonObject, "SpecialEnrollment");
    specialEnrollmentConfig.put(WIDGET_CONFIG_KEY, demographicsConfig);

    JSONObject enrollmentDateConfig = getWidget(jsonObject, "EnrollmentDate");
    enrollmentDateConfig.put(WIDGET_CONFIG_KEY, demographicsConfig);

    return jsonObject;
  }

@Override
  protected void processApplicationConfigData(JSONObject jsonObject) throws JSONException
  {
    super.processApplicationConfigData(jsonObject);
    
    // this is called in the processData of the parent object, so we can use anything set through the "get" above.

    JSONObject applicationConfigJSON = jsonObject.getJSONObject("applicationConfig");

    applicationConfigJSON.put("ffmEnabled", ffmEnabled);

    applicationConfigJSON.put("ffmPriorToApplyURL", ffmPriorToApplyURL);

// start change for the issue encountered after cherry pick of code for SHSBCBSNE - 1201
    applicationConfigJSON.put("quotingEnabled", individualData.isQuotingEnabled());
// end change for the issue encountered after cherry pick of code for SHSBCBSNE - 1201

    String isOnExchangeFlow = configCriteria.get("onExchange");
    if(isOnExchangeFlow != null && isOnExchangeFlow.equalsIgnoreCase("true"))
    {
      applicationConfigJSON.put("displayPlansDisclaimer", true);
    }
    else
    {
      applicationConfigJSON.put("displayPlansDisclaimer", false);
    }
  }

  private JSONArray buildDemographicsFormFieldsJSON(List<DemographicsFormField> demographicsFields)
    throws JSONException
  {
    // build individual field JSON and handle any list-level logic, like sorting

    // sort the fields by "sort" property
    Collections.sort(demographicsFields, DemographicsFormField.FieldOrderComparator);

    JSONArray demographicsFieldsJSON = new JSONArray();
    for (DemographicsFormField demographicsField : demographicsFields)
    {
      demographicsFieldsJSON.put(buildDemographicsFieldJSON(demographicsField));
    }

    return demographicsFieldsJSON;
  }

  private JSONObject buildDemographicsFieldJSON(DemographicsFormField demographicsFieldData)
    throws JSONException
  {
    JSONObject fieldJSON = new JSONObject();

    fieldJSON.put("key", demographicsFieldData.getKey());
    fieldJSON.put("name", demographicsFieldData.getName());
    fieldJSON.put("dataType", demographicsFieldData.getDataType());
    fieldJSON.put("styleClass", demographicsFieldData.getStyleClass());
    fieldJSON.put("headerStyleClass", demographicsFieldData.getHeaderStyleClass());
    fieldJSON.put("inputType", demographicsFieldData.getInputType());
    fieldJSON.put("label", demographicsFieldData.getLabel());
    fieldJSON.put("order", demographicsFieldData.getOrder());
    fieldJSON.put("size", demographicsFieldData.getSize());

    fieldJSON.put("options",
      this.buildDemographicsFieldOptionsJSON(demographicsFieldData.getOption()));
    fieldJSON.put("mask", buildDemographcisFieldMaskJSON(demographicsFieldData.getMask()));

    return fieldJSON;
  }

  private JSONObject buildDemographcisFieldMaskJSON(DemographicsFormFieldMask maskData)
    throws JSONException
  {
    JSONObject maskJSON = new JSONObject();
    if (maskData != null)
    {
      maskJSON.put("mask", maskData.getMask());
      maskJSON.put("maskClass", maskData.getMaskClass());
    }
    return maskJSON;
  }

  private JSONArray buildDemographicsFieldOptionsJSON(List<DemographicsFormFieldOption> options)
    throws JSONException
  {
    Collections.sort(options, DemographicsFormFieldOption.OptionOrderComparator);

    JSONArray optionsJSON = new JSONArray();
    if (options != null && !options.isEmpty())
    {
      for (DemographicsFormFieldOption optionData : options)
      {
        optionsJSON.put(buildDemographicsFieldOptionJSON(optionData));
      }
    }
    return optionsJSON;
  }

  private JSONObject buildDemographicsFieldOptionJSON(DemographicsFormFieldOption option)
    throws JSONException
  {
    JSONObject optionJSON = new JSONObject();
    optionJSON.put("dataGroupType", option.getDataGroupType());
    optionJSON.put("defaultSelection", option.getDefaultSelection());
    optionJSON.put("initialSelection", option.getInitialSelection());
    optionJSON.put("label", option.getLabel());
    optionJSON.put("order", option.getOrder());
    if ("_".equalsIgnoreCase(option.getValue()))
    {
      optionJSON.put("value", "");
    }
    else
    {
      optionJSON.put("value", option.getValue());
    }
    return optionJSON;
  }

  // TODO | account version ?
  // TODO | then another method to validate the mapping
  private JSONObject getDemographicsConfigMemberFieldMappings(
    List<DemographicsFormField> demographicsFields) throws JSONException
  {
    JSONObject demographicFieldMapping = new JSONObject();

    for (DemographicsFormField fieldData : demographicsFields)
    {
      demographicFieldMapping.put(fieldData.getKey(), fieldData.getName());
    }

    return demographicFieldMapping;
  }

  public void setIndividualDataRequest(IAToShoppingIndividualDataRequest individualDataRequest)
  {
    this.individualDataRequest = individualDataRequest;
  }

  public void setHcrEffectiveDateRulesEngineBean(
    EnrollmentEventRulesEngineBean hcrEffectiveDateRulesEngineBean)
  {
    this.hcrEffectiveDateRulesEngineBean = hcrEffectiveDateRulesEngineBean;
  }

  public void setHcrSpecialEnrollmentReasonsRulesEngineBean(
    EnrollmentEventRulesEngineBean hcrSpecialEnrollmentReasonsRulesEngineBean)
  {
    this.hcrSpecialEnrollmentReasonsRulesEngineBean = hcrSpecialEnrollmentReasonsRulesEngineBean;
  }

  public void setShoppingDemographicsRulesEngineBean(
    ShoppingRulesEngineBean shoppingDemographicsRulesEngineBean)
  {
    this.shoppingDemographicsRulesEngineBean = shoppingDemographicsRulesEngineBean;
  }

  public Boolean getFfmEnabled()
  {
    return ffmEnabled;
  }

  public void setFfmEnabled(Boolean ffmEnabled)
  {
    this.ffmEnabled = ffmEnabled;
  }

  public String getFfmPriorToApplyURL()
  {
    return ffmPriorToApplyURL;
  }

  public void setFfmPriorToApplyURL(String ffmPriorToApplyURL)
  {
    this.ffmPriorToApplyURL = ffmPriorToApplyURL;
  }

  public String getHcrEffectiveDate()
  {
    return hcrEffectiveDate;
  }

  public void setHcrEffectiveDate(String hcrEffectiveDate)
  {
    this.hcrEffectiveDate = hcrEffectiveDate;
  }
  public void setIfpActorService(ActorService ifpActorService)
  {
    this.ifpActorService = ifpActorService;
  }
}
