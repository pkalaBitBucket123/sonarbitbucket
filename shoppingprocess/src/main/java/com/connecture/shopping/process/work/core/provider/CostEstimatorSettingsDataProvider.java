package com.connecture.shopping.process.work.core.provider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.RulesEngineDataRequest;
import com.connecture.shopping.process.camel.request.ShoppingRulesEngineDataRequest;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public abstract class CostEstimatorSettingsDataProvider implements DataProvider<ShoppingDomainModel>
{
  private static Logger LOG = Logger.getLogger(CostEstimatorSettingsDataProvider.class);

  private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

  private RulesEngineDataRequest<ShoppingDomainModel> rulesEngineDataRequest;
  private String transactionId;
  private Long effectiveDateMs;
  private JSONObject account;
  
  private String ruleSet;
  
  private String ruleEngineGroupId;

  private ShoppingDomainModel rulesEngineDomain;

  @Override
  public ShoppingDomainModel fetchData() throws Exception
  {
    rulesEngineDataRequest.setGroupId(ruleEngineGroupId);

    List<String> ruleCodes = new ArrayList<String>();
    ruleCodes.add(ruleSet);
    rulesEngineDataRequest.setRuleCodes(ruleCodes);

    rulesEngineDataRequest.setEffDate(new Date().getTime());

    rulesEngineDomain.getRequest().setId("0");
    rulesEngineDomain.getRequest().setTimeInMs(new Date().getTime());
    rulesEngineDomain.getRequest().setEffectiveDate(dateFormat.format(new Date(effectiveDateMs)));

    if (null != account)
    {
      try
      {
        List<Consumer> consumers = new ArrayList<Consumer>();
        if (validDemographicsForProcessing(account))
        {
          JSONArray membersArray = account.getJSONArray("members");
          for (int m = 0; m < membersArray.length(); m++)
          {
            Consumer consumer = new Consumer();
            JSONObject memberObject = membersArray.getJSONObject(m);

            if (isNotNullAndNotJSONNull(memberObject, "memberRefName"))
            {
              consumer.setXid(memberObject.getString("memberRefName"));
            }

            if (isNotNullAndNotJSONNull(memberObject, "gender"))
            {
              consumer.setGender(memberObject.getString("gender"));
            }

            populateMemberRelationshipcode(consumer, memberObject);

            if (isNotNullAndNotJSONNull(memberObject, "birthDate"))
            {
              String birthDate = memberObject.getString("birthDate");
              consumer.setDateOfBirth(birthDate);
            }
            
            populateMemberZipCode(consumer, memberObject);

            consumers.add(consumer);
          }
        }
        rulesEngineDomain.getRequest().setConsumer(consumers);

        if (LOG.isDebugEnabled())
        {
          LOG.debug(new JSONObject(rulesEngineDomain).toString(4));
        }

        if (consumers.size() > 0)
        {
          rulesEngineDataRequest.setDomain(rulesEngineDomain);

          rulesEngineDomain = rulesEngineDataRequest.submitRequest();
        }
      }
      catch (JSONException e)
      {
        LOG.error("Failed to parse account from DB", e);
      }
    }
    else
    {
      LOG.error("No Account found for " + transactionId);
    }

    return rulesEngineDomain;
  }

  protected boolean validDemographicsForProcessing(JSONObject accountJSON) throws JSONException
  {
    boolean isValidDemographicsForProcessing = null != accountJSON;
        
    if(isValidDemographicsForProcessing)
    {
      //check if we have a zip code
      isValidDemographicsForProcessing = validDemographicsZipCode(accountJSON);
      
      if(isValidDemographicsForProcessing)
      {
        //check members for what we need      
        JSONArray membersJSON = null;
        if(isNotNullAndNotJSONNull(accountJSON, "members"))
        {
          membersJSON = accountJSON.getJSONArray("members");
        }
        
        isValidDemographicsForProcessing = null != membersJSON;
        
        for (int i = 0; isValidDemographicsForProcessing && i < membersJSON.length(); i++)
        {
          JSONObject memberJSON = membersJSON.getJSONObject(i);
          
          isValidDemographicsForProcessing &= null != memberJSON.optString("birthDate", null);
    
          isValidDemographicsForProcessing &= null != memberJSON.optString("gender", null);
        }
      }
    }
    
    return isValidDemographicsForProcessing;
  }
  
  protected abstract boolean validDemographicsZipCode(JSONObject accountJSON) throws JSONException;
  
  protected abstract void populateMemberRelationshipcode(Consumer consumer, JSONObject memberObject) throws JSONException;

  protected abstract void populateMemberZipCode(Consumer consumer, JSONObject memberObject) throws JSONException;

  public static Boolean isNotNullAndNotJSONNull(JSONObject object, String key) throws JSONException
  {
    return object.has(key) && !JSONObject.NULL.equals(object.get(key));
  }

  public void setRequest(ShoppingRulesEngineDataRequest rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }

  public void setRulesEngineDomain(ShoppingDomainModel rulesEngineDomain)
  {
    this.rulesEngineDomain = rulesEngineDomain;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setEffectiveDateMs(Long effectiveDateMs)
  {
    this.effectiveDateMs = effectiveDateMs;
  }

  protected JSONObject getAccount()
  {
    return account;
  }

  public void setAccount(JSONObject account)
  {
    this.account = account;
  }

  public void setRuleSet(String ruleSet)
  {
    this.ruleSet = ruleSet;
  }

  public void setRulesEngineGroupId(String ruleEngineGroupId)
  {
    this.ruleEngineGroupId = ruleEngineGroupId;
  }
}
