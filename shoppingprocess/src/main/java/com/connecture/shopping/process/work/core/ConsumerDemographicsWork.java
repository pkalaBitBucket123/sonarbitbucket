package com.connecture.shopping.process.work.core;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingProducer;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingFfmMemberEligibilityData;
import com.connecture.model.integration.data.IAToShoppingMemberDetails;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldOption;
import com.connecture.shopping.process.work.WorkImpl;

public abstract class ConsumerDemographicsWork extends WorkImpl
{
  // Start Required Inputs
  private String transactionId;
  private JSONObject params;
  private ShoppingRulesEngineBean shoppingDemographicsBean;
  private EnrollmentEventRulesEngineBean hcrEffectiveDateBean;

  // End Required Inputs

  @Override
  public JSONObject processData() throws Exception
  {
    return new JSONObject();
  }

  protected JSONObject processProducerDemographics(ShoppingProducer shoppingProducer) throws JSONException
  {
    JSONObject producerDemographics = new JSONObject();

    // name
    JSONObject producerNameJSON = new JSONObject();
    if (null != shoppingProducer.getName())
    {
      producerNameJSON.put("first", shoppingProducer.getName().getFirst());
      producerNameJSON.put("last", shoppingProducer.getName().getLast());
    }
    producerDemographics.put("name", producerNameJSON);

    // address
    JSONObject producerAddressJSON = new JSONObject();
    if (null != shoppingProducer.getAddress())
    {
      producerAddressJSON.put("street", shoppingProducer.getAddress().getStreetAddress1());
      producerAddressJSON.put("city", shoppingProducer.getAddress().getCity());
      producerAddressJSON.put("state", shoppingProducer.getAddress().getStateCode());
      producerAddressJSON.put("zipCode", shoppingProducer.getAddress().getZipCode());
    }
    producerDemographics.put("address", producerAddressJSON);

    // contact
    JSONObject contactJSON = new JSONObject();
    if (StringUtils.isNotBlank(shoppingProducer.getAreaCode()))
    {
      contactJSON.put("areacode", shoppingProducer.getAreaCode());
    }
    contactJSON.put("phonenumber", shoppingProducer.getPhoneNumber());
    if (StringUtils.isNotBlank(shoppingProducer.getExtension()))
    {
      contactJSON.put("extension", shoppingProducer.getExtension());
    }
    contactJSON.put("email", shoppingProducer.getEmailAddress());
    producerDemographics.put("contact", contactJSON);

    // agency
    producerDemographics.put("agency", shoppingProducer.getAgency());

    return producerDemographics;
  }

  protected Map<String, String> buildOptionValueTypeMap(String effectiveDate) throws Exception
  {
    shoppingDemographicsBean.setDataCache(getDataCache());
    shoppingDemographicsBean.setCoverageEffectiveDate(EffectiveDateWorkHelper.getEffectiveDate(effectiveDate));
    ShoppingDomainModel domainData = shoppingDemographicsBean.executeRule();

    Map<String, String> optionValueTypes = new HashMap<String, String>();

    List<DemographicsFormField> configFields = domainData.getRequest().getDemographicsForm().getField();
    for (DemographicsFormField configField : configFields)
    {
      List<DemographicsFormFieldOption> options = configField.getOption();
      for (DemographicsFormFieldOption option : options)
      {
        optionValueTypes.put(option.getValue(), option.getDataGroupType());
      }
    }

    return optionValueTypes;
  }

  protected String getEffectiveDate() throws Exception
  {
    hcrEffectiveDateBean.setDataCache(getDataCache());

    return EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(getAccount().getAccount(getTransactionId()), hcrEffectiveDateBean);
  }

  protected void populateAccountDemographicData(JSONObject accountDataJSON, IAToShoppingData individualData, Map<String, String> optionValueTypeMap, boolean isNew) throws Exception
  {
    // now convert the data into JSON and save it into the account
    String zipCode = "";
    String countyId = "";

    BigDecimal aptcAmountTotal = BigDecimal.ZERO;

    List<IAToShoppingMemberDetails> membersData = individualData.getMemberDetails();

    JSONArray memberJSONArray = new JSONArray();

    if (membersData != null)
    {
      for (IAToShoppingMemberDetails memberData : membersData)
      {
        JSONObject memberJSON = new JSONObject();

        memberJSON.put("isNew", isNew);

        // member Id
        memberJSON.put("memberRefName", memberData.getMemberId());

        // firstName
        JSONObject name = new JSONObject();
        name.put("first", memberData.getFirstName());
        memberJSON.put("name", name);

        // date of birth
        // It may be null based on the origin of the Quote.
        if (null != memberData.getDateOfBirth())
        {
          memberJSON.put("birthDate", ShoppingDateUtils.dateToString(memberData.getDateOfBirth()));
        }

        // gender
        // It may be null based on the origin of the Quote
        if (StringUtils.isNotBlank(memberData.getGender()))
        {
          memberJSON.put("gender", memberData.getGender());
        }

        // relationship
        memberJSON.put("memberRelationship", memberData.getMemberType());

        // relationshipKey
        memberJSON.put("relationshipKey", optionValueTypeMap.get(memberData.getMemberType()));

        // smoker (optionally included)
        if (StringUtils.isNotBlank(memberData.getSmokerStatus()))
        {
          // TODO | change rules and handle literal smoker status
          memberJSON.put("isSmoker", "TRUE".equalsIgnoreCase(memberData.getSmokerStatus()) ? "y" : "n");
        }

        // check for ffmData
        IAToShoppingFfmMemberEligibilityData memberEligibility = memberData.getFfmMemberEligibilityData();
        if (memberEligibility != null)
        {
          memberJSON.put("ffmData", this.getMemberFFMData(memberEligibility));
          if (memberEligibility.isAptcEligibIndicator() && memberEligibility.getMonthlyAPTCAmount() != null)
          {
            aptcAmountTotal = aptcAmountTotal.add(memberEligibility.getMonthlyAPTCAmount());
          }
        }

        // add member
        memberJSONArray.put(memberJSON);

        // get account-level zipCode
        if (StringUtils.isNotBlank(memberData.getZipCode()))
        {
          zipCode = memberData.getZipCode();
        }

        // get account-level countyId
        if (StringUtils.isNotBlank(memberData.getCounty()))
        {
          countyId = memberData.getCounty();
        }
      }
    }

    if (aptcAmountTotal.doubleValue() > 0)
    {
      accountDataJSON.put("householdPremiumTaxCredit", aptcAmountTotal.doubleValue());
    }

    // Pre-fill members
    accountDataJSON.put("members", memberJSONArray);

    // Pre-fill zipCode
    if (StringUtils.isNotBlank(zipCode))
    {
      accountDataJSON.put("zipCode", zipCode);
    }

    // Pre-fill county
    if (StringUtils.isNotBlank(countyId))
    {
      accountDataJSON.put("countyId", countyId);
    }

    // populate special enrollment data if available
    if (individualData.getSpecialEnrollmentEventDate() != null && StringUtils.isNotBlank(individualData.getSpecialEnrollmentReasonKey()))
    {
      accountDataJSON.put("eventDate", ShoppingDateUtils.dateToString(individualData.getSpecialEnrollmentEventDate()));
      accountDataJSON.put("eventType", individualData.getSpecialEnrollmentReasonKey());
    }
  }

  private JSONObject getMemberFFMData(IAToShoppingFfmMemberEligibilityData ffmMemberEligibilityData) throws JSONException
  {
    JSONObject ffmDataJSON = new JSONObject();
    ffmDataJSON.put("premiumTaxCredit", ffmMemberEligibilityData.getMonthlyAPTCAmount());
    return ffmDataJSON;
  }

  public JSONObject getParams()
  {
    return params;
  }

  public void setParams(JSONObject params)
  {
    this.params = params;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setShoppingDemographicsBean(ShoppingRulesEngineBean shoppingDemographicsBean)
  {
    this.shoppingDemographicsBean = shoppingDemographicsBean;
  }

  public void setHcrEffectiveDateBean(EnrollmentEventRulesEngineBean hcrEffectiveDateBean)
  {
    this.hcrEffectiveDateBean = hcrEffectiveDateBean;
  }
}
