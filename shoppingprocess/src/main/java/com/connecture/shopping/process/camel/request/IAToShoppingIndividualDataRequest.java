package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class IAToShoppingIndividualDataRequest extends BaseCamelRequest<IAToShoppingData>
{
  @Produce(uri="{{camel.shopping.producer.iaIfpShoppingData}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
  
  public void setTransactionId(String transactionId)
  {
    headers.put(TRANSACTION_ID_KEY, transactionId);
  }
  
  public void setContext(ShoppingContext context)
  {
    headers.put(CONTEXT_KEY, context.getContextName());
  }
}
