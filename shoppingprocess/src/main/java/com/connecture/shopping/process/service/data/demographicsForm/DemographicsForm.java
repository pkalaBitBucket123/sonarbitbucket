package com.connecture.shopping.process.service.data.demographicsForm;

import java.util.ArrayList;
import java.util.List;

public class DemographicsForm
{
  private String id;
  private String name;
  private List<DemographicsFormField> field = new ArrayList<DemographicsFormField>(); 
  private List<DemographicsFormMember> member = new ArrayList<DemographicsFormMember>();
  private List<DemographicsFormValidationErrorConfig> validationErrorConfig = new ArrayList<DemographicsFormValidationErrorConfig>();
  private List<DemographicsFormValidationError> validationError = new ArrayList<DemographicsFormValidationError>();
  private Boolean hasErrors;

  public String getId()
  {
    return id;
  }

  public void setId(String id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public List<DemographicsFormField> getField()
  {
    return field;
  }

  public void setField(List<DemographicsFormField> field)
  {
    this.field = field;
  }

  public List<DemographicsFormMember> getMember()
  {
    return member;
  }

  public void setMember(List<DemographicsFormMember> member)
  {
    this.member = member;
  }

  public List<DemographicsFormValidationErrorConfig> getValidationErrorConfig()
  {
    return validationErrorConfig;
  }

  public void setValidationErrorConfig(
    List<DemographicsFormValidationErrorConfig> validationErrorConfig)
  {
    this.validationErrorConfig = validationErrorConfig;
  }

  public List<DemographicsFormValidationError> getValidationError()
  {
    return validationError;
  }

  public void setValidationError(List<DemographicsFormValidationError> validationError)
  {
    this.validationError = validationError;
  }

  public Boolean getHasErrors()
  {
    return hasErrors;
  }

  public void setHasErrors(Boolean hasErrors)
  {
    this.hasErrors = hasErrors;
  }
}