package com.connecture.shopping.process.common;

import java.util.Locale;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.connecture.shopping.process.domain.PageComponentInfo;

public class PageComponentProcess
{
  private SessionFactory sessionFactory;
  
  public PageComponentInfo findByComponentKey(String componentKey, Locale locale)
  {
    return (PageComponentInfo) sessionFactory.getCurrentSession().createCriteria(PageComponentInfo.class)
      .add(Restrictions.eq("componentKey", componentKey))
      .add(Restrictions.eq("locale", locale))
      .uniqueResult();
  }

  public void save(PageComponentInfo pageComponent)
  {
    sessionFactory.getCurrentSession().saveOrUpdate(pageComponent);
  }
  
  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }
}
