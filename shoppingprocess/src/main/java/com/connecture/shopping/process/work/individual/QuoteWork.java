package com.connecture.shopping.process.work.individual;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingQuotePlanData;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.camel.request.IAToShoppingQuotePlanDataRequest;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;

public class QuoteWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(QuoteWork.class);
  
  // inputs
  private String transactionId;

  // individual request objects
  private IAToShoppingIndividualDataRequest individualDataRequest;
  
  // Value for update
  private JSONObject value;

  // IFP data
  private IAToShoppingData individualData;

  private IAToShoppingQuotePlanDataRequest quotePlanDataRequest;
  private JSONObject rider = new JSONObject();
  
  @Override
  public void get() throws Exception
  {
    // if transactionId is available, make a data request to IA
    if (StringUtils.isNotBlank(transactionId))
    {
      individualData = this.getIndividualData();
    }
  }

  private IAToShoppingData getIndividualData() throws Exception
  {
    individualDataRequest.setTransactionId(transactionId);
    individualDataRequest.setContext(ShoppingContext.ANONYMOUS);
    
    DataProvider<IAToShoppingData> dataProvider = new CamelRequestingDataProvider<IAToShoppingData>(individualDataRequest);
    return getCachedData(ShoppingDataProviderKey.INDIVIDUAL_DATA, dataProvider);
  }

  @Override
  public void update() throws Exception
  {
    if (null != value)
    {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Saving Quote: " + value.toString(4));
      }
      
      quotePlanDataRequest.setTransactionId(transactionId);
      List<Long> planIds = new ArrayList<Long>();
      Map<Long, Long> selectedRiders = new HashMap<Long, Long>();
      if (value.has("plans"))
      {
        JSONArray planArray = value.getJSONArray("plans");
        for (int i = 0; i < planArray.length(); i++)
        {
        	Long plandId = planArray.getJSONObject(i).getLong("planId");
          planIds.add(planArray.getJSONObject(i).getLong("planId"));
          composeRiders(plandId, selectedRiders);
        }
      }
      quotePlanDataRequest.setRidersMap(selectedRiders);  
      quotePlanDataRequest.setQuotePlanIds(planIds);
      
      individualData = quotePlanDataRequest.submitRequest();
    }
  }
  private void composeRiders(Long plandId, Map<Long, Long> selectedRiders) throws Exception{
	  if (value.has("selectedRiders"))
      {
		  JSONObject riderObject = value.getJSONObject("selectedRiders");
		  if(riderObject != null && riderObject.has("DENTAL")){
		  JSONArray keys = riderObject.getJSONObject("DENTAL").names();
		  if(keys != null)
			  for (int i = 0; i < keys.length(); i++)
			  {
				  
				  try {
					 
					  Long obj = riderObject.getJSONObject("DENTAL").getLong(keys.getString(i));
        			selectedRiders.put(plandId, obj);
				  }catch(Exception r){
        		
				  }
          //planIds.add(planArray.getJSONObject(i).getLong("planId"));
        }
		  }
      }
  }
  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    JSONArray quotePlansArray = new JSONArray();
    JSONObject plansObject = new JSONObject();
    if (null != individualData && null != individualData.getQuotePlanData())
    {
      populateQuoteModelFromIndividualData(quotePlansArray);
    }
    // Otherwise, it will be empty
    plansObject.put("plans", quotePlansArray);
    plansObject.put("selectedRiders", rider);
    jsonObject.put("quote", plansObject);
    
    return jsonObject;
  }

  void populateQuoteModelFromIndividualData(JSONArray quotePlansArray) throws JSONException
  {
    // Load the quote model, currenty just plans, from the incoming IFP Data if available
    for (IAToShoppingQuotePlanData data : individualData.getQuotePlanData())
    {
      JSONObject quotePlanObject = new JSONObject();
      quotePlanObject.put("planId", data.getPlanId());
      //Rider change
      Set<Long> set = findRiders(data);
      JSONObject jsonObject = new JSONObject();
      if(rider.has("DENTAL"))
    	  jsonObject = rider.getJSONObject("DENTAL");
      if(!CollectionUtils.isEmpty(set)){
    	  
    	  for(Long id : set)
    		  jsonObject.put( data.getPlanId()+"", id);
    	  rider.put("DENTAL", jsonObject);
    			  
     // quotePlanObject.put();
      }
      //Rider Change
      quotePlansArray.put(quotePlanObject);
    }
  }

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public JSONObject getValue()
  {
    return value;
  }

  public void setValue(JSONObject value)
  {
    this.value = value;
  }

  public void setIndividualDataRequest(IAToShoppingIndividualDataRequest individualDataRequest)
  {
    this.individualDataRequest = individualDataRequest;
  }

  public void setQuotePlanDataRequest(IAToShoppingQuotePlanDataRequest quotePlanDataRequest)
  {
    this.quotePlanDataRequest = quotePlanDataRequest;
  }
  //<BCBSNE Rider change>
  private Set<Long> findRiders(
			Object obj) {
		
	  	try{
	  		Class noparams[] = {};
	  		Method method = obj.getClass().getDeclaredMethod("getValues", noparams);
			if(method != null){
				Object returnVal = method.invoke(obj, null);
				Set<Long> riders = (Set<Long>) ((Map<String, Object>)returnVal).get("riders");
				return riders;
			}
			return null;
		}catch(Exception exception){
			//As the required method does not exist so return nothing from this method
			return null;
		}
		
	}
  //</BCBSNe Rider cHange>
}
