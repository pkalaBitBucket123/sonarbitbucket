package com.connecture.shopping.process.work.employee;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingMember;
import com.connecture.shopping.process.camel.request.ConsumerTierDataRequest;
import com.connecture.shopping.process.camel.request.ProductDataRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.service.data.converter.ConsumerDataConverterUtil;
import com.connecture.shopping.process.work.core.CostEstimatorCostsWork;
import com.connecture.shopping.process.work.core.ProductPlanWork;

public class SGCostEstimatorCostsWork extends CostEstimatorCostsWork
{
  private ConsumerTierDataRequest consumerTierDataRequest;
  private ProductDataRequest productDataRequest;
  
  private String productServerGroup;
  
  @Override
  protected String getEffectiveDate()
  {
    JSONObject accountObject = getAccount().getAccount(getTransactionId());
    
    return accountObject.optString("effectiveDate", null);
  }

  @Override
  protected Long getEffectiveDateMS()
  {
    String effectiveDateString = getEffectiveDate();
    Long effectiveDateMs = null != effectiveDateString ? ShoppingDateUtils.stringToDate(effectiveDateString).getTime() : 0L;
    
    return effectiveDateMs;
  }

  @Override
  protected ProductPlanData getPlanData() throws JSONException, ParseException, Exception
  {
    JSONObject accountObject = getAccount().getAccount(getTransactionId());
    
    // set eligibility group Id and members on the consumerTierDataRequest
    JSONArray membersJSON = accountObject.optJSONArray("members");
     
    if (membersJSON != null)
    {
    // 1. get eligibilityGroupId
      Long eligibilityGroupId = accountObject.optLong("eligibilityGroupId");

      if (eligibilityGroupId != null)
      {
        consumerTierDataRequest.setEligibilityGroupId(eligibilityGroupId);
      }
      else
      {
        consumerTierDataRequest.setEligibilityGroupId(null);
      }

      // 2. get shopping member data
      List<ShoppingMember> shoppingMembers = ConsumerDataConverterUtil.buildShoppingMembers(membersJSON);
      consumerTierDataRequest.setShoppingMembers(shoppingMembers);
    }

    Set<String> selectedProductLines = ConsumerDataConverterUtil
      .determineSelectedProductLines(membersJSON);
    
    ProductPlanWork productPlanWork = new ProductPlanWork();
    productPlanWork.setDataCache(getDataCache());
    productPlanWork.setConsumerTierDataRequest(consumerTierDataRequest);
    productPlanWork.setProductDataRequest(productDataRequest);
    ProductPlanData productPlanData = productPlanWork.getProductPlanData(getTransactionId(), productServerGroup, selectedProductLines);
    return productPlanData;
  }

  @Override
  protected String getPlanIdentifier(PlanData planData)
  {
    // For SG we use ExtRefId
    return planData.getExtRefId();
  }

  @Override
  protected boolean isApplicableForCostEstimation(JSONObject memberObject) throws JSONException, Exception
  {
    boolean isApplicableForCostEstimation = false;
    
    // For SG we check if the member has coverage selected that matches the coverage being
    //   estimated
    String productLineForEstimation = getProductLineForEstimation();
    
    JSONArray products = memberObject.optJSONArray("products");
    for (int p = 0; null != products && p < products.length() && !isApplicableForCostEstimation; p++)
    {
      String productLineCode = products.getJSONObject(p).optString("productLineCode");
      isApplicableForCostEstimation = null != productLineCode && productLineCode.equals(productLineForEstimation); 
    }
    
    return isApplicableForCostEstimation;
  }

  public void setConsumerTierDataRequest(ConsumerTierDataRequest consumerProductsDataRequest)
  {
    this.consumerTierDataRequest = consumerProductsDataRequest;
  }

  public void setProductDataRequest(ProductDataRequest productPlanDataRequest)
  {
    this.productDataRequest = productPlanDataRequest;
  }

  public void setProductServerGroup(String productServerGroup)
  {
    this.productServerGroup = productServerGroup;
  }
}
