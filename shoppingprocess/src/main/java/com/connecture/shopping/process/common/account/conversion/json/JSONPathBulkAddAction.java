package com.connecture.shopping.process.common.account.conversion.json;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONPathBulkAddAction implements JSONPathAction
{
  // List of objects to be added
  private List<Object> allObjectsToAdd = new ArrayList<Object>();
  
  public JSONPathBulkAddAction()
  {
  }
  
  public JSONPathBulkAddAction(List<Object> allObjectsToAdd)
  {
    this.allObjectsToAdd.addAll(allObjectsToAdd);
  }
  
  @Override
  public boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException
  {
    boolean success = false;
    if (isValidFor(state))
    {
      Object defaultValue = null;
      if (!PathState.MISSING_SUBPATH.equals(state) && allObjectsToAdd.size() > 0)
      {
        defaultValue = allObjectsToAdd.remove(0);
      }
      
      Object newValue = defaultValue;
      if (PathState.MISSING_SUBPATH.equals(state) || null == defaultValue) {
        newValue = new JSONObject();
      }
      jsonObject.put(key, newValue);
      success = true;
    }
    return success;
  }

  @Override
  public boolean isValidFor(PathState state)
  {
    return JSONPathAction.PathState.MISSING.equals(state) || JSONPathAction.PathState.MISSING_SUBPATH.equals(state);
  }
}
