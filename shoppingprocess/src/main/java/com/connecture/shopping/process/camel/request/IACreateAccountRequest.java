package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;


public class IACreateAccountRequest extends BaseCamelRequest<JSONObject>
{
  @Produce(uri="{{camel.shopping.producer.assignTransIdAnonymous}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
  
  public void setTransactionId(String transactionId)
  {
    headers.put(TRANSACTION_ID_KEY, transactionId);
  }
}
