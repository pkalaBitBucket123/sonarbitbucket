package com.connecture.shopping.process.common.account.conversion.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPRenameConverter extends AVPConverterImpl
{
  private String oldPath;
  private String newPath;
  private String reason;
  
  public AVPRenameConverter() {
    super(AVPConverterType.RENAME);
  }
  
  @Override
  public void apply(JSONObject jsonObject) throws JSONException
  {
    JSONPathUtil.rename(jsonObject, oldPath, newPath);
  }

  public String getOldPath()
  {
    return oldPath;
  }

  public void setOldPath(String oldPath)
  {
    this.oldPath = oldPath;
  }

  public String getNewPath()
  {
    return newPath;
  }

  public void setNewPath(String newPath)
  {
    this.newPath = newPath;
  }

  public String getReason()
  {
    return reason;
  }

  public void setReason(String reason)
  {
    this.reason = reason;
  }
}
