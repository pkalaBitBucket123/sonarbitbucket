package com.connecture.shopping.process.camel.request;

import java.util.List;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.shopping.process.service.data.ProductPlanData;

public class ProductDataRequest extends BaseCamelRequest<ProductPlanData>
{
  public static final String PRODUCT_XREFS_KEY = "productXrefIds";
  public static final String EFF_DATE_KEY = "effDateMs";
  public static final String GROUP_ID_KEY = "groupId";
  
  
  @Produce(uri = "{{camel.shopping.producer.products}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setPlanExtRefIds(List<String> planExtRefIds)
  {
    headers.put(PRODUCT_XREFS_KEY, planExtRefIds);
  }

  public void setEffectiveDate(long effectiveDate)
  {
    headers.put(EFF_DATE_KEY, effectiveDate);
  }

  public void setGroup(String group)
  {
    headers.put(GROUP_ID_KEY, group);
  }
}
