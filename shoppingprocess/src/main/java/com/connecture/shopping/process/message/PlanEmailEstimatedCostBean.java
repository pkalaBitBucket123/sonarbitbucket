package com.connecture.shopping.process.message;

public class PlanEmailEstimatedCostBean
{
  private boolean estimated;
  private String minimumCost;
  private String expectedCost;
  private String maximumCost;
  private Double estimatedCostPercentage;
  
  /**
   * @return the estimated
   */
  public boolean isEstimated()
  {
    return estimated;
  }
  /**
   * @param estimated the estimated to set
   */
  public void setEstimated(boolean estimated)
  {
    this.estimated = estimated;
  }
  /**
   * @return the minimumCost
   */
  public String getMinimumCost()
  {
    return minimumCost;
  }
  /**
   * @param minimumCost the minimumCost to set
   */
  public void setMinimumCost(String minimumCost)
  {
    this.minimumCost = minimumCost;
  }
  /**
   * @return the expectedCost
   */
  public String getExpectedCost()
  {
    return expectedCost;
  }
  /**
   * @param expectedCost the expectedCost to set
   */
  public void setExpectedCost(String expectedCost)
  {
    this.expectedCost = expectedCost;
  }
  /**
   * @return the maximumCost
   */
  public String getMaximumCost()
  {
    return maximumCost;
  }
  /**
   * @param maximumCost the maximumCost to set
   */
  public void setMaximumCost(String maximumCost)
  {
    this.maximumCost = maximumCost;
  }
  /**
   * @return the estimatedCostPercentage
   */
  public Double getEstimatedCostPercentage()
  {
    return estimatedCostPercentage;
  }
  /**
   * @param estimatedCostPercentage the estimatedCostPercentage to set
   */
  public void setEstimatedCostPercentage(Double estimatedCostPercentage)
  {
    this.estimatedCostPercentage = estimatedCostPercentage;
  }
}