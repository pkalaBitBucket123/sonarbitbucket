package com.connecture.shopping.process.common;

import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.context.ThreadLocalSessionContext;
import org.hibernate.engine.SessionFactoryImplementor;

/**
 * A subclass of ThreadLocalSessionContext that automatically starts a new
 * transaction if no transaction exists when the session is retrieved.
 */
public class AutoCreateTxSessionContext extends ThreadLocalSessionContext
{
//  private Logger logger = Logger.getLogger(AutoCreateTxSessionContext.class);

  public AutoCreateTxSessionContext(SessionFactoryImplementor factory)
  {
    super(factory);
  }

  @Override
  protected Session buildOrObtainSession()
  {
    Session session = super.buildOrObtainSession();
    Transaction trans = session.getTransaction();
    if (session.getTransaction() == null || !trans.isActive())
    {
      session.beginTransaction();
    }
    return session;
  }

}
