package com.connecture.shopping.process.service.data;

import java.util.Comparator;
import java.util.List;

public class DemographicsConfigFieldData
{
  private String key;
  private String name;
  private String dataType;
  private String styleClass;
  private String headerStyleClass;
  private String inputType;
  private String label;
  private boolean applyMask;
  private int size;
  private int order;
  private List<DemographicsConfigFieldOptionData> options;
  private DemographicsConfigFieldMaskData mask;

  public static Comparator<DemographicsConfigFieldData> FieldOrderComparator = new Comparator<DemographicsConfigFieldData>()
    {

      public int compare(DemographicsConfigFieldData field1, DemographicsConfigFieldData field2)
      {
        // ascending order
        return Integer.valueOf(field1.getOrder()).compareTo(Integer.valueOf(field2.getOrder()));

      }

    };

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getStyleClass()
  {
    return styleClass;
  }

  public void setStyleClass(String styleClass)
  {
    this.styleClass = styleClass;
  }

  public String getHeaderStyleClass()
  {
    return headerStyleClass;
  }

  public void setHeaderStyleClass(String headerStyleClass)
  {
    this.headerStyleClass = headerStyleClass;
  }

  public String getInputType()
  {
    return inputType;
  }

  public void setInputType(String inputType)
  {
    this.inputType = inputType;
  }

  public int getSize()
  {
    return size;
  }

  public void setSize(int size)
  {
    this.size = size;
  }

  public String getDataType()
  {
    return dataType;
  }

  public void setDataType(String dataType)
  {
    this.dataType = dataType;
  }

  public boolean isApplyMask()
  {
    return applyMask;
  }

  public void setApplyMask(boolean applyMask)
  {
    this.applyMask = applyMask;
  }

  public int getOrder()
  {
    return order;
  }

  public void setOrder(int order)
  {
    this.order = order;
  }

  public List<DemographicsConfigFieldOptionData> getOptions()
  {
    return options;
  }

  public void setOptions(List<DemographicsConfigFieldOptionData> options)
  {
    this.options = options;
  }

  public DemographicsConfigFieldMaskData getMask()
  {
    return mask;
  }

  public void setMask(DemographicsConfigFieldMaskData mask)
  {
    this.mask = mask;
  }
}