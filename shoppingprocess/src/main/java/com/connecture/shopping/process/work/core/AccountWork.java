package com.connecture.shopping.process.work.core;

import org.json.JSONObject;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkImpl;

public class AccountWork extends WorkImpl
{
  // inputs
  protected String transactionId;
  // Value for update
  private JSONObject value;
  // Value for get
  protected JSONObject accountJSON;

  @Override
  public void get() throws Exception
  {
    Account account = getAccount();

    accountJSON = account.getOrCreateAccount(transactionId);
  }

  @Override
  public void update() throws Exception
  {
    Account account = getAccount();

    // Try to get an existing AccountInfo
    get();

    account.updateAccountInfo(transactionId, value);
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();
    
    // Since we are trying to be RESTful with our works here, if this is an update
    // we do not want to return anything unless something has changed.  Right now we do
    // not change anything during an UPDATE of account, so return nothing.  
    if (null != accountJSON && !WorkConstants.Method.UPDATE.equals(getMethod()))
    {
      JSONObject accountData = accountJSON;
      jsonObject.put("account", accountData);
    }

    return jsonObject;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setValue(JSONObject value)
  {
    this.value = value;
  }

}
