package com.connecture.shopping.process.service.data.cache;

public interface DataProvider<Data>
{
  Data fetchData() throws Exception;
}
