/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

import java.util.Comparator;

/**
 * 
 */
public class PlanBenefitCategoryDataComparator implements Comparator<PlanBenefitCategoryData>
{
  /**
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(PlanBenefitCategoryData planBenefitCategoryData1, PlanBenefitCategoryData planBenefitCategoryData2)
  {
    return Integer.valueOf(planBenefitCategoryData1.getSortOrder()).compareTo(planBenefitCategoryData2.getSortOrder());
  }
}
