package com.connecture.shopping.process.service.data.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingMember;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.service.plan.data.PlanContributionData;

public class ConsumerDataConverterUtil
{
  // private static Logger LOG = Logger.getLogger(ConsumerDataConverterUtil.class);

  public static Long getPolicyEffectiveTime(JSONObject jsonObject) throws Exception
  {
    JSONObject eventJSON = jsonObject.getJSONObject("event");
    String dateStr = eventJSON.getString("Coverage_Effective_Date");
    Date effectiveDate = ShoppingDateUtils.stringToDate(dateStr);
    return effectiveDate.getTime();
  }

  public static PlanContributionData getPlanContributionData(JSONObject planJSON)
    throws JSONException
  {
    PlanContributionData contributionData = new PlanContributionData();

    Double resultAmount = planJSON.getDouble("Employer_Contribution");
    contributionData.setAmount(resultAmount);

    return contributionData;
  }

  public static Double getCostData(JSONObject planJSON) throws Exception
  {
    Double costAmount = null;

    if (planJSON.has("EE_Cost"))
    {
      costAmount = planJSON.getDouble("EE_Cost");
    }
    else
    {
      throw new Exception("No Cost data found for plan");
    }

    return costAmount;
  }

  public static Set<String> determineSelectedProductLines(JSONArray membersJSON)
    throws JSONException
  {
    Set<String> selectedProductLines = new HashSet<String>();
    if (membersJSON != null)
    {
      for (int i = 0; i < membersJSON.length(); i++)
      {

        JSONObject memberJSON = membersJSON.getJSONObject(i);

        JSONArray products = memberJSON.optJSONArray("products");

        if (products != null && products.length() > 0)
        {
          for (int j = 0; j < products.length(); j++)
          {
            JSONObject productLine = products.getJSONObject(j);
            // product line code is what comes from product server
            String productLineCode = productLine.getString("productLineCode");
            selectedProductLines.add(productLineCode);
          }
        }
      }
    }

    return selectedProductLines;
  }

  public static List<ShoppingMember> buildShoppingMembers(JSONArray membersJSON)
    throws JSONException, ParseException
  {
    // return object
    List<ShoppingMember> shoppingMembers = new ArrayList<ShoppingMember>();

    for (int i = 0; i < membersJSON.length(); i++)
    {
      JSONObject memberJSON = membersJSON.getJSONObject(i);

      // build out demographics for each member

      ShoppingMember member = new ShoppingMember();

      // birth date
      Date date = ShoppingDateUtils.stringToDate(memberJSON.getString("birthDate"));

      member.setDateOfBirth(date);

      // first name
      JSONObject name = memberJSON.getJSONObject("name");
      String firstName = name.getString("first");
      member.setFirstName(firstName);

      // gender type key
      String gender = memberJSON.optString("gender");
      member.setGenderTypeKey(gender);

      // set member ref name (id if existing member, refName (int) if new)
      String externalId = memberJSON.optString("memberExtRefId");
      member.setNew(memberJSON.getBoolean("isNew"));  
      String memberRefName = memberJSON.getString("memberRefName");
      member.setMemberExtRefId(member.isNew() ? memberRefName : externalId); 

      // relationship
      String relationshipTypeKey = memberJSON.getString("memberRelationship");
      member.setRelationshipTypeKey(relationshipTypeKey);

      // smoker
      String smoker = memberJSON.optString("isSmoker", "n");
      member.setSmoker(smoker);

      // build out product line elections for each member
      List<String> selectedProductLines = new ArrayList<String>();
      JSONArray products = memberJSON.optJSONArray("products");
      if (products != null && products.length() > 0)
      {
        for (int j = 0; j < products.length(); j++)
        {
          JSONObject productLine = products.getJSONObject(j);
          // product line name is what comes from product server, and
          // it is
          // what the rulesEngine in GPA is expecting
          String productLineCode = productLine.getString("productLineCode");
          selectedProductLines.add(productLineCode);
        }
      }

      member.setSelectedProductLineKeys(selectedProductLines);

      shoppingMembers.add(member);
    }

    return shoppingMembers;
  }

  public static int calculateAgeAsOfDate(Date birthDate, Date asOfDate)
  {
    Calendar birthDateCalendar = new GregorianCalendar();
    birthDateCalendar.setTime(birthDate);

    Calendar asOfDateCalendar = new GregorianCalendar();
    asOfDateCalendar.setTime(asOfDate);

    int memberBirthYear = birthDateCalendar.get(Calendar.YEAR);
    int asOfYear = asOfDateCalendar.get(Calendar.YEAR);

    // default for memberBirthMonth >= currentMonth
    int age = asOfYear - memberBirthYear;

    int memberBirthMonth = birthDateCalendar.get(Calendar.MONTH);
    int asOfMonth = asOfDateCalendar.get(Calendar.MONTH);

    if (asOfMonth < memberBirthMonth)
    {
      // one less year if we are before the birth month in the same year
      age--;
    }
    else if (asOfMonth == memberBirthMonth)
    {
      int memberBirthDay = birthDateCalendar.get(Calendar.DATE);
      int asOfDay = asOfDateCalendar.get(Calendar.DATE);

      if (asOfDay < memberBirthDay)
      {
        // One less if we are before the birth day in the same month in the same year
        age--;
      }
    }

    return age;
  }
}
