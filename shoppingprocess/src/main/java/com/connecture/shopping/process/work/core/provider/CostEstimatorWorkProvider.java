package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.CostEstimatorWork;

public abstract class CostEstimatorWorkProvider implements WorkProviderImpl<CostEstimatorWork>
{
  @Override
  public CostEstimatorWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    CostEstimatorWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}
