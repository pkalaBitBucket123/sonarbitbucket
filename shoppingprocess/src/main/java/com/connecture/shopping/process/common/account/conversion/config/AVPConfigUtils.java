package com.connecture.shopping.process.common.account.conversion.config;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.converter.AVPConverterUtils;
import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;

public abstract class AVPConfigUtils
{
  public static AVPConfig valueOf(JSONObject versionConfigJSON) throws JSONException
  {
    List<String> errors = validateJSON(versionConfigJSON);
    if (!errors.isEmpty())
    {
      throw new JSONException("Errors detected in configuration\n" + errors);
    }
    
    // no errors, so process the JSON
    return createAVPConfigFromJSON(versionConfigJSON);
  }

  public static List<String> validateJSON(JSONObject configurationObject) throws JSONException
  {
    List<String> errors = new ArrayList<String>();
    
    JSONValidationUtils.checkForMissingField(errors, configurationObject, "newVersion");
    JSONValidationUtils.checkForMissingField(errors, configurationObject, "previousVersion");
      
    if (configurationObject.has("converters"))
    {
      JSONArray converterObjects = configurationObject.getJSONArray("converters");
      
      for (int i = 0; i < converterObjects.length(); i++)
      {
        JSONObject converterObject = converterObjects.getJSONObject(i);
        JSONValidationUtils.checkForMissingField(errors, "converters[" + i + "]", converterObject, "type");
        
        errors.addAll(AVPConverterUtils.validateJSON(converterObject));
      }
    }
      
    return errors;
  }

  private static AVPConfig createAVPConfigFromJSON(JSONObject configurationObject) throws JSONException
  {
    AVPConfig config = new AVPConfig();
    
    config.setNewVersion(configurationObject.getString("newVersion"));
    config.setPreviousVersion(configurationObject.getString("previousVersion"));

    JSONArray converterObjects = configurationObject.getJSONArray("converters");
    
    for (int i = 0; i < converterObjects.length(); i++)
    {
      JSONObject converterObject = converterObjects.getJSONObject(i);
      config.addConverter(AVPConverterUtils.createAVPConverter(converterObject));
    }
    
    return config;
  }
}
