package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.ProductLinesWork;

public abstract class ProductLinesWorkProvider implements WorkProviderImpl<ProductLinesWork>
{
  @Override
  public ProductLinesWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject input,
    JSONObject data) throws JSONException
  {
    ProductLinesWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}
