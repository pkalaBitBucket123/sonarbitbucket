package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;

public class AVPAddConverterUtil implements AVPConverterUtil<AVPAddConverter>
{
  @Override
  public List<String> validateJSON(JSONObject converterObject) throws JSONException
  {
    List<String> errors = new ArrayList<String>();

    JSONValidationUtils.checkForMissingField(errors, converterObject, "path");
    JSONValidationUtils.checkForMissingField(errors, converterObject, "reason");

    return errors;
  }

  @Override
  public AVPAddConverter createConverter(JSONObject converterObject) throws JSONException
  {
    AVPAddConverter converter = new AVPAddConverter();

    converter.setPath(converterObject.getString("path"));

    if (converterObject.has("defaultValue"))
    {
      converter.setDefaultValue(converterObject.get("defaultValue"));
    }

    converter.setReason(converterObject.getString("reason"));

    return converter;
  }
}
