package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.core.ActorWork;

public abstract class ActorWorkProvider implements WorkProviderImpl<ActorWork>
{
  @Override
  public ActorWork getWork(
    String transactionId,
    String requestKey,
    WorkConstants.Method method,
    JSONObject input,
    JSONObject data) throws JSONException
  {
    ActorWork retVal = createWork();
    retVal.setTransactionId(transactionId);
    
    return retVal;
  }

}
