package com.connecture.shopping.process;

public class DataConversionException extends IntegrationException
{
  public DataConversionException()
  {
    super();
  }

  public DataConversionException(String message)
  {
    super(message);
  }

  public DataConversionException(Throwable cause)
  {
    super(cause);
  }

  public DataConversionException(String message, Throwable cause)
  {
    super(message, cause);
  }
}
