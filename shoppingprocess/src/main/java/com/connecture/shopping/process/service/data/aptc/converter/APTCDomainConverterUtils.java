package com.connecture.shopping.process.service.data.aptc.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.rules.aptc.APTCDomain;

public class APTCDomainConverterUtils
{
  private static final String APPLICATION = "Application";

  private static final String APTC = "APTC";
  private static final String APTC_ELECTED_AMOUNT = "APTC_Elected_Amt";
  private static final String STATE_CODE = "State_Code";

  // plan-level attributes
  private static final String SELECTED_PLANS = "Plans";
 
  // shopping-ONLY attributes
  // plans that aren't selected
  private static final String AVAILABLE_PLANS = "Unselected_Plans";

  // TO JSON
  
  /**
   * The intent here is to take the APTCDomain object and convert it into a
   * JSONObject that reflects exactly the structure of the AppConfig domain used
   * to run the APTC Apportionment Rule in the IFP_Enrollment rules package.
   * This way, Shopping and AppConfig will use a common rule for determining net
   * plan premiums within the Shopping module and the AppConfig Shopping Cart
   * widget.
   * 
   * @param aptcDomain
   * @return
   */
  public static JSONObject toJSON(APTCDomain aptcDomain) throws JSONException
  {
    JSONObject aptcDomainJSON = new JSONObject();

    JSONObject applicationJSON = new JSONObject();
    JSONObject aptcJSON = new JSONObject();
    aptcJSON.put(STATE_CODE, aptcDomain.getStateCode());
    aptcJSON.put(APTC_ELECTED_AMOUNT, aptcDomain.getElectedAmount());
    applicationJSON.put(APTC, aptcJSON);

    applicationJSON.put(SELECTED_PLANS, APTCDomainPlanConverterUtils.toJSON(aptcDomain.getSelectedPlans()));
    
    // shopping-ONLY attribute
    applicationJSON.put(AVAILABLE_PLANS, APTCDomainPlanConverterUtils.toJSON(aptcDomain.getAvailablePlans()));

    aptcDomainJSON.put(APPLICATION, applicationJSON);

    return aptcDomainJSON;
  }

  // FROM JSON
  
  public static APTCDomain fromJSON(JSONObject aptcDomainJSON) throws JSONException
  {
    APTCDomain aptcDomain = new APTCDomain(); 
    JSONObject rootDomainJSON = aptcDomainJSON.getJSONObject(APPLICATION);
    aptcDomain.setSelectedPlans(APTCDomainPlanConverterUtils.fromJSON(rootDomainJSON.optJSONArray(SELECTED_PLANS)));
    aptcDomain.setAvailablePlans(APTCDomainPlanConverterUtils.fromJSON(rootDomainJSON.optJSONArray(AVAILABLE_PLANS)));
    return aptcDomain;
  }
}