/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;


/**
 * 
 */
public class PlanBenefitValueTypeGroupData
{
  private String name;
  
  /**
   * @return the name
   */
  public String getName()
  {
    return name;
  }

  /**
   * @param name the new value of name
   */
  public void setName(String name)
  {
    this.name = name;
  }
}
