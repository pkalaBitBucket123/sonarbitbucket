package com.connecture.shopping.process.common.account.conversion.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPAddConverter extends AVPConverterImpl
{
  private String path;
  private Object defaultValue;
  private String reason;
  
  public AVPAddConverter() {
    super(AVPConverterType.ADD);
  }
  
  @Override
  public void apply(JSONObject jsonObject) throws JSONException
  {
    JSONPathUtil.add(jsonObject, path);
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }
  
  public Object getDefaultValue()
  {
    return defaultValue;
  }

  public void setDefaultValue(Object defaultValue)
  {
    this.defaultValue = defaultValue;
  }

  public String getReason()
  {
    return reason;
  }

  public void setReason(String reason)
  {
    this.reason = reason;
  }
}
