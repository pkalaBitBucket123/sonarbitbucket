package com.connecture.shopping.process.service.data.availability;

import java.util.ArrayList;
import java.util.List;

public class AvailabilityProduct
{
  private String internalCode;
  private String productLineCode;
  private String type;
  private List<AvailabilityProductBenefit> benefits = new ArrayList<AvailabilityProductBenefit>();
  private String availabilityCode;
  private String availabilityReason;
  
  public String getInternalCode()
  {
    return internalCode;
  }
  public void setInternalCode(String internalCode)
  {
    this.internalCode = internalCode;
  }
  public String getProductLineCode()
  {
    return productLineCode;
  }
  public void setProductLineCode(String productLineCode)
  {
    this.productLineCode = productLineCode;
  }
  public String getType()
  {
    return type;
  }
  public void setType(String type)
  {
    this.type = type;
  }
  public List<AvailabilityProductBenefit> getBenefits()
  {
    return benefits;
  }
  public void setBenefits(List<AvailabilityProductBenefit> benefits)
  {
    this.benefits = benefits;
  }
  public String getAvailabilityCode()
  {
    return availabilityCode;
  }
  public void setAvailabilityCode(String availabilityCode)
  {
    this.availabilityCode = availabilityCode;
  }
  public String getAvailabilityReason()
  {
    return availabilityReason;
  }
  public void setAvailabilityReason(String availabilityReason)
  {
    this.availabilityReason = availabilityReason;
  }
}