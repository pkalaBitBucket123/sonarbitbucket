package com.connecture.shopping.process.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

@Entity
@Table(name = "SHOP_ACCOUNT")
@GenericGenerator(name = "AccountInfo", strategy = "org.hibernate.id.enhanced.TableGenerator", parameters = {
    @Parameter(name = "segment_value", value = "SHOP_ACCOUNT"),
    @Parameter(name = "increment_size", value = "10"),
    @Parameter(name = "optimizer", value = "pooled")})
@SuppressWarnings("serial")
public class AccountInfo implements Serializable
{
  @Id
  @GeneratedValue(generator = "AccountInfo")
  @Column(name = "ACCOUNT_ID")
  private Long accountId;
  
  @Column(name = "TRANSACTION_ID")
  private String transactionId;
  
  @Column(name = "VERSION")
  private String version;
  
  @Type(type = "com.connecture.shopping.process.common.JSONObjectUserType")
  private JSONObject value;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created", nullable = false)
  private Date created;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "updated", nullable = false)
  private Date updated;
  
  public Long getAccountId()
  {
    return accountId;
  }

  public void setAccountId(Long accountId)
  {
    this.accountId = accountId;
  }

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }
  
  public String getVersion()
  {
    return version;
  }

  public void setVersion(String version)
  {
    this.version = version;
  }

  public JSONObject getValue()
  {
    return value;
  }
  
  public void setValue(JSONObject value)
  {
    this.value = value;
  }

  public Date getCreated()
  {
    return created;
  }
  
  public void setCreated(Date created)
  {
    this.created = created;
  }
  
  public Date getUpdated()
  {
    return updated;
  }
  
  public void setUpdated(Date updated)
  {
    this.updated = updated;
  }
}
