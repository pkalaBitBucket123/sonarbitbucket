package com.connecture.shopping.process.camel.request.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ListToCSVConverter
{
  private static final Logger logger = Logger.getLogger(ListToCSVConverter.class);
  
  private static final String DELIMITER_REGEX = ";\\s*";
  
  public void convert(Map<String, Object> headers, String headersToConvertStr)
  {
    if(StringUtils.isNotBlank(headersToConvertStr))
    {
      String[] headersToConvert = headersToConvertStr.split(DELIMITER_REGEX);
      
      for(String header : headersToConvert)
      {
        Object headerVal = headers.get(header);
        if(headerVal instanceof Collection)
        {
          @SuppressWarnings("unchecked")
          Collection<Object> headerCollection = (Collection<Object>) headerVal;
          String convertedHeader = convertHeader(headerCollection);
          headers.put(header, convertedHeader);
        }
        else
        {
          logger.error("Attempted to convert non-collection header " + header + "|" + String.valueOf(headerVal));
        }
      }
    }
  }
  
  private String convertHeader(Collection<Object> headerVals)
  {
    StringBuilder builder = new StringBuilder();
    Iterator<Object> valItr = headerVals.iterator();
    
    while(valItr.hasNext())
    {
      String val = String.valueOf(valItr.next());
      builder.append(val);
      
      if(valItr.hasNext())
      {
        builder.append(",");
      }
    }
    
    return builder.toString();
  }
}
