package com.connecture.shopping.process.common.account.conversion;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.connecture.shopping.process.domain.AccountInfo;

public class AVPAccountInfoDBDataStore implements AccountInfoDataStore
{
  private static Logger LOG = Logger.getLogger(AVPAccountInfoDBDataStore.class);

  private SessionFactory sessionFactory;

  @Override
  public void storeData(List<AccountInfo> accountInfoToStore) throws Exception
  {
    if (accountInfoToStore.isEmpty())
    {
      if (LOG.isDebugEnabled())
      {
        LOG.debug("No AccountInfo objects to update");
      }
    }
    else
    {
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Updating " + accountInfoToStore.size() + " AccountInfo objects to DB");
      }
      Session session = sessionFactory.getCurrentSession();

      for (AccountInfo accountInfo : accountInfoToStore)
      {
        session.update(accountInfo);
      }

      session.flush();
    }

  }

  public SessionFactory getSessionFactory()
  {
    return sessionFactory;
  }

  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }
}
