package com.connecture.shopping.process.work.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.connecture.shopping.process.service.data.EnrollmentEvent;
import com.connecture.shopping.process.service.data.Request;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.availability.Availability;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsForm;

public class ShoppingRulesEngineBean extends RulesEngineBean<ShoppingDomainModel>
{
  private Availability availability;
  private DemographicsForm demographicsForm;
  private EnrollmentEvent enrollmentEvent;
  private Date coverageEffectiveDate;

  @Override
  protected ShoppingDomainModel createDefaultDomainModel()
  {
    ShoppingDomainModel model = new ShoppingDomainModel();
    Request request = model.getRequest();
    request.setEffectiveDate(new SimpleDateFormat("MM/dd/yyyy").format(getCoverageEffectiveDate()));
    request.setId(UUID.randomUUID().toString());
    if (null != demographicsForm)
    {
      request.setDemographicsForm(demographicsForm);
    }
    if (null != enrollmentEvent) {
      request.setEnrollmentEvent(enrollmentEvent);
    }
    if (null != availability) {
      request.setAvailability(availability);
    }
    return model;
  }
  
  public Date getCoverageEffectiveDate()
  {
    return coverageEffectiveDate;
  }

  public void setCoverageEffectiveDate(Date coverageEffectiveDate)
  {
    this.coverageEffectiveDate = coverageEffectiveDate;
  }

  public void setDemographicsForm(DemographicsForm demographicsForm)
  {
    this.demographicsForm = demographicsForm;
  }
  
  public void setEnrollmentEvent(EnrollmentEvent enrollmentEvent)
  {
    this.enrollmentEvent = enrollmentEvent;
  }

  public void setAvailability(Availability availability)
  {
    this.availability = availability;
  }
}
