package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.ProviderSearchWork;

public abstract class ProviderSearchWorkProvider implements WorkProviderImpl<ProviderSearchWork>
{
  @Override
  public ProviderSearchWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject input,
    JSONObject data) throws JSONException
  {
    ProviderSearchWork work = createWork();
    work.setDomain(data);
    return work;
  }
}
