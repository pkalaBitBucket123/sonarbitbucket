package com.connecture.shopping.process.work.individual;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.RatingProductDataRequest;
import com.connecture.shopping.process.camel.request.StateKeyForCountyAndZipCodeDataRequest;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.work.core.APTCRulesEngineBean;
import com.connecture.shopping.process.work.core.CostEstimatorCostsWork;
import com.connecture.shopping.process.work.core.EnrollmentEventEffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class IFPCostEstimatorCostsWork extends CostEstimatorCostsWork
{
  private static Logger LOG = Logger.getLogger(IFPCostEstimatorCostsWork.class);
  
  // APTC crap
  private APTCRulesEngineBean aptcRulesEngineBean;
  private StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest;
  
  // Start Required Inputs
  private RatingProductDataRequest ratingProductDataRequest;
  private EnrollmentEventRulesEngineBean hcrEffectiveDateBean;
  // End Required Inputs
  
  @Override
  protected String getEffectiveDate()
  {
    hcrEffectiveDateBean.setDataCache(getDataCache());
    return EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(getAccount().getAccount(getTransactionId()), hcrEffectiveDateBean);
  } 

  @Override
  protected Long getEffectiveDateMS()
  {
    Long effectiveDateMs = 0L;
    try {
      effectiveDateMs = new SimpleDateFormat("MM/dd/yyyy").parse(getEffectiveDate()).getTime();
    }
    catch (ParseException e) {
      LOG.error("Failed to parse effectiveDate from " + getEffectiveDate(), e);
    }
    return effectiveDateMs;
  }

  @Override
  protected ProductPlanData getPlanData() throws JSONException, ParseException, Exception
  {
    RatingPlanWork work = new RatingPlanWork();
    // TODO | HACK!
    work.setAptcRulesEngineBean(aptcRulesEngineBean);
    work.setStateKeyForCountyAndZipCodeDataRequest(stateKeyForCountyAndZipCodeDataRequest);
    
    work.setDataCache(getDataCache());
    work.setRatingProductDataRequest(ratingProductDataRequest);
    work.setTransactionId(getTransactionId());
    work.setAccount(getAccount());
    work.get();
    return work.getProductPlanData();
  }
  
  @Override
  protected String getPlanIdentifier(PlanData planData)
  {
    // IFP we use InternalCode as the unique identifier
    return planData.getInternalCode();
  }
  
  @Override
  protected boolean isApplicableForCostEstimation(JSONObject memberObject) throws JSONException, Exception
  {
    return true;
  }

  @Override
  protected boolean validDemographicsZipCode(JSONObject accountJSON)
  {
    return null != accountJSON.optString("zipCode", null);
  }

  /**
   * IFP introduces the concept of a single zipCode for the "family".  Populate the consumer zipCode
   * from the Account object.
   * 
   * @see com.connecture.shopping.process.work.core.CostEstimatorCostsWork#populateMemberZipCode(Consumer, org.json.JSONObject)
   */
  @Override
  protected void populateMemberZipCode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(getAccountObject(), "zipCode"))
    {
      consumer.setZip(getAccountObject().getString("zipCode"));
    }
  }

  @Override
  protected void populateMemberRelationshipcode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(memberObject, "relationshipKey"))
    {
      String oldRelationshipCode = "Primary";
      String newRelationshipCode = memberObject.getString("relationshipKey");
      if (newRelationshipCode.equals("SPOUSE"))
      {
        oldRelationshipCode = "Spouse (opposite sex)";
      }
      else if (newRelationshipCode.equals("CHILD"))
      {
        oldRelationshipCode = "Child - dependent on subscriber for support and is legal guardian";
      }
      consumer.setRelationshipCode(oldRelationshipCode);
    }    
  }

  public void setRatingProductDataRequest(RatingProductDataRequest ratingProductDataRequest)
  {
    this.ratingProductDataRequest = ratingProductDataRequest;
  }

  public void setHcrEffectiveDateBean(EnrollmentEventRulesEngineBean hcrEffectiveDateBean)
  {
    this.hcrEffectiveDateBean = hcrEffectiveDateBean;
  }

  public void setAptcRulesEngineBean(APTCRulesEngineBean aptcRulesEngineBean)
  {
    this.aptcRulesEngineBean = aptcRulesEngineBean;
  }

  public void setStateKeyForCountyAndZipCodeDataRequest(
    StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest)
  {
    this.stateKeyForCountyAndZipCodeDataRequest = stateKeyForCountyAndZipCodeDataRequest;
  }
  
  
}
