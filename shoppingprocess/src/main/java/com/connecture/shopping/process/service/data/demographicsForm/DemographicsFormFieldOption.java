package com.connecture.shopping.process.service.data.demographicsForm;

import java.util.Comparator;

public class DemographicsFormFieldOption
{
  private String label;
  private String value;
  private Integer order;
  private Boolean defaultSelection;
  private Boolean initialSelection;
  private String dataGroupType;

  public static Comparator<DemographicsFormFieldOption> OptionOrderComparator = new Comparator<DemographicsFormFieldOption>()
    {

      public int compare(DemographicsFormFieldOption option1, DemographicsFormFieldOption option2)
      {
        // ascending order
        return Integer.valueOf(option1.getOrder()).compareTo(Integer.valueOf(option2.getOrder()));

      }

    };
  
  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(String value)
  {
    this.value = value;
  }

  public Integer getOrder()
  {
    return order;
  }

  public void setOrder(Integer order)
  {
    this.order = order;
  }

  public Boolean getDefaultSelection()
  {
    return defaultSelection;
  }

  public void setDefaultSelection(Boolean defaultSelection)
  {
    this.defaultSelection = defaultSelection;
  }

  public Boolean getInitialSelection()
  {
    return initialSelection;
  }

  public void setInitialSelection(Boolean initialSelection)
  {
    this.initialSelection = initialSelection;
  }

  public String getDataGroupType()
  {
    return dataGroupType;
  }

  public void setDataGroupType(String dataGroupType)
  {
    this.dataGroupType = dataGroupType;
  }
}