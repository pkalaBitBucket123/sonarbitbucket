package com.connecture.shopping.process.work.anonymous;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.Actor;
import com.connecture.model.data.ShoppingProducer;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.camel.request.ProducerDemographicsDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.integration.ShoppingIndividualIntegrationProcess;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.core.ConsumerDemographicsWork;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;
import com.connecture.shopping.process.work.individual.IFPActorWork;

public class AnonymousConsumerDemographicsWork extends ConsumerDemographicsWork
{
  // inputs
  private String transactionId;
  
  private static Logger LOG = Logger.getLogger(AnonymousConsumerDemographicsWork.class);

  // individual request objects
  private IAToShoppingIndividualDataRequest individualDataRequest;

  // IFP ConsumerDemographics data
  IAToShoppingData individualData;
  
  private Account persistedAccountImpl;

  private ProducerDemographicsDataRequest producerDemographicsDataRequest;

  private ShoppingProducer shoppingProducer;
  private ActorService ifpActorService;
  private String tagUrl;
  private String startDateOE;
  private String endDateOE;

  /**
   * @see com.connecture.shopping.process.work.Work#get()
   */
  @Override
  public void get() throws Exception
  {
    super.get();
    JSONObject params = getParams();
    Actor actor = ifpActorService.getActorData();
    // set the effective date for the account
    String effectiveDateStr = this.getEffectiveDate();
    
    Account account = getAccount();
    JSONObject accountJSON = account.getOrCreateAccount(null);    
    accountJSON.put("effectiveDate", effectiveDateStr);
    accountJSON.put("userRole", actor.getUserRole());
    accountJSON.put("tagUrl", tagUrl);
    accountJSON.put("endDateOE", endDateOE);
    accountJSON.put("startDateOE", startDateOE);
    accountJSON.put("effectiveMs", EffectiveDateWorkHelper.getEffectiveDateMs(effectiveDateStr));
    
    // use the same key for both, should be fine
    if (params.has(ProducerDemographicsDataRequest.PRODUCER_TOKEN))
    {
      String producerToken = params.getString(ProducerDemographicsDataRequest.PRODUCER_TOKEN);
      producerDemographicsDataRequest.setProducerToken(producerToken);
      
      DataProvider<ShoppingProducer> dataProvider = new CamelRequestingDataProvider<ShoppingProducer>(producerDemographicsDataRequest);
      shoppingProducer = getCachedData(ShoppingDataProviderKey.PRODUCER, dataProvider);
    }

    // if transactionId is available, make a data request to IA
    if (!StringUtils.isBlank(transactionId))
    {
      individualData = this.getIndividualData();
      
      Map<String, String> optionValueTypeMap = buildOptionValueTypeMap(effectiveDateStr);
      this.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, true);
      setCoverageTypeAndEffectiveDate1(accountJSON);
    }
    
    // update the account (possibly with just the effectiveMs)
    account.updateAccountInfo(null, accountJSON);
  }
  
  /**
   * @return either the HCR effective date, or the coverage effective
   * date entered by the user.  User coverage effective date overrides HCR.
   * @throws Exception
   */
  protected String getEffectiveDate() throws Exception
  {
    String effectiveDate = null;
    
    Account account = getAccount();
    JSONObject accountJSON = account.getOrCreateAccount(null); 
    
    if (accountJSON.has("coverageDate"))
    {
      effectiveDate = accountJSON.getString("coverageDate");
    }
    else
    {
      effectiveDate = super.getEffectiveDate();
    }
    
    return effectiveDate;
  }

  private IAToShoppingData getIndividualData() throws Exception
  {
    individualDataRequest.setTransactionId(transactionId);
    individualDataRequest.setContext(ShoppingContext.ANONYMOUS);
    
    DataProvider<IAToShoppingData> dataProvider = new CamelRequestingDataProvider<IAToShoppingData>(individualDataRequest);
    return getCachedData(ShoppingDataProviderKey.INDIVIDUAL_DATA, dataProvider);
  }

  /**
   * @see com.connecture.shopping.process.work.Work#processData(java.lang.Object)
   */
  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = super.processData();

    if (shoppingProducer != null)
    {
      JSONObject producerDemographics = processProducerDemographics(shoppingProducer);

      jsonObject.put("producer", producerDemographics);
    }

    return jsonObject;
  }
  
  public void setCoverageTypeAndEffectiveDate1(JSONObject accountJSON){
    try {
      if( persistedAccountImpl.getAccount(transactionId)!=null && persistedAccountImpl.getAccount(transactionId).has("coverageType") ){
        String coverageType = (String)persistedAccountImpl.getAccount(transactionId).get("coverageType");
        accountJSON.put("coverageType", coverageType);
        if( persistedAccountImpl.getAccount(transactionId).has("effectiveMs") && ("DEN".equalsIgnoreCase(coverageType) || "STH".equalsIgnoreCase(coverageType) ) ){
          long effectiveMs = (Long)persistedAccountImpl.getAccount(transactionId).get("effectiveMs");
          accountJSON.put("effectiveDate1", new SimpleDateFormat("MM/dd/yyyy").format(new Date(effectiveMs)));
        }
      }
    } catch(JSONException e){
      LOG.error("Error while while fetching coverageType & setting it in accountJSON " + e.getMessage());
    }
  }

  public void setProducerDemographicsDataRequest(
    ProducerDemographicsDataRequest producerDemographicsDataRequest)
  {
    this.producerDemographicsDataRequest = producerDemographicsDataRequest;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setIndividualDataRequest(IAToShoppingIndividualDataRequest individualDataRequest)
  {
    this.individualDataRequest = individualDataRequest;
  }
  public void setIfpActorService(ActorService ifpActorService)
  {
    this.ifpActorService = ifpActorService;
  }

  public String getTagUrl()
  {
    return tagUrl;
  }

  public void setTagUrl(String tagUrl)
  {
    this.tagUrl = tagUrl;
  }

  public void setPersistedAccountImpl(Account persistedAccountImpl)
  {
    this.persistedAccountImpl = persistedAccountImpl;
  }

  public String getStartDateOE()
  {
    return startDateOE;
  }

  public void setStartDateOE(String startDateOE)
  {
    this.startDateOE = startDateOE;
  }

  public String getEndDateOE()
  {
    return endDateOE;
  }

  public void setEndDateOE(String endDateOE)
  {
    this.endDateOE = endDateOE;
  }

 
}
