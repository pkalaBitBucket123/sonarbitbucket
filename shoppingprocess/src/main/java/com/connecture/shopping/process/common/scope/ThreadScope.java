/*
 * Copyright 2002-2006 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.connecture.shopping.process.common.scope;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * Thread scope implementation.
 */
public class ThreadScope implements Scope {
    private Logger logger = Logger.getLogger(ThreadScope.class);
    /**
     * Gets bean from scope.
     */
    public Object get(String name, ObjectFactory<?> factory) {
        Object result = null;
        
        Map<String, Object> hBeans = ThreadScopeContextHolder.currentThreadScopeAttributes().getBeanMap();
        
        if (!hBeans.containsKey(name)) {
            logger.debug("Creating bean: " + name);
            result = factory.getObject();
            
            hBeans.put(name, result);
        } else {
            result = hBeans.get(name);
        }
        return result;
    }

    /**
     * Removes bean from scope.
     */
    public Object remove(String name) {
        Object result = null;
        
        Map<String, Object> hBeans = ThreadScopeContextHolder.currentThreadScopeAttributes().getBeanMap();

        if (hBeans.containsKey(name)) {
            result = hBeans.get(name);
            
            hBeans.remove(name);
        }

        return result;
    }

    public void registerDestructionCallback(String name, Runnable callback) {
        ThreadScopeContextHolder.currentThreadScopeAttributes().registerRequestDestructionCallback(name, callback);
    }

    /**
     * Gets conversation id.  Not implemented.
     */
    public String getConversationId() {
        return null;
    }

    /**
     * ContextualObjects not supported.
     */
    public Object resolveContextualObject(String arg0)
    {
      return null;
    }
}