package com.connecture.shopping.process.service.stub;

import com.connecture.model.data.Address;
import com.connecture.model.data.Name;
import com.connecture.model.data.ShoppingProducer;


public class ProducerDemographicsDataServiceStub
{
  public ShoppingProducer getDemographicsData(String producerToken)
  {
    ShoppingProducer shoppingProducer = new ShoppingProducer();
    Name name = new Name();
    name.setFirst("Jeffrey");
    name.setLast("Lebowski");
    shoppingProducer.setName(name);
    Address address = new Address();
    address.setStreetAddress1("1234 W North Ave");
    address.setStreetAddress2("Apt 777");
    address.setCity("Chicago");
    address.setStateCode("IL");
    address.setZipCode("60657");
    shoppingProducer.setAddress(address);
    shoppingProducer.setAgency("Lebowski Rug Insurance");
    shoppingProducer.setEmailAddress("thedude@lebowski.com");
    shoppingProducer.setAreaCode("773");
    shoppingProducer.setPhoneNumber("2028660");
    shoppingProducer.setExtension("1234");
    return shoppingProducer;
  }
}