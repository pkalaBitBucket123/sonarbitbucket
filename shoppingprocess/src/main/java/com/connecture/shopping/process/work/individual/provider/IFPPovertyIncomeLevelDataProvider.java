package com.connecture.shopping.process.work.individual.provider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.RulesEngineDataRequest;
import com.connecture.shopping.process.camel.request.ShoppingRulesEngineDataRequest;
import com.connecture.shopping.process.camel.request.StateKeyForCountyAndZipCodeDataRequest;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public class IFPPovertyIncomeLevelDataProvider implements DataProvider<ShoppingDomainModel>
{
  private static Logger LOG = Logger.getLogger(IFPPovertyIncomeLevelDataProvider.class);

  private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

  private RulesEngineDataRequest<ShoppingDomainModel> rulesEngineDataRequest;
  private StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest;
  private String transactionId;
  private Long effectiveDateMs;
  private JSONObject account;
  
  private List<String> ruleSet;
  
  private String ruleEngineGroupId;

  private ShoppingDomainModel rulesEngineDomain;

  @Override
  public ShoppingDomainModel fetchData() throws Exception
  {
    rulesEngineDataRequest.setGroupId(ruleEngineGroupId);

    rulesEngineDataRequest.setRuleCodes(ruleSet);

    rulesEngineDataRequest.setEffDate(new Date().getTime());

    rulesEngineDomain.getRequest().setId("0");
    rulesEngineDomain.getRequest().setTimeInMs(new Date().getTime());
    rulesEngineDomain.getRequest().setEffectiveDate(dateFormat.format(new Date(effectiveDateMs)));

    if (null != account)
    {
      try
      {
        List<Consumer> consumers = new ArrayList<Consumer>();
        JSONObject subsidyAnswersObject = null;
        if (hasValueAndValueIsNotJSONNull(account, "tempSubsidyAnswers"))
        {
          // Use the tempSubsidyAnswers first
          subsidyAnswersObject = account.getJSONObject("tempSubsidyAnswers");
        }
        else if (hasValueAndValueIsNotJSONNull(account, "subsidyAnswers"))
        {
          // If no temp answers, then try for previous answers
          subsidyAnswersObject = account.getJSONObject("subsidyAnswers");
        }
        else {
          // No answers at all, then set the householdCount to the default,
          //    i.e. the number of members.
          if (hasValueAndValueIsNotJSONNull(account, "members"))
          {
            subsidyAnswersObject = new JSONObject();
            subsidyAnswersObject.put("householdCount", account.getJSONArray("members").length());
          }
        }
        
        String state = "";
        // If the account has a zipCode Then
        if (hasValueAndValueIsNotJSONNull(account, "zipCode"))
        {
          // Get state from account
          Integer countyId = null;
          // If the account has a countyId Then
          if (hasValueAndValueIsNotJSONNull(account, "countyId"))
          {
            // Use the countyId from the account
            countyId = account.getInt("countyId");
          }
          String zipCode = account.getString("zipCode");
          stateKeyForCountyAndZipCodeDataRequest.setCountyId(countyId);
          stateKeyForCountyAndZipCodeDataRequest.setZipCode(zipCode);
          
          // Lookup the state for the zipCode and optional countyId
          state = stateKeyForCountyAndZipCodeDataRequest.submitRequest();
        }

        // Use a default value of one, if there is no answer
        Integer householdCount = 1;
        // If the account has an answer for subsidy questions Then
        if (null != subsidyAnswersObject)
        {
          // Get the household Count from the answers
          householdCount = subsidyAnswersObject.getInt("householdCount");
          
          if (householdCount < 1) 
          {
            householdCount = 1;
          }
        }
        
        // Build the collection of consumers for the poverty level calc.
        for (int c = 0; c < householdCount; c++)
        {
          Consumer consumer = new Consumer();
          
          // Use the same state for all consumers
          consumer.setState(state);

          consumers.add(consumer);
        }
        
        rulesEngineDomain.getRequest().setConsumer(consumers);

        if (LOG.isDebugEnabled()) 
        {
          LOG.debug(new JSONObject(rulesEngineDomain).toString(4));
        }

        if (consumers.size() > 0)
        {
          rulesEngineDataRequest.setDomain(rulesEngineDomain);

          rulesEngineDomain = rulesEngineDataRequest.submitRequest();
        }
      }
      catch (JSONException e)
      {
        LOG.error("Failed to parse account from DB", e);
      }
    }
    else
    {
      LOG.error("No Account found for " + transactionId);
    }

    return rulesEngineDomain;
  }
  
  public static Boolean hasValueAndValueIsNotJSONNull(JSONObject object, String key) throws JSONException
  {
    return object.has(key) && !JSONObject.NULL.equals(object.get(key));
  }

  public void setRequest(ShoppingRulesEngineDataRequest rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }
  
  public void setStateKeyForCountyAndZipCodeDataRequest(
    StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest)
  {
    this.stateKeyForCountyAndZipCodeDataRequest = stateKeyForCountyAndZipCodeDataRequest;
  }

  public void setRulesEngineDomain(ShoppingDomainModel rulesEngineDomain)
  {
    this.rulesEngineDomain = rulesEngineDomain;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setEffectiveDateMs(Long effectiveDateMs)
  {
    this.effectiveDateMs = effectiveDateMs;
  }

  protected JSONObject getAccount()
  {
    return account;
  }

  public void setAccount(JSONObject account)
  {
    this.account = account;
  }

  public void setRuleSet(List<String> ruleSet)
  {
    this.ruleSet = ruleSet;
  }

  public void setRulesEngineGroupId(String ruleEngineGroupId)
  {
    this.ruleEngineGroupId = ruleEngineGroupId;
  }
}
