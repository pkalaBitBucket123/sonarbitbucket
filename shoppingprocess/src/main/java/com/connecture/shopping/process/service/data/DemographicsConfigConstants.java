/**
 * 
 */
package com.connecture.shopping.process.service.data;

/**
 * 
 */
public final class DemographicsConfigConstants
{
  public static final class ConfigFieldKey
  {
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String BIRTH_DATE = "BIRTH_DATE";
    public static final String RELATIONSHIP = "RELATIONSHIP";
    public static final String GENDER = "GENDER";
    public static final String TOBACCO = "TOBACCO";
  }
  
  private DemographicsConfigConstants()
  {
  }
}
