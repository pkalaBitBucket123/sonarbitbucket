package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class Profile
{
  private String id;
  private String name;
  private String Description;

  // NOTE: The following are initialized so when it is marshalled it becomes an empty JSONArray.
  // The rulesEngine requires this
  private List<UsageCategory> usageCategory = new ArrayList<UsageCategory>();

  public String getId()
  {
    return id;
  }

  public void setId(String id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getDescription()
  {
    return Description;
  }

  public void setDescription(String description)
  {
    Description = description;
  }
  
  public List<UsageCategory> getUsageCategory()
  {
    return usageCategory;
  }

  public void setUsageCategory(List<UsageCategory> usageCategory)
  {
    this.usageCategory = usageCategory;
  }
}
