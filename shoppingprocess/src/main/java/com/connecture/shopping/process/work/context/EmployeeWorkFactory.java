/**
 * 
 */
package com.connecture.shopping.process.work.context;

import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.WorkProviderImpl;

public class EmployeeWorkFactory extends ContextWorkFactoryImpl
{
  public void setAccountWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.ACCOUNT, workProvider);
  }

  public void setConsumerDemographicsWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.CONSUMER_DEMOGRAPHICS, workProvider);
  }

  public void setConsumerEligibilityWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.MEMBER_ELIGIBILITY, workProvider);
  }

  public void setProductLinesWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.PRODUCT_LINES, workProvider);
  }

  public void setProductPlanWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.PRODUCTS, workProvider);
  }

  public void setCostEstimatorWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.COST_ESTIMATOR_CONFIG, workProvider);
  }

  public void setCostEstimatorCostsWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.COST_ESTIMATOR_COSTS, workProvider);
  }

  public void setRulesEngineWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.RULES_ENGINE, workProvider);
  }

  public void setProviderSearchWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.PROVIDER_SEARCH, workProvider);
  }

  public void setApplicationConfigWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.APPLICATION_CONFIG, workProvider);
  }

  public void setActorWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.ACTOR, workProvider);
  }
}