package com.connecture.shopping.process.message;

/**
 * Bean to encapsulate all data pertaining to message recipients
 */
public class MessageRecipient
{
  private String email;

  /**
   * @return the email
   */
  public String getEmail()
  {
    return email;
  }

  /**
   * @param email the new value of email
   */
  public void setEmail(String email)
  {
    this.email = email;
  }
}