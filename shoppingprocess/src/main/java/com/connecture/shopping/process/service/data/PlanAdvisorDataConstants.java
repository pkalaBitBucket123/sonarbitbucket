/**
 * 
 */
package com.connecture.shopping.process.service.data;

/**
 * 
 */
public final class PlanAdvisorDataConstants
{
  public static final class Operators
  {
    public static final String LESS_THAN = "less than";
    public static final String GREATER_THAN = "greater than";
    public static final String EQUALS = "equals";
    public static final String IN_BETWEEN = "in between";
  }

  public static final class DisplayType
  {
    public static final String CHECKBOX = "Check Box";
    public static final String RADIO_BUTTON = "Radio Button";
    public static final String SELECT = "Dropdown";
    public static final String SLIDER = "Slider";
  }

  public static final class PlanEvaulationType
  {
    public static final String SCORING = "Scoring";
    public static final String CONFIGURATION = "Configuration Tag";
    public static final String DATA_TAG = "Data Tag";
  }
}