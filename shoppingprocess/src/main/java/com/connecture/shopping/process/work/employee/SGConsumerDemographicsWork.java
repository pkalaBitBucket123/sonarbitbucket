/**
 *
 */
package com.connecture.shopping.process.work.employee;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingProduct;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.ConsumerDemographicsDataRequest;
import com.connecture.shopping.process.camel.request.ProductDataRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.ConsumerData;
import com.connecture.shopping.process.service.data.ConsumerDemographicsData;
import com.connecture.shopping.process.service.data.ConsumerPlanData;
import com.connecture.shopping.process.service.data.GenderData;
import com.connecture.shopping.process.service.data.MemberData;
import com.connecture.shopping.process.service.data.NameData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.data.ProductLineData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.service.data.RelationshipData;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.core.ConsumerDemographicsWork;
import com.connecture.shopping.process.work.core.ProductLinesWork;

/**
 *
 */
public class SGConsumerDemographicsWork extends ConsumerDemographicsWork
  implements ProductPlanDataWork
{
  private ConsumerDemographicsDataRequest consumerDemographicsDataRequest;
  private ProductDataRequest productDataRequest;

  private String productServerGroup;

  // work data
  private ConsumerData consumerData;
  private ProductLineData productLineData;
  private ProductPlanData productPlanData;

  /**
   * @see com.connecture.shopping.process.work.Work#get()
   */
  @Override
  public void get() throws Exception
  {
    consumerDemographicsDataRequest.setContext(ShoppingContext.EMPLOYEE);
    consumerDemographicsDataRequest.setTransactionId(getTransactionId());

    // 1. get consumer demographics for SG (from GPA)

    DataProvider<ConsumerData> consumerDataProvider = new CamelRequestingDataProvider<ConsumerData>(consumerDemographicsDataRequest);
    consumerData = getCachedData(ShoppingDataProviderKey.CONSUMER_DEMOGRAPHICS, consumerDataProvider);

    // 2. set non-member data from SG consumer demographics into the account

    Account account = getAccount();

    JSONObject accountObject = account.getAccount(getTransactionId());

    accountObject.put("effectiveDate", consumerData.getDemographicsData().getPolicyEffectiveDate());
    accountObject.put("effectiveMs", consumerData.getDemographicsData().getPolicyEffectiveTime());

    Long expirationTimeMs = consumerData.getDemographicsData().getExpirationTime();
    if (null != expirationTimeMs)
    {
      accountObject.put("expirationTimeMs", expirationTimeMs);
    }

    Long eligibilityGroupId = consumerData.getDemographicsData().getEligibilityGroupId();
    // store eligibilityGroupId from demographics call;
    // can be used for 2nd integration call
    if (eligibilityGroupId != null)
    {
      accountObject.put("eligibilityGroupId", eligibilityGroupId);
    }

    account.updateAccountInfo(getTransactionId(), accountObject);

    // 3. get product line data to set into members later

    ConsumerPlanData consumerPlanData = consumerData.getPlanData();

    if (null != consumerPlanData.getPlans())
    {
      Map<String, ShoppingProduct> consumerProductsByExternalPlanId = new HashMap<String, ShoppingProduct>();

      for (ShoppingProduct planData : consumerPlanData.getPlans())
      {
        consumerProductsByExternalPlanId.put(planData.getExtRefId(), planData);
      }

      List<String> planExtRefIds = new LinkedList<String>();
      planExtRefIds.addAll(consumerProductsByExternalPlanId.keySet());

      productDataRequest.setPlanExtRefIds(planExtRefIds);
      productDataRequest.setEffectiveDate(consumerData.getDemographicsData()
        .getPolicyEffectiveTime());
      productDataRequest.setGroup(productServerGroup);

      DataProvider<ProductPlanData> productPlanDataProvider = new CamelRequestingDataProvider<ProductPlanData>(productDataRequest);
      productPlanData = getCachedData(ShoppingDataProviderKey.PRODUCTS, productPlanDataProvider);

      productLineData = ProductLinesWork.getProductLineData(productPlanData);
    }

    // Now pull in GPA members, correlating with any members already in the
    // AccountInfo::value JSON
    JSONArray memberArray = new JSONArray();

    // Used for lookup of old JSON when we have matches from GPA or new
    // (existing) members from shopping
    Map<String, JSONObject> existingMemberJSONMap = new HashMap<String, JSONObject>();

    List<MemberData> existingMembers = new ArrayList<MemberData>();

    // Used when creating new Shopping members from GPA data
    int nextRefName = 0;


    // 4. get member data from shopping account
    // builds existingMemberJSONMap and
    // existingMembers collection (objects built from JSON)

    if (accountObject.has("members"))
    {
      // Populate the collection of existingMembers from the account.members
      JSONArray existingMembersArray = accountObject.getJSONArray("members");
      for (int m = 0; m < existingMembersArray.length(); m++)
      {
        JSONObject member = existingMembersArray.getJSONObject(m);
        MemberData memberData = new MemberData();
        String memberRefName = member.getString("memberRefName");
        if (StringUtils.isNotBlank(memberRefName))
        {
          Integer currentRefName = Integer.parseInt(memberRefName);
          if (nextRefName <= currentRefName)
          {
            nextRefName = currentRefName + 1;
          }
        }

        memberData.setMemberRefName(memberRefName);
        if (member.has("memberExtRefId"))
        {
          memberData.setMemberExtRefId(member.getString("memberExtRefId"));
        }
        memberData.setBirthDate(member.getString("birthDate"));
        memberData.setMemberRelationship(member.getString("memberRelationship"));
        NameData nameData = new NameData();
        JSONObject name = member.getJSONObject("name");
        nameData.setFirst(name.getString("first"));
        memberData.setName(nameData);
        memberData.setGender(member.getString("gender"));
        if (!StringUtils.isBlank(member.optString("isSmoker")))
        {
          memberData.setSmokerStatus(member.getString("isSmoker"));
        }
        existingMembers.add(memberData);

        // Build the lookup table of existing members by memberRefName
        existingMemberJSONMap.put(member.getString("memberRefName"), member);
      }
    }

    // 5. look through SG consumer demographics data
    // match SG data to account data by memberExtRefId

    for (MemberData memberData : consumerData.getDemographicsData().getMembers())
    {
      boolean matched = false;

      // If memberData.extRefId is contained in existingMembers
      for (MemberData existingMember : new ArrayList<MemberData>(existingMembers))
      {
        if (StringUtils.equals(memberData.getMemberExtRefId(), existingMember.getMemberExtRefId()))
        {
          // Copy pertinent info from GPA member into the existing Shopping
          // member
          JSONObject existingMemberJSON = existingMemberJSONMap.get(existingMember
            .getMemberRefName());
          JSONObject nameObject = new JSONObject(memberData.getName());
          existingMemberJSON.put("name", nameObject);
          existingMemberJSON.put("isPrimary", memberData.getIsPrimary());
          existingMemberJSON.put("isNew", memberData.isNew());
          existingMemberJSON.put("memberRelationship", memberData.getMemberRelationship());
          existingMemberJSON.put("gender", memberData.getGender());

          if (!StringUtils.isBlank(memberData.getSmokerStatus()))
          {
            existingMemberJSON.put("isSmoker", memberData.getSmokerStatus());
          } else {
            // TODO | default to "No" for now;
            // "No" is hard-coded on the client side too
            existingMemberJSON.put("isSmoker", "No");
          }

          existingMemberJSON.put("birthDate", memberData.getBirthDate());
          existingMemberJSON.put("zipCode", memberData.getZipCode());
          existingMemberJSON.put("workEmail", memberData.getWorkEmail());

          // Add the updated existing member JSON to the final member JSON
          // array
          memberArray.put(existingMemberJSON);

          // Remove the matched existing member
          // because we JUST handled this person, don't
          // need to handle them further down the method
          existingMembers.remove(existingMember);

          matched = true;
        }
      }

      // Else If memberData."demographics" data matches existingMembers
      if (!matched)
      {
        for (MemberData existingMember : new ArrayList<MemberData>(existingMembers))
        {
          if (demographicsMatch(memberData, existingMember))
          {
            // Copy the extRefId from the GPA member into the existing
            // Shopping member
            JSONObject existingMemberJSON = existingMemberJSONMap.get(existingMember
              .getMemberRefName());
            existingMemberJSON.put("memberExtRefId", memberData.getMemberExtRefId());

            // Add the updated existing member JSON to the final member JSON
            // array
            memberArray.put(existingMemberJSON);

            // Remove the matched existing member
            existingMembers.remove(existingMember);

            matched = true;
          }
        }
      }

      // Else Add a new member object
      if (!matched)
      {
        memberData.setMemberRefName(String.valueOf(nextRefName++));

        // TODO | so this is taking a common object and converting it straight to JSON?
        // with smokerStatus and isSmoker, can't do this anymore, at least not
        // until we change the client-side
        //JSONObject memberObject = new JSONObject(memberData);
        JSONObject memberObject = buildEEShoppingMemberFromMemberData(memberData);

        // Build up the default members and member.products selections
        // i.e. All members have all product lines selected
        JSONArray productsArray = new JSONArray();
        for (ProductData productData : productLineData.getProducts())
        {
          JSONObject productObject = new JSONObject();
          productObject.put("productLineCode", productData.getProductLineCode());
          productsArray.put(productObject);
        }
        memberObject.put("products", productsArray);
        memberObject.put("coverageSelected", true);

        memberArray.put(memberObject);
      }
    }

    // Remove any members that had an extRefId, these were removed from GPA
    for (MemberData memberData : new ArrayList<MemberData>(existingMembers))
    {
      if (StringUtils.isNotBlank(memberData.getMemberExtRefId()))
      {
        existingMembers.remove(memberData);
      }
    }

    // Directly add any remaining "existing" members back to the memberArray.
    for (MemberData memberData : existingMembers)
    {
      // Use the memberRefName to get the existing JSON back so we don't have
      // to reconstruct it
      memberArray.put(existingMemberJSONMap.get(memberData.getMemberRefName()));
    }

    accountObject.put("members", memberArray);

    account.updateAccountInfo(getTransactionId(), accountObject);
  }

  // TODO | temp fix until we can refactor this class a bit, and change smoker
  // stuff on the client side
  private JSONObject buildEEShoppingMemberFromMemberData(MemberData memberData) throws JSONException
  {
    JSONObject memberJSON = new JSONObject();

    memberJSON.put("memberExtRefId", memberData.getMemberExtRefId());
    memberJSON.put("memberRefName", memberData.getMemberRefName());

    JSONObject nameObject = new JSONObject(memberData.getName());
    memberJSON.put("name", nameObject);
    memberJSON.put("isPrimary", memberData.getIsPrimary());
    memberJSON.put("isNew", memberData.isNew());
    memberJSON.put("memberRelationship", memberData.getMemberRelationship());
    memberJSON.put("gender", memberData.getGender());

    // TODO : check for smokerStatus == null

    if (!StringUtils.isBlank(memberData.getSmokerStatus()))
    {
      memberJSON.put("isSmoker", memberData.getSmokerStatus());
    } else {
      // TODO | default to "No" for now;
      // "No" is hard-coded on the client side too
      memberJSON.put("isSmoker", "No");
    }

    memberJSON.put("birthDate", memberData.getBirthDate());
    memberJSON.put("zipCode", memberData.getZipCode());
    memberJSON.put("workEmail", memberData.getWorkEmail());

    return memberJSON;
  }

  private boolean demographicsMatch(MemberData m1, MemberData m2)
  {
    boolean equals = true;
    equals &= StringUtils.equals(m1.getName().getFirst(), m2.getName().getFirst());
    if (equals)
    {
      String m1BirthDate = null;
      String m2BirthDate = null;

      Date m1BirthDateParsed = ShoppingDateUtils.stringToDate(m1.getBirthDate());
      if (null != m1BirthDateParsed)
      {
        m1BirthDate = ShoppingDateUtils.dateToString(m1BirthDateParsed);
      }

      Date m2BirthDateParsed = ShoppingDateUtils.stringToDate(m2.getBirthDate());
      if (null != m1BirthDateParsed && null != m2BirthDateParsed)
      {
        m2BirthDate = ShoppingDateUtils.dateToString(m2BirthDateParsed);
      }

      equals &= StringUtils.equals(m1BirthDate, m2BirthDate);
    }
    equals &= StringUtils.equals(m1.getMemberRelationship(), m2.getMemberRelationship());
    equals &= StringUtils.equals(m1.getGender(), m2.getGender());
    equals &= StringUtils.equals(m1.getSmokerStatus(), m2.getSmokerStatus());
    return equals;
  }

  /**
   * @see com.connecture.shopping.process.work.Work#processData(java.lang.Object)
   */
  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    // give back errors
    ConsumerDemographicsData consumerDemographicsData = consumerData.getDemographicsData();

    // add employee plan data
    if (consumerDemographicsData != null)
    {
      // using consumer demographics data, populate JSON
      // employer data
      JSONObject employerJSON = new JSONObject();
      employerJSON.put("name", consumerDemographicsData.getEmployerName());
      employerJSON.put("logoPath", consumerDemographicsData.getEmployerLogoPath());
      jsonObject.put("employer", employerJSON);

      // employee data
      JSONObject employeeJSON = new JSONObject();
      employeeJSON.put("firstName", consumerDemographicsData.getEmployeeData().getFirstName());
      employeeJSON.put("lastName", consumerDemographicsData.getEmployeeData().getLastName());
      employeeJSON.put("email", consumerDemographicsData.getEmployeeData().getEmail());
      jsonObject.put("accountHolder", employeeJSON);

      // relationships
      JSONArray relationshipsArray = new JSONArray();
      for (RelationshipData relationshipData : consumerDemographicsData.getRelationships())
      {
        relationshipsArray.put(new JSONObject(relationshipData));
      }
      jsonObject.put("relationships", relationshipsArray);

      // genders
      JSONArray gendersArray = new JSONArray();
      for (GenderData genderData : consumerDemographicsData.getGenders())
      {
        gendersArray.put(new JSONObject(genderData));
      }
      jsonObject.put("genders", gendersArray);

    }

    return jsonObject;
  }

  @Override
  public ProductPlanData getProductPlanData()
  {
    return productPlanData;
  }

  public void setConsumerDemographicsDataRequest(
    ConsumerDemographicsDataRequest consumerDemographicsDataRequest)
  {
    this.consumerDemographicsDataRequest = consumerDemographicsDataRequest;
  }

  public void setProductDataRequest(ProductDataRequest productPlanDataRequest)
  {
    this.productDataRequest = productPlanDataRequest;
  }

  /**
   * @return the productServerGroup
   */
  public String getProductServerGroup()
  {
    return productServerGroup;
  }

  /**
   * @param productServerGroup the productServerGroup to set
   */
  public void setProductServerGroup(String productServerGroup)
  {
    this.productServerGroup = productServerGroup;
  }
}