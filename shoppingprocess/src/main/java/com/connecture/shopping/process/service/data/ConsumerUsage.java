package com.connecture.shopping.process.service.data;

public class ConsumerUsage
{
  private String xid;
  private Double value;

  public String getXid()
  {
    return xid;
  }

  public void setXid(String xid)
  {
    this.xid = xid;
  }

  public Double getValue()
  {
    return value;
  }

  public void setValue(Double value)
  {
    this.value = value;
  }
}
