package com.connecture.shopping.process.service.data;

public class FilterOptionData
{
  private String optionRefId;
  private String text;
  private String helpText;
  private String helpType;
  private String classOverride;
  private String dataTagValue;  // Not sure what this is for?
  private String dataTagOperator;

  public String getOptionRefId()
  {
    return optionRefId;
  }

  public void setOptionRefId(String optionRefId)
  {
    this.optionRefId = optionRefId;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getHelpText()
  {
    return helpText;
  }

  public void setHelpText(String helpText)
  {
    this.helpText = helpText;
  }

  public String getHelpType()
  {
    return helpType;
  }

  public void setHelpType(String helpType)
  {
    this.helpType = helpType;
  }

  public String getClassOverride()
  {
    return classOverride;
  }

  public void setClassOverride(String classOverride)
  {
    this.classOverride = classOverride;
  }

  public String getDataTagValue()
  {
    return dataTagValue;
  }

  public void setDataTagValue(String dataTagValue)
  {
    this.dataTagValue = dataTagValue;
  }

  public String getDataTagOperator()
  {
    return dataTagOperator;
  }

  public void setDataTagOperator(String dataTagOperator)
  {
    this.dataTagOperator = dataTagOperator;
  }
}
