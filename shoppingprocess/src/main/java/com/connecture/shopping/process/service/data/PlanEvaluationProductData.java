package com.connecture.shopping.process.service.data;

public class PlanEvaluationProductData
{
  private String productId;
  private Long score;
  private Boolean match;

  public String getProductId()
  {
    return productId;
  }

  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  public Long getScore()
  {
    return score;
  }

  public void setScore(Long score)
  {
    this.score = score;
  }

  public Boolean getMatch()
  {
    return match;
  }

  public void setMatch(Boolean match)
  {
    this.match = match;
  }

}
