package com.connecture.shopping.process.common;

public interface Environment
{
  public interface PageComponents
  {
    public static final String PAGE_COMPONENT_KEY = "component.key";
    public static final String PAGE_COMPONENT_LOCALE = "component.locale";
  }
}
