package com.connecture.shopping.process.service.data;

public class RelationshipData
{
  private String type;
  private String display;

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getDisplay()
  {
    return display;
  }

  public void setDisplay(String display)
  {
    this.display = display;
  }
}
