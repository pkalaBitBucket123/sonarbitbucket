package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.individual.QuoteWork;

public abstract class QuoteWorkProvider implements WorkProviderImpl<QuoteWork>
{
  @Override
  public QuoteWork getWork(
    String transactionId,
    String requestKey,
    WorkConstants.Method method,
    JSONObject params,
    JSONObject object) throws JSONException
  {
    QuoteWork work = createWork();
    
    if (WorkConstants.Method.GET.equals(method))
    {
      work.setTransactionId(transactionId);
    }
    else if (WorkConstants.Method.UPDATE.equals(method))
    {
      JSONObject updateObject = object.getJSONObject("quote");
      work.setValue(updateObject);
      work.setTransactionId(transactionId);
    }

    return work;
  }
}