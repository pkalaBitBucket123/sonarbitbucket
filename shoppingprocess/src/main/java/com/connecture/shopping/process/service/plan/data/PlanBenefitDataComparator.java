/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

import java.util.Comparator;

/**
 * 
 */
public class PlanBenefitDataComparator implements Comparator<PlanBenefitData>
{
  /**
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(PlanBenefitData planBenefitData1, PlanBenefitData planBenefitData2)
  {
    return Integer.valueOf(planBenefitData1.getSortOrder()).compareTo(planBenefitData2.getSortOrder());
    //return Integer.compare(planBenefitData1.getSortOrder(), planBenefitData2.getSortOrder());
  }
}
