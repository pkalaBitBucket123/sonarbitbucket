package com.connecture.shopping.process.common.account;

import org.json.JSONObject;

/**
 * 
 * TODO: Might be able to combine some of these methods
 * 
 * @author Shopping Development
 *
 */
public interface Account
{
  public JSONObject getAccount(String transactionId);

  public void updateAccountInfo(String transactionId, JSONObject account);
  
  public void updateAccountTransactionId(String oldTransactionId, String newTransactionId);
  
  public JSONObject getOrCreateAccount(String transactionId);
  
  public void clearAccount();
}
