package com.connecture.shopping.process.service.data;

public class ShoppingDomainModel
{
  private Request request = new Request();

  public Request getRequest()
  {
    return request;
  }

  public void setRequest(Request request)
  {
    this.request = request;
  }
}
