package com.connecture.shopping.process.work.individual;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.CollectionUtils;

import com.connecture.model.data.rules.aptc.APTCDomain;
import com.connecture.model.data.rules.aptc.APTCDomainPlan;
import com.connecture.shopping.process.camel.request.RatingProductDataRequest;
import com.connecture.shopping.process.camel.request.StateKeyForCountyAndZipCodeDataRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.service.data.PlanAPTCData;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.PlanRiderData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.data.ProductLineData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.service.data.aptc.converter.APTCDomainPlanConverterUtils;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.service.data.converter.RatingPlanDataConverter;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.APTCRulesEngineBean;
import com.connecture.shopping.process.work.core.ProductLinesWork;
import com.connecture.shopping.process.work.core.ProductPlanWork;

public class RatingPlanWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(RatingPlanWork.class);

  // APTC crap
  private APTCRulesEngineBean aptcRulesEngineBean;
  private StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest;

  // Start Required Inputs
  private String transactionId;
  private RatingProductDataRequest ratingProductDataRequest;
  // End Required Inputs

  // Start Outputs
  private ProductPlanData productPlanData;
  private ProductLineData productLineData;

  // End Outputs

  private static final String RATING_TOBACCO_YES = "Y";
  private static final String RATING_TOBACCO_NO = "N";

  private class RatingMemberModel
  {
    JSONArray membersJSON;
    Boolean isNewBusiness = Boolean.TRUE;
    String zipCode;
    String countyId;
    long effectiveMs;
    boolean isOnExchangePlans;
    String csrCode;
  }

  @Override
  public void get() throws Exception
  {
    RatingMemberModel ratingMemberModel = getRatingMemberModel();

    productPlanData = getProductPlanData(ratingMemberModel);

    productLineData = ProductLinesWork.getProductLineData(productPlanData);
  }

  public ProductPlanData getProductPlanData(RatingMemberModel ratingMemberModel)
    throws JSONException, Exception
  {
    ProductPlanData productPlanData = null;

    JSONArray membersJSON = ratingMemberModel.membersJSON;
    String coverageType = "IFP";
    if(getAccount() != null && getAccount().getAccount(transactionId) != null 
    		&& getAccount().getAccount(transactionId).has("coverageType")){
    	coverageType = getAccount().getAccount(transactionId).getString("coverageType");
    }
    ratingProductDataRequest.setCovergaeType(coverageType);
    if (validDemographicsForProcessing(ratingMemberModel.zipCode, membersJSON))
    {
      // If AccountInfo exists and AccountInfo.value has member demographics
      // data, then populate the MemberData of the request using AccountInfo
      // member demographics data
      if (LOG.isDebugEnabled())
      {
        LOG.debug("product plan members: " + membersJSON.toString(4));
      }

      ratingProductDataRequest.setZipCode(ratingMemberModel.zipCode);

      if (StringUtils.isNotBlank(ratingMemberModel.countyId))
      {
        ratingProductDataRequest.setCounty(ratingMemberModel.countyId);
      }

      for (int i = 0; i < membersJSON.length(); i++)
      {
        JSONObject memberJSON = membersJSON.getJSONObject(i);

        String memberId = memberJSON.getString("memberRefName");

        String birthDate = memberJSON.getString("birthDate");

        String gender = memberJSON.getString("gender");

        // need capital letters for rating to run properly.
        // tobacco may not be set on member, if getting data from FFM.
        String tobacco = memberJSON.optString("isSmoker");
        if ("y".equalsIgnoreCase(tobacco))
        {
          tobacco = RATING_TOBACCO_YES;
        }
        else
        {
          tobacco = RATING_TOBACCO_NO;
        }

        String relationshipKey = memberJSON.optString("relationshipKey");
        if ("PRIMARY".equals(relationshipKey))
        {
          ratingProductDataRequest.setPrimary(memberId, birthDate, gender, tobacco);
        }
        else if ("SPOUSE".equals(relationshipKey))
        {
          ratingProductDataRequest.setSpouse(memberId, birthDate, gender, tobacco);
        }
        else if ("CHILD".equals(relationshipKey))
        {
          ratingProductDataRequest.addChild(memberId, birthDate, gender, tobacco);
        }
        else if("WARD".equals(relationshipKey)){
        	ratingProductDataRequest.setWard(memberId, birthDate, gender, tobacco);
        }
      }

      Date effectiveDate = new Date(ratingMemberModel.effectiveMs);
      ratingProductDataRequest.setEffectiveDate(effectiveDate);

      ratingProductDataRequest.setISNewBusiness(ratingMemberModel.isNewBusiness);

      ratingProductDataRequest.setIsOnExchangePlans(ratingMemberModel.isOnExchangePlans);

      ratingProductDataRequest.setCsrCode(ratingMemberModel.csrCode);

      DataProvider<ProductPlanData> dataProvider = new CamelRequestingDataProvider<ProductPlanData>(
        ratingProductDataRequest);
      productPlanData = getCachedData(ShoppingDataProviderKey.RATING_PRODUCTS, dataProvider);

      // APTC crap
      if (ratingMemberModel.isOnExchangePlans)
      {
        // don't do anything if there isn't any subsidy to begin with
        if (SubsidyUsageWork.getHouseholdSubsidyAmount(membersJSON) > 0)
        {
          APTCDomain aptcDomain = getAPTCDomainData(productPlanData.getPlans(), membersJSON);
          // build a map of APTC plan internalCodes and properties
          Map<String, APTCDomainPlan> aptcPlanInternalCodeMapping = new HashMap<String, APTCDomainPlan>();
          for (APTCDomainPlan aptcPlan : aptcDomain.getSelectedPlans())
          {
            aptcPlanInternalCodeMapping.put(aptcPlan.getPlanId(), aptcPlan);
          }
          for (APTCDomainPlan aptcPlan : aptcDomain.getAvailablePlans())
          {
            aptcPlanInternalCodeMapping.put(aptcPlan.getPlanId(), aptcPlan);
          }

          for (PlanData planData : productPlanData.getPlans())
          {
            if (aptcPlanInternalCodeMapping.containsKey(planData.getInternalCode()))
            {
              APTCDomainPlan planAptcData = aptcPlanInternalCodeMapping.get(planData
                .getInternalCode());
              PlanAPTCData aptcData = new PlanAPTCData();
              aptcData.setAppliedAPTC(planAptcData.getAppliedAPTC());
              aptcData.setBasePremium(planAptcData.getBasePremium());
              aptcData.setNetPremium(planAptcData.getNetPremium());
              planData.setAptcData(aptcData);
            }
          }
        }
      }
    }

    return productPlanData;
  }

  private boolean validDemographicsForProcessing(String zipCode, JSONArray membersJSON)
    throws JSONException
  {
    boolean isValidDemographicsForProcessing = null != membersJSON;

    isValidDemographicsForProcessing &= StringUtils.isNotBlank(zipCode);

    for (int i = 0; isValidDemographicsForProcessing && i < membersJSON.length(); i++)
    {
      JSONObject memberJSON = membersJSON.getJSONObject(i);

      isValidDemographicsForProcessing &= null != memberJSON.optString("birthDate", null);

      isValidDemographicsForProcessing &= null != memberJSON.optString("gender", null);

      // tobacco may not be set on member, if getting data from FFM.
      // isValidDemographicsForProcessing &= null !=
      // memberJSON.optString("isSmoker", null);
    }

    return isValidDemographicsForProcessing;
  }

  private APTCDomain getAPTCDomainData(List<PlanData> plans, JSONArray membersJSON)
    throws Exception
  {
    // get the account. we need it for all sorts of things.
    JSONObject accountJSON = this.getAccount().getAccount(transactionId);

    Double householdSubsidyAmount = SubsidyUsageWork.getHouseholdSubsidyAmount(membersJSON);

    stateKeyForCountyAndZipCodeDataRequest.setZipCode(accountJSON
      .getString(SubsidyUsageWork.ZIP_CODE));
    if (accountJSON.has(SubsidyUsageWork.COUNTY_ID))
    {
      stateKeyForCountyAndZipCodeDataRequest.setCountyId(accountJSON
        .getInt(SubsidyUsageWork.COUNTY_ID));
    }

    String stateCode = stateKeyForCountyAndZipCodeDataRequest.submitRequest();

    // build plan data from input json. this should be a collection
    // containing ALL of the available plans.
    List<APTCDomainPlan> aptcPlans = APTCDomainPlanConverterUtils.fromPlanData(plans);

    // build the subsidy domain object
    APTCDomain aptcDomain = SubsidyUsageWork.buildAPTCDomain(stateCode, householdSubsidyAmount,
      aptcPlans, SubsidyUsageWork.getSelectedProductInternalCodes(accountJSON
        .optJSONObject(SubsidyUsageWork.CART)));

    // call rules engine
    aptcRulesEngineBean.setDomainModel(aptcDomain);
    aptcRulesEngineBean.setDataCache(getDataCache());
    return aptcRulesEngineBean.executeRule();
  }

  private RatingMemberModel getRatingMemberModel()
  {
    RatingMemberModel ratingMemberModel = new RatingMemberModel();

    if (getAccount() != null)
    {
      JSONObject accountValueJSON = getAccount().getAccount(transactionId);

      if (accountValueJSON != null)
      {
        ratingMemberModel.membersJSON = accountValueJSON.optJSONArray("members");
        ratingMemberModel.zipCode = accountValueJSON.optString("zipCode");
        ratingMemberModel.countyId = accountValueJSON.optString("countyId");
        Date effectiveDate = ShoppingDateUtils.stringToDate(accountValueJSON.optString("effectiveDate", null));
        ratingMemberModel.effectiveMs = effectiveDate.getTime();
        ratingMemberModel.isNewBusiness = Boolean.valueOf(!accountValueJSON
          .has("renewingDefaultPlanId"));
        ratingMemberModel.isOnExchangePlans = accountValueJSON.optBoolean("isOnExchangePlans",
          false);
        ratingMemberModel.csrCode = accountValueJSON.optString("csrCode", "NA");
      }
    }
    return ratingMemberModel;
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    if (null != productPlanData && null != productPlanData.getPlans())
    {
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Got some (" + productPlanData.getPlans().size() + ") data: " + productPlanData);
      }

      ProductPlanWork planWork = new ProductPlanWork();

      JSONArray planArray = new JSONArray();

      // This may come out null (valid), depending on how domain data was set up
      Integer paychecksPerYear = productPlanData.getPaychecksPerYear();

      JSONObject planConfig = new JSONObject();
      planConfig.put("hasPaycheckCost", paychecksPerYear != null);

      for (PlanData planData : productPlanData.getPlans())
      {
        // plan (empty)
        JSONObject planDataJSON = new JSONObject();

        // use internal code as ID, as this is how data will be configured
        // in other systems (plan advisor)
        planDataJSON.put("id", planData.getInternalCode());
        
        planDataJSON.put("sortOrder", planData.getSortOrder());

        planDataJSON.put("planId", planData.getIdentifier()); // needed for lead
                                                              // currently

        planDataJSON.put("name", planData.getPlanName());

        planDataJSON.put("tier", planData.getTierCode());

        planDataJSON.put("planMetalTier", planData.getMetalTier());

        planDataJSON.put("planType", planData.getPlanType());
        
        planDataJSON.put("benefitSummaryUrl", planData.getSbcUrl());
        
        // start change(s) by rsingh for user story 229  
        planDataJSON.put("description", planData.getDescription());
        // end change(s) by rsingh for user story 229  

      //start change(s) by rsingh for user story 20,21,92,258,260,229  
        planDataJSON.put("sbcUrl", planData.getSbcUrl());
        planDataJSON.put("brochureUrl", planData.getBrochureUrl());
      //end change(s) by rsingh for user story  20,21,92,258,260,229   
        // create COST JSON

        planDataJSON.put("cost", buildCostJSON(planData));
      //<NEBRASKA RIDER SUPPORT MI>
        setRiders(planDataJSON, planData);
      //</NEBRASKA RIDER SUPPORT MI>
        // This is a temporary fix for first release
        // we need the premium for the sorting, we are going
        // to use the monthly rate because that will always be there or
        // should be there anyway :)
        if (planData.getRate() != null)
        {
          planDataJSON.put("premium", planData.getPlanCost());
        }

        // get networks as array
        JSONArray networksJSON = new JSONArray();
        for (String networkId : planData.getNetworks())
        {
          networksJSON.put(networkId);
        }
        planDataJSON.put("networks", networksJSON);

        // product line
        // TODO | change "productLine" to "productLineName" here and on UI
        planDataJSON.put("productLine", planData.getProductData().getProductLineName());
        planDataJSON.put("productLineCode", planData.getProductData().getProductLineCode());

        planDataJSON.put("benefitCategories", planWork.getPlanBenefitCategoryJSONArray(planData));

        // filterValues
        // construct filter values (flattened values), allowing us to filter
        // on anything regardless of JSON object structure

        Map<String, String> filterValuesMap = new HashMap<String, String>();
        filterValuesMap.putAll(planWork.getFilterValueMap(planData.getProductData()
          .getProductLineCode(), planData.getBenefitCategories()));

        // Now put each one of these directly on the plan. This will
        // allow us to easily access both benefit values and any other
        // property on the product automatically, without having hardcoded
        // properties
        for (Entry<String, String> entry : filterValuesMap.entrySet())
        {
          planDataJSON.put(entry.getKey(), entry.getValue());
        }

        planArray.put(planDataJSON);
      }

      jsonObject.put("plans", planArray);
      jsonObject.put("planConfig", planConfig);

      // add product lines data
      if (productLineData != null)
      {
        JSONArray productArray = new JSONArray();
        for (ProductData productData : productLineData.getProducts())
        {
          productArray.put(new JSONObject(productData));
        }

        jsonObject.put("productLines", productArray);
      }

      return jsonObject;
    }
    else
    {
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Got no data :-(");
      }

      // basic interface for this work, I think we need
      // these for now, even if nothing exists.
      jsonObject.put("plans", new JSONArray());
      jsonObject.put("planConfig", new JSONObject());
      jsonObject.put("productLines", new JSONArray());
    }

    return jsonObject;
  }

  JSONObject buildCostJSON(PlanData planData) throws Exception
  {
    JSONObject costJSON = new JSONObject();

    // our goal here is to provide "monthly" rates
    JSONObject monthlyCost = new JSONObject();
    monthlyCost.put("rate", ProductPlanWork.buildMonthlyRateJSON(planData));

    // get tobacco surcharge amount for the household
    monthlyCost.put("tobaccoSurcharge", planData.getTobaccoSurcharge());

    // by definition, all plans in RatingPlanWork are memberLevelRated
    // However, pre-HCR rates are not member-level rated
    monthlyCost.put("memberLevelRated", null != planData.getMemberLevelRates());

    JSONObject memberLevelData = new JSONObject();

    // add perMember Rates
    memberLevelData.put("memberRates", RatingPlanDataConverter.buildMemberLevelRateJSON(planData));

    monthlyCost.put("memberLevelRating", memberLevelData);

    // TODO | add APTC
    if (planData.getAptcData() != null)
    {
      JSONObject subsidyDataJSON = new JSONObject();
      subsidyDataJSON.put("appliedSubsidy", planData.getAptcData().getAppliedAPTC());
      subsidyDataJSON.put("netRate", planData.getAptcData().getNetPremium());
      subsidyDataJSON.put("baseRate", planData.getAptcData().getBasePremium());
      monthlyCost.put("subsidyData", subsidyDataJSON);
    }

    costJSON.put("monthly", monthlyCost);

    return costJSON;
  }
//<NEBRASKA RIDER SUPPORT MI>
  private void setRiders(JSONObject planObject,PlanData planData) throws JSONException {
		if(!CollectionUtils.isEmpty(planData.getRiders() )){
			JSONArray  array = new JSONArray();
			for(PlanRiderData info :  planData.getRiders())
				array.put(toRiderJson(info));
			planObject.put("riders", array);
		}
		
	  }
	  // Setters for Struts

	  private JSONObject toRiderJson(PlanRiderData info) throws JSONException {
		JSONObject object = new JSONObject();
		object.put("riderId", info.getRiderId());
		object.put("name", info.getDisplayName());
		object.put("description", info.getDescription());
		object.put("xref",info.getXref());
		object.put("benefitSummary", info.getBenefitSummary());
		object.put("monthlyRate", info.getMonthlyRate());
		object.put("alternateName", info.getAlternateName());
		
		return object;
	  }
	//</NEBRASKA RIDER SUPPORT MI>
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setRatingProductDataRequest(RatingProductDataRequest ratingProductDataRequest)
  {
    this.ratingProductDataRequest = ratingProductDataRequest;
  }

  public ProductPlanData getProductPlanData()
  {
    return productPlanData;
  }

  // APTC crap START
  public void setAptcRulesEngineBean(APTCRulesEngineBean aptcRulesEngineBean)
  {
    this.aptcRulesEngineBean = aptcRulesEngineBean;
  }

  public void setStateKeyForCountyAndZipCodeDataRequest(
    StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest)
  {
    this.stateKeyForCountyAndZipCodeDataRequest = stateKeyForCountyAndZipCodeDataRequest;
  }
  // APTC crap END
}
