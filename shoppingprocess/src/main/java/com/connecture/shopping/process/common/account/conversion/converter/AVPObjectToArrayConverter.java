package com.connecture.shopping.process.common.account.conversion.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPObjectToArrayConverter extends AVPConverterImpl
{
  private String path;
  private String reason;
  private String rename;
  
  public AVPObjectToArrayConverter() {
    super(AVPConverterType.OBJECT_TO_ARRAY);
  }
  
  @Override
  public void apply(JSONObject jsonObject) throws JSONException
  {
    JSONPathUtil.objectToArray(jsonObject, path, rename);
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }

  public String getReason()
  {
    return reason;
  }

  public void setReason(String reason)
  {
    this.reason = reason;
  }

  public String getRename()
  {
    return rename;
  }

  public void setRename(String rename)
  {
    this.rename = rename;
  }
}