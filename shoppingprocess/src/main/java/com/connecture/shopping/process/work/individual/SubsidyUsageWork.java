package com.connecture.shopping.process.work.individual;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.rules.aptc.APTCDomain;
import com.connecture.model.data.rules.aptc.APTCDomainPlan;
import com.connecture.shopping.process.camel.request.StateKeyForCountyAndZipCodeDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.aptc.converter.APTCDomainPlanConverterUtils;
import com.connecture.shopping.process.service.data.converter.ProductDataConverterUtil;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.APTCRulesEngineBean;

public class SubsidyUsageWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(SubsidyUsageWork.class);

  private APTCRulesEngineBean aptcRulesEngineBean;
  private StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest;

  // INPUT
  private JSONObject inputJSON;
  private String transactionId;

  // OUTPUT
  private APTCDomain aptcDomain;

  // TODO | get some kind of class for account values; get rid of this
  public static final String PLANS = "plans";
  public static final String MEMBERS = "members";
  public static final String CART = "cart";
  public static final String SUBSIDY_USAGE = "subsidyUsage";
  public static final String ZIP_CODE = "zipCode";
  public static final String COUNTY_ID = "countyId";

  // member ffm data
  public static final String PREMIUM_TAX_CREDIT = "premiumTaxCredit";
  public static final String FFM_DATA = "ffmData";

  @Override
  public void update() throws Exception
  {
    // TODO | so this is how we handle it elsewhere
    // (DemographicsValidationWork)...
    // running a save on the client-side to POST because we are sending in a TON
    // of data.
    // not a fan, need to figure this out
    // el o el
    get();
  }

  @Override
  public void get() throws Exception
  {
    JSONObject subsidyInputJSON = inputJSON.getJSONObject(SUBSIDY_USAGE);

    // get the account. we need it for all sorts of things.
    JSONObject accountJSON = this.getAccountJSON();

    // pull member data from the account. this will give us
    // subsidy amounts, ages, tobacco usage.
    JSONArray membersJSON = accountJSON.getJSONArray(MEMBERS);

    Double householdSubsidyAmount = getHouseholdSubsidyAmount(membersJSON);

    stateKeyForCountyAndZipCodeDataRequest.setZipCode(accountJSON.getString(ZIP_CODE));
    if (accountJSON.has(COUNTY_ID))
    {
      stateKeyForCountyAndZipCodeDataRequest.setCountyId(accountJSON.getInt(COUNTY_ID));
    }
    
    String stateCode = stateKeyForCountyAndZipCodeDataRequest.submitRequest();
      
    // build plan data from input json. this should be a collection
    // containing ALL of the available plans.
    List<APTCDomainPlan> aptcPlans = APTCDomainPlanConverterUtils
      .fromPlanData(ProductDataConverterUtil.convertFromShoppingPlansJSON(subsidyInputJSON
        .getJSONArray(PLANS)));

    // build the subsidy domain object
    aptcDomain = buildAPTCDomain(stateCode, householdSubsidyAmount,
      aptcPlans, SubsidyUsageWork.getSelectedProductInternalCodes(accountJSON.optJSONObject(CART)));

    // TODO | call rules engine... 
    aptcRulesEngineBean.setDomainModel(aptcDomain);
    aptcRulesEngineBean.setDataCache(getDataCache());
    aptcDomain = aptcRulesEngineBean.executeRule();
  }

  @Override
  public JSONObject processData() throws Exception
  {
    // return object
    JSONObject jsonObject = new JSONObject();
    JSONObject subsidyUsageJSON = new JSONObject();
    
    // pull data from the subsidy usage domain
    // put the full list back together and process into JSON
    List<APTCDomainPlan> aptcPlans = new ArrayList<APTCDomainPlan>();
    aptcPlans.addAll(aptcDomain.getSelectedPlans());
    aptcPlans.addAll(aptcDomain.getAvailablePlans());

    // create a Map of product internal code, rates
    subsidyUsageJSON.put("plans", createPlanPropertyMappingJSON(aptcPlans));

    jsonObject.put("subsidyUsage", subsidyUsageJSON);
    return jsonObject;
  }

  private JSONObject createPlanPropertyMappingJSON(List<APTCDomainPlan> aptcPlans)
    throws JSONException
  {
    JSONObject planPropertyMapping = new JSONObject();

    for (APTCDomainPlan aptcPlan : aptcPlans)
    {
      String planKey = aptcPlan.getPlanId();
      planPropertyMapping.put(planKey, createPlanPropertiesJSON(aptcPlan));
    }

    return planPropertyMapping;
  }

  private JSONObject createPlanPropertiesJSON(APTCDomainPlan aptcPlan) throws JSONException
  {
    JSONObject planProperties = new JSONObject();
    planProperties.put("netRate", aptcPlan.getNetPremium());
    planProperties.put("baseRate", aptcPlan.getBasePremium());
    planProperties.put("appliedSubsidy", aptcPlan.getAppliedAPTC());
    return planProperties;
  }

  protected JSONObject getAccountJSON()
  {
    Account account = getAccount();
    JSONObject accountJSON = account.getAccount(transactionId);
    return accountJSON;
  }

  public static Double getHouseholdSubsidyAmount(JSONArray membersJSON) throws JSONException
  {
    Double householdSubsidy = 0.0;
    for (int i = 0; i < membersJSON.length(); i++)
    {
      JSONObject memberJSON = membersJSON.getJSONObject(i);
      JSONObject memberFFMjson = memberJSON.optJSONObject(FFM_DATA);
      if (memberFFMjson != null)
      {
        Double premiumTaxCredit = memberFFMjson.optDouble(PREMIUM_TAX_CREDIT, 0);
        if (premiumTaxCredit == null || premiumTaxCredit == Double.NaN)
        {
          // would be weird, so log it
          LOG.error("FFM Data was found for a member, but [" + PREMIUM_TAX_CREDIT
            + "] was not included.");
        }
        else
        {
          householdSubsidy += premiumTaxCredit;
        }
      }
    }
    return householdSubsidy;
  }

  public static Set<String> getSelectedProductInternalCodes(JSONObject cartJSON) throws JSONException
  {  
    Set<String> selectedProductIds = new HashSet<String>();
    
    if (cartJSON != null)
    {
      JSONObject productLinesJSON = cartJSON.optJSONObject("productLines");
      if (productLinesJSON != null && JSONObject.getNames(productLinesJSON) != null)
      {
        // an object for each product line
        for (String productLineCode : JSONObject.getNames(productLinesJSON))
        {
          JSONObject productLineJSON = productLinesJSON.getJSONObject(productLineCode);
          JSONArray productsJSON = productLineJSON.optJSONArray("products");
          if (productsJSON != null)
          {
            for (int i = 0; i < productsJSON.length(); i++)
            {
              JSONObject productJSON = productsJSON.getJSONObject(i);
              String productInternalCode = productJSON.getString("id");
              selectedProductIds.add(productInternalCode);
            }
          }
        }
      }
    }
    
    return selectedProductIds;   
  }
  
  public static APTCDomain buildAPTCDomain(
    String stateCode,
    Double householdSubsidy,
    List<APTCDomainPlan> aptcDomainPlans,
    Set<String> cartProductKeys)
  {
    List<APTCDomainPlan> selectedPlans = new ArrayList<APTCDomainPlan>();
    List<APTCDomainPlan> availablePlans = new ArrayList<APTCDomainPlan>();

    // separate into plans IN-CART, plans OUT-OF-CART
    for (APTCDomainPlan aptcDomainPlan : aptcDomainPlans)
    {
      if (cartProductKeys.contains(aptcDomainPlan.getPlanId()))
      {
        selectedPlans.add(aptcDomainPlan);
      }
      else
      {
        availablePlans.add(aptcDomainPlan);
      }
    }

    APTCDomain aptcDomain = new APTCDomain();
    // always use 100% of household subsidy
    aptcDomain.setStateCode(stateCode);
    aptcDomain.setElectedAmount(householdSubsidy);
    aptcDomain.setSelectedPlans(selectedPlans);
    aptcDomain.setAvailablePlans(availablePlans);
    return aptcDomain;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setInputJSON(JSONObject inputJSON)
  {
    this.inputJSON = inputJSON;
  }

  public void setAptcRulesEngineBean(APTCRulesEngineBean aptcRulesEngineBean)
  {
    this.aptcRulesEngineBean = aptcRulesEngineBean;
  }

  public void setStateKeyForCountyAndZipCodeDataRequest(
    StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest)
  {
    this.stateKeyForCountyAndZipCodeDataRequest = stateKeyForCountyAndZipCodeDataRequest;
  }
}