package com.connecture.shopping.process.work;

public enum RequestType
{
  ACCOUNT("account"),
  ACTOR("actor"),
  AVAILABILITY("availability"),
  APPLICATION_CONFIG("applicationConfig"),
  COST_ESTIMATOR_CONFIG("costEstimatorConfig"),
  COST_ESTIMATOR_COSTS("costEstimatorCosts"),
  DEMOGRAPHICS_CONFIG("demographicsConfig"),
  DEMOGRAPHICS_VALIDATION("demographicsValidation"),
  MEMBER_ELIGIBILITY("eligibility"),
  CONSUMER_DEMOGRAPHICS("consumerDemographics"),
  PLAN_ADVISOR("planAdvisor"),
  PRODUCTS("products"),
  PRODUCT_LINES("productLines"),
  PROVIDER_SEARCH("providerSearch"),
  POVERTY_INCOME_LEVEL("povertyIncomeLevel"),
  RULES_ENGINE("rulesEngine"),
  SPECIAL_ENROLLMENT_EFFECTIVE_DATE("enrollmentEffectiveDate"),
  SPECIAL_ENROLLMENT_EVENT_DATE_VALIDATION("specialEnrollmentEventDateValidation"),
  SUBSIDY_USAGE("subsidyUsage"),
  VALID_ZIP_CODE_COUNTY_DATA("validZipCodeCountyData"), 
  QUOTE("quote");
  
  private String value;
  
  private RequestType(String value)
  {
    this.value = value;
  }

  public String getValue()
  {
    return value;
  }
  
  public static RequestType ignoreCaseValueOf(String requestTypeKey)
  {
    RequestType matchedType = null;
    for (RequestType type : RequestType.values())
    {
      if (type.value.equalsIgnoreCase(requestTypeKey))
      {
        matchedType = type;
      }
    }
    return matchedType;
  }

  @Override
  public String toString()
  {
    return value;
  }
}