package com.connecture.shopping.process.service.stub;

import com.connecture.shopping.process.common.JSONFileUtils;

public class ProductServiceStub
{
  public String getPlanData(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "productServerData.json");
  }
}
