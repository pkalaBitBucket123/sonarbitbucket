package com.connecture.shopping.process.service.stub;

import com.connecture.model.data.Actor;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

public class ActorServiceStub
{
  public Actor getActorForContext(ShoppingContext context)
  {
    Actor actor = new Actor();
    actor.setInternalUser(false);
    return actor;
  }
}
