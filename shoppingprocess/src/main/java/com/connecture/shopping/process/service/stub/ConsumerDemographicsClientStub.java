package com.connecture.shopping.process.service.stub;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.connecture.model.api.ShoppingInitializationProvider;
import com.connecture.model.data.ShoppingDependent;
import com.connecture.model.data.ShoppingEmployee;
import com.connecture.model.data.ShoppingEmployer;
import com.connecture.model.data.ShoppingEvent;
import com.connecture.model.data.ShoppingGender;
import com.connecture.model.data.ShoppingInitializationData;
import com.connecture.model.data.ShoppingRelationship;

public class ConsumerDemographicsClientStub implements ShoppingInitializationProvider
{
  public ShoppingInitializationData getInitialMemberData(String eventId, Long groupId)
  {
    // return the object we want back from IA
    ShoppingInitializationData resultObj = new ShoppingInitializationData();
    
    // set dependents
    resultObj.setDependents(createDependentData());

    // set employee data .. much like dependent data but with a little more
    resultObj.setEmployee(createEmployeeData());

    // ..and the big one
    resultObj.setShoppingData(ConsumerProductsClientStub.createConsumerShoppingData());
    
    // employerSetup.company - name | logoPath
    resultObj.setEmployer(createEmployerData());

    // employerSetup.effectiveDate?
    // employerSetup.enrollmentPeriod.enrollmentPeriodEndDateId
    resultObj.setEvent(createEventData());

    resultObj.setRelationshipRefData(createRelationshipRefData());
    
    resultObj.setGenderRefData(createGenderRefData());

    return resultObj;
  }

  private static List<ShoppingDependent> createDependentData()
  {
    List<ShoppingDependent> currentDependents = new ArrayList<ShoppingDependent>();

    try
    {
      ShoppingDependent dependent1 = new ShoppingDependent();
      Date birthDate1 = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse("10/04/1970");
      dependent1.setDateOfBirth(birthDate1);
      dependent1.setExtRefId("DEP_EXT_1");
      dependent1.setFirstName("Mary");
      dependent1.setGenderTypeKey("Female");
      dependent1.setRelationshipTypeKey("Spouse (opposite sex)");
      dependent1.setSmoker("No");

      currentDependents.add(dependent1);

      ShoppingDependent dependent2 = new ShoppingDependent();
      Date birthDate2 = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse("04/15/2007");
      dependent2.setDateOfBirth(birthDate2);
      dependent2.setExtRefId("DEP_EXT_2");
      dependent2.setFirstName("Frank");
      dependent2.setGenderTypeKey("Male");
      dependent2.setRelationshipTypeKey("Child - dependent on subscriber for support and is legal guardian");
      dependent2.setSmoker("No");

      currentDependents.add(dependent2);
      
//       This is for testing GPA/Shopping Member Data Correlation
//      ShoppingDependent dependent3 = new ShoppingDependent();
//      Date birthDate3 = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse("01/01/2003");
//      dependent3.setDateOfBirth(birthDate3);
//      dependent3.setExtRefId("DEP_EXT_3");
//      dependent3.setFirstName("Jim");
//      dependent3.setGenderTypeKey("MALE");
//      dependent3.setRelationshipTypeKey("Child - dependent on subscriber for support and is legal guardian");
//      dependent3.setSmoker("No");
//
//      currentDependents.add(dependent3);
      
//    This is for testing GPA/Shopping Member Data Correlation
//    ShoppingDependent dependent4 = new ShoppingDependent();
//    Date birthDate4 = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse("01/06/2003");
//    dependent4.setDateOfBirth(birthDate4);
//    dependent4.setExtRefId("DEP_EXT_4");
//    dependent4.setFirstName("Bob");
//    dependent4.setGenderTypeKey("MALE");
//    dependent4.setRelationshipTypeKey("Child - dependent on subscriber for support and is legal guardian");
//    dependent4.setSmoker("No");
//
//    currentDependents.add(dependent4);
    }
    catch (ParseException e)
    {
      // yummmm
    }

    return currentDependents;
  }

  private static ShoppingEmployee createEmployeeData()
  {
    ShoppingEmployee employee = null;
    try
    {
      /*
      Date birthDate = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse("11/05/1965");
      employee.setDateOfBirth(birthDate);
      employee.setExtRefId("EMP_EXT_1");
      employee.setFirstName("Bruce");
      employee.setLastName("Campbell");
      employee.setGenderTypeKey("MALE");
      employee.setRelationshipTypeKey("Primary");
      employee.setEmail("ckroll@connecture.com");
      employee.setZipCode("53186");
      employee.setSmoker("No");
      */
      
      // do it Scott's way then
      employee = new ShoppingEmployee.Builder("EMP_EXT_1",
        "Male",
        "Primary",   
        new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse("11/05/1965"),
        "53186",
        "ckroll@connecture.com",
        "No",
        "Bruce",
        "Campbell").build();
      
    }
    catch (ParseException e)
    {
      // yummmm
    }

    return employee;
  }
  
  private static ShoppingEmployer createEmployerData()
  {
    ShoppingEmployer employer = new ShoppingEmployer();
    employer.setName("Conjecture");
    employer.setLogoURL("http://localhost:8080/shopping/images/carrierlogo.png");
    return employer;
  }

  private static ShoppingEvent createEventData()
  {
    ShoppingEvent event = new ShoppingEvent();
    try
    {
      Date coverageEffectiveDate = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)
        .parse("02/20/2013");
      event.setCoverageEffectiveDate(coverageEffectiveDate);
      Date enrollmentPeriodEndDate = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)
        .parse("02/02/2015");
      event.setEnrollmentPeriodEndDate(enrollmentPeriodEndDate);
    }
    catch (ParseException e)
    {
      // yummmm
    }
    return event;
  }

  private static List<ShoppingGender> createGenderRefData()
  {
    List<ShoppingGender> genderRefList = new ArrayList<ShoppingGender>();

    ShoppingGender genderMale = new ShoppingGender();
    genderMale.setDisplayValue("M");
    genderMale.setKey("Male");
    genderRefList.add(genderMale);

    ShoppingGender genderFemale = new ShoppingGender();
    genderFemale.setDisplayValue("F");
    genderFemale.setKey("Female");
    genderRefList.add(genderFemale);

    return genderRefList;
  }
  
  private static List<ShoppingRelationship> createRelationshipRefData()
  {
    List<ShoppingRelationship> relationshipRefList = new ArrayList<ShoppingRelationship>();

    ShoppingRelationship relationshipPrimary = new ShoppingRelationship();
    relationshipPrimary.setDisplayValue("Primary");
    relationshipPrimary.setKey("Primary");
    relationshipRefList.add(relationshipPrimary);

    ShoppingRelationship relationshipSpouse = new ShoppingRelationship();
    relationshipSpouse.setDisplayValue("Spouse (opposite sex)");
    relationshipSpouse.setKey("Spouse (opposite sex)");
    relationshipRefList.add(relationshipSpouse);

    ShoppingRelationship relationshipChild = new ShoppingRelationship();
    relationshipChild.setDisplayValue("Child - dependent on subscriber for support and is legal guardian");
    relationshipChild.setKey("Child - dependent on subscriber for support and is legal guardian");
    relationshipRefList.add(relationshipChild);

    return relationshipRefList;
  }
}
