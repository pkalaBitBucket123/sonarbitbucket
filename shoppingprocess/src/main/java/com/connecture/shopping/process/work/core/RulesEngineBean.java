package com.connecture.shopping.process.work.core;

import java.util.Date;
import java.util.List;

import com.connecture.shopping.process.camel.request.RulesEngineDataRequest;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.DataProviderKey;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;

public abstract class RulesEngineBean<DomainModel>
{
  // Start Required Inputs
  private RulesEngineDataRequest<DomainModel> rulesEngineDataRequest;
  private String ruleGroupId;
  private List<String> ruleCodes;
  private ShoppingDataProviderKey dataProviderKey;
  private DataCache dataCache;
  private Date rulePackageEffectiveDate = new Date();
  // End Required Inputs
  
  private DomainModel domainModel;

  public DomainModel executeRule() throws Exception
  {
    return getCachedOrExecuteRule(ruleCodes, dataProviderKey);
  }

  /**
   * @return <DomainModel> populated either from the cache or from the output of
   *         executing the specified rule.
   * @throws Exception
   */
  private DomainModel getCachedOrExecuteRule(List<String> ruleCodes, DataProviderKey dataProviderKey)
    throws Exception
  {
    RulesEngineDataRequest<DomainModel> dataRequest = createDataRequest(ruleGroupId, ruleCodes);
    
    DataProvider<DomainModel> dataProvider = new CamelRequestingDataProvider<DomainModel>(dataRequest);

    // Get the domain via a rule
    DomainModel results = dataCache.getData(dataProviderKey, dataProvider);

    return results;
  }

  /**
   * Method for building the simple <code>RulesEngineDataRequest</code> needed
   * to retrieve the DomainModel from the Rules Engine.
   * 
   * @param ruleGroupId
   * @param ruleCodes
   * @return RulesEngineDataRequest<DomainModel>
   */
  private RulesEngineDataRequest<DomainModel> createDataRequest(
    String ruleGroupId,
    List<String> ruleCodes)
  {
    // set properties on the request and return it.
    rulesEngineDataRequest.setGroupId(ruleGroupId);
    rulesEngineDataRequest.setRuleCodes(ruleCodes);

    rulesEngineDataRequest.setEffDate(new Date().getTime());

    if (null == domainModel)
    {
      domainModel = createDefaultDomainModel();
    }
    rulesEngineDataRequest.setDomain(domainModel);

    return rulesEngineDataRequest;
  }

  protected abstract DomainModel createDefaultDomainModel();
  
  public void setDomainModel(DomainModel domainModel)
  {
    this.domainModel = domainModel;
  }

  public Date getRulePackageEffectiveDate()
  {
    return rulePackageEffectiveDate;
  }

  public void setRulesEngineDataRequest(RulesEngineDataRequest<DomainModel> rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }

  public void setRuleGroupId(String ruleGroupId)
  {
    this.ruleGroupId = ruleGroupId;
  }

  public void setRuleCodes(List<String> ruleCodes)
  {
    this.ruleCodes = ruleCodes;
  }

  public void setDataCache(DataCache dataCache)
  {
    this.dataCache = dataCache;
  }

  public void setDataProviderKey(String dataProviderKeyString)
  {
    this.dataProviderKey = ShoppingDataProviderKey.findByKey(dataProviderKeyString);
  }
}
