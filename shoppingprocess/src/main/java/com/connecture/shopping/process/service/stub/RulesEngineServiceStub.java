package com.connecture.shopping.process.service.stub;

import com.connecture.shopping.process.common.JSONFileUtils;

public class RulesEngineServiceStub
{
  public String executeRules(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "rulesenginedata.json");
  }
}
