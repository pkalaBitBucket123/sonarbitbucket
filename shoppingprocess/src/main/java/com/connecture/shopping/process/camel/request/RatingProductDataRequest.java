package com.connecture.shopping.process.camel.request;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.shopping.process.service.data.ProductPlanData;

public class RatingProductDataRequest extends BaseCamelRequest<ProductPlanData>
{
  static final String EFF_DATE_KEY = "effDate";
  static final String IS_NEW_BUSINESS = "isNewBusiness";
  static final String ZIP_CODE = "zipCode";
  static final String COUNTY = "county";
  static final String IS_ON_EXCHANGE_PLANS = "isOnExchangePlans";
  static final String CSR_CODE = "csrCode";
  
  static final String PRIMARY_PREFIX = "primary";
  static final String WARD_PREFIX = "ward";
  static final String SPOUSE_PREFIX = "spouse";
  static final String CHILD_PREFIX = "children";
  
  static final String CHILD_ONLY = "childOnly";
  static final String CHILD_ONLY_YES = "Y";
  static final String CHILD_ONLY_NO = "N";
  
  static final String ID = "id";
  static final String AGE = "birthDate";
  static final String GENDER = "gender";
  static final String TOBACCO = "tobacco";
  static final String COVERGAE_TYPE = "coverageType";
  
  static final String DATE_PATTERN = "MM/dd/yyyy";
  
  @Produce(uri = "{{camel.shopping.producer.ratingProducts}}")
  private ProducerTemplate producer;
  
  private int childCount = 0;
  private int wardCount = 0;

  
  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  @Override
  public Map<String, Object> getRequestHeaders()
  {
    if (null == headers.get(CHILD_ONLY))
    {
      String isChildOnly = CHILD_ONLY_NO;
      if (childCount > 0) {
        isChildOnly = CHILD_ONLY_YES;
      }
      headers.put(CHILD_ONLY, isChildOnly);
    }
    
    return headers;
  }

  public void setEffectiveDate(Date effectiveDate)
  {
    headers.put(EFF_DATE_KEY, new SimpleDateFormat(DATE_PATTERN).format(effectiveDate));
  }

  public void setISNewBusiness(Boolean isNewBusiness)
  {
    headers.put(IS_NEW_BUSINESS, isNewBusiness);
  }

  public void setZipCode(String zipCode)
  {
    headers.put(ZIP_CODE, zipCode);
  }

  public void setCounty(String county)
  {
    headers.put(COUNTY, county);
  }
  
  public void setIsOnExchangePlans(boolean isOnExchangePlans)
  {
    headers.put(IS_ON_EXCHANGE_PLANS, isOnExchangePlans);
  }
  
  public void setCsrCode(String csrCode)
  {
    headers.put(CSR_CODE, csrCode);
  }
  
  public void setPrimary(String id, String birthDate, String gender, String tobacco) {
    headers.put(PRIMARY_PREFIX + "." + ID, id);
    headers.put(PRIMARY_PREFIX + "." + AGE, birthDate);
    headers.put(PRIMARY_PREFIX + "." + GENDER, gender);
    headers.put(PRIMARY_PREFIX + "." + TOBACCO, tobacco);
    
    headers.put(CHILD_ONLY, CHILD_ONLY_NO);
  }
  
  public void setSpouse(String id, String birthDate, String gender, String tobacco) {
    headers.put(SPOUSE_PREFIX + "." + ID, id);
    headers.put(SPOUSE_PREFIX + "." + AGE, birthDate);
    headers.put(SPOUSE_PREFIX + "." + GENDER, gender);
    headers.put(SPOUSE_PREFIX + "." + TOBACCO, tobacco);
    
    headers.put(CHILD_ONLY, CHILD_ONLY_NO);
  }
  
  public void addChild(String id, String birthDate, String gender, String tobacco) {
    headers.put(createCurrentChildKey(ID), id);
    headers.put(createCurrentChildKey(AGE), birthDate);
    headers.put(createCurrentChildKey(GENDER), gender);
    headers.put(createCurrentChildKey(TOBACCO), tobacco);
    
    childCount++;
  }
  
  public void setWard(String id, String birthDate, String gender, String tobacco) {
	    headers.put(createCurrentWardKey(ID), id);
	    headers.put(createCurrentWardKey(AGE), birthDate);
	    headers.put(createCurrentWardKey(GENDER), gender);
	    headers.put(createCurrentWardKey(TOBACCO), tobacco);
	    
	    headers.put(CHILD_ONLY, CHILD_ONLY_NO);
	    wardCount++;
	  }
  
  private String createCurrentChildKey(String key) {
    return CHILD_PREFIX + "[" + childCount + "]." + key;
  }
  
  private String createCurrentWardKey(String key) {
	    return WARD_PREFIX + "[" + wardCount + "]." + key;
	  }
  
  public void setCovergaeType(String coverageType)
  {
    headers.put(COVERGAE_TYPE, coverageType);
  }
}
