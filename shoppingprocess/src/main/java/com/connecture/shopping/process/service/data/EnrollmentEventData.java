package com.connecture.shopping.process.service.data;

import java.util.Date;

public class EnrollmentEventData
{
  private Date enrollmentEffectiveDate;
  private Date enrollmentEndDate;
  /**
   * @return the enrollmentEffectiveDate
   */
  public Date getEnrollmentEffectiveDate()
  {
    return enrollmentEffectiveDate;
  }
  /**
   * @param enrollmentEffectiveDate the enrollmentEffectiveDate to set
   */
  public void setEnrollmentEffectiveDate(Date enrollmentEffectiveDate)
  {
    this.enrollmentEffectiveDate = enrollmentEffectiveDate;
  }
  /**
   * @return the enrollmentEndDate
   */
  public Date getEnrollmentEndDate()
  {
    return enrollmentEndDate;
  }
  /**
   * @param enrollmentEndDate the enrollmentEndDate to set
   */
  public void setEnrollmentEndDate(Date enrollmentEndDate)
  {
    this.enrollmentEndDate = enrollmentEndDate;
  }
}