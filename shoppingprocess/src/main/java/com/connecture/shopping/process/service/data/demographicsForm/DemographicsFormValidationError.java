package com.connecture.shopping.process.service.data.demographicsForm;

public class DemographicsFormValidationError
{
  private String key;
  private String label;

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }
}