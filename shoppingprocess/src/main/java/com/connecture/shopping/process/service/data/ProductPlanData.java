package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class ProductPlanData
{
  private List<PlanData> plans = new ArrayList<PlanData>();
  private Integer paychecksPerYear;

  public List<PlanData> getPlans()
  {
    return plans;
  }

  public void setPlans(List<PlanData> plans)
  {
    this.plans = plans;
  }

  /**
   * @return the paychecksPerYear
   */
  public Integer getPaychecksPerYear()
  {
    return paychecksPerYear;
  }

  /**
   * @param paychecksPerYear the new value of paychecksPerYear
   */
  public void setPaychecksPerYear(Integer paychecksPerYear)
  {
    this.paychecksPerYear = paychecksPerYear;
  }
}