package com.connecture.shopping.process.service.data.availability;

public class AvailabilityProductLine
{
  private String code;
  private boolean available;

  public String getCode()
  {
    return code;
  }

  public void setCode(String code)
  {
    this.code = code;
  }

  public boolean isAvailable()
  {
    return available;
  }

  public void setAvailable(boolean available)
  {
    this.available = available;
  }
}