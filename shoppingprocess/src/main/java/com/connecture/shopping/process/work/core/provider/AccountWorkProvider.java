package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.core.AccountWork;

public abstract class AccountWorkProvider implements WorkProviderImpl<AccountWork>
{
  @Override
  public AccountWork getWork(
    String transactionId,
    String requestKey,
    WorkConstants.Method method,
    JSONObject params,
    JSONObject object) throws JSONException
  {
    AccountWork work = createWork();
    
    if (WorkConstants.Method.GET.equals(method))
    {
      work.setTransactionId(transactionId);
    }
    else if (WorkConstants.Method.UPDATE.equals(method))
    {
      JSONObject accountDataObject = object.getJSONObject("account");
      work.setValue(accountDataObject);

      work.setTransactionId(transactionId);
    }

    return work;
  }
}
