package com.connecture.shopping.process.common;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

/**
 * A converter based on a list of supported formats. The first format in the
 * list is considered the primary format and will be used in converted
 * Dates/Calendars to Strings.
 */
public class ShoppingDateConverter extends StrutsTypeConverter
{

  /**
   * @see com.opensymphony.webwork.util.WebWorkTypeConverter#convertFromString(java.util.Map,
   *      java.lang.String[], java.lang.Class)
   */
  @SuppressWarnings("rawtypes")
  public Object convertFromString(Map context, String[] values, Class toClass)
  {
    if (StringUtils.isBlank(values[0]))
    {
      return null;
    }
    // first check for valid lengths (no 3 digit or 1 digit dates)
    int length = values[0].length();
    int slash = values[0].indexOf('/');
    if (slash >= 0)
    {
      if (length != 10)
      {
        throw new TypeConversionException("Unsupported year length: "
          + values[0]);
      }
    }
    else
    {
      if (length != 8)
      {
        throw new TypeConversionException("Unsupported year length: "
          + values[0]);
      }
    }

    Date d = ShoppingDateUtils.stringToDate(values[0]);
    if (d != null)
    {
      return convertToFinal(d, toClass);
    }
    throw new TypeConversionException("Unable to parse date: " + values[0]);
  }

  private Object convertToFinal(Date dateValue, Class<?> toClass)
  {
    if (toClass.equals(Calendar.class))
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(dateValue);
      return cal;
    }
    return dateValue;
  }

  /**
   * @see com.opensymphony.webwork.util.WebWorkTypeConverter#convertToString(java.util.Map,
   *      java.lang.Object)
   */
  @Override
  @SuppressWarnings("rawtypes") // StrutsTypeConverter
  public String convertToString(Map context, Object o)
  {
    Date date = null;
    if (o instanceof Calendar)
    {
      date = ((Calendar) o).getTime();
    }
    else {
      date = (Date)o;
    }
    return ShoppingDateUtils.dateToString(date);
  }

}

