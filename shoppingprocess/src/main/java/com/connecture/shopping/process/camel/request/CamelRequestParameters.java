package com.connecture.shopping.process.camel.request;

public interface CamelRequestParameters
{
  public static final String CONTEXT_KEY = "context";
  public static final String TRANSACTION_ID_KEY = "transactionId";
}
