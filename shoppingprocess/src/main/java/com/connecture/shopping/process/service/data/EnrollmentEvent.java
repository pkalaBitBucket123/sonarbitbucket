package com.connecture.shopping.process.service.data;

public class EnrollmentEvent
{
  private String date;
  private String type;

  public String getDate()
  {
    return date;
  }

  public void setDate(String date)
  {
    this.date = date;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }
}
