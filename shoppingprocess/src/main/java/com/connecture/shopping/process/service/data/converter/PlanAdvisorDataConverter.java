package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.AnswerData;
import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.FilterData;
import com.connecture.shopping.process.service.data.FilterEvaluationData;
import com.connecture.shopping.process.service.data.FilterOptionData;
import com.connecture.shopping.process.service.data.FilterQuestionReferenceData;
import com.connecture.shopping.process.service.data.PlanAdvisorData;
import com.connecture.shopping.process.service.data.PlanAdvisorDataConstants;
import com.connecture.shopping.process.service.data.PlanEvaluationAnswerData;
import com.connecture.shopping.process.service.data.PlanEvaluationData;
import com.connecture.shopping.process.service.data.PlanEvaluationProductData;
import com.connecture.shopping.process.service.data.QuestionData;
import com.connecture.shopping.process.service.data.QuestionnaireData;
import com.connecture.shopping.process.service.data.WorkflowData;

/**
 * Returns a domain data object representing the converted contents of the json
 * (String) input data
 */
public class PlanAdvisorDataConverter implements DataConverter<String, PlanAdvisorData>
{
  @Override
  public PlanAdvisorData convertData(String data) throws Exception
  {
    PlanAdvisorData planAdvisorData = new PlanAdvisorData();

    // This implementation presumes the data is a json string
    JSONObject jsonObject = new JSONObject(data);
    
    String planAdvisorVersion = jsonObject.getString("questionSetHash");
    planAdvisorData.setVerisonId(planAdvisorVersion);

    // Create WorkflowData
    List<WorkflowData> workflowDataList = new ArrayList<WorkflowData>();

    JSONArray workflowObjects = jsonObject.getJSONArray("workflows");
    for (int p = 0; p < workflowObjects.length(); p++)
    {
      workflowDataList.add(createWorkflowData(workflowObjects.getJSONObject(p)));
    }

    // Create QuestionnaireData
    List<QuestionnaireData> questionnaireDataList = new ArrayList<QuestionnaireData>();

    JSONArray questionnaireObjects = jsonObject.getJSONArray("questionCategories");
    for (int p = 0; p < questionnaireObjects.length(); p++)
    {
      questionnaireDataList.add(createQuestionnaireData(questionnaireObjects.getJSONObject(p)));
    }

    // Create QuestionData
    List<QuestionData> questionDataList = new ArrayList<QuestionData>();

    JSONArray questionObjects = jsonObject.getJSONArray("questions");
    for (int p = 0; p < questionObjects.length(); p++)
    {
      questionDataList.add(createQuestionData(questionObjects.getJSONObject(p)));
    }

    // Create FilterData
    List<FilterData> filterDataList = new ArrayList<FilterData>();

    JSONArray filterObjects = jsonObject.getJSONArray("filters");
    for (int p = 0; p < filterObjects.length(); p++)
    {
      filterDataList.add(createFilterData(filterObjects.getJSONObject(p)));
    }

    // Create FilterEvaluationData
    Map<String, FilterEvaluationData> filterEvaluationDataMap = new HashMap<String, FilterEvaluationData>();

    JSONArray filterEvaluationObjects = jsonObject.getJSONArray("filterEvaluations");
    for (int fe = 0; fe < filterEvaluationObjects.length(); fe++)
    {
      FilterEvaluationData filterEvaluation = createFilterEvaluationData(filterEvaluationObjects
        .getJSONObject(fe));
      filterEvaluationDataMap.put(filterEvaluation.getFilterRefId(), filterEvaluation);
    }

    // Create PlanEvaluationData
    List<PlanEvaluationData> planEvaluationDataList = new ArrayList<PlanEvaluationData>();

    JSONArray planEvaluationObjects = jsonObject.getJSONArray("planEvaluations");
    for (int pe = 0; pe < planEvaluationObjects.length(); pe++)
    {
      planEvaluationDataList.add(createPlanEvaluationData(planEvaluationObjects.getJSONObject(pe)));
    }

    planAdvisorData.setWorkflows(workflowDataList);
    planAdvisorData.setQuestionnaires(questionnaireDataList);
    planAdvisorData.setQuestions(questionDataList);
    planAdvisorData.setFilters(filterDataList);
    planAdvisorData.setFilterEvaluationMap(filterEvaluationDataMap);
    planAdvisorData.setPlanEvaluations(planEvaluationDataList);

    // Return converted domain data
    return planAdvisorData;
  }

  private WorkflowData createWorkflowData(JSONObject workflowObject) throws JSONException
  {
    WorkflowData workflowData = new WorkflowData();

    workflowData.setWorkflowRefId(workflowObject.optString("workflowId"));
    workflowData.setTitle(workflowObject.optString("title"));
    workflowData.setInfoBlock(workflowObject.optString("description"));

    String value = workflowObject.optString("classOverride", null);
    if (null != value && !"null".equals(value))
    {
      workflowData.setClassOverride(value);
    }

    workflowData.setDefaultWorkflow(workflowObject.getBoolean("default"));

    List<String> questionRefIds = new ArrayList<String>();

    JSONArray questionRefIdArray = workflowObject.getJSONArray("questionRefIds");
    for (int q = 0; q < questionRefIdArray.length(); q++)
    {
      questionRefIds.add(questionRefIdArray.getString(q));
    }

    workflowData.setQuestionRefIds(questionRefIds);

    return workflowData;
  }

  private QuestionnaireData createQuestionnaireData(JSONObject questionnaireObject)
    throws JSONException
  {
    QuestionnaireData questionnaireData = new QuestionnaireData();

    questionnaireData.setQuestionnaireRefId(questionnaireObject.optString("categoryId"));
    questionnaireData.setTitle(questionnaireObject.optString("title"));
    questionnaireData.setHeader(questionnaireObject.optString("description"));

    String value = questionnaireObject.optString("classOverride", null);
    if (null != value && !"null".equals(value))
    {
      questionnaireData.setClassOverride(value);
    }

    return questionnaireData;
  }

  public static boolean hasValueNotNull(JSONObject object, String key) throws JSONException
  {
    return object.has(key) && !JSONObject.NULL.equals(object.get(key));
  }

  public static boolean hasValueNotNullNotNullString(JSONObject object, String key)
    throws JSONException
  {
    return hasValueNotNull(object, key) && !"null".equals(object.getString(key));
  }

  private QuestionData createQuestionData(JSONObject questionObject) throws JSONException
  {
    QuestionData questionData = new QuestionData();

    questionData.setQuestionRefId(questionObject.optString("questionId"));

    questionData.setQuestionnaireRefId(questionObject.optString("categoryRefId"));

    if (hasValueNotNull(questionObject, "required"))
    {
      questionData.setRequired(questionObject.getBoolean("required"));
    }

    if (hasValueNotNullNotNullString(questionObject, "text"))
    {
      questionData.setText(questionObject.getString("text"));
    }

    if (hasValueNotNullNotNullString(questionObject, "educationalBlock"))
    {
      questionData.setEducationalContent(questionObject.getString("educationalBlock"));
    }

    if (hasValueNotNullNotNullString(questionObject, "navText"))
    {
      questionData.setTitle(questionObject.getString("navText"));
    }

    if (hasValueNotNullNotNullString(questionObject, "helpText"))
    {
      questionData.setHelpText(questionObject.getString("helpText"));
    }

    if (hasValueNotNullNotNullString(questionObject, "helpType"))
    {
      questionData.setHelpType(questionObject.getString("helpType"));
    }

    if (hasValueNotNullNotNullString(questionObject, "displayType"))
    {
      questionData.setDisplayType(questionObject.getString("displayType"));
    }

    if (hasValueNotNullNotNullString(questionObject, "widgetId"))
    {
      questionData.setWidgetId(questionObject.getString("widgetId"));
    }

    if (hasValueNotNull(questionObject, "memberLevelQuestion"))
    {
      questionData.setMemberLevelQuestion(questionObject.getBoolean("memberLevelQuestion"));
    }

    if (hasValueNotNullNotNullString(questionObject, "filterRefId"))
    {
      questionData.setFilterRefId(questionObject.getString("filterRefId"));
    }

    if (hasValueNotNullNotNullString(questionObject, "dataTagKey"))
    {
      questionData.setDataTagKey(questionObject.getString("dataTagKey"));
    }

    if (hasValueNotNullNotNullString(questionObject, "evaluationType"))
    {
      questionData.setEvaluationType(questionObject.getString("evaluationType"));
    }

    if (hasValueNotNull(questionObject, "minimumAnswers"))
    {
      questionData.setMinimumAnswers(questionObject.getInt("minimumAnswers"));
    }

    if (hasValueNotNull(questionObject, "maximumAnswers"))
    {
      questionData.setMaximumAnswers(questionObject.getInt("maximumAnswers"));
    }

    if (hasValueNotNullNotNullString(questionObject, "classOverride"))
    {
      questionData.setClassOverride(questionObject.getString("classOverride"));
    }

    if (hasValueNotNullNotNullString(questionObject, "weight"))
    {
      questionData.setWeight(questionObject.getString("weight"));
    }

    // Create AnswerData
    JSONArray answerObjects = questionObject.optJSONArray("answers");
    if (null != answerObjects)
    {
      List<AnswerData> answerDataList = new ArrayList<AnswerData>();

      for (int p = 0; p < answerObjects.length(); p++)
      {
        answerDataList.add(createAnswerData(answerObjects.getJSONObject(p)));
      }
      questionData.setAnswers(answerDataList);
    }

    return questionData;
  }

  private AnswerData createAnswerData(JSONObject answerObject) throws JSONException
  {
    AnswerData answerData = new AnswerData();

    answerData.setAnswerId(answerObject.optString("answerId"));

    if (hasValueNotNullNotNullString(answerObject, "text"))
    {
      answerData.setText(answerObject.getString("text"));
    }

    if (hasValueNotNullNotNullString(answerObject, "summary"))
    {
      answerData.setSummaryText(answerObject.getString("summary"));
    }

    if (hasValueNotNullNotNullString(answerObject, "helpText"))
    {
      answerData.setHelpText(answerObject.getString("helpText"));
    }

    if (hasValueNotNullNotNullString(answerObject, "helpType"))
    {
      answerData.setHelpType(answerObject.getString("helpType"));
    }

    if (hasValueNotNullNotNullString(answerObject, "dataTagValue"))
    {
      answerData.setDataTagValue(answerObject.getString("dataTagValue"));
    }

    if (hasValueNotNullNotNullString(answerObject, "dataTagOperator"))
    {
      answerData.setDataTagOperator(convertDataTagOperator(answerObject
        .getString("dataTagOperator")));
    }

    if (hasValueNotNullNotNullString(answerObject, "filterOptionRefId"))
    {
      answerData.setFilterOptionRefId(answerObject.getString("filterOptionRefId"));
    }

    if (hasValueNotNullNotNullString(answerObject, "classOverride"))
    {
      answerData.setClassOverride(answerObject.getString("classOverride"));
    }

    return answerData;
  }

  private static String convertDisplayType(String displayType)
  {
    String resultDisplayType = "";

    if (PlanAdvisorDataConstants.DisplayType.SLIDER.equalsIgnoreCase(displayType))
    {
      resultDisplayType = "slider";
    }
    else if (PlanAdvisorDataConstants.DisplayType.CHECKBOX.equalsIgnoreCase(displayType))
    {
      resultDisplayType = "checkbox";
    }
    else if (PlanAdvisorDataConstants.DisplayType.RADIO_BUTTON.equalsIgnoreCase(displayType))
    {
      resultDisplayType = "radio";
    }
    else if (PlanAdvisorDataConstants.DisplayType.SELECT.equalsIgnoreCase(displayType))
    {
      resultDisplayType = "select";
    }

    return resultDisplayType;
  }

  private FilterData createFilterData(JSONObject filterObject) throws JSONException
  {
    FilterData filterData = new FilterData();

    filterData.setFilterRefId(filterObject.optString("filterId"));
    filterData.setTitle(filterObject.optString("title"));
    filterData.setEvaluationType(filterObject.optString("evaluationType"));
    filterData.setDisplayType(convertDisplayType(filterObject.optString("displayType")));

    if (hasValueNotNullNotNullString(filterObject, "helpText"))
    {
      filterData.setHelpText(filterObject.getString("helpText"));
    }

    if (hasValueNotNullNotNullString(filterObject, "helpType"))
    {
      filterData.setHelpType(filterObject.getString("helpType"));
    }

    if (hasValueNotNullNotNullString(filterObject, "classOverride"))
    {
      filterData.setClassOverride(filterObject.getString("classOverride"));
    }
    
    filterData.setDataTagKey(filterObject.optString("dataTagKey"));

    List<FilterOptionData> options = new ArrayList<FilterOptionData>();

    JSONArray optionObjects = filterObject.getJSONArray("options");
    for (int op = 0; op < optionObjects.length(); op++)
    {
      options.add(createFilterOptionData(optionObjects.getJSONObject(op)));
    }

    filterData.setOptions(options);

    return filterData;
  }

  private FilterOptionData createFilterOptionData(JSONObject optionObject) throws JSONException
  {
    FilterOptionData filterOptionData = new FilterOptionData();

    filterOptionData.setOptionRefId(optionObject.optString("optionId"));
    filterOptionData.setText(optionObject.optString("text"));

    if (hasValueNotNullNotNullString(optionObject, "helpText"))
    {
      filterOptionData.setHelpText(optionObject.getString("helpText"));
    }

    if (hasValueNotNullNotNullString(optionObject, "helpType"))
    {
      filterOptionData.setHelpType(optionObject.getString("helpType"));
    }

    if (hasValueNotNullNotNullString(optionObject, "classOverride"))
    {
      filterOptionData.setClassOverride(optionObject.getString("classOverride"));
    }

    String dataTagValue = optionObject.optString("dataTagValue");
    // plan advisor requires a value, even for space
    // using underscore
    if ("_".equalsIgnoreCase(dataTagValue))
    {
      dataTagValue = "";
    }

    filterOptionData.setDataTagValue(dataTagValue);

    // Plan Advisor specific conversion
    filterOptionData.setDataTagOperator(convertDataTagOperator(optionObject
      .optString("dataTagOperator")));

    return filterOptionData;
  }

  // Plan Advisor - specific values
  private static String convertDataTagOperator(String dataTagOperator)
  {
    String dataTagOperatorConverted = "";
    if (PlanAdvisorDataConstants.Operators.LESS_THAN.equalsIgnoreCase(dataTagOperator))
    {
      dataTagOperatorConverted = "<";
    }
    else if (PlanAdvisorDataConstants.Operators.GREATER_THAN.equalsIgnoreCase(dataTagOperator))
    {
      dataTagOperatorConverted = ">";
    }
    else if (PlanAdvisorDataConstants.Operators.IN_BETWEEN.equalsIgnoreCase(dataTagOperator))
    {
      dataTagOperatorConverted = "between";
    }
    else if (PlanAdvisorDataConstants.Operators.EQUALS.equalsIgnoreCase(dataTagOperator))
    {
      dataTagOperatorConverted = "=";
    }

    return dataTagOperatorConverted;
  }

  private FilterEvaluationData createFilterEvaluationData(JSONObject filterEvaluationObject)
    throws JSONException
  {
    FilterEvaluationData filterEvaluationData = new FilterEvaluationData();

    filterEvaluationData.setFilterRefId(filterEvaluationObject.getString("filterRefId"));

    Map<String, FilterQuestionReferenceData> filterOptionMap = new HashMap<String, FilterQuestionReferenceData>();

    JSONArray filterOptionsJSON = filterEvaluationObject.getJSONArray("options");
    for (int i = 0; i < filterOptionsJSON.length(); i++)
    {
      JSONObject optionJSON = filterOptionsJSON.getJSONObject(i);

      FilterQuestionReferenceData filterQuestionReferenceData = new FilterQuestionReferenceData();
      filterQuestionReferenceData.setFilterRefId(optionJSON.getString("optionRefId"));

      List<String> planIds = new ArrayList<String>();

      // get planIds for filter
      JSONArray planIdsJSON = optionJSON.optJSONArray("planIds");
      for (int j = 0; j < planIdsJSON.length(); j++)
      {
        planIds.add(planIdsJSON.getString(j));
      }

      filterQuestionReferenceData.setEnabledOptionRefIds(planIds);

      filterOptionMap
        .put(filterQuestionReferenceData.getFilterRefId(), filterQuestionReferenceData);
    }

    filterEvaluationData.setFilterOptionMap(filterOptionMap);

    return filterEvaluationData;
  }

  private PlanEvaluationData createPlanEvaluationData(JSONObject planEvaluationObject)
    throws JSONException
  {
    PlanEvaluationData retVal = new PlanEvaluationData();

    retVal.setQuestionRefId(planEvaluationObject.getString("questionRefId"));
    // not sure this one is required
    retVal.setQuestionWeight(planEvaluationObject.optLong("questionWeight"));
    retVal.setQuestionType(planEvaluationObject.getString("questionType"));

    JSONArray answersJSON = planEvaluationObject.optJSONArray("answers");

    if (answersJSON != null)
    {
      List<PlanEvaluationAnswerData> answers = new ArrayList<PlanEvaluationAnswerData>();
      for (int i = 0; i < answersJSON.length(); i++)
      {
        PlanEvaluationAnswerData answerData = new PlanEvaluationAnswerData();

        JSONObject answerJSON = answersJSON.getJSONObject(i);

        answerData.setAnswerRefId(answerJSON.getString("answerRefId"));

        JSONArray productRefs = answerJSON.optJSONArray("productRefs");

        if (productRefs != null)
        {
          List<PlanEvaluationProductData> products = new ArrayList<PlanEvaluationProductData>();
          for (int j = 0; j < productRefs.length(); j++)
          {
            PlanEvaluationProductData productData = new PlanEvaluationProductData();

            JSONObject productJSON = productRefs.getJSONObject(j);

            productData.setProductId(productJSON.getString("productId"));
            productData.setMatch(productJSON.optBoolean("match"));
            productData.setScore(productJSON.optLong("score"));

            products.add(productData);
          }

          answerData.setProducts(products);
        }

        answers.add(answerData);
      }

      retVal.setAnswers(answers);
    }

    return retVal;
  }
}