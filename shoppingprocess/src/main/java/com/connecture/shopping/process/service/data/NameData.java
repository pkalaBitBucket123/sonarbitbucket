/**
 * 
 */
package com.connecture.shopping.process.service.data;

/**
 * 
 */
public class NameData
{
  private String prefix;
  private String first;
  private String middle;
  private String last;

  /**
   * @return the first
   */
  public String getFirst()
  {
    return first;
  }

  /**
   * @param first the new value of first
   */
  public void setFirst(String first)
  {
    this.first = first;
  }

  /**
   * @return the last
   */
  public String getLast()
  {
    return last;
  }

  /**
   * @param last the new value of last
   */
  public void setLast(String last)
  {
    this.last = last;
  }

  public String getPrefix()
  {
    return prefix;
  }

  public void setPrefix(String prefix)
  {
    this.prefix = prefix;
  }

  public String getMiddle()
  {
    return middle;
  }

  public void setMiddle(String middle)
  {
    this.middle = middle;
  }
}