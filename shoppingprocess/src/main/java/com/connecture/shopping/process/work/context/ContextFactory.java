package com.connecture.shopping.process.work.context;

import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;

/**
 * @author Shopping Development
 */
public abstract class ContextFactory
{
  /**
   * @param context
   * @return
   * @throws Exception
   */
  public ContextWorkFactory getFactory(ShoppingContext context) throws Exception
  {
    ContextWorkFactory retVal = null;

    switch(context)
    {
      case EMPLOYEE:
      {
        retVal = createEmployeeWorkFactory();
        break;
      }
      case INDIVIDUAL:
      {
        retVal = createIndividualWorkFactory();
        break;
      }
      case ANONYMOUS:
      {
        retVal = createAnonymousWorkFactory();
        break;
      }
      default:
        break;
    }

    return retVal;

  }

  /**
   * @return
   */
  protected abstract ContextWorkFactory createEmployeeWorkFactory();

  /**
   * @return
   */
  protected abstract ContextWorkFactory createIndividualWorkFactory();
  
  /**
   * @return
   */
  protected abstract ContextWorkFactory createAnonymousWorkFactory();

}
