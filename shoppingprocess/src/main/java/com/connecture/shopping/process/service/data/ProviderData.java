package com.connecture.shopping.process.service.data;

import java.util.Map;

/**
 * This class represents a provider and its information.  Currently the default provider
 * supported is a physician which has a specific name object.  This can be broken out
 * into more objects if necessary to provide different types with different data.
 */
public class ProviderData
{
  private String xrefid;
  private NameData name;
  private ProviderType type;
  private Map<String, String> data;

  public ProviderType getType()
  {
    return type;
  }

  public void setType(ProviderType type)
  {
    this.type = type;
  }

  public Map<String, String> getData()
  {
    return data;
  }

  public void setData(Map<String, String> data)
  {
    this.data = data;
  }

  public String getId()
  {
    return xrefid;
  }

  public void setId(String id)
  {
    this.xrefid = id;
  }

  public NameData getName()
  {
    return name;
  }

  public void setName(NameData name)
  {
    this.name = name;
  }

}
