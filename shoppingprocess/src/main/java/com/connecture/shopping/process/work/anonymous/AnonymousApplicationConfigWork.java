package com.connecture.shopping.process.work.anonymous;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.Actor;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentEvent;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldMask;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldOption;
import com.connecture.shopping.process.work.core.ApplicationConfigWork;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventDaysLeftToEnrollWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventEffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventEnrollmentDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRuleEngineHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;
import com.connecture.shopping.process.work.core.ShoppingRulesEngineBean;

/**
 * @author shopping development Extending <code>ApplicationConfigWork</code> to add in the demographics configuration calls. This is most likely temporary because we are using the rules engine to
 *         handle that, so making this Anonymous specific so we don't muck up the IFP logic.
 */
public class AnonymousApplicationConfigWork extends ApplicationConfigWork
{
  private static Logger LOG = Logger.getLogger(AnonymousApplicationConfigWork.class);

  // Start Required Inputs
  private EnrollmentEventRulesEngineBean hcrEffectiveDateRulesEngineBean;
  private EnrollmentEventRulesEngineBean hcrSpecialEnrollmentReasonsRulesEngineBean;
  private ShoppingRulesEngineBean shoppingDemographicsRulesEngineBean;
  
  private Boolean ffmEnabled;
  private String hcrEffectiveDate;
  private String ffmPriorToApplyURL;
//Changes for PRDARCS-126
  private String ffmURL;
  private ActorService ifpActorService;
  private String tagUrl;
  private String startDateOE;
  private String endDateOE;
  // End Required Inputs

  // Start Outputs
  private ShoppingDomainModel demographicsRulesEngineDomainData;
  private EnrollmentEventDomainModel specialEnrollmentReasonsDomain;

  private String effectiveDateStr;
  private String enrollmentEndDateStr;
  // End Outputs
 
  private boolean isEffectiveDateProvided;
  
  /*
   * (non-Javadoc)
   * @see com.connecture.shopping.process.work.ApplicationConfigWork#get()
   */
  @Override
  public void get() throws Exception
  {
    // handle all of our parent logic
    super.get();

    JSONObject accountJSON = getAccount().getAccount(getTransactionId());
    
    configCriteria.put("ffmEnabled", Boolean.toString(ffmEnabled));
    
    // By default, use the HCR version of the bean
    EnrollmentEventRulesEngineBean enrollmentEventRulesEngineBean = hcrEffectiveDateRulesEngineBean;
    enrollmentEventRulesEngineBean.setDataCache(getDataCache());
    
    isEffectiveDateProvided = EnrollmentEventEffectiveDateWorkHelper.isEffectiveDateProvided(accountJSON); 
    effectiveDateStr = EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(accountJSON, enrollmentEventRulesEngineBean);

    EnrollmentEventDomainModel domainModel = EnrollmentEventRuleEngineHelper
      .getDomainModel(hcrEffectiveDateRulesEngineBean);
    
    enrollmentEndDateStr = EnrollmentEventEnrollmentDateWorkHelper.getHCREnrollmentEndDate(domainModel);
      
    configCriteria.put(
      "specialEnrollment", Boolean.toString(!EnrollmentEventEnrollmentDateWorkHelper.isWithinHCREnrollmentWindow(domainModel)));
    
    hcrSpecialEnrollmentReasonsRulesEngineBean.setDataCache(getDataCache());
    specialEnrollmentReasonsDomain = hcrSpecialEnrollmentReasonsRulesEngineBean.executeRule();      

    shoppingDemographicsRulesEngineBean.setCoverageEffectiveDate(EffectiveDateWorkHelper.getEffectiveDate(effectiveDateStr));
    shoppingDemographicsRulesEngineBean.setDataCache(getDataCache());
    demographicsRulesEngineDomainData = shoppingDemographicsRulesEngineBean.executeRule();
  }

  /*
   * (non-Javadoc)
   * @see com.connecture.shopping.process.work.ApplicationConfigWork#processData()
   */
  @Override
  public JSONObject processData() throws Exception
  {
    // return object
    JSONObject jsonObject = super.processData();

    JSONObject demographicsConfig = new JSONObject();
    Actor actor = ifpActorService.getActorData();

    if (null != demographicsRulesEngineDomainData)
    {
      if (StringUtils.isBlank(demographicsRulesEngineDomainData.getRequest().getError()))
      {
        // TODO | get specific items out of the rules engine result
        List<DemographicsFormField> demographicsFields = demographicsRulesEngineDomainData
          .getRequest().getDemographicsForm().getField();

        demographicsConfig.put("demographicFields",
          this.buildDemographicsFormFieldsJSON(demographicsFields));

        demographicsConfig.put("demographicsConfigMapping",
          getDemographicsConfigMemberFieldMappings(demographicsFields));

        // TOOD | get account data by version and validate demographic mappings
      }
      else
      {
        LOG.error("Unable to obtain demographic configuration from RulesEngine. Cause: "
          + demographicsRulesEngineDomainData.getRequest().getError());
      }
    }

    boolean hcrEffective = new Date().getTime() >= EnrollmentEventEffectiveDateWorkHelper.getEffectiveDateMs(hcrEffectiveDate);
   // demographicsConfig.put("hcrEffective", hcrEffective);
    demographicsConfig.put("hcrEffective", true);
    
    // we need to determine here if we need the datepicker to appear for 2013/2014.
    // conditions for showing the datepicker are:
    // 1. an effective date isn't already provided from the calling module (renewal)
    // 2. Health Care Reform (HCR) is not yet effective, in which case the user must choose the date before
    // advancing in shopping because the rules will differ.
    //demographicsConfig.put("showDatePicker", !(isEffectiveDateProvided || hcrEffective));
    demographicsConfig.put("showDatePicker", false);
    
    demographicsConfig.put("effectiveDate", effectiveDateStr);

    demographicsConfig.put("effectiveMs", EnrollmentEventEffectiveDateWorkHelper.getEffectiveDateMs(effectiveDateStr));
    demographicsConfig.put("userRole", actor.getUserRole());
    demographicsConfig.put("tagUrl", tagUrl);
    demographicsConfig.put("startDateOE", startDateOE);
    demographicsConfig.put("endDateOE", endDateOE);
    demographicsConfig.put("enrollmentEndDate", enrollmentEndDateStr);
    
    String enrollmentStartDate = EnrollmentEventEnrollmentDateWorkHelper.getEnrollmentStartDate(
      getAccount().getAccount(getTransactionId()), hcrEffectiveDateRulesEngineBean);
    if (StringUtils.isNotBlank(enrollmentStartDate))
    {
      demographicsConfig.put("enrollmentStartDate", enrollmentStartDate);
    }
    
    Integer daysLeftToEnroll = EnrollmentEventDaysLeftToEnrollWorkHelper.getDaysLeftToEnroll(getAccount().getAccount(getTransactionId()), hcrEffectiveDateRulesEngineBean);
    if (null != daysLeftToEnroll)
    {
      demographicsConfig.put("daysLeftToEnroll", daysLeftToEnroll);
    }

    JSONArray reasonTypesArray = new JSONArray();

    List<SpecialEnrollmentEvent> events = specialEnrollmentReasonsDomain.getApplication()
      .getSpecialEnrollment().getEvents();

    for (SpecialEnrollmentEvent event : events)
    {
      JSONObject reasonType = new JSONObject();
      reasonType.put("key", event.getEventType());
      reasonType.put("value", event.getDisplayName());
      reasonType.put("enrollmentDays", event.getEnrollmentDays());
      reasonTypesArray.put(reasonType);
    }

    demographicsConfig.put("specialEnrollmentEventTypes", reasonTypesArray);

    JSONObject demographicWidgetConfig = getWidget(jsonObject, "Demographics");

    // demographicWidgetConfig.put(WIDGET_ID_KEY, "Demographics");

    demographicWidgetConfig.put(WIDGET_CONFIG_KEY, demographicsConfig);

    // demographicWidgetConfig.put(WIDGET_TYPE_KEY, "Demographics");

    // addWidget(jsonObject, demographicWidgetConfig);
    
    JSONObject specialEnrollmentConfig = getWidget(jsonObject, "SpecialEnrollment");
    specialEnrollmentConfig.put(WIDGET_CONFIG_KEY, demographicsConfig);
    
    JSONObject enrollmentDateConfig = getWidget(jsonObject, "EnrollmentDate");
    enrollmentDateConfig.put(WIDGET_CONFIG_KEY, demographicsConfig);

    return jsonObject;
  }
  
  @Override
  protected void processApplicationConfigData(JSONObject jsonObject) throws JSONException
  {
    super.processApplicationConfigData(jsonObject);
    
    JSONObject applicationConfigJSON = jsonObject.getJSONObject("applicationConfig");
    applicationConfigJSON.put("ffmURL", ffmURL);
    
    applicationConfigJSON.put("ffmEnabled", ffmEnabled);
    applicationConfigJSON.put("ffmPriorToApplyURL", ffmPriorToApplyURL);
    applicationConfigJSON.put("quotingEnabled", true); // quoting is always available for internal users
    applicationConfigJSON.put("tagUrl", tagUrl);
    applicationConfigJSON.put("startDateOE", startDateOE);
    applicationConfigJSON.put("endDateOE", endDateOE);
  }

  private JSONArray buildDemographicsFormFieldsJSON(List<DemographicsFormField> demographicsFields)
    throws JSONException
  {
    // build individual field JSON and handle any list-level logic, like sorting

    // sort the fields by "sort" property
    Collections.sort(demographicsFields, DemographicsFormField.FieldOrderComparator);

    JSONArray demographicsFieldsJSON = new JSONArray();
    for (DemographicsFormField demographicsField : demographicsFields)
    {
      demographicsFieldsJSON.put(buildDemographicsFieldJSON(demographicsField));
    }

    return demographicsFieldsJSON;
  }

  private JSONObject buildDemographicsFieldJSON(DemographicsFormField demographicsFieldData)
    throws JSONException
  {
    JSONObject fieldJSON = new JSONObject();

    fieldJSON.put("key", demographicsFieldData.getKey());
    fieldJSON.put("name", demographicsFieldData.getName());
    fieldJSON.put("dataType", demographicsFieldData.getDataType());
    fieldJSON.put("styleClass", demographicsFieldData.getStyleClass());
    fieldJSON.put("headerStyleClass", demographicsFieldData.getHeaderStyleClass());
    fieldJSON.put("inputType", demographicsFieldData.getInputType());
    fieldJSON.put("label", demographicsFieldData.getLabel());
    fieldJSON.put("order", demographicsFieldData.getOrder());
    fieldJSON.put("size", demographicsFieldData.getSize());

    fieldJSON.put("options",
      this.buildDemographicsFieldOptionsJSON(demographicsFieldData.getOption()));
    fieldJSON.put("mask", buildDemographcisFieldMaskJSON(demographicsFieldData.getMask()));

    return fieldJSON;
  }

  private JSONObject buildDemographcisFieldMaskJSON(DemographicsFormFieldMask maskData)
    throws JSONException
  {
    JSONObject maskJSON = new JSONObject();
    if (maskData != null)
    {
      maskJSON.put("mask", maskData.getMask());
      maskJSON.put("maskClass", maskData.getMaskClass());
    }
    return maskJSON;
  }

  private JSONArray buildDemographicsFieldOptionsJSON(List<DemographicsFormFieldOption> options)
    throws JSONException
  {
    Collections.sort(options, DemographicsFormFieldOption.OptionOrderComparator);

    JSONArray optionsJSON = new JSONArray();
    if (options != null && !options.isEmpty())
    {
      for (DemographicsFormFieldOption optionData : options)
      {
        optionsJSON.put(buildDemographicsFieldOptionJSON(optionData));
      }
    }
    return optionsJSON;
  }

  private JSONObject buildDemographicsFieldOptionJSON(DemographicsFormFieldOption option)
    throws JSONException
  {
    JSONObject optionJSON = new JSONObject();
    optionJSON.put("dataGroupType", option.getDataGroupType());
    optionJSON.put("defaultSelection", option.getDefaultSelection());
    optionJSON.put("initialSelection", option.getInitialSelection());
    optionJSON.put("label", option.getLabel());
    optionJSON.put("order", option.getOrder());
    if ("_".equalsIgnoreCase(option.getValue()))
    {
      optionJSON.put("value", "");
    }
    else
    {
      optionJSON.put("value", option.getValue());
    }
    return optionJSON;
  }

  // TODO | account version ?
  // TODO | then another method to validate the mapping
  private JSONObject getDemographicsConfigMemberFieldMappings(
    List<DemographicsFormField> demographicsFields) throws JSONException
  {
    JSONObject demographicFieldMapping = new JSONObject();

    for (DemographicsFormField fieldData : demographicsFields)
    {
      demographicFieldMapping.put(fieldData.getKey(), fieldData.getName());
    }

    return demographicFieldMapping;
  }

  public void setHcrEffectiveDateRulesEngineBean(EnrollmentEventRulesEngineBean hcrEffectiveDateRulesEngineBean)
  {
    this.hcrEffectiveDateRulesEngineBean = hcrEffectiveDateRulesEngineBean;
  }

  public void setHcrSpecialEnrollmentReasonsRulesEngineBean(
    EnrollmentEventRulesEngineBean hcrSpecialEnrollmentReasonsRulesEngineBean)
  {
    this.hcrSpecialEnrollmentReasonsRulesEngineBean = hcrSpecialEnrollmentReasonsRulesEngineBean;
  }

  public void setShoppingDemographicsRulesEngineBean(
    ShoppingRulesEngineBean shoppingDemographicsRulesEngineBean)
  {
    this.shoppingDemographicsRulesEngineBean = shoppingDemographicsRulesEngineBean;
  }

  public void setHcrEffectiveDate(String hcrEffectiveDate)
  {
    this.hcrEffectiveDate = hcrEffectiveDate;
  }

  public void setFfmEnabled(Boolean ffmEnabled)
  {
    this.ffmEnabled = ffmEnabled;
  }

  public void setFfmPriorToApplyURL(String ffmPriorToApplyURL)
  {
    this.ffmPriorToApplyURL = ffmPriorToApplyURL;
  }
  
  public String getFfmURL() {
    return ffmURL;
  }
  
  public void setFfmURL(String ffmURL) {
    this.ffmURL = ffmURL;
  }
  
  public void setIfpActorService(ActorService ifpActorService)
  {
    this.ifpActorService = ifpActorService;
  }
  public void setTagUrl(String tagUrl)
  {
    this.tagUrl = tagUrl;
  }
  public void setStartDateOE(String startDateOE)
  {
    this.startDateOE = startDateOE;
  }
  public void setEndDateOE(String endDateOE)
  {
    this.endDateOE = endDateOE;
  }
}
