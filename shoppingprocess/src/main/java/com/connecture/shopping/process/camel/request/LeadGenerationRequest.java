package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.data.ShoppingLead;
import com.connecture.model.integration.data.ShoppingToIAData;

public class LeadGenerationRequest extends BaseCamelRequest<Boolean>
{
  public static final String SHOPPING_DATA_KEY = "shoppingData";
  public static final String SHOPPING_LEAD_KEY = "shoppingLeadData";
  
  @Produce(uri="{{camel.shopping.producer.leadGen}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }
  
  public void setTransactionId(String transactionId)
  {
    headers.put(TRANSACTION_ID_KEY, transactionId);
  }
  
  /**
   * for integration through client jar objects
   * @param shoppingData
   */
  public void setShoppingToIAData(ShoppingToIAData shoppingData)
  {
    headers.put(SHOPPING_DATA_KEY, shoppingData);
  }
  
  /**
   * for integration through client jar objects
   * @param shoppingData
   */
  public void setShoppingLead(ShoppingLead shoppingLeadData)
  {
    headers.put(SHOPPING_LEAD_KEY, shoppingLeadData);
  }
}
