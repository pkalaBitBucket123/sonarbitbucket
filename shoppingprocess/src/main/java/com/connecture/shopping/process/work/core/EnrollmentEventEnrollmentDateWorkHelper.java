package com.connecture.shopping.process.work.core;

import java.text.ParseException;
import java.util.Date;

import org.json.JSONObject;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;

public class EnrollmentEventEnrollmentDateWorkHelper
{
  public static boolean isWithinHCREnrollmentWindow(EnrollmentEventDomainModel domainModel)
  {
    boolean withinWindow = false;
    
    if (null != domainModel)
    {
      withinWindow = "YES".equalsIgnoreCase(domainModel.getApplication().getSpecialEnrollment().getOpenEnrollmentIndicator());
    }

    return withinWindow;
  }
  
  public static String getEnrollmentStartDate(
    JSONObject accountObject,
    EnrollmentEventRulesEngineBean hcrRulesEngineBean)
  {
    EnrollmentEventDomainModel domainModel = EnrollmentEventRuleEngineHelper
      .getDomainModel(hcrRulesEngineBean);

    return getHCREnrollmentStartDate(domainModel);
  }

  public static String getEnrollmentEndDate(
    JSONObject accountObject,
    EnrollmentEventRulesEngineBean hcrRulesEngineBean)
  {
    EnrollmentEventDomainModel domainModel = EnrollmentEventRuleEngineHelper
      .getDomainModel(hcrRulesEngineBean);

    return getHCREnrollmentEndDate(domainModel);
  }

  public static String getHCREnrollmentStartDate(EnrollmentEventDomainModel domainModel)
  {
    String enrollmentStartDate = null;

    if (null != domainModel)
    {
      enrollmentStartDate = domainModel.getApplication().getSpecialEnrollment()
        .getOpenEnrollmentStartDate();
    }

    return enrollmentStartDate;
  }

  public static String getHCREnrollmentEndDate(EnrollmentEventDomainModel domainModel)
  {
    String enrollmentEndDate = null;

    if (null != domainModel)
    {
      enrollmentEndDate = domainModel.getApplication().getSpecialEnrollment()
        .getOpenEnrollmentEndDate();
    }

    return enrollmentEndDate;
  }

  public static boolean inHCROpenEnrollment(EnrollmentEventDomainModel domainModel) throws ParseException
  {
    String enrollmentStartDateStr = EnrollmentEventEnrollmentDateWorkHelper.getHCREnrollmentStartDate(domainModel);
    String enrollmentEndDateStr = EnrollmentEventEnrollmentDateWorkHelper.getHCREnrollmentEndDate(domainModel);

    long enrollmentStartDateMs = EffectiveDateWorkHelper.getEffectiveDateMs(enrollmentStartDateStr);
    long enrollmentEndDateMs = EffectiveDateWorkHelper.getEffectiveDateMs(enrollmentEndDateStr);

    // SpecialEnrollment is only enabled if we are after the HCR enrollment end
    // date (at 23:59:59.999)
    boolean beforeOpenEnrollmentStartDate = new Date().getTime() < enrollmentStartDateMs + 24 * 60
      * 60 * 1000 - 1l;
    boolean afterOpenEnrollmentEndDate = new Date().getTime() > enrollmentEndDateMs + 24 * 60 * 60
      * 1000 - 1l;

    return !(beforeOpenEnrollmentStartDate || afterOpenEnrollmentEndDate);
  }
}
