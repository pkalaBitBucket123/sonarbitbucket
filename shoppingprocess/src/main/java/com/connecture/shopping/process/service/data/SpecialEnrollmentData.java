package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class SpecialEnrollmentData
{
  private String openEnrollmentStartDate;
  private String openEnrollmentEndDate;
  private Integer daysLeftToEnroll;
  private String withinEnrollmentWindow;
  private String selectedSpecialEnrollmentReason;
  private String eventDate; 
  private String openEnrollmentIndicator;
  private List<SpecialEnrollmentEvent> events = new ArrayList<SpecialEnrollmentEvent>();

  public String getOpenEnrollmentStartDate()
  {
    return openEnrollmentStartDate;
  }

  public void setOpenEnrollmentStartDate(String openEnrollmentStartDate)
  {
    this.openEnrollmentStartDate = openEnrollmentStartDate;
  }

  public String getOpenEnrollmentEndDate()
  {
    return openEnrollmentEndDate;
  }

  public void setOpenEnrollmentEndDate(String openEnrollmentEndDate)
  {
    this.openEnrollmentEndDate = openEnrollmentEndDate;
  }

  public Integer getDaysLeftToEnroll()
  {
    return daysLeftToEnroll;
  }

  public void setDaysLeftToEnroll(Integer daysLeftToEnroll)
  {
    this.daysLeftToEnroll = daysLeftToEnroll;
  }
  
  public String getWithinEnrollmentWindow()
  {
    return withinEnrollmentWindow;
  }

  public void setWithinEnrollmentWindow(String withinEnrollmentWindow)
  {
    this.withinEnrollmentWindow = withinEnrollmentWindow;
  }

  public String getSelectedSpecialEnrollmentReason()
  {
    return selectedSpecialEnrollmentReason;
  }

  public void setSelectedSpecialEnrollmentReason(String selectedSpecialEnrollmentReason)
  {
    this.selectedSpecialEnrollmentReason = selectedSpecialEnrollmentReason;
  }

  public String getEventDate()
  {
    return eventDate;
  }

  public void setEventDate(String eventDate)
  {
    this.eventDate = eventDate;
  }

  public void addSpecialEnrollmentEvent(SpecialEnrollmentEvent event) 
  {
    events.add(event);
  }
  
  public List<SpecialEnrollmentEvent> getEvents()
  {
    return new ArrayList<SpecialEnrollmentEvent>(events);
  }

  public String getOpenEnrollmentIndicator()
  {
    return openEnrollmentIndicator;
  }

  public void setOpenEnrollmentIndicator(String openEnrollmentIndicator)
  {
    this.openEnrollmentIndicator = openEnrollmentIndicator;
  }
}