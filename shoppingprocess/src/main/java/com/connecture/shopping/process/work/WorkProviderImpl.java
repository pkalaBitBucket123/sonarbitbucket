package com.connecture.shopping.process.work;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants.Method;

public interface WorkProviderImpl<W extends Work>
{
  W createWork();

  W getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException;
}
