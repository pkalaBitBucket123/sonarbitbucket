package com.connecture.shopping.process.service.data.demographicsForm;

import java.util.ArrayList;
import java.util.List;

public class DemographicsFormMember
{
  private String id;
  private List<DemographicsFormMemberField> field = new ArrayList<DemographicsFormMemberField>();

  public String getId()
  {
    return id;
  }

  public void setId(String id)
  {
    this.id = id;
  }

  public List<DemographicsFormMemberField> getField()
  {
    return field;
  }

  public void setField(List<DemographicsFormMemberField> field)
  {
    this.field = field;
  }
}