package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.IntegrationException;
import com.connecture.shopping.process.common.JSONUtils;
import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.PlanRiderData;
import com.connecture.shopping.process.service.data.ProductData;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.service.plan.data.CustomAttributeData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryDataComparator;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitDataComparator;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueSetData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueTypeGroupData;

/**
 * Returns a domain data object representing the converted contents of the json
 * (String) input data
 */
public class ProductDataConverter implements DataConverter<String, ProductPlanData>
{
  private static Logger LOG = Logger.getLogger(ProductDataConverter.class);

  @Override
  public ProductPlanData convertData(String data) throws Exception
  {
    ProductPlanData productPlanData = new ProductPlanData();

    // When we don't get product data back, we actually get a "null" String
    if (!"null".equals(data) && StringUtils.isNotBlank(data))
    {
      List<PlanData> planDataList = new ArrayList<PlanData>();

      // This implementation presumes the data is a json string
      // Based on input, it looks like it is a JSONArray of objects.
      JSONArray planObjects = null;
      try
      {
        planObjects = new JSONArray(data);
      }
      catch (JSONException e)
      {
        throw new IntegrationException("The product data provided was not in the proper format.", e);
      }

      if (LOG.isDebugEnabled())
      {
        LOG.debug("ProductServiceDataConverter - input data " + planObjects.toString(4));
      }

      for (int p = 0; p < planObjects.length(); p++)
      {
        planDataList.add(createPlanData(planObjects.getJSONObject(p)));
      }

      productPlanData.setPlans(planDataList);
    }
    else
    {
      LOG.warn("*** No Product Data ***");
    }

    return productPlanData;
  }

  protected PlanData createPlanData(JSONObject planObject) throws JSONException
  {
    PlanData planData = new PlanData();

    planData.setIdentifier(planObject.getString("planId"));
    planData.setExtRefId(planObject.getString("externalCode"));
    planData.setInternalCode(planObject.getString("internalCode"));
    planData.setPlanName(planObject.getString("displayName"));
    planData.setMetalTier(planObject.optString("planMetalTier"));
    try{
    	planData.setBenefitSummaryUrl(planObject.optString("benefitSummaryUrl"));
    }catch(Exception e){}
    // start change(s) by rsingh for user story 229 
    if(planObject.has("description"))
    	planData.setDescription(planObject.getString("description"));
    // end change(s) by rsingh for user story 229 
    // get network(s)
    JSONArray networksJSON = planObject.optJSONArray("networks");
    List<String> networks = new LinkedList<String>();
    for (int i = 0; i < networksJSON.length(); i++)
    {
      // TODO This might not be the correct value, as networks is a collection
      // of NetworkView objects.
      networks.add(networksJSON.optString(i));
    }
    planData.setNetworks(networks);

    JSONObject productTypeObject = planObject.getJSONObject("productType");

    ProductData productData = new ProductData();

    // get productLine code from productType object
    String productTypeCode = productTypeObject.getString("internalCode");
    // translate the code into a value that can be used by the UI, if a
    // translated value exists
    productData.setProductLineCode(productTypeCode);

    productData.setProductLineName(productTypeObject.getString("displayName"));
    productData.setSortOrder(productTypeObject.getInt("sortOrder"));
    planData.setProductData(productData);

    JSONObject planTypeObject = planObject.getJSONObject("planType");
    String planTypeExternalCode = planTypeObject.getString("externalCode");
    planData.setPlanType(planTypeExternalCode);

    // get benefits
    JSONObject benefitsObject = planObject.getJSONObject("benefits");

    // get benefits
    JSONArray benefitCategoriesJSON = benefitsObject.getJSONArray("benefitCategories");
    planData.setBenefitCategories(this.getBenefitCategories(benefitCategoriesJSON));

    // get all benefits (flat)
    JSONArray benefitsJSON = benefitsObject.getJSONArray("allBenefits");
    planData.setBenefits(this.getBenefits(benefitsJSON));

    if (planObject.has("sortOrder"))
    {
      int sortOrder = planObject.getInt("sortOrder");
      planData.setSortOrder(sortOrder);
    }
  //start change(s) by rsingh for user story 20,21,92,258,260,229 
    if(planObject.has("sbcUrl"))
    	planData.setSbcUrl(planObject.getString("sbcUrl"));
    if(planObject.has("brochureUrl"))
    	planData.setBrochureUrl(planObject.getString("brochureUrl"));
  //end change(s) by rsingh for user story  20,21,92,258,260,229  
    
  //<NEBRASKA RIDER SUPPORT MI>
    List<PlanRiderData> riders = getRiders(planObject);
    planData.setRiders(riders);
  //</NEBRASKA RIDER SUPPORT MI>
    return planData;
  }
//<NEBRASKA RIDER SUPPORT MI>
  private List<PlanRiderData> getRiders(JSONObject planObject) throws JSONException {
	if(planObject.has("riders")){
		JSONArray array = planObject.getJSONArray("riders");
		List<PlanRiderData> riders = new ArrayList<PlanRiderData>();
		for(int i = 0; i<array.length();i++){
			JSONObject o = array.getJSONObject(i);
			riders.add(toPlanRiderData(o));
		}
		return riders;
	}
	return null;
  }

  private PlanRiderData toPlanRiderData(JSONObject o) throws JSONException {
	  PlanRiderData p = new PlanRiderData();
	  p.setAlternateName(o.getString("alternateName"));
	  if(o.has("benefitSummary"))
	  p.setBenefitSummary(o.getString("benefitSummary"));
	  p.setDescription(o.getString("description"));
	  if(o.has("monthlyRate"))
	  p.setDisplayMonthlyRate(String.valueOf(o.getDouble("monthlyRate")));
	  p.setDisplayName(o.getString("name"));
	  if(o.has("monthlyRate"))
	  p.setMonthlyRate(o.getDouble("monthlyRate"));
	  p.setRiderId(o.getLong("riderId"));
	  if(o.has("xref"))
	  p.setXref(o.getString("xref"));
	return p;
  }
//</NEBRASKA RIDER SUPPORT MI>
private List<PlanBenefitCategoryData> getBenefitCategories(JSONArray benefitCategoriesJSON)
    throws JSONException
  {
    // linked hashMap to keep order
    List<PlanBenefitCategoryData> resultData = new ArrayList<PlanBenefitCategoryData>();

    for (int i = 0; i < benefitCategoriesJSON.length(); i++)
    {
      JSONObject benefitCategoryJSON = benefitCategoriesJSON.getJSONObject(i);
      PlanBenefitCategoryData benefitCategory = new PlanBenefitCategoryData();

      // benefit category name; e.g. "In Network / Out of Network",
      // "Standard-Single Value"
      String benefitCategoryName = benefitCategoryJSON.getString("name");
      benefitCategory.setName(benefitCategoryName);

      int sortOrder = benefitCategoryJSON.getInt("sortOrder");
      benefitCategory.setSortOrder(sortOrder);

      // benefit values
      JSONArray benefitsJSON = benefitCategoryJSON.getJSONArray("benefits");
      benefitCategory.setBenefits(this.getBenefits(benefitsJSON));

      resultData.add(benefitCategory);
    }

    Collections.sort(resultData, new PlanBenefitCategoryDataComparator());

    return resultData;
  }

  private List<PlanBenefitData> getBenefits(JSONArray benefitsJSON) throws JSONException
  {
    List<PlanBenefitData> resultData = new ArrayList<PlanBenefitData>();

    for (JSONObject benefitJSON : JSONUtils.toIterable(JSONObject.class, benefitsJSON))
    {
      PlanBenefitData benefit = new PlanBenefitData();

      // benefit type group
      JSONObject benefitValueTypeGroupJSON = benefitJSON.getJSONObject("benefitValueTypeGroup");
      benefit.setBenefitValueTypeGroup(this.getBenefitValueTypeGroup(benefitValueTypeGroupJSON));

      // displayName
      String displayName = benefitJSON.getString("displayName");
      benefit.setDisplayName(displayName);

      // internalCode
      String internalCode = benefitJSON.getString("internalCode");
      benefit.setInternalCode(internalCode);

      // externalCode
      String externalCode = benefitJSON.getString("externalCode");
      benefit.setExternalCode(externalCode);

      // get benefit value set Map
      JSONArray benefitValueSetsJSON = benefitJSON.getJSONArray("benefitValueSets");
      benefit.setBenefitValueSetMap(this.getBenefitValueSetMap(benefitValueSetsJSON));

      // custom attributes
      List<CustomAttributeData> customAttributes = getCustomAttributes(benefitJSON);
      benefit.setCustomAttributes(customAttributes);

      // selected value
      String selectedValue = benefitJSON.getString("selectedValue");
      benefit.setSelectedValue(selectedValue);

      // sortOrder
      int sortOrder = benefitJSON.getInt("sortOrder");
      benefit.setSortOrder(sortOrder);

      // add benefit to the result
      resultData.add(benefit);
    }

    // sort list
    Collections.sort(resultData, new PlanBenefitDataComparator());

    return resultData;
  }

  public List<CustomAttributeData> getCustomAttributes(JSONObject benefitJSON) throws JSONException
  {
    List<CustomAttributeData> customAttributes = new ArrayList<CustomAttributeData>();
    if (benefitJSON.has("customAttributes"))
    {
      JSONObject customAttributesObject = benefitJSON.getJSONObject("customAttributes");
      if (customAttributesObject.has("all"))
      {
        JSONArray attributesArray = customAttributesObject.getJSONArray("all");

        for (int a = 0; a < attributesArray.length(); a++)
        {
          JSONObject attributeObject = attributesArray.getJSONObject(a);
          CustomAttributeData attributeData = new CustomAttributeData();
          String value = "false";
          if (attributeObject.has("choiceCode"))
          {
            value = attributeObject.getString("choiceCode");
            if ("true|true".equals(value))
            {
              value = "true";
            }
          }
          else if (attributeObject.has("value")) {
            value = attributeObject.getString("value");
          }
          attributeData.setAttributeValue(value);
          attributeData.setCode(attributeObject.getString("code"));
          attributeData.setDisplayName(attributeObject.getString("displayName"));
          customAttributes.add(attributeData);
        }
      }
    }
    return customAttributes;
  }

  private PlanBenefitValueTypeGroupData getBenefitValueTypeGroup(
    JSONObject benefitValueTypeGroupJSON) throws JSONException
  {
    PlanBenefitValueTypeGroupData benefitValueTypeGroup = new PlanBenefitValueTypeGroupData();

    String name = benefitValueTypeGroupJSON.getString("name");

    benefitValueTypeGroup.setName(name);

    return benefitValueTypeGroup;
  }

  private Map<String, PlanBenefitValueSetData> getBenefitValueSetMap(JSONArray benefitValueSetsJSON)
    throws JSONException
  {
    Map<String, PlanBenefitValueSetData> resultData = new LinkedHashMap<String, PlanBenefitValueSetData>();

    for (int i = 0; i < benefitValueSetsJSON.length(); i++)
    {
      JSONObject benefitValueSetJSON = benefitValueSetsJSON.getJSONObject(i);
      PlanBenefitValueSetData benefitValueSet = new PlanBenefitValueSetData();

      // exclude invalid typeCodes
      JSONArray benefitValueKeysJSON = benefitValueSetJSON.getJSONArray("benefitValueKeys");

      List<String> benefitValueKeyList = new ArrayList<String>();
      for (int j = 0; j < benefitValueKeysJSON.length(); j++)
      {
        benefitValueKeyList.add(benefitValueKeysJSON.getString(j));
      }

      benefitValueSet.setBenefitValueKeys(benefitValueKeyList);

      // externalCode
      String externalCode = benefitValueSetJSON.getString("externalCode");
      benefitValueSet.setExternalCode(externalCode);

      // internalCode
      String internalCode = benefitValueSetJSON.getString("internalCode");
      benefitValueSet.setInternalCode(internalCode);

      // benefit value Map
      JSONArray benefitValuesJSON = benefitValueSetJSON.getJSONArray("benefitValues");
      benefitValueSet.setBenefitValueMap(this.getPlanBenefitValueMap(benefitValuesJSON));

      // add to result map
      resultData.put(benefitValueSet.getInternalCode(), benefitValueSet);
    }

    return resultData;
  }

  private Map<String, PlanBenefitValueData> getPlanBenefitValueMap(JSONArray benefitValuesJSON)
    throws JSONException
  {
    Map<String, PlanBenefitValueData> resultData = new LinkedHashMap<String, PlanBenefitValueData>();

    for (int i = 0; i < benefitValuesJSON.length(); i++)
    {
      JSONObject benefitValueJSON = benefitValuesJSON.getJSONObject(i);

      // benefitValueTypeCode
      String benefitValueTypeCode = benefitValueJSON.getString("benefitValueTypeCode");

      PlanBenefitValueData benefitValue = new PlanBenefitValueData();
      benefitValue.setBenefitValueTypeCode(benefitValueTypeCode);

      // displayName
      String displayName = benefitValueJSON.getString("displayName");
      benefitValue.setDisplayName(displayName);

      // description
      String description = "";
      if (benefitValueJSON.has("description"))
      {
        description = benefitValueJSON.getString("description");
      }
      benefitValue.setDescription(description);

      // numeric value
      Double numericValue = benefitValueJSON.optDouble("numericValue");
      if (!numericValue.isNaN())
      {
        benefitValue.setNumericValue(numericValue);
      }

      List<CustomAttributeData> customAttributes = getCustomAttributes(benefitValueJSON);

      benefitValue.setCustomAttributes(customAttributes);

      resultData.put(benefitValue.getBenefitValueTypeCode(), benefitValue);
    }

    return resultData;
  }
}