package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class UsageCategory
{
  private String categoryId;
  private String display;

  // NOTE: The following are initialized so when it is marshalled it becomes an empty JSONArray.
  // The rulesEngine requires this
  private List<UsageData> usageData = new ArrayList<UsageData>();

  public String getCategoryId()
  {
    return categoryId;
  }

  public void setCategoryId(String categoryId)
  {
    this.categoryId = categoryId;
  }

  public String getDisplay()
  {
    return display;
  }

  public void setDisplay(String display)
  {
    this.display = display;
  }

  public List<UsageData> getUsageData()
  {
    return usageData;
  }

  public void setUsageData(List<UsageData> usageData)
  {
    this.usageData = usageData;
  }
}
