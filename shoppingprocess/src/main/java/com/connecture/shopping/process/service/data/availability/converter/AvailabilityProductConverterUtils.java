package com.connecture.shopping.process.service.data.availability.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.availability.AvailabilityProduct;
import com.connecture.shopping.process.service.data.availability.AvailabilityProductBenefit;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;

public class AvailabilityProductConverterUtils
{
  // private static Logger LOG =
  // Logger.getLogger(AvailabilityProductConverterUtils.class);

  private static final String INTERNAL_CODE = "internalCode";
  private static final String AVAILABILITY_CODE = "availabilityCode";
  private static final String AVAILABILITY_REASON = "availabilityReason";

  // FROM PLANDATA

  public static List<AvailabilityProduct> fromPlanData(List<PlanData> plans)
  {
    List<AvailabilityProduct> availabilityProducts = new ArrayList<AvailabilityProduct>();

    for (PlanData plan : plans)
    {
      availabilityProducts.add(fromPlanData(plan));
    }

    return availabilityProducts;
  }

  private static AvailabilityProduct fromPlanData(PlanData planData)
  {
    AvailabilityProduct availabilityProduct = new AvailabilityProduct();
    availabilityProduct.setInternalCode(planData.getInternalCode());
    availabilityProduct.setType(planData.getPlanType());
    availabilityProduct.setProductLineCode(planData.getProductData().getProductLineCode());

    List<AvailabilityProductBenefit> productBenefits = new ArrayList<AvailabilityProductBenefit>();

    for (PlanBenefitCategoryData benefitCategory : planData.getBenefitCategories())
    {
      for (PlanBenefitData benefit : benefitCategory.getBenefits())
      {
        AvailabilityProductBenefit productBenefit = new AvailabilityProductBenefit();
        productBenefit.setInternalCode(benefit.getInternalCode());
        productBenefits.add(productBenefit);
      }
    }

    availabilityProduct.setBenefits(productBenefits);

    return availabilityProduct;
  }

  // FROM JSON
  public static List<AvailabilityProduct> fromJSON(JSONArray productsJSON) throws JSONException
  {
    List<AvailabilityProduct> availabilityProducts = new ArrayList<AvailabilityProduct>();
    if (productsJSON != null){
      for (int i = 0; i < productsJSON.length(); i++)
      {
        JSONObject productJSON = productsJSON.getJSONObject(i);
        availabilityProducts.add(fromJSON(productJSON));
      }
    }
    return availabilityProducts;
  }
  
  private static AvailabilityProduct fromJSON(JSONObject productJSON) throws JSONException
  {
    AvailabilityProduct product = new AvailabilityProduct();
    product.setInternalCode(productJSON.getString(INTERNAL_CODE));
    product.setAvailabilityCode(productJSON.getString(AVAILABILITY_CODE));
    if (productJSON.has(AVAILABILITY_REASON)){
      product.setAvailabilityReason(productJSON.getString(AVAILABILITY_REASON));
    }
    return product;
  }
}
