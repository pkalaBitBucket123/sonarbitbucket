package com.connecture.shopping.process.service.stub;

import java.util.ArrayList;
import java.util.List;

import com.connecture.model.integration.api.IAShoppingIntegrationProvider;
import com.connecture.model.integration.data.IAToShoppingContactData;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingMemberDetails;
import com.connecture.model.integration.data.ShoppingToIAData;
import com.connecture.model.integration.data.TransactionIDData;
import com.connecture.shopping.process.common.ShoppingDateUtils;

public class IndividualDataServiceStub implements IAShoppingIntegrationProvider
{
  @Override
  public TransactionIDData buildTransactionIDDataFromTransactionId(String transactionId)
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IAToShoppingData getDataFromIA(String transactionId)
  {
    IAToShoppingData mockData = new IAToShoppingData();

    mockData.setTransactionId(transactionId);

//    IAToShoppingFfmHouseholdData ffmHouseholdData = new IAToShoppingFfmHouseholdData();
//    ffmHouseholdData.setOnExchangePlans(true);
//    mockData.setFfmHouseholdData(ffmHouseholdData);
    
    List<IAToShoppingMemberDetails> memberDetails = new ArrayList<IAToShoppingMemberDetails>();

    IAToShoppingMemberDetails simpleMemberDetails = new IAToShoppingMemberDetails();
    simpleMemberDetails.setCounty("");
    simpleMemberDetails.setMemberId(12345);
    simpleMemberDetails.setDateOfBirth(ShoppingDateUtils.stringToDate("11/10/1981"));
    simpleMemberDetails.setFirstName("Francis");
    simpleMemberDetails.setGender("M");
    simpleMemberDetails.setMemberType("MEMBER");
    simpleMemberDetails.setSmokerStatus("true");
    simpleMemberDetails.setStateKey("");
    simpleMemberDetails.setZipCode("53186");
    simpleMemberDetails.setCounty("1289");

    IAToShoppingMemberDetails simpleSpouseDetails = new IAToShoppingMemberDetails();
    simpleSpouseDetails.setCounty("");
    simpleSpouseDetails.setMemberId(12377);
    simpleSpouseDetails.setDateOfBirth(ShoppingDateUtils.stringToDate("04/10/1983"));
    simpleSpouseDetails.setFirstName("Molly");
    simpleSpouseDetails.setGender("M");
    simpleSpouseDetails.setMemberType("SPOUSE");
    simpleSpouseDetails.setSmokerStatus(null);
    simpleSpouseDetails.setStateKey("");
    simpleSpouseDetails.setZipCode("53186");
    simpleSpouseDetails.setCounty("1289");
    
    memberDetails.add(simpleMemberDetails);
    memberDetails.add(simpleSpouseDetails);

    mockData.setMemberDetails(memberDetails);

    IAToShoppingContactData contactData = new IAToShoppingContactData();
    contactData.setFirstName("Vic");
    contactData.setLastName("Mackey");
    contactData.setPhoneNumber("415-444-4444");

    mockData.setAccountHolderContactData(contactData);

//    mockData.setSpecialEnrollmentRequired(true);

//    IAToShoppingRenewalData renewalData = new IAToShoppingRenewalData();
//
//    renewalData.setRenewingDefaultPlanInternalCode("HMO_Max_500");
//
//    mockData.setRenewalData(renewalData);

    // need zipCode, countyId (TODO | looks like we only get this through IA member data)

    return mockData;
  }

  @Override
  public ShoppingToIAData getShoppingToIADataForAnonymousEnrollment()
  {
    // TODO Auto-generated method stub
    return null;
  }

}