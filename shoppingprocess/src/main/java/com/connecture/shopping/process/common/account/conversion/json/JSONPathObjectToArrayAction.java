package com.connecture.shopping.process.common.account.conversion.json;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONPathObjectToArrayAction implements JSONPathAction
{
  private String rename;
  
  public JSONPathObjectToArrayAction()
  {
  }
  
  public JSONPathObjectToArrayAction(String rename)
  {
    this.rename = rename;
  }

  @Override
  public boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException
  {
    boolean success = false;

    if (isValidFor(state))
    {
      Object removed = jsonObject.remove(key);

      if (!StringUtils.isBlank(rename)){
        key = rename;
      }
      
      if (removed instanceof JSONObject)
      {
        jsonObject.put(key, new JSONArray().put(0, removed));
      }
      else
      {
        jsonObject.put(key, removed);
      }

      success = removed != null;
    }

    return success;
  }

  @Override
  public boolean isValidFor(PathState state)
  {
    return JSONPathAction.PathState.FOUND.equals(state);
  }

  public String getRename()
  {
    return rename;
  }

  public void setRename(String rename)
  {
    this.rename = rename;
  }
}