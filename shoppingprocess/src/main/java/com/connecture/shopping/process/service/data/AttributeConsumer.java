package com.connecture.shopping.process.service.data;

public class AttributeConsumer
{
  private String xid;
  private Integer cost;

  public String getXid()
  {
    return xid;
  }

  public void setXid(String xid)
  {
    this.xid = xid;
  }

  public Integer getCost()
  {
    return cost;
  }

  public void setCost(Integer cost)
  {
    this.cost = cost;
  }
}
