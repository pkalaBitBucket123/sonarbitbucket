package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.shopping.process.service.data.ProvidersData;

public class ProviderSearchDataRequest extends BaseCamelRequest<ProvidersData>
{
  public static final String PROVIDER_SEARCH_KEY = "providerSearch";
  
  @Produce(uri = "{{camel.shopping.producer.providerSearch}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setProviderSearch(String providerSearch)
  {
    headers.put(PROVIDER_SEARCH_KEY, providerSearch);
  }
}
