package com.connecture.shopping.process.common.account.conversion.json;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

public abstract class JSONValidationUtils
{
  public static void checkForMissingField(
    List<String> errors,
    String prefix,
    JSONObject jsonObject,
    String fieldName)
  {
    if (!jsonObject.has(fieldName)) {
      errors.add("Missing required field " + (StringUtils.isBlank(prefix) ? "" : prefix + ".") + fieldName);
    }
  }

  public static void checkForMissingField(
    List<String> errors,
    JSONObject jsonObject,
    String fieldName)
  {
    checkForMissingField(errors, null, jsonObject, fieldName);
  }
}
