package com.connecture.shopping.process.service.data;

public class ProductData
{
  private String productLineName;
  private String productLineCode;

  private Integer sortOrder;
  
  private Integer planCount;
  
  public String getProductLineName()
  {
    return productLineName;
  }

  public void setProductLineName(String productLineName)
  {
    this.productLineName = productLineName;
  }
  
  /**
   * @return the productLineCode
   */
  public String getProductLineCode()
  {
    return productLineCode;
  }

  /**
   * @param productLineCode the new value of productLineCode
   */
  public void setProductLineCode(String productLineCode)
  {
    this.productLineCode = productLineCode;
  }
  
  public Integer getSortOrder()
  {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  
  public Integer getPlanCount()
  {
    return planCount;
  }

  public void setPlanCount(Integer planCount)
  {
    this.planCount = planCount;
  }

  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((productLineName == null) ? 0 : productLineName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    ProductData other = (ProductData) obj;
    if (productLineName == null)
    {
      if (other.productLineName != null) return false;
    }
    else if (!productLineName.equals(other.productLineName)) return false;
    return true;
  }
  
  
}
