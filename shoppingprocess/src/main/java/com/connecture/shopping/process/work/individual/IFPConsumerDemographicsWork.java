/**
 *
 */
package com.connecture.shopping.process.work.individual;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.Actor;
import com.connecture.model.data.ShoppingProducer;
import com.connecture.model.integration.data.IAToShoppingContactData;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.IAToShoppingFfmHouseholdData;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.camel.request.ProducerDemographicsDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.ActorService;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.core.ConsumerDemographicsWork;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;

/**
 *
 */
public class IFPConsumerDemographicsWork extends ConsumerDemographicsWork
{
  private static Logger LOG = Logger.getLogger(IFPConsumerDemographicsWork.class);

  private ProducerDemographicsDataRequest producerDemographicsDataRequest;

  // inputs
  private ShoppingProducer shoppingProducer;

  // individual request objects
  private IAToShoppingIndividualDataRequest individualDataRequest;

  // IFP ConsumerDemographics data
  private IAToShoppingData individualData;
  private ActorService ifpActorService;

  /**
   * @see com.connecture.shopping.process.work.Work#get()
   */
  @Override
  public void get() throws Exception
  {
    // This will override to retrieve consumer data from IA as well
    super.get();

    // get consumer data from IA

    // get a reference to a new or existing account for the transaction Id
    Actor actor = ifpActorService.getActorData();
    Account account = getAccount();
    JSONObject accountJSON = account.getAccount(getTransactionId());

    individualData = getIndividualData();

    // create a clone; but we only really need the members
    JSONObject currentShoppingJSON = new JSONObject(accountJSON.toString());

    String effectiveDateStr = this.getEffectiveDate();
    accountJSON.put("effectiveDate", effectiveDateStr);
    accountJSON.put("effectiveMs", EffectiveDateWorkHelper.getEffectiveDateMs(effectiveDateStr));
    accountJSON.put("userRole", actor.getUserRole());

    // we need this to get the correct plans for the household
    boolean isOnExchange = false;
    IAToShoppingFfmHouseholdData householdData = individualData.getFfmHouseholdData();
    if (null != householdData)
    {
      isOnExchange = householdData.isOnExchangePlans();
      if(isOnExchange)
      {
        accountJSON.put("csrCode", householdData.getCsrCode());
      }
    }
    accountJSON.put("isOnExchangePlans", isOnExchange);

    Map<String, String> optionValueTypeMap = buildOptionValueTypeMap(effectiveDateStr);

    // 3. find any incoming IFP Data
    //  3a. populate member data
    this.populateAccountDemographicData(accountJSON, individualData, optionValueTypeMap, false);

    // 4. try to match plan advisor data for old users to new Ids
    Map<String, String> oldNewMemberIdMapping = this.getUpdatedMemberReferences(
      currentShoppingJSON, accountJSON);

    Set<String> newMemberIds = this.getLatestMemberIds(accountJSON);

    // 5. try to preserve "provider" data, and anything else stored on the
    // member that has not come back to us from IA
    updateShoppingSpecificMemberData(currentShoppingJSON.optJSONArray("members"),
      accountJSON.optJSONArray("members"), oldNewMemberIdMapping);

    if (!oldNewMemberIdMapping.isEmpty())
    {
      // update cart (coveredMembers) data
      this.updateAccountCartMemberIds(oldNewMemberIdMapping, accountJSON);

      // update questions (members)
      this.updateQuestionMemberIds(oldNewMemberIdMapping, accountJSON);
    }

    // remove data for members that no longer exist
    // see if anything exists for members NOT in newMemberIds; if so, remove
    // those items
    this.cleanUpQuestionMemberIds(newMemberIds, accountJSON);

    // 6. update/save the account with data from 3, 4.
    account.updateAccountInfo(getTransactionId(), accountJSON);

    JSONObject params = getParams();

    // use the same key for both, should be fine
    if (params.has(ProducerDemographicsDataRequest.PRODUCER_TOKEN))
    {
      String producerToken = params.getString(ProducerDemographicsDataRequest.PRODUCER_TOKEN);
      producerDemographicsDataRequest.setProducerToken(producerToken);

      DataProvider<ShoppingProducer> dataProvider = new CamelRequestingDataProvider<ShoppingProducer>(producerDemographicsDataRequest);
      shoppingProducer = getCachedData(ShoppingDataProviderKey.PRODUCER, dataProvider);
    }
  }

  void updateShoppingSpecificMemberData(
    JSONArray oldMemberData,
    JSONArray newMemberData,
    Map<String, String> oldNewMemberIdMapping) throws JSONException
  {
    if (oldMemberData != null && newMemberData != null)
    {
      // try to find a match based on demographics
      for (int i = 0; i < oldMemberData.length(); i++)
      {
        JSONObject oldMember = oldMemberData.getJSONObject(i);
        for (int j = 0; j < newMemberData.length(); j++)
        {
          JSONObject newMember = newMemberData.getJSONObject(j);

          String oldMemberRefName = oldMember.getString("memberRefName");
          String newMemberRefName = newMember.getString("memberRefName");

          boolean isMatch = (!oldNewMemberIdMapping.isEmpty() && newMemberRefName
            .equalsIgnoreCase(oldNewMemberIdMapping.get(oldMemberRefName)))
            || newMemberRefName.equalsIgnoreCase(oldMemberRefName);

          if (isMatch)
          {
            if (!oldMember.isNull("providers"))
            {
              newMember.put("providers", oldMember.getJSONArray("providers"));
            }
          }
        }
      }
    }
  }

  void updateAccountCartMemberIds(
    Map<String, String> oldNewMemberIdMapping,
    JSONObject accountJSON) throws JSONException
  {
    JSONObject cartJSON = accountJSON.optJSONObject("cart");
    if (cartJSON != null)
    {
      JSONObject productLinesJSON = cartJSON.optJSONObject("productLines");
      if (productLinesJSON != null)
      {
        String[] productLineNames = JSONObject.getNames(productLinesJSON);
        if (null != productLineNames)
        {
          for (String productLine : JSONObject.getNames(productLinesJSON))
          {
            JSONObject productLineJSON = productLinesJSON.getJSONObject(productLine);
            JSONArray coveredMembersJSON = productLineJSON.optJSONArray("coveredMembers");
            if (coveredMembersJSON != null)
            {
              for (int i = 0; i < coveredMembersJSON.length(); i++)
              {
                JSONObject coveredMemberJSON = coveredMembersJSON.getJSONObject(i);
                String memberRefName = coveredMemberJSON.getString("memberRefName");
                // swap out memberRefName
                String newMemberId = oldNewMemberIdMapping.get(memberRefName);
                if (newMemberId != null)
                {
                  if (LOG.isDebugEnabled())
                  {
                    LOG.debug("Swapping account coveredMember ID : [" + memberRefName + "] for ID : ["
                      + newMemberId + "]");
                  }
                  coveredMemberJSON.put("memberRefName", newMemberId);
                }
              }
            }
          }
        }
      }
    }
  }

  void cleanUpQuestionMemberIds(Set<String> memberIds, JSONObject accountJSON)
    throws JSONException
  {
    JSONArray questionsJSON = accountJSON.optJSONArray("questions");
    if (questionsJSON != null)
    {
      for (int i = 0; i < questionsJSON.length(); i++)
      {
        JSONObject questionJSON = questionsJSON.getJSONObject(i);
        JSONArray membersJSON = questionJSON.optJSONArray("members");
        if (membersJSON != null)
        {
          JSONArray remainingMembersJSON = new JSONArray();
          for (int j = 0; j < membersJSON.length(); j++)
          {
            JSONObject memberJSON = membersJSON.getJSONObject(j);
            String memberRefName = memberJSON.getString("memberRefName");
            if (memberIds.contains(memberRefName))
            {
              // no longer a valid member; remove question
              remainingMembersJSON.put(memberJSON);
            }
          }
          questionJSON.put("members", remainingMembersJSON);
        }
      }
    }
  }

  void updateQuestionMemberIds(
    Map<String, String> oldNewMemberIdMapping,
    JSONObject accountJSON) throws JSONException
  {
    JSONArray questionsJSON = accountJSON.optJSONArray("questions");
    if (questionsJSON != null)
    {
      for (int i = 0; i < questionsJSON.length(); i++)
      {
        JSONObject questionJSON = questionsJSON.getJSONObject(i);
        JSONArray membersJSON = questionJSON.optJSONArray("members");
        if (membersJSON != null)
        {
          for (int j = 0; j < membersJSON.length(); j++)
          {
            JSONObject memberJSON = membersJSON.getJSONObject(j);
            String memberRefName = memberJSON.getString("memberRefName");
            // swap out memberRefName
            String newMemberId = oldNewMemberIdMapping.get(memberRefName);
            if (newMemberId != null)
            {
              if (LOG.isDebugEnabled())
              {
                LOG.debug("Swapping question member ID : [" + memberRefName + "] for ID : ["
                  + newMemberId + "]");
              }
              memberJSON.put("memberRefName", newMemberId);
            }
          }
        }
      }
    }
  }

  Map<String, String> getUpdatedMemberReferences(
    JSONObject currentShoppingJSON,
    JSONObject newAccountJSON) throws JSONException
  {
    Map<String, String> oldNewMemberMapping = new HashMap<String, String>();

    // saved in local shopping
    JSONArray shoppingMembers = currentShoppingJSON.optJSONArray("members");

    // from external system
    JSONArray externalMembers = newAccountJSON.optJSONArray("members");
    // Will always be populated with at least an empty JSONArray

    if (shoppingMembers != null)
    {
      // try to find a match based on demographics
      for (int i = 0; i < shoppingMembers.length(); i++)
      {
        JSONObject oldMember = shoppingMembers.getJSONObject(i);
        for (int j = 0; j < externalMembers.length(); j++)
        {
          JSONObject newMember = externalMembers.getJSONObject(j);

          String oldMemberRefName = oldMember.getString("memberRefName");
          String newMemberRefName = newMember.getString("memberRefName");

          // if the memberRefNames are different but the demographics match,
          // add to updatedRefName map.
          if (!oldMemberRefName.equalsIgnoreCase(newMemberRefName)
            && memberDemographicsMatch(oldMember, newMember))
          {
            oldNewMemberMapping.put(oldMemberRefName, newMemberRefName);
            break;
          }
        }
      }
    }
    return oldNewMemberMapping;
  }

  Set<String> getLatestMemberIds(JSONObject accountJSON) throws JSONException
  {
    Set<String> memberIdSet = new HashSet<String>();

    JSONArray members = accountJSON.getJSONArray("members");

    // try to find a match based on demographics
    for (int i = 0; i < members.length(); i++)
    {
      JSONObject member = members.getJSONObject(i);
      // try to find a match in the NEW members
      memberIdSet.add(member.getString("memberRefName"));
    }

    return memberIdSet;
  }

  boolean memberDemographicsMatch(JSONObject member1, JSONObject member2)
    throws JSONException
  {
    boolean isMatch = propertyMatchString(member1, member2, "birthDate")
      && propertyMatchString(member1, member2, "gender")
      && propertyMatchString(member1, member2, "memberRelationship")
      && propertyMatchString(member1.getJSONObject("name"), member2.getJSONObject("name"), "first")
      && propertyMatchOptionalBooleanString(member1, member2, "isSmoker");
    return isMatch;
  }

  private boolean propertyMatchString(JSONObject member1, JSONObject member2, String propertyName)
    throws JSONException
  {
    return member1.getString(propertyName).equalsIgnoreCase(member2.getString(propertyName));
  }

  public static boolean propertyMatchOptionalBooleanString(
    JSONObject member1,
    JSONObject member2,
    String propertyName) throws JSONException
  {
    if (member1.isNull(propertyName))
    {
      return member2.isNull(propertyName) || "n".equalsIgnoreCase(member2.getString(propertyName));
    }
    else
    {
      return ("n".equalsIgnoreCase(member1.getString(propertyName)) && member2.isNull(propertyName) )
        || member1.getString(propertyName).equalsIgnoreCase(member2.optString(propertyName));
    }
  }

  private IAToShoppingData getIndividualData() throws Exception
  {
    individualDataRequest.setTransactionId(getTransactionId());
    individualDataRequest.setContext(ShoppingContext.INDIVIDUAL);
    DataProvider<IAToShoppingData> dataProvider = new CamelRequestingDataProvider<IAToShoppingData>(individualDataRequest);
    return getCachedData(ShoppingDataProviderKey.INDIVIDUAL_DATA, dataProvider);
  }

  static JSONObject buildAccountHolderData(IAToShoppingContactData iaContactData)
    throws JSONException
  {
    JSONObject accountHolderJSON = new JSONObject();
    if (iaContactData != null)
    {
      accountHolderJSON.put("firstName", iaContactData.getFirstName());
      accountHolderJSON.put("lastName", iaContactData.getLastName());
      accountHolderJSON.put("email", iaContactData.getEmail());
      accountHolderJSON.put("phone", iaContactData.getPhoneNumber());
    }
    return accountHolderJSON;
  }

  /**
   * @see com.connecture.shopping.process.work.Work#processData(java.lang.Object)
   */
  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = super.processData();

    // account holder data
    jsonObject.put("accountHolder",
      buildAccountHolderData(individualData.getAccountHolderContactData()));

    // add config criteria
    // This is being set in the IFP application work;
    // configCriteria.put("specialEnrollment", Boolean.toString(individualData.isSpecialEnrollmentRequired()));

    String isOnExchange = Boolean.FALSE.toString();
    IAToShoppingFfmHouseholdData householdData = individualData.getFfmHouseholdData();
    if (null != householdData)
    {
      isOnExchange = Boolean.toString(householdData.isOnExchangePlans());
    }
    // coerce a disabled demographics, onExchange workflow
    configCriteria.put("onExchange", isOnExchange);

    if (shoppingProducer != null)
    {
      JSONObject producerDemographics = processProducerDemographics(shoppingProducer);

      jsonObject.put("producer", producerDemographics);
    }

    return jsonObject;
  }

  public void setIndividualDataRequest(IAToShoppingIndividualDataRequest individualDataRequest)
  {
    this.individualDataRequest = individualDataRequest;
  }

  public void setProducerDemographicsDataRequest(
    ProducerDemographicsDataRequest producerDemographicsDataRequest)
  {
    this.producerDemographicsDataRequest = producerDemographicsDataRequest;
  }
  public void setIfpActorService(ActorService ifpActorService)
  {
    this.ifpActorService = ifpActorService;
  }
}