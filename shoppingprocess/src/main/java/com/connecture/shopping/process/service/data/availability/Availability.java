package com.connecture.shopping.process.service.data.availability;

import java.util.ArrayList;
import java.util.List;

public class Availability
{
  private String contextCode;
  private List<AvailabilityProduct> selectedProducts = new ArrayList<AvailabilityProduct>();
  private List<AvailabilityProduct> unselectedProducts = new ArrayList<AvailabilityProduct>();
  
  public List<AvailabilityProduct> getSelectedProducts()
  {
    return selectedProducts;
  }
  public void setSelectedProducts(List<AvailabilityProduct> selectedProducts)
  {
    this.selectedProducts = selectedProducts;
  }
  public List<AvailabilityProduct> getUnselectedProducts()
  {
    return unselectedProducts;
  }
  public void setUnselectedProducts(List<AvailabilityProduct> unselectedProducts)
  {
    this.unselectedProducts = unselectedProducts;
  }
  public String getContextCode()
  {
    return contextCode;
  }
  public void setContextCode(String contextCode)
  {
    this.contextCode = contextCode;
  }
}