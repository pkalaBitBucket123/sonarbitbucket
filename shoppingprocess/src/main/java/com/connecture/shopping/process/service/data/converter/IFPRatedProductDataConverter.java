package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductPlanData;

public class IFPRatedProductDataConverter implements DataConverter<String, ProductPlanData>
{
  public ProductPlanData convertData(String data) throws Exception
  {
    ProductPlanData productPlanData = new ProductPlanData();
    List<PlanData> planDataList = new ArrayList<PlanData>();

    // This implementation presumes the data is a JSON object
    // following a particular known format.
    JSONObject ratedPlansJSON = new JSONObject(data);
    JSONArray ratedPlanJSONElements = ratedPlansJSON.names();
    for (int i = 0; i < ratedPlanJSONElements.length(); i++)
    {
      JSONArray ratedCategoryJSONArray = ratedPlanJSONElements.getJSONArray(i);
      for (int j = 0; j < ratedCategoryJSONArray.length(); j++)
      {
        JSONObject ratedPlanJSON = ratedCategoryJSONArray.getJSONObject(j);
        planDataList.add(createIFPRatedPlanData(ratedPlanJSON));
      }
    }

    productPlanData.setPlans(planDataList);
    return productPlanData;
  }

  private PlanData createIFPRatedPlanData(JSONObject planObject) throws JSONException
  {
    PlanData planData = new PlanData();

    planData.setIdentifier(planObject.getString("planId"));

    // planData.setInternalCode(internalCode);
    // planData.setExtRefId(extRefId);

    // planData.setMetalTier(metalTier);
    // planData.setBenefitCategories(benefitCategories);
    // planData.setBenefits(benefits);
    // planData.setContributionData(contributionData);
    // planData.setPlanCost(planCost);
    // planData.setPlanName(planName);
    // planData.setPlanType(planType);
    // planData.setProductData(productData);
    // planData.setRate(rate);
    // planData.setRate(rate);
    // planData.setTierCode(tierCode);
    //
    return planData;
  }
}
