package com.connecture.shopping.process.camel.request;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.EnrollmentEventApplication;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentData;
import com.connecture.shopping.process.service.data.converter.EnrollmentEventRulesEngineDataConverter;

public class EnrollmentEventRulesEngineDataRequest extends RulesEngineDataRequest<EnrollmentEventDomainModel>
{
  private static Logger LOG = Logger.getLogger(EnrollmentEventRulesEngineDataRequest.class);
  
  @Override
  public EnrollmentEventDomainModel convertData(Object data) throws Exception
  {
    return new EnrollmentEventRulesEngineDataConverter().convertData((String)data);
  }

  /**
   * Manually generate the JSON so it conforms to the HCR domain model naming
   * convention, i.e. underscore-delimitation (aggrevation, leading to impending
   * inebriation)
   */
  @Override
  public Object getRequestBody()
  {
    JSONObject requestObject = new JSONObject();
    
    try
    {
      EnrollmentEventDomainModel domain = getDomain();
      if (null != domain) 
      {
        EnrollmentEventApplication application = domain.getApplication();
        JSONObject applicationJSON = new JSONObject();
        
        if (null != application.getEffectiveDate()) 
        {
          applicationJSON.put("Effective_Date", application.getEffectiveDate());
        }
        
        SpecialEnrollmentData specialEnrollment = application.getSpecialEnrollment();
        
        JSONObject specialEnrollmentJSON = new JSONObject();
        
        if (null != specialEnrollment.getEventDate())
        {
          specialEnrollmentJSON.put("Event_Date", specialEnrollment.getEventDate());
        }
        
        if (null != specialEnrollment.getSelectedSpecialEnrollmentReason())
        {
          specialEnrollmentJSON.put("Selected_Special_Enrollment_Reason", specialEnrollment.getSelectedSpecialEnrollmentReason());
        }
        
        applicationJSON.put("Special_Enrollment", specialEnrollmentJSON);
      
        requestObject.put("Application", applicationJSON);
      }
    }
    catch (JSONException e)
    {
      LOG.error("Failed to convert HCRDomainModel to JSON", e);
    }
    
    // We will want to pass things through, eventually
    return requestObject.toString();
  }
}
