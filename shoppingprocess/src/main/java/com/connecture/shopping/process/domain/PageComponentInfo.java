package com.connecture.shopping.process.domain;

import java.io.Serializable;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

@Entity
@Table(name = "SHOP_PAGE_COMPONENT")
@GenericGenerator(name = "PageComponentInfo", strategy = "org.hibernate.id.enhanced.TableGenerator", parameters = {
    @Parameter(name = "segment_value", value = "SHOP_PAGE_COMPONENT"),
    @Parameter(name = "increment_size", value = "10"),
    @Parameter(name = "optimizer", value = "pooled")})
@SuppressWarnings("serial")
public class PageComponentInfo implements Serializable
{
  @Id
  @GeneratedValue(generator = "PageComponentInfo")
  @Column(name = "PAGE_COMPONENT_ID")
  private long pageComponentId;

  @Column(name = "COMPONENT_KEY")
  private String componentKey;
  
  @Column(name = "LOCALE")
  @Type(type = "com.connecture.shopping.process.common.LocaleUserType")
  private Locale locale;
  
  @Column(name = "CONTENT")
  @Type(type = "com.connecture.shopping.process.common.JSONObjectUserType")
  private JSONObject content;
    
  public long getPageComponentId()
  {
    return pageComponentId;
  }
  
  public void setPageComponentId(long pageComponentId)
  {
    this.pageComponentId = pageComponentId; 
  }
  
  public String getComponentKey()
  {
    return componentKey;
  }
  
  public void setComponentKey(String componentKey)
  {
    this.componentKey = componentKey;
  }
  
  public Locale getLocale()
  {
    return locale;
  }
  
  public void setLocale(Locale locale)
  {
    this.locale = locale;
  }
  
  public JSONObject getContent()
  {
    return content;
  }
  
  public void setContent(JSONObject content)
  {
    this.content = content;
  }
}
