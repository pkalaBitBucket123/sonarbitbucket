package com.connecture.shopping.process.common.account;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class UserAccount implements Serializable
{
  /**
   * 
   */
  private static final long serialVersionUID = 4125610911212189262L;

  private String transactionId;

  // private JSONObject account = new JSONObject();
  private String account = "";

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public JSONObject getAccount()
  {
    JSONObject retVal = new JSONObject();
    // TODO, sallow for now
    if (!"".equals(account))
    {
      try
      {
        retVal = new JSONObject(account);
      }
      catch (JSONException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    return retVal;
  }

  public void setAccount(JSONObject account)
  {
    this.account = account.toString();
  }
}
