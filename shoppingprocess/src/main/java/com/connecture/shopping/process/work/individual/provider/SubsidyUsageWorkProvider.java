package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.individual.SubsidyUsageWork;

public abstract class SubsidyUsageWorkProvider implements WorkProviderImpl<SubsidyUsageWork>
{
  @Override
  public SubsidyUsageWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    SubsidyUsageWork work = createWork();
    // will contain our big chunk of plans, we hope :)
    work.setInputJSON(data);
    work.setTransactionId(transactionId);
    return work;
  }

}
