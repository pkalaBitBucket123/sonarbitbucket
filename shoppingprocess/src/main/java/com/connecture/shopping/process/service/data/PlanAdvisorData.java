package com.connecture.shopping.process.service.data;

import java.util.List;
import java.util.Map;

public class PlanAdvisorData
{
  private String verisonId;
  private List<QuestionData> questions;
  private List<QuestionnaireData> questionnaires;
  private Map<String, FilterEvaluationData> filterEvaluationMap;
  private List<FilterData> filters;
  private List<PlanEvaluationData> planEvaluations;
  private List<WorkflowData> workflows;

  public List<QuestionData> getQuestions()
  {
    return questions;
  }

  public void setQuestions(List<QuestionData> questions)
  {
    this.questions = questions;
  }

  public List<QuestionnaireData> getQuestionnaires()
  {
    return questionnaires;
  }

  public void setQuestionnaires(List<QuestionnaireData> questionnaires)
  {
    this.questionnaires = questionnaires;
  }

  public List<WorkflowData> getWorkflows()
  {
    return workflows;
  }

  public void setWorkflows(List<WorkflowData> workflows)
  {
    this.workflows = workflows;
  }

  /**
   * @return the filterEvaluationMap
   */
  public Map<String, FilterEvaluationData> getFilterEvaluationMap()
  {
    return filterEvaluationMap;
  }

  /**
   * @param filterEvaluationMap the new value of filterEvaluationMap
   */
  public void setFilterEvaluationMap(Map<String, FilterEvaluationData> filterEvaluationMap)
  {
    this.filterEvaluationMap = filterEvaluationMap;
  }

  public List<FilterData> getFilters()
  {
    return filters;
  }

  public void setFilters(List<FilterData> filters)
  {
    this.filters = filters;
  }

  public List<PlanEvaluationData> getPlanEvaluations()
  {
    return planEvaluations;
  }

  public void setPlanEvaluations(List<PlanEvaluationData> planEvaluations)
  {
    this.planEvaluations = planEvaluations;
  }

  public String getVersionId()
  {
    return this.verisonId;
  }

  public void setVerisonId(String verisonId)
  {
    this.verisonId = verisonId;
  }
}
