package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.individual.IFPPovertyIncomeLevelWork;

public abstract class IFPPovertyIncomeLevelWorkProvider implements WorkProviderImpl<IFPPovertyIncomeLevelWork>
{
  @Override
  public IFPPovertyIncomeLevelWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    IFPPovertyIncomeLevelWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}
