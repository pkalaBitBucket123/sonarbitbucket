package com.connecture.shopping.process.message;

import com.connecture.model.data.ShoppingLead;

public class MessageUtils
{
  public static ShoppingLead constructShoppingLead(
    String email,
    boolean callMe,
    boolean futureEmails,
    String firstName,
    String lastName,
    String phoneNumber)
  {
    ShoppingLead lead = new ShoppingLead();
    lead.setEmail(email);
    lead.setCallMe(callMe);
    lead.setFutureEmails(futureEmails);
    lead.setFirstName(firstName);
    lead.setLastName(lastName);
    lead.setLastName(lastName);
    lead.setPhoneNumber(phoneNumber);
    return lead;
  }
}
