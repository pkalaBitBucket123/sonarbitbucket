package com.connecture.shopping.process.work.core;

import org.json.JSONObject;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;

public class EnrollmentEventEffectiveDateWorkHelper extends EffectiveDateWorkHelper
{
  public static boolean isEffectiveDateProvided(JSONObject accountObject) {
    return (getRenewingEffectiveDate(accountObject) != null);
  }
  
  public static String getEffectiveDate(JSONObject accountObject, EnrollmentEventRulesEngineBean hcrEffectiveDateBean)
  {
    // Renewing Effective Date overrides HCR
    String effectiveDate = getRenewingEffectiveDate(accountObject);
    
    // No renewing effective date, so use the HCR effective date
    if (null == effectiveDate) {
      EnrollmentEventDomainModel domainModel = EnrollmentEventRuleEngineHelper.getDomainModel(hcrEffectiveDateBean);

      if (null != domainModel)
      {
        effectiveDate = domainModel.getApplication().getEffectiveDate();
      }
    }
    
    return effectiveDate;
  }
  
  private static String getRenewingEffectiveDate(JSONObject accountObject)
  {
    return accountObject.optString("renewingEffectiveDate", null);
  }
}
