package com.connecture.shopping.process.common.account;

import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 * This implementation of Account is backed by a session bean.
 * 
 * @author Shopping Development
 */
public class SessionAccountImpl implements Account
{
  private static Logger LOG = Logger.getLogger(SessionAccountImpl.class);

  private UserAccount userAccount;

  @Override
  public JSONObject getAccount(String transactionId)
  {
    if (LOG.isDebugEnabled())
    {
      LOG.debug("Getting Account Data for { transactionId : \"" + transactionId + " }");
    }

    JSONObject retVal = null;
    if (userAccount != null)
    {
      retVal = userAccount.getAccount();
    }

    if (LOG.isDebugEnabled())
    {
      LOG.debug("Retrieved AccountInfo of " + String.valueOf(retVal));
    }
    
    return retVal;
  }

  @Override
  public void updateAccountInfo(String transactionId, JSONObject account)
  {
    if (userAccount != null)
    {
      userAccount.setAccount(account);

      if (LOG.isDebugEnabled())
      {
        LOG.debug("Updating AccountInfo to " + String.valueOf(account.toString()));
      }
    }
  }
  
  @Override
  public void updateAccountTransactionId(String oldTransactionId, String newTransactionId){
    // do nothing; we don't need to update the session transactionId with another session transactionId
  }
  
  @Override
  public JSONObject getOrCreateAccount(String transactionId)
  {
    return userAccount.getAccount();
  }

  @Override
  public void clearAccount()
  {
    userAccount.setAccount(new JSONObject());
  }
  
  public void setUserAccount(UserAccount userAccount)
  {
    this.userAccount = userAccount;
  }

}
