package com.connecture.shopping.process.service.data;

import java.util.List;

public class ProductLineData
{
  private List<ProductData> products;

  public List<ProductData> getProducts()
  {
    return products;
  }

  public void setProducts(List<ProductData> products)
  {
    this.products = products;
  }
}
