package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.NameData;
import com.connecture.shopping.process.service.data.ProviderData;
import com.connecture.shopping.process.service.data.ProviderType;
import com.connecture.shopping.process.service.data.ProvidersData;

public class ProviderSearchDataConverter implements DataConverter<String, ProvidersData>
{
  private static Logger LOG = Logger.getLogger(ProviderSearchDataConverter.class);
  
  static final String PHYSICIAN_TYPE = "physician";

  @Override
  public ProvidersData convertData(String data) throws JSONException
  {
    ProvidersData retVal = new ProvidersData();

    List<ProviderData> providers = new ArrayList<ProviderData>();
    
    if (StringUtils.isNotBlank(data))
    {
      JSONArray providersJSON = new JSONArray(data);
  
      for (int i = 0; i < providersJSON.length(); i++)
      {
        JSONObject providerJSON = providersJSON.getJSONObject(i);
  
        // Logic here to determine type based on client needs
        if (providerJSON.optString("type").equalsIgnoreCase(PHYSICIAN_TYPE))
        {
          try
          {
            ProviderData provider = new ProviderData();
            
            provider.setType(ProviderType.PHYSICIAN);
            provider.setId(providerJSON.getString("xrefId"));
            
            NameData name = new NameData();
            JSONObject nameJSON = providerJSON.getJSONObject("name");
            name.setPrefix(nameJSON.optString("prefix"));
            name.setFirst(nameJSON.optString("first"));
            name.setLast(nameJSON.optString("last"));
            name.setMiddle(nameJSON.optString("middle"));
            
            provider.setName(name);
            
            // TODO: We support generic data...implement as needed
            providers.add(provider);
          }
          catch (JSONException e)
          {
            LOG.error("Invalid providerJSON format: ", e);
          }
        }
      }
    }

    retVal.setProviders(providers);

    return retVal;
  }
}
