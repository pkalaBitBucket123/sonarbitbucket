package com.connecture.shopping.process.service.stub;

import com.connecture.shopping.process.common.JSONFileUtils;

public class PlanAdvisorServiceStub
{
  public String getPlanAdvisorData(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "paconfig1_3.json");
  }
}
