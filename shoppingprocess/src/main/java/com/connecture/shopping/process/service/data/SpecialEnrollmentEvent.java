package com.connecture.shopping.process.service.data;

public class SpecialEnrollmentEvent
{
  private String eventType;
  private String displayName;
  private Integer enrollmentDays;

  public String getEventType()
  {
    return eventType;
  }

  public void setEventType(String eventType)
  {
    this.eventType = eventType;
  }

  public String getDisplayName()
  {
    return displayName;
  }

  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  public Integer getEnrollmentDays()
  {
    return enrollmentDays;
  }

  public void setEnrollmentDays(Integer enrollmentDays)
  {
    this.enrollmentDays = enrollmentDays;
  }
}
