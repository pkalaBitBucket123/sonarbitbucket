package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class Consumer
{
  private String xid;
  private String relationshipCode;
  private String dateOfBirth;
  private String gender;
  private String zip;
  private String state;
  private Boolean smoker;
  private Integer age;

  // This is required because the Rules Engine can't handle null values
  //    from the input domain.
  private String profileId = "";
  
  // NOTE: The following are initialized so when it is marshalled it becomes an empty JSONArray.
  // The rulesEngine requires this
  private List<ConsumerProfile> profile = new ArrayList<ConsumerProfile>();

  public String getXid()
  {
    return xid;
  }

  public void setXid(String xid)
  {
    this.xid = xid;
  }

  public String getRelationshipCode()
  {
    return relationshipCode;
  }

  public void setRelationshipCode(String relationshipCode)
  {
    this.relationshipCode = relationshipCode;
  }

  public String getDateOfBirth()
  {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth)
  {
    this.dateOfBirth = dateOfBirth;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public Integer getAge()
  {
    return age;
  }

  public void setAge(Integer age)
  {
    this.age = age;
  }

  public String getZip()
  {
    return zip;
  }

  public void setZip(String zip)
  {
    this.zip = zip;
  }

  public String getState()
  {
    return state;
  }

  public void setState(String state)
  {
    this.state = state;
  }

  public Boolean getSmoker()
  {
    return smoker;
  }

  public void setSmoker(Boolean smoker)
  {
    this.smoker = smoker;
  }
  
  public String getProfileId()
  {
    return profileId;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }

  public List<ConsumerProfile> getProfile()
  {
    return profile;
  }

  public void setProfile(List<ConsumerProfile> profile)
  {
    this.profile = profile;
  }
}
