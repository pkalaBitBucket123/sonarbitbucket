package com.connecture.shopping.process.work.individual;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.ShoppingRulesEngineDataRequest;
import com.connecture.shopping.process.camel.request.StateKeyForCountyAndZipCodeDataRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.individual.provider.IFPPovertyIncomeLevelDataProvider;

public class IFPPovertyIncomeLevelWork extends WorkImpl
{
  private static final Logger LOG = Logger.getLogger(IFPPovertyIncomeLevelWork.class);

  // Start Required Inputs
  private ShoppingRulesEngineDataRequest rulesEngineDataRequest;
  private StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest;
  private String rulesEngineGroupId;
  private List<String> ruleSet;
  private IFPPovertyIncomeLevelDataProvider dataProvider;
  private String transactionId;
  // End Required Inputs
  
  private ShoppingDomainModel rulesEngineResult;

  @Override
  public void get() throws Exception
  {
    Account account = getAccount();

    if (null != account)
    {
      JSONObject accountObject = account.getAccount(transactionId);

      if (accountObject.has("effectiveDate"))
      {
        Long effectiveDateMs = ShoppingDateUtils.stringToDate(accountObject.getString("effectiveDate")).getTime();
        
        dataProvider.setRuleSet(ruleSet);
        dataProvider.setRulesEngineGroupId(rulesEngineGroupId);
        dataProvider.setRequest(rulesEngineDataRequest);
        
        ShoppingDomainModel domainModel = new ShoppingDomainModel();
        dataProvider.setRulesEngineDomain(domainModel);
        
        dataProvider.setEffectiveDateMs(effectiveDateMs);
        dataProvider.setTransactionId(transactionId);
        dataProvider.setAccount(accountObject);

        dataProvider.setStateKeyForCountyAndZipCodeDataRequest(stateKeyForCountyAndZipCodeDataRequest);

        rulesEngineResult = getCachedData(ShoppingDataProviderKey.POVERTY_INCOME_LEVEL,
          dataProvider);
      }
    }
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();
    
    JSONObject povertyIncomeLevelObject = new JSONObject();
    
    ShoppingDomainModel domainModel = getResult(); // Using getResult() to make it cleaner to test

    // If there was no AccountInfo yet there won't be a RulesEngine result either
    if (null != domainModel)  
    {
      if (StringUtils.isBlank(domainModel.getRequest().getError()))
      {
        if (domainModel.getRequest().getConsumer().size() > 0) {
          Integer povertyIncomeLevel = domainModel.getRequest().getConsumer().get(0).getProfile().get(0).getValue();
          povertyIncomeLevelObject.put("povertyIncomeLevel", povertyIncomeLevel);
        }
      }
      else
      {
        LOG.error("Unable to obtain IFP Poverty Income Level from RulesEngine. Cause: "
          + domainModel.getRequest().getError());
      }
    }

    if (LOG.isDebugEnabled())
    {
      LOG.debug("Poverty Income Level Object:\n" + povertyIncomeLevelObject.toString(4));
    }

    jsonObject.put("povertyIncomeLevel", povertyIncomeLevelObject);

    return jsonObject;
  }

  public ShoppingDomainModel getResult()
  {
    return rulesEngineResult;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setRulesEngineDataRequest(ShoppingRulesEngineDataRequest rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }
  
  public void setStateKeyForCountyAndZipCodeDataRequest(
    StateKeyForCountyAndZipCodeDataRequest stateKeyForCountyAndZipCodeDataRequest)
  {
    this.stateKeyForCountyAndZipCodeDataRequest = stateKeyForCountyAndZipCodeDataRequest;
  }

  public void setRuleSet(List<String> ruleSet)
  {
    this.ruleSet = ruleSet;
  }

  public void setDataProvider(IFPPovertyIncomeLevelDataProvider dataProvider)
  {
    this.dataProvider = dataProvider;
  }

  public void setRulesEngineGroupId(String rulesEngineGroupId)
  {
    this.rulesEngineGroupId = rulesEngineGroupId;
  }
}
