package com.connecture.shopping.process.work.employee;

import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.work.Work;

public interface ProductPlanDataWork extends Work
{
  ProductPlanData getProductPlanData();
  
  void setTransactionId(String transactionId);
}
