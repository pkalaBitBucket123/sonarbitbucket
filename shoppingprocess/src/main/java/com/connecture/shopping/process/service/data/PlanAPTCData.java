package com.connecture.shopping.process.service.data;

public class PlanAPTCData
{
  private Double appliedAPTC;
  private Double netPremium;
  private Double basePremium;
  
  public Double getAppliedAPTC()
  {
    return appliedAPTC;
  }
  public void setAppliedAPTC(Double appliedAPTC)
  {
    this.appliedAPTC = appliedAPTC;
  }
  public Double getNetPremium()
  {
    return netPremium;
  }
  public void setNetPremium(Double netPremium)
  {
    this.netPremium = netPremium;
  }
  public Double getBasePremium()
  {
    return basePremium;
  }
  public void setBasePremium(Double basePremium)
  {
    this.basePremium = basePremium;
  } 
}
