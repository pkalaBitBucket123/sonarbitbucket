package com.connecture.shopping.process.integration;

import java.util.HashMap;
import java.util.Map;

import com.connecture.model.integration.data.ShoppingToIAData;

public class ShoppingToIADataExtn extends ShoppingToIAData {
	private Map<String, Object> values = new HashMap<String, Object>(); 
	public void addAttribute(String key,Object value){
		values.put(key, value);
	}
	public Map<String, Object> getValues(){
		return values;
	}
}
