package com.connecture.shopping.process.work.context;

import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.Work;
import com.connecture.shopping.process.work.WorkConstants;

public interface ContextWorkFactory
{
  public Work getWork(
    String transactionId,
    RequestType type,
    String requestKey,
    WorkConstants.Method method,
    JSONArray params,
    JSONObject data,
    DataCache dataCache) throws Exception;

  /**
   * this method returns an AccountWork to be worked upon by other works.
   */
  public Work getAccountWork(String transactionId) throws Exception;
}
