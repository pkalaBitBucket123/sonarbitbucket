package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

import com.connecture.shopping.process.service.data.availability.Availability;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsForm;

public class Request
{
  private String id;
  private Long timeInMs;
  private Long timeOutMs;
  private String effectiveDate;
  private String error;
  
  // NOTE: The following are initialized so when it is marshalled it becomes an empty JSONArray.
  // The rulesEngine requires this
  private List<Consumer> consumer = new ArrayList<Consumer>();
  private List<Product> product = new ArrayList<Product>();
  private List<Profile> profile = new ArrayList<Profile>();
  private List<Benefit> benefit = new ArrayList<Benefit>();
  private List<Property> property = new ArrayList<Property>();
  
  private EnrollmentEvent enrollmentEvent = new EnrollmentEvent();

  private DemographicsForm demographicsForm = new DemographicsForm();
  
  private Availability availability = new Availability();
  
  public String getId()
  {
    return id;
  }

  public void setId(String id)
  {
    this.id = id;
  }
  
  public String getEffectiveDate()
  {
    return effectiveDate;
  }

  public void setEffectiveDate(String effectiveDate)
  {
    this.effectiveDate = effectiveDate;
  }

  public Long getTimeInMs()
  {
    return timeInMs;
  }

  public void setTimeInMs(Long timeInMs)
  {
    this.timeInMs = timeInMs;
  }

  public Long getTimeOutMs()
  {
    return timeOutMs;
  }

  public void setTimeOutMs(Long timeOutMs)
  {
    this.timeOutMs = timeOutMs;
  }

  public String getError()
  {
    return error;
  }

  public void setError(String error)
  {
    this.error = error;
  }

  public List<Consumer> getConsumer()
  {
    return consumer;
  }

  public void setConsumer(List<Consumer> consumer)
  {
    this.consumer = consumer;
  }

  public List<Product> getProduct()
  {
    return product;
  }

  public void setProduct(List<Product> product)
  {
    this.product = product;
  }

  public List<Profile> getProfile()
  {
    return profile;
  }

  public void setProfile(List<Profile> profile)
  {
    this.profile = profile;
  }

  public List<Benefit> getBenefit()
  {
    return benefit;
  }

  public void setBenefit(List<Benefit> benefit)
  {
    this.benefit = benefit;
  }

  public List<Property> getProperty()
  {
    return property;
  }

  public void setProperty(List<Property> property)
  {
    this.property = property;
  }
  
  public EnrollmentEvent getEnrollmentEvent()
  {
    return enrollmentEvent;
  }

  public void setEnrollmentEvent(EnrollmentEvent enrollmentEvent)
  {
    this.enrollmentEvent = enrollmentEvent;
  }

  public DemographicsForm getDemographicsForm()
  {
    return demographicsForm;
  }

  public void setDemographicsForm(DemographicsForm demographicsForm)
  {
    this.demographicsForm = demographicsForm;
  }

  public Availability getAvailability()
  {
    return availability;
  }

  public void setAvailability(Availability availability)
  {
    this.availability = availability;
  }
}