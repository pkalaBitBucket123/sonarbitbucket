package com.connecture.shopping.process.work.individual;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.availability.Availability;
import com.connecture.shopping.process.service.data.availability.AvailabilityProduct;
import com.connecture.shopping.process.service.data.availability.converter.AvailabilityProductConverterUtils;
import com.connecture.shopping.process.service.data.converter.ProductDataConverterUtil;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventEffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;
import com.connecture.shopping.process.work.core.ShoppingRulesEngineBean;

public class IFPProductAvailabilityWork extends WorkImpl
{
  private EnrollmentEventRulesEngineBean hcrEffectiveDateBean;
  private ShoppingRulesEngineBean availabilityRulesEngineBean;

  // INPUT
  private JSONObject inputJSON;
  private String transactionId;

  // OUTPUT
  private ShoppingDomainModel shoppingDomain;

  // TODO | get some kind of class for account values; get rid of this
  private static final String AVAILABILITY = "availability";
  private static final String PLANS = "plans";
  private static final String CART = "cart";

  @Override
  public void update() throws Exception
  {
    // TODO | so this is how we handle it elsewhere
    // running a save on the client-side to POST because we are sending in a TON
    // of data.
    get();
  }

  @Override
  public void get() throws Exception
  {
    JSONObject availabilityInputJSON = inputJSON.getJSONObject(AVAILABILITY);

    // get the account. we need it for all sorts of things.
    JSONObject accountJSON = this.getAccountJSON();

    // find the effective date from the account.
    // needed for the rule determination, but also
    // for calculating member ages.
    Date effectiveDate = EffectiveDateWorkHelper.getEffectiveDate(getEffectiveDate(accountJSON));

    // build plan data from input json. this should be a collection
    // containing ALL of the available plans.
    List<AvailabilityProduct> products = AvailabilityProductConverterUtils
      .fromPlanData(ProductDataConverterUtil.convertFromShoppingPlansJSON(availabilityInputJSON
        .getJSONArray(PLANS)));
    
    Availability availability = buildAvailability(products,
      getSelectedProductInternalCodes(accountJSON.optJSONObject(CART)));
    
    availabilityRulesEngineBean.setCoverageEffectiveDate(effectiveDate);
    
    // call rules engine for availability results
    availabilityRulesEngineBean.setAvailability(availability);
    availabilityRulesEngineBean.setDataCache(getDataCache());
    shoppingDomain = availabilityRulesEngineBean.executeRule();
  }
  
  @Override
  public JSONObject processData() throws Exception
  {
    // return object
    JSONObject jsonObject = new JSONObject();
    JSONObject availabilityJSON = new JSONObject();
    Availability availability = shoppingDomain.getRequest().getAvailability();

    // pull data from the subsidy usage domain
    // put the full list back together and process into JSON
    List<AvailabilityProduct> availabilityProducts = new ArrayList<AvailabilityProduct>();
    availabilityProducts.addAll(availability.getSelectedProducts());
    availabilityProducts.addAll(availability.getUnselectedProducts());

    // create a Map of product internal code, rates
    availabilityJSON.put("plans", createPlanPropertyMappingJSON(availabilityProducts));

    jsonObject.put("availability", availabilityJSON);
    return jsonObject;
  }

  private JSONObject createPlanPropertyMappingJSON(List<AvailabilityProduct> availabilityProducts)
    throws JSONException
  {
    JSONObject productPropertyMapping = new JSONObject();

    for (AvailabilityProduct availabilityProduct : availabilityProducts)
    {
      String planKey = availabilityProduct.getInternalCode();
      productPropertyMapping.put(planKey, createProductPropertiesJSON(availabilityProduct));
    }

    return productPropertyMapping;
  }

  private JSONObject createProductPropertiesJSON(AvailabilityProduct availabilityProduct) throws JSONException
  {
    JSONObject planProperties = new JSONObject();
    planProperties.put("availabilityCode", availabilityProduct.getAvailabilityCode().toLowerCase());
    if (!StringUtils.isBlank(availabilityProduct.getAvailabilityReason()))
    {
      planProperties.put("availabilityReason", availabilityProduct.getAvailabilityReason());
    }
    return planProperties;
  }

  protected JSONObject getAccountJSON()
  {
    Account account = getAccount();
    JSONObject accountJSON = account.getAccount(transactionId);
    return accountJSON;
  }

  protected String getEffectiveDate(JSONObject accountJSON) throws Exception
  {
    hcrEffectiveDateBean.setDataCache(getDataCache());

    return EnrollmentEventEffectiveDateWorkHelper.getEffectiveDate(accountJSON,
      hcrEffectiveDateBean);
  }
  
  private Set<String> getSelectedProductInternalCodes(JSONObject cartJSON) throws JSONException
  {
    Set<String> selectedProductIds = new HashSet<String>();

    if (cartJSON != null)
    {
      JSONObject productLinesJSON = cartJSON.optJSONObject("productLines");
      if (productLinesJSON != null && JSONObject.getNames(productLinesJSON) != null)
      {
        // an object for each product line
        for (String productLineCode : JSONObject.getNames(productLinesJSON))
        {
          JSONObject productLineJSON = productLinesJSON.getJSONObject(productLineCode);
          JSONArray productsJSON = productLineJSON.optJSONArray("products");
          if (productsJSON != null)
          {
            for (int i = 0; i < productsJSON.length(); i++)
            {
              JSONObject productJSON = productsJSON.getJSONObject(i);
              String productInternalCode = productJSON.getString("id");
              selectedProductIds.add(productInternalCode);
            }
          }
        }
      }
    }

    return selectedProductIds;
  }

  private static Availability buildAvailability(
    List<AvailabilityProduct> availabilityProducts,
    Set<String> cartProductKeys)
  {
    List<AvailabilityProduct> selectedProducts = new ArrayList<AvailabilityProduct>();
    List<AvailabilityProduct> unselectedProducts = new ArrayList<AvailabilityProduct>();

    // separate into plans IN-CART, plans OUT-OF-CART
    for (AvailabilityProduct availabilityProduct : availabilityProducts)
    {
      if (cartProductKeys.contains(availabilityProduct.getInternalCode()))
      {
        selectedProducts.add(availabilityProduct);
      }
      else
      {
        unselectedProducts.add(availabilityProduct);
      }
    }

    Availability availability = new Availability();
 
    availability.setSelectedProducts(selectedProducts);
    availability.setUnselectedProducts(unselectedProducts);
    //availability.setProductLines(productLines);
    
    return availability;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setInputJSON(JSONObject inputJSON)
  {
    this.inputJSON = inputJSON;
  }

  public void setAvailabilityRulesEngineBean(ShoppingRulesEngineBean availabilityRulesEngineBean)
  {
    this.availabilityRulesEngineBean = availabilityRulesEngineBean;
  }

  public void setHcrEffectiveDateBean(EnrollmentEventRulesEngineBean hcrEffectiveDateBean)
  {
    this.hcrEffectiveDateBean = hcrEffectiveDateBean;
  }
}