package com.connecture.shopping.process.common.account.conversion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.config.AVPConfig;
import com.connecture.shopping.process.common.account.conversion.config.AVPConfigChain;
import com.connecture.shopping.process.common.account.conversion.config.AVPConfigUtils;
import com.connecture.shopping.process.common.account.conversion.converter.AVPConverter;
import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;
import com.connecture.shopping.process.domain.AccountInfo;

public class AccountVersioningProcess
{
  private static Logger LOG = Logger.getLogger(AccountVersioningProcess.class);

  private AccountVersioningConverterDataProvider avpConverterDataProvider;
  private AccountInfoDataProvider accountInfoDataProvider;
  private AccountInfoDataStore accountInfoDataStore;

  private String convertToVersion;

  private AVPConfigChain configChain = new AVPConfigChain();
  private List<AccountInfo> accountsToConvert = new ArrayList<AccountInfo>();
  private List<AccountInfo> convertedAccounts = new ArrayList<AccountInfo>();
  private List<AccountInfo> erroredAccounts = new ArrayList<AccountInfo>();
  private List<String> errors = new ArrayList<String>();

  public void versionAccounts() throws Exception
  {
    LOG.debug("*** Account Versioning starting ***");

    StopWatch watch = new StopWatch();
    watch.start();

    loadConverterConfigurationChain();

    if (errors.isEmpty())
    {
      loadAccountsToConvert();
    }
    else
    {
      LOG.warn("Skipping loading of accounts to be converted");
    }

    if (errors.isEmpty())
    {
      convertAccounts();
      storeConvertedAccounts();
    }
    else
    {
      LOG.warn("Skipping conversion of accounts");
    }

    if (!errors.isEmpty())
    {
      LOG.warn("Errors detected during conversion process\n" + errors);
    }

    LOG.debug("*** Account Versioning completed in " + watch.getTime() + " ms ***");
  }

  void loadConverterConfigurationChain()
  {
    StopWatch watch = new StopWatch();
    watch.start();
    LOG.debug("Starting to load configuration chain");
    try
    {
      String json = avpConverterDataProvider.fetchData();

      JSONObject jsonObject = new JSONObject(json);

      List<String> errors = new ArrayList<String>();
      JSONValidationUtils.checkForMissingField(errors, jsonObject, "configurations");

      if (errors.isEmpty())
      {
        JSONArray versionConfigs = jsonObject.getJSONArray("configurations");
        for (int i = 0; i < versionConfigs.length(); i++)
        {
          JSONObject versionConfig = versionConfigs.getJSONObject(i);

          errors = AVPConfigUtils.validateJSON(versionConfig);

          if (errors.isEmpty())
          {
            AVPConfig config = AVPConfigUtils.valueOf(versionConfig);
            configChain.add(config);
          }
          else
          {
            LOG.error("Errors detected in configurations[" + i + "]\n" + errors);
          }
        }
      }
      else
      {
        LOG.error("Errors detected in configuration file \""
          + avpConverterDataProvider.getDataFile() + "\"");
      }

      LOG.debug("Loaded configuration chain in " + watch.getTime() + " ms ");
    }
    catch (Exception e)
    {
      String errorText = "Failed to load field converters for version " + convertToVersion;
      LOG.error(errorText + " in " + watch.getTime() + " ms", e);
      errors.add(errorText);
    }
  }

  void loadAccountsToConvert()
  {
    StopWatch watch = new StopWatch();
    watch.start();
    LOG.debug("Starting to load accounts");
    try
    {
      accountInfoDataProvider.setVersion(convertToVersion);
      accountsToConvert = accountInfoDataProvider.fetchData();
      LOG.debug("Loaded accounts in " + watch.getTime() + " ms ");
    }
    catch (Exception e)
    {
      String errorText = "Failed to load accounts to be converted";
      LOG.error(errorText + " in " + watch.getTime() + " ms", e);
      errors.add(errorText);
    }
  }

  void convertAccounts()
  {
    StopWatch watch = new StopWatch();
    watch.start();
    LOG.debug("Starting converting accounts");
    try
    {
      for (AccountInfo account : accountsToConvert)
      {
        // TODO cache these by account.version (bulk performance improvement)
        List<AVPConfig> configs = getConfigurationsForAccount(account);

        boolean accountInError = false;

        for (Iterator<AVPConfig> configIterator = configs.iterator(); !accountInError
          && configIterator.hasNext();)
        {
          AVPConfig config = configIterator.next();

          // Apply Field Conversions until there is an error
          for (Iterator<AVPConverter> iter = config.getConverters().iterator(); !accountInError
            && iter.hasNext();)
          {
            AVPConverter converter = iter.next();
            try
            {
              converter.apply(account.getValue());
            }
            catch (JSONException e)
            {
              accountInError = true;
              errors.add("Conversion ERROR [" + config.getPreviousVersion() + "-"
                + config.getNewVersion() + "] of accountId:" + account.getAccountId() + " during "
                + converter.getClass().getSimpleName() + ", reason: " + e.getMessage());
              erroredAccounts.add(account);
            }
          }
        }

        if (configs.size() > 0 && !accountInError)
        {
          // update the version of the account because it has been converted
          // successfully
          account.setVersion(convertToVersion);
          convertedAccounts.add(account);
        }
      }
      LOG.debug("Converted accounts in " + watch.getTime() + " ms ");
    }
    catch (Exception e)
    {
      String errorText = "Failed to convert accounts to version " + convertToVersion;
      LOG.error(errorText + " in " + watch.getTime() + " ms", e);
      errors.add(errorText);
    }
  }

  private List<AVPConfig> getConfigurationsForAccount(AccountInfo account)
  {
    List<AVPConfig> configs = null;

    if (StringUtils.isNotBlank(account.getVersion()))
    {
      configs = configChain.getConfigurationsFor(account.getVersion(), convertToVersion);
    }
    else
    {
      configs = configChain.getConfigurationsFor(convertToVersion);
    }

    return configs;
  }

  void storeConvertedAccounts()
  {
    StopWatch watch = new StopWatch();
    watch.start();
    LOG.debug("Starting storing accounts");
    try
    {
      accountInfoDataStore.storeData(convertedAccounts);
      LOG.debug("Stored accounts in " + watch.getTime() + " ms ");
    }
    catch (Exception e)
    {
      String errorText = "Failed to store accounts after conversion";
      LOG.error(errorText + " in " + watch.getTime() + " ms", e);
      errors.add(errorText);
    }
  }

  public String getConvertToVersion()
  {
    return convertToVersion;
  }

  public void setConvertToVersion(String convertToVersion)
  {
    this.convertToVersion = convertToVersion;
  }

  public List<String> getErrors()
  {
    return errors;
  }

  public AVPConfigChain getAVPConfigChain()
  {
    return configChain;
  }

  public List<AccountInfo> getAccountsToConvert()
  {
    return accountsToConvert;
  }

  public List<AccountInfo> getConvertedAccounts()
  {
    return convertedAccounts;
  }

  public List<AccountInfo> getErroredAccounts()
  {
    return erroredAccounts;
  }

  public void setAVPConverterDataProvider(
    AccountVersioningConverterDataProvider avpConverterDataProvider)
  {
    this.avpConverterDataProvider = avpConverterDataProvider;
  }

  public void setAccountInfoDataProvider(AccountInfoDataProvider accountInfoDataProvider)
  {
    this.accountInfoDataProvider = accountInfoDataProvider;
  }

  public void setAccountInfoDataStore(AccountInfoDataStore accountInfoDataStore)
  {
    this.accountInfoDataStore = accountInfoDataStore;
  }
}
