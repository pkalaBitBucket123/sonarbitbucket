package com.connecture.shopping.process.common;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Locale;

import org.apache.commons.lang.LocaleUtils;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class LocaleUserType implements UserType
{
  private static final int[] SQL_TYPES = { Types.VARCHAR };

  @Override
  public Object assemble(Serializable cached, Object owner) throws HibernateException
  {
    return LocaleUtils.toLocale((String)cached);
  }

  @Override
  public Object deepCopy(Object value) throws HibernateException
  {
    String langTag = ((Locale)value).toString();
    return LocaleUtils.toLocale(langTag);
  }

  @Override
  public Serializable disassemble(Object value) throws HibernateException
  {
    return ((Locale)value).toString();
  }

  @Override
  public boolean equals(Object lhs, Object rhs) throws HibernateException
  {
    if(lhs == null)
    {
      return rhs != null;
    }
    return lhs.equals(rhs);
  }

  @Override
  public int hashCode(Object value) throws HibernateException
  {
    return ((Locale)value).hashCode();
  }

  @Override
  public boolean isMutable()
  {
    return true;
  }

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws HibernateException, SQLException
  {
    Object ret = null;
    if (!rs.wasNull())
    {
      String localStr = rs.getString(names[0]);
      ret = LocaleUtils.toLocale(localStr);
    }
    return ret;
  }

  @Override
  public void nullSafeSet(PreparedStatement st, Object value, int index) throws HibernateException, SQLException
  {
    if (value == null)
    {
      st.setNull(index, SQL_TYPES[0]);
    }
    else
    {
      st.setString(index, ((Locale)value).toString());
    }
  }

  @Override
  public Object replace(Object original, Object target, Object owner) throws HibernateException
  {
    return deepCopy(original);
  }

  @Override
  public Class<?> returnedClass()
  {
    return Locale.class;
  }

  @Override
  public int[] sqlTypes()
  {
    return SQL_TYPES;
  }

}
