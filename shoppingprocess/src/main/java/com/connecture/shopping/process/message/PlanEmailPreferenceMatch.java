package com.connecture.shopping.process.message;

public class PlanEmailPreferenceMatch
{
  private boolean match;
  private String description;
  /**
   * @return the isMatch
   */
  public boolean isMatch()
  {
    return match;
  }
  /**
   * @param isMatch the isMatch to set
   */
  public void setMatch(boolean match)
  {
    this.match = match;
  }
  /**
   * @return the description
   */
  public String getDescription()
  {
    return description;
  }
  /**
   * @param description the description to set
   */
  public void setDescription(String description)
  {
    this.description = description;
  }
}