package com.connecture.shopping.process.work.employee.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.employee.ConsumerEligibilityWork;

public abstract class ConsumerEligibilityWorkProvider implements WorkProviderImpl<ConsumerEligibilityWork>
{
  @Override
  public ConsumerEligibilityWork getWork(
    String transactionId,
    String requestKey,
    WorkConstants.Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    ConsumerEligibilityWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}
