/**
 * 
 */
package com.connecture.shopping.process.work;

import java.util.Map;

import org.json.JSONObject;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.cache.DataCache;

/**
 * 
 */
public interface Work
{
  public void get() throws Exception;

  public void create() throws Exception;

  public void update() throws Exception;

  public void delete() throws Exception;

  public void run() throws Exception;

  public JSONObject processData() throws Exception;

  public JSONObject processErrors();

  public void setKey(String key);

  public String getKey();

  public Map<String, String> getErrors();

  public void setMethod(WorkConstants.Method method);

  public boolean isAsynchronous();

  public void setDataCache(DataCache dataCache);
  
  public Map<String, String> getConfigCriteria();
  public void setConfigCriteria(Map<String, String> configCriteria);
  
  /**
   * 
   * @param account
   */
  public void setAccount(Account account);
}
