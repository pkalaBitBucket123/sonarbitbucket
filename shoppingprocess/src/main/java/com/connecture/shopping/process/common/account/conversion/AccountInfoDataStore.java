package com.connecture.shopping.process.common.account.conversion;

import java.util.List;

import com.connecture.shopping.process.domain.AccountInfo;

public interface AccountInfoDataStore
{
  void storeData(List<AccountInfo> data) throws Exception;
}
