package com.connecture.shopping.process.service.data;

import java.util.Comparator;

public class DemographicsConfigFieldOptionData
{
  private String label;
  private String value;
  private String dataGroupType;
  private Boolean defaultSelection;
  private Integer order;

  public static Comparator<DemographicsConfigFieldOptionData> OptionOrderComparator = new Comparator<DemographicsConfigFieldOptionData>()
    {

      public int compare(DemographicsConfigFieldOptionData option1, DemographicsConfigFieldOptionData option2)
      {
        // ascending order
        return Integer.valueOf(option1.getOrder()).compareTo(Integer.valueOf(option2.getOrder()));

      }

    };
  
  public String getLabel()
  {
    return label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(String value)
  {
    this.value = value;
  }

  public String getDataGroupType()
  {
    return dataGroupType;
  }

  public void setDataGroupType(String dataGroupType)
  {
    this.dataGroupType = dataGroupType;
  }

  public Boolean getDefaultSelection()
  {
    return defaultSelection;
  }

  public void setDefaultSelection(Boolean defaultSelection)
  {
    this.defaultSelection = defaultSelection;
  }

  public Integer getOrder()
  {
    return order;
  }

  public void setOrder(Integer order)
  {
    this.order = order;
  }
}