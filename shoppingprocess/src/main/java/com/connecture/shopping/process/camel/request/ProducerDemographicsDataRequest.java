package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.data.ShoppingProducer;

public class ProducerDemographicsDataRequest extends BaseCamelRequest<ShoppingProducer>
{
  public static final String PRODUCER_TOKEN = "producerToken";

  @Produce(uri = "{{camel.shopping.producer.producerDemographicsData}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setProducerToken(String producerToken)
  {
    headers.put(PRODUCER_TOKEN, producerToken);
  }

}
