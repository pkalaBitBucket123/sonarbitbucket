package com.connecture.shopping.process.common.account.conversion.json;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONPathAddAction implements JSONPathAction
{
  private Object defaultValue;

  public JSONPathAddAction() {
  }
  
  public JSONPathAddAction(Object defaultValue) {
    this.defaultValue = defaultValue;
  }
  
  @Override
  public boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException
  {
    boolean success = false;
    if (isValidFor(state))
    {
      Object newValue = defaultValue;
      if (PathState.MISSING_SUBPATH.equals(state) || null == defaultValue) {
        newValue = new JSONObject();
      }
      jsonObject.put(key, newValue);
      success = true;
    }
    return success;
  }

  @Override
  public boolean isValidFor(PathState state)
  {
    return JSONPathAction.PathState.MISSING.equals(state) || JSONPathAction.PathState.MISSING_SUBPATH.equals(state);
  }
}
