package com.connecture.shopping.process.camel.logging;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.concurrent.Future;

import javax.xml.transform.stream.StreamSource;

import org.apache.camel.BytesSource;
import org.apache.camel.Message;
import org.apache.camel.StreamCache;
import org.apache.camel.StringSource;
import org.apache.camel.WrappedFile;
import org.apache.camel.util.MessageHelper;
import org.codehaus.jackson.map.ObjectMapper;

public class LogFormatter extends org.apache.camel.component.log.LogFormatter
{
  private static final ObjectMapper mapper = new ObjectMapper();
  
  @Override
  protected String getBodyAsString(Message message)
  {
    Object obj = message.getBody();
    if (obj instanceof Future)
    {
      if (!isShowFuture())
      {
        // just use a to string of the future object
        return obj.toString();
      }
    }
    //if the body isn't null or any of the types handled by the message helper...
    if (obj != null
    && !(obj instanceof String
      || obj instanceof StreamSource
      || obj instanceof StringSource
      || obj instanceof BytesSource
      || obj instanceof StreamCache
      || obj instanceof InputStream
      || obj instanceof OutputStream
      || obj instanceof Reader
      || obj instanceof Writer
      || obj instanceof WrappedFile
      || obj instanceof File))
    {
      String ret = obj.toString();
      try
      {
        ret = mapper.writeValueAsString(obj);
      }
      catch(Exception e)
      {
        //not necessary, just eat it and return the toString vlaue
      }
      return ret;
    }

    return MessageHelper.extractBodyForLogging(message, "", isShowStreams(), isShowFiles(), -1);
  }
}
