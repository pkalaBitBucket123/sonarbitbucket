package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class Product
{
  private String xid;
  private String tierCode;
  private Double cost;
  private Double maxOOPCost;
  private Double deductible;
  
  // NOTE: The following are initialized so when it is marshalled it becomes an empty JSONArray.
  // The rulesEngine requires this
  private List<Attribute> attribute = new ArrayList<Attribute>();
  
  private List<ProductConsumer> consumer = new ArrayList<ProductConsumer>();

  public String getXid()
  {
    return xid;
  }

  public void setXid(String xid)
  {
    this.xid = xid;
  }

  public String getTierCode()
  {
    return tierCode;
  }

  public void setTierCode(String tierCode)
  {
    this.tierCode = tierCode;
  }
  
  public Double getCost()
  {
    return cost;
  }

  public void setCost(Double cost)
  {
    this.cost = cost;
  }

  public Double getMaxOOPCost()
  {
    return maxOOPCost;
  }

  public void setMaxOOPCost(Double maxOOPCost)
  {
    this.maxOOPCost = maxOOPCost;
  }

  public Double getDeductible()
  {
    return deductible;
  }

  public void setDeductible(Double deductible)
  {
    this.deductible = deductible;
  }

  public List<Attribute> getAttribute()
  {
    return attribute;
  }

  public void setAttribute(List<Attribute> attribute)
  {
    this.attribute = attribute;
  }

  public List<ProductConsumer> getConsumer()
  {
    return consumer;
  }

  public void setConsumer(List<ProductConsumer> consumer)
  {
    this.consumer = consumer;
  }
}
