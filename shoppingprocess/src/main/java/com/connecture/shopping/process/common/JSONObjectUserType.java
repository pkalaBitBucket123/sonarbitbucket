package com.connecture.shopping.process.common;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Hibernate user type to persist JSONObject
 * 
 * @see http://www.json.org/javadoc/org/json/JSONObject.html
 * @see http ://docs.jboss.org/hibernate/stable/annotations/api/org/hibernate/usertype /UserType.html
 * @author "Jan Jonas <mail@janjonas.net>"
 */
public class JSONObjectUserType implements UserType
{
  private static Logger LOG = Logger.getLogger(JSONObjectUserType.class);

  private static final int[] SQL_TYPES = {Types.LONGVARCHAR};

  @Override
  public Object assemble(Serializable cached, Object owner) throws HibernateException
  {
    if (cached == null)
    {
      return cached;
    }
    
    try
    {
      return new JSONObject((String)cached);
    }
    catch (JSONException e)
    {
      throw new HibernateException("Failed to assemble cached " + cached.getClass().getSimpleName(), e);
    }
  }

  @Override
  public Object deepCopy(Object value) throws HibernateException
  {
    if (value == null) 
    {
      return value;
    }
    
    try
    {
      return new JSONObject(((JSONObject) value).toString());
    }
    catch (JSONException e)
    {
      throw new HibernateException("Failed to deepcopy value " + value.getClass().getSimpleName(), e);
    }
  }

  @Override
  public Serializable disassemble(Object value) throws HibernateException
  {
    return ((JSONObject) value).toString();
  }

  @Override
  public boolean equals(Object x, Object y) throws HibernateException
  {
    return ObjectUtils.equals(x, y) || 
      x != null && y != null && x.toString().equals(y.toString()); 
  }

  @Override
  public int hashCode(Object x) throws HibernateException
  {
    return ((JSONObject) x).toString().hashCode();
  }

  @Override
  public boolean isMutable()
  {
    return true;
  }

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
    throws HibernateException, SQLException
  {
    if (!rs.wasNull())
    {
      if (rs.getString(names[0]) == null)
      {
        return new JSONObject();
      }
      else
      {
        XMLSerializer serializer = new XMLSerializer();
        String xml = rs.getString(names[0]);
        if (LOG.isDebugEnabled())
        {
          LOG.debug("XML from database:\n" + xml);
        }
        JSON json = (JSON) serializer.read(xml);
        String data = json.toString();
        try
        {
          return new JSONObject(data);
        }
        catch (JSONException e)
        {
          throw new HibernateException("Failed parsing JSON from converted xml: " + data, e);
        }
      }
    }
    return null;
  }

  @Override
  public void nullSafeSet(PreparedStatement st, Object value, int index)
    throws HibernateException, SQLException
  {
    if (value == null)
    {
      st.setNull(index, SQL_TYPES[0]);
    }
    else
    {
      XMLSerializer serializer = new XMLSerializer();
      JSON json = JSONSerializer.toJSON(((JSONObject) value).toString());

      String xml = serializer.write(json, "UTF-16");
      if (LOG.isDebugEnabled())
      {
        LOG.debug("XML to database:\n" + xml);
      }
      st.setString(index, xml);
    }
  }

  @Override
  public Object replace(Object original, Object target, Object owner) throws HibernateException
  {
    return deepCopy(original);
  }

  @Override
  public Class<?> returnedClass()
  {
    return JSONObject.class;
  }

  @Override
  public int[] sqlTypes()
  {
    return SQL_TYPES;
  }

}
