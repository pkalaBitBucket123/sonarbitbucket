/**
 * 
 */
package com.connecture.shopping.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.domain.AccountInfo;

/**
 * 
 */
public class ShoppingIntegrationProcess
{
  private static Logger LOG = Logger.getLogger(ShoppingIntegrationProcess.class);

  private SessionFactory sessionFactory;

  /**
   * Gets the ID for the current active (non-expired) account
   * 
   * @param xrefId
   * @return
   * @throws JSONException
   */
  public Long getAccountId(String transactionId)
  {
    Session session = sessionFactory.getCurrentSession();

    Long accountId = null;

    Criteria crit = session.createCriteria(AccountInfo.class).add(
      Restrictions.eq("transactionId", transactionId));

    AccountInfo accountInfo = (AccountInfo) crit.uniqueResult();

    if (accountInfo != null)
    {
      accountId = accountInfo.getAccountId();
    }

    return accountId;
  }

  public Map<String, JSONObject> buildMemberMap(JSONObject accountJSON) throws JSONException
  {
    Map<String, JSONObject> resultMap = new HashMap<String, JSONObject>();

    // start with a collection of members
    JSONArray membersJSONArray = accountJSON.getJSONArray("members");
    for (int i = 0; i < membersJSONArray.length(); i++)
    {
      JSONObject memberJSON = membersJSONArray.getJSONObject(i);

      // build out demographics for each member

      JSONObject resultMemberJSON = new JSONObject();

      String memberRefName = memberJSON.getString("memberRefName");

      // individualId
      String memberId = memberJSON.optString("memberExtRefId");
      // relationshipType
      String relationshipType = memberJSON.getString("memberRelationship");
      JSONObject name = memberJSON.getJSONObject("name");
      // first name
      String firstName = name.getString("first");

      // date string
      String dob = memberJSON.optString("birthDate");

      // gender
      String gender = memberJSON.optString("gender");
      // smoker
      String smoker = memberJSON.getString("isSmoker");
      boolean isSmoker = "Yes".equalsIgnoreCase(smoker) ? true : false;

      if (!StringUtils.isBlank(memberId))
      {
        resultMemberJSON.put("individualId", memberId);
      }
      else
      {
        resultMemberJSON.put("memberRefName", memberId);
      }

      resultMemberJSON.put("isNew", memberJSON.getBoolean("isNew"));

      resultMemberJSON.put("relationship", relationshipType);
      resultMemberJSON.put("firstName", firstName);
      resultMemberJSON.put("dob", dob);
      resultMemberJSON.put("gender", gender);
      resultMemberJSON.put("smoker", isSmoker);

      resultMap.put(memberRefName, resultMemberJSON);
    }

    return resultMap;
  }

  // public Map<String, String> getMemberAmounts(JSONObject memberAmountJSON)
  // throws JSONException
  // {
  // Map<String, String> memberAmounts = new HashMap<String, String>();
  //
  // JSONArray memberIds = memberAmountJSON.names();
  // if (memberIds != null)
  // {
  // for (int i = 0; i < memberIds.length(); i++)
  // {
  // String memberId = memberIds.getString(i);
  // String amount = memberAmountJSON.getString(memberId);
  // memberAmounts.put(memberId, amount);
  // }
  // }
  // else
  // {
  // LOG.error("No members found when trying to determine member amounts.");
  // }
  //
  // return memberAmounts;
  // }

  // public Map<MemberTypeEnum, Double> getMemberTypeAmounts(JSONObject
  // memberTypeAmountJSON)
  // throws JSONException
  // {
  // Map<MemberTypeEnum, Double> memberTypeAmounts = new HashMap<MemberTypeEnum,
  // Double>();
  //
  // JSONArray memberTypes = memberTypeAmountJSON.names();
  // if (memberTypes != null)
  // {
  // for (int i = 0; i < memberTypes.length(); i++)
  // {
  // String memberType = memberTypes.getString(i);
  // Double amount = memberTypeAmountJSON.getDouble(memberType);
  // MemberTypeEnum memberTypeEnum = MemberTypeEnum.valueOfString(memberType);
  // memberTypeAmounts.put(memberTypeEnum, amount);
  // }
  // }
  // else
  // {
  // LOG.error("No member types found when trying to determine member type amounts.");
  // }
  //
  // return memberTypeAmounts;
  // }

  public JSONObject buildMemberLevelRatedData(JSONObject memberLevelJSON) throws JSONException
  {
    JSONObject memberLevelRateDataJSON = new JSONObject();

    // get whatever data is available
    JSONArray memberRatesJSON = memberLevelJSON.getJSONArray("memberRates");

    // TODO | ultimately we will use this (just here to see if it blows up)
    // Map<MemberTypeEnum, Double> memberTypeAmounts =
    // getMemberTypeAmounts(memberRatesJSON);
    // TODO | but for now, we just pass it through
    memberLevelRateDataJSON.put("memberRates", memberRatesJSON);

    // get whatever data is available
    JSONObject memberTypeContributionJSON = memberLevelJSON
      .getJSONObject("memberTypeContributions");

    // TODO | ultimately we will use this (just here to see if it blows up)
    // Map<MemberTypeEnum, Double> memberTypeAmounts =
    // getMemberTypeAmounts(memberTypeContributionJSON);
    // TODO | but for now, we just pass it through
    memberLevelRateDataJSON.put("memberTypeContributions", memberTypeContributionJSON);

    // get whatever data is available
    JSONObject memberTypeCostJSON = memberLevelJSON.getJSONObject("memberTypeCosts");

    // TODO | ultimately we will use this (just here to see if it blows up)
    // Map<MemberTypeEnum, Double> memberTypeAmounts =
    // getMemberTypeAmounts(memberTypeCostJSON);
    // TODO | but for now, we just pass it through
    memberLevelRateDataJSON.put("memberTypeCosts", memberTypeCostJSON);

    return memberLevelRateDataJSON;

  }

  public JSONObject buildProduct(JSONObject productJSON) throws JSONException
  {
    JSONObject resultJSON = new JSONObject();

    // get tier
    String tier = productJSON.getString("tier");

    // get id
    String productId = productJSON.getString("id");

    // get productType
    String productType = productJSON.getString("planType");

    // get displayName
    String displayName = productJSON.getString("name");

    // get monthly cost
    JSONObject costJSON = productJSON.getJSONObject("cost");
    JSONObject monthlyCostJSON = costJSON.getJSONObject("monthly");

    if (monthlyCostJSON.getBoolean("memberLevelRated"))
    {
      resultJSON.put("memberLevelRating",
        buildMemberLevelRatedData(monthlyCostJSON.getJSONObject("memberLevelRating")));
    }

    // get monthly rate
    JSONObject monthlyRate = monthlyCostJSON.getJSONObject("rate");
    Double rate = monthlyRate.getDouble("amount");

    // get monthly contribution
    JSONObject monthlyContribution = monthlyCostJSON.getJSONObject("contribution");
    Double contribution = monthlyContribution.getDouble("amount");

    Double netCost = (rate - contribution);
    BigDecimal roundedCost = new BigDecimal(netCost).setScale(2, RoundingMode.HALF_UP);
    Double employeeCost = roundedCost.doubleValue() > 0 ? roundedCost.doubleValue() : 0;

    resultJSON.put("productId", productId);
    resultJSON.put("productType", productType);
    resultJSON.put("productName", displayName);
    resultJSON.put("tier", tier);
    resultJSON.put("monthlyRate", rate);
    resultJSON.put("employerContribution", contribution);
    resultJSON.put("employeeCost", employeeCost);

    return resultJSON;
  }

  public JSONObject getEnrollmentData(Long accountId) throws JSONException
  {
    JSONObject enrollmentJSON = new JSONObject();

    Session session = sessionFactory.getCurrentSession();

    Criteria crit = session.createCriteria(AccountInfo.class).add(
      Restrictions.eq("accountId", accountId));

    AccountInfo accountInfo = (AccountInfo) crit.uniqueResult();

    if (null != accountInfo)
    {
      JSONObject accountValueJSON = accountInfo.getValue();

      // build memberMap
      Map<String, JSONObject> memberMap = this.buildMemberMap(accountValueJSON);

      // member - productLine Map
      Map<String, JSONArray> memberProductLineMap = new HashMap<String, JSONArray>();

      // build productLineMap
      // find all products in cart and add to resultProductsJSON
      JSONObject cartJSON = accountValueJSON.getJSONObject("cart");
      // product lines
      JSONObject productLinesJSON = cartJSON.getJSONObject("productLines");
      String[] productLines = JSONObject.getNames(productLinesJSON);
      for (int j = 0; j < productLines.length; j++)
      {
        String productLineName = productLines[j];
        JSONObject productLineJSON = productLinesJSON.getJSONObject(productLineName);

        JSONObject resultProductLineJSON = new JSONObject();

        // construct product line fields
        String prodoctLineCode = productLineJSON.getString("code");
        resultProductLineJSON.put("productLineCode", prodoctLineCode);

        // TODO | changed for support of multiple products per product line
        //JSONObject productJSON = this.buildProduct(productLineJSON.getJSONObject("product"));       
        JSONArray currentProductsJSON = productLineJSON.getJSONArray("products"); 
        JSONArray newProductsJSON = new JSONArray();
        
        
        for (int prodIdx = 0; prodIdx < currentProductsJSON.length(); prodIdx++){
          JSONObject productJSON = this.buildProduct(currentProductsJSON.getJSONObject(prodIdx)); 
          newProductsJSON.put(prodIdx, productJSON);
        }
        
        //resultProductLineJSON.put("product", productJSON);
        resultProductLineJSON.put("products", newProductsJSON);

        JSONArray coveredMembers = productLineJSON.getJSONArray("coveredMembers");
        // is a member covered in this plan? if so, add the productLineJSON
        for (int k = 0; k < coveredMembers.length(); k++)
        {
          JSONObject coveredMemberJSON = coveredMembers.getJSONObject(k);
          String memberRefName = coveredMemberJSON.getString("memberRefName");
          JSONArray currentMemberProductLines = memberProductLineMap.get(memberRefName);
          if (currentMemberProductLines == null)
          {
            currentMemberProductLines = new JSONArray();
            currentMemberProductLines.put(resultProductLineJSON);
          }
          else
          {
            currentMemberProductLines.put(resultProductLineJSON);
          }

          memberProductLineMap.put(memberRefName, currentMemberProductLines);
        }
      }

      JSONArray resultMembersArray = new JSONArray();
      // so now we have a Map of memberId - members, and a map of memberId -
      // productLines
      for (Map.Entry<String, JSONObject> memberEntry : memberMap.entrySet())
      {
        // find a member
        JSONObject memberJSON = memberEntry.getValue();

        // find the member's product lines

        JSONArray currentMemberProductLines = memberProductLineMap.get(memberEntry.getKey());

        JSONArray memberProductLines = new JSONArray();

        // deep copy clone the objects and make a new array
        for (int i = 0; i < currentMemberProductLines.length(); i++)
        {
          JSONObject memberProductLine = currentMemberProductLines.getJSONObject(i);
          memberProductLines.put(new JSONObject(memberProductLine.toString()));
        }

        // groom the product data to fit the member
        for (int i = 0; i < memberProductLines.length(); i++)
        {
          String memberRate = "";

          JSONObject memberProductLine = memberProductLines.getJSONObject(i);
          
          //JSONObject memberProduct = memberProductLine.getJSONObject("product");
          
          JSONArray productsJSON = memberProductLine.getJSONArray("products");
          // TODO | iterate to get all products.
          // for now, we only support one object, so just send that;
          JSONObject memberProduct = productsJSON.getJSONObject(0);
          

          if (memberProduct.has("memberLevelRating"))
          {
            // find the member rate for the current member
            JSONObject memberLevelRating = memberProduct.getJSONObject("memberLevelRating");
            JSONArray memberRates = memberLevelRating.getJSONArray("memberRates");

            // try to grab individual id for a member, if it exists,
            // otherwise the member should be using a numeric temp id

            String memberId = memberEntry.getKey();
            if (!memberJSON.getBoolean("isNew"))
            {
              memberId = memberJSON.optString("individualId");
            }

            for (int j = 0; j < memberRates.length(); j++)
            {
              JSONObject memberRateJSON = memberRates.getJSONObject(j);

              if (memberRateJSON.getString("memberId").equalsIgnoreCase(memberId))
              {
                memberRate = memberRateJSON.getString("amount");
                memberLevelRating.put("memberRate", memberRate);
                break;
              }
            }

            memberLevelRating.remove("memberRates");
          }
        }

        memberJSON.put("productLines", memberProductLines);
        resultMembersArray.put(memberJSON);
      }

      enrollmentJSON.put("members", resultMembersArray);

      LOG.info("enrollmentJSON: " + enrollmentJSON.toString(4));
    }

    return enrollmentJSON;
  }

  /**
   * @param sessionFactory the new value of sessionFactory
   */
  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }

}