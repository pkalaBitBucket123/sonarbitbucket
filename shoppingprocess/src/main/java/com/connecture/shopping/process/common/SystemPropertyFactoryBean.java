package com.connecture.shopping.process.common;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Loads properties from a file defined by a system property if it has been set.
 */
public class SystemPropertyFactoryBean extends PropertiesFactoryBean implements ApplicationContextAware
{
  private String systemProperty;
  private ApplicationContext ctx;
  
  @Override
  @SuppressWarnings("deprecation")
  protected Object createInstance() throws IOException
  {
    String sysPropValue = System.getProperty(systemProperty);
    if (sysPropValue != null)
    {
      setLocation(ctx.getResource(sysPropValue));
      return super.createInstance();
    }
    return new Properties();
  }

  public void setApplicationContext(ApplicationContext ctx) throws BeansException
  {
    this.ctx = ctx;
  }

  public String getSystemProperty()
  {
    return systemProperty;
  }

  public void setSystemProperty(String systemProperty)
  {
    this.systemProperty = systemProperty;
  }
}
