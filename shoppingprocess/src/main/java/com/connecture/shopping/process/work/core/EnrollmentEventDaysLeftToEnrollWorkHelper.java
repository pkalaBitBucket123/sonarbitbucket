package com.connecture.shopping.process.work.core;

import org.json.JSONObject;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;

public class EnrollmentEventDaysLeftToEnrollWorkHelper
{
  public static Integer getDaysLeftToEnroll(
    JSONObject accountObject,
    EnrollmentEventRulesEngineBean hcrRulesEngineBean)
  {
    EnrollmentEventDomainModel domainModel = EnrollmentEventRuleEngineHelper.getDomainModel(hcrRulesEngineBean);
    
    return getHCRDaysLeftToEnroll(domainModel);
  }

  public static Integer getHCRDaysLeftToEnroll(EnrollmentEventDomainModel domainModel)
  {
    Integer daysLeftToEnroll = null;

    if (null != domainModel)
    {
      daysLeftToEnroll = domainModel.getApplication().getSpecialEnrollment().getDaysLeftToEnroll();
    }

    return daysLeftToEnroll;
  }
}
