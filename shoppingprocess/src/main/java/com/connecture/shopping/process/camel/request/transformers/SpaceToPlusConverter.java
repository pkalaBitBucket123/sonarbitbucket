package com.connecture.shopping.process.camel.request.transformers;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class SpaceToPlusConverter
{
  private static final Logger logger = Logger.getLogger(SpaceToPlusConverter.class);
  
  private static final String DELIMITER_REGEX = ";\\s*";
  private static final String WHITESPACE_REGEX = "\\s";
  public void convert(Map<String, Object> headers, String headersToConvertStr)
  {
    if(StringUtils.isNotBlank(headersToConvertStr))
    {
      String[] headersToConvert = headersToConvertStr.split(DELIMITER_REGEX);
      for(String header : headersToConvert)
      {
        Object headerVal = headers.get(header);
        if(headerVal instanceof String)
        {
          headerVal = ((String) headerVal).replaceAll(WHITESPACE_REGEX, "+");
          headers.put(header, headerVal);
        }
        else
        {
          logger.error("Attempted to convert non-string header " + header + "|" + String.valueOf(headerVal));
        }
      }
    }
  }
}
