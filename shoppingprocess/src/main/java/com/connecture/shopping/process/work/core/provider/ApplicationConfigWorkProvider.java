package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.ApplicationConfigWork;

public abstract class ApplicationConfigWorkProvider implements WorkProviderImpl<ApplicationConfigWork>
{
  @Override
  public ApplicationConfigWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject object) throws JSONException
  {
    ApplicationConfigWork work = createWork();
    
    if (WorkConstants.Method.GET.equals(method))
    {
      work.setTransactionId(transactionId);
    }
    return work;
  }
}
