/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

import java.util.List;

/**
 * 
 */
public class PlanBenefitValueData
{
  private String benefitValueTypeCode;
  private String displayName;
  private Double numericValue;
  private String description;
  private List<CustomAttributeData> customAttributes;
  
  /**
   * @return the benefitValueTypeCode
   */
  public String getBenefitValueTypeCode()
  {
    return benefitValueTypeCode;
  }
  
  /**
   * @param benefitValueTypeCode the new value of benefitValueTypeCode
   */
  public void setBenefitValueTypeCode(String benefitValueTypeCode)
  {
    this.benefitValueTypeCode = benefitValueTypeCode;
  }
  
  /**
   * @return the displayName
   */
  public String getDisplayName()
  {
    return displayName;
  }
  
  /**
   * @param displayName the new value of displayName
   */
  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }
  
  /**
   * @return the numericValue
   */
  public Double getNumericValue()
  {
    return numericValue;
  }
  
  /**
   * @param numericValue the new value of numericValue
   */
  public void setNumericValue(Double numericValue)
  {
    this.numericValue = numericValue;
  }
  
  /**
   * @return the description
   */
  public String getDescription()
  {
    return description;
  }
  
  /**
   * @param description the new value of description
   */
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public List<CustomAttributeData> getCustomAttributes()
  {
    return customAttributes;
  }

  public void setCustomAttributes(List<CustomAttributeData> customAttributes)
  {
    this.customAttributes = customAttributes;
  }
}