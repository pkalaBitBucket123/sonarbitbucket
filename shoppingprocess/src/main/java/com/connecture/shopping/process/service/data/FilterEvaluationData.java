package com.connecture.shopping.process.service.data;

import java.util.Map;

public class FilterEvaluationData
{
  private String filterRefId;
  private Map<String, FilterQuestionReferenceData> filterOptionMap;

  /**
   * @return the filterRefId
   */
  public String getFilterRefId()
  {
    return filterRefId;
  }

  /**
   * @param filterRefId the new value of filterRefId
   */
  public void setFilterRefId(String filterRefId)
  {
    this.filterRefId = filterRefId;
  }

  /**
   * @return the filterOptionMap
   */
  public Map<String, FilterQuestionReferenceData> getFilterOptionMap()
  {
    return filterOptionMap;
  }

  /**
   * @param filterOptionMap the new value of filterOptionMap
   */
  public void setFilterOptionMap(Map<String, FilterQuestionReferenceData> filterOptionMap)
  {
    this.filterOptionMap = filterOptionMap;
  }
}