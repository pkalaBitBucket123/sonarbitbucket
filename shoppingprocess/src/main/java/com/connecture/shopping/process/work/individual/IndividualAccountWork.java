package com.connecture.shopping.process.work.individual;

import org.json.JSONObject;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.core.AccountWork;

public class IndividualAccountWork extends AccountWork
{
  @Override
  public void get() throws Exception
  {
    // get a reference to a new or existing account for the transaction Id
    Account account = getAccount();
    accountJSON = account.getOrCreateAccount(transactionId);
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    // Since we are trying to be RESTful with our works here, if this is an update
    // we do not want to return anything unless something has changed. Right now we do
    // not change anything during an UPDATE of account, so return nothing.
    if (null != accountJSON && !WorkConstants.Method.UPDATE.equals(getMethod()))
    {
      // JSONObject accountData = accountJSON;
      jsonObject.put("account", accountJSON);
      jsonObject.put("members", accountJSON.optJSONArray("members"));
    }

    return jsonObject;
  }
  
  JSONObject getAccountJSON()
  {
    return accountJSON;
  }
}