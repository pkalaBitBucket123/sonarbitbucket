package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.work.core.provider.CostEstimatorSettingsDataProvider;

public class IFPCostEstimatorSettingsDataProvider extends CostEstimatorSettingsDataProvider
{
  @Override
  protected boolean validDemographicsZipCode(JSONObject accountJSON)
  {
    return null != accountJSON.optString("zipCode", null);
  }
  
  /**
   * IFP introduces the concept of a single zipCode for the "family".  Populate the consumer zipCode
   * from the Account object.
   * 
   * @see com.connecture.shopping.process.work.core.CostEstimatorCostsWork#populateMemberZipCode(Consumer, org.json.JSONObject)
   */
  @Override
  protected void populateMemberZipCode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(getAccount(), "zipCode"))
    {
      consumer.setZip(getAccount().getString("zipCode"));
    }
  }

  @Override
  protected void populateMemberRelationshipcode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(memberObject, "relationshipKey"))
    {
      String oldRelationshipCode = "Primary";
      String newRelationshipCode = memberObject.getString("relationshipKey");
      if (newRelationshipCode.equals("SPOUSE"))
      {
        oldRelationshipCode = "Spouse (opposite sex)";
      }
      else if (newRelationshipCode.equals("CHILD"))
      {
        oldRelationshipCode = "Child - dependent on subscriber for support and is legal guardian";
      }
      consumer.setRelationshipCode(oldRelationshipCode);
    }    
  }
}
