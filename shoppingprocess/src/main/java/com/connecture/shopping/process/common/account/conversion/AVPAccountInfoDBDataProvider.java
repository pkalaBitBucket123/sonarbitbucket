package com.connecture.shopping.process.common.account.conversion;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.connecture.shopping.process.domain.AccountInfo;

public class AVPAccountInfoDBDataProvider extends AccountInfoDataProvider
{
  private static Logger LOG = Logger.getLogger(AVPAccountInfoDBDataProvider.class);
  
  private SessionFactory sessionFactory;
  
  @Override
  public List<AccountInfo> fetchData() throws Exception
  {
    Session session = sessionFactory.getCurrentSession();
    Criteria crit = session.createCriteria(AccountInfo.class).add(
      Restrictions.ne("version", getVersion()));

    @SuppressWarnings("unchecked")
    List<AccountInfo> accountInfo = (List<AccountInfo>) crit.list();

    if (LOG.isDebugEnabled())
    {
      if (accountInfo.isEmpty())
      {
        LOG.debug("No AccountInfo objects found for conversion");
      }
      else
      {
        LOG.debug("Retrieved " + accountInfo.size() + " AccountInfo objects from DB");
      }
    }
    
    return accountInfo;
  }

  public SessionFactory getSessionFactory()
  {
    return sessionFactory;
  }

  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }
}
