package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;


public class StateKeyForCountyAndZipCodeDataRequest extends BaseCamelRequest<String>
{
  @Produce(uri="{{camel.shopping.producer.getStateKeyForCountyAndZipCode}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setCountyId(Integer countyId)
  {
    headers.put("countyId", countyId);
  }

  public void setZipCode(String zipCode)
  {
    headers.put("zipCode", zipCode);
  }
}
