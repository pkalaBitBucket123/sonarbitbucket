package com.connecture.shopping.process.service.data;

public class AnswerData
{
  private String answerId;
  private String text;
  private String summaryText;
  private String helpText;
  private String helpType;
  private String classOverride;
  private String filterOptionRefId;
  private String dataTagValue;
  private String dataTagOperator;

  public String getAnswerId()
  {
    return answerId;
  }

  public void setAnswerId(String answerId)
  {
    this.answerId = answerId;
  }

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getSummaryText()
  {
    return summaryText;
  }

  public void setSummaryText(String summaryText)
  {
    this.summaryText = summaryText;
  }

  public String getHelpText()
  {
    return helpText;
  }

  public void setHelpText(String helpText)
  {
    this.helpText = helpText;
  }
  
  public String getHelpType()
  {
    return helpType;
  }

  public void setHelpType(String helpType)
  {
    this.helpType = helpType;
  }

  public String getClassOverride()
  {
    return classOverride;
  }

  public void setClassOverride(String classOverride)
  {
    this.classOverride = classOverride;
  }

  public String getFilterOptionRefId()
  {
    return filterOptionRefId;
  }

  public void setFilterOptionRefId(String filterOptionRefId)
  {
    this.filterOptionRefId = filterOptionRefId;
  }

  public String getDataTagValue()
  {
    return dataTagValue;
  }

  public void setDataTagValue(String dataTagValue)
  {
    this.dataTagValue = dataTagValue;
  }

  public String getDataTagOperator()
  {
    return dataTagOperator;
  }

  public void setDataTagOperator(String dataTagOperator)
  {
    this.dataTagOperator = dataTagOperator;
  }
}
