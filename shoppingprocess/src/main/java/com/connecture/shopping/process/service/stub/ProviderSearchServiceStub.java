package com.connecture.shopping.process.service.stub;

import com.connecture.shopping.process.common.JSONFileUtils;

public class ProviderSearchServiceStub
{
  public String getProviders(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "providersearchdata.json");
  }
}
