package com.connecture.shopping.process.service.data;

import java.util.List;

public class PlanEvaluationData
{
  private String questionRefId;
  private Long questionWeight;
  private String questionType;
  private List<PlanEvaluationAnswerData> answers;

  public String getQuestionRefId()
  {
    return questionRefId;
  }

  public void setQuestionRefId(String questionRefId)
  {
    this.questionRefId = questionRefId;
  }

  public Long getQuestionWeight()
  {
    return questionWeight;
  }

  public void setQuestionWeight(Long questionWeight)
  {
    this.questionWeight = questionWeight;
  }

  public String getQuestionType()
  {
    return questionType;
  }

  public void setQuestionType(String questionType)
  {
    this.questionType = questionType;
  }

  public List<PlanEvaluationAnswerData> getAnswers()
  {
    return answers;
  }

  public void setAnswers(List<PlanEvaluationAnswerData> answers)
  {
    this.answers = answers;
  }

}
