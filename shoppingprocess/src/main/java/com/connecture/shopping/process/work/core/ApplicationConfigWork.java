package com.connecture.shopping.process.work.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.PlanAdvisorConfigRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.service.data.AnswerData;
import com.connecture.shopping.process.service.data.FilterData;
import com.connecture.shopping.process.service.data.FilterEvaluationData;
import com.connecture.shopping.process.service.data.FilterOptionData;
import com.connecture.shopping.process.service.data.FilterQuestionReferenceData;
import com.connecture.shopping.process.service.data.PlanAdvisorData;
import com.connecture.shopping.process.service.data.PlanAdvisorDataConstants;
import com.connecture.shopping.process.service.data.PlanAdvisorDataConstants.PlanEvaulationType;
import com.connecture.shopping.process.service.data.PlanEvaluationAnswerData;
import com.connecture.shopping.process.service.data.PlanEvaluationData;
import com.connecture.shopping.process.service.data.PlanEvaluationProductData;
import com.connecture.shopping.process.service.data.QuestionData;
import com.connecture.shopping.process.service.data.QuestionnaireData;
import com.connecture.shopping.process.service.data.WorkflowData;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.provider.AuxShoppingConfigDataProvider;

/**
 * This work will be used to build all configuration for shopping. This configuration can come from both remote calls and internal properties. As this grows we could break this out into other
 * configuration specific classes but this should be the one work that bundles all configuration.
 * 
 * @author Shopping Development
 */
public class ApplicationConfigWork extends WorkImpl
{
  // for broker
  private String rosterURL;

  // header URLs
  private String messagesURL;
  private String myAccountURL;
  private String dashboardURL;
  private String userProfileURL;

  // product links
  private String sbcBaseURL;
  private String brochureBaseURL;

  // email form data
  private String emailFormPhoneVisible;
  private String emailFormPhoneRequired;
  private String emailFormFirstNameVisible;
  private String emailFormFirstNameRequired;
  private String emailFormLastNameVisible;
  private String emailFormLastNameRequired;

  private PlanAdvisorConfigRequest planAdvisorConfigRequest;

  private String auxShoppingConfig;

  private String transactionId;
  private String key;
  private String planAdvisorGroup;
  private PlanAdvisorData planAdvisorData;

  protected static final String WIDGETS_KEY = "widgets";
  protected static final String WIDGET_ID_KEY = "widgetId";
  protected static final String WIDGET_CONFIG_KEY = "config";
  protected static final String WIDGET_TYPE_KEY = "widgetType";

  protected static final String PLAN_ADVISOR_COST_ESTIMATOR_WDIGET_KEY = "CostEstimator";
  protected static final String PLAN_ADVISOR_PROVIDER_SEARCH_WIDGET_KEY = "ProviderLookup";
  protected static final String PLAN_ADVISOR_CONFIGURED_QUESTION_WIDGET_KEY = "ConfiguredQuestion";

  protected static final String WORKFLOW_ITEMS_KEY = "workflowItems";
  protected static final String WORKFLOW_ITEM_QUESTIONS_KEY = "Questions";

  // key from plan advisor config for Tool Tips
  private static final String HELP_TEXT_TYPE_TOOL_TIP_KEY = "Tool Tip";
  private static final String HELP_TEXT_TYPE_TOOL_TIP_INTERNAL_KEY = "tooltip";

  /**
   * @throws Exception
   * @see com.connecture.shopping.process.work.Work#get()
   */
  @Override
  public void get() throws Exception
  {
    planAdvisorConfigRequest.setGroupId(planAdvisorGroup);
    /** 
    *Cherry picked from SUP-3463 
    *SHSBCBSNE-1353: Plan Advisor to be pulled based on Effective date
    *Change Date : 10/29/2014
    *Change Start : SHSBCBSNE-1353
    */
    JSONObject accountValueJSON = getAccount().getAccount(transactionId);
    Date effectiveDate = null;
    if (accountValueJSON != null && accountValueJSON.length() != 0)
    {
      effectiveDate = ShoppingDateUtils.stringToDate(accountValueJSON.optString("effectiveDate", null));
    }
    planAdvisorConfigRequest.setEffectiveDate(effectiveDate.getTime());
    //Change End : SHSBCBSNE-1353
    // To handle new behavior, I think we need to merge all plan advisor
    // configuration
    // into one main configuration for views.

    // This way the client side can better react to the different configurations
    // being passed. At some point we can take it a step further to maybe have
    // an internal view id that we can then connect to different components

    // For the data coming back from plan advisor, if they don't have a
    // widgetid, assign 'Question'

    // The first step is to create an overall workflow for plan advisor.
    // Demographics, Subsidy, Method, then Questions (for now we can group
    // Questions
    // together as one workflow item that would have sub workflows)
    //
    // We will make it follow similar to the workflows config used for the
    // questions.

    // We should also move demographics config call here and then merge that
    // into the widget/view configuration, similar to subsidy, so it is all
    // coming down on the same line
    DataProvider<PlanAdvisorData> dataProvider = new CamelRequestingDataProvider<PlanAdvisorData>(planAdvisorConfigRequest);

    planAdvisorData = getCachedData(ShoppingDataProviderKey.PLAN_ADVISOR, dataProvider);

    auxShoppingConfig = getCachedData(ShoppingDataProviderKey.AUX_SHOPPING_CONFIG, new AuxShoppingConfigDataProvider());
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    processApplicationConfigData(jsonObject);
    // This will add the widgets to the object, these
    // will contain all questions and other "widgets" outside
    // of standard question
    processAuxShoppingConfigData(jsonObject);

    // Shopping Configuration Tool
    // add plan advisor data
    if (getPlanAdvisorData() != null)
    {
      processAndUpdateAccountForVersionData();

      // TODO: While we are in the process of converting
      // things over, these methods will be explicit. Once
      // we are all finished, we can make this more automated

      // this one is going to move the data into the method
      // widget
      processPlanAdvisorWorkflowData(jsonObject);
      processQuestionData(jsonObject);
      processFilterData(jsonObject);
      processQuestionEvaluationData(jsonObject);
    }

    processWorkflowItems(jsonObject);
    // this one has to run near the end because we need
    // data from the other process methods
    processPlugins(jsonObject);

    return jsonObject;
  }

  /**
   * @param jsonObject
   * @param widget
   * @throws JSONException
   */
  protected void addWidget(JSONObject jsonObject, JSONObject widget) throws JSONException
  {
    JSONArray widgets = getOrCreateWidgets(jsonObject);

    widgets.put(widget);
  }

  /**
   * This method will get the widgets array at the root level of the <code>jsonObject</code> that is passed in. If the array does not exist at that level, one will be created and then returned.
   * 
   * @param jsonObject
   * @return
   * @throws JSONException
   */
  protected JSONArray getOrCreateWidgets(JSONObject jsonObject) throws JSONException
  {
    JSONArray widgets = jsonObject.optJSONArray(WIDGETS_KEY);

    if (widgets == null)
    {
      widgets = new JSONArray();
      jsonObject.put(WIDGETS_KEY, widgets);
    }

    return widgets;

  }

  /**
   * @param jsonObject
   * @param widgetId
   * @return
   * @throws JSONException
   */
  protected JSONObject getWidget(JSONObject jsonObject, String widgetId) throws JSONException
  {
    JSONObject retVal = null;
    JSONArray widgets = getOrCreateWidgets(jsonObject);

    for (int i = 0; retVal == null && i < widgets.length(); i++)
    {
      JSONObject widget = widgets.getJSONObject(i);

      if (widgetId.equals(widget.getString(WIDGET_ID_KEY)))
      {
        retVal = widget;
      }
    }

    return retVal;
  }

  private void processQuestionWorkflowItem(JSONObject jsonObject) throws JSONException
  {
    // stores all widgets for this workflow item
    JSONArray questionWidgets = new JSONArray();

    jsonObject.put("widgets", questionWidgets);

    // subworkflows
    JSONArray questionWorkflows = new JSONArray();

    jsonObject.put("workflows", questionWorkflows);

    // going to try it this way first. Grab all questionnaires loop through
    // those to pull
    // in all widgets
    for (QuestionnaireData questionnaireData : getPlanAdvisorData().getQuestionnaires())
    {

      JSONObject questionWorkflow = new JSONObject();

      questionWorkflow.put("id", questionnaireData.getQuestionnaireRefId());

      questionWorkflow.put("config", new JSONObject(questionnaireData));

      JSONArray questionnaireWorflow = new JSONArray();

      questionWorkflow.put("widgets", questionnaireWorflow);

      String questionnaireRefId = questionnaireData.getQuestionnaireRefId();
      for (QuestionData questionData : getPlanAdvisorData().getQuestions())
      {
        if (questionData.getQuestionnaireRefId().equals(questionnaireRefId))
        {
          questionWidgets.put(questionData.getQuestionRefId());
          questionnaireWorflow.put(questionData.getQuestionRefId());
        }

      }

      questionWorkflows.put(questionWorkflow);
    }

  }

  /**
   * This method processes the workflow items that belong to workflows. Every workflow has configuration and is made up of widgets.
   * 
   * @param jsonObject
   * @throws JSONException
   */
  private void processWorkflowItems(JSONObject jsonObject) throws JSONException
  {
    // As of now these must exist in auxShoppingConfig to work properly

    JSONArray workflowItems = jsonObject.getJSONArray(WORKFLOW_ITEMS_KEY);

    for (int i = 0; i < workflowItems.length(); i++)
    {
      JSONObject workflowItem = workflowItems.getJSONObject(i);

      if (WORKFLOW_ITEM_QUESTIONS_KEY.equals(workflowItem.optString("id")))
      {
        processQuestionWorkflowItem(workflowItem);
      }
    }

    // handle questions
  }

  /**
   * Use this method to do any extra processing around plugin configuration
   * 
   * @param jsonObject
   * @throws JSONException
   */
  private void processPlugins(JSONObject jsonObject) throws JSONException
  {
    // grab the array plugins, will process these and then
    // return the plugins we are using
    JSONArray plugins = jsonObject.getJSONArray("plugins");
    JSONArray processedPlugins = new JSONArray();

    // loop through each plugin and see if it can
    // be used
    for (int i = 0; i < plugins.length(); i++)
    {
      JSONObject plugin = (JSONObject) plugins.get(i);

      JSONArray criterias = plugin.optJSONArray("criteria");

      // now we need to loop through all of the criteria and
      // make sure everything matches.
      boolean match = true;
      // for now if there are no criteria it will always match
      if (criterias != null)
      {
        for (int x = 0; x < criterias.length(); x++)
        {
          JSONObject criteria = criterias.getJSONObject(x);
          String criteriaLabel = criteria.getString("label");
          String criteriaValue = criteria.getString("value");

          String value = configCriteria.get(criteriaLabel);

          if (!criteriaValue.equalsIgnoreCase(value))
          {
            match = false;
          }
        }
      }

      if (match)
      {
        // Needs to be an array of objects..don't ask
        JSONObject failObject = new JSONObject();
        failObject.put("id", plugin.getString("id"));

        processedPlugins.put(failObject);
      }
    }

    // overwrite the existing config with our processed config
    jsonObject.put("plugins", processedPlugins);

  }

  private void processQuestionEvaluationData(JSONObject jsonObject) throws JSONException
  {
    // Combining a bunch of the question stuff into one answer evaluation
    // to make it easier to work with
    JSONArray questionEvaluations = new JSONArray();
    for (QuestionData questionData : getPlanAdvisorData().getQuestions())
    {
      // Only process the data tag ones here
      if (questionData.getEvaluationType().equals(PlanEvaulationType.DATA_TAG))
      {

        JSONObject questionEvaluation = new JSONObject();
        // save off key
        questionEvaluation.put("key", questionData.getDataTagKey());
        // save off type
        questionEvaluation.put("type", questionData.getEvaluationType());
        // and the id of the question
        questionEvaluation.put("questionRefId", questionData.getQuestionRefId());

        // Now loop through each answer because we will need some stuff for
        // that
        JSONArray answers = new JSONArray();

        // get this here because we are going to add it to each one
        String weight = questionData.getWeight();
        for (AnswerData answerData : questionData.getAnswers())
        {
          JSONObject answer = new JSONObject();
          answer.put("answerRefId", answerData.getAnswerId());
          // Depending on operator we might want to break this out
          answer.put("value", answerData.getDataTagValue());
          answer.put("operator", answerData.getDataTagOperator());
          answer.put("text", answerData.getText());
          answer.put("summaryText", answerData.getSummaryText());
          answer.put("score", weight);

          answers.put(answer);
        }

        questionEvaluation.put("answers", answers);

        questionEvaluations.put(questionEvaluation);
      }
    }

    // Now loop through the plan evaluations which are for the configuration
    // tags
    // and add those

    for (PlanEvaluationData planEvaluationData : getPlanAdvisorData().getPlanEvaluations())
    {
      // Just make sure it is configuration
      if (planEvaluationData.getQuestionType().equals(PlanEvaulationType.CONFIGURATION))
      {
        JSONObject questionEvaluation = new JSONObject();
        // The key for these is always going to be id
        questionEvaluation.put("key", "id");
        // save off type
        questionEvaluation.put("type", planEvaluationData.getQuestionType());
        // and the id of the question
        questionEvaluation.put("questionRefId", planEvaluationData.getQuestionRefId());

        JSONArray answers = new JSONArray();

        // get this here because we are going to add it to each one
        Long weight = planEvaluationData.getQuestionWeight();
        for (PlanEvaluationAnswerData answerData : planEvaluationData.getAnswers())
        {
          JSONObject answer = new JSONObject();
          answer.put("answerRefId", answerData.getAnswerRefId());

          JSONArray values = new JSONArray();
          for (PlanEvaluationProductData planEvaluationProductData : answerData.getProducts())
          {
            JSONObject value = new JSONObject();
            value.put("value", planEvaluationProductData.getProductId());

            // if the match is false, then set the score to 0 for now,
            // doing this because this tag is being used incorrectly
            Long score = 0L;
            if (planEvaluationProductData.getMatch() != null && planEvaluationProductData.getMatch())
            {
              score = weight;
            }

            value.put("score", score);
            value.put("match", planEvaluationProductData.getMatch());
            values.put(value);
          }

          answer.put("value", values);
          answer.put("operator", PlanAdvisorDataConstants.Operators.EQUALS);
          answer.put("score", weight);

          // We have to pull the summary text from the actual question
          // loop through them to grab it
          // TODO: refactor
          for (QuestionData questionData : getPlanAdvisorData().getQuestions())
          {
            if (questionData.getQuestionRefId().equals(planEvaluationData.getQuestionRefId()))
            {
              for (AnswerData questionAnswerData : questionData.getAnswers())
              {
                if (questionAnswerData.getAnswerId().equals(answerData.getAnswerRefId()))
                {
                  answer.put("text", questionAnswerData.getText());
                  answer.put("summaryText", questionAnswerData.getSummaryText());
                  break;
                }
              }
              break;
            }
          }

          answers.put(answer);
        }
        questionEvaluation.put("answers", answers);

        questionEvaluations.put(questionEvaluation);
      }
      else if (planEvaluationData.getQuestionType().equals(PlanEvaulationType.SCORING))
      {
        JSONObject questionEvaluation = new JSONObject();
        // The key for these is always going to be id
        questionEvaluation.put("key", "id");
        // save off type
        questionEvaluation.put("type", planEvaluationData.getQuestionType());
        // and the id of the question
        questionEvaluation.put("questionRefId", planEvaluationData.getQuestionRefId());

        JSONArray answers = new JSONArray();

        // get this here because we are going to add it to each one
        Long weight = planEvaluationData.getQuestionWeight();
        for (PlanEvaluationAnswerData answerData : planEvaluationData.getAnswers())
        {
          JSONObject answer = new JSONObject();
          answer.put("answerRefId", answerData.getAnswerRefId());

          JSONArray values = new JSONArray();
          for (PlanEvaluationProductData planEvaluationProductData : answerData.getProducts())
          {
            JSONObject value = new JSONObject();
            value.put("value", planEvaluationProductData.getProductId());
            value.put("score", planEvaluationProductData.getScore());

            // uhh best way to figure out match for now until plan
            // advisor config tells us?
            boolean match = true;
            if (planEvaluationProductData.getScore() == 0)
            {
              match = false;
            }

            value.put("match", match);
            values.put(value);
          }

          answer.put("value", values);
          answer.put("operator", PlanAdvisorDataConstants.Operators.EQUALS);
          answer.put("score", weight);

          // We have to pull the summary text from the actual question
          // loop through them to grab it
          // TODO: refactor
          for (QuestionData questionData : getPlanAdvisorData().getQuestions())
          {
            if (questionData.getQuestionRefId().equals(planEvaluationData.getQuestionRefId()))
            {
              for (AnswerData questionAnswerData : questionData.getAnswers())
              {
                if (questionAnswerData.getAnswerId().equals(answerData.getAnswerRefId()))
                {
                  answer.put("text", questionAnswerData.getText());
                  answer.put("summaryText", questionAnswerData.getSummaryText());
                  break;
                }
              }
              break;
            }
          }

          answers.put(answer);
        }
        questionEvaluation.put("answers", answers);

        questionEvaluations.put(questionEvaluation);
      }
    }

    jsonObject.put("questionEvaluations", questionEvaluations);
  }

  private void processFilterData(JSONObject jsonObject) throws JSONException
  {
    Map<String, JSONObject> filterConfigurationMap = new LinkedHashMap<String, JSONObject>();
    for (FilterData filterData : getPlanAdvisorData().getFilters())
    {
      // TODO | clean this up
      filterConfigurationMap.put(filterData.getFilterRefId(), createJSONFromFiltersData(filterData, getPlanAdvisorData().getFilterEvaluationMap().get(filterData.getFilterRefId())));
    }
    jsonObject.put("filters", filterConfigurationMap);
  }

  private void processAndUpdateAccountForVersionData() throws JSONException
  {
    String versionId = getPlanAdvisorData().getVersionId();

    JSONObject accountObject = getAccount().getAccount(transactionId);

    // If the planAdvisorVersion doesn't match Then
    if (!accountObject.has("planAdvisorVersion") || !accountObject.getString("planAdvisorVersion").equals(versionId))
    {
      // Remove any stored question answers
      accountObject.remove("questions");

      // Update the planAdvisorVersion
      accountObject.put("planAdvisorVersion", versionId);

      // Save the changes
      getAccount().updateAccountInfo(transactionId, accountObject);
    }
  }

  private void processQuestionData(JSONObject jsonObject) throws JSONException
  {
    // We are going to merge these into the widget configuration so
    // that we now have only one set of configuration for all plan advisor
    // widgets/views

    for (QuestionData questionData : getPlanAdvisorData().getQuestions())
    {
      JSONObject questionConfig = new JSONObject(questionData);

      // We need to merge some data from the plan advisor request with our local
      // configuration, for the two specific widgets we handle, replace their
      // widget ids with the question id for now
      if (PLAN_ADVISOR_COST_ESTIMATOR_WDIGET_KEY.equals(questionConfig.get("widgetId")))
      {
        JSONObject widget = getWidget(jsonObject, PLAN_ADVISOR_COST_ESTIMATOR_WDIGET_KEY);

        widget.put(WIDGET_ID_KEY, questionConfig.optString("questionRefId"));

        // TODO: Might make more sense to add these to view config
        JSONObject widgetConfig = widget.getJSONObject(WIDGET_CONFIG_KEY);

        JSONArray names = questionConfig.names();

        for (int i = 0; i < names.length(); i++)
        {
          String name = names.getString(i);
          widgetConfig.put(name, questionConfig.get(name));
        }

        // this is used later to determine what plugins to run
        configCriteria.put("hasCostEstimator", "true");
      }
      else if (PLAN_ADVISOR_PROVIDER_SEARCH_WIDGET_KEY.equals(questionConfig.get("widgetId")))
      {
        JSONObject widget = getWidget(jsonObject, PLAN_ADVISOR_PROVIDER_SEARCH_WIDGET_KEY);

        widget.put(WIDGET_ID_KEY, questionConfig.optString("questionRefId"));

        // TODO: Might make more sense to add these to view config
        JSONObject widgetConfig = widget.getJSONObject(WIDGET_CONFIG_KEY);

        // Maybe someone smarter than me can figure this out but I don't think
        // there is a good
        // way to merge to fucking JSONObjects.
        JSONArray names = questionConfig.names();

        for (int i = 0; i < names.length(); i++)
        {
          String name = names.getString(i);
          widgetConfig.put(name, questionConfig.get(name));
        }

        // this is used later to determine what plugins to run
        configCriteria.put("hasProviderSearch", "true");
      }
      else
      {
        JSONObject questionWidgetConfig = new JSONObject();

        // using the questionRefId for the widgetId for now
        questionWidgetConfig.put(WIDGET_ID_KEY, questionConfig.optString("questionRefId"));

        questionWidgetConfig.put(WIDGET_CONFIG_KEY, questionConfig);

        questionWidgetConfig.put(WIDGET_TYPE_KEY, PLAN_ADVISOR_CONFIGURED_QUESTION_WIDGET_KEY);

        addWidget(jsonObject, questionWidgetConfig);

      }
    }

    boolean hasQuestions = false;
    if (getPlanAdvisorData().getQuestions() != null && !getPlanAdvisorData().getQuestions().isEmpty())
    {
      hasQuestions = true;
    }
    // this is used later to determine what plugins to run
    configCriteria.put("hasQuestions", String.valueOf(hasQuestions));
  }

  /**
   * This method is process the "Method" widget. We can rename if necessary. The workflows coming back from PA Config will now be placed undernearth the Method Widget.
   * 
   * @param jsonObject
   * @throws JSONException
   */
  private void processPlanAdvisorWorkflowData(JSONObject jsonObject) throws JSONException
  {
    JSONArray workflowArray = new JSONArray();
    for (WorkflowData workflowData : getPlanAdvisorData().getWorkflows())
    {
      // Sort the workflow question references by questionnaire
      Map<String, List<String>> questionsByQuestionnaire = new HashMap<String, List<String>>();

      List<String> questionRefIds = workflowData.getQuestionRefIds();
      for (String questionRefId : new ArrayList<String>(questionRefIds))
      {
        QuestionData questionRef = null;
        for (QuestionData question : getPlanAdvisorData().getQuestions())
        {
          if (question.getQuestionRefId().equals(questionRefId))
          {
            questionRef = question;
            String questionnaireRefId = questionRef.getQuestionnaireRefId();
            List<String> questionRefsList = questionsByQuestionnaire.get(questionnaireRefId);
            if (null == questionRefsList)
            {
              questionRefsList = new ArrayList<String>();
              questionsByQuestionnaire.put(questionnaireRefId, questionRefsList);
            }
            questionRefsList.add(questionRefId);
          }
        }
      }

      // Now rebuild the list of questions sorted by questionnaire, in
      // questionnaire order
      questionRefIds = new ArrayList<String>();
      for (QuestionnaireData questionnaireData : getPlanAdvisorData().getQuestionnaires())
      {
        List<String> questionRefList = questionsByQuestionnaire.get(questionnaireData.getQuestionnaireRefId());
        if (null != questionRefList)
        {
          questionRefIds.addAll(questionRefList);
        }
      }

      workflowData.setQuestionRefIds(questionRefIds);

      JSONObject workflowObject = createJSONFromWorkflowData(workflowData);
      workflowArray.put(workflowObject);
    }

    // make sure we have some workflows, not sure this will stay like this
    // but a good way to check for now if we will have the method view.
    if (workflowArray.length() > 0)
    {
      JSONObject methodWidget = getWidget(jsonObject, "Method");

      JSONObject methodConfig = methodWidget.getJSONObject(WIDGET_CONFIG_KEY);
      methodConfig.put("workflows", workflowArray);
    }
  }

  protected void processApplicationConfigData(JSONObject jsonObject) throws JSONException
  {
    JSONObject applicationConfigJSON = new JSONObject();

    // Property driven config
    applicationConfigJSON.put("messages-url", messagesURL);
    applicationConfigJSON.put("my-account-url", myAccountURL);
    applicationConfigJSON.put("roster-url", rosterURL);
    applicationConfigJSON.put("sbc-base-url", sbcBaseURL);
    applicationConfigJSON.put("brochure-base-url", brochureBaseURL);
    applicationConfigJSON.put("dashboard-url", dashboardURL);
    applicationConfigJSON.put("user-profile-url", userProfileURL);

    // TODO | anonymous only, split this off
    // email form config (lead gen)
    applicationConfigJSON.put("emailFormPhoneVisible", Boolean.parseBoolean(emailFormPhoneVisible));
    applicationConfigJSON.put("emailFormPhoneRequired", Boolean.parseBoolean(emailFormPhoneRequired));
    applicationConfigJSON.put("emailFormFirstNameVisible", Boolean.parseBoolean(emailFormFirstNameVisible));
    applicationConfigJSON.put("emailFormFirstNameRequired", Boolean.parseBoolean(emailFormFirstNameRequired));
    applicationConfigJSON.put("emailFormLastNameVisible", Boolean.parseBoolean(emailFormLastNameVisible));
    applicationConfigJSON.put("emailFormLastNameRequired", Boolean.parseBoolean(emailFormLastNameRequired));

    jsonObject.put("applicationConfig", applicationConfigJSON);
  }

  private void processAuxShoppingConfigData(JSONObject jsonObject) throws JSONException
  {
    JSONObject auxShoppingConfigObject = new JSONObject(getAuxShoppingConfig());

    // use the configuration criteria to find the proper workflow
    this.filterWorkflowsByCriteria(auxShoppingConfigObject);

    JSONArray auxShoppingConfigObjectJSONNames = auxShoppingConfigObject.names();
    for (int i = 0; null != auxShoppingConfigObjectJSONNames && i < auxShoppingConfigObjectJSONNames.length(); i++)
    {
      String name = auxShoppingConfigObjectJSONNames.getString(i);
      jsonObject.put(name, auxShoppingConfigObject.get(name));
    }
  }

  private void filterWorkflowsByCriteria(JSONObject auxShoppingConfigObject) throws JSONException
  {
    JSONObject selectedWorkflow = null;

    JSONObject workflowsJSON = auxShoppingConfigObject.getJSONObject("workflows");

    String[] names = JSONObject.getNames(workflowsJSON);
    if (null == names)
    {
      throw new JSONException("workflow definitions missing");
    }

    for (String workflowName : names)
    {
      JSONObject workflow = workflowsJSON.getJSONObject(workflowName);

      boolean isMatch = true;

      // check criteria against workflow criteria
      JSONArray workflowCriteria = workflow.getJSONArray("criteria");
      if (!configCriteria.isEmpty() && workflowCriteria.length() > 0)
      {
        for (int i = 0; i < workflowCriteria.length(); i++)
        {
          JSONObject configCriterion = workflowCriteria.getJSONObject(i);
          String configLabel = configCriterion.getString("label");
          String configValue = configCriterion.getString("value");

          String value = configCriteria.get(configLabel);

          if (!configValue.equalsIgnoreCase(value))
          {
            isMatch = false;
          }
        }
      }
      else
      {
        isMatch = false;
      }

      // set the selected workflow if match, else take default if that's what
      // we're looking
      // at here and nothing better has come through
      if (isMatch || (workflowName.equalsIgnoreCase("default") && selectedWorkflow == null))
      {
        selectedWorkflow = workflow;
      }
    }

    auxShoppingConfigObject.remove("workflows");
    auxShoppingConfigObject.put("workflow", selectedWorkflow);
  }

  /**
   * @return the messagesURL
   */
  public String getMessagesURL()
  {
    return messagesURL;
  }

  /**
   * @param messagesURL the new value of messagesURL
   */
  public void setMessagesURL(String messagesURL)
  {
    this.messagesURL = messagesURL;
  }

  /**
   * @return the myAccountURL
   */
  public String getMyAccountURL()
  {
    return myAccountURL;
  }

  /**
   * @param myAccountURL the new value of myAccountURL
   */
  public void setMyAccountURL(String myAccountURL)
  {
    this.myAccountURL = myAccountURL;
  }

  /**
   * @return the rosterURL
   */
  public String getRosterURL()
  {
    return rosterURL;
  }

  /**
   * @param rosterURL the new value of rosterURL
   */
  public void setRosterURL(String rosterURL)
  {
    this.rosterURL = rosterURL;
  }

  /**
   * @return the sbcBaseURL
   */
  public String getSbcBaseURL()
  {
    return sbcBaseURL;
  }

  /**
   * @param sbcBaseURL the new value of sbcBaseURL
   */
  public void setSbcBaseURL(String sbcBaseURL)
  {
    this.sbcBaseURL = sbcBaseURL;
  }

  /**
   * @return the brochureBaseURL
   */
  public String getBrochureBaseURL()
  {
    return brochureBaseURL;
  }

  /**
   * @param brochureBaseURL the new value of brochureBaseURL
   */
  public void setBrochureBaseURL(String brochureBaseURL)
  {
    this.brochureBaseURL = brochureBaseURL;
  }

  /**
   * @return the key
   */
  public String getKey()
  {
    return key;
  }

  /**
   * @param key the new value of key
   */
  public void setKey(String key)
  {
    this.key = key;
  }

  /**
   * @return the userId
   */
  public String getTransactionId()
  {
    return transactionId;
  }

  /**
   * @param userId the new value of userId
   */
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setPlanAdvisorConfigRequest(PlanAdvisorConfigRequest planAdvisorConfigRequest)
  {
    this.planAdvisorConfigRequest = planAdvisorConfigRequest;
  }

  /**
   * @return the planAdvisorGroup
   */
  public String getPlanAdvisorGroup()
  {
    return planAdvisorGroup;
  }

  /**
   * @param planAdvisorGroup the new value of planAdvisorGroup
   */
  public void setPlanAdvisorGroup(String planAdvisorGroup)
  {
    this.planAdvisorGroup = planAdvisorGroup;
  }

  private JSONObject createJSONFromFilterOptionsData(FilterOptionData filterOptionData) throws JSONException
  {
    JSONObject filterOptionObject = new JSONObject();
    filterOptionObject.put("optionId", filterOptionData.getOptionRefId());
    filterOptionObject.put("label", filterOptionData.getText());
    filterOptionObject.put("value", filterOptionData.getDataTagValue());
    filterOptionObject.put("type", filterOptionData.getDataTagOperator());

    // only supporting tooltips for now
    if (HELP_TEXT_TYPE_TOOL_TIP_KEY.equalsIgnoreCase(filterOptionData.getHelpType()))
    {
      filterOptionObject.put("helpText", filterOptionData.getHelpText());
      filterOptionObject.put("helpTextType", HELP_TEXT_TYPE_TOOL_TIP_INTERNAL_KEY);
    }

    return filterOptionObject;
  }

  private JSONObject createJSONFromFilterEvaluationOptionsData(FilterOptionData filterOptionData, FilterQuestionReferenceData filterOptionEvalData) throws JSONException
  {

    JSONArray planIdsJSON = new JSONArray();
    for (String planIdJSON : filterOptionEvalData.getEnabledOptionRefIds())
    {
      planIdsJSON.put(planIdJSON);
    }

    JSONObject filterOptionObject = new JSONObject();
    filterOptionObject.put("optionId", filterOptionData.getOptionRefId());
    filterOptionObject.put("label", filterOptionData.getText());
    filterOptionObject.put("value", planIdsJSON);
    // filter evaluations is a configurable filter, meaning the filter points at
    // particular plans (by id);
    // needs to be an equals type
    filterOptionObject.put("type", "=");

    // only supporting tooltips for now
    if (HELP_TEXT_TYPE_TOOL_TIP_KEY.equalsIgnoreCase(filterOptionData.getHelpType()))
    {
      filterOptionObject.put("helpText", filterOptionData.getHelpText());
      filterOptionObject.put("helpTextType", HELP_TEXT_TYPE_TOOL_TIP_INTERNAL_KEY);
    }

    // filterOptionObject.put("type", filterOptionData.getDataTagOperator());
    return filterOptionObject;
  }

  /**
   * Required to turn the WorkflowData into JSON. The "questionRefIds" didn't populate correctly using the default approach.
   * 
   * @param workflowData
   * @return JSONObject
   * @throws JSONException
   */
  private JSONObject createJSONFromWorkflowData(WorkflowData workflowData) throws JSONException
  {
    JSONObject workflowObject = new JSONObject();
    workflowObject.put("workflowRefId", workflowData.getWorkflowRefId());
    workflowObject.put("title", workflowData.getTitle());
    workflowObject.put("infoBlock", workflowData.getInfoBlock());
    workflowObject.put("classOverride", workflowData.getClassOverride());
    workflowObject.put("defaultWorkflow", workflowData.getDefaultWorkflow());
    JSONArray questionRefIds = new JSONArray();
    for (String questionRefId : workflowData.getQuestionRefIds())
    {
      questionRefIds.put(questionRefId);
    }
    workflowObject.put("questionRefIds", questionRefIds);
    return workflowObject;
  }

  private JSONObject createJSONFromFiltersData(FilterData filterData, FilterEvaluationData evaluationData) throws JSONException
  {
    JSONObject filterObject = new JSONObject();

    boolean isConfigurationTag = filterData.getEvaluationType().equalsIgnoreCase("Configuration Tag");

    // boolean isExclusive = filterData.getDisplayType().equalsIgnoreCase("checkbox") || filterData.getDisplayType().equalsIgnoreCase("slider");

    boolean isExclusive = !filterData.getDisplayType().equalsIgnoreCase("radio");

    // Based on display type, we determine if a filter is exclusive or not
    if (isExclusive)
    {

      filterObject.put("exclusive", false);
    }
    else
    {
      filterObject.put("exclusive", true);
    }

    // Property refers to the property on the plan to which the filter will be
    // applied
    if (isConfigurationTag)
    {
      // doesn't link to a specific property across all plans, so user the
      // filterRefId
      // needs to point to a property we know will be present and unique on the
      // plan; id that bitch
      filterObject.put("property", "id");
    }
    else
    {
      filterObject.put("property", filterData.getDataTagKey());
    }

    filterObject.put("evaluationType", filterData.getEvaluationType());
    filterObject.put("displayType", filterData.getDisplayType());

    // create view object
    JSONObject viewObject = new JSONObject();

    // create view-control object
    JSONObject controlObject = new JSONObject();
    controlObject.put("type", filterData.getDisplayType());

    // create view-attributes object
    JSONObject attributesObject = new JSONObject();
    attributesObject.put("label", filterData.getTitle());

    // we are only supporting tool tips for now
    if (HELP_TEXT_TYPE_TOOL_TIP_KEY.equalsIgnoreCase(filterData.getHelpType()) && filterData.getHelpType() != null)
    {
      attributesObject.put("helpTextType", HELP_TEXT_TYPE_TOOL_TIP_INTERNAL_KEY);
      attributesObject.put("helpText", filterData.getHelpText());
    }

    // assemble view object
    viewObject.put("control", controlObject);
    viewObject.put("attributes", attributesObject);
    filterObject.put("view", viewObject);

    JSONArray filterOptionsArray = new JSONArray();
    for (FilterOptionData filterOptionData : filterData.getOptions())
    {

      // if evaluationType is "configuration", then merge in "filterEvaluation"
      // values
      if (isConfigurationTag)
      {
        FilterQuestionReferenceData filterOptionEvalData = evaluationData.getFilterOptionMap().get(filterOptionData.getOptionRefId());
        filterOptionsArray.put(this.createJSONFromFilterEvaluationOptionsData(filterOptionData, filterOptionEvalData));
      }
      else
      {
        filterOptionsArray.put(this.createJSONFromFilterOptionsData(filterOptionData));
      }
    }

    filterObject.put("filters", filterOptionsArray);

    return filterObject;
  }

  public void setEmailFormPhoneVisible(String emailFormPhoneVisible)
  {
    this.emailFormPhoneVisible = emailFormPhoneVisible;
  }

  public void setEmailFormPhoneRequired(String emailFormPhoneRequired)
  {
    this.emailFormPhoneRequired = emailFormPhoneRequired;
  }

  public void setEmailFormFirstNameVisible(String emailFormFirstNameVisible)
  {
    this.emailFormFirstNameVisible = emailFormFirstNameVisible;
  }

  public void setEmailFormFirstNameRequired(String emailFormFirstNameRequired)
  {
    this.emailFormFirstNameRequired = emailFormFirstNameRequired;
  }

  public void setEmailFormLastNameVisible(String emailFormLastNameVisible)
  {
    this.emailFormLastNameVisible = emailFormLastNameVisible;
  }

  public void setEmailFormLastNameRequired(String emailFormLastNameRequired)
  {
    this.emailFormLastNameRequired = emailFormLastNameRequired;
  }

  public String getAuxShoppingConfig()
  {
    return auxShoppingConfig;
  }

  public void setAuxShoppingConfig(String auxShoppingConfig)
  {
    this.auxShoppingConfig = auxShoppingConfig;
  }

  public PlanAdvisorData getPlanAdvisorData()
  {
    return planAdvisorData;
  }

  public String getDashboardURL()
  {
    return dashboardURL;
  }

  public void setDashboardURL(String dashboardURL)
  {
    this.dashboardURL = dashboardURL;
  }

  public String getUserProfileURL()
  {
    return userProfileURL;
  }

  public void setUserProfileURL(String userProfileURL)
  {
    this.userProfileURL = userProfileURL;
  }
}