package com.connecture.shopping.process.common.account.conversion.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONPathUtil;

public class AVPDeleteConverter extends AVPConverterImpl
{
  private String path;
  private String reason;
  
  public AVPDeleteConverter() {
    super(AVPConverterType.DELETE);
  }
  
  @Override
  public void apply(JSONObject jsonObject) throws JSONException
  {
    JSONPathUtil.remove(jsonObject, path);
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }

  public String getReason()
  {
    return reason;
  }

  public void setReason(String reason)
  {
    this.reason = reason;
  }
}
