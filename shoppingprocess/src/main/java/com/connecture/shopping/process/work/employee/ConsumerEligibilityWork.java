package com.connecture.shopping.process.work.employee;

import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingMember;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.shopping.process.camel.request.ConsumerTierDataRequest;
import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.ConsumerPlanData;
import com.connecture.shopping.process.service.data.MemberEligibilityData;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.service.data.converter.ConsumerDataConverterUtil;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;

public class ConsumerEligibilityWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(ConsumerEligibilityWork.class);

  // inputs
  private String transactionId;
  
  private List<MemberEligibilityData> memberEligibilities;
  
  // product server group, configurable
  private String productServerGroup;
  
  // requests
  private ConsumerTierDataRequest consumerTierDataRequest;

  @Override
  public void get() throws Exception
  {
    // get account value JSON

    Account account = getAccount();

    JSONObject accountValueJSON = null;
    JSONArray membersJSON = null;

    if (account != null)
    {
      accountValueJSON = account.getAccount(transactionId);

      if (accountValueJSON != null)
      {
        membersJSON = accountValueJSON.optJSONArray("members");
      }
    }

    if (membersJSON != null)
    {
      // check for eligibilityGroupId
      Long eligibilityGroupId = accountValueJSON.optLong("eligibilityGroupId");

      if (eligibilityGroupId != null)
      {
        consumerTierDataRequest.setEligibilityGroupId(eligibilityGroupId);
      }
      else
      {
        consumerTierDataRequest.setEligibilityGroupId(null);
      }
      
      // If AccountInfo exists and AccountInfo.value has member demographics
      // data, then populate the MemberData of the request using AccountInfo
      // member demographics data
      if (LOG.isDebugEnabled())
      {
        LOG.debug("product plan members: " + membersJSON.toString(4));
      }

      // build List of shoppingMembers from memberJSON
      List<ShoppingMember> shoppingMembers = ConsumerDataConverterUtil.buildShoppingMembers(membersJSON);
      consumerTierDataRequest.setShoppingMembers(shoppingMembers);
    }
    else
    {
      consumerTierDataRequest.setEligibilityGroupId(null);

      // send no member data; this will result in the existing member data
      // being
      // returned from the consumer system
      consumerTierDataRequest.setMemberJSON(new JSONObject());
    }

    memberEligibilities = getMemberEligibility(transactionId, productServerGroup);
  }

  public List<MemberEligibilityData> getMemberEligibility(
    String transactionId,
    String productServerGroup) throws Exception
  {
    consumerTierDataRequest.setTransactionId(transactionId);
    consumerTierDataRequest.setContext(ShoppingContext.EMPLOYEE);
    
    // Call to GPA to get the plan / eligibility data
    DataProvider<ConsumerPlanData> consumerTierDataProvider = new CamelRequestingDataProvider<ConsumerPlanData>(consumerTierDataRequest);

    ConsumerPlanData consumerProductsData = getCachedData(ShoppingDataProviderKey.CONSUMER_TIER,
      consumerTierDataProvider);

    return consumerProductsData.getMemberEligibilities();
  }
  
  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();
    
    JSONObject eligibilityJSON = new JSONObject();
    JSONArray memberEligibilitiesJSON = new JSONArray();
    
    for (MemberEligibilityData memberEligibility : memberEligibilities)
    {
      JSONObject productLineEligibilitiesJSON = new JSONObject();
      
      JSONObject memberEligibilityJSON = new JSONObject();
      memberEligibilityJSON.put("memberKey", memberEligibility.getMemberKey());

      for (Entry<String, Boolean> productLineEligibility : memberEligibility.getProductLineEligibility().entrySet())
      {
        productLineEligibilitiesJSON.put(productLineEligibility.getKey(), productLineEligibility.getValue());
      }
      memberEligibilityJSON.put("productLineEligibility", productLineEligibilitiesJSON);
      
      memberEligibilitiesJSON.put(memberEligibilityJSON);
    }
    
    eligibilityJSON.put("memberProductLines", memberEligibilitiesJSON);
    
    jsonObject.put("eligibility", eligibilityJSON);
    //jsonObject.put("eligibility", "true");
    return jsonObject;
  }

  /**
   * @return the transactionId
   */
  public String getTransactionId()
  {
    return transactionId;
  }

  /**
   * @param transactionId the transactionId to set
   */
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  /**
   * @param consumerTierDataRequest the consumerTierDataRequest to set
   */
  public void setConsumerTierDataRequest(ConsumerTierDataRequest consumerTierDataRequest)
  {
    this.consumerTierDataRequest = consumerTierDataRequest;
  }

  /**
   * @param productServerGroup the productServerGroup to set
   */
  public void setProductServerGroup(String productServerGroup)
  {
    this.productServerGroup = productServerGroup;
  }
}