package com.connecture.shopping.process.work.employee.provider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.work.core.provider.CostEstimatorSettingsDataProvider;

public class SGCostEstimatorSettingsDataProvider extends CostEstimatorSettingsDataProvider
{
  @Override
  protected boolean validDemographicsZipCode(JSONObject accountJSON) throws JSONException
  {
    boolean validDemographicsZipCode = false;

    JSONArray membersJSON = null;
    if (isNotNullAndNotJSONNull(accountJSON, "members"))
    {
      membersJSON = accountJSON.getJSONArray("members");
    }

    for (int i = 0; !validDemographicsZipCode && i < membersJSON.length(); i++)
    {
      JSONObject memberJSON = membersJSON.getJSONObject(i);

      validDemographicsZipCode = isNotNullAndNotJSONNull(memberJSON, "zipCode");
    }

    return validDemographicsZipCode;
  }
  
  @Override
  protected void populateMemberZipCode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(memberObject, "zipCode"))
    {
      consumer.setZip(memberObject.getString("zipCode"));
    }
  }

  @Override
  protected void populateMemberRelationshipcode(Consumer consumer, JSONObject memberObject)
    throws JSONException
  {
    if (isNotNullAndNotJSONNull(memberObject, "memberRelationship"))
    {
      consumer.setRelationshipCode(memberObject.getString("memberRelationship"));
    }    
  }
}
