package com.connecture.shopping.process.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * Utility methods for working with Dates in Shopping.
 */
public class ShoppingDateUtils
{
  private static Logger LOG = Logger.getLogger(ShoppingDateUtils.class);
  
  public static final String PRIMARY_DATE_FORMAT = "MM/dd/yyyy";
  public static final String SECONDARY_DATE_FORMAT = "MMddyyyy";
  public static final String DISPLAY_FORMAT = "MM/dd/yyyy";

  static final String supportedParseFormats[] = {
      PRIMARY_DATE_FORMAT, SECONDARY_DATE_FORMAT
  };

  public static String dateToString(Date date)
  {
    String formattedDate = null;
    try
    {
      SimpleDateFormat formatter = new SimpleDateFormat(DISPLAY_FORMAT);
      formatter.setLenient(false);
      formattedDate = formatter.format(date);
    }
    catch (Exception e) 
    {
      LOG.error("Failed formatting date \"" + date + "\" to String", e);
    }
    return formattedDate;
  }

  /**
   * Attempts to convert the given string to a Date. It supports both 'MM/dd/yyyy' and 'MMddyyyy' as
   * valid formats.
   *
   * @param dateString the string to try to convert.
   * @return a Date of the passed in string or null if the date is not valid.
   */
  public static synchronized Date stringToDate(String dateString)
  {
    Date parsed = null;
    
    if (StringUtils.isNotBlank(dateString))
    {
      // See if it matches a format
      // keep trying formats until one is successful
      for (int i = 0; i < supportedParseFormats.length && null == parsed; i++)
      {
        String format = supportedParseFormats[i];
        try
        {
          SimpleDateFormat parser = new SimpleDateFormat(format);
          parser.setLenient(false);
          parsed = parser.parse(dateString);
        }
        catch (ParseException e)
        {
          LOG.info("Failed to parse \"" + dateString + "\" as \"" + format + "\"");
        }
      }
    }
    else
    {
      LOG.error("Failed to parse String, cannot be null or Blank");
    }
    
    return parsed;
  }
}