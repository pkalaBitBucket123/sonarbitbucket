/**
 * 
 */
package com.connecture.shopping.process.work;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.DataProviderKey;

/**
 * 
 */
public abstract class WorkImpl implements Work
{
  private static Logger LOG = Logger.getLogger(WorkImpl.class);

  // unique identifier
  private String key;
  private WorkConstants.Method method;
  private DataCache dataCache;
  // I think it might be overkill for the works
  // to get the AccountWork, so instead they
  // will get an account "dao".
  private Account account;

  // errors
  private Map<String, String> errors = new HashMap<String, String>();
  // configuration params (used currently to help determine workflow)
  protected Map<String, String> configCriteria = new HashMap<String, String>();

  public JSONObject processErrors()
  {
    JSONObject jsonErrors = new JSONObject();

    try
    {
      for (String key : errors.keySet())
      {
        jsonErrors.put(key, errors.get(key));
      }
    }
    catch (JSONException e)
    {
      LOG.error(e.getMessage());
    }
    return jsonErrors;
  }

  @Override
  public void run() throws Exception
  {
    LOG.info("Started Work " + getClass().getSimpleName());
    StopWatch watch = new StopWatch();
    watch.start();

    // handle these in order; read operations will run AFTER other
    // operations
    if (WorkConstants.Method.GET.equals(method))
    {
      get();
    }
    else if (WorkConstants.Method.CREATE.equals(method))
    {
      create();
    }
    else if (WorkConstants.Method.UPDATE.equals(method))
    {
      update();
    }
    else if (WorkConstants.Method.DELETE.equals(method))
    {
      delete();
    }

    watch.stop();
    LOG.info("Completed Work " + getClass().getSimpleName() + " in " + watch.getTime() + "ms");
  }

  @Override
  public void get() throws Exception
  {
  }

  @Override
  public void create() throws Exception
  {
  }

  @Override
  public void update() throws Exception
  {
  }

  @Override
  public void delete() throws Exception
  {
  }

  @Override
  public boolean isAsynchronous()
  {
    return false;
    // return WorkConstants.Method.GET.equalsIgnoreCase(method);
  }

  /**
   * @return the key
   */
  public String getKey()
  {
    return key;
  }

  /**
   * @param key the new value of key
   */
  public void setKey(String key)
  {
    this.key = key;
  }

  public WorkConstants.Method getMethod()
  {
    return method;
  }

  public void setMethod(WorkConstants.Method method)
  {
    this.method = method;
  }

  /**
   * @return the errors
   */
  public Map<String, String> getErrors()
  {
    return errors;
  }

  /**
   * @param errors the new value of errors
   */
  public void setErrors(Map<String, String> errors)
  {
    this.errors = errors;
  }

  protected <T> T getCachedData(DataProviderKey key) throws Exception
  {
    @SuppressWarnings("unchecked")
    T data = (T) getCachedData(key, null);
    return data;
  }

  protected <T> T getCachedData(DataProviderKey key, DataProvider<T> provider) throws Exception
  {
    T data = (T) dataCache.getData(key, provider);
    return data;
  }

  protected DataCache getDataCache()
  {
    return dataCache;
  }

  public void setDataCache(DataCache dataCache)
  {
    this.dataCache = dataCache;
  }

  @Override
  public void setAccount(Account account)
  {
    this.account = account;
  }

  protected Account getAccount()
  {
    return this.account;
  }

  public Map<String, String> getConfigCriteria()
  {
    return configCriteria;
  }

  public void setConfigCriteria(Map<String, String> configCriteria)
  {
    this.configCriteria = configCriteria;
  }
}