package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;

public abstract class AVPConverterUtils
{
  private static Map<AVPConverterType, AVPConverterUtil<?>> converterUtils = new HashMap<AVPConverterType, AVPConverterUtil<?>>();

  static {
    converterUtils.put(AVPConverterType.DELETE, new AVPDeleteConverterUtil());
    converterUtils.put(AVPConverterType.ADD, new AVPAddConverterUtil());
    converterUtils.put(AVPConverterType.RENAME, new AVPRenameConverterUtil());
    converterUtils.put(AVPConverterType.OBJECT_TO_ARRAY, new AVPObjectToArrayConverterUtil());
  }
  
  public static List<String> validateJSON(JSONObject converterObject) throws JSONException
  {
    List<String> errors = new ArrayList<String>();
    
    JSONValidationUtils.checkForMissingField(errors, converterObject, "type");
    
    if (errors.isEmpty())
    {
      if (null != AVPConverterType.findForName(converterObject.getString("type")))
      {
        // Can't continue validating if "type" is not found
        errors.addAll(validateJSON(AVPConverterType.findForName(converterObject.getString("type")), converterObject));
      }
      else
      {
        errors.add("Converter type \"" + converterObject.getString("type") + "\" is not supported");
      }
    }
      
    return errors;
  }

  public static List<String> validateJSON(AVPConverterType type, JSONObject converterObject) throws JSONException
  {
    return converterUtils.get(type).validateJSON(converterObject);
  }

  public static <T extends AVPConverter> T  createAVPConverter(JSONObject converterObject) throws JSONException
  {
    return (T)createAVPConverter(AVPConverterType.findForName(converterObject.getString("type")), converterObject);
  }
  
  public static <T extends AVPConverter> T  createAVPConverter(AVPConverterType type, JSONObject converterObject) throws JSONException {
    @SuppressWarnings("unchecked")
    T converter = (T)converterUtils.get(type).createConverter(converterObject);
    return converter;
  }
}
