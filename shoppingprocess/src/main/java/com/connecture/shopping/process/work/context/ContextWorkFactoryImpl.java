package com.connecture.shopping.process.work.context;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.Account;
import com.connecture.shopping.process.service.data.cache.DataCache;
import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.Work;
import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;

public class ContextWorkFactoryImpl implements ContextWorkFactory
{
  private Map<RequestType, WorkProviderImpl<?>> workProviderMap = new HashMap<RequestType, WorkProviderImpl<?>>();

  private Account account;

  @Override
  public Work getWork(
    String transactionId,
    RequestType type,
    String requestKey,
    Method method,
    JSONArray params,
    JSONObject data,
    DataCache dataCache) throws Exception
  {
    Work work = null;

    JSONObject paramsObject = toJSONObject(params);
    
    WorkProviderImpl<?> provider = workProviderMap.get(type);
    if (null != provider)
    {
      work = provider.getWork(transactionId, requestKey, method, paramsObject, data);

      if (null != work)
      {
        work.setKey(requestKey);
        work.setMethod(method);
        work.setDataCache(dataCache);
        // Set the account for each employee work.

        // We are setting the account here for each work based on
        // context because technically each context might have different
        // account DAO.
        work.setAccount(getAccount());
      }
    }
    else
    {
      throw new Exception("Unable to create object for request type [" + String.valueOf(type) + "]");
    }

    return work;
  }
  
  @Override
  public Work getAccountWork(String transactionId) throws Exception
  {
    return getWork(transactionId, RequestType.ACCOUNT, "", WorkConstants.Method.GET, new JSONArray(),
      new JSONObject(), null);
  }

  protected void registerProvider(RequestType type, WorkProviderImpl<?> workProvider)
  {
    workProviderMap.put(type, workProvider);
  }
  
  JSONObject toJSONObject(JSONArray array) throws JSONException
  {
    JSONObject input = new JSONObject();
    
    if (null != array && array.length() > 0)
    {
      for (int i = 0; i < array.length(); i++)
      {
        JSONObject inputObject = array.getJSONObject(i);
        if (inputObject.has("key") && inputObject.has("value"))
        {
          input.put(inputObject.getString("key"), inputObject.getString("value"));
        }

      }
    }
    
    return input;
  }

  public Account getAccount()
  {
    return account;
  }

  public void setAccount(Account account)
  {
    this.account = account;
  }
}
