package com.connecture.shopping.process.service.data;

public class MemberData
{
  private String memberExtRefId;
  private String memberRefName;
  private String memberRelationship;
  private Integer memberAge;
  private String gender;
  private Long dateOfBirth;
  private String birthDate;
  private String smokerStatus;
  private String workEmail;
  private String zipCode;
  private NameData nameData;
  private Boolean isPrimary = false;
  private Boolean isNew = false;

  public String getMemberExtRefId()
  {
    return memberExtRefId;
  }

  public void setMemberExtRefId(String memberExtRefId)
  {
    this.memberExtRefId = memberExtRefId;
  }
  
  public String getMemberRefName()
  {
    return memberRefName;
  }

  public void setMemberRefName(String memberRefName)
  {
    this.memberRefName = memberRefName;
  }

  public String getMemberRelationship()
  {
    return memberRelationship;
  }

  public void setMemberRelationship(String memberRelationship)
  {
    this.memberRelationship = memberRelationship;
  }

  public Integer getMemberAge()
  {
    return memberAge;
  }

  public void setMemberAge(Integer memberAge)
  {
    this.memberAge = memberAge;
  }
  
  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public Long getDateOfBirth()
  {
    return dateOfBirth;
  }

  public void setDateOfBirth(Long dateOfBirth)
  {
    this.dateOfBirth = dateOfBirth;
  }

  public String getBirthDate()
  {
    return birthDate;
  }

  public void setBirthDate(String birthDate)
  {
    this.birthDate = birthDate;
  }

  public String getSmokerStatus()
  {
    return smokerStatus;
  }

  public void setSmokerStatus(String smokerStatus)
  {
    this.smokerStatus = smokerStatus;
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }

  /**
   * @return the workEmail
   */
  public String getWorkEmail()
  {
    return workEmail;
  }

  /**
   * @param workEmail the new value of workEmail
   */
  public void setWorkEmail(String workEmail)
  {
    this.workEmail = workEmail;
  }

  /**
   * @return the nameData
   */
  public NameData getName()
  {
    return nameData;
  }

  /**
   * @param nameData the new value of nameData
   */
  public void setName(NameData nameData)
  {
    this.nameData = nameData;
  }

  public Boolean getIsPrimary()
  {
    return isPrimary;
  }

  public void setIsPrimary(Boolean isPrimary) {
    this.isPrimary = isPrimary;
  }

  /**
   * @return the nameData
   */
  public NameData getNameData()
  {
    return nameData;
  }

  /**
   * @param nameData the new value of nameData
   */
  public void setNameData(NameData nameData)
  {
    this.nameData = nameData;
  }

  /**
   * @return the isNew
   */
  public Boolean isNew()
  {
    return isNew;
  }

  /**
   * @param isNew the new value of isNew
   */
  public void setNew(Boolean isNew)
  {
    this.isNew = isNew;
  }
}