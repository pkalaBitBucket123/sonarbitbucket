package com.connecture.shopping.process.common.account.conversion.json;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONPathBulkRemoveAction implements JSONPathAction
{
  // List of removed objects
  private List<Object> allRemoved = new ArrayList<Object>();
  
  @Override
  public boolean perform(String key, JSONObject jsonObject, PathState state) throws JSONException
  {
    boolean success = false;
    
    if (isValidFor(state))
    {
      Object removed = jsonObject.remove(key);
      allRemoved.add(removed);
    
      success = removed != null;
    }
    
    return success;
  }

  @Override
  public boolean isValidFor(PathState state)
  {
    return JSONPathAction.PathState.FOUND.equals(state);
  }

  public List<Object> getAllRemoved()
  {
    return allRemoved;
  }
}
