package com.connecture.shopping.process.work.core;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.PlanAdvisorConfigRequest;
import com.connecture.shopping.process.camel.request.ShoppingRulesEngineDataRequest;
import com.connecture.shopping.process.service.data.Attribute;
import com.connecture.shopping.process.service.data.AttributeProperty;
import com.connecture.shopping.process.service.data.Benefit;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ConsumerProfile;
import com.connecture.shopping.process.service.data.PlanAdvisorData;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.Product;
import com.connecture.shopping.process.service.data.ProductPlanData;
import com.connecture.shopping.process.service.data.Property;
import com.connecture.shopping.process.service.data.QuestionData;
import com.connecture.shopping.process.service.data.Request;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.service.plan.data.CustomAttributeData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitValueSetData;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.provider.AuxShoppingConfigDataProvider;
import com.connecture.shopping.process.work.core.provider.CostEstimatorSettingsDataProvider;

public abstract class CostEstimatorCostsWork extends WorkImpl
{
  private static Logger LOG = Logger.getLogger(CostEstimatorCostsWork.class);

  // Start Required Inputs
  private PlanAdvisorConfigRequest planAdvisorConfigRequest;
  private ShoppingRulesEngineDataRequest rulesEngineDataRequest;
  private String costEstimatorBenefitKeyRuleSet;
  private String costEstimatorSettingsRuleSet;
  private CostEstimatorSettingsDataProvider costEstimatorSettingsDataProvider;
  private String costEstimatorCostsRuleSet;
  private String planAdvisorGroup;
  private String rulesEngineGroupId;
  private String transactionId;
  // End Required Inputs

  private ShoppingDomainModel rulesEngineDomain;
  private PlanAdvisorData planAdvisorData;

  @Override
  public void get() throws Exception
  {
    JSONObject accountJSON = getAccountObject();
    if (accountJSON != null)
    {
      if(validDemographicsForProcessing(accountJSON))
      {
        rulesEngineDomain = getCostEstimatorSettings();

        List<String> ruleCodes = null;

        rulesEngineDataRequest.setGroupId(rulesEngineGroupId);
        ruleCodes = new ArrayList<String>();
        ruleCodes.add(costEstimatorBenefitKeyRuleSet);
        rulesEngineDataRequest.setRuleCodes(ruleCodes);
        rulesEngineDataRequest.setEffDate(new Date().getTime());
    
        rulesEngineDomain.getRequest().setId("1");
        rulesEngineDomain.getRequest().setEffectiveDate(getEffectiveDate());
        rulesEngineDomain.getRequest().setTimeInMs(new Date().getTime());

        // Use the Cost Estimator Settings output as input to the Calculator Rule
        rulesEngineDataRequest.setDomain(rulesEngineDomain);

        DataProvider<ShoppingDomainModel> benefitsDataProvider = new CamelRequestingDataProvider<ShoppingDomainModel>(rulesEngineDataRequest);
        ShoppingDomainModel benefitsRulesEngineResult = getCachedData(ShoppingDataProviderKey.COST_ESTIMATOR_KEYS, benefitsDataProvider);

        Map<String, String> benefitKeyMap = createBenefitKeyMap(benefitsRulesEngineResult.getRequest());
        Map<String, String> propertyKeyMap = createPropertyKeyMap(benefitsRulesEngineResult.getRequest());

        ProductPlanData productPlanData = getPlanData();

        planAdvisorConfigRequest.setGroupId(planAdvisorGroup);
        planAdvisorConfigRequest.setEffectiveDate(getEffectiveDateMS());

        DataProvider<PlanAdvisorData> dataProvider = new CamelRequestingDataProvider<PlanAdvisorData>(planAdvisorConfigRequest);

        planAdvisorData = getCachedData(ShoppingDataProviderKey.PLAN_ADVISOR, dataProvider);

        rulesEngineDataRequest.setGroupId(rulesEngineGroupId);
        ruleCodes = new ArrayList<String>();
        ruleCodes.add(costEstimatorCostsRuleSet);
        rulesEngineDataRequest.setRuleCodes(ruleCodes);
        rulesEngineDataRequest.setEffDate(getEffectiveDateMS());

        rulesEngineDomain.getRequest().setId("2");
        rulesEngineDomain.getRequest().setTimeInMs(new Date().getTime());

        List<Consumer> consumers = new ArrayList<Consumer>();

        Map<String, Consumer> consumerByMemberRefName = new HashMap<String, Consumer>();

        List<Object[]> nameConsumerPairs = getNameConsumerPairsFromMembers();

        for (Object[] pair : nameConsumerPairs)
        {
          consumers.add((Consumer)pair[1]);
          consumerByMemberRefName.put((String)pair[0], (Consumer)pair[1]);
        }

        rulesEngineDomain.getRequest().setConsumer(consumers);

        // Now setup the profile key values
        if (getAccountObject().has("questions"))
        {
          String costEstimatorQuestionRefId = null;

          // find the CostEstimator question
          for (QuestionData questionData : planAdvisorData.getQuestions())
          {
            if ("CostEstimator".equals(questionData.getWidgetId()))
            {
              costEstimatorQuestionRefId = questionData.getQuestionRefId();
            }
          }

          if (null != costEstimatorQuestionRefId)
          {
            JSONObject questionObject = null;

            // Now get the costEstimator question responses
            JSONArray questionArray = getAccountObject().getJSONArray("questions");
            for (int q = 0; q < questionArray.length(); q++)
            {
              JSONObject potentialQuestionObject = questionArray.getJSONObject(q);
              // If the questionRefId matches we found the costEstimator
              // question responses
              if (costEstimatorQuestionRefId.equals(potentialQuestionObject
                .getString("questionRefId")))
              {
                questionObject = potentialQuestionObject;
              }
            }

            // If we have answers for the CostEstimator
            if (null != questionObject)
            {
              // Create ConsumerProfile objects for each member and set of
              // UsageData
              // and store it with the corresponding Consumer
              if (isNotNullAndNotJSONNull(questionObject, "members"))
              {
                JSONArray membersArray = questionObject.getJSONArray("members");
                for (int m = 0; m < membersArray.length(); m++)
                {
                  JSONObject memberObject = membersArray.getJSONObject(m);

                  String memberRefName = memberObject.getString("memberRefName");
                  // Store it with the correct Consumer
                  Consumer consumer = consumerByMemberRefName.get(memberRefName);

                  // Only supply values for consumers that have coverage
                  if (null != consumer)
                  {
                    List<ConsumerProfile> consumerProfiles = new ArrayList<ConsumerProfile>();

                    if (isNotNullAndNotJSONNull(memberObject, "value"))
                    {
                      JSONObject valueObject = memberObject.getJSONObject("value");
                      if (isNotNullAndNotJSONNull(valueObject, "usageData"))
                      {
                        JSONArray usageDataArray = valueObject.getJSONArray("usageData");
                        for (int u = 0; u < usageDataArray.length(); u++)
                        {
                          JSONObject usageDataObject = usageDataArray.getJSONObject(u);
                          // Create a ConsumerProfile from the UsageData
                          ConsumerProfile consumerProfile = new ConsumerProfile();
                          consumerProfile.setKey(usageDataObject.getString("key"));

                          if (isNotNullAndNotJSONNull(usageDataObject, "value"))
                          {
                            consumerProfile.setValue(usageDataObject.getInt("value"));
                            consumerProfiles.add(consumerProfile);
                          }
                        }
                      }

                      if (isNotNullAndNotJSONNull(valueObject, "profileId"))
                      {
                        consumer.setProfileId(valueObject.getString("profileId"));
                      }

                      consumer.setProfile(consumerProfiles);
                    }
                  }
                }
              }
            }
          }
        }

        List<Product> products = new ArrayList<Product>();

        String productLineForEstimation = getProductLineForEstimation();

        // Populate Product Attributes
        for (PlanData planData : productPlanData.getPlans())
        {
          if (productLineForEstimation.equals(planData.getProductData().getProductLineCode())) {
            Product product = new Product();

            product.setXid(getPlanIdentifier(planData));
            product.setTierCode(planData.getTierCode());

            List<Attribute> attributes = new ArrayList<Attribute>();

            for (PlanBenefitCategoryData categoryData : planData.getBenefitCategories())
            {
              for (PlanBenefitData benefitData : categoryData.getBenefits())
              {
                String benefitInternalCode = benefitData.getInternalCode();

                String selectedValue = benefitData.getSelectedValue();
                PlanBenefitValueSetData benefitValueSetData = benefitData.getBenefitValueSetMap()
                  .get(selectedValue);
                Map<String, PlanBenefitValueData> benefitValueMap = benefitValueSetData
                  .getBenefitValueMap();

                List<AttributeProperty> baseAttributeProperties = new ArrayList<AttributeProperty>();

                for (CustomAttributeData attributeData : benefitData.getCustomAttributes())
                {
                  // Only add the properties that are configured by the
                  //   COST_ESTIMATOR_BENEFIT_KEY_LIST rule
                  if (propertyKeyMap.values().contains(attributeData.getCode())) {
                    AttributeProperty attributeProperty = new AttributeProperty();
                    attributeProperty.setKey(attributeData.getCode());
                    attributeProperty.setValue(attributeData.getAttributeValue());
                    baseAttributeProperties.add(attributeProperty);
                  }
                }

                List<String> benefitValueKeys = benefitValueSetData.getBenefitValueKeys();
                for (String valueKey : benefitValueKeys)
                {
                  PlanBenefitValueData planBenefit = benefitValueMap.get(valueKey);

                  String attributeCode = planData.getProductData().getProductLineCode() + "." + benefitInternalCode + "."
                    + valueKey;

                  // Only add the benefits that are configured by the
                  //   COST_ESTIMATOR_BENEFIT_KEY_LIST rule
                  if (benefitKeyMap.values().contains(attributeCode))
                  {
                    Double attributeValue = planBenefit.getNumericValue();

                    // Only add the property if the value is set
                    if (null != attributeValue)
                    {
                      Attribute attribute = new Attribute();
                      attribute.setKey(attributeCode);
                      attribute.setValue(attributeValue);

                      List<AttributeProperty> attributeProperties = new ArrayList<AttributeProperty>(baseAttributeProperties);

                      for (CustomAttributeData attributeData : planBenefit.getCustomAttributes()) {
                        if (propertyKeyMap.values().contains(attributeData.getCode())) {
                          AttributeProperty attributeProperty = new AttributeProperty();
                          attributeProperty.setKey(attributeData.getCode());
                          attributeProperty.setValue(attributeData.getAttributeValue());
                          attributeProperties.add(attributeProperty);
                        }
                      }

                      attribute.setProperty(attributeProperties);

                      attributes.add(attribute);
                    }
                  }
                }
              }
            }

            product.setAttribute(attributes);

            products.add(product);
          }
        }

        rulesEngineDomain.getRequest().setProduct(products);

        rulesEngineDataRequest.setDomain(rulesEngineDomain);

        if (LOG.isDebugEnabled())
        {
          LOG.debug("Request is " + new JSONObject(rulesEngineDomain).toString(4));
        }

        DataProvider<ShoppingDomainModel> costsDataProvider = new CamelRequestingDataProvider<ShoppingDomainModel>(rulesEngineDataRequest);
        rulesEngineDomain = getCachedData(ShoppingDataProviderKey.COST_ESTIMATOR_COSTS, costsDataProvider);

        rulesEngineDomain = rulesEngineDataRequest.submitRequest();
      }
    }
    else
    {
      throw new Exception("No valid account or demographics found for " + transactionId);
    }
  }

  protected abstract String getPlanIdentifier(PlanData planData);

  List<Object[]> getNameConsumerPairsFromMembers() throws JSONException, Exception
  {
    List<Object[]> nameConsumerPairs = new ArrayList<Object[]>();

    JSONArray accountMembersArray = getAccountObject().getJSONArray("members");
    for (int m = 0; m < accountMembersArray.length(); m++)
    {
      JSONObject memberObject = accountMembersArray.getJSONObject(m);

      if (isApplicableForCostEstimation(memberObject))
      {
        Object[] nameConsumerPair = new Object[2];

        Consumer consumer = new Consumer();
        nameConsumerPair[1] = consumer;

        if (isNotNullAndNotJSONNull(memberObject, "memberRefName"))
        {
          String memberRefName = memberObject.getString("memberRefName");
          consumer.setXid(memberRefName);

          nameConsumerPair[0] = memberRefName;
        }

        if (isNotNullAndNotJSONNull(memberObject, "gender"))
        {
          consumer.setGender(memberObject.getString("gender"));
        }

        populateMemberRelationshipcode(consumer, memberObject);

        if (isNotNullAndNotJSONNull(memberObject, "birthDate"))
        {
          consumer.setDateOfBirth(memberObject.getString("birthDate"));
        }

        populateMemberZipCode(consumer, memberObject);

        nameConsumerPairs.add(nameConsumerPair);
      }
    }

    return nameConsumerPairs;
  }

  protected abstract boolean isApplicableForCostEstimation(JSONObject memberObject) throws JSONException, Exception;

  protected boolean validDemographicsForProcessing(JSONObject accountJSON) throws JSONException
  {
    boolean isValidDemographicsForProcessing = null != accountJSON;

    if(isValidDemographicsForProcessing)
    {
      //check if we have a zip code
      isValidDemographicsForProcessing = validDemographicsZipCode(accountJSON);

      if(isValidDemographicsForProcessing)
      {
        //check members for what we need
        JSONArray membersJSON = null;
        if(isNotNullAndNotJSONNull(accountJSON, "members"))
        {
          membersJSON = accountJSON.getJSONArray("members");
        }

        isValidDemographicsForProcessing = null != membersJSON;

        for (int i = 0; isValidDemographicsForProcessing && i < membersJSON.length(); i++)
        {
          JSONObject memberJSON = membersJSON.getJSONObject(i);

          isValidDemographicsForProcessing &= null != memberJSON.optString("birthDate", null);

          isValidDemographicsForProcessing &= null != memberJSON.optString("gender", null);
        }
      }
    }

    return isValidDemographicsForProcessing;
  }

  protected boolean validDemographicsZipCode(JSONObject accountJSON) throws JSONException
  {
    boolean validDemographicsZipCode = false;

    JSONArray membersJSON = null;
    if (isNotNullAndNotJSONNull(accountJSON, "members"))
    {
      membersJSON = accountJSON.getJSONArray("members");
    }

    for (int i = 0; !validDemographicsZipCode && i < membersJSON.length(); i++)
    {
      JSONObject memberJSON = membersJSON.getJSONObject(i);

      validDemographicsZipCode = isNotNullAndNotJSONNull(memberJSON, "zipCode");
    }

    return validDemographicsZipCode;
  }

  protected void populateMemberRelationshipcode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(memberObject, "memberRelationship"))
    {
      consumer.setRelationshipCode(memberObject.getString("memberRelationship"));
    }
  }

  protected void populateMemberZipCode(Consumer consumer, JSONObject memberObject) throws JSONException
  {
    if (isNotNullAndNotJSONNull(memberObject, "zipCode"))
    {
      consumer.setZip(memberObject.getString("zipCode"));
    }
  }

  private ShoppingDomainModel getCostEstimatorSettings() throws Exception
  {
    ShoppingDomainModel costEstimatorSettings = null;

    costEstimatorSettingsDataProvider.setRuleSet(costEstimatorSettingsRuleSet);
    costEstimatorSettingsDataProvider.setRulesEngineGroupId(rulesEngineGroupId);
    costEstimatorSettingsDataProvider.setRequest(rulesEngineDataRequest);

    // Start with a new Rules Engine Domain
    ShoppingDomainModel domainModel = new ShoppingDomainModel();
    costEstimatorSettingsDataProvider.setRulesEngineDomain(domainModel);
    costEstimatorSettingsDataProvider.setTransactionId(transactionId);

    // This has the side effect of setting the EffectiveDateMs on the rules engine Request object
    costEstimatorSettingsDataProvider.setEffectiveDateMs(getEffectiveDateMS());
    costEstimatorSettingsDataProvider.setAccount(getAccountObject());

    costEstimatorSettings = getCachedData(ShoppingDataProviderKey.COST_ESTIMATOR_SETTINGS,
      costEstimatorSettingsDataProvider);

    return costEstimatorSettings;
  }

  protected String getProductLineForEstimation() throws JSONException, Exception
  {
    String auxShoppingConfigData = getCachedData(ShoppingDataProviderKey.AUX_SHOPPING_CONFIG,
      new AuxShoppingConfigDataProvider());

    JSONObject auxShoppingConfigDataObject = new JSONObject(auxShoppingConfigData);

    if (!auxShoppingConfigDataObject.has("widgets")) {
      throw new Exception("Missing config property: widgets");
    }

    JSONArray widgetArray = auxShoppingConfigDataObject.getJSONArray("widgets");
    JSONObject costEstimatorWidgetObject = null;
    for (int i = 0; i < widgetArray.length() && null == costEstimatorWidgetObject; i++) {
      JSONObject widgetObject = widgetArray.getJSONObject(i);
      if (!widgetObject.has("widgetId")) {
        throw new Exception("Missing config property: widgets[" + i + "].widgetId");
      }
      if ("CostEstimator".equals(widgetObject.getString("widgetId"))) {
        costEstimatorWidgetObject = widgetObject;
      }
    }

    if (null == costEstimatorWidgetObject || !costEstimatorWidgetObject.has("config")) {
      throw new Exception("CostEstimator widget config missing property: config");
    }

    JSONObject configObject = costEstimatorWidgetObject.getJSONObject("config");
    if (!configObject.has("productLineForEstimation")) {
      throw new Exception("CostEstimator config missing property: productLineForEstimation");
    }

    return configObject.getString("productLineForEstimation");
  }

  protected JSONObject getAccountObject()
  {
    return getAccount().getAccount(getTransactionId());
  }

  protected abstract String getEffectiveDate();

  protected abstract Long getEffectiveDateMS();

  protected abstract ProductPlanData getPlanData()
    throws JSONException, ParseException, Exception;

  private Map<String, String> createBenefitKeyMap(Request rulesEngineResult)
  {
    Map<String, String> referenceBenefitKeyMap = new HashMap<String, String>();

    for (Benefit benefit : rulesEngineResult.getBenefit())
    {
      referenceBenefitKeyMap.put(benefit.getKey(), benefit.getCode());
    }

    return referenceBenefitKeyMap;
  }

  private Map<String, String> createPropertyKeyMap(Request rulesEngineResult)
  {
    Map<String, String> referenceBenefitKeyMap = new HashMap<String, String>();

    for (Property property : rulesEngineResult.getProperty())
    {
      referenceBenefitKeyMap.put(property.getKey(), property.getCode());
    }

    return referenceBenefitKeyMap;
  }

  public static Boolean isNotNullAndNotJSONNull(JSONObject object, String key) throws JSONException
  {
    return object.has(key) && !JSONObject.NULL.equals(object.get(key));
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject jsonObject = new JSONObject();

    JSONObject costEstimatorCostsObject = new JSONObject();

    // If there was no AccountInfo yet there won't be a RulesEngine result
    // either
    if (null != rulesEngineDomain)
    {
      if (StringUtils.isBlank(rulesEngineDomain.getRequest().getError()))
      {
        JSONArray productCostsArray = new JSONArray();
        for (Product product : rulesEngineDomain.getRequest().getProduct())
        {
          productCostsArray.put(new JSONObject(product));
        }
        costEstimatorCostsObject.put("product", productCostsArray);
      }
      else
      {
        LOG.error("Unable to obtain estimated costs from RulesEngine. Cause: "
          + rulesEngineDomain.getRequest().getError());
      }
    }

    jsonObject.put("costEstimatorCosts", costEstimatorCostsObject);

    if (LOG.isDebugEnabled())
    {
      LOG.debug("CostEstimatorCosts output:\n" + jsonObject.toString(4));
    }

    return jsonObject;
  }

  public String getTransactionId()
  {
    return transactionId;
  }

  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }

  public void setRulesEngineGroupId(String rulesEngineGroupId)
  {
    this.rulesEngineGroupId = rulesEngineGroupId;
  }

  public void setPlanAdvisorConfigRequest(PlanAdvisorConfigRequest planAdvisorConfigRequest)
  {
    this.planAdvisorConfigRequest = planAdvisorConfigRequest;
  }

  public void setRulesEngineDataRequest(ShoppingRulesEngineDataRequest rulesEngineDataRequest)
  {
    this.rulesEngineDataRequest = rulesEngineDataRequest;
  }

  public void setPlanAdvisorGroup(String planAdvisorGroup)
  {
    this.planAdvisorGroup = planAdvisorGroup;
  }

  public void setCostEstimatorBenefitKeyRuleSet(String costEstimatorBenefitKeyRuleSet)
  {
    this.costEstimatorBenefitKeyRuleSet = costEstimatorBenefitKeyRuleSet;
  }

  public void setCostEstimatorSettingsRuleSet(String costEstimatorSettingsRuleSet)
  {
    this.costEstimatorSettingsRuleSet = costEstimatorSettingsRuleSet;
  }

  public void setCostEstimatorSettingsDataProvider(
    CostEstimatorSettingsDataProvider costEstimatorSettingsDataProvider)
  {
    this.costEstimatorSettingsDataProvider = costEstimatorSettingsDataProvider;
  }

  public void setCostEstimatorCostsRuleSet(String costEstimatorCostsRuleSet)
  {
    this.costEstimatorCostsRuleSet = costEstimatorCostsRuleSet;
  }
}