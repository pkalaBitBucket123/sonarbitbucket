package com.connecture.shopping.process.service.data.converter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.integration.data.MemberTypeEnum;
import com.connecture.shopping.process.service.data.PlanData;
import com.connecture.shopping.process.service.data.ProductPlanData;

public class RatingPlanDataConverter extends ProductDataConverter
{
  private static Logger LOG = Logger.getLogger(RatingPlanDataConverter.class);

  @Override
  public ProductPlanData convertData(String data) throws Exception
  {
    // This implementation presumes the data is a json string
    // Based on input, it looks like it is a JSONArray of objects.
    JSONArray planObjects = new JSONArray(data);

    if (LOG.isDebugEnabled())
    {
      LOG.debug("RatingPlanDataConverter - input data " + planObjects.toString(4));
    }

    return super.convertData(data);
  }

  protected PlanData createPlanData(JSONObject planObject) throws JSONException
  {
    PlanData planData = super.createPlanData(planObject);

    planData.setRate(planObject.getDouble("rate"));

    // Extract member level rates, if they exist
    JSONObject memberRateObject = planObject.optJSONObject("memberRates");
    if (null != memberRateObject)
    {
      Map<String, String> memberRates = new HashMap<String, String>();
      @SuppressWarnings("unchecked")
      Iterator<String> iter = (Iterator<String>) memberRateObject.keys();
      for (; iter.hasNext();)
      {
        String key = iter.next();
        memberRates.put(key, memberRateObject.optString(key));
      }

      planData.setMemberLevelRates(memberRates);
    }

    // get tobacco surcharge data (optional)
    planData.setTobaccoSurcharge(planObject.optDouble("tobaccoSurcharge", 0.0));

    JSONObject memberTobaccoSurchargesJSON = planObject.optJSONObject("memberTobaccoSurcharges");
    if (memberTobaccoSurchargesJSON != null)
    {
      Map<String, String> memberTobaccoSurcharges = new HashMap<String, String>();
      @SuppressWarnings("unchecked")
      Iterator<String> iter = (Iterator<String>) memberTobaccoSurchargesJSON.keys();
      for (; iter.hasNext();)
      {
        String key = iter.next();
        memberTobaccoSurcharges.put(key, memberTobaccoSurchargesJSON.optString(key));
      }

      planData.setMemberLevelTobaccoSurcharges(memberTobaccoSurcharges);
    }

    return planData;
  }

  public static JSONArray buildMemberLevelRateJSON(PlanData planData) throws JSONException
  {
    return buildMemberLevelRateJSON(planData, FrequencyCode.MONTHLY, null);
  }

  public static JSONArray buildMemberLevelRateJSON(
    PlanData planData,
    FrequencyCode frequencyCode,
    Integer paychecksPerYear) throws JSONException
  {
    JSONArray memberRatesJSON = new JSONArray();

    Map<String, String> memberRates = planData.getMemberLevelRates();
    Map<String, String> memberTobaccoSurcharges = planData.getMemberLevelTobaccoSurcharges();

    if (null != memberRates)
    {
      for (Map.Entry<String, String> entry : memberRates.entrySet())
      {
        // amount: the rate with taxes, fees, surcharges
        String amount = entry.getValue();
        
        // tobacco surcharge; only include this if it exists, for now
        String tobaccoSurcharge = "";
        if (memberTobaccoSurcharges != null)
        {
          tobaccoSurcharge = memberTobaccoSurcharges.get(entry.getKey());
        }
        
        // note : as of now (8/18/13) there is no such thing as paycheck
        // frequency rating anyway for the context in which this is used (IFP)
        if (FrequencyCode.PAYCHECK.equals(frequencyCode))
        {
          // check if the amount is numeric, and if so do a perPaycheck
          // calculation
          String numericRegEx = "\\d+(\\.\\d+)?";
          if (amount.matches(numericRegEx))
          {
            amount = Double.toString((Double.parseDouble(amount) * 12) / paychecksPerYear);
          }
          
          // check tobacco surcharge; do paycheck calculation if numeric        
          if (tobaccoSurcharge.matches(numericRegEx))
          {
            tobaccoSurcharge = Double.toString((Double.parseDouble(tobaccoSurcharge) * 12) / paychecksPerYear);
          }
        }

        JSONObject memberRate = new JSONObject();
        memberRate.put("memberId", entry.getKey());
        memberRate.put("amount", amount);
        if (!StringUtils.isBlank(tobaccoSurcharge))
        {
          memberRate.put("tobaccoSurcharge", tobaccoSurcharge);
        }

        memberRatesJSON.put(memberRate);
      }
    }

    return memberRatesJSON;
  }

  public static JSONObject buildMemberTypeValueJSON(
    Map<MemberTypeEnum, Double> memberTypeValues,
    FrequencyCode frequencyCode,
    Integer paychecksPerYear) throws JSONException
  {
    JSONObject memberTypeValueObject = null;

    if (null != memberTypeValues)
    {
      memberTypeValueObject = new JSONObject();

      for (Map.Entry<MemberTypeEnum, Double> entry : memberTypeValues.entrySet())
      {
        Double amount = entry.getValue();
        if (FrequencyCode.PAYCHECK.equals(frequencyCode))
        {
          amount = (amount * 12) / paychecksPerYear;
        }
        memberTypeValueObject.put(entry.getKey().getKey(), amount);
      }
    }

    return memberTypeValueObject;
  }
}