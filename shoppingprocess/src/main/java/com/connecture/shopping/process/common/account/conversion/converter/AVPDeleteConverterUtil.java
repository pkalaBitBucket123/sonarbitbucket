package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;

public class AVPDeleteConverterUtil implements AVPConverterUtil<AVPDeleteConverter>
{
  @Override
  public List<String> validateJSON(JSONObject converterObject) throws JSONException
  {
    List<String> errors = new ArrayList<String>();

    JSONValidationUtils.checkForMissingField(errors, converterObject, "path");
    JSONValidationUtils.checkForMissingField(errors, converterObject, "reason");
    
    return errors;
  }

  @Override
  public AVPDeleteConverter createConverter(JSONObject converterObject) throws JSONException
  {
    AVPDeleteConverter converter = new AVPDeleteConverter();
    
    converter.setPath(converterObject.getString("path"));
    converter.setReason(converterObject.getString("reason"));
    
    return converter;
  }
}
