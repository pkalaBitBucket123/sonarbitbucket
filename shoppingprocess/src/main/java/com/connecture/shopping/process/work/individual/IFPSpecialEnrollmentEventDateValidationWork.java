package com.connecture.shopping.process.work.individual;

import org.json.JSONObject;

import com.connecture.shopping.process.service.data.EnrollmentEventApplication;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentData;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.EnrollmentEventRulesEngineBean;

public class IFPSpecialEnrollmentEventDateValidationWork extends WorkImpl
{
  // Start Required Inputs
  private EnrollmentEventRulesEngineBean hcrRulesEngineBean;
  private String eventReason;
  private String eventDate;
  // End Required Inputs

  private EnrollmentEventDomainModel specialEnrollmentDomain;
  
  @Override
  public void get() throws Exception
  {
    EnrollmentEventDomainModel domainModel = new EnrollmentEventDomainModel();
    EnrollmentEventApplication application = new EnrollmentEventApplication();
    SpecialEnrollmentData specialEnrollment = new SpecialEnrollmentData();
    specialEnrollment.setSelectedSpecialEnrollmentReason(eventReason);
    specialEnrollment.setEventDate(eventDate);
    application.setSpecialEnrollment(specialEnrollment);
    domainModel.setApplication(application);
    
    hcrRulesEngineBean.setDomainModel(domainModel);
    hcrRulesEngineBean.setDataCache(getDataCache());
    specialEnrollmentDomain = hcrRulesEngineBean.executeRule();
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject results = new JSONObject();
    
    JSONObject specialEnrollmentEventDateValidation = new JSONObject();
    String withinEnrollmentWindow = specialEnrollmentDomain.getApplication().getSpecialEnrollment().getWithinEnrollmentWindow();
    specialEnrollmentEventDateValidation.put("withinEnrollmentWindow", withinEnrollmentWindow);
    results.put("specialEnrollmentEventDateValidation", specialEnrollmentEventDateValidation);
    
    return results;
  }
  
  public void setEventReason(String eventReason)
  {
    this.eventReason = eventReason;
  }

  public void setEventDate(String eventDate)
  {
    this.eventDate = eventDate;
  }

  public void setHcrRulesEngineBean(EnrollmentEventRulesEngineBean hcrRulesEngineBean)
  {
    this.hcrRulesEngineBean = hcrRulesEngineBean;
  }
}
