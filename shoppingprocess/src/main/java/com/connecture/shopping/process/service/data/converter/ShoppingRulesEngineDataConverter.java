package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.Attribute;
import com.connecture.shopping.process.service.data.AttributeConsumer;
import com.connecture.shopping.process.service.data.Benefit;
import com.connecture.shopping.process.service.data.Consumer;
import com.connecture.shopping.process.service.data.ConsumerProfile;
import com.connecture.shopping.process.service.data.ConsumerUsage;
import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.Product;
import com.connecture.shopping.process.service.data.ProductConsumer;
import com.connecture.shopping.process.service.data.Profile;
import com.connecture.shopping.process.service.data.Property;
import com.connecture.shopping.process.service.data.Request;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.UsageCategory;
import com.connecture.shopping.process.service.data.UsageData;
import com.connecture.shopping.process.service.data.availability.Availability;
import com.connecture.shopping.process.service.data.availability.converter.AvailabilityProductConverterUtils;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsForm;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldMask;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormFieldOption;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormValidationError;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormValidationErrorConfig;

public class ShoppingRulesEngineDataConverter implements DataConverter<String, ShoppingDomainModel>
{
  private static Logger LOG = Logger.getLogger(ShoppingRulesEngineDataConverter.class);

  @Override
  public ShoppingDomainModel convertData(String data) throws Exception
  {
    ShoppingDomainModel domainModel = new ShoppingDomainModel();

    Request request = domainModel.getRequest();

    try
    {
      JSONObject domainObject = new JSONObject((String) data);

      if (LOG.isDebugEnabled())
      {
        LOG.debug("Got " + domainObject.toString(4));
      }

      if (!domainObject.has("request"))
      {
        throw new JSONException("Failed parsing rules engine result JSON, missing \"request\" node");
      }

      JSONObject requestObject = domainObject.getJSONObject("request");

      request.setId(requestObject.optString("id"));
      request.setTimeInMs(requestObject.optLong("timeInMs"));
      request.setTimeOutMs(requestObject.optLong("timeOutMs"));

      if (requestObject.has("error"))
      {
        request.setError(requestObject.getString("error"));
      }
      if (requestObject.has("consumer"))
      {
        request.setConsumer(createConsumer(requestObject.getJSONArray("consumer")));
      }
      if (requestObject.has("product"))
      {
        request.setProduct(createProduct(requestObject.getJSONArray("product")));
      }
      if (requestObject.has("profile"))
      {
        request.setProfile(createProfile(requestObject.getJSONArray("profile")));
      }
      if (requestObject.has("benefit"))
      {
        request.setBenefit(createBenefit(requestObject.getJSONArray("benefit")));
      }
      if (requestObject.has("property"))
      {
        request.setProperty(createProperty(requestObject.getJSONArray("property")));
      }
      if (requestObject.has("demographicsForm"))
      {
        request.setDemographicsForm(createDemographicsForm(requestObject
          .getJSONObject("demographicsForm")));
      }
      if (requestObject.has("availability"))
      {
        request.setAvailability(createAvailability(requestObject.getJSONObject("availability")));
      }
      
    }
    catch (JSONException e)
    {
      throw new Exception("Failed parsing rules engine result JSON.", e);
    }

    return domainModel;
  }

  Availability createAvailability(JSONObject availabilityJSON) throws JSONException
  {
    // TODO | move to a separate converter class
    Availability availability = new Availability();
    availability.setSelectedProducts(AvailabilityProductConverterUtils.fromJSON(availabilityJSON.optJSONArray("selectedProducts")));
    availability.setUnselectedProducts(AvailabilityProductConverterUtils.fromJSON(availabilityJSON.optJSONArray("unselectedProducts")));
    return availability;
  }

  DemographicsForm createDemographicsForm(JSONObject demographicsFormJSON) throws JSONException
  {
    DemographicsForm demographicsForm = new DemographicsForm();
    List<DemographicsFormField> fields = new ArrayList<DemographicsFormField>();
    List<DemographicsFormValidationErrorConfig> validationErrorConfigurations = new ArrayList<DemographicsFormValidationErrorConfig>();
    List<DemographicsFormValidationError> validationErrors = new ArrayList<DemographicsFormValidationError>();

    // hasErrors
    Boolean hasErrors = demographicsFormJSON.optBoolean("hasErrors");
    demographicsForm.setHasErrors(hasErrors);

    // form field configurations
    JSONArray demographicsFields = demographicsFormJSON.optJSONArray("field");
    if (demographicsFields != null && demographicsFields.length() > 0)
    {
      for (int i = 0; i < demographicsFields.length(); i++)
      {
        JSONObject demographicsField = demographicsFields.getJSONObject(i);
        fields.add(createDemographicsFormField(demographicsField));
      }
      demographicsForm.setField(fields);
    }

    // member (input only)

    // validation error configurations
    JSONArray validationErrorConfigsJSON = demographicsFormJSON
      .optJSONArray("validationErrorConfig");
    if (validationErrorConfigsJSON != null && validationErrorConfigsJSON.length() > 0)
    {
      for (int i = 0; i < validationErrorConfigsJSON.length(); i++)
      {
        JSONObject validationErrorConfig = validationErrorConfigsJSON.getJSONObject(i);
        validationErrorConfigurations
          .add(createValidationErrorConfiguration(validationErrorConfig));
      }
      demographicsForm.setValidationErrorConfig(validationErrorConfigurations);
    }

    // validation errors
    JSONArray validationErrorsJSON = demographicsFormJSON.optJSONArray("validationError");
    if (validationErrorsJSON != null && validationErrorsJSON.length() > 0)
    {
      for (int i = 0; i < validationErrorsJSON.length(); i++)
      {
        JSONObject validationErrorJSON = validationErrorsJSON.getJSONObject(i);
        validationErrors.add(createValidationError(validationErrorJSON));
      }
      demographicsForm.setValidationError(validationErrors);
    }

    return demographicsForm;
  }

  DemographicsFormField createDemographicsFormField(JSONObject fieldJSON) throws JSONException
  {
    DemographicsFormField formField = new DemographicsFormField();

    String fieldKey = fieldJSON.getString("key");
    String fieldName = fieldJSON.getString("name");
    String fieldDataType = fieldJSON.getString("dataType");
    String fieldStyleClass = fieldJSON.getString("styleClass");
    String fieldHeaderStyleClass = fieldJSON.getString("headerStyleClass");
    String fieldInputType = fieldJSON.getString("inputType");
    Integer fieldSize = fieldJSON.getInt("size");
    Integer fieldOrder = fieldJSON.getInt("order");

    JSONArray fieldOptions = fieldJSON.optJSONArray("option");

    formField.setKey(fieldKey);
    formField.setName(fieldName);
    formField.setLabel(fieldJSON.getString("label"));
    formField.setDataType(fieldDataType);
    formField.setStyleClass(fieldStyleClass);
    formField.setHeaderStyleClass(fieldHeaderStyleClass);
    formField.setInputType(fieldInputType);
    formField.setSize(fieldSize);
    formField.setOrder(fieldOrder);

    formField.setOption(createFormFieldOptions(fieldOptions));

    JSONObject fieldMask = fieldJSON.optJSONObject("mask");
    if (fieldMask != null)
    {
      formField.setMask(createFormFieldMask(fieldMask));
    }
    return formField;
  }

  List<DemographicsFormFieldOption> createFormFieldOptions(JSONArray fieldOptionsJSON)
    throws JSONException
  {
    List<DemographicsFormFieldOption> options = new ArrayList<DemographicsFormFieldOption>();

    if (fieldOptionsJSON != null && fieldOptionsJSON.length() > 0)
    {
      for (int i = 0; i < fieldOptionsJSON.length(); i++)
      {
        JSONObject fieldOptionJSON = fieldOptionsJSON.getJSONObject(i);
        options.add(this.createFormFieldOption(fieldOptionJSON));
      }
    }
    return options;
  }

  DemographicsFormFieldOption createFormFieldOption(JSONObject fieldOptionJSON)
    throws JSONException
  {
    String fieldDataGroupType = fieldOptionJSON.getString("dataGroupType");
    Boolean fieldDefaultSelection = "Y".equalsIgnoreCase(fieldOptionJSON
      .optString("defaultSelection"));

    Boolean fieldInitialSelection = "Y".equalsIgnoreCase(fieldOptionJSON
      .optString("initialSelection"));

    String fieldLabel = fieldOptionJSON.getString("label");
    Integer fieldOrder = fieldOptionJSON.getInt("order");
    String fieldValue = fieldOptionJSON.getString("value");

    DemographicsFormFieldOption option = new DemographicsFormFieldOption();
    option.setDataGroupType(fieldDataGroupType);
    option.setDefaultSelection(fieldDefaultSelection);
    option.setInitialSelection(fieldInitialSelection);
    option.setLabel(fieldLabel);
    option.setOrder(fieldOrder);
    option.setValue(fieldValue);
    return option;
  }

  DemographicsFormFieldMask createFormFieldMask(JSONObject fieldMaskJSON) throws JSONException
  {
    DemographicsFormFieldMask maskData = new DemographicsFormFieldMask();

    String fieldMask = fieldMaskJSON.getString("mask");
    String fieldMaskClass = fieldMaskJSON.getString("maskClass");

    maskData.setMask(fieldMask);
    maskData.setMaskClass(fieldMaskClass);
    return maskData;
  }

  DemographicsFormValidationErrorConfig createValidationErrorConfiguration(
    JSONObject errorConfigJSON) throws JSONException
  {
    DemographicsFormValidationErrorConfig validationErrorConfig = new DemographicsFormValidationErrorConfig();

    String keyField = errorConfigJSON.getString("key");
    String labelField = errorConfigJSON.getString("label");

    validationErrorConfig.setKey(keyField);
    validationErrorConfig.setLabel(labelField);

    return validationErrorConfig;
  }

  DemographicsFormValidationError createValidationError(JSONObject errorJSON) throws JSONException
  {
    DemographicsFormValidationError validationError = new DemographicsFormValidationError();

    String keyField = errorJSON.getString("key");
    String labelField = errorJSON.getString("label");

    validationError.setKey(keyField);
    validationError.setLabel(labelField);

    return validationError;
  }

  List<Consumer> createConsumer(JSONArray consumerArray) throws JSONException
  {
    List<Consumer> consumers = new ArrayList<Consumer>();

    for (int c = 0; c < consumerArray.length(); c++)
    {
      Consumer consumer = new Consumer();

      JSONObject consumerObject = consumerArray.getJSONObject(c);

      if (consumerObject.has("xid"))
      {
        consumer.setXid(consumerObject.getString("xid"));
      }
      if (consumerObject.has("dateOfBirth"))
      {
        consumer.setDateOfBirth(consumerObject.getString("dateOfBirth"));
      }
      if (consumerObject.has("age"))
      {
        consumer.setAge(consumerObject.getInt("age"));
      }
      if (consumerObject.has("gender"))
      {
        consumer.setGender(consumerObject.getString("gender"));
      }
      if (consumerObject.has("zip"))
      {
        consumer.setZip(consumerObject.getString("zip"));
      }
      if (consumerObject.has("state"))
      {
        consumer.setState(consumerObject.getString("state"));
      }
      if (consumerObject.has("smoker"))
      {
        consumer.setSmoker(consumerObject.getBoolean("smoker"));
      }
      if (consumerObject.has("profile"))
      {
        consumer.setProfile(createConsumerProfile(consumerObject.getJSONArray("profile")));
      }

      consumers.add(consumer);
    }

    return consumers;
  }

  List<ConsumerProfile> createConsumerProfile(JSONArray profileArray) throws JSONException
  {
    List<ConsumerProfile> consumerProfiles = new ArrayList<ConsumerProfile>();

    for (int p = 0; p < profileArray.length(); p++)
    {
      ConsumerProfile consumerProfile = new ConsumerProfile();
      JSONObject consumerProfileObject = profileArray.getJSONObject(p);
      consumerProfile.setKey(consumerProfileObject.getString("key"));
      consumerProfile.setValue(consumerProfileObject.getInt("value"));
      consumerProfiles.add(consumerProfile);
    }

    return consumerProfiles;
  }

  List<Product> createProduct(JSONArray productArray) throws JSONException
  {
    List<Product> products = new ArrayList<Product>();

    for (int p = 0; p < productArray.length(); p++)
    {
      Product product = new Product();
      JSONObject productObject = productArray.getJSONObject(p);
      product.setXid(productObject.getString("xid"));
      if (productObject.has("tierCode"))
      {
        product.setTierCode(productObject.getString("tierCode"));
      }
      if (productObject.has("cost"))
      {
        product.setCost(productObject.getDouble("cost"));
      }
      if (productObject.has("maxOOPCost"))
      {
        product.setMaxOOPCost(productObject.getDouble("maxOOPCost"));
      }
      if (productObject.has("deductible"))
      {
        product.setDeductible(productObject.getDouble("deductible"));
      }
      if (productObject.has("attribute"))
      {
        product.setAttribute(createAttribute(productObject.getJSONArray("attribute")));
      }
      if (productObject.has("consumer"))
      {
        product.setConsumer(createProductConsumer(productObject.getJSONArray("consumer")));
      }
      products.add(product);
    }

    return products;
  }

  List<Attribute> createAttribute(JSONArray attributeArray) throws JSONException
  {
    List<Attribute> attributes = new ArrayList<Attribute>();

    for (int a = 0; a < attributeArray.length(); a++)
    {
      Attribute attribute = new Attribute();
      JSONObject attributeObject = attributeArray.getJSONObject(a);
      attribute.setKey(attributeObject.getString("key"));
      attribute.setValue(attributeObject.getDouble("value"));

      if (attributeObject.has("consumer"))
      {
        attribute.setConsumer(createAttributeConsumer(attributeObject.getJSONArray("consumer")));
      }
      attributes.add(attribute);
    }

    return attributes;
  }

  List<AttributeConsumer> createAttributeConsumer(JSONArray attributeConsumerArray)
    throws JSONException
  {
    List<AttributeConsumer> attributeConsumers = new ArrayList<AttributeConsumer>();

    for (int ac = 0; ac < attributeConsumerArray.length(); ac++)
    {
      AttributeConsumer attributeConsumer = new AttributeConsumer();
      JSONObject attributeConsumerObject = attributeConsumerArray.getJSONObject(ac);
      attributeConsumer.setXid(attributeConsumerObject.getString("xid"));
      attributeConsumer.setCost(attributeConsumerObject.getInt("cost"));
      attributeConsumers.add(attributeConsumer);
    }

    return attributeConsumers;
  }

  List<ProductConsumer> createProductConsumer(JSONArray productConsumerArray) throws JSONException
  {
    List<ProductConsumer> productConsumers = new ArrayList<ProductConsumer>();

    for (int pc = 0; pc < productConsumerArray.length(); pc++)
    {
      ProductConsumer productConsumer = new ProductConsumer();
      JSONObject productConsumerObject = productConsumerArray.getJSONObject(pc);
      productConsumer.setXid(productConsumerObject.getString("xid"));
      productConsumer.setCost(productConsumerObject.getDouble("cost"));
      productConsumers.add(productConsumer);
    }

    return productConsumers;
  }

  List<Profile> createProfile(JSONArray profileArray) throws JSONException
  {
    List<Profile> profiles = new ArrayList<Profile>();

    for (int p = 0; p < profileArray.length(); p++)
    {
      Profile profile = new Profile();
      JSONObject profileObject = profileArray.getJSONObject(p);
      profile.setId(profileObject.getString("id"));
      profile.setName(profileObject.getString("name"));
      profile.setDescription(profileObject.getString("description"));
      if (profileObject.has("usageCategory"))
      {
        profile.setUsageCategory(createUsageCategory(profileObject.getJSONArray("usageCategory")));
      }

      profiles.add(profile);
    }

    return profiles;
  }

  List<UsageCategory> createUsageCategory(JSONArray usageCategoryArray) throws JSONException
  {
    List<UsageCategory> usageCategories = new ArrayList<UsageCategory>();

    for (int uc = 0; uc < usageCategoryArray.length(); uc++)
    {
      UsageCategory usageCategory = new UsageCategory();
      JSONObject usageCategoryObject = usageCategoryArray.getJSONObject(uc);
      usageCategory.setCategoryId(usageCategoryObject.getString("categoryId"));
      usageCategory.setDisplay(usageCategoryObject.getString("display"));
      if (usageCategoryObject.has("usageData"))
      {
        usageCategory.setUsageData(createUsageData(usageCategoryObject.getJSONArray("usageData")));
      }
      usageCategories.add(usageCategory);
    }

    return usageCategories;
  }

  List<UsageData> createUsageData(JSONArray usageDataArray) throws JSONException
  {
    List<UsageData> usageDataList = new ArrayList<UsageData>();

    for (int uc = 0; uc < usageDataArray.length(); uc++)
    {
      UsageData usageData = new UsageData();
      JSONObject usageDataObject = usageDataArray.getJSONObject(uc);
      usageData.setKey(usageDataObject.getString("key"));
      usageData.setDisplay(usageDataObject.getString("display"));
      if (usageDataObject.has("consumer"))
      {
        usageData.setConsumer(createConsumerUsage(usageDataObject.getJSONArray("consumer")));
      }
      usageDataList.add(usageData);
    }

    return usageDataList;
  }

  List<ConsumerUsage> createConsumerUsage(JSONArray consumerUsageArray) throws JSONException
  {
    List<ConsumerUsage> consumerUsageList = new ArrayList<ConsumerUsage>();

    for (int cu = 0; cu < consumerUsageArray.length(); cu++)
    {
      JSONObject consumerUsageObject = consumerUsageArray.getJSONObject(cu);
      if (isNotNullAndNotJSONNull(consumerUsageObject, "value"))
      {
        ConsumerUsage consumerUsage = new ConsumerUsage();
        consumerUsage.setValue(consumerUsageObject.getDouble("value"));
        consumerUsage.setXid(consumerUsageObject.getString("xid"));
        consumerUsageList.add(consumerUsage);
      }
    }

    return consumerUsageList;
  }

  List<Benefit> createBenefit(JSONArray benefitArray) throws JSONException
  {
    List<Benefit> benefits = new ArrayList<Benefit>();

    for (int p = 0; p < benefitArray.length(); p++)
    {
      Benefit benefit = new Benefit();
      JSONObject benefitObject = benefitArray.getJSONObject(p);
      benefit.setKey(benefitObject.getString("key"));
      benefit.setCode(benefitObject.getString("code"));

      benefits.add(benefit);
    }

    return benefits;
  }

  List<Property> createProperty(JSONArray propertyArray) throws JSONException
  {
    List<Property> properties = new ArrayList<Property>();

    for (int p = 0; p < propertyArray.length(); p++)
    {
      Property property = new Property();
      JSONObject propertyObject = propertyArray.getJSONObject(p);
      property.setKey(propertyObject.getString("key"));
      property.setCode(propertyObject.getString("code"));

      properties.add(property);
    }

    return properties;
  }

  public static Boolean isNotNullAndNotJSONNull(JSONObject object, String key) throws JSONException
  {
    return object.has(key) && !JSONObject.NULL.equals(object.get(key));
  }
}
