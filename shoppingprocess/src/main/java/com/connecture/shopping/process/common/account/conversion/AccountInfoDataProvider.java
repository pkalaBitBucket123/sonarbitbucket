package com.connecture.shopping.process.common.account.conversion;

import java.util.List;

import com.connecture.shopping.process.domain.AccountInfo;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public abstract class AccountInfoDataProvider implements DataProvider<List<AccountInfo>>
{
  private String version;

  public String getVersion()
  {
    return version;
  }

  public void setVersion(String version)
  {
    this.version = version;
  }
}
