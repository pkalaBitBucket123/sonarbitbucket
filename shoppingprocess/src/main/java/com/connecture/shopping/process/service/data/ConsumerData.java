package com.connecture.shopping.process.service.data;

public class ConsumerData
{
  private ConsumerPlanData planData;
  private ConsumerDemographicsData demographicsData;

  public ConsumerPlanData getPlanData()
  {
    return planData;
  }

  public void setPlanData(ConsumerPlanData planData)
  {
    this.planData = planData;
  }

  public ConsumerDemographicsData getDemographicsData()
  {
    return demographicsData;
  }

  public void setDemographicsData(ConsumerDemographicsData demographicsData)
  {
    this.demographicsData = demographicsData;
  }
}
