package com.connecture.shopping.process.common.account.conversion.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.common.account.conversion.json.JSONValidationUtils;

public class AVPRenameConverterUtil implements AVPConverterUtil<AVPRenameConverter>
{
  @Override
  public List<String> validateJSON(JSONObject converterObject) throws JSONException
  {
    List<String> errors = new ArrayList<String>();

    JSONValidationUtils.checkForMissingField(errors, converterObject, "oldPath");
    JSONValidationUtils.checkForMissingField(errors, converterObject, "newPath");
    JSONValidationUtils.checkForMissingField(errors, converterObject, "reason");

    return errors;
  }

  @Override
  public AVPRenameConverter createConverter(JSONObject converterObject) throws JSONException
  {
    AVPRenameConverter converter = new AVPRenameConverter();

    converter.setOldPath(converterObject.getString("oldPath"));
    converter.setNewPath(converterObject.getString("newPath"));
    converter.setReason(converterObject.getString("reason"));

    return converter;
  }
}
