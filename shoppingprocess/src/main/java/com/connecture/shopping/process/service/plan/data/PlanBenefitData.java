/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

import java.util.List;
import java.util.Map;

/**
 * 
 */
public class PlanBenefitData
{
  private String displayName;
  
  private String internalCode;
  private String externalCode;
  
  // if selected value, we can easily get the value, otherwise we can get the values
  private String selectedValue;
  
  private int sortOrder;
  
  // may need to be an object...
  private PlanBenefitValueTypeGroupData benefitValueTypeGroup;
  
  private Map<String, PlanBenefitValueSetData> benefitValueSetMap;
  
  private List<CustomAttributeData> customAttributes;

  
  // TODO | just for client-server implementation now,
  // but other code should be changed to use this simpler format
  List<PlanBenefitValueData> values;
  
  /**
   * @return the displayName
   */
  public String getDisplayName()
  {
    return displayName;
  }

  /**
   * @param displayName the new value of displayName
   */
  public void setDisplayName(String displayName)
  {
    this.displayName = displayName;
  }

  /**
   * @return the internalCode
   */
  public String getInternalCode()
  {
    return internalCode;
  }

  /**
   * @param internalCode the new value of internalCode
   */
  public void setInternalCode(String internalCode)
  {
    this.internalCode = internalCode;
  }

  /**
   * @return the externalCode
   */
  public String getExternalCode()
  {
    return externalCode;
  }

  /**
   * @param externalCode the new value of externalCode
   */
  public void setExternalCode(String externalCode)
  {
    this.externalCode = externalCode;
  }

  /**
   * @return the selectedValue
   */
  public String getSelectedValue()
  {
    return selectedValue;
  }

  /**
   * @param selectedValue the new value of selectedValue
   */
  public void setSelectedValue(String selectedValue)
  {
    this.selectedValue = selectedValue;
  }

  /**
   * @return the benefitValueTypeGroup
   */
  public PlanBenefitValueTypeGroupData getBenefitValueTypeGroup()
  {
    return benefitValueTypeGroup;
  }

  /**
   * @param benefitValueTypeGroup the new value of benefitValueTypeGroup
   */
  public void setBenefitValueTypeGroup(PlanBenefitValueTypeGroupData benefitValueTypeGroup)
  {
    this.benefitValueTypeGroup = benefitValueTypeGroup;
  }

  /**
   * @return the benefitValueSetMap
   */
  public Map<String, PlanBenefitValueSetData> getBenefitValueSetMap()
  {
    return benefitValueSetMap;
  }

  /**
   * @param benefitValueSetMap the new value of benefitValueSetMap
   */
  public void setBenefitValueSetMap(Map<String, PlanBenefitValueSetData> benefitValueSetMap)
  {
    this.benefitValueSetMap = benefitValueSetMap;
  }
  
  public List<CustomAttributeData> getCustomAttributes()
  {
    return customAttributes;
  }

  public void setCustomAttributes(List<CustomAttributeData> customAttributes)
  {
    this.customAttributes = customAttributes;
  }

  /**
   * @return the sortOrder
   */
  public int getSortOrder()
  {
    return sortOrder;
  }

  /**
   * @param sortOrder the new value of sortOrder
   */
  public void setSortOrder(int sortOrder)
  {
    this.sortOrder = sortOrder;
  }

  public List<PlanBenefitValueData> getValues()
  {
    return values;
  }

  public void setValues(List<PlanBenefitValueData> values)
  {
    this.values = values;
  }
}