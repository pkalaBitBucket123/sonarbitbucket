package com.connecture.shopping.process.initialization;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang.LocaleUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.connecture.shopping.process.common.Environment;
import com.connecture.shopping.process.common.PageComponentProcess;
import com.connecture.shopping.process.domain.PageComponentInfo;

public class ShoppingInitializationHelper
{
  private static final Logger LOG = Logger.getLogger(ShoppingInitializationHelper.class);
   
  private final ResourcePatternResolver resourceResolver;
  private final Collection<String> configLocations;
  
  private PageComponentProcess pageComponentProcess;
  
  public ShoppingInitializationHelper(List<String> configLocations)
  {
    resourceResolver = new PathMatchingResourcePatternResolver();
    this.configLocations = configLocations;
  }
  
  public void initComponentConfigurations() throws IOException
  {
    LOG.info("Loading component default configurations");
    for(String configLocation : configLocations)
    {
      Resource[] configs = resourceResolver.getResources(configLocation);
      for(Resource config : configs)
      {
        processComponentConfiguration(config);
      }
    }
  }
  
  public void initExternalServices()
  {
    //if one fails, don't fail all
    //ping product server
    //ping rules engine?
    //ping provider lookup
    //ping cost estimator
    //etc.
  }
  
  private void processComponentConfiguration(Resource config) throws IOException
  {
    String configName = config.getFilename();
    LOG.info("Processing component configuration [" + configName + "]");
    Properties props = new Properties();
    props.load(config.getInputStream());
    
    String componentKey = props.getProperty(Environment.PageComponents.PAGE_COMPONENT_KEY);
    String componentLocaleStr = props.getProperty(Environment.PageComponents.PAGE_COMPONENT_LOCALE);
    Locale componentLocale = Locale.getDefault();
    if(componentLocaleStr != null && !componentLocaleStr.isEmpty())
    {
      componentLocale = LocaleUtils.toLocale(componentLocaleStr);
    }
    if(componentKey == null || componentKey.isEmpty())
    {
      LOG.error("Component configuration [" + configName + "] must define component identifier [" + Environment.PageComponents.PAGE_COMPONENT_KEY + "] to be valid");
    }
    else
    {
      PageComponentInfo pageComponent = pageComponentProcess.findByComponentKey(componentKey, componentLocale);
      if(pageComponent == null) //initialize
      {
        pageComponent = initializeComponent(props, componentLocale);
        pageComponentProcess.save(pageComponent);
      }
    }
  }
  
  private PageComponentInfo initializeComponent(Properties props, Locale locale)
  {
    PageComponentInfo ret = new PageComponentInfo();

    String componentKey = props.getProperty(Environment.PageComponents.PAGE_COMPONENT_KEY);
    
    ret.setComponentKey(componentKey);
    ret.setLocale(locale);
    
    JSONObject content = new JSONObject();
    for(Entry<Object, Object> entry : props.entrySet())
    {
      String fullKey = (String)entry.getKey();
      
      if(!fullKey.equals(Environment.PageComponents.PAGE_COMPONENT_KEY)
      && !fullKey.equals(Environment.PageComponents.PAGE_COMPONENT_LOCALE))
      {      
        Object data = entry.getValue();
        Object node = content;
        String[] keys = fullKey.split("\\.");
        try
        {
          for(int i = 0; i < keys.length; ++i)
          {
            boolean lastKey = i == keys.length - 1;
            String key = keys[i];
            
            if(node instanceof JSONObject)
            {
              JSONObject jsonNode = (JSONObject)node;
              if(jsonNode.has(key))
              {
                node = jsonNode.get(key);
                if(lastKey)
                {
                  ((JSONObject) node).put(key, data);
                }
              }
              else if(lastKey)
              {
                jsonNode.put(key, data);
              }
              else
              {
                node = new JSONObject();
                jsonNode.put(key, node);
              }
            }
          }
        }
        catch(JSONException ex)
        {
          LOG.warn("Property [" + fullKey + "] was skipped due to a failure to convert to JSON.", ex);
        }
      }
    }
    
    ret.setContent(content);

    return ret;
  }
  
  public void setPageComponentProcess(PageComponentProcess pageComponentProcess)
  {
    this.pageComponentProcess = pageComponentProcess;
  }
}
