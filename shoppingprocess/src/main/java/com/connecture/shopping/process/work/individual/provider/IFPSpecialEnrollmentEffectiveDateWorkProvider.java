package com.connecture.shopping.process.work.individual.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.individual.IFPSpecialEnrollmentEffectiveDateWork;

public abstract class IFPSpecialEnrollmentEffectiveDateWorkProvider implements WorkProviderImpl<IFPSpecialEnrollmentEffectiveDateWork>
{
  @Override
  public IFPSpecialEnrollmentEffectiveDateWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    IFPSpecialEnrollmentEffectiveDateWork work = createWork();

    try
    {
      String eventDate = params.getString("eventDate");
      work.setEventDate(eventDate);
    }
    catch (JSONException e)
    {
      throw new JSONException("\"eventDate\" param must be specified");
    }

    try
    {
      String eventReason = params.getString("eventReason");
      work.setEventReason(eventReason);
    }
    catch (JSONException e)
    {
      throw new JSONException("\"eventReason\" param must be specified");
    }

    return work;
  }
}