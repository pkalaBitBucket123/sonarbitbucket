package com.connecture.shopping.process.common.account.conversion.converter;

public abstract class AVPConverterImpl implements AVPConverter
{
  private AVPConverterType type;

  public AVPConverterImpl(AVPConverterType type) 
  {
    this.type = type;
  }

  @Override
  public AVPConverterType getType()
  {
    return type;
  }
}
