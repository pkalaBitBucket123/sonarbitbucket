package com.connecture.shopping.process.message;

import java.util.List;

public class PlanEmailBean {
	private String planName;

	private String monthlyRate;
	private String monthlyContribution;
	private String monthlyCost;

	private String paycheckRate;
	private String paycheckContribution;
	private String paycheckCost;

	// estimated costs
	private PlanEmailEstimatedCostBean estimatedCost;
	// benefits
	private List<PlanEmailBenefitCategoryBean> benefitCategories;
	// providers
	private List<PlanEmailProviderBean> inNetworkProviders;
	private List<PlanEmailProviderBean> outOfNetworkProviders;
	// preference matches
	private List<PlanEmailPreferenceMatch> preferenceMatches;

	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}

	/**
	 * @param planName
	 *            the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}

	/**
	 * @return the monthlyRate
	 */
	public String getMonthlyRate() {
		return monthlyRate;
	}

	/**
	 * @param monthlyRate
	 *            the monthlyRate to set
	 */
	public void setMonthlyRate(String monthlyRate) {
		this.monthlyRate = monthlyRate;
	}

	/**
	 * @return the monthlyContribution
	 */
	public String getMonthlyContribution() {
		return monthlyContribution;
	}

	/**
	 * @param monthlyContribution
	 *            the monthlyContribution to set
	 */
	public void setMonthlyContribution(String monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	/**
	 * @return the monthlyCost
	 */
	public String getMonthlyCost() {
		return monthlyCost;
	}

	/**
	 * @param monthlyCost
	 *            the monthlyCost to set
	 */
	public void setMonthlyCost(String monthlyCost) {
		this.monthlyCost = monthlyCost;
	}

	/**
	 * @return the estimatedCost
	 */
	public PlanEmailEstimatedCostBean getEstimatedCost() {
		return estimatedCost;
	}

	/**
	 * @return the paycheckRate
	 */
	public String getPaycheckRate() {
		return paycheckRate;
	}

	/**
	 * @param paycheckRate
	 *            the paycheckRate to set
	 */
	public void setPaycheckRate(String paycheckRate) {
		this.paycheckRate = paycheckRate;
	}

	/**
	 * @return the paycheckContribution
	 */
	public String getPaycheckContribution() {
		return paycheckContribution;
	}

	/**
	 * @param paycheckContribution
	 *            the paycheckContribution to set
	 */
	public void setPaycheckContribution(String paycheckContribution) {
		this.paycheckContribution = paycheckContribution;
	}

	/**
	 * @return the paycheckCost
	 */
	public String getPaycheckCost() {
		return paycheckCost;
	}

	/**
	 * @param paycheckCost
	 *            the paycheckCost to set
	 */
	public void setPaycheckCost(String paycheckCost) {
		this.paycheckCost = paycheckCost;
	}

	/**
	 * @param estimatedCost
	 *            the estimatedCost to set
	 */
	public void setEstimatedCost(PlanEmailEstimatedCostBean estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	/**
	 * @return the benefitCategories
	 */
	public List<PlanEmailBenefitCategoryBean> getBenefitCategories() {
		return benefitCategories;
	}

	/**
	 * @param benefitCategories
	 *            the benefitCategories to set
	 */
	public void setBenefitCategories(
			List<PlanEmailBenefitCategoryBean> benefitCategories) {
		this.benefitCategories = benefitCategories;
	}

	/**
	 * @return the inNetworkProviders
	 */
	public List<PlanEmailProviderBean> getInNetworkProviders() {
		return inNetworkProviders;
	}

	/**
	 * @param inNetworkProviders
	 *            the inNetworkProviders to set
	 */
	public void setInNetworkProviders(
			List<PlanEmailProviderBean> inNetworkProviders) {
		this.inNetworkProviders = inNetworkProviders;
	}

	/**
	 * @return the outOfNetworkProviders
	 */
	public List<PlanEmailProviderBean> getOutOfNetworkProviders() {
		return outOfNetworkProviders;
	}

	/**
	 * @param outOfNetworkProviders
	 *            the outOfNetworkProviders to set
	 */
	public void setOutOfNetworkProviders(
			List<PlanEmailProviderBean> outOfNetworkProviders) {
		this.outOfNetworkProviders = outOfNetworkProviders;
	}

	/**
	 * @return the preferenceMatches
	 */
	public List<PlanEmailPreferenceMatch> getPreferenceMatches() {
		return preferenceMatches;
	}

	/**
	 * @param preferenceMatches
	 *            the preferenceMatches to set
	 */
	public void setPreferenceMatches(
			List<PlanEmailPreferenceMatch> preferenceMatches) {
		this.preferenceMatches = preferenceMatches;
	}
}