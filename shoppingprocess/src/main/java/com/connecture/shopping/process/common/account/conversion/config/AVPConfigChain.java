package com.connecture.shopping.process.common.account.conversion.config;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class AVPConfigChain
{
  private List<AVPConfig> configurations = new ArrayList<AVPConfig>();

  public void add(AVPConfig config)
  {
    if (null == config)
    {
      throw new IllegalArgumentException("null AVPConfig is not supported");
    }
    
    if (StringUtils.isBlank(config.getNewVersion()) || StringUtils.isBlank(config.getPreviousVersion()))
    {
      throw new IllegalStateException("AVPConfig must be properly initialized");
    }
    
    for(Iterator<AVPConfig> iter = new ArrayList<AVPConfig>(configurations).iterator(); iter.hasNext() && !configurations.contains(config);)
    {
      AVPConfig existingConfig = iter.next();
      if (existingConfig.getPreviousVersion().equals(config.getNewVersion()))
      {
        configurations.add(configurations.indexOf(existingConfig), config);
      }
      else if (existingConfig.getNewVersion().equals(config.getPreviousVersion()))
      {
        configurations.add(configurations.indexOf(existingConfig) + 1, config);
      }
    }
    
    if (!configurations.contains(config))
    {
      configurations.add(config);
    }
  }
  
  public List<AVPConfig> getConfigurationsFor(String version)
  {
    List<AVPConfig> configurationsFor = null;
    
    for(Iterator<AVPConfig> iter = configurations.iterator(); iter.hasNext() && configurationsFor == null;)
    {
      AVPConfig config = iter.next();
      if (config.getNewVersion().equals(version))
      {
        configurationsFor = new ArrayList<AVPConfig>(configurations.subList(0, configurations.indexOf(config) + 1));
      }
    }
    
    return null != configurationsFor ? configurationsFor : new ArrayList<AVPConfig>();
  }

  public List<AVPConfig> getConfigurationsFor(String startVersion, String endVersion)
  {
    List<AVPConfig> allConfigurations = getConfigurationsFor(endVersion);
    List<AVPConfig> previousConfigurations = getConfigurationsFor(startVersion);
    if (!previousConfigurations.isEmpty())
    {
      allConfigurations.removeAll(previousConfigurations);
    }
    
    return allConfigurations;
  }

  public List<AVPConfig> getConfigurations()
  {
    return configurations;
  }
}
