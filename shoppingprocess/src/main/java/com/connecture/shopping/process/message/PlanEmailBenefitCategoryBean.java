package com.connecture.shopping.process.message;

import java.util.List;

public class PlanEmailBenefitCategoryBean
{
  private String categoryName;
  private List<PlanEmailBenefitBean> benefits;
  
  /**
   * @return the categoryName
   */
  public String getCategoryName()
  {
    return categoryName;
  }

  /**
   * @param categoryName the categoryName to set
   */
  public void setCategoryName(String categoryName)
  {
    this.categoryName = categoryName;
  }

  /**
   * @return the benefits
   */
  public List<PlanEmailBenefitBean> getBenefits()
  {
    return benefits;
  }

  /**
   * @param benefits the benefits to set
   */
  public void setBenefits(List<PlanEmailBenefitBean> benefits)
  {
    this.benefits = benefits;
  }
}