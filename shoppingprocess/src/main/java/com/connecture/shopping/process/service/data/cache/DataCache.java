package com.connecture.shopping.process.service.data.cache;

import java.util.HashMap;
import java.util.Map;

public class DataCache
{
  private Map<DataProviderKey, Object> cache = new HashMap<DataProviderKey, Object>();
  
  public <T> T getData(DataProviderKey key, DataProvider<T> provider) throws Exception {
    T data = null;
    
    synchronized(cache) {
      @SuppressWarnings("unchecked")
      T cachedData = (T)cache.get(key);
      
      if (null == cachedData && null != provider) {
        data = provider.fetchData();
        cache.put(key, data);
      }
      else {
        data = cachedData;
      }
    }
    
    return data;
  }
}
