package com.connecture.shopping.process.service.data.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.connecture.model.data.ShoppingMemberEligibility;
import com.connecture.model.data.ShoppingTierChangeData;
import com.connecture.shopping.process.service.data.ConsumerPlanData;
import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.MemberEligibilityData;

public class ConsumerProductsDataConverter implements DataConverter<ShoppingTierChangeData, ConsumerPlanData>
{
  // private static Logger LOG = Logger.getLogger(ConsumerProductsDataConverter.class);
  
  @Override
  public ConsumerPlanData convertData(ShoppingTierChangeData shoppingData) throws Exception
  {
    ConsumerPlanData consumerPlanData = new ConsumerPlanData();
         
    consumerPlanData.setPlans(shoppingData.getPlans());
    
    consumerPlanData.setPolicyEffectiveTime(shoppingData.getPolicyEffectiveDate().getTime());
    
    if (shoppingData.getPaychecksPerYear() != null)
    {    
      consumerPlanData.setPaychecksPerYear(shoppingData.getPaychecksPerYear().intValue());
    }
    else
    {
      consumerPlanData.setPaychecksPerYear(null);
    }
    
    List<MemberEligibilityData> memberEligibilities = new ArrayList<MemberEligibilityData>();
    
    // set eligibility
    List<ShoppingMemberEligibility> shoppingMemberEligibilities = shoppingData.getMemberEligibility();
    
    for (ShoppingMemberEligibility shoppingMemberEligibility : shoppingMemberEligibilities)
    {
      MemberEligibilityData memberEligibility = new MemberEligibilityData();
      Map<String, Boolean> productLineEligibility = new HashMap<String, Boolean>();
      
      memberEligibility.setMemberKey(shoppingMemberEligibility.getMemberKey());
      
      for (String eligibileProductLine : shoppingMemberEligibility.getEligibleProductLines())
      {
        productLineEligibility.put(eligibileProductLine, true);
      }
      
      for (String ineligibileProductLine : shoppingMemberEligibility.getIneligibleProductLines())
      {
        productLineEligibility.put(ineligibileProductLine, false);
      }
      
      memberEligibility.setProductLineEligibility(productLineEligibility);
      
      memberEligibilities.add(memberEligibility);
    }
    
    consumerPlanData.setMemberEligibilities(memberEligibilities);
    
    return consumerPlanData;
  }
}
