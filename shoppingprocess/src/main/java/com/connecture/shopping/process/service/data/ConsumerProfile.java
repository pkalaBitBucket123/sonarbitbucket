package com.connecture.shopping.process.service.data;

public class ConsumerProfile
{
  private String key;
  private Integer value;

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public Integer getValue()
  {
    return value;
  }

  public void setValue(Integer value)
  {
    this.value = value;
  }
}
