package com.connecture.shopping.process.work.core;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.ProviderSearchDataRequest;
import com.connecture.shopping.process.service.data.NameData;
import com.connecture.shopping.process.service.data.ProviderData;
import com.connecture.shopping.process.service.data.ProviderType;
import com.connecture.shopping.process.service.data.ProvidersData;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;

public class ProviderSearchWork extends WorkImpl
{
  private JSONObject domainObject;
  private ProviderSearchDataRequest providerSearchDataRequest;

  private ProvidersData providersResponse;

  @Override
  public void get() throws Exception
  {
    String search = domainObject.optString("search");

    providerSearchDataRequest.setProviderSearch(search);
    
    DataProvider<ProvidersData> dataProvider = new CamelRequestingDataProvider<ProvidersData>(providerSearchDataRequest);
    providersResponse = getCachedData(ShoppingDataProviderKey.PROVIDER_SEARCH, dataProvider);
  }

  @Override
  public JSONObject processData() throws Exception
  {
    JSONObject retVal = new JSONObject();

    JSONArray providersJSON = new JSONArray();

    if (providersResponse != null)
    {
      List<ProviderData> providers = providersResponse.getProviders();

      if (providers != null)
      {
        for (ProviderData provider : providers)
        {

          // Default implementation supports one type for now
          if (provider.getType() == ProviderType.PHYSICIAN)
          {
            JSONObject providerJSON = new JSONObject();

            providerJSON.put("xrefid", provider.getId());
            NameData name = provider.getName();
            JSONObject nameJSON = new JSONObject();
            nameJSON.put("prefix", name.getPrefix());
            nameJSON.put("first", name.getFirst());
            nameJSON.put("middle", name.getMiddle());
            nameJSON.put("last", name.getLast());
            providerJSON.put("name", nameJSON);
            providerJSON.put("type", provider.getType().toString());

            // TODO: We support generic data for each network, implement
            // as needed

            providersJSON.put(providerJSON);
          }

        }
      }
    }

    retVal.put("providers", providersJSON);

    return retVal;
  }

  public JSONObject getDomain()
  {
    return domainObject;
  }

  public void setDomain(JSONObject domainObject)
  {
    this.domainObject = domainObject;
  }

  public ProviderSearchDataRequest getProviderSearchDataRequest()
  {
    return providerSearchDataRequest;
  }

  public void setProviderSearchDataRequest(ProviderSearchDataRequest providerSearchDataRequest)
  {
    this.providerSearchDataRequest = providerSearchDataRequest;
  }

}
