package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;


public class ValidateZipCodeRequest extends BaseCamelRequest<Boolean>
{
  @Produce(uri = "{{camel.shopping.producer.validateZipCode}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setZipCode(String zipCode)
  {
    headers.put("zipCode", zipCode);
  }
}
