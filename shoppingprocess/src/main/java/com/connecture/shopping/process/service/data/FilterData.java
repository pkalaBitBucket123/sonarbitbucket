package com.connecture.shopping.process.service.data;

import java.util.List;

public class FilterData
{
  private String filterRefId;
  private String title;
  private String evaluationType; // e.g. Config Tag
  private String displayType; // e.g. Slider
  private String helpText;
  private String helpType;
  private String classOverride; 
  private String dataTagKey;  // Not sure what this is for ?
  private List<FilterOptionData> options;

  public String getFilterRefId()
  {
    return filterRefId;
  }

  public void setFilterRefId(String filterRefId)
  {
    this.filterRefId = filterRefId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getDisplayType()
  {
    return displayType;
  }

  public void setDisplayType(String displayType)
  {
    this.displayType = displayType;
  }

  public String getHelpText()
  {
    return helpText;
  }

  public void setHelpText(String helpText)
  {
    this.helpText = helpText;
  }

  public String getHelpType()
  {
    return helpType;
  }

  public void setHelpType(String helpType)
  {
    this.helpType = helpType;
  }

  public String getClassOverride()
  {
    return classOverride;
  }

  public void setClassOverride(String classOverride)
  {
    this.classOverride = classOverride;
  }
  
  public String getEvaluationType()
  {
    return evaluationType;
  }

  public void setEvaluationType(String evaluationType)
  {
    this.evaluationType = evaluationType;
  }

  public String getDataTagKey()
  {
    return dataTagKey;
  }

  public void setDataTagKey(String dataTagKey)
  {
    this.dataTagKey = dataTagKey;
  }

  public List<FilterOptionData> getOptions()
  {
    return options;
  }

  public void setOptions(List<FilterOptionData> options)
  {
    this.options = options;
  }
}
