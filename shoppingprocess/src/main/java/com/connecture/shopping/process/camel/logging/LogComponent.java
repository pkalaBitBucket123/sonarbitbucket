package com.connecture.shopping.process.camel.logging;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.component.log.LogEndpoint;
import org.apache.camel.processor.CamelLogProcessor;
import org.apache.camel.processor.ThroughputLogger;
import org.apache.camel.spi.ExchangeFormatter;
import org.apache.camel.util.CamelLogger;
import org.apache.camel.util.IntrospectionSupport;

public class LogComponent extends org.apache.camel.component.log.LogComponent
{
  private ExchangeFormatter formatter;
  
  /**{@inheritDoc}*/
  @Override
  protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception
  {
    LoggingLevel level = getLoggingLevel(parameters);
    String marker = getAndRemoveParameter(parameters, "marker", String.class);
    Integer groupSize = getAndRemoveParameter(parameters, "groupSize", Integer.class);
    Long groupInterval = getAndRemoveParameter(parameters, "groupInterval", Long.class);

    CamelLogger camelLogger = new CamelLogger(remaining, level, marker);
    Processor logger;
    if (groupSize != null)
    {
      logger = new ThroughputLogger(camelLogger, groupSize);
    }
    else if (groupInterval != null)
    {
      Boolean groupActiveOnly = getAndRemoveParameter(parameters, "groupActiveOnly", Boolean.class, Boolean.TRUE);
      Long groupDelay = getAndRemoveParameter(parameters, "groupDelay", Long.class);
      logger = new ThroughputLogger(camelLogger, this.getCamelContext(), groupInterval, groupDelay, groupActiveOnly);
    }
    else
    {
      if(formatter == null)
      {
        formatter = new org.apache.camel.component.log.LogFormatter();
      }
      IntrospectionSupport.setProperties(formatter, parameters);

      logger = new CamelLogProcessor(camelLogger, formatter);
    }

    LogEndpoint endpoint = new LogEndpoint(uri, this);
    setProperties(endpoint, parameters);
    return new LogEndpoint(uri, this, logger);
  }
  
  public void setFormatter(ExchangeFormatter formatter)
  {
    this.formatter = formatter;
  }
}
