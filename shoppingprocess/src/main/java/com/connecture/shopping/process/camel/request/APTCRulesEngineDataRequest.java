package com.connecture.shopping.process.camel.request;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.rules.aptc.APTCDomain;
import com.connecture.shopping.process.service.data.aptc.converter.APTCDomainConverterUtils;

public class APTCRulesEngineDataRequest extends RulesEngineDataRequest<APTCDomain>
{
  private static Logger LOG = Logger.getLogger(APTCRulesEngineDataRequest.class);
  
  @Override
  public APTCDomain convertData(Object data) throws Exception
  {
    return APTCDomainConverterUtils.fromJSON(new JSONObject((String)data));
  }

  /**
   * Manually generate the JSON so it conforms to the IFP Enrollment domain model naming
   * convention, i.e. underscore-delimitation (aggrevation, leading to impending
   * inebriation)
   */
  @Override
  public Object getRequestBody()
  {
    JSONObject requestObject = new JSONObject();
    
    try
    {
      APTCDomain aptcDomain = getDomain();
      if (null != aptcDomain) 
      {
        requestObject = APTCDomainConverterUtils.toJSON(aptcDomain);
      }
    }
    catch (JSONException e)
    {
      LOG.error("Failed to convert APTCDomain to JSON", e);
    }
    
    // We will want to pass things through, eventually
    return requestObject.toString();
  }
}
