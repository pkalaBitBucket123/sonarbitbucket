package com.connecture.shopping.process.service.data;

import java.util.ArrayList;
import java.util.List;

public class Attribute
{
  private String key;
  private Double value;
  
  // NOTE: The following are initialized so when it is marshalled it becomes an empty JSONArray.
  // The rulesEngine requires this
  private List<AttributeConsumer> consumer = new ArrayList<AttributeConsumer>();
  
  private List<AttributeProperty> property = new ArrayList<AttributeProperty>();

  public String getKey()
  {
    return key;
  }

  public void setKey(String key)
  {
    this.key = key;
  }

  public Double getValue()
  {
    return value;
  }

  public void setValue(Double value)
  {
    this.value = value;
  }

  public List<AttributeConsumer> getConsumer()
  {
    return consumer;
  }

  public void setConsumer(List<AttributeConsumer> consumer)
  {
    this.consumer = consumer;
  }

  public List<AttributeProperty> getProperty()
  {
    return property;
  }

  public void setProperty(List<AttributeProperty> property)
  {
    this.property = property;
  }
}
