package com.connecture.shopping.process.service.data;

public class EnrollmentEventDomainModel
{
  private EnrollmentEventApplication application = new EnrollmentEventApplication();

  public EnrollmentEventApplication getApplication()
  {
    return application;
  }

  public void setApplication(EnrollmentEventApplication application)
  {
    this.application = application;
  }
}
