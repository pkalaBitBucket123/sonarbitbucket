package com.connecture.shopping.process.integration;

import java.util.HashMap;
import java.util.Map;

import com.connecture.model.integration.data.ShoppingToIAMemberDetails;
//<NEBRASKA RIDER SUPPORT MI>
public class ShoppingToIAMemberDetailsExtn extends ShoppingToIAMemberDetails {
	private Map<String, Object> values = new HashMap<String, Object>(); 
	public void addAttribute(String key,Object value){
		values.put(key, value);
	}
	public Map<String, Object> getValues(){
		return values;
	}
}
