package com.connecture.shopping.process.work.individual;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.camel.request.ValidateZipCodeRequest;
import com.connecture.shopping.process.service.data.EnrollmentEvent;
import com.connecture.shopping.process.service.data.ShoppingDomainModel;
import com.connecture.shopping.process.service.data.cache.DataProvider;
import com.connecture.shopping.process.service.data.cache.ShoppingDataProviderKey;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormMember;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormMemberField;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormValidationError;
import com.connecture.shopping.process.service.data.demographicsForm.DemographicsFormValidationErrorConfig;
import com.connecture.shopping.process.work.CamelRequestingDataProvider;
import com.connecture.shopping.process.work.WorkImpl;
import com.connecture.shopping.process.work.core.EffectiveDateWorkHelper;
import com.connecture.shopping.process.work.core.ShoppingRulesEngineBean;

public class DemographicsValidationWork extends WorkImpl
{
  private ShoppingDomainModel formConfigDomainData;
  private ShoppingDomainModel formValidationDomainData;
  private ValidateZipCodeRequest validateZipCodeRequest;

  private JSONObject inputJSON;
  private String coverageEffectiveDateStr;
  private String zipCode;
  private String eventDate;
  private String eventType;

  private ShoppingRulesEngineBean shoppingDemographicsBean;
  private ShoppingRulesEngineBean shoppingDemographicsValidationBean;

  // private JSONObject zipCodeValidationJSON;
  private Boolean isValidUSZip;

  @Override
  public void get() throws Exception
  {
    // 1. get form configuration first; this will have the field keys and
    // validation
    // configuration needed as input to the validation rule call.

    shoppingDemographicsBean.setCoverageEffectiveDate(EffectiveDateWorkHelper.getEffectiveDate(coverageEffectiveDateStr));
    shoppingDemographicsBean.setDataCache(getDataCache());
    formConfigDomainData = shoppingDemographicsBean.executeRule();

    // 2. get form member data from inputJSON and map member data to
    // configuration fields.

    Map<String, String> fieldNameKeyMap = this.getDemographicsFieldNameKeyMap(formConfigDomainData
      .getRequest().getDemographicsForm().getField());

    List<DemographicsFormMember> members = this.buildMemberDataFromInputJSON(inputJSON, fieldNameKeyMap);

    formConfigDomainData.getRequest().getDemographicsForm().setMember(members);

    // 3. pass the form configuration and user input data to the validation rule
    // call

    shoppingDemographicsValidationBean.setDemographicsForm(formConfigDomainData.getRequest().getDemographicsForm());
    
    shoppingDemographicsValidationBean.setEnrollmentEvent(buildEnrollmentEvent());
    
    shoppingDemographicsValidationBean.setCoverageEffectiveDate(EffectiveDateWorkHelper.getEffectiveDate(coverageEffectiveDateStr));
    shoppingDemographicsValidationBean.setDataCache(getDataCache());
    formValidationDomainData = shoppingDemographicsValidationBean.executeRule();

    validateZipCodeRequest.setZipCode(zipCode);

    DataProvider<Boolean> dataProvider = new CamelRequestingDataProvider<Boolean>(validateZipCodeRequest);

    isValidUSZip = getCachedData(ShoppingDataProviderKey.ZIP_CODE_VALIDATION, dataProvider);
  }

  private EnrollmentEvent buildEnrollmentEvent()
  {
    EnrollmentEvent enrollmentEvent = new EnrollmentEvent();
    enrollmentEvent.setType(eventType);
    enrollmentEvent.setDate(eventDate);
    return enrollmentEvent;
  }

  @Override
  public void update() throws Exception
  {
    //el o el
    get();
  }

  @Override
  public JSONObject processData() throws Exception
  {
    // return object
    JSONObject jsonObject = new JSONObject();

    JSONObject demographicsValidationJSON = new JSONObject();

    // construct the "errors" array
    JSONArray errors = new JSONArray();

    // add rules validation
    if (formValidationDomainData.getRequest().getDemographicsForm() != null)
    {
      demographicsValidationJSON.put("hasErrors", formValidationDomainData.getRequest()
        .getDemographicsForm().getHasErrors());

      List<DemographicsFormValidationError> validationErrors = formValidationDomainData
        .getRequest().getDemographicsForm().getValidationError();

      errors = this.buildValidationErrorsJSON(validationErrors);
    }

    // add rating zipCode validation
    if (!isValidUSZip)
    {
      demographicsValidationJSON.put("hasErrors", true);

      // get the message from the rules engine code
      List<DemographicsFormValidationErrorConfig> formErrorConfigMessages = formConfigDomainData
        .getRequest().getDemographicsForm().getValidationErrorConfig();

      for (DemographicsFormValidationErrorConfig formErrorConfig : formErrorConfigMessages)
      {
        if (formErrorConfig.getKey().equalsIgnoreCase("INVALID_US_ZIP"))
        {
          // build validation error from configuration
          errors.put(buildValidationErrorJSON(formErrorConfig));
        }
      }
    }

    if (errors != null && errors.length() > 0)
    {
      demographicsValidationJSON.put("errors", errors);
    }

    jsonObject.put("demographicsValidation", demographicsValidationJSON);

    return jsonObject;
  }

  private Map<String, String> getDemographicsFieldNameKeyMap(
    List<DemographicsFormField> demographicsFields) throws JSONException
  {
    Map<String, String> demographicFieldMapping = new HashMap<String, String>();

    for (DemographicsFormField fieldData : demographicsFields)
    {
      demographicFieldMapping.put(fieldData.getName(), fieldData.getKey());
    }

    return demographicFieldMapping;
  }

  private List<DemographicsFormMember> buildMemberDataFromInputJSON(
    JSONObject inputJSON,
    Map<String, String> fieldNameKeyMap) throws JSONException
  {

    List<DemographicsFormMember> members = new ArrayList<DemographicsFormMember>();

    JSONObject validationJSON = inputJSON.getJSONObject("demographicsValidation");
    JSONArray membersJSON = validationJSON.optJSONArray("members");
    if (membersJSON != null && membersJSON.length() > 0)
    {
      for (int i = 0; i < membersJSON.length(); i++)
      {
        JSONObject memberJSON = membersJSON.getJSONObject(i);

        DemographicsFormMember member = new DemographicsFormMember();

        // memberRefName (id)
        String memberRefName = memberJSON.getString("memberRefName");
        member.setId(memberRefName);

        member.setField(this.extractFieldData(memberJSON, fieldNameKeyMap));

        members.add(member);
      }
    }

    return members;
  }

  private List<DemographicsFormMemberField> extractFieldData(
    JSONObject memberJSON,
    Map<String, String> fieldNameKeyMap)
  {
    List<DemographicsFormMemberField> fields = new ArrayList<DemographicsFormMemberField>();

    for (Map.Entry<String, String> entry : fieldNameKeyMap.entrySet())
    {
      DemographicsFormMemberField field = new DemographicsFormMemberField();

      String fieldValue = "";
      
      String fieldName = entry.getKey();

      // find the value of a property in the memberJSON matching this fieldName
      String[] fieldNameParts = fieldName.split(".");

      if (fieldNameParts.length > 1)
      {
        int lastIndex = fieldNameParts.length - 1;
        JSONObject parentObject = memberJSON.optJSONObject(fieldNameParts[0]);
        for (int i = 1; i < fieldNameParts.length; i++)
        {
          if (i < lastIndex)
          {
            parentObject = parentObject.optJSONObject(fieldNameParts[i]);
            if (parentObject == null)
            {
              break;
            }
          }
          else
          {
            if (parentObject.opt(fieldNameParts[i]) != null)
            {
              fieldValue = parentObject.opt(fieldNameParts[i]).toString();
            }
          }
        }
      }
      else
      {
        if (memberJSON.opt(fieldName) != null)
        {
          fieldValue = memberJSON.opt(fieldName).toString();
        }
      }

      if (fieldValue != null)
      {
        field.setValue(fieldValue);
        field.setName(fieldName);
        field.setKey(entry.getValue());
        fields.add(field);
      }
    }

    return fields;
  }

  private JSONArray buildValidationErrorsJSON(List<DemographicsFormValidationError> validationErrors)
    throws JSONException
  {
    JSONArray validationErrorsJSON = new JSONArray();

    for (DemographicsFormValidationError validationError : validationErrors)
    {
      validationErrorsJSON.put(buildValidationErrorJSON(validationError));
    }

    return validationErrorsJSON;
  }

  private JSONObject buildValidationErrorJSON(DemographicsFormValidationError validationError)
    throws JSONException
  {
    JSONObject validationErrorJSON = new JSONObject();
    validationErrorJSON.put("key", validationError.getKey());
    validationErrorJSON.put("label", validationError.getLabel());
    return validationErrorJSON;
  }

  private JSONObject buildValidationErrorJSON(DemographicsFormValidationErrorConfig validationError)
    throws JSONException
  {
    JSONObject validationErrorJSON = new JSONObject();
    validationErrorJSON.put("key", validationError.getKey());
    validationErrorJSON.put("label", validationError.getLabel());
    return validationErrorJSON;
  }

  public void setInputJSON(JSONObject inputJSON)
  {
    this.inputJSON = inputJSON;
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }

  public void setEventDate(String eventDate)
  {
    this.eventDate = eventDate;
  }

  public void setEventType(String eventType)
  {
    this.eventType = eventType;
  }

  public void setCoverageEffectiveDate(String coverageEffectiveDate)
  {
    this.coverageEffectiveDateStr = coverageEffectiveDate;
  }

  public void setShoppingDemographicsBean(ShoppingRulesEngineBean shoppingDemographicsBean)
  {
    this.shoppingDemographicsBean = shoppingDemographicsBean;
  }

  public void setShoppingDemographicsValidationBean(
    ShoppingRulesEngineBean shoppingDemographicsValidationBean)
  {
    this.shoppingDemographicsValidationBean = shoppingDemographicsValidationBean;
  }

  public void setValidateZipCodeRequest(ValidateZipCodeRequest validateZipCodeRequest)
  {
    this.validateZipCodeRequest = validateZipCodeRequest;
  }
}