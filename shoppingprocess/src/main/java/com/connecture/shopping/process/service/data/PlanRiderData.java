package com.connecture.shopping.process.service.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.connecture.model.data.MemberLevelRateData;
import com.connecture.model.data.ShoppingProduct;
import com.connecture.model.integration.data.MemberTypeEnum;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanContributionData;
//<NEBRASKA RIDER SUPPORT MI>
/**
 * 
 */
public class PlanRiderData
{
 private Long riderId;
 private String displayName;
 private String alternateName;
 private String benefitSummary;
 private Double monthlyRate;
 private String displayMonthlyRate;
 private String xref;
 private String description;
public Long getRiderId() {
	return riderId;
}
public void setRiderId(Long riderId) {
	this.riderId = riderId;
}
public String getDisplayName() {
	return displayName;
}
public void setDisplayName(String displayName) {
	this.displayName = displayName;
}
public String getAlternateName() {
	return alternateName;
}
public void setAlternateName(String alternateName) {
	this.alternateName = alternateName;
}
public String getBenefitSummary() {
	return benefitSummary;
}
public void setBenefitSummary(String benefitSummary) {
	this.benefitSummary = benefitSummary;
}
public Double getMonthlyRate() {
	return monthlyRate;
}
public void setMonthlyRate(Double monthlyRate) {
	this.monthlyRate = monthlyRate;
}
public String getDisplayMonthlyRate() {
	return displayMonthlyRate;
}
public void setDisplayMonthlyRate(String displayMonthlyRate) {
	this.displayMonthlyRate = displayMonthlyRate;
}
public String getXref() {
	return xref;
}
public void setXref(String xref) {
	this.xref = xref;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
 
}