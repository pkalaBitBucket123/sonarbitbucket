package com.connecture.shopping.process.service.data.converter;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.service.data.DataConverter;
import com.connecture.shopping.process.service.data.EnrollmentEventApplication;
import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentData;
import com.connecture.shopping.process.service.data.SpecialEnrollmentEvent;

public class EnrollmentEventRulesEngineDataConverter implements DataConverter<String, EnrollmentEventDomainModel>
{
  private static Logger LOG = Logger.getLogger(EnrollmentEventRulesEngineDataConverter.class);

  @Override
  public EnrollmentEventDomainModel convertData(String data) throws Exception
  {
    EnrollmentEventDomainModel domainModel = new EnrollmentEventDomainModel();

    try
    {
      JSONObject domainObject = new JSONObject((String) data);
      
      if (LOG.isDebugEnabled())
      {
        LOG.debug("Got " + domainObject.toString(4));
      }
      
      if (domainObject.has("request") && domainObject.getJSONObject("request").has("error"))
      {
        throw new Exception("Errors detected running rule:" + domainObject.getJSONObject("request").getString("error"));
      }
      
      if (!domainObject.has("Application"))
      {
        throw new JSONException("Failed parsing rules engine result JSON, missing \"Application\" node");
      }

      JSONObject applicationObject = domainObject.getJSONObject("Application");
      EnrollmentEventApplication application = domainModel.getApplication();
      
      if (applicationObject.has("Effective_Date"))
      {
        application.setEffectiveDate(applicationObject.getString("Effective_Date"));
      }
      
      if (applicationObject.has("Special_Enrollment"))
      {
        JSONObject specialEnrollmentObject = applicationObject.getJSONObject("Special_Enrollment");
        
        SpecialEnrollmentData specialEnrollment = application.getSpecialEnrollment();
        
        if (specialEnrollmentObject.has("Open_Enrollment_Start_Date"))
        {
          specialEnrollment.setOpenEnrollmentStartDate(specialEnrollmentObject.getString("Open_Enrollment_Start_Date"));
        }
        if (specialEnrollmentObject.has("Open_Enrollment_End_Date"))
        {
          specialEnrollment.setOpenEnrollmentEndDate(specialEnrollmentObject.getString("Open_Enrollment_End_Date"));
        }
        if (specialEnrollmentObject.has("Days_Left_In_Open_Enrollment"))
        {
          specialEnrollment.setDaysLeftToEnroll(specialEnrollmentObject.getInt("Days_Left_In_Open_Enrollment"));
        }
        if (specialEnrollmentObject.has("Within_Enrollment_Window"))
        {
          specialEnrollment.setWithinEnrollmentWindow(specialEnrollmentObject.getString("Within_Enrollment_Window"));
        }
        
        if (specialEnrollmentObject.has("Special_Enrollment_Reasons"))
        {
          JSONArray specialEnrollmentArray = specialEnrollmentObject.getJSONArray("Special_Enrollment_Reasons");
          for (int s = 0; s < specialEnrollmentArray.length(); s++)
          {
            JSONObject specialEnrollmentReasonObject = specialEnrollmentArray.getJSONObject(s);
            SpecialEnrollmentEvent event = new SpecialEnrollmentEvent();
            event.setEventType(specialEnrollmentReasonObject.getString("Reason_Key"));
            event.setDisplayName(specialEnrollmentReasonObject.getString("Reason_Value"));
            event.setEnrollmentDays(specialEnrollmentReasonObject.getInt("EnrollmentWindowValue"));
            specialEnrollment.addSpecialEnrollmentEvent(event); 
          }
        }
        
        if(specialEnrollmentObject.has("Open_Enrollment_Indicator"))
        {
          specialEnrollment.setOpenEnrollmentIndicator(specialEnrollmentObject.getString("Open_Enrollment_Indicator"));
        }
      }
    }
    catch (JSONException e)
    {
      throw new Exception("Failed parsing rules engine result JSON.", e);
    }
    
    return domainModel;
  }
}
