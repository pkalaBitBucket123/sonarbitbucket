/**
 * 
 */
package com.connecture.shopping.process.integration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.model.data.ShoppingLead;
import com.connecture.model.integration.api.ShoppingIndividualEnrollmentDataProvider;
import com.connecture.model.integration.data.IAToShoppingData;
import com.connecture.model.integration.data.ShoppingParams.ShoppingContext;
import com.connecture.model.integration.data.ShoppingToIAData;
import com.connecture.model.integration.data.ShoppingToIAMemberDetails;
import com.connecture.model.integration.data.ShoppingToIAMemberProviderData;
import com.connecture.shopping.process.camel.request.IAToShoppingIndividualDataRequest;
import com.connecture.shopping.process.camel.request.LeadGenerationRequest;
import com.connecture.shopping.process.common.ShoppingDateUtils;
import com.connecture.shopping.process.common.account.Account;

/**
 * 
 */
public class ShoppingIndividualIntegrationProcess
  implements ShoppingIndividualEnrollmentDataProvider
{
  private static Logger LOG = Logger.getLogger(ShoppingIndividualIntegrationProcess.class);

  private IAToShoppingIndividualDataRequest individualDataRequest;
  private LeadGenerationRequest leadGenRequest;

  private SessionFactory sessionFactory;

  private Account sessionAccountImpl;
  private Account persistedAccountImpl;

  public IAToShoppingData getIFPShoppingData(String transactionId) throws Exception
  {
    // request IA data
    individualDataRequest.setTransactionId(transactionId);
    individualDataRequest.setContext(ShoppingContext.INDIVIDUAL);
    IAToShoppingData individualData = individualDataRequest.submitRequest();
    return individualData;
  }

  public boolean updateTransactionId(String oldTransactionId, String newTransactionId)
    throws Exception
  {
    boolean success = false;
    try
    {
      persistedAccountImpl.updateAccountTransactionId(oldTransactionId, newTransactionId);
    }
    catch (Exception e)
    {
      LOG.error("Error while attempting to update transaction [" + oldTransactionId + "] to ["
        + newTransactionId + "]: " + e.getMessage());
      throw new Exception(e);
    }
    return success;
  }

  public boolean copyTransaction(String oldTransactionId, String newTransactionId) throws Exception
  {
    boolean success = false;
    
    try
    {
      // get the original account
      JSONObject accountJSON = persistedAccountImpl.getAccount(oldTransactionId);
      // create the new account
      persistedAccountImpl.getOrCreateAccount(newTransactionId);
      // update the new account with the original account data
      persistedAccountImpl.updateAccountInfo(newTransactionId, accountJSON);
    }
    catch (Exception e)
    {
      LOG.error("Error while attempting to copy transaction [" + oldTransactionId + "] to ["
        + newTransactionId + "]: " + e.getMessage());
      throw new Exception(e);
    }
    
    return success;
  }
  
  public boolean assignTransactionId(String transactionId) throws Exception
  {
    boolean success = false;

    // get the account data from session
    JSONObject accountJSON = sessionAccountImpl.getAccount(null);

    // save the account data into the database
    try
    {

      persistedAccountImpl.getOrCreateAccount(transactionId);
      persistedAccountImpl.updateAccountInfo(transactionId, accountJSON);
      success = true;
    }
    catch (Exception e)
    {
      LOG.error("Error while attempting to assign transactionId [" + transactionId
        + "] to account data in session: " + e.getMessage());
      throw new Exception(e);
    }
    return success;
  }

  public ShoppingToIAData getAnonymousEnrollmentData() throws Exception
  {
    JSONObject accountJSON = sessionAccountImpl.getAccount(null);
    ShoppingToIAData shoppingData = new ShoppingToIADataExtn();
    shoppingData.setContext(ShoppingContext.ANONYMOUS);
	if(accountJSON.has("covType"))
	{
         ((ShoppingToIADataExtn)shoppingData).addAttribute("coverageType", accountJSON.getString("covType"));
	} else if(accountJSON.has("coverageType")){
	     ((ShoppingToIADataExtn)shoppingData).addAttribute("coverageType", accountJSON.getString("coverageType")); 
	}
	if( ( accountJSON.has("covType") || accountJSON.has("coverageType")) && accountJSON.has("effectiveDate1") ){
		  SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		  shoppingData.setEffectiveDate( formatter.parse(accountJSON.getString("effectiveDate1")) );
		  accountJSON.put("effectiveDate", accountJSON.getString("effectiveDate1"));
          accountJSON.put("effectiveMs", shoppingData.getEffectiveDate().getTime());
		}
	// SHSBCBSNE - 1164
	else if (accountJSON.has("coverageDate"))
    {
    	shoppingData.setEffectiveDate(ShoppingDateUtils.stringToDate(accountJSON.optString("coverageDate", null)));
    }
   
    if(accountJSON.has("childOnly") && accountJSON.getString("childOnly") != null && accountJSON.getString("childOnly").equalsIgnoreCase("Yes"))
    {
    	shoppingData.setChildOnly(true);
    }
    else
    {
    	shoppingData.setChildOnly(false);
    }
    return populateEnrollmentData(accountJSON, shoppingData);
  }

  public ShoppingToIAData getIndividualEnrollmentData(String transactionId) throws Exception
  {
    JSONObject accountJSON = persistedAccountImpl.getAccount(transactionId);

    ShoppingToIAData shoppingData = new ShoppingToIADataExtn();
    shoppingData.setContext(ShoppingContext.INDIVIDUAL);
    if(accountJSON==null || !accountJSON.has("effectiveDate") )
      return new ShoppingToIADataExtn();
    if(accountJSON.has("covType"))
	{
         ((ShoppingToIADataExtn)shoppingData).addAttribute("coverageType", accountJSON.getString("covType"));
	} else if( accountJSON.has("coverageType") ) {
		 ((ShoppingToIADataExtn)shoppingData).addAttribute("coverageType", accountJSON.getString("coverageType"));
	}
    if(accountJSON.has("childOnly") && accountJSON.getString("childOnly") != null && accountJSON.getString("childOnly").equalsIgnoreCase("Yes"))
    {
    	shoppingData.setChildOnly(true);
    }
    else
    {
    	shoppingData.setChildOnly(false);
    }

    shoppingData.setTransactionId(transactionId);

    return populateEnrollmentData(accountJSON, shoppingData);
  }

  private static ShoppingToIAData populateEnrollmentData(
    JSONObject accountJSON,
    ShoppingToIAData shoppingData) throws Exception
  {
    // JSONObject accountJSON = persistedAccountImpl.getAccount(transactionId);
    //
    // ShoppingToIAData shoppingData = new ShoppingToIAData();
    //
    // shoppingData.setContext(ShoppingContext.INDIVIDUAL);

    // effectiveDate
	  if(shoppingData.getEffectiveDate()==null) {
			String effectiveDateStr = "";
			if (accountJSON.has("coverageDate")) {
				effectiveDateStr = accountJSON.getString("coverageDate");
			}
			else{
			    effectiveDateStr = accountJSON.getString("effectiveDate");
			}
		    Date effectiveDate = ShoppingDateUtils.stringToDate(effectiveDateStr);
		    shoppingData.setEffectiveDate(effectiveDate);
			
		  }else{
		  shoppingData.setEffectiveDate(shoppingData.getEffectiveDate());
	  }

    // If Special Enrollment Then
    if (accountJSON.has("eventDate"))
    {
      // eventDate
      String eventDateString = accountJSON.getString("eventDate");
      Date eventDate = ShoppingDateUtils.stringToDate(eventDateString);
      shoppingData.setEventDate(eventDate);

      // eventType
      String eventType = accountJSON.getString("eventType");
      shoppingData.setSpecialEnrollmentReasonKey(eventType);
    }

    // members
    // check for countyId, stateKey if available in the account.
    // (this means that the selected ZipCode spans multiple counties)

    String zipCode = accountJSON.getString("zipCode");
    Long countyId = accountJSON.optLong("countyId");
    String stateKey = accountJSON.optString("stateKey");
    JSONArray membersJSON = accountJSON.getJSONArray("members");

    Map<Long, List<String>> memberPlanMap = buildMemberPlanMap(accountJSON);

    shoppingData.setMemberDetails(buildShoppingMembersData(membersJSON, memberPlanMap, zipCode,
      countyId, stateKey));
//<NEBRASKA RIDER SUPPORT MI>
    setRidersData(shoppingData.getMemberDetails(),accountJSON);
  //</NEBRASKA RIDER SUPPORT MI>
    // TODO | special enrollment... ?
    // shoppingData.setEventDate(eventDate);
    // shoppingData.setSpecialEnrollmentReasonKey(specialEnrollmentReasonKey);

    // shoppingData.setTransactionId(transactionId);

     return shoppingData;
  }

  public static List<ShoppingToIAMemberDetails> buildShoppingMembersData(
    JSONArray membersJSON,
    Map<Long, List<String>> memberPlanMap,
    String zipCode,
    Long countyId,
    String stateKey) throws JSONException, ParseException
  {
    List<ShoppingToIAMemberDetails> members = new ArrayList<ShoppingToIAMemberDetails>();

    for (int i = 0; i < membersJSON.length(); i++)
    {
      JSONObject memberJSON = membersJSON.getJSONObject(i);
      Long memberId = memberJSON.getLong("memberRefName");
      List<String> memberPlanCodes = memberPlanMap.get(memberId);
      members
        .add(buildShoppingMemberData(memberJSON, memberPlanCodes, zipCode, countyId, stateKey));
    }
    return members;
  }

  private static ShoppingToIAMemberDetails buildShoppingMemberData(
    JSONObject memberJSON,
    List<String> memberPlanCodes,
    String zipCode,
    Long countyId,
    String stateKey) throws JSONException, ParseException
  {
	//<NEBRASKA RIDER SUPPORT MI>
    //ShoppingToIAMemberDetails member = new ShoppingToIAMemberDetails();
	  ShoppingToIAMemberDetails member = new ShoppingToIAMemberDetailsExtn();
	//</NEBRASKA RIDER SUPPORT MI>
    // countyId (may be null)
    member.setCountyKey(countyId);
    // stateKey (may be null)
    member.setStateKey(stateKey);

    // date of birth
    String birthDateString = memberJSON.getString("birthDate");
    Date dateOfBirth = ShoppingDateUtils.stringToDate(birthDateString);
    member.setDateOfBirth(dateOfBirth);

    // first name
    JSONObject nameJSON = memberJSON.getJSONObject("name");
    member.setFirstName(nameJSON.getString("first"));

    // gender
    member.setGender(memberJSON.getString("gender"));

    Boolean isNew = memberJSON.optBoolean("new");

    if (isNew)
    {
      // memberId
      member.setMemberId(null);
    }
    else
    {
      // memberId
      member.setMemberId(memberJSON.getLong("memberRefName"));
    }

    // member plan external reference code
    member.setSelectedPlanInternalCode(memberPlanCodes);

    // provider (PCP - Primary Care Provider) data
    member.setMemberProviderData(buildMemberProviderData(memberJSON.optJSONArray("providers")));

    // relationshipType
    member.setMemberType(memberJSON.getString("memberRelationship"));

    // smoker
    String isSmoker = memberJSON.optString("isSmoker");
    boolean smoker = ("y".equalsIgnoreCase(isSmoker));
    member.setSmoker(smoker);

    // zipCode
    member.setZipCode(zipCode);

    return member;
  }

  /**
   * This is a sample (BS) implementation of translating Provider search JSON
   * into a common integration object; provider search will be
   * client-implemented and provider data will be client-specific.
   * 
   * @param providersJSON
   * @return List<ShoppingToIAMemberProviderData>
   * @throws JSONException
   */
  private static List<ShoppingToIAMemberProviderData> buildMemberProviderData(
    JSONArray providersJSON) throws JSONException
  {
    List<ShoppingToIAMemberProviderData> providers = new ArrayList<ShoppingToIAMemberProviderData>();

    if (providersJSON != null)
    {

      for (int i = 0; i < providersJSON.length(); i++)
      {
        JSONObject providerJSON = providersJSON.getJSONObject(i);

        ShoppingToIAMemberProviderData memberProviderData = new ShoppingToIAMemberProviderData();
        memberProviderData.setProviderId(providerJSON.getString("xrefid"));
        memberProviderData.setProviderName(buildProviderName(providerJSON));

        // TODO | resolve this issue with IA
        // memberProviderData.setSelectedPlanInternalCode(selectedPlanInternalCode);

        providers.add(memberProviderData);
      }
    }

    return providers;
  }

  private static String buildProviderName(JSONObject providerJSON) throws JSONException
  {
    String nameStr = "";

    String providerType = providerJSON.getString("type");

    if ("PHYSICIAN".equalsIgnoreCase(providerType))
    {
      // should be a name object
      JSONObject physicianNameJSON = providerJSON.getJSONObject("name");
      // physician name, ASSEMBLE!
      String prefix = physicianNameJSON.optString("prefix");
      String firstName = physicianNameJSON.optString("first");
      String middleName = physicianNameJSON.optString("middle");
      String lastName = physicianNameJSON.optString("last");

      nameStr += !StringUtils.isBlank(prefix) ? (prefix + " ") : "";
      nameStr += !StringUtils.isBlank(firstName) ? (firstName + " ") : "";
      nameStr += (!StringUtils.isBlank(firstName) && !StringUtils.isBlank(middleName))
        ? (middleName + " ")
        : "";
      nameStr += !StringUtils.isBlank(lastName) ? (lastName) : "";
      nameStr = nameStr.trim();
    }
    else
    {
      nameStr = providerJSON.getString("name");
    }

    return nameStr;
  }

  public static Map<Long, List<String>> buildMemberPlanMap(JSONObject accountJSON)
    throws JSONException
  {
    Map<Long, List<String>> memberPlanMap = new HashMap<Long, List<String>>();

    if (!accountJSON.isNull("cart"))
    {
      JSONObject cartJSON = accountJSON.getJSONObject("cart");
      JSONObject productLinesJSON = cartJSON.optJSONObject("productLines");

      if (productLinesJSON != null)
      {
        String[] productLines = JSONObject.getNames(productLinesJSON);
        for (int i = 0; null != productLines && i < productLines.length; i++)
        {
          JSONObject productLineJSON = productLinesJSON.getJSONObject(productLines[i]);

          // get product identifier
          
          //JSONObject productJSON = productLineJSON.getJSONObject("product");
          JSONArray productsJSON = productLineJSON.getJSONArray("products");
          // TODO | iterate to get all products.
          // for now, we only support one object, so just send that;
          JSONObject productJSON = productsJSON.getJSONObject(0);
          
          String productId = productJSON.getString("id");

          // get covered members
          JSONArray coveredMembersJSON = productLineJSON.getJSONArray("coveredMembers");

          // add product identifier to the list of identifiers for the member
          for (int j = 0; j < coveredMembersJSON.length(); j++)
          {
            JSONObject coveredMemberJSON = coveredMembersJSON.getJSONObject(j);
            Long memberId = coveredMemberJSON.getLong("memberRefName");

            // attempt to get the current set of product external reference IDs
            // for
            // this member.
            List<String> memberProducts = memberPlanMap.get(memberId);
            // if the set does not exist, create it now.
            if (memberProducts == null)
            {
              memberProducts = new ArrayList<String>();
            }

            memberProducts.add(productId);

            memberPlanMap.put(memberId, memberProducts);
          }
        }
      }
    }

    return memberPlanMap;
  }

  public boolean generateLead(ShoppingLead lead, String contextStr, String transactionId)
    throws Exception
  {
    Boolean success = false;
    ShoppingToIAData accountData = null;
    
    ShoppingContext context = ShoppingContext.fromContextName(contextStr);
    
    // TODO | hack; see why context isn't coming through for email
    if (context == null ||  context == ShoppingContext.ANONYMOUS)
    {
      accountData = this.getAnonymousEnrollmentData();
    }
    else if (context == ShoppingContext.INDIVIDUAL)
    {
      accountData = this.getIndividualEnrollmentData(transactionId);
    }

    if (accountData != null)
    {
      lead.setMembers(accountData.getMemberDetails());
      lead.setEffectiveDate(accountData.getEffectiveDate());
      lead.setSpecialEnrollmentReasonKey(accountData.getSpecialEnrollmentReasonKey());    
      lead.setSpecialEnrollmentEventDate(accountData.getEventDate());
      // Camel route through to IA
      leadGenRequest.setShoppingLead(lead);
      success = leadGenRequest.submitRequest();
    } else {
      LOG.error("Unable to determine context / obtain account data for lead generation.");
    }
    
    return success;
  }
//<NEBRASKA RIDER SUPPORT MI>
  private static void setRidersData(
		List<ShoppingToIAMemberDetails> memberDetails, JSONObject accountJSON) throws JSONException {
	  Map<Long, List<Long>> riderMap = buildMemberPlanRiderMap(accountJSON);
	  if(memberDetails != null && riderMap != null){
		  for(ShoppingToIAMemberDetails mem : memberDetails){			 
			  if(mem instanceof ShoppingToIAMemberDetailsExtn && "MEMBER".equalsIgnoreCase(mem.getMemberType())){
				  List<Long> list = riderMap.get(0l);
				  ((ShoppingToIAMemberDetailsExtn)mem).addAttribute("riders", list);
			  }
		  }
	  }
	
  }
  public static Map<Long, List<Long>> buildMemberPlanRiderMap(JSONObject accountJSON)
		    throws JSONException
		  {
		    Map<Long, List<Long>> memberPlanMap = new HashMap<Long, List<Long>>();

		    if (!accountJSON.isNull("cart"))
		    {
		      JSONObject cartJSON = accountJSON.getJSONObject("cart");
		      JSONObject productLinesJSON = cartJSON.optJSONObject("productLines");

		      if (productLinesJSON != null)
		      {
		        String[] productLines = JSONObject.getNames(productLinesJSON);
		        if(productLines != null)
		        for (int i = 0; i < productLines.length; i++)
		        {
		          JSONObject productLineJSON = productLinesJSON.getJSONObject(productLines[i]);

		          // get product identifier
		          JSONObject riderJSON = productLineJSON.optJSONObject("selectedRiders");
		          Long riderId = null;
		          if(riderJSON != null && riderJSON.has("DENTAL"))
		           riderId = riderJSON.getJSONObject("DENTAL").getLong("riderId");

		          if(riderId == null)
		        	  continue;
		          // get covered members
		          JSONArray coveredMembersJSON = productLineJSON.getJSONArray("coveredMembers");

		          // add product identifier to the list of identifiers for the member
		          for (int j = 0; j < coveredMembersJSON.length(); j++)
		          {
		            JSONObject coveredMemberJSON = coveredMembersJSON.getJSONObject(j);
		            Long memberId = coveredMemberJSON.getLong("memberRefName");

		            // attempt to get the current set of product external reference IDs
		            // for
		            // this member.
		            List<Long> memberProducts = memberPlanMap.get(memberId);
		            // if the set does not exist, create it now.
		            if (memberProducts == null)
		            {
		              memberProducts = new ArrayList<Long>();
		            }

		            memberProducts.add(riderId);

		            memberPlanMap.put(memberId, memberProducts);
		          }
		        }
		      }
		    }

		    return memberPlanMap;
		  }
//</NEBRASKA RIDER SUPPORT MI>

  /**
   * @return the sessionFactory
   */
  public SessionFactory getSessionFactory()
  {
    return sessionFactory;
  }

  /**
   * @param sessionFactory the new value of sessionFactory
   */
  public void setSessionFactory(SessionFactory sessionFactory)
  {
    this.sessionFactory = sessionFactory;
  }

  public void setSessionAccountImpl(Account sessionAccountImpl)
  {
    this.sessionAccountImpl = sessionAccountImpl;
  }

  public void setPersistedAccountImpl(Account persistedAccountImpl)
  {
    this.persistedAccountImpl = persistedAccountImpl;
  }

  public void setIndividualDataRequest(IAToShoppingIndividualDataRequest individualDataRequest)
  {
    this.individualDataRequest = individualDataRequest;
  }

  public void setLeadGenRequest(LeadGenerationRequest leadGenRequest)
  {
    this.leadGenRequest = leadGenRequest;
  }
}