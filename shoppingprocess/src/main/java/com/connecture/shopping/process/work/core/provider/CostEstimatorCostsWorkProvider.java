package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.core.CostEstimatorCostsWork;

public abstract class CostEstimatorCostsWorkProvider implements WorkProviderImpl<CostEstimatorCostsWork>
{
  @Override
  public CostEstimatorCostsWork getWork(
    String transactionId,
    String requestKey,
    WorkConstants.Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    CostEstimatorCostsWork work = createWork();
    work.setTransactionId(transactionId);
    return work;
  }
}