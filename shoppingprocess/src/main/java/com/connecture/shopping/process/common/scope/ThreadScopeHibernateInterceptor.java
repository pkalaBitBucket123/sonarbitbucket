package com.connecture.shopping.process.common.scope;

import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;

/**
 * A hibernate interceptor that clears thread scope when the Hibernate transaction is completed. (Either rolled back or committed)
 */
public class ThreadScopeHibernateInterceptor extends EmptyInterceptor
{
  private Logger logger = Logger.getLogger(ThreadScopeHibernateInterceptor.class);
  
  public void afterTransactionCompletion(Transaction tx)
  {
    logger.debug("Destroying thread scope");
    ThreadScopeContextHolder.currentThreadScopeAttributes().clear();
  }
}
