package com.connecture.shopping.process.service.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.connecture.model.data.MemberLevelRateData;
import com.connecture.model.data.ShoppingProduct;
import com.connecture.model.integration.data.MemberTypeEnum;
import com.connecture.shopping.process.service.plan.data.PlanBenefitCategoryData;
import com.connecture.shopping.process.service.plan.data.PlanBenefitData;
import com.connecture.shopping.process.service.plan.data.PlanContributionData;

/**
 * 
 */
public class PlanData
{

  // identifiers
  private String identifier;
  private String extRefId;
  private String internalCode;

  // base plan properties
  private String planName;
  private String planType;
  private String metalTier;
  private String tierCode;
  private Integer sortOrder;
  //start change(s) by rsingh for user story 20,21,92,258,260,229 
  private String sbcUrl;
  private String brochureUrl;
  //end change(s) by rsingh for user story  20,21,92,258,260,229
  private List<String> networks;
  // start change(s) by rsingh for user story 229
  private String description;
  // end change(s) by rsingh for user story 229
  private ProductData productData; // product line data

  // benefits : TODO | need both lists?
  private List<PlanBenefitCategoryData> benefitCategories;
  private List<PlanBenefitData> benefits;

  // standard rate properties
  
  // plan total rate
  private Double rate;
  // plan total contribution (employee)
  private PlanContributionData contributionData;
  // plan tobacco surcharge (individual)
  private Double tobaccoSurcharge;
  // plan total cost
  private Double planCost;
  
  private PlanAPTCData aptcData;

  // member-level rate properties 
  
  // is memberLevelRated
  private boolean memberLevelRated;
  // rates per member
  private Map<String, String> memberLevelRates;
  // tobacco surcharge per member
  private Map<String, String> memberLevelTobaccoSurcharges;
  // contributions per type
  private Map<MemberTypeEnum, Double> memberTypeContributions;
  // total costs per type
  private Map<MemberTypeEnum, Double> memberTypeCosts;
//<NEBRASKA RIDER SUPPORT MI>
  private List<PlanRiderData> riders; 
//</NEBRASKA RIDER SUPPORT MI>
  private String benefitSummaryUrl;
  public void updateFrom(ShoppingProduct sourceData)
  {
    PlanContributionData sourceContributionData = new PlanContributionData();
    sourceContributionData.setAmount(sourceData.getContribution());
    sourceContributionData.setTypeCode(sourceData.getContributionType());

    contributionData = sourceContributionData;
    tierCode = sourceData.getTierKey();
    rate = sourceData.getRate();
    planCost = sourceData.getCost();

    // set memberLevelRate and data
    if (sourceData.getMemberLevelRateData() != null)
    {
      memberLevelRated = true;

      Map<String, String> memberLevelRates = new HashMap<String, String>();
      MemberLevelRateData memberLevelRateData = sourceData.getMemberLevelRateData();

      // get member level rates
      for (String memberId : memberLevelRateData.getMemberRates().keySet())
      {
        memberLevelRates.put(memberId,
          Double.toString(memberLevelRateData.getMemberRates().get(memberId)));
      }
      this.memberLevelRates = memberLevelRates;
      // contributions (pass-through) 6/14/13
      this.memberTypeContributions = memberLevelRateData.getEmployerContributions();
      // costs (pass-through) 6/14/13
      this.memberTypeCosts = memberLevelRateData.getMemberCosts();
    }

  }

  public String getIdentifier()
  {
    return identifier;
  }

  public void setIdentifier(String identifier)
  {
    this.identifier = identifier;
  }

  public String getExtRefId()
  {
    return extRefId;
  }

  public void setExtRefId(String extRefId)
  {
    this.extRefId = extRefId;
  }

  public String getInternalCode()
  {
    return internalCode;
  }

  public void setInternalCode(String internalCode)
  {
    this.internalCode = internalCode;
  }

  public String getPlanName()
  {
    return planName;
  }

  public void setPlanName(String planName)
  {
    this.planName = planName;
  }

  public String getPlanType()
  {
    return planType;
  }

  public void setPlanType(String planType)
  {
    this.planType = planType;
  }

  public String getMetalTier()
  {
    return metalTier;
  }

  public void setMetalTier(String metalTier)
  {
    this.metalTier = metalTier;
  }

  public String getTierCode()
  {
    return tierCode;
  }

  public void setTierCode(String tierCode)
  {
    this.tierCode = tierCode;
  }

  public Integer getSortOrder()
  {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
//start change(s) by rsingh for user story 20,21,92,258,260,229
  public String getSbcUrl() {
	return sbcUrl;
}

public void setSbcUrl(String sbcUrl) {
	this.sbcUrl = sbcUrl;
}

public String getBrochureUrl() {
	return brochureUrl;
}

public void setBrochureUrl(String brochureUrl) {
	this.brochureUrl = brochureUrl;
}
//end change(s) by rsingh for user story 20,21,92,258,260,229
public List<String> getNetworks()
  {
    return networks;
  }

  public void setNetworks(List<String> networks)
  {
    this.networks = networks;
  }
//start change(s) by rsingh for user story 229
  public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}
//start change(s) by rsingh for user story 229
public ProductData getProductData()
  {
    return productData;
  }

  public void setProductData(ProductData productData)
  {
    this.productData = productData;
  }

  public List<PlanBenefitCategoryData> getBenefitCategories()
  {
    return benefitCategories;
  }

  public void setBenefitCategories(List<PlanBenefitCategoryData> benefitCategories)
  {
    this.benefitCategories = benefitCategories;
  }

  public List<PlanBenefitData> getBenefits()
  {
    return benefits;
  }

  public void setBenefits(List<PlanBenefitData> benefits)
  {
    this.benefits = benefits;
  }

  public boolean isMemberLevelRated()
  {
    return memberLevelRated;
  }

  public void setMemberLevelRated(boolean memberLevelRated)
  {
    this.memberLevelRated = memberLevelRated;
  }

  public Double getRate()
  {
    return rate;
  }

  public void setRate(Double rate)
  {
    this.rate = rate;
  }

  public PlanContributionData getContributionData()
  {
    return contributionData;
  }

  public void setContributionData(PlanContributionData contributionData)
  {
    this.contributionData = contributionData;
  }

  public Double getTobaccoSurcharge()
  {
    return tobaccoSurcharge;
  }

  public void setTobaccoSurcharge(Double tobaccoSurcharge)
  {
    this.tobaccoSurcharge = tobaccoSurcharge;
  }

  public Double getPlanCost()
  {
    return planCost;
  }

  public void setPlanCost(Double planCost)
  {
    this.planCost = planCost;
  }

  public Map<String, String> getMemberLevelRates()
  {
    return memberLevelRates;
  }

  public void setMemberLevelRates(Map<String, String> memberLevelRates)
  {
    this.memberLevelRates = memberLevelRates;
  }

  public Map<String, String> getMemberLevelTobaccoSurcharges()
  {
    return memberLevelTobaccoSurcharges;
  }

  public void setMemberLevelTobaccoSurcharges(Map<String, String> memberLevelTobaccoSurcharges)
  {
    this.memberLevelTobaccoSurcharges = memberLevelTobaccoSurcharges;
  }

  public Map<MemberTypeEnum, Double> getMemberTypeContributions()
  {
    return memberTypeContributions;
  }

  public void setMemberTypeContributions(Map<MemberTypeEnum, Double> memberTypeContributions)
  {
    this.memberTypeContributions = memberTypeContributions;
  }

  public Map<MemberTypeEnum, Double> getMemberTypeCosts()
  {
    return memberTypeCosts;
  }

  public void setMemberTypeCosts(Map<MemberTypeEnum, Double> memberTypeCosts)
  {
    this.memberTypeCosts = memberTypeCosts;
  }

  public PlanAPTCData getAptcData()
  {
    return aptcData;
  }

  public void setAptcData(PlanAPTCData aptcData)
  {
    this.aptcData = aptcData;
  }
//<NEBRASKA RIDER SUPPORT MI>
public List<PlanRiderData> getRiders() {
	return riders;
}

public void setRiders(List<PlanRiderData> riders) {
	this.riders = riders;
}
//</NEBRASKA RIDER SUPPORT MI>

public String getBenefitSummaryUrl() {
	return benefitSummaryUrl;
}

public void setBenefitSummaryUrl(String benefitSummaryUrl) {
	this.benefitSummaryUrl = benefitSummaryUrl;
}
}