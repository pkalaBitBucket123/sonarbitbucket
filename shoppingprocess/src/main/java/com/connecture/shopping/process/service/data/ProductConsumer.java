package com.connecture.shopping.process.service.data;

public class ProductConsumer
{
  private String xid;
  private Double cost;

  public String getXid()
  {
    return xid;
  }

  public void setXid(String xid)
  {
    this.xid = xid;
  }

  public Double getCost()
  {
    return cost;
  }

  public void setCost(Double cost)
  {
    this.cost = cost;
  }
}
