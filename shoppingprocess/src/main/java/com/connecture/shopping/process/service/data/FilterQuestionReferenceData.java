package com.connecture.shopping.process.service.data;

import java.util.List;

public class FilterQuestionReferenceData
{
  private String filterRefId;
  private List<String> enabledOptionRefIds;

  public String getFilterRefId()
  {
    return filterRefId;
  }

  public void setFilterRefId(String filterRefId)
  {
    this.filterRefId = filterRefId;
  }

  public List<String> getEnabledOptionRefIds()
  {
    return enabledOptionRefIds;
  }

  public void setEnabledOptionRefIds(List<String> enabledOptionRefIds)
  {
    this.enabledOptionRefIds = enabledOptionRefIds;
  }
}
