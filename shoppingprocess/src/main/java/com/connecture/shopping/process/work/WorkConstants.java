/**
 * 
 */
package com.connecture.shopping.process.work;

public final class WorkConstants
{
  public enum Method
  {
    CREATE("create"),
    GET("get"),
    DELETE("method"),
    UPDATE("update");
    
    private String value;
    
    private Method(String value)
    {
      this.value = value;
    }

    public String getValue()
    {
      return value;
    }
    
    public static Method ignoreCaseValueOf(String methodKey)
    {
      Method matchedMethod = null;
      for (Method method : Method.values())
      {
        if (method.value.equalsIgnoreCase(methodKey))
        {
          matchedMethod = method;
        }
      }
      return matchedMethod;
    }

    @Override
    public String toString()
    {
      return value;
    }
  }

  private WorkConstants()
  {
  }
}
