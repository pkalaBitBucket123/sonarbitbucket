/**
 * 
 */
package com.connecture.shopping.process.work.core.provider;

import org.json.JSONException;
import org.json.JSONObject;

import com.connecture.shopping.process.work.WorkConstants;
import com.connecture.shopping.process.work.WorkProviderImpl;
import com.connecture.shopping.process.work.WorkConstants.Method;
import com.connecture.shopping.process.work.core.ConsumerDemographicsWork;

public abstract class ConsumerDemographicsWorkProvider implements WorkProviderImpl<ConsumerDemographicsWork>
{
  @Override
  public ConsumerDemographicsWork getWork(
    String transactionId,
    String requestKey,
    Method method,
    JSONObject params,
    JSONObject data) throws JSONException
  {
    ConsumerDemographicsWork work = createWork();

    if (WorkConstants.Method.GET.equals(method))
    {
      work.setTransactionId(transactionId);
      // right now this is being used for the producer link stuff
      work.setParams(params);
    }

    return work;
  }
}
