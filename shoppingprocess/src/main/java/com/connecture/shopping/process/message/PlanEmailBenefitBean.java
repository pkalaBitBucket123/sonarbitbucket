package com.connecture.shopping.process.message;

public class PlanEmailBenefitBean
{
  private String benefitName;
  private String benefitValue;
  /**
   * @return the benefitName
   */
  public String getBenefitName()
  {
    return benefitName;
  }
  /**
   * @param benefitName the benefitName to set
   */
  public void setBenefitName(String benefitName)
  {
    this.benefitName = benefitName;
  }
  /**
   * @return the benefitValue
   */
  public String getBenefitValue()
  {
    return benefitValue;
  }
  /**
   * @param benefitValue the benefitValue to set
   */
  public void setBenefitValue(String benefitValue)
  {
    this.benefitValue = benefitValue;
  }
}