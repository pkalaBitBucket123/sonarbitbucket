package com.connecture.shopping.process.camel.request;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;


public class ValidZipCodeCountyDataRequest extends BaseCamelRequest<JSONObject>
{
  @Produce(uri="{{camel.shopping.producer.validZipCodeCountyData}}")
  private ProducerTemplate producer;
  
  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setZipCode(String zipCode)
  {
    headers.put("zipCode", zipCode);
  }
}
