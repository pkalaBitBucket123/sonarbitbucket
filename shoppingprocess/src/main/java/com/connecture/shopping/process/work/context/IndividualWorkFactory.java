package com.connecture.shopping.process.work.context;

import com.connecture.shopping.process.work.RequestType;
import com.connecture.shopping.process.work.WorkProviderImpl;

public class IndividualWorkFactory extends ContextWorkFactoryImpl
{
  // TODO Individual is going to require both, once we handle
  // logged in individuals, remove this and setup
  // both account types as lookup-methods

  public void setAccountWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.ACCOUNT, workProvider);
  }

  public void setQuoteWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.QUOTE, workProvider);
  }

  public void setRatingPlanWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.PRODUCTS, workProvider);
  }

  public void setCostEstimatorWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.COST_ESTIMATOR_CONFIG, workProvider);
  }

  public void setCostEstimatorCostsWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.COST_ESTIMATOR_COSTS, workProvider);
  }

  public void setRulesEngineWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.RULES_ENGINE, workProvider);
  }

  public void setPovertyIncomeLevelWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.POVERTY_INCOME_LEVEL, workProvider);
  }

  public void setProviderSearchWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.PROVIDER_SEARCH, workProvider);
  }

  public void setApplicationConfigWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.APPLICATION_CONFIG, workProvider);
  }

  public void setActorWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.ACTOR, workProvider);
  }

  public void setDemographicsValidationWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.DEMOGRAPHICS_VALIDATION, workProvider);
  }

  public void setConsumerDemographicsWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.CONSUMER_DEMOGRAPHICS, workProvider);
  }

  public void setValidZipCodeCountyWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.VALID_ZIP_CODE_COUNTY_DATA, workProvider);
  }

  public void setIfpSpecialEnrollmentEffectiveDateWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.SPECIAL_ENROLLMENT_EFFECTIVE_DATE, workProvider);
  }

  public void setIfpSpecialEnrollmentEventDateValidationWorkProvider(
    WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.SPECIAL_ENROLLMENT_EVENT_DATE_VALIDATION, workProvider);
  }
  
  public void setSubsidyUsageWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.SUBSIDY_USAGE, workProvider);
  }
  
  public void setIfpProductAvailabilityWorkProvider(WorkProviderImpl<?> workProvider)
  {
    registerProvider(RequestType.AVAILABILITY, workProvider);
  }
}