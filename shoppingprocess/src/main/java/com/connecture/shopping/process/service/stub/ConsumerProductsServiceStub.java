package com.connecture.shopping.process.service.stub;

import com.connecture.shopping.process.common.JSONFileUtils;

public class ConsumerProductsServiceStub
{
  public String getConsumerProducts(Object request)
  {
    return JSONFileUtils.readJSONData(getClass(), "current-consumerProductsData.json");
  }
}
