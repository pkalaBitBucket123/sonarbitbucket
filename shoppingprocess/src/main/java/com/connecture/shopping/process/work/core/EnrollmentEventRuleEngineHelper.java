package com.connecture.shopping.process.work.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;
import com.connecture.shopping.process.service.data.SpecialEnrollmentEvent;

public class EnrollmentEventRuleEngineHelper
{
  private static Logger LOG = Logger.getLogger(EnrollmentEventEffectiveDateWorkHelper.class);
  
  public static EnrollmentEventDomainModel getDomainModel(EnrollmentEventRulesEngineBean hcrRulesEngineBean)
  {
    EnrollmentEventDomainModel domainModel = null;
    
    try 
    {
      domainModel = hcrRulesEngineBean.executeRule();
    }
    catch (Exception e) 
    {
      LOG.error("Unable to obtain HCRDomainModel", e);
    }
    
    return domainModel;
  }
  
  public static List<SpecialEnrollmentEvent> getSpecialEnrollmentEvents(
    EnrollmentEventDomainModel domainModel)
  {
    List<SpecialEnrollmentEvent> events = null;
    if (null != domainModel)
    {
      events = domainModel.getApplication().getSpecialEnrollment().getEvents();
    }
    else
    {
      events = new ArrayList<SpecialEnrollmentEvent>();
    }

    return events;
  }

  public static String getSpecialEnrollmentEffectiveDate(EnrollmentEventDomainModel domainModel)
  {
    String effectiveDate = null;
    if (null != domainModel)
    {
      effectiveDate = domainModel.getApplication().getEffectiveDate();
    }

    return effectiveDate;
  }
}
