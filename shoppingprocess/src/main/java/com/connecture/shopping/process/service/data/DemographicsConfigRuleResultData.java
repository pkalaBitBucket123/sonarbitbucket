package com.connecture.shopping.process.service.data;


public class DemographicsConfigRuleResultData
{
  private String id;
  private Long timeInMs;
  private Long timeOutMs;
  private String error;
  
  public String getId()
  {
    return id;
  }

  public void setId(String id)
  {
    this.id = id;
  }
  
  public Long getTimeInMs()
  {
    return timeInMs;
  }

  public void setTimeInMs(Long timeInMs)
  {
    this.timeInMs = timeInMs;
  }

  public Long getTimeOutMs()
  {
    return timeOutMs;
  }

  public void setTimeOutMs(Long timeOutMs)
  {
    this.timeOutMs = timeOutMs;
  }

  public String getError()
  {
    return error;
  }

  public void setError(String error)
  {
    this.error = error;
  }
}
