package com.connecture.shopping.process.common.account.conversion;

import com.connecture.shopping.process.common.JSONFileUtils;
import com.connecture.shopping.process.service.data.cache.DataProvider;

public class AccountVersioningConverterDataProvider implements DataProvider<String> 
{
  private String dataFile;
  
  @Override
  public String fetchData()
  {
    return JSONFileUtils.readJSONData(getClass(), dataFile);
  }

  public String getDataFile()
  {
    return dataFile;
  }

  public void setDataFile(String dataFile)
  {
    this.dataFile = dataFile;
  }
}
