package com.connecture.shopping.process.common.account.conversion.config;

import java.util.ArrayList;
import java.util.List;

import com.connecture.shopping.process.common.account.conversion.converter.AVPConverter;

public class AVPConfig
{
  private String newVersion;
  private String previousVersion;
  private List<AVPConverter> converters = new ArrayList<AVPConverter>();

  public String getNewVersion()
  {
    return newVersion;
  }

  public void setNewVersion(String newVersion)
  {
    this.newVersion = newVersion;
  }

  public String getPreviousVersion()
  {
    return previousVersion;
  }

  public void setPreviousVersion(String previousVersion)
  {
    this.previousVersion = previousVersion;
  }

  public List<AVPConverter> getConverters()
  {
    return converters;
  }

  public void addConverter(AVPConverter avpConvertor)
  {
    converters.add(avpConvertor);
  }
}
