package com.connecture.shopping.process.work.core;

import com.connecture.shopping.process.service.data.EnrollmentEventDomainModel;

public class EnrollmentEventRulesEngineBean extends RulesEngineBean<EnrollmentEventDomainModel>
{
  @Override
  protected EnrollmentEventDomainModel createDefaultDomainModel()
  {
    return new EnrollmentEventDomainModel();
  }
}
