package com.connecture.shopping.process.service.data;

public interface DataConverter<S, R>
{
  R convertData(S data) throws Exception;
}
