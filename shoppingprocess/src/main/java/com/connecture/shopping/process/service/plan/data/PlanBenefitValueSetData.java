/**
 * 
 */
package com.connecture.shopping.process.service.plan.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class PlanBenefitValueSetData
{
  private String internalCode;
  private String externalCode;
  private List<String> benefitValueKeys = new ArrayList<String>();
  private Map<String, PlanBenefitValueData> benefitValueMap;
  /**
   * @return the internalCode
   */
  public String getInternalCode()
  {
    return internalCode;
  }
  /**
   * @param internalCode the new value of internalCode
   */
  public void setInternalCode(String internalCode)
  {
    this.internalCode = internalCode;
  }
  /**
   * @return the externalCode
   */
  public String getExternalCode()
  {
    return externalCode;
  }
  /**
   * @param externalCode the new value of externalCode
   */
  public void setExternalCode(String externalCode)
  {
    this.externalCode = externalCode;
  }
  /**
   * @return the benefitValueKeys
   */
  public List<String> getBenefitValueKeys()
  {
    return benefitValueKeys;
  }
  /**
   * @param benefitValueKeys the new value of benefitValueKeys
   */
  public void setBenefitValueKeys(List<String> benefitValueKeys)
  {
    this.benefitValueKeys = benefitValueKeys;
  }
  /**
   * @return the benefitValueMap
   */
  public Map<String, PlanBenefitValueData> getBenefitValueMap()
  {
    return benefitValueMap;
  }
  /**
   * @param benefitValueMap the new value of benefitValueMap
   */
  public void setBenefitValueMap(Map<String, PlanBenefitValueData> benefitValueMap)
  {
    this.benefitValueMap = benefitValueMap;
  }
}