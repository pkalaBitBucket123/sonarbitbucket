package com.connecture.shopping.process.common.account.conversion.converter;

import org.json.JSONException;
import org.json.JSONObject;

public interface AVPConverter
{
  AVPConverterType getType();

  void apply(JSONObject jsonObject) throws JSONException;
}
