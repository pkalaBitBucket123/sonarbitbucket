package com.connecture.shopping.process.camel.request;

import java.util.List;
import java.util.Map;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;

import com.connecture.model.integration.data.IAToShoppingData;

public class IAToShoppingQuotePlanDataRequest extends BaseCamelRequest<IAToShoppingData>
{
  public static final String QUOTE_PLAN_IDS = "quotePlanIds";
  public static final String TRANSACTION_ID = "transactionId";

  @Produce(uri = "{{camel.shopping.producer.iaQuotePlanData}}")
  private ProducerTemplate producer;

  @Override
  public ProducerTemplate getProducer()
  {
    return producer;
  }

  public void setQuotePlanIds(List<Long> quotePlanIds)
  {
    headers.put(QUOTE_PLAN_IDS, quotePlanIds);
  }
  
  public void setTransactionId(String transactionId)
  {
    headers.put(TRANSACTION_ID, transactionId);
  }
  public void setRidersMap(Map<Long, Long> ridersMap)
  {
    headers.put("ridersMap", ridersMap);
  }
}
